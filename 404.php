<?
$notDefined = false;
if (!defined('ERROR_404')) {
    $notDefined = true;
    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
    CHTTP::SetStatus('404 Not Found');
    define('ERROR_404', 'Y');
}
?>
<? $APPLICATION->SetPageProperty('SINGLE_COLUMN', 'Y') ?>
<? $APPLICATION->SetTitle('Ошибка 404') ?>
<? $APPLICATION->IncludeComponent('romza:dummy', 'er_404') ?>
<?
if ($notDefined) {
    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");
}
?>