<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Тестовая страница");
$APPLICATION->AddChainItem("Тестовая страница", $APPLICATION->GetCurPage());
?>
	<style type="text/css">
		select.search {
			width: 100%;
		}
	</style>
	<div class="row">
		<div class="col-sm-4">
			<div class="autoPodborForm">
				<div class="form-group">
					<label for="" class="control-label">Марка</label>
					<select class="autoMark normal search" title=""></select>
				</div>
				<div class="form-group">
					<label for="">Модель</label>
					<select class="autoModel normal search" title="">
						<option value="-1">Выберите марку</option></select>
				</div>
				<div class="form-group">
					<label for="">Год</label>
					<select class="autoYear normal search" title="">
						<option value="-1">Выберите модель</option></select>
				</div>
				<div class="form-group">
					<label for="">Модификация</label>
					<select class="autoModif normal search" title="">
						<option value="-1">Выберите Год</option></select>
				</div>
				<a class="submit btn btn-primary" disabled href="javascript:;">показать варианты</a>
			</div>
		</div>
	</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>