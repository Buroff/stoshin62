<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Корзина");
?>
<?$APPLICATION->IncludeComponent(
	"bitrix:breadcrumb",
	"default",
	Array(),
	false
);?>
<h2><?$APPLICATION->ShowTitle()?></h2>
<? if (Cmodule::IncludeModule('sale')): ?>
<?$APPLICATION->IncludeComponent(
	"bitrix:sale.basket.basket", 
	".default", 
	array(
		"COLUMNS_LIST" => array(
			0 => "NAME",
			1 => "DISCOUNT",
			2 => "PROPS",
			3 => "DELETE",
			4 => "DELAY",
			5 => "PRICE",
			6 => "QUANTITY",
			7 => "SUM",
		),
		"PATH_TO_ORDER" => "/personal/order/make/",
		"HIDE_COUPON" => "N",
		"PRICE_VAT_SHOW_VALUE" => "Y",
		"COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",
		"USE_PREPAYMENT" => "N",
		"QUANTITY_FLOAT" => "N",
		"SET_TITLE" => "Y",
		"ACTION_VARIABLE" => "action",
		"OFFERS_PROPS" => array(
			0 => "COLOR_REF",
			1 => "SIZES_SHOES",
			2 => "SIZES_CLOTHES",
		),
		"RESIZER_PRODUCT" => "10",
		"ONECLICK_PERSON_TYPE_ID" => "1",
		"ONECLICK_SHOW_FIELDS" => array(
			0 => "FIO",
			1 => "EMAIL",
		),
		"ONECLICK_REQ_FIELDS" => array(
			0 => "FIO",
			1 => "EMAIL",
		),
		"ONECLICK_ALLOW_AUTO_REGISTER" => "Y",
		"ONECLICK_MESSAGE_OK" => "Ваш заказ принят, его номер - <b>#ID#</b>. Менеджер свяжется с вами в ближайшее время.<br> Спасибо что выбрали нас!",
		"ONECLICK_DELIVERY_ID" => "0",
		"ONECLICK_PAY_SYSTEM_ID" => "0",
		"ONECLICK_AS_EMAIL" => "EMAIL",
		"ONECLICK_AS_NAME" => "FIO",
		"ONECLICK_FIELD_PLACEHOLDER" => "Y",
		"ONECLICK_SEND_REGISTER_EMAIL" => "Y",
		"ONECLICK_USE_CAPTCHA" => "Y",
		"ONECLICK_USER_REGISTER_EVENT_NAME" => "USER_INFO",
	),
	false
	); ?>
<? elseif (Cmodule::IncludeModule('yenisite.market')): ?>
	<? $APPLICATION->IncludeComponent("yenisite:catalog.basket", ".default", array(
		"PROPERTY_CODE" => array(
			0 => "FIO",
			1 => "EMAIL",
			2 => "PHONE",
			3 => "ABOUT",
			4 => "DELIVERY_E",
			5 => "PAYMENT_E",
		),
		"EVENT" => "SALE_ORDER",
		"EVENT_ADMIN" => "SALE_ORDER_ADMIN",
		"YENISITE_BS_FLY" => "",
		"THANK_URL" => "//personal/cart/thank_you.php",
		"ADMIN_MAIL" => "admin@email.ru",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"INCLUDE_JQUERY" => "Y",
		"RESIZER_PRODUCT" => "10"
	),
		false
	); ?>
<? endif ?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>