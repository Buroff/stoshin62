<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Настройки пользователя");
?>
<? $APPLICATION->IncludeComponent('romza:dummy','nocomposite')?>
<? $APPLICATION->IncludeComponent(
	"bitrix:breadcrumb",
	"default",
	Array(),
	false
); ?>
	<h2><? $APPLICATION->ShowTitle() ?></h2>
	<div class="profile_wrapper">
		<? $APPLICATION->ShowViewContent('user_aside'); ?>
		<div class="profile_content">
			<? global $USER;
			if ($USER->IsAuthorized()):?>
				<? $APPLICATION->IncludeComponent("bitrix:main.profile", ".default", Array(
					"SET_TITLE" => "Y",
				),
					false
				);?>
			<? else: ?>
				<?
				if(isset($_REQUEST['register']) && $_REQUEST['register'] == 'yes') {
					$APPLICATION->IncludeComponent('bitrix:system.auth.authorize','');
				} else {
					$APPLICATION->AuthForm('', false, false, 'N', false);
				}

				?>
			<? endif ?>
		</div>
		<? ob_start();?>
		<? $APPLICATION->IncludeComponent("bitrix:menu", "personal", Array(
			"ROOT_MENU_TYPE" => "personal",
			"MAX_LEVEL" => "1",
			"CHILD_MENU_TYPE" => "personal",
			"USE_EXT" => "Y",
			"DELAY" => "N",
			"ALLOW_MULTI_SELECT" => "N",
			"MENU_CACHE_TYPE" => "N",
			"MENU_CACHE_TIME" => "3600",
			"MENU_CACHE_USE_GROUPS" => "Y",
			"MENU_CACHE_GET_VARS" => "",
		),
			false
		); ?>
		<? $content = ob_get_clean();
			$APPLICATION->AddViewContent('user_aside',$content);
		?>
	</div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>