<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Рассылки");
?>
<? $APPLICATION->IncludeComponent('romza:dummy','nocomposite')?>
<? $APPLICATION->IncludeComponent(
	"bitrix:breadcrumb",
	"default",
	Array(),
	false
); ?>
	<h2><? $APPLICATION->ShowTitle() ?></h2>
	<div class="profile_wrapper">
<? if (CModule::IncludeModule('subscribe')): ?>
		<? $APPLICATION->IncludeComponent("bitrix:menu", "personal", Array(
			"ROOT_MENU_TYPE" => "personal",
			"MAX_LEVEL" => "1",
			"CHILD_MENU_TYPE" => "personal",
			"USE_EXT" => "Y",
			"DELAY" => "N",
			"ALLOW_MULTI_SELECT" => "N",
			"MENU_CACHE_TYPE" => "N",
			"MENU_CACHE_TIME" => "3600",
			"MENU_CACHE_USE_GROUPS" => "Y",
			"MENU_CACHE_GET_VARS" => "",
		),
			false
		); ?>
		<div class="profile_content">
			<? $APPLICATION->IncludeComponent("bitrix:subscribe.edit", ".default", Array(
				"AJAX_MODE" => "N",
				"SHOW_HIDDEN" => "N",
				"ALLOW_ANONYMOUS" => "Y",
				"SHOW_AUTH_LINKS" => "Y",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "36000000",
				"SET_TITLE" => "N",
				"AJAX_OPTION_SHADOW" => "Y",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "Y",
				"AJAX_OPTION_HISTORY" => "N",
			),
				false
			); ?>
		</div>
	</div>
<?endif?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>