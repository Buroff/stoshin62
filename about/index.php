<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("О компании");
?><p style="text-align: justify;">
	 Наша огранизация ООО «Шинсервис» одна из первых начала поставку импортных автомобильных шин и дисков на Рязанский рынок.<br>
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 Мы&nbsp; являемся официальными дилерами производителей шин: <i>Michelin</i>, <i>Goodyear</i>, <i>Hankook</i>, <i>Bontyre</i>, <i>Continental</i>, <i>Bridgestone</i>, <i>Cordiant professional</i>, Кама
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
 <b>Шинсервис</b> — первый официальный представитель сети профессиональных шинных центров «Грузовой сервисный центр <i>Michelin</i>» в Рязани.
</p>
<p style="text-align: justify;">
	 Грузовой сервисный центр <i>Michelin</i>&nbsp;—&nbsp;это сеть профессиональных легковых и грузовых шинных центров, имеющая единые стандарты организации бизнеса по всей Украине, а&nbsp;также представленная в таких странах СНГ, как Россия, Казахстан и Молдова.
</p>
<p style="text-align: justify;">
	 Грузовой сервисный центр <i>Michelin</i>&nbsp;—&nbsp;это доступность широкого ассортимента шин и сопутствующих товаров, а также высокий уровень обслуживания покупателей.
</p>
<h2 style="text-align: justify; font-size: 16px;">
Основные направления деятельности компании ООО "Шинсервис"</h2>
<p style="text-align: justify;">
 <b>Продажи</b>:
</p>
<p style="text-align: justify;">
</p>
<ul style="text-align: justify;">
	<li>автошин ведущих марок мировых производителей, таких как <i>Michelin</i>, <i>Goodyear</i>, <i>Nokian</i>, <i>Bridgestone</i>, <i>Bontyre </i>и др.; </li>
	<li>колесных легкосплавных, кованных и железных дисков (СКАД, <i>K&amp;K</i>, <i>HAYES LEMMEZ</i>, <i>MAXION</i>&nbsp;...); </li>
	<li>сопутствующих товаров: гайки, болты и т.д.; </li>
	<li>аккумуляторов отечественного и импортного производства. </li>
</ul>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 На весь предлагаемый товар предоставляется гарантия.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
 <b>Оказание услуг</b>:
</p>
<p style="text-align: justify;">
</p>
<ul style="text-align: justify;">
	<li>
	&nbsp;шиномонтаж; </li>
	<li>
	&nbsp;балансировка; </li>
	<li>
	&nbsp;мойка колес; </li>
	<li>
	&nbsp;ремонт шин; </li>
	<li>
	&nbsp;нарезка протектора; </li>
	<li>
	&nbsp;ошиповка шин. </li>
</ul>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 Все работы проводятся на современном и профессиональном оборудовании квалифицированными специалистами.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 Мы готовы предложить Вам любую удобную для Вас форму оплаты: <b>наличные</b> и <b>безналичные</b> платежи,&nbsp; расчет <b>по пластиковым картам</b>.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 Для постоянных клиентов действует прогрессивная система скидок.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 Мы рады видеть Вас в своих магазинах и шинных центрах.
</p>
<p style="text-align: justify;">
</p>
<p style="text-align: justify;">
	 Мы работаем для Вас!
</p>
 <br><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>