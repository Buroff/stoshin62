<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
use \Yenisite\Core\Tools;
include 'include_module.php';
include_once($_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH . '/lang/'.LANGUAGE_ID.'/header.php');
Tools::encodeAjaxRequest($_REQUEST['REGISTER']);
?>
<? $APPLICATION->IncludeComponent('bitrix:system.auth.form', $_REQUEST['template'], Tools::GetDecodedArParams($_REQUEST['arParams'])) ?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php") ?>