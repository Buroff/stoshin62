<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

// ---
$arResult = array(
    'success' => false,
    'msg' => ''
);

// --- AJAX
$getInfo = !empty($_POST['action_get_info']) ? $_POST['action_get_info'] : '';
$type = $_POST['type'];

/*if($type == 'shiny'){
    switch ($getInfo){
        case 'by_vendor':
            $arResult['success'] = true;
            $strVendor = htmlspecialcharsbx($_POST['vendor']);
             //$arResult['data_base'] = ManagerCalc::processItemsByVendor($strVendor);
            $arResult['data_base'] = getAjaxData($strVendor);

            break;

        case 'by_vendor_model':
            $arResult['success'] = true;
            $strVendor = htmlspecialcharsbx($_POST['vendor']);
            $strModel = htmlspecialcharsbx($_POST['model']);
            //$arResult['data_base'] = ManagerCalc::processItemsByVendMod($strVendor,$strModel);
            $arResult['data_base'] = getAjaxData($strVendor, $strModel);
            break;

        case 'by_vendor_model_year':
            $arResult['success'] = true;
            $strVendor = htmlspecialcharsbx($_POST['vendor']);
            $strModel = htmlspecialcharsbx($_POST['model']);
            $strYear = htmlspecialcharsbx($_POST['year']);
            //$arResult['data_base'] = ManagerCalc::processItemsByVendModYear($strVendor,$strModel, $strYear);
            $arResult['data_base'] = getAjaxData($strVendor, $strModel, $strYear);
            break;

        case 'by_vendor_model_year_modif':
            $arResult['success'] = true;
            $strVendor = htmlspecialcharsbx($_POST['vendor']);
            $strModel = htmlspecialcharsbx($_POST['model']);
            $strYear = htmlspecialcharsbx($_POST['year']);
            $strModif = htmlspecialcharsbx($_POST['modif']);
            //$arResult['data_base'] = ManagerCalc::processItemsByVendModYearModif($strVendor,$strModel, $strYear, $strModif);
            $arResult['data_base'] = getAjaxData($strVendor, $strModel, $strYear, $strModif);
            break;
    }
}
*/
if($type == 'disky'){
 switch ($getInfo){
        case 'by_vendor':
            $arResult['success'] = true;
            $strVendor = htmlspecialcharsbx($_POST['vendor']);
             //$arResult['data_base'] = ManagerCalc::processItemsByVendor($strVendor);
            $arResult['data_base'] = getDiskAjaxData($strVendor);

            break;

        case 'by_vendor_model':
            $arResult['success'] = true;
            $strVendor = htmlspecialcharsbx($_POST['vendor']);
            $strModel = htmlspecialcharsbx($_POST['model']);
            //$arResult['data_base'] = ManagerCalc::processItemsByVendMod($strVendor,$strModel);
            $arResult['data_base'] = getDiskAjaxData($strVendor, $strModel);
            break;

        case 'by_vendor_model_year':
            $arResult['success'] = true;
            $strVendor = htmlspecialcharsbx($_POST['vendor']);
            $strModel = htmlspecialcharsbx($_POST['model']);
            $strYear = htmlspecialcharsbx($_POST['year']);
            //$arResult['data_base'] = ManagerCalc::processItemsByVendModYear($strVendor,$strModel, $strYear);
            $arResult['data_base'] = getDiskAjaxData($strVendor, $strModel, $strYear);
            break;

        case 'by_vendor_model_year_modif':
            $arResult['success'] = true;
            $strVendor = htmlspecialcharsbx($_POST['vendor']);
            $strModel = htmlspecialcharsbx($_POST['model']);
            $strYear = htmlspecialcharsbx($_POST['year']);
            $strModif = htmlspecialcharsbx($_POST['modif']);
            //$arResult['data_base'] = ManagerCalc::processItemsByVendModYearModif($strVendor,$strModel, $strYear, $strModif);
            $arResult['data_base'] = getDiskAjaxData($strVendor, $strModel, $strYear, $strModif);
            break;
    }
}

if($type == 'shiny'){
    switch ($getInfo){
        case 'by_vendor':
            $arResult['success'] = true;
            $strVendor = htmlspecialcharsbx($_POST['vendor']);
            $arResult['data_base'] = tyreAjaxData($strVendor);

            break;

        case 'by_vendor_model':
            $arResult['success'] = true;
            $strVendor = htmlspecialcharsbx($_POST['vendor']);
            $strModel = htmlspecialcharsbx($_POST['model']);
            $arResult['data_base'] = tyreAjaxData($strVendor, $strModel);
            break;

        case 'by_vendor_model_year':
            $arResult['success'] = true;
            $strVendor = htmlspecialcharsbx($_POST['vendor']);
            $strModel = htmlspecialcharsbx($_POST['model']);
            $strYear = htmlspecialcharsbx($_POST['year']);
            $arResult['data_base'] = tyreAjaxData($strVendor, $strModel, $strYear);
            break;

        case 'by_vendor_model_year_modif':
            $arResult['success'] = true;
            $strVendor = htmlspecialcharsbx($_POST['vendor']);
            $strModel = htmlspecialcharsbx($_POST['model']);
            $strYear = htmlspecialcharsbx($_POST['year']);
            $strModif = htmlspecialcharsbx($_POST['modif']);
            $arResult['data_base'] = tyreAjaxData($strVendor, $strModel, $strYear, $strModif);
            break;
    }
}



$arResult['request'] = $_REQUEST;
echo json_encode($arResult);