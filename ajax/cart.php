<?
if(isset($_REQUEST['URL'])) {
    $_SERVER["REQUEST_URI"] = $_REQUEST['URL'];
}
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
use \Yenisite\Core\Tools;
include 'include_module.php';
global $rz_options;

$APPLICATION->IncludeComponent("yenisite:settings.panel", "empty", array(
    "SOLUTION" => CRZShinmarketSettings::getModuleId(),
    "SETTINGS_CLASS" => "CRZShinmarketSettings",
    "GLOBAL_VAR" => "rz_options",
    "EDIT_SETTINGS" => array()
),
    false
);

Tools::encodeAjaxRequest($_REQUEST);
$arParams = Tools::GetDecodedArParams($_REQUEST['arParams']);
$arParams["USE_ONE_CLICK"] = $rz_options['show_one_click_in_small_basket'];

if (CModule::IncludeModule('sale') || CModule::IncludeModule('catalog')):?>
    <? if (empty($arParams["ACTION_VARIABLE"])) {
        $arParams["ACTION_VARIABLE"] = 'action';
    }
    if (strlen($_REQUEST["BasketRefresh"]) > 0 || strlen($_REQUEST["BasketOrder"]) > 0 || strlen($_REQUEST[$arParams["ACTION_VARIABLE"]]) > 0) {

        $arParamsBigBasket = Tools::getPararmsOfCMP($_SERVER['DOCUMENT_ROOT'].SITE_DIR.'personal/cart/index.php',true);

        //delete form bitrix request not needed params
        $request = Bitrix\Main\Application::getInstance()->getContext()->getRequest();
        $post = $request->getPostList()->toArray();
        unset($post["BasketOrder"],$post["BasketRefresh"],$post[$arParams["ACTION_VARIABLE"]]);
        $request->set($post);

        // todo: tmp hack until ajax recalculation is made
        if (isset($_REQUEST["BasketRefresh"]) && strlen($_REQUEST["BasketRefresh"]) > 0)
            unset($_REQUEST["BasketOrder"]);
        if (strlen($_REQUEST[$arParams["ACTION_VARIABLE"]]) > 0) {
            $id = intval($_REQUEST["id"]);
            if ($id > 0) {
                /** @noinspection PhpDynamicAsStaticMethodCallInspection */
                $dbBasketItems = CSaleBasket::GetList(
                    array("ID" => "ASC"),
                    array(
                        "FUSER_ID" => CSaleBasket::GetBasketUserID(),
                        "LID" => SITE_ID,
                        "ORDER_ID" => "NULL",
                        "ID" => $id,
                    ),
                    false,
                    false,
                    array("ID", "CALLBACK_FUNC", "MODULE", "PRODUCT_ID", "QUANTITY", "DELAY", "CAN_BUY", "CURRENCY")
                );
                $arItem = $dbBasketItems->Fetch();
                if ($arItem && !CSaleBasketHelper::isSetItem($arItem)) {
                    if ($_REQUEST[$arParams["ACTION_VARIABLE"]] == "delete") {
                        /** @noinspection PhpDynamicAsStaticMethodCallInspection */
                        CSaleBasket::Delete($arItem["ID"]);
                    } elseif ($_REQUEST[$arParams["ACTION_VARIABLE"]] == "delay") {
                        if ($arItem["DELAY"] == "N" && $arItem["CAN_BUY"] == "Y")
                            /** @noinspection PhpDynamicAsStaticMethodCallInspection */
                            CSaleBasket::Update($arItem["ID"], array("DELAY" => "Y"));
                    } elseif ($_REQUEST[$arParams["ACTION_VARIABLE"]] == "add") {
                        if ($arItem["DELAY"] == "Y" && $arItem["CAN_BUY"] == "Y")
                            /** @noinspection PhpDynamicAsStaticMethodCallInspection */
                            CSaleBasket::Update($arItem["ID"], array("DELAY" => "N"));
                    }
                    unset($_SESSION["SALE_BASKET_NUM_PRODUCTS"][SITE_ID]);
                }
            }
        } else { // if quantity is changed or coupon is set
            $_REQUEST["coupon"] = $_REQUEST["COUPON"];
            CBitrixComponent::includeComponentClass("bitrix:sale.basket.basket");
            $Basket = new CBitrixBasketComponent();
            $Basket->onPrepareComponentParams($arParamsBigBasket);
            $Basket->columns = array('QUANTITY');
            /** @noinspection PhpDynamicAsStaticMethodCallInspection */
            $arRes = $Basket->recalculateBasket($_REQUEST);
            $Basket->executeComponent();
            foreach ($arRes as $key => $value)
                $arParams[$key] = $value;

            unset($_SESSION["SALE_BASKET_NUM_PRODUCTS"][SITE_ID]);
            if (strlen($_REQUEST["BasketOrder"]) > 0 && empty($arResult["WARNING_MESSAGE"])) {
            } else {
                unset($_REQUEST["BasketRefresh"]);
                unset($_REQUEST["BasketOrder"]);

                if (!empty($arResult["WARNING_MESSAGE"]))
                    $_SESSION["SALE_BASKET_MESSAGE"] = $arResult["WARNING_MESSAGE"];

            }
        }
    }
    ?>
    <? $APPLICATION->IncludeComponent("bitrix:sale.basket.basket", $_REQUEST['template'], $arParams, false); ?>
<? elseif (CModule::IncludeModule('yenisite.market')):
    if (strlen($_REQUEST["BasketRefresh"]) > 0 || strlen($_REQUEST["BasketOrder"]) > 0 || strlen($_REQUEST['action']) > 0) {
        if (isset($_REQUEST["BasketRefresh"]) && strlen($_REQUEST["BasketRefresh"]) > 0) {
            foreach ($_REQUEST as $key => $val) {
                if (substr($key, 0, 8) == 'QUANTITY') {
                    $arItemQ = explode('_', $key);
                    $basKey = $arItemQ[1];
                    if (strlen($basKey) > 0) {
                        /** @noinspection PhpDynamicAsStaticMethodCallInspection */
                        CMarketBasket::setQuantity($basKey, $val);
                    }
                }
            }
            unset($_REQUEST["BasketRefresh"]);
            if (isset($_REQUEST['count']) && count($_REQUEST['count']) > 0) {
                foreach ($_REQUEST['count'] as $key => $val) {
                    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
                    CMarketBasket::setQuantity($key, $val);
                }
            }
            unset($_REQUEST['order'], $_REQUEST['no_calculate'], $_REQUEST['count'],
                $_POST['order'], $_POST['no_calculate'], $_POST['count']);
        }
        if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'delete') {
            /** @noinspection PhpDynamicAsStaticMethodCallInspection */
            CMarketBasket::setQuantity($_REQUEST['id'], 0);
        }
    }
    ?>
    <? $APPLICATION->IncludeComponent("yenisite:catalog.basket", $_REQUEST['template'], $arParams, false); ?>
<? else: ?>
    <? die() ?>
<? endif ?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php") ?>