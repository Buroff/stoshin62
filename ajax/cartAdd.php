<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php") ?>
<?
include_once $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/tools/include_module.php';
$bIsLite = false;
if (!CModule::IncludeModule('catalog') || !CModule::IncludeModule('sale')) {
	if (
		!function_exists('Add2BasketByProductID') && CModule::IncludeModule('yenisite.market')
	) {
		function Add2BasketByProductID($prodId, $q, $arProps = array()) {
			//check available quantity
			return !!CMarketBasket::Add($prodId, $arProps, $q);
		}
	} else {
		die();
	}
	$bIsLite = true;
}
?>
<?
$arResult = array(
	'success' => false,
	'msg' => ''
);

$arProps = array();
if(!empty($_REQUEST['PROP'])) {
	if(!CModule::IncludeModule('iblock')) die();
	if(!empty($_REQUEST['VARID'])) {
		$ID = intval($_REQUEST[$_REQUEST['VARID']]);
	} else {
		$ID = intval($_REQUEST['ID']);
	}
	/** @noinspection PhpDynamicAsStaticMethodCallInspection */
	$IBLOCK_ID = CIBlockElement::GetIBlockByID($ID);
	$rsProps = CIBlockProperty::GetList(array(),array('IBLOCK_ID' => $IBLOCK_ID));
	while($ar = $rsProps->Fetch()) {
		if(isset($_REQUEST['PROP'][$ar['CODE']])) {
			if($bIsLite) {
				$arProps[$ar['CODE']] = $_REQUEST['PROP'][$ar['CODE']];
			} else {
				$arProps[] = array(
					'NAME' => $ar['NAME'],
					'CODE' => $ar['CODE'],
					'VALUE' => $_REQUEST['PROP'][$ar['CODE']]
				);

			}
		}
	}
}

if (isset($_REQUEST['SIMPLE']) && $_REQUEST['SIMPLE'] == "Y") {
	$q = intval($_REQUEST['Q']);
	$result = Add2BasketByProductID($_REQUEST['ID'], ($q > 0) ? $q : 1, $arProps);
	if ($result !== false) {
		$arResult['success'] = true;
	} else {
		$arResult['msg'] = 'error';
	}
} else {
	$_REQUEST['VARQ'] = trim($_REQUEST['VARQ']);
	$_REQUEST['VARID'] = trim($_REQUEST['VARID']);
	$_REQUEST['VARACT'] = trim($_REQUEST['VARACT']);
	if (strlen($_REQUEST['VARQ']) == 0 || strlen($_REQUEST['VARID']) == 0 || strlen($_REQUEST['VARACT']) == 0) {
		$arResult['msg'] = 'NO Var names in request';
		echo json_encode($arResult);
		die();
	}
	$id = $_REQUEST[$_REQUEST['VARID']];
	$q = $_REQUEST[$_REQUEST['VARQ']];
	$action = $_REQUEST[$_REQUEST['VARACT']];
	if (empty($id) || empty($action)) {
		$arResult['msg'] = 'NO Vars in request';
		echo json_encode($arResult);
		die();
	}

	if ($action == 'ADD2BASKET') {
		$result = Add2BasketByProductID($id, ($q > 0) ? $q : 1, $arProps);
		if ($result !== false) {
			$arResult['success'] = true;
		} else {
			$arResult['msg'] = 'error';
		}
	}
}
echo json_encode($arResult);
?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php") ?>