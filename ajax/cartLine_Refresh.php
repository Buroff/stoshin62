<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
/**
 * @global $APPLICATION
 */
use Bitrix\Main\Loader;
use Yenisite\Core\Tools;
include 'include_module.php';
if (Loader::IncludeModule('sale')) {
    $arParamsBigBasket = Tools::getPararmsOfCMP($_SERVER['DOCUMENT_ROOT'].SITE_DIR.'personal/cart/index.php',true);
    \CBitrixComponent::includeComponentClass("bitrix:sale.basket.basket");
    $Basket = new \CBitrixBasketComponent();
    $Basket->onPrepareComponentParams($arParamsBigBasket);
    $APPLICATION->IncludeComponent("bitrix:sale.basket.basket.line", $_REQUEST['template'], Tools::GetDecodedArParams($_REQUEST['arParams']));
} elseif (Loader::IncludeModule('yenisite.market')) {
    $APPLICATION->IncludeComponent("yenisite:catalog.basket.small", $_REQUEST['template'], Tools::GetDecodedArParams($_REQUEST['arParams']));
} else {
    die();
}
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");