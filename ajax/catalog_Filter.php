<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
/**
 * @global $APPLICATION
 */
use Yenisite\Core\Tools;

include 'include_module.php';
if (!\Bitrix\Main\Loader::IncludeModule('iblock')) die();
$_REQUEST['ajax'] = 'y';
$APPLICATION->IncludeComponent("bitrix:catalog.smart.filter", "null", Tools::GetDecodedArParams($_REQUEST['arParams']));
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");