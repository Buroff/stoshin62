<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
/**
 * @global $APPLICATION
 */
use Yenisite\Core\Tools;

include 'include_module.php';
if (!\Bitrix\Main\Loader::IncludeModule('iblock')) die();

$data = json_decode($_REQUEST['data']);
echo json_encode($data);



// $_REQUEST['ajax'] = 'y';
// $APPLICATION->IncludeComponent("bitrix:catalog.smart.filter", "null", Tools::GetDecodedArParams($_REQUEST['arParams']));

$result = Tools::GetDecodedArParams('eyJJQkxPQ0tfVFlQRSI6ImNhdGFsb2ciLCJJQkxPQ0tfSUQiOjEzLCJTRUNUSU9OX0lEIjoxNiwiRklMVEVSX05BTUUiOiJhcnJGaWx0ZXIiLCJQUklDRV9DT0RFIjpbIkJBU0UiXSwiVEFCX1RJVExFIjoiIFx1MDQxZlx1MDQzZVx1MDQzNFx1MDQzMVx1MDQzZVx1MDQ0MCBcdTA0NDhcdTA0MzhcdTA0M2QiLCJJQ09OX0NMQVNTIjoiaWNvbl90eXJlYm9sZCIsIkNBQ0hFX1RZUEUiOiJBIiwiQ0FDSEVfVElNRSI6IjM2MDAwIiwiQ0FDSEVfR1JPVVBTIjp0cnVlLCJTQVZFX0lOX1NFU1NJT04iOmZhbHNlLCJYTUxfRVhQT1JUIjoiTiIsIlNFQ1RJT05fVElUTEUiOiJOQU1FIiwiU0VDVElPTl9ERVNDUklQVElPTiI6IkRFU0NSSVBUSU9OIiwiSElERV9OT1RfQVZBSUxBQkxFIjoiTiIsIkFERElUSU9OQUxfVVJMIjoiXC9zdGF0XC9rYWtfcHJhdmlsbm9fdmlicmF0X3NoaW55LnBocCIsIkFERElUSU9OQUxfVVJMX1RFWFQiOiJcdTA0MjFcdTA0M2VcdTA0MzJcdTA0MzVcdTA0NDJcdTA0NGIgXHUwNDNhXHUwNDMwXHUwNDNhIFx1MDQzZlx1MDQ0MFx1MDQzMFx1MDQzMlx1MDQzOFx1MDQzYlx1MDQ0Y1x1MDQzZFx1MDQzZSBcdTA0MzJcdTA0NGJcdTA0MzFcdTA0NDBcdTA0MzBcdTA0NDJcdTA0NGMgXHUwNDQ4XHUwNDM4XHUwNDNkXHUwNDRiIiwiSU5TVEFOVF9SRUxPQUQiOmZhbHNlLCJDT05WRVJUX0NVUlJFTkNZIjpmYWxzZSwiQ1VSUkVOQ1lfSUQiOiIifQ==');
$APPLICATION->IncludeComponent("bitrix:catalog.smart.filter", "null", Tools::GetDecodedArParams($result));

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");