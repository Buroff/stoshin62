<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
use \Yenisite\Core\Tools;

include 'include_module.php';
include_once($_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH . '/lang/'.LANGUAGE_ID.'/header.php');
Tools::encodeAjaxRequest($_REQUEST);
Tools::encodeAjaxRequest($_POST);
if (\Bitrix\Main\Loader::IncludeModule('forum')) {
	$APPLICATION->IncludeComponent("bitrix:forum.topic.reviews", $_REQUEST['template'], Tools::GetDecodedArParams($_REQUEST['arParams']));
} else {
	$APPLICATION->IncludeComponent('yenisite:feedback', $_REQUEST['template'], Tools::GetDecodedArParams($_REQUEST['arParams']));
}
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");