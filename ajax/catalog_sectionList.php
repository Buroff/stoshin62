<?
use Yenisite\Core\Tools;
use Yenisite\Core\Catalog;
use \Yenisite\Shinmarket\CacheProvider;

require_once(__DIR__ . '/functions.php');
$requestURI = prepareGetParams();
if(isset($_REQUEST['URL'])) {
	$_SERVER["REQUEST_URI"] = $_REQUEST['URL'];
}
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php") ?>
<?
include('include_module.php');
if (!CModule::IncludeModule('iblock')) die();
Tools::encodeAjaxRequest($_REQUEST);
$arParams = Tools::GetDecodedArParams($_REQUEST['arParams']);
$arFilterParams = Tools::GetDecodedArParams($_REQUEST['arFilterParams']);
if (is_array($arFilterParams) && !empty($arFilterParams)) {
	$_GET['set_filter'] = 'Y';
	$APPLICATION->IncludeComponent('bitrix:catalog.smart.filter', 'null', $arFilterParams);
}
Catalog::setAdditionalFilter($arParams['FILTER_NAME']);
$arSort = Catalog::GetSort($arParams, $_REQUEST['IS_SKU']);
$arParams['PAGE_ELEMENT_COUNT'] = $arParams['ELEMENT_COUNT'] = Catalog::getCount();
$arParams['VIEW_MODE'] = Catalog::GetViewMode();
$arParams['ELEMENT_SORT_FIELD'] = $arSort['BY'];
$arParams['ELEMENT_SORT_ORDER'] = $arSort['ORDER'];
$arParams['DISPLAY_COMPARE'] = $arParams['DISPLAY_COMPARE'] ? 'Y' : 'N';
$arParams['PAGEN_URL'] = $requestURI;
define("IS_CATALOG_LIST",true);
\CHTMLPagesCache::setUserPrivateKey(CacheProvider::getCachePrefix(), 0);

$arCustomFilter = array();
if (isset($_GET['CUSTOM_FILTER'])) {
	foreach ($_GET['CUSTOM_FILTER'] as $key => $getVal) {
		if (strpos($getVal, ',') !== false) {
			$filterVal = explode(',', $getVal);
		} else {
			$filterVal = $getVal;
		}
		$arCustomFilter[$key] = $filterVal;
	}
	unset($_GET['CUSTOM_FILTER'], $_REQUEST['CUSTOM_FILTER']);
}
if (count($arCustomFilter) > 0) {
	if (empty($arParams['FILTER_NAME'])) $arParams['FILTER_NAME'] = 'arrFilter';
	global ${$arParams['FILTER_NAME']};
	foreach ($arCustomFilter as $key => $val) {
		${$arParams['FILTER_NAME']}[$key] = $val;
	}
}
?>
<? $APPLICATION->RestartBuffer();?>
<? $APPLICATION->IncludeComponent('bitrix:catalog.section', $_REQUEST['template'], $arParams) ?>
<? die() ?>

