<?
if (isset($_REQUEST['URL'])) {
	$_SERVER["REQUEST_URI"] = $_REQUEST['URL'];
}
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
use Yenisite\Core\Tools;

include 'include_module.php';
Tools::encodeAjaxRequest($_REQUEST);
$APPLICATION->IncludeComponent('bitrix:catalog.compare.result', $_REQUEST['template'], Tools::GetDecodedArParams($_REQUEST['arParams']));
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");