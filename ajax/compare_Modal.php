<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use Yenisite\Core\Tools;

include 'include_module.php';

Tools::encodeAjaxRequest($_REQUEST);
$arParams = Tools::GetDecodedArParams($_REQUEST['arParams']);
if (isset($_REQUEST[$arParams['ACTION_VARIABLE']]) && $_REQUEST[$arParams['ACTION_VARIABLE']] == 'FLUSH_ALL') {
	if (isset($_SESSION[$arParams["NAME"]][$arParams["IBLOCK_ID"]]["ITEMS"]))
		$_SESSION[$arParams["NAME"]][$arParams["IBLOCK_ID"]]["ITEMS"] = array();
	unset($_REQUEST[$arParams['ACTION_VARIABLE']], $_POST[$arParams['ACTION_VARIABLE']], $_REQUEST[$arParams['PRODUCT_ID_VARIABLE']], $_POST[$arParams['PRODUCT_ID_VARIABLE']]);
}

$APPLICATION->IncludeComponent('bitrix:catalog.compare.list', $_REQUEST['template'], Tools::GetDecodedArParams($_REQUEST['arParams']));
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");