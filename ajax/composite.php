<?
include_once "include_stop_statistic.php";
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

include($_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/tools/include_module.php');

CBitrixComponent::includeComponentClass('yenisite:settings.panel');

function YNS_getCacheProvider()
{
	foreach (GetModuleEvents("main", "OnGetStaticCacheProvider", true) as $arEvent) {
		$provider = ExecuteModuleEventEx($arEvent);
		if (is_object($provider) && $provider instanceof \Bitrix\Main\Data\StaticCacheProvider) {
			return $provider;
		}
	}

	return null;
}

$APPLICATION->IncludeComponent("yenisite:settings.panel", "empty", array(
	"SOLUTION" => CRZShinmarketSettings::getModuleId(),
	"SETTINGS_CLASS" => "CRZShinmarketSettings",
	"GLOBAL_VAR" => "rz_options",
	"EDIT_SETTINGS" => array(
		// "COLOR_SCHEME"
	)
),
	false
);

$arOptions = \CHTMLPagesCache::getOptions();
$cookieName = $arOptions['COOKIE_PK'];
$cacheProvider = YNS_getCacheProvider();
$privateKey = $cacheProvider !== null ? $cacheProvider->setUserPrivateKey() : null;
echo json_encode(array('name' => $cookieName, 'value' => $privateKey));
die();