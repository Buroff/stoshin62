<?php
function prepareGetParams() {
	$_GET = array();
	$requestURI = "?";
	foreach ($_REQUEST as $key => $val) {
		if ($key{0} == "_") {
			$getKey = substr($key, 1);
			if (strpos($getKey, 'PAGEN') !== false) {
				$requestURI .= $getKey . '=' . $val . '&';
			}
			$_GET[$getKey] = $val;
			$_REQUEST[$getKey] = $val;
			unset($_REQUEST[$key]);
			unset($_POST[$key]);
		}
	}
	return $requestURI;
}