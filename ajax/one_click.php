<?
include_once "include_stop_statistic.php";

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

include_once "include_module.php";
include_once($_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH . '/lang/'.LANGUAGE_ID.'/header.php');

if($_SERVER["REQUEST_METHOD"] == "POST" && !defined('BX_UTF'))
{
	\Yenisite\Core\Tools::encodeAjaxRequest($_REQUEST);
	\Yenisite\Core\Tools::encodeAjaxRequest($_POST);
}

if(!isset($_REQUEST['ajax_rz']) && $_REQUEST['ajax_rz'] != 'Y') return;
if (!empty($_POST['params']) && (!empty($_POST['ID']) || !empty($_POST['RZ_BASKET']))){
	$arResult = $_POST;
	$arResult['params'] = \Yenisite\Core\Tools::GetDecodedArParams($arResult['params']);
	$arResult['params']['IBLOCK_ELEMENT_ID'] = $arResult['ID'];
	$arResult['params'] = \Yenisite\Core\Tools::GetEncodedArParams($arResult['params']);
	$arResult['sucess'] = true;
	echo  json_encode($arResult);
} else{
	$arResult['sucess'] = false;
	$arResult['msg'] = 'sorry bit you have not all data';
	echo  json_encode($arResult);
}
 require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");