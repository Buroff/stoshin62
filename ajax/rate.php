<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
use Yenisite\Core\Tools;
include 'include_module.php';

Tools::encodeAjaxRequest($_REQUEST);
$arRequest = Tools::GetDecodedArParams($_REQUEST['arParams']);
unset($_REQUEST['arParams']);
foreach ($arRequest as $key => $value) {
	$_REQUEST[$key] = $value;
}
unset($arRequest);

define('PUBLIC_AJAX_MODE', true);
/** @global CMain $APPLICATION */
global $APPLICATION, $USER;
$arResult = array('success' => true);
if (\Bitrix\Main\Loader::IncludeModule("iblock")) {
	ob_start();
	/** @noinspection PhpDynamicAsStaticMethodCallInspection */
	$arCache = CIBlockRSS::GetCache($_REQUEST["SESSION_PARAMS"]);
	if ($arCache && ($arCache["VALID"] == "Y")) {
		$arParams = unserialize($arCache["CACHE"]);

		foreach ($arParams["PAGE_PARAMS"] as $param_name) {
			if (!array_key_exists($param_name, $arParams))
				$arParams[$param_name] = $_REQUEST["PAGE_PARAMS"][$param_name];
		}

		if (array_key_exists("PARENT_NAME", $arParams)) {
			$component = new CBitrixComponent();
			$component->InitComponent($arParams["PARENT_NAME"], $arParams["PARENT_TEMPLATE_NAME"]);
			$component->InitComponentTemplate($arParams["PARENT_TEMPLATE_PAGE"]);
		} else {
			$component = null;
		}

		$arParams["AJAX_CALL"] = "Y";
		--$_REQUEST["rating"];
		$APPLICATION->IncludeComponent($arParams["COMPONENT_NAME"], $arParams["TEMPLATE_NAME"], $arParams, $component);
	}
	$content = ob_get_clean();
	$arResult['msg'] = $content;
}
echo json_encode(Tools::GetArrayResponse($arResult));
die();