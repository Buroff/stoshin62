<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
use Yenisite\Core\Tools;

include 'include_module.php';
include_once($_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH . '/lang/'.LANGUAGE_ID.'/header.php');

Tools::encodeAjaxRequest($_REQUEST);
Tools::encodeAjaxRequest($_POST);

if ($_POST['pass_gen'] > 0) {
	$arTmp["NEW_EMAIL"] = $_POST['REGISTER']['EMAIL'];
	$arTmp["NEW_LOGIN"] = $_POST['REGISTER']['EMAIL'];

	$pos = strpos($arTmp["NEW_LOGIN"], "@");
	if ($pos !== false)
		$_POST['REGISTER']['LOGIN'] = substr($arTmp["NEW_LOGIN"], 0, $pos);

	if (strlen($arTmp["NEW_LOGIN"]) > 47)
		$_POST['REGISTER']['LOGIN'] = substr($arTmp["NEW_LOGIN"], 0, 47);

	if (strlen($arTmp["NEW_LOGIN"]) < 3)
		$_POST['REGISTER']['LOGIN'] .= "_";

	if (strlen($arTmp["NEW_LOGIN"]) < 3)
		$_POST['REGISTER']['LOGIN'] .= "_";

	/** @noinspection PhpDynamicAsStaticMethodCallInspection */
	$dbUserLogin = CUser::GetByLogin($_POST['REGISTER']['LOGIN']);
	if ($arUserLogin = $dbUserLogin->Fetch()) {
		$newLoginTmp = $arTmp["NEW_LOGIN"];
		$uind = 0;
		do {
			$uind++;
			if ($uind == 10) {
				$arTmp["NEW_LOGIN"] = $arTmp["NEW_EMAIL"];
				$newLoginTmp = $arTmp["NEW_LOGIN"];
			} elseif ($uind > 10) {
				$arTmp["NEW_LOGIN"] = "buyer" . time() . GetRandomCode(2);
				$newLoginTmp = $arTmp["NEW_LOGIN"];
				break;
			} else {
				$newLoginTmp = $arTmp["NEW_LOGIN"] . $uind;
			}
			/** @noinspection PhpDynamicAsStaticMethodCallInspection */
			$dbUserLogin = CUser::GetByLogin($newLoginTmp);
		} while ($arUserLogin = $dbUserLogin->Fetch());
		$_POST['REGISTER']['LOGIN'] = $newLoginTmp;
	}

	$def_group = COption::GetOptionString("main", "new_user_registration_def_group", "");
	if ($def_group != "") {
		$GROUP_ID = explode(",", $def_group);
		$arPolicy = $USER->GetGroupPolicy($GROUP_ID);
	} else {
		$arPolicy = $USER->GetGroupPolicy(array());
	}

	$password_min_length = intval($arPolicy["PASSWORD_LENGTH"]);
	if ($password_min_length <= 0)
		$password_min_length = 6;
	$password_chars = array(
		"abcdefghijklnmopqrstuvwxyz",
		"ABCDEFGHIJKLNMOPQRSTUVWXYZ",
		"0123456789",
	);
	if ($arPolicy["PASSWORD_PUNCTUATION"] === "Y")
		$password_chars[] = ",.<>/?;:'\"[]{}\|`~!@#\$%^&*()-_+=";
	$_POST['REGISTER']['PASSWORD'] = $_POST['REGISTER']['CONFIRM_PASSWORD'] = randString($password_min_length + 2, $password_chars);

	$_REQUEST['REGISTER'] = $_POST['REGISTER'];
}

$APPLICATION->IncludeComponent("bitrix:main.register", $_REQUEST['template'], Tools::GetDecodedArParams($_REQUEST['arParams']));
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");