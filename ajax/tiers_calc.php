<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
use \Yenisite\Shinmarket\ManagerCalc;
use \Yenisite\Core\Tools;

include 'include_module.php';

$arResult = array(
    'success' => false,
    'msg' => ''
);


$getInfo = !empty($_POST['action_get_info']) ? $_POST['action_get_info'] : '';

switch ($getInfo){
    case 'by_vendor':
        $arResult['success'] = true;
        $strVendor = htmlspecialcharsbx($_POST['vendor']);
        $arResult['data_base'] = ManagerCalc::processItemsByVendor($strVendor);
        break;

    case 'by_vendor_model':
        $arResult['success'] = true;
        $strVendor = htmlspecialcharsbx($_POST['vendor']);
        $strModel = htmlspecialcharsbx($_POST['model']);
        $arResult['data_base'] = ManagerCalc::processItemsByVendMod($strVendor,$strModel);
        break;

    case 'by_vendor_model_year':
        $arResult['success'] = true;
        $strVendor = htmlspecialcharsbx($_POST['vendor']);
        $strModel = htmlspecialcharsbx($_POST['model']);
        $strYear = htmlspecialcharsbx($_POST['year']);
        $arResult['data_base'] = ManagerCalc::processItemsByVendModYear($strVendor,$strModel, $strYear);
        break;

    case 'by_vendor_model_year_modif':
        $arResult['success'] = true;
        $strVendor = htmlspecialcharsbx($_POST['vendor']);
        $strModel = htmlspecialcharsbx($_POST['model']);
        $strYear = htmlspecialcharsbx($_POST['year']);
        $strModif = htmlspecialcharsbx($_POST['modif']);
        $arResult['data_base'] = ManagerCalc::processItemsByVendModYearModif($strVendor,$strModel, $strYear, $strModif);
        break;
}

if (empty($arResult['data_base']) && !empty($getInfo)){
    $arResult['success'] = false;
    $arResult['msg'] = 'not right request send';
}

//get count of finded elements
if (!empty($_REQUEST['get_count']) || !empty($getInfo)){
    $arParamsOfCalc = Tools::getPararmsOfCMP($_SERVER['DOCUMENT_ROOT'].SITE_DIR.'include_areas/site_template/filter_on_main.php',true);
    if (!empty($arParamsOfCalc)){
        $arFilter = array(
            "ACTIVE" => 'Y',
            "INCLUDE_SUBSECTIONS" => "Y",
            "SECTION_ID" => $arParamsOfCalc["CALC_SECTION_TYRES_ID"],
            "IBLOCK_ID" => $arParamsOfCalc["CALC_IBLOCK_ID"],
            array("LOGIC" => 'OR',
                array("!PROPERTY_".$arParamsOfCalc['CALC_PROP_WIDTH'] => false),
                array("!PROPERTY_".$arParamsOfCalc['CALC_PROP_HEIGHT'] => false),
                array("!PROPERTY_".$arParamsOfCalc['CALC_PROP_DIAMTER'] => false)
            )
        );
        $arSelect =  array("PROPERTY_".$arParamsOfCalc['CALC_PROP_WIDTH'],"PROPERTY_".$arParamsOfCalc['CALC_PROP_HEIGHT'],"PROPERTY_".$arParamsOfCalc['CALC_PROP_DIAMTER']);
        $dbElements = CRZShinmarket::getElementsByFilter($arFilter,$arSelect);
        $arPropsValues = CRZShinmarket::proccessTiersProps($dbElements,$arParamsOfCalc);

        $arFilter = array();
        $dataBase = $arResult['data_base']['TIERS_SIZES'] ? $arResult['data_base']['TIERS_SIZES'] : $arResult['data_base']['MODELS'][0]['TIERS_SIZES'];
        $width = htmlspecialcharsbx($_REQUEST['width']) ? htmlspecialcharsbx($_REQUEST['width']) : $dataBase['FRONTS_W'][0];
        $height = htmlspecialcharsbx($_REQUEST['height']) ? htmlspecialcharsbx($_REQUEST['height']) : $dataBase['FRONTS_P'][0];
        $radius = htmlspecialcharsbx($_REQUEST['radius']) ? htmlspecialcharsbx($_REQUEST['radius']) : $dataBase['FRONTS_D'][0];

        if (in_array($width,$arPropsValues['W'])) {
            $arFilter['PROPERTY_' . $arParamsOfCalc['CALC_PROP_WIDTH'] . '_VALUE'] = $width;
        }
        if (in_array($height,$arPropsValues['H'])) {
            $arFilter['PROPERTY_' . $arParamsOfCalc['CALC_PROP_HEIGHT']. '_VALUE'] = $height;
        }
        if (in_array($radius,$arPropsValues['R'])) {
            $arFilter['PROPERTY_' . $arParamsOfCalc['CALC_PROP_DIAMTER']. '_VALUE'] = $radius;
        }
        $arFilter['SECTION_ID'] = $arParamsOfCalc['CALC_SECTION_TYRES_ID'];
        $arFilter['IBLOCK_ID'] = $arParamsOfCalc['CALC_IBLOCK_ID'];
        $arResult['count'] = CRZShinmarket::getCntElementsByFilter($arFilter);
        $arResult['success'] = true;
    }
}


echo json_encode($arResult);