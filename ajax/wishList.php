<?
include_once "include_stop_statistic.php";
use Yenisite\Core\Ajax;
use Yenisite\Core\Tools;

if((isset($_POST["rz_ajax"]) && $_POST["rz_ajax"] === "y") || ($_GET["rz_ajax"] && $_GET["rz_ajax"] === "y"))
{
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
}else{
	if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
}

include($_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/tools/include_module.php');

Tools::encodeAjaxRequest($_REQUEST);
Tools::encodeAjaxRequest($_GET);
$arAjax = Tools::GetDecodedArParams($_REQUEST['ajax']);

$arParams = Ajax::getParams($arAjax['CMP'], $arAjax['ID'], $arAjax['PAGE'], SITE_ID);
if (empty($arParams)) {
	die('[ajax died] arParams - empty');
}

include_once($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH."/lang/".LANGUAGE_ID."/header.php");
include_once($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH."/lang/".LANGUAGE_ID."/ajax.php");

$APPLICATION->IncludeComponent($arAjax['CMP'], $arAjax['TMPL'], $arParams, $Cmp);
?>