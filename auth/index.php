<?
define("NEED_AUTH", true);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

if (isset($_REQUEST["backurl"]) && strlen($_REQUEST["backurl"]) > 0) {
	LocalRedirect($backurl);
}

$APPLICATION->SetTitle("Авторизация");
$APPLICATION->AddChainItem("Авторизация");
?>
	<section class="password-page">
        <div>Вы зарегистрированы и успешно авторизовались.</div>
		<h4>Каждый зарегистрированный пользователь получает ряд преимуществ:</h4>
		<ul>
			<li>Отслеживание состояния заказа</li>
			<li>Информация об акциях и скидках</li>
			<li>Индивидуальные предложения</li>
		</ul>

		<p><a href="<?= SITE_DIR ?>">Вернуться на главную страницу</a></p>

	</section>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>