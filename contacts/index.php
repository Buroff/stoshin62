<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Контакты");
?><h2>Контакты</h2>
<div>
	 После оформления заказа менеджер перезванивает для подтверждения и уточнения деталей в рабочее время<br>
	<b>пн-пт</b>: 9:00 - 17:00
</div>
 <br>
<p style="font-weight: bold;">
	 Офисы:
</p>
<ul>
</ul>
<ul>
	<li>г.Рязань, ул. Строителей,&nbsp; д.14&nbsp;&nbsp;<br>
	 тел.:&nbsp;<b>(4912) 99-33-66<br>
	 (4912)&nbsp;20-51-50</b></li>
 <br>
	<li>г.Рязань, р-н Южный Промузел, д.6, стр.4 (территория завода “Центролит”), <span style="color: #f16522;">шинный центр</span><br>
	 тел.:&nbsp;<b>(4912) 24-02-69</b><br>
 <b>+7 (903) 641-90-54</b></li>
 <br>
	<li>г.Рязань, ФАД М-5 “Москва-Самара” 178 км +930 м (слева), <span style="color: #f16522;">шинный центр</span><br>
	 тел.:&nbsp;<b>(4912) 40-71-18</b></li>
</ul>
<p style="font-weight: bold;">
	 E-mail:
</p>
<ul>
 <a href="mailto:205150@mail.ru">205150@mail.ru</a>
</ul>
 <br>
<div style="margin: 0 auto;">

	 <?$APPLICATION->IncludeComponent(
	"bitrix:map.yandex.view",
	".default",
	Array(
		"COMPONENT_TEMPLATE" => ".default",
		"CONTROLS" => array(0=>"ZOOM",1=>"TYPECONTROL",2=>"SCALELINE",3=>"SEARCH",),
		"INIT_MAP_TYPE" => "MAP",
		"MAP_DATA" => "a:4:{s:10:\"yandex_lat\";d:54.637939105664245;s:10:\"yandex_lon\";d:39.657475956922006;s:12:\"yandex_scale\";i:11;s:10:\"PLACEMARKS\";a:3:{i:0;a:3:{s:3:\"LON\";d:39.685316;s:3:\"LAT\";d:54.612446;s:4:\"TEXT\";s:32:\"ул Строителей, д.14\";}i:1;a:3:{s:3:\"LON\";d:39.727693542328;s:3:\"LAT\";d:54.581819567184;s:4:\"TEXT\";s:83:\"р-н Южный промузел, д.6, стр. 4###RN###(шинный центр)\";}i:2;a:3:{s:3:\"LON\";d:39.578187;s:3:\"LAT\";d:54.67386;s:4:\"TEXT\";s:105:\"ФАД М-5 «Москва-Самара» 178км  + 930 м (слева)###RN###(шинный центр)\";}}}",
		"MAP_HEIGHT" => "400",
		"MAP_ID" => "",
		"MAP_WIDTH" => "750",
		"OPTIONS" => array(0=>"ENABLE_SCROLL_ZOOM",1=>"ENABLE_DRAGGING",)
	)
);?>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>