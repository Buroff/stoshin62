<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Отзывы покупателей"); ?>
 <?$APPLICATION->IncludeComponent(
	"yenisite:feedback", 
	".default", 
	array(
		"IBLOCK_TYPE" => "romza_feedback_shinmarket",
		"IBLOCK" => "8",
		"NAME_FIELD" => "name",
		"COLOR_SCHEME" => "green",
		"TITLE" => "",
		"SUCCESS_TEXT" => "Ваше сообщение отправлено, оно появится после проверки модератором",
		"ACTIVE" => "N",
		"ALLOW_RESPONSE" => "Y",
		"ALWAYS_SHOW_PAGES" => "N",
		"USE_CAPTCHA" => "Y",
		"TEXT_SHOW" => "Y",
		"TEXT_REQUIRED" => "Y",
		"EVENT_NAME" => "Гостевая книга",
		"PRINT_FIELDS" => array(
			0 => "NAME",
			1 => "EMAIL",
			2 => "AVATAR",
			3 => "RAITING",
		),
		"MESS_PER_PAGE" => "10",
		"ELEMENT_ID" => "",
		"SEF_MODE" => "Y",
		"SEF_FOLDER" => "/guestbook/",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "300",
		"NAME" => "NAME",
		"EMAIL" => "EMAIL",
		"PHONE" => "EMAIL",
		"AJAX_OPTION_ADDITIONAL" => ""
	),
	false
);?> 
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>