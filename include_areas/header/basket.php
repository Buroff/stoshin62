<?if(CModule::IncludeModule('catalog')):?>
    <?$APPLICATION->IncludeComponent("bitrix:sale.basket.basket.line", ".default", Array(
        "PATH_TO_BASKET" => SITE_DIR."personal/cart/",
        "PATH_TO_PERSONAL" => SITE_DIR."personal/",
        "SHOW_PERSONAL_LINK" => "N",
        "SHOW_NUM_PRODUCTS" => "Y",
        "SHOW_TOTAL_PRICE" => "Y",
        "SHOW_PRODUCTS" => "Y",
        "POSITION_FIXED" => "N",
        "SHOW_DELAY" => "N",
        "SHOW_NOTAVAIL" => "N",
        "SHOW_SUBSCRIBE" => "N",
        "SHOW_IMAGE" => "N",
        "SHOW_PRICE" => "N",
    ),
        false
    );?>
<?elseif(CModule::IncludeModule('yenisite.market')):?>
    <?$APPLICATION->IncludeComponent(
        "yenisite:catalog.basket.small",
        ".default",
        array(
            "VALUTA" => "руб.",
            "BASKET_URL" => "/#SITE_DIR#/personal/cart/"
        ),
        false
    );?>
<?endif;?>