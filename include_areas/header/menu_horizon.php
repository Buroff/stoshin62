<?global $rz_options;
$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"main_horizontal", 
	array(
		"ROOT_MENU_TYPE" => "catalog",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_TIME" => "604800",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MAX_LEVEL" => "2",
		"CHILD_MENU_TYPE" => "",
		"USE_EXT" => "Y",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "N",
		"VIEW_HIT" => "Y",
		"PRICE_CODE" => "BASE",
		"CURRENCY" => "RUB",
		"INCLUDE_JQUERY" => "Y",
		"RUB_SIGN" => "Y",
		"RESIZER_PRODUCT" => "3",
		"VIEW_MODE" => $rz_options["switch_main_menu"],
		"COMPONENT_TEMPLATE" => "main_horizontal",
		"THEME" => $ys_options["color_scheme"]
	),
	false
);

?>