<? if (CModule::IncludeModule('sale')): ?>
<?

$APPLICATION->IncludeComponent(
	"bitrix:sale.basket.basket.line",
	"popup", 
	array(
		"PATH_TO_BASKET" => "/personal/cart/",
		"PATH_TO_ORDER" => "/personal/order.php",
		"SHOW_PERSONAL_LINK" => "N",
		"SHOW_NUM_PRODUCTS" => "Y",
		"SHOW_TOTAL_PRICE" => "Y",
		"SHOW_PRODUCTS" => "Y",
		"POSITION_FIXED" => "N",
		"SHOW_DELAY" => "N",
		"SHOW_NOTAVAIL" => "N",
		"SHOW_SUBSCRIBE" => "N",
		"SHOW_IMAGE" => "N",
		"SHOW_PRICE" => "N",
		"RESIZER_PRODUCT" => "11",
		"ONECLICK_PERSON_TYPE_ID" => "1",
		"ONECLICK_SHOW_FIELDS" => array(
			0 => "FIO",
			1 => "EMAIL",
		),
		"ONECLICK_REQ_FIELDS" => array(
			0 => "FIO",
			1 => "EMAIL",
		),
		"ONECLICK_ALLOW_AUTO_REGISTER" => "Y",
		"ONECLICK_MESSAGE_OK" => "Ваш заказ принят, его номер - <b>#ID#</b>. Менеджер свяжется с вами в ближайшее время.<br> Спасибо что выбрали нас!",
		"ONECLICK_DELIVERY_ID" => "0",
		"ONECLICK_PAY_SYSTEM_ID" => "0",
		"ONECLICK_AS_EMAIL" => "EMAIL",
		"ONECLICK_AS_NAME" => "FIO",
		"ONECLICK_FIELD_PLACEHOLDER" => "Y",
		"ONECLICK_SEND_REGISTER_EMAIL" => "Y",
		"ONECLICK_USE_CAPTCHA" => "Y",
		"ONECLICK_USER_REGISTER_EVENT_NAME" => "USER_INFO",
	),

	false
);?><? else: ?>
<? $APPLICATION->IncludeComponent("yenisite:catalog.basket.small", "popup", Array(
		"PATH_TO_BASKET" => "/personal/cart/",
		"PATH_TO_ORDER" => "/personal/order/",
		"SHOW_DELAY" => "Y",
		"SHOW_NOTAVAIL" => "Y",
		"SHOW_SUBSCRIBE" => "Y",
		"RESIZER_PRODUCT" => "11",
		"PROPERTY_CODE" => array(
			0 => "FIO",
			1 => "EMAIL",
			2 => "PHONE",
			3 => "ABOUT",
			4 => "DELIVERY_E",
		),
		"EVENT" => "SALE_ORDER",
		"EVENT_ADMIN" => "SALE_ORDER_ADMIN",
		"THANK_URL" => "/personal/cart/thank_you.php",
		"ADMIN_MAIL" => ".",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
	),
	false
); ?>
<? endif;