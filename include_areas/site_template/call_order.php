<?if ( CModule::IncludeModule('yenisite.feedback') ):?>
<?$APPLICATION->IncludeComponent("yenisite:feedback.add", "call_order", array(
	"IBLOCK_TYPE" => "romza_feedback_shinmarket",
	"IBLOCK" => "5",
	"NAME_FIELD" => "name",
	"COLOR_SCHEME" => "green",
	"TITLE" => "Заказать звонок",
	"SUCCESS_TEXT" => "Спасибо! Ваше обращение принято. После обработки наш специалист свяжется с Вами.",
	"ACTIVE" => "Y",
	"USE_CAPTCHA" => "N",
	"TEXT_SHOW" => "N",
	"TEXT_REQUIRED" => "N",
	"SHOW_SECTIONS" => "Y",
	"EVENT_NAME" => "Заказать звонок",
	"PRINT_FIELDS" => array(
		0 => "name",
		1 => "phone",
	),
	"SECTION_CODE" => "",
	"ELEMENT_ID" => "",
	"AJAX_MODE" => "Y",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "N",
	"AJAX_OPTION_HISTORY" => "N",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "300",
	"NAME" => "name",
	"EMAIL" => "email",
	"PHONE" => "phone",
	"AJAX_OPTION_ADDITIONAL" => ""
	),
	false
);?>
<?endif?>