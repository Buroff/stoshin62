<?if ( CModule::IncludeModule('yenisite.feedback') ):?>
<? $APPLICATION->IncludeComponent(
	"yenisite:feedback.add",
	"price_abuse",
	array(
		"IBLOCK_TYPE" => "romza_feedback_shinmarket",
		"IBLOCK" => "4",
		"NAME_FIELD" => "email",
		"COLOR_SCHEME" => "green",
		"TITLE" => "Нашли дешевле",
		"SUCCESS_TEXT" => "Спасибо! После обработки запроса наш специалист свяжется с Вами.",
		"USE_CAPTCHA" => "N",
		"SHOW_SECTIONS" => "N",
		"PRINT_FIELDS" => array(
			0 => "email",
			1 => "TOVAR",
			2 => "HREF",
		),
		"AJAX_MODE" => "Z",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "300",
		"ACTIVE" => "Y",
		"EVENT_NAME" => "Нашли дешевле",
		"TEXT_REQUIRED" => "N",
		"TEXT_SHOW" => "N",
		"NAME" => "email",
		"EMAIL" => "email",
		"PHONE" => "email",
		"ELEMENT_ID" => $arParams["ID"],
		"MESSAGE" => $_POST["romza_feedback"]["text"],
		"SECTION_CODE" => "",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => ""
	),
	false
); ?>
<?endif?>