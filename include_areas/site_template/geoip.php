<? if (CModule::IncludeModule('catalog') && CModule::IncludeModule('yenisite.geoipstore')): ?>
	<?
	global $rz_options;

	$arRes = $APPLICATION->IncludeComponent(
		"yenisite:geoip.store",
		".default",
		array(
			"CACHE_TYPE" => "A",
			"CACHE_TIME" => "360000",
			"COLOR_SCHEME" => '',
			"INCLUDE_JQUERY" => "N",
			"NEW_FONTS" => "Y"
		),
		false
	);
	if (!empty($arRes['PRICES'])) {
		$rz_options['GEOIP']['PRICES'] = $arRes['PRICES'];
	}
	$rz_options['GEOIP']['STORES'] = $arRes['STORES'];
	?>
<? endif; ?>
<? if (CModule::IncludeModule('yenisite.geoip')): ?>
	<? $APPLICATION->IncludeComponent(
		"yenisite:geoip.city",
		".default",
		array(
			"CACHE_TYPE" => "A",
			"CACHE_TIME" => "36000",
			"COLOR_SCHEME" => "",
			"INCLUDE_JQUERY" => "N",
			"NEW_FONTS" => "Y",
			"P1_LOCATION_ID" => "6",
			"P1_CITY_ID" => "5",
			"P2_LOCATION_ID" => "18",
			"P2_CITY_ID" => "17",
			"AUTOCONFIRM" => "Y",
			"DISABLE_CONFIRM_POPUP" => "N",
			"CITY_1" => "671",
			"CITY_2" => "617",
			"CITY_3" => "822",
			"CITY_4" => "148",
			"CITY_5" => "1009",
			"CITY_6" => "219",
			"CITY_7" => "270",
			"CITY_8" => "922",
			"CITY_9" => "246"
		),
		false
	); ?>
<? endif ?>