<?if ( CModule::IncludeModule('yenisite.feedback') ):?>
<? $APPLICATION->IncludeComponent(
	"yenisite:feedback.add",
	"price_follow",
	array(
		"IBLOCK_TYPE" => "romza_feedback_shinmarket",
		"IBLOCK" => "7",
		"NAME_FIELD" => "email",
		"COLOR_SCHEME" => "green",
		"TITLE" => "Следить за ценой",
		"SUCCESS_TEXT" => "Спасибо! В случае снижения цены до заданной мы сообщим Вам.",
		"USE_CAPTCHA" => "N",
		"SHOW_SECTIONS" => "N",
		"PRINT_FIELDS" => array(
			0 => "email",
			1 => "TOVAR",
			2 => "NEED_PRICE",
		),
		"AJAX_MODE" => "Z",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "300",
		"ACTIVE" => "Y",
		"EVENT_NAME" => "Снижение цены",
		"TEXT_REQUIRED" => "N",
		"TEXT_SHOW" => "N",
		"NAME" => "email",
		"EMAIL" => "email",
		"PHONE" => "email",
		"ELEMENT_ID" => $arParams['ID'],
		'MIN_PRICE' => $arParams['MIN_PRICE_VALUE'],
		"MESSAGE" => $_POST["romza_feedback"]["text"],
		"SECTION_CODE" => "",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => ""
	),
	false
); ?>
<?endif?>