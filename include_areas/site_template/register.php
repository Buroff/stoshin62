<?
if ($USER->IsAuthorized() || COption::GetOptionString("main", "new_user_registration", "N") == "N") {
	return;
}

$APPLICATION->IncludeComponent(
	"bitrix:main.register", 
	".default", 
	array(
		"USER_PROPERTY_NAME" => "",
		"SHOW_FIELDS" => array(
			0 => "NAME",
			1 => "LAST_NAME",
		),
		"REQUIRED_FIELDS" => array(
		),
		"AUTH" => "Y",
		"USE_BACKURL" => "Y",
		"SUCCESS_PAGE" => "",
		"SET_TITLE" => "N",
		"USER_PROPERTY" => array(),
		"RULEZ_URL" => "/about/usloviya-konfidetsialnosti.php",
	),
	false
);?>