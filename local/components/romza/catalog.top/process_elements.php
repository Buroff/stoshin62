<?php
$rsElements = CIBlockElement::GetList($arSort, $arrFilter, false, array("nTopCount" => $arParams["ELEMENT_COUNT"]), $arSelect);
$rsElements->SetUrlTemplates($arParams["DETAIL_URL"]);

while($arItem = $rsElements->GetNext())
{
	$arItem['ID'] = intval($arItem['ID']);

	$arItem['ACTIVE_FROM'] = $arItem['DATE_ACTIVE_FROM'];
	$arItem['ACTIVE_TO'] = $arItem['DATE_ACTIVE_TO'];

	$arButtons = CIBlock::GetPanelButtons(
		$arItem["IBLOCK_ID"],
		$arItem["ID"],
		$arItem["IBLOCK_SECTION_ID"],
		array("SECTION_BUTTONS"=>false, "SESSID"=>false, "CATALOG"=>true)
	);
	$arItem["EDIT_LINK"] = $arButtons["edit"]["edit_element"]["ACTION_URL"];
	$arItem["DELETE_LINK"] = $arButtons["edit"]["delete_element"]["ACTION_URL"];

	$ipropValues = new \Bitrix\Iblock\InheritedProperty\ElementValues($arItem["IBLOCK_ID"], $arItem["ID"]);
	$arItem["IPROPERTY_VALUES"] = $ipropValues->getValues();

	$arItem["PREVIEW_PICTURE"] = (0 < $arItem["PREVIEW_PICTURE"] ? CFile::GetFileArray($arItem["PREVIEW_PICTURE"]) : false);
	if ($arItem["PREVIEW_PICTURE"])
	{
		$arItem["PREVIEW_PICTURE"]["ALT"] = $arItem["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_ALT"];
		if ($arItem["PREVIEW_PICTURE"]["ALT"] == "")
			$arItem["PREVIEW_PICTURE"]["ALT"] = $arItem["NAME"];
		$arItem["PREVIEW_PICTURE"]["TITLE"] = $arItem["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_TITLE"];
		if ($arItem["PREVIEW_PICTURE"]["TITLE"] == "")
			$arItem["PREVIEW_PICTURE"]["TITLE"] = $arItem["NAME"];
	}
	$arItem["DETAIL_PICTURE"] = (0 < $arItem["DETAIL_PICTURE"] ? CFile::GetFileArray($arItem["DETAIL_PICTURE"]) : false);
	if ($arItem["DETAIL_PICTURE"])
	{
		$arItem["DETAIL_PICTURE"]["ALT"] = $arItem["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"];
		if ($arItem["DETAIL_PICTURE"]["ALT"] == "")
			$arItem["DETAIL_PICTURE"]["ALT"] = $arItem["NAME"];
		$arItem["DETAIL_PICTURE"]["TITLE"] = $arItem["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"];
		if ($arItem["DETAIL_PICTURE"]["TITLE"] == "")
			$arItem["DETAIL_PICTURE"]["TITLE"] = $arItem["NAME"];
	}

	$arItem["PROPERTIES"] = array();
	$arItem["DISPLAY_PROPERTIES"] = array();
	$arItem["PRODUCT_PROPERTIES"] = array();
	$arItem['PRODUCT_PROPERTIES_FILL'] = array();

	if ($bIBlockCatalog)
	{
		if (!isset($arItem["CATALOG_MEASURE_RATIO"]))
			$arItem["CATALOG_MEASURE_RATIO"] = 1;
		if (!isset($arItem['CATALOG_MEASURE']))
			$arItem['CATALOG_MEASURE'] = 0;
		$arItem['CATALOG_MEASURE'] = intval($arItem['CATALOG_MEASURE']);
		if (0 > $arItem['CATALOG_MEASURE'])
			$arItem['CATALOG_MEASURE'] = 0;
		if (!isset($arItem['CATALOG_MEASURE_NAME']))
			$arItem['CATALOG_MEASURE_NAME'] = '';

		$arItem['CATALOG_MEASURE_NAME'] = $arDefaultMeasure['SYMBOL_RUS'];
		$arItem['~CATALOG_MEASURE_NAME'] = $arDefaultMeasure['~SYMBOL_RUS'];
		if (0 < $arItem['CATALOG_MEASURE'])
		{
			if (!isset($arMeasureMap[$arItem['CATALOG_MEASURE']]))
				$arMeasureMap[$arItem['CATALOG_MEASURE']] = array();
			$arMeasureMap[$arItem['CATALOG_MEASURE']][] = $intKey;
		}
	}
	$arResult["ITEMS"][$intKey] = $arItem;
	$arResult["ELEMENTS"][$intKey] = $arItem["ID"];
	if($bAllocateBySections) {
		$arResult["SECTIONS"][$arrFilter['SECTION_ID']][$intKey] = $arItem["ID"];
	}
	$arElementLink[$arItem['ID']] = &$arResult["ITEMS"][$intKey];
	$intKey++;
}