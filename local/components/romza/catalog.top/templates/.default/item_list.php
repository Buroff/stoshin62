<div class="goods_list grid_view<?if(isset($arSection) && $sectCounter == 0):?> active<?endif;?>"
	 <?if(isset($sectID)):?>
		 id="<?=$strContID?>_section_<?=$sectID?>"
	 <?endif;?>
	>
	<?
	$boolFirst = true;
	$arRowIDs = array();
	foreach ($arItems as $keyItem => $arItem) {
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
		$strMainID = $this->GetEditAreaId($arItem['ID']);

		$strTitle = (
		isset($arItem["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_TITLE"]) && '' != isset($arItem["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_TITLE"])
			? $arItem["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_TITLE"]
			: $arItem['NAME']
		);?>
		<div class="goods_item<? if ($arItem['SECOND_PICT']):?> with_second_pic<?endif;?>">
			<div class="goods_item__inner" id="<? echo $strMainID; ?>">
				<?if ($arItem['LABEL']) { ?>
					<div class="goods_item__labels" title="<?= $arItem['LABEL_VALUE']; ?>">
						<span class="label label-<?= $arItem['LABEL_CODE']; ?>"><?= $arItem['LABEL_VALUE']; ?></span>
					</div>
				<? } ?>
				<a href="<?=$arItem['DETAIL_PAGE_URL']; ?>"
				   class="goods_item__img_wrapper" >
					<img src="<?=$arItem['PREVIEW_PICTURE']['SRC'] ?>"
						 alt="<?= $strTitle; ?>" title="<?= $strTitle; ?>"
						 class="goods_item__img" >
					<? if ($arItem['SECOND_PICT']):?>
						<? $secondPicUrl = !empty($arItem['PREVIEW_PICTURE_SECOND']) ? $arItem['PREVIEW_PICTURE_SECOND']['SRC'] : $arItem['PREVIEW_PICTURE']['SRC'];?>
						<img src="<?=$secondPicUrl ?>"
							 alt="<?= $strTitle; ?>" title="<?= $strTitle; ?>"
							 class="goods_item__img goods_item__img_second" >
					<?endif;?>
				</a>
				<div class="goods_item__info">
					<div class="goods_item__title">
						<a href="<?= $arItem['DETAIL_PAGE_URL']; ?>" title="<?= $arItem['NAME']; ?>">
							<?= $arItem['NAME']; ?>
						</a>
					</div>
					<div class="goods_item__rating">
						<span class="rateit" data-rateit-value="4" data-rateit-step="1" data-rateit-resetable="false" data-rateit-starwidth="19" data-rateit-starheight="18"></span>
					</div>
					<div class="goods_item__price">
						<? if (!empty($arItem['MIN_PRICE'])) {
							if ('N' == $arParams['PRODUCT_DISPLAY_MODE'] && isset($arItem['OFFERS']) && !empty($arItem['OFFERS'])) {
								echo GetMessage( 'CT_BCT_TPL_MESS_PRICE_SIMPLE_MODE',
									array (
										'#PRICE#' => $arItem['MIN_PRICE']['PRINT_DISCOUNT_VALUE'],
									)
								);
							} else {
								echo $arItem['MIN_PRICE']['PRINT_DISCOUNT_VALUE'];
							}
							if ( false && 'Y' == $arParams['SHOW_OLD_PRICE'] && $arItem['MIN_PRICE']['DISCOUNT_VALUE'] < $arItem['MIN_PRICE']['VALUE']) {
								?> <span><? echo $arItem['MIN_PRICE']['PRINT_VALUE']; ?></span><?
							}
						}?>
					</div>
					<div class="goods_item__manage_wrapper">
						<div class="goods_item__manage">
							<a href="<?=$arItem['COMPARE_URL']?>" class="compare_link">
								<span class="icon icon_measuring7"></span>
								��������
							</a>
							<a href="" class="favourite_link">
								<span class="icon icon_new6"></span>
								� ���������
							</a>
						</div>
						<div class="goods_item__amount">
							<input class="js-touchspin" type="text" name="<?= $arParams["PRODUCT_QUANTITY_VARIABLE"]; ?>" value="<?= $arItem['CATALOG_MEASURE_RATIO']; ?>" title="">
						</div>
						<div class="goods_item__buy_button">
							<a href="<?=$arItem['ADD_URL']?>" class="btn btn-primary btn-buy"><?=$arParams['MESS_BTN_BUY']?></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	<? } // endforeach ?>
</div>