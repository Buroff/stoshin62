<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @var string $strElementEdit */
/** @var string $strElementDelete */
/** @var array $arElementDeleteParams */
/** @var array $arSkuTemplate */
if (method_exists($this, 'setFrameMode')) $this->setFrameMode(true); ?>
<?
global $APPLICATION;
if (!empty($arResult['ITEMS'])) {
	$strElementEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT");
	$strElementDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE");
	$arElementDeleteParams = array("CONFIRM" => GetMessage('CT_BCT_ELEMENT_DELETE_CONFIRM'));

	$strContID = 'slider_' . mt_rand(0, 1000000);
	?>
	<div class="js-slider_goods_list slider_goods_list goods_list_block">
		<div class="goods_list__head">
			<?if(strlen($arParams['PROPERTY_TITLE']) > 0 ):?>
				<h2>
					<?=$arParams['PROPERTY_TITLE'];?>
					<?if(isset($arResult['CNT'])):?>
						<a class="more_items_link" href="#"><?=$arResult['CNT']?></a>
					<?endif;?>
				</h2>
			<?endif;?>
			<div class="btn-group btn-group-arrows">
				<span class="btn btn-default btn-arrow flaticon-arrow133"></span>
				<span class="btn btn-default btn-arrow flaticon-right20"></span>
			</div>
		</div>
		<?if(isset($arResult['ITEMS_BY_SECTION'])):?>
			<ul class="goods_list__categories">
				<?$first = true; foreach ($arResult['ITEMS_BY_SECTION'] as $sectID => $arSection):?>
					<li class="goods_list__categories_item">
						<a href="javascript: void(0)"
							class="goods_list__categories_item__link<?if($first):?> active<?$first= false; endif;?>" data-target="<?=$strContID?>_section_<?=$sectID?>">
							<span class="icon <?=$arSection['ICON']?>"></span>
							<span class="inner"><?=$arSection['NAME']?></span>
						</a>
					</li>
				<?endforeach?>
			</ul>
		<?endif;?>
		<?if (isset($arResult['ITEMS_BY_SECTION'])) {
			$sectCounter = 0;
			foreach ($arResult['ITEMS_BY_SECTION'] as $sectID => $arSection) {
				$arItems = $arSection['ITEMS'];
				include(__DIR__ . '/item_list.php');
				++$sectCounter;
			}
		} else {
			$arItems = $arResult['ITEMS'];
			include(__DIR__ . '/item_list.php');
		}?>
	</div>
<?
}
?>