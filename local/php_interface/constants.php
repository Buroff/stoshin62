<?php
// для базы данных автомобилей
// define('DBHOST', 'localhost');
// define('DBPASSWORD', '');
// define('DBLOGIN', 'root');
// define('DBNAME', 'romza_up_2');


// новый каталог товаров
define('CATALOG_FINAL', 23);
define('CATALOG_FINAL_TYPE', 'final');

// ШИНЫ
define('SHINY', 52);
define('STALNYE', 50);
define('GRUZOVYE', 53);
define('LEGKOVYE', 59);
define('LEGKOGRUZOVYE', 76);
define('FOUR_FOUR', 58);
define('SHINY_TRASH', 73);
define('SHINY_TRASH_WINTER', 74);

// СВОЙСТВА ШИН
define('SHIRINA_PROFILA_ID', 194);
define('VISOTA_PROFILA_ID', 195);
define('DIAMETR_ID', 196);

define('SHIRINA_PROFILA_CODE', 'SHIRINA_PROFILA');
define('VISOTA_PROFILA_CODE', 'VISOTA_PROFILA');
define('DIAMETR_CODE', 'DIAMETR');


// ДИСКИ
define('DISKI', 49); // stalnye
define('STALNYE_DISKI', 50);
define('DISKI_TRASH', 72);

// СВОЙСТВА ДИСКОВ
define('SHIRINA_DISKA_ID', 197); // SHIRINA_DISKA_ID 197
define('SHIRINA_DISKA_CODE', 'SHIRINA_DISKA'); // SHIRINA_DISKA_CODE SHIRINA_DISKA
define('DIAMETR_OBODA_ID', 198); // DIAMETR_OBODA_ID 198
define('DIAMETR_OBODA_CODE', 'DIAMETR_OBODA'); // DIAMETR_OBODA_CODE DIAMETR_OBODA
define('PCD_ID', 199); // PCD_ID 199
define('PCD_CODE', 'DIAMETR_RASPOLOZHENIYA_OTVERSTIY_PCD'); // PCD_CODE DIAMETR_RASPOLOZHENIYA_OTVERSTIY_PCD
define('DIA_ID', 200); // DIA_ID 200
define('DIA_CODE', 'DIAMETR_TSENTRALNOGO_OTVERSTIYA'); // DIA_CODE DIAMETR_TSENTRALNOGO_OTVERSTIYA
define('VYLET_ET_ID', 201); // VYLET_ET_ID 201
define('VYLET_ET_CODE', 'VYLET_ET'); // VYLET_ET_CODE VYLET_ET




