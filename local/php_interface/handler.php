<?php

function OnSaleComponentOrderJsDataHandler(&$arResult, &$arParams) {
    $groupParamsId = 2; //ID группы свойств с параметрами доставки
    foreach ($arResult['JS_DATA']['ORDER_PROP']['properties'] as $key => $prop) {
        if ($prop['PROPS_GROUP_ID']==$groupParamsId) {
            $arResult['JS_DATA']['DELIVERY_PROPS']['properties'][] = $arResult['JS_DATA']['ORDER_PROP']['properties'][$key];
            unset($arResult['JS_DATA']['ORDER_PROP']['properties'][$key]);
        }
    }
    foreach ($arResult['JS_DATA']['ORDER_PROP']['groups'] as $key => $group) {
        if ($group['ID']==$groupParamsId) {
            $arResult['JS_DATA']['DELIVERY_PROPS']['groups'][] = $arResult['JS_DATA']['ORDER_PROP']['groups'][$key];
            unset($arResult['JS_DATA']['ORDER_PROP']['groups'][$key]);
        }
    }
}





