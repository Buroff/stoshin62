<?php

use Bitrix\Main;
use Bitrix\Main\Entity;
 
CModule::IncludeModule('iblock');
CModule::IncludeModule('highloadblock');
CModule::IncludeModule("sale");

if(file_exists($_SERVER["DOCUMENT_ROOT"]."/local/php_interface/constants.php")){
    require_once($_SERVER["DOCUMENT_ROOT"]."/local/php_interface/constants.php");
}


// класс по шинам (получению данных по модели авто)
if(file_exists($_SERVER["DOCUMENT_ROOT"]."/local/php_interface/libs/db_vehicle_sample.class.php")){
    require_once($_SERVER["DOCUMENT_ROOT"]."/local/php_interface/libs/db_vehicle_sample.class.php");
}



// функции по дискам
if(file_exists($_SERVER["DOCUMENT_ROOT"]."/local/php_interface/libs/disk_functions.php")){
    require_once($_SERVER["DOCUMENT_ROOT"]."/local/php_interface/libs/disk_functions.php");
}

// класс по дискам (получению данных по модели авто)
if(file_exists($_SERVER["DOCUMENT_ROOT"]."/local/php_interface/libs/disk_db_vehicle.class.php")){
  require_once($_SERVER["DOCUMENT_ROOT"]."/local/php_interface/libs/disk_db_vehicle.class.php");
}

// класс по получеию данных по модели авто
// if(file_exists($_SERVER["DOCUMENT_ROOT"]."/local/php_interface/reader_functions.php")){
//     require_once($_SERVER["DOCUMENT_ROOT"]."/local/php_interface/reader_functions.php");
// }

// класс по получеию данных по модели авто
// if(file_exists($_SERVER["DOCUMENT_ROOT"]."/local/php_interface/reader_tyres_functions.php")){
//     require_once($_SERVER["DOCUMENT_ROOT"]."/local/php_interface/reader_tyres_functions.php");
// }

// новые шины 
// класс по получеию данных по модели авто
if(file_exists($_SERVER["DOCUMENT_ROOT"]."/local/php_interface/libs/tyres/tyres_vehicle.class.php")){
 	require_once($_SERVER["DOCUMENT_ROOT"]."/local/php_interface/libs/tyres/tyres_vehicle.class.php");
}

// класс по получеию данных по модели авто
if(file_exists($_SERVER["DOCUMENT_ROOT"]."/local/php_interface/libs/tyres/tyres_functions.php")){
  	require_once($_SERVER["DOCUMENT_ROOT"]."/local/php_interface/libs/tyres/tyres_functions.php");
}




// --- обновление записей в инфоблоке
function updateIBlockElemenst(){

}

function getIBlockElements($PARAMS, $arSelect, $filterID = null, $arNavStartParams = null){
    CModule::IncludeModule("iblock");
      $arFilter = Array("IBLOCK_ID"=>$PARAMS['block_id'], "ACTIVE"=>"Y","SECTION_ID"=>$PARAMS['section_id']); 
      if($filterID != null){ $arFilter["><ID"] = $filterID;} 
      if($arNavStartParams == null){ $arNavStartParams = Array("nPageSize"=>100);}     

      $res = CIBlockElement::GetList(Array("ID"=>"ASC"), $arFilter, false, $arNavStartParams, $arSelect);   
      $array_line_full = array();

      while($ar = $res->GetNext()) {
            $array_line_full[]  = $ar;
      }

      return  $array_line_full;
}

// $PARAMS = array('section_id' => 894, 'block_id' => 1,);
// $arSelect = Array("ID", "NAME");
// $arrayDb = getCIBlockElementList($PARAMS, $arSelect, $filterID = null, $arNavStartParams = Array("nPageSize"=>800));


// --- вывод данных 
function pr($var, $type = false) {
    echo '<pre style="font-size:10px; border:1px solid #000; background:#FFF; text-align:left; color:#000;">';
    if ($type)
        var_dump($var);
    else
        print_r($var);
    echo '</pre>';
}

// событие обратотки заказа

function OnSaleComponentOrderJsDataHandler(&$arResult, &$arParams) {
    $groupParamsId = 5; //ID группы свойств с параметрами доставки
    foreach ($arResult['JS_DATA']['ORDER_PROP']['properties'] as $key => $prop) {
        if ($prop['PROPS_GROUP_ID']==$groupParamsId) {
            $arResult['JS_DATA']['DELIVERY_PROPS']['properties'][] = $arResult['JS_DATA']['ORDER_PROP']['properties'][$key];
            unset($arResult['JS_DATA']['ORDER_PROP']['properties'][$key]);
        }
    }
    foreach ($arResult['JS_DATA']['ORDER_PROP']['groups'] as $key => $group) {
        if ($group['ID']==$groupParamsId) {
            $arResult['JS_DATA']['DELIVERY_PROPS']['groups'][] = $arResult['JS_DATA']['ORDER_PROP']['groups'][$key];
            unset($arResult['JS_DATA']['ORDER_PROP']['groups'][$key]);
        }
    }

    if (isset($arResult['JS_DATA']['LAST_ORDER_DATA']['DELIVERY']) && $arResult['JS_DATA']['LAST_ORDER_DATA']['DELIVERY']!='') {
          $arResult['JS_DATA']['LAST_ORDER_DATA']['DELIVERY'] = '';
    }

}

// события
$eventManager = Main\EventManager::getInstance();
$eventManager->addEventHandler("sale", "OnSaleComponentOrderJsData", "OnSaleComponentOrderJsDataHandler");

