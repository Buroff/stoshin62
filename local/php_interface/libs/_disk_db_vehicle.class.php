<?php
use Bitrix\Main\Application;

	class DiskDBVehicleSample
	{

		public $models_by_vendor;
		public $connection;

		public function __construct(){
			$this->connection = Application::getConnection();
		}


		 

		function _get_data($vendor, $car, $year, $modification)
		{
			$sql = "SELECT * FROM search_by_vehicle 
					WHERE vendor = '" . $vendor . "' 
						AND car = '" . $car . "' 
						AND year = " . $year . " 
						AND modification = '" . $modification . "'";
			
			$result = $this->_query($sql);

			if (isset($result[0]))
			{
				$result[0]['tyres_factory_list'] 	= explode("|", $result[0]['tyres_factory']);
				$result[0]['tyres_tuning_list'] 	= explode("|", $result[0]['tyres_tuning']);
				$result[0]['wheels_factory_list'] 	= explode("|", $result[0]['wheels_factory']);
				$result[0]['wheels_replace_list'] 	= explode("|", $result[0]['wheels_replace']);
				$result[0]['wheels_tuning_list'] 	= explode("|", $result[0]['wheels_tuning']);
				$result[0]['tyres_replace_list'] 	= explode("|", $result[0]['tyres_replace']);


			}

			return $result[0];
		}


// --- BASE

		function get_vendors()
		{
			$sql = "SELECT vendor FROM search_by_vehicle GROUP BY vendor";
			return $this->_query($sql);
		}

		function get_data($vendor, $car, $year, $modification)
		{
			$sql = "SELECT * FROM search_by_vehicle 
					WHERE vendor = '" . $vendor . "' 
						AND car = '" . $car . "' 
						AND year = " . $year . " 
						AND modification = '" . $modification . "'";
			
			$result = $this->_query($sql);

			if (isset($result[0]))
			{
				$result[0]['tyres_factory_list'] 	= explode("|", $result[0]['tyres_factory']);
				$result[0]['tyres_replace_list'] 	= explode("|", $result[0]['tyres_replace']);
				$result[0]['tyres_tuning_list'] 	= explode("|", $result[0]['tyres_tuning']);

				$result[0]['wheels_factory_list'] 	= explode("|", $result[0]['wheels_factory']);
				$result[0]['wheels_replace_list'] 	= explode("|", $result[0]['wheels_replace']);
				$result[0]['wheels_tuning_list'] 	= explode("|", $result[0]['wheels_tuning']);
			}

			return $result[0];
		}


		function _query($query){
			$recordset = $this->connection->query($query);
			$rows = array();
			while ($record = $recordset->fetch())
			{
			  $rows[] = $record;
			}

			return $rows;
		}

		function get_models_by_vendor($vendor, $order = "ORDER by car"){
			// echo $sql = "SELECT * FROM search_by_vehicle WHERE vendor = '" . $vendor . "' ".$order;
			$sql = "SELECT * FROM search_by_vehicle WHERE vendor = '" . $vendor . "' ".$order;
			return $this->_query($sql);
		}

		function get_years_by_model($model)
		{
			$sql = "SELECT year FROM search_by_vehicle 
					WHERE car = '" . $model . "' GROUP BY year";
			return $this->_query($sql);
		}

		function get_modifications_by_model($model)
		{
			$sql = "SELECT modification FROM search_by_vehicle 
					WHERE car = '" . $model . "' GROUP BY modification";
			return $this->_query($sql);
		}







    // --- DEFAULT

        // список моделей
        function get_cars($vendor){
            $sql = "SELECT car FROM search_by_vehicle WHERE vendor='" . $vendor . "' GROUP BY car";
            return $this->_query($sql);
        }

        // список годов по модели
        function get_years($vendor, $car = null, $year = null){

        	// var_dump($car);

            if($car){ $order = " AND car = '".$car."' GROUP BY year";
            }else{ $order = " GROUP BY year";}

            $sql = "SELECT year FROM search_by_vehicle WHERE vendor = '".$vendor."' ".$order;
            $data = [];
            $years = $this->_query($sql);

            $selectedYear = false;

            foreach ($years as $key => $value){
	            $data[$key] = $value['year'];
	            if($year){
	            	if($value['year'] == $year){
	            		$selectedYear = $value['year'];
	            		unset($data[$key]);
	            	}
	            		
	            }
            }
            if($selectedYear){ 
            	array_unshift($data, $selectedYear); 
            }

            return $data;
            //return  $years;
        }


        function get_default_models_by_vendor($vendor, $order = "ORDER by car"){
			$sql = "SELECT car FROM search_by_vehicle WHERE vendor = '" . $vendor ."' GROUP BY car ";
			// return $this->_query($sql);

			$models = $this->_query($sql);
			$data = [];

			foreach ($models as $key => $value){
	            $data[$key] = $value['car'];
            }

            return $data;
		}


        // список модификаций
        function get_modifications($vendor = null, $car = null, $year = null, $modification = null){

            if($year){
                $order = " AND year = " . $year . " GROUP BY modification";}
            else{ $order = " GROUP BY modification";}

            $sql = "SELECT modification FROM search_by_vehicle  WHERE vendor = '" . $vendor . "' AND car = '" . $car . "' ".$order;

            $selectedModification = false;
			$data = [];
			$modifications = $this->_query($sql);

            foreach ($modifications as $key => $value){
	            $data[$key] = $value['modification'];
	            if($modification){
	            		if($value['modification'] == $modification){
	            			$selectedModification = $value['modification'];
	            			unset($data[$key]);
	            		}
	            		
	            }
            }
            if($selectedModification){ 
            	array_unshift($data, $selectedModification); 
            }
            return $data;
        }


        // список моделей по умолчанию // !!!
        function get_models_default($vendor, $modelName = null, $year = null, $modification = null){

            // получаем модели по производителю для поля "Модель"
            $models =  $this->get_models_by_vendor($vendor);
            $data = null;

            if(!$modelName){
                $modelName = $models[0]['car'];
            }

            foreach ($models as $key => $model) {
                    if($model['car'] == $modelName){
                        $data = array(
                            'VENDOR'=>$vendor,
                            'NAME'=>$model['car'],
                            'YEARS'=>$this->get_years_by_model($model['car']), // получаем данные по моделям для поля "Год"
                            'MODIFICATIONS' => $this->get_modifications_by_model($model['car']), // получаем данные по моделям для поля "Модификация"
                            'WHEELS_FACTORY' =>$model['wheels_factory'],
                            'DIAMETR_RASPOLOZHENIYA_OTVERSTIY_PCD'=>$this->parse_pcd($model['param_pcd']),  // PCD
                            'DIAMETR_TSENTRALNOGO_OTVERSTIYA'=>$this->parse_dia($model['param_dia'])  // DIA
                        );
                        unset($models[$key]);
                    }else{
                        $models[$key] = array('NAME'=>$model['car']);

                    }
            }

            array_unshift($models, $data);

            //  var_dump($models);
            //  die();

            return $models;
        }


		// параметры данных для вывода в фильтр по умолчанию
		function _get_default_data(){
			
			$arr = $this->get_vendors(); // получаем список производителей
			$vendors = [];
		    foreach ($arr as $key => $value) {
		          $vendors[] = $value['vendor'];
		    }	

		    $models = $this->get_models_default($vendors[0]);
            $names = $this->get_cars($vendors[0]);
            $years = $this->get_years($vendors[0], $models[0]['NAME']);
            $modifications = $this->get_modifications($vendors[0], $models[0]['NAME'], $years[0]);

            //var_dump($modifications);

            $result = array(
                'MODELS' => $models,
                'VENDORS' =>$vendors,
                'NAMES' =>$names,
                'YEARS'=>$years,
                'MODIFICATIONS'=>$modifications
            );

			return $result;
		}


		function get_default_data(){

			$arr = $this->get_vendors(); // получаем список производителей
			$vendors = [];
		    foreach ($arr as $key => $value) {
		          $vendors[] = $value['vendor'];
		    }	

		    $this->models_by_vendor =  $this->get_models_by_vendor($vendors[0], $order = 'ORDER BY id ASC');
		    $models = $this->models_by_vendor;
		    //$models = $this->get_models_default($vendors[0], );
			$first = $models[0]; // первая модель
			$years = $this->get_years($vendors[0], $first['car']);
		    $modifications = $this->get_modifications($vendors[0], $first['car'], $first['year']);
		    $modelNames = $this->get_default_models_by_vendor($vendors[0]);

		    $models = $this->select_by_year_modef($first['vendor'], $first['car'], $first['year'], $modifications[0]);
	
		    // параметры для подсчета товаров в каталоге по выбранному сочетанию (модели автомобиля)
			$vylet_et = $models[0]['WHEELS_FACTORY']['VYLET_ET'][0];
			$shirina_diska = $models[0]['WHEELS_FACTORY']['SHIRINA_DISKA'][0];
			$diametr_oboda = $models[0]['WHEELS_FACTORY']['DIAMETR_OBODA'][0];
			$pcd = $models[0]['DIAMETR_RASPOLOZHENIYA_OTVERSTIY_PCD'];
			$dia = $models[0]['DIAMETR_TSENTRALNOGO_OTVERSTIYA'];


			// количестов товаров по данному сочетанию параметров 
			$result = checkDisksByModel($vylet_et, $shirina_diska, $diametr_oboda, $pcd, $dia);
			$models[0]['ROWS'] = $result['ROWS']; 	
			$models[0]['VALID'] = $result['VALID'];
			$models[0]['CHECK_DISK_BY_MODEL'] = array(
				'vylet_et'=>$vylet_et,
				'shirina_diska'=>$shirina_diska,
				'diametr_oboda'=>$diametr_oboda,
				'pcd'=>$pcd,
				'dia'=>$dia
			);


			// замена дисков 
			$wheels_replace = $this->wheels_replace($models[0]['WHEELS_REPLACE'], $pcd, $dia);

		    // получаем подробные данные по модели
			return $result = array(
				'MODEL' => $models[0], // 1-я модель автомобиля, из нее берем параметры в поиск по шинам и дискам
				'VENDORS' =>$vendors,
				'MODELS'=>$modelNames,
				'YEARS'=>$years,
                'MODIFICATIONS'=>$modifications,
                'WHEELS_REPLACE'=>$wheels_replace // здесь ссылки по сочетаниям замены
			);

		}



		// --- AJAX 

		// параметры данных для вывода в фильтр данных списка после AJAX
		function get_ajax_data($vendor, $modelName = null, $year = null, $modification = null){

			// получаем все модели по производителю и сохраняем их в поле
			if(!empty($year) || !empty($modification)){
				$this->models_by_vendor =  $this->get_models_by_vendor($vendor, $order = 'ORDER BY id'); 
			}else{
				$this->models_by_vendor =  $this->get_models_by_vendor($vendor, $order = 'ORDER BY id ASC');
			}
			$models = $this->models_by_vendor;
			$first = $models[0]; // первая модель
			// pr($first);
			// exit();

			// получаем список производителей и ставим выбранного первым (selected)
			$arr = $this->get_vendors($vendor);
			$vendors = [];
		    foreach ($arr as $key => $value) {
		          $vendors[] = $value['vendor'];
		    }
		    if($vendor){
		    	$selectedVendor = null;
		 		foreach ($vendors as $key => $item) {
		    		if($item==$vendor){
		    			$selectedVendor = $item;
		    			unset($vendors[$key]);
		    		}
		    	}
		    	if($selectedVendor){
					array_unshift($vendors, $selectedVendor);
				}
		    }
		    // фильтр по параметрам год если есть имя модели
		    if(!empty($year)){
				$years = $this->get_years($vendor, $modelName, $year);
			}else{
				$years = $this->get_years($vendor, $modelName);
			}
			// pr($year);
			// exit();

		    // фильтр по параметрам модель/год/модификация
		    if(!empty($modelName)){

			    if($year && $modification){
			    	$models = $this->select_by_year_modef($vendor, $modelName, $year, $modification);
	                $modifications = $this->get_modifications($vendor, $modelName, $year, $modification);
			    }
			    elseif($year){
			    	$models = $this->select_by_year_modef($vendor, $modelName, $year);
	                $modifications = $this->get_modifications($vendor, $modelName, $year);
			    }
			    else{
			    	$models = $this->select_by_model($vendors[0], $modelName, $years[0]);
	                $modifications = $this->get_modifications($vendor, $modelName);
			    }
		
		    }else{
		    	$years = $this->get_years($vendor, $first['car']);
		    	$modifications = $this->get_modifications($vendor, $first['car'], $first['year']);
		    	// $models = $this->select_by_model($vendors[0], $first['car'], $years[0]);
		    	$models = $this->select_by_year_modef($first['vendor'], $first['car'], $first['year'], $modification['modification']); // !!!
		    }

			$modelNames = $this->get_model_names($models);

			// параметры для подсчета товаров в каталоге по выбранному сочетанию (модели автомобиля)
			$vylet_et = $models[0]['WHEELS_FACTORY']['VYLET_ET'][0];
			$shirina_diska = $models[0]['WHEELS_FACTORY']['SHIRINA_DISKA'][0];
			$diametr_oboda = $models[0]['WHEELS_FACTORY']['DIAMETR_OBODA'][0];
			$pcd = $models[0]['DIAMETR_RASPOLOZHENIYA_OTVERSTIY_PCD'];
			$dia = $models[0]['DIAMETR_TSENTRALNOGO_OTVERSTIYA'];

			// количестов товаров по данному сочетанию параметров 
			$result = checkDisksByModel($vylet_et, $shirina_diska, $diametr_oboda, $pcd, $dia);
			$models[0]['ROWS'] = $result['ROWS']; 	
			$models[0]['VALID'] = $result['VALID'];
			$models[0]['CHECK_DISK_BY_MODEL'] = array(
				'vylet_et'=>$vylet_et,
				'shirina_diska'=>$shirina_diska,
				'diametr_oboda'=>$diametr_oboda,
				'pcd'=>$pcd,
				'dia'=>$dia
			);
			// if(empty($rows)){
			//  	// $models[0]['URL'] = 
			// }
			// var_dump($models[0]['WHEELS_REPLACE']);

			// замена дисков 
			$wheels_replace = $this->wheels_replace($models[0]['WHEELS_REPLACE'], $pcd, $dia);
	  
		    // получаем подробные данные по модели
			return $result = array(
				'MODEL' => $models[0], // 1-я модель автомобиля, из нее берем параметры в поиск по шинам и дискам
				'VENDORS' =>$vendors,
				'MODELS'=>$modelNames,
				'YEARS'=>$years,
                'MODIFICATIONS'=>$modifications,
                'WHEELS_REPLACE'=>$wheels_replace // здесь ссылки по сочетаниям замены
			);
	
		}

		// замена
		function wheels_replace($wheels_replace, $pcd, $dia){

				$replace_data =  parseWheelsReplace($wheels_replace); // парсим
				// var_dump($replace_data);
				$links = replaceLinks($replace_data, $pcd, $dia); // генерим ссылки
				if(!empty($links)){
					return $links;
				}
				return false;
		}

		// список моделей
		function select_by_model($vendor, $modelName = null, $year = null, $modification = null){
			// сохраненный список моделей по производителю
			$models = $this->models_by_vendor;
			$result = [];
			$data = null;

			foreach ($models as $key => $model) {

		    	if($model['car'] == $modelName && $model['year'] == $year){
		    		$data = array(
								'NAME'=>$model['car'],
								'SELECTED_CAR_ID'=>$model['id'],
								'NAME'=>$model['car'],
								'YEAR'=>$model['year'],
                                'MODIFICATION'=>$model['modification'],
                                'DIAMETR_RASPOLOZHENIYA_OTVERSTIY_PCD'=>$this->parse_pcd($model['param_pcd']),  // PCD
                                'DIAMETR_TSENTRALNOGO_OTVERSTIYA'=>$this->parse_dia($model['param_dia']), // DIA
                                'WHEELS_FACTORY' =>$this->parse_weel_factory($model['wheels_factory']),
                                'WHEELS_REPLACE' =>$model['wheels_replace']
					);
		    		unset($models[$key]);
		    	}else{
		    		$models[$key] = array('NAME'=>$model['car']);

		    	}
		    }

		    array_unshift($models, $data);
			return $models;
		}

		function select_by_year_modef($vendor, $modelName, $year, $modification = null){

			$models =  $this->models_by_vendor; // сохраненный список моделей по производителю
			$data = null;
		

			if($modification){
				foreach ($models as $key => $model) {

					$modelNames[$model['car']] = $model['car'];

					if($model['year'] == $year && $model['car'] == $modelName && $model['modification'] == $modification){
						
							$data = array(
								'SELECTED_CAR_ID'=>$model['id'],
								'NAME'=>$model['car'],
								'YEAR'=>$model['year'],
                                'MODIFICATION'=>$model['modification'],
                                'DIAMETR_RASPOLOZHENIYA_OTVERSTIY_PCD'=>$this->parse_pcd($model['param_pcd']),  // PCD
                                'DIAMETR_TSENTRALNOGO_OTVERSTIYA'=>$this->parse_dia($model['param_dia']), // DIA
                                'WHEELS_FACTORY' =>$this->parse_weel_factory($model['wheels_factory']),
                                'WHEELS_REPLACE' =>$model['wheels_replace']
							);
							unset($models[$key]);
					
					}else{
						$models[$key] = array('NAME'=>$model['car']);
					}
				}
			}else{
				$step = 0;
				foreach ($models as $key => $model) {
					if($model['year'] == $year && $model['car'] == $modelName && $step == 0){
						
							$data = array(
								'SELECTED_CAR_ID'=>$model['id'],
								'NAME'=>$model['car'],
								'YEAR'=>$model['year'],
                                'MODIFICATION'=>$model['modification'],
                                'DIAMETR_RASPOLOZHENIYA_OTVERSTIY_PCD'=>$this->parse_pcd($model['param_pcd']),  // PCD
                                'DIAMETR_TSENTRALNOGO_OTVERSTIYA'=>$this->parse_dia($model['param_dia']), // DIA
                                'WHEELS_FACTORY' =>$this->parse_weel_factory($model['wheels_factory']),
                                'WHEELS_REPLACE' =>$model['wheels_replace']
							);
							unset($models[$key]);
							$step++;
					}else{
						$models[$key] = array('NAME'=>$model['car']);
					}
				}
			}
			
			array_unshift($models, $data);
			return $models;
		}

		
			// !!!
        // парсинг данных по шинам полученные от модели автомобиля
        function parse_weel_factory($disk_data){

            $data = array(
                'VYLET_ET'=>[],
                'SHIRINA_DISKA'=>[],
                'DIAMETR_OBODA'=>[]
            );

           

            $needle = stripos($disk_data,"|");
            if($needle){
                
                $disk_data = explode("|", $disk_data);

                // foreach ($disk_data as $key => $item){
                // 	var_dump($item);
                //     $var  = explode(' ', $item);   
                //     $et_data = explode('ET',$var[3]); 
                //     $data['VYLET_ET'][] = $et_data[1];
                //     $data['SHIRINA_DISKA'][] = $var[0];
                //     $data['DIAMETR_OBODA'][] = $var[2];
                // }

                $var  = explode(' ', $disk_data[0]);   
                $et_data = explode('ET',$var[3]); 
                $data['VYLET_ET'][] = $et_data[1];
                $data['SHIRINA_DISKA'][] = $var[0];
                $data['DIAMETR_OBODA'][] = $var[2];
               

            }else{
                $var  = explode(' ', $disk_data); 
                $et_data = explode('ET',$var[3]); 
                $data['VYLET_ET'][] = $et_data[1];
                $data['SHIRINA_DISKA'][] = $var[0];
                $data['DIAMETR_OBODA'][] = $var[2];
            }

            return $data;

        }


        function get_model_names($models){
			$modelNames = array();
			
			foreach ($models as $key => $model) {
				if(!(isset($modelNames[$model['NAME']]))){
					$modelNames[$model['NAME']] = $model['NAME'];
				}
			}

			$data = [];
			foreach ($modelNames as $key => $value) {
					$data[] = $value;
			}
			return $data;
		}

		// получение данных от модели автомобиля
		function get_data_by_model($vendor, $car, $year, $modification){
			$sql = "SELECT * FROM search_by_vehicle 
					WHERE vendor = '" . $vendor . "' 
						AND car = '" . $car . "' 
						AND year = " . $year . " 
						AND modification = '" . $modification . "'";
			
			$result = $this->_query($sql);

			if (isset($result[0]))
			{
				$result[0]['wheels_factory_list'] 	= explode("|", $result[0]['wheels_factory']);
				$result[0]['wheels_replace_list'] 	= explode("|", $result[0]['wheels_replace']);
				$result[0]['wheels_tuning_list'] 	= explode("|", $result[0]['wheels_tuning']);
			}

			return $result[0];
		}


		function parse_pcd($param_pcd){
			$param_pcd_ = str_replace('*', 'х', $param_pcd);
			return $result = str_replace(".",",", $param_pcd_);
		}


		function parse_dia($param_dia){
			return $param_dia.' мм';
		} 

	

	}

// --- функции для работы классом DBVehicleSample (таблицы БД по автомобилям)

function getDiskDefaultData(){ // первый производитель по нему устававливаются остальные поля
    $db = new DiskDBVehicleSample();
    return $db->get_default_data();
}

function getTestDiskDefaultData(){ // первый производитель по нему устававливаются остальные поля
    $db = new DiskDBVehicleSample();
    return $db->test_get_default_data();
}

function getDiskVendors(){ // список всех производителей для поля "Марка"
    $db = new DiskDBVehicleSample();
    $arr = $db->get_vendors();
    $vendors = [];
    foreach ($arr as $key => $value) {
          $vendors[] = $value['vendor'];
    }
    return  $vendors;
}


function getDiskModifications($vendor, $car, $year = null, $modification = null){ // список модификаций
    //var_dump($modification);

    $db = new DiskDBVehicleSample();
    return $db->get_modifications($vendor, $car, $year, $modification);
}

// !!!
function parseDiskData($disk_data){
	// var_dump($disk_data);
    $db = new DiskDBVehicleSample();
    return $db->parse_weel_factory($disk_data); 
}

function getDiskAjaxData($strVendor = null, $strModel = null, $strYear = null, $strModif = null){ // получаеам Ajax массив по Марка/Модель/Год/Модификация
    // echo '-- getAjaxData -- <br>';
    $db = new DiskDBVehicleSample();
    return $db->get_ajax_data($strVendor, $strModel, $strYear, $strModif);
}


function getDiskYears($strVendor = null, $strModel = null){ // получаеам Ajax массив по Марка/Модель/Год/Модификация
    // echo '-- getAjaxData -- <br>';
    $db = new DiskDBVehicleSample();
    return $db->get_years($strVendor, $strModel, $strYear, $strModif);
}


function _getDiskModifications($strVendor, $strModel, $strYear = null, $strModif = null){ 
    // echo '-- getDiskModifications -- <br>';
    // var_dump($strVendor);
    $db = new DiskDBVehicleSample();
    return $db->get_modifications($strVendor, $strModel, $strYear, $strModif);
}

function testData(){
	// echo '--- testData ---';
}

// ---  ФИЛЬТР ПО МОДЕЛИ

// проверка на наличие товара в магазине по сочетанию параметров
function checkDisksByModel($vylet_et = null, $shirina_diska = null, $diametr_oboda  = null, $pcd = null, $dia = null){

	//echo '--- checkDisksByModel --- <br/>';
	// var_dump($vylet_et);
	// var_dump($shirina_diska);
	// var_dump($diametr_oboda);
	// var_dump($pcd);
	// var_dump($dia);

	CModule::IncludeModule('iblock');
	$arSelect = Array("ID","NAME", "PROPERTY_*");

	$arFilter = Array(
    	"IBLOCK_ID"=>23, 
    	"ACTIVE"=>"Y", 
    	"SECTION_ID"=>array(49,50,51,72,82)
    );

    $valid = array();

	if($vylet_et){
		$arFilter['PROPERTY_VYLET_ET_VALUE'] = $vylet_et.' мм';
		$valid['VYLET_ET'] = $vylet_et.' мм';
	}
	if($shirina_diska){
		if(!strpos($shirina_diska, ' "')){
			$shirina_diska = $shirina_diska.' "';
		}

		$arFilter['PROPERTY_SHIRINA_DISKA_VALUE'] = $shirina_diska;
		$valid['SHIRINA_DISKA'] = $shirina_diska;
	}
	if($diametr_oboda){
		if(!strpos($diametr_oboda, ' "')){
			$diametr_oboda = $diametr_oboda.' "';
		}

		$arFilter['PROPERTY_DIAMETR_OBODA_VALUE'] = $diametr_oboda;
		$valid['DIAMETR_OBODA'] = $diametr_oboda;
	}

	if($pcd){
		if(!strpos($pcd, ',')){
			$pcd = str_replace(".",",", $pcd);
		}

		if(!strpos($pcd, 'х')){
			$pcd = str_replace("*","х", $pcd);
		}

		$en_pcd = str_replace("*","x", $pcd);
		$arFilter['PROPERTY_DIAMETR_RASPOLOZHENIYA_OTVERSTIY_PCD_VALUE'] = $pcd;
		$valid['PCD'] = $pcd;

	}

	if($dia){
		if(!strpos($dia, ' мм')){
			$dia = $dia.' мм';
		}
		$arFilter['PROPERTY_DIAMETR_TSENTRALNOGO_OTVERSTIYA_VALUE'] = $dia;
		$valid['DIA'] = $dia;
	}

	// var_dump($arFilter);
	// $arNavStartParams = Array("nPageSize"=>100);
    $res = CIBlockElement::GetList(Array("ID"=>"ASC"), $arFilter, false, false, $arSelect);   
	$result['ROWS'] = intval($res->SelectedRowsCount());
	
	// проверка на символ x (он может быть в английской раскладке)
	if($result['ROWS']<1){

		$arFilter['PROPERTY_DIAMETR_RASPOLOZHENIYA_OTVERSTIY_PCD_VALUE'] = $en_pcd;
		$valid['PCD'] = $en_pcd;

		$res = CIBlockElement::GetList(Array("ID"=>"ASC"), $arFilter, false, false, $arSelect); 
		$result['ROWS'] = intval($res->SelectedRowsCount());
		
	}
	$result['VALID'] = $valid;
	return $result;
}

