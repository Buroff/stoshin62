<?php
use Bitrix\Main\Application;


// ---  ФИЛЬТР ПО МОДЕЛИ

// поиск товаров по параметерам
// function _checkDisksByModel($vylet_et = null, $shirina_diska = null, $diametr_oboda  = null, $pcd = null, $dia = null){

// 	CModule::IncludeModule('iblock');
// 	$arSelect = Array("ID","NAME", "PROPERTY_SHIRINA_PROFILA");

// 	$arFilter = Array(
//     	"IBLOCK_ID"=>23, 
//     	"ACTIVE"=>"Y", 
//     	"SECTION_ID"=>array(49,50,51,72,82),
//     	'PROPERTY_VYLET_ET_VALUE'=>array('60 мм'), // VYLET_ET
//     	'PROPERTY_SHIRINA_DISKA_VALUE'=>array('7 "'),
//     	'PROPERTY_DIAMETR_OBODA_VALUE'=>array('16 "'),
//     	'PROPERTY_DIAMETR_RASPOLOZHENIYA_OTVERSTIY_PCD_VALUE'=>array('4х114,3'), // PCD
//     	'PROPERTY_DIAMETR_TSENTRALNOGO_OTVERSTIYA_VALUE'=>array('67.1 мм')
//     ); 
//     $arNavStartParams = Array("nPageSize"=>100);
//     $res = CIBlockElement::GetList(Array("ID"=>"ASC"), $arFilter, false, false, $arSelect);   
//     // $array_line_full = array();
//  	//  while($ar = $res->GetNext()) {
// 	//   	$array_line_full[]  = $ar;
// 	// }

//     return intval($res->SelectedRowsCount());
// }

// проверка на наличие товара в магазине по сочетанию параметров
function checkDisksByModel($vylet_et = null, $shirina_diska = null, $diametr_oboda  = null, $pcd = null, $dia = null){

	//echo '--- checkDisksByModel --- <br/>';
	// var_dump($vylet_et);
	// var_dump($shirina_diska);
	// var_dump($diametr_oboda);
	// var_dump($pcd);
	// var_dump($dia);

	CModule::IncludeModule('iblock');
	$arSelect = Array("ID","NAME", "PROPERTY_*");

	$arFilter = Array(
    	"IBLOCK_ID"=>23, 
    	"ACTIVE"=>"Y", 
    	"SECTION_ID"=>array(49,50,51,72,82)
    );

    $valid = array();

	if($vylet_et){
		$arFilter['PROPERTY_VYLET_ET_VALUE'] = $vylet_et.' мм';
		$valid['VYLET_ET'] = $vylet_et.' мм';
	}
	if($shirina_diska){
		if(!strpos($shirina_diska, ' "')){
			$shirina_diska = $shirina_diska.' "';
		}

		$arFilter['PROPERTY_SHIRINA_DISKA_VALUE'] = $shirina_diska;
		$valid['SHIRINA_DISKA'] = $shirina_diska;
	}
	if($diametr_oboda){
		if(!strpos($diametr_oboda, ' "')){
			$diametr_oboda = $diametr_oboda.' "';
		}

		$arFilter['PROPERTY_DIAMETR_OBODA_VALUE'] = $diametr_oboda;
		$valid['DIAMETR_OBODA'] = $diametr_oboda;
	}

	if($pcd){
		if(!strpos($pcd, ',')){
			$pcd = str_replace(".",",", $pcd);
		}

		if(!strpos($pcd, 'х')){
			$pcd = str_replace("*","х", $pcd);
		}

		$en_pcd = str_replace("*","x", $pcd);
		$arFilter['PROPERTY_DIAMETR_RASPOLOZHENIYA_OTVERSTIY_PCD_VALUE'] = $pcd;
		$valid['PCD'] = $pcd;

	}

	if($dia){
		if(!strpos($dia, ' мм')){
			$dia = $dia.' мм';
		}
		$arFilter['PROPERTY_DIAMETR_TSENTRALNOGO_OTVERSTIYA_VALUE'] = $dia;
		$valid['DIA'] = $dia;
	}

	// var_dump($arFilter);
	// $arNavStartParams = Array("nPageSize"=>100);
    $res = CIBlockElement::GetList(Array("ID"=>"ASC"), $arFilter, false, false, $arSelect);   
	$result['ROWS'] = intval($res->SelectedRowsCount());
	
	// проверка на символ x (он может быть в английской раскладке)
	if($result['ROWS']<1){

		$arFilter['PROPERTY_DIAMETR_RASPOLOZHENIYA_OTVERSTIY_PCD_VALUE'] = $en_pcd;
		$valid['PCD'] = $en_pcd;

		$res = CIBlockElement::GetList(Array("ID"=>"ASC"), $arFilter, false, false, $arSelect); 
		$result['ROWS'] = intval($res->SelectedRowsCount());
		
	}
	$result['VALID'] = $valid;
	return $result;
}




// добавление значения свойства
function setPropertyValue($prop_id, $prop_value){

	$PROPERTY_ID = false;

	switch (intval($prop_id)) {
		case PCD_ID:
			$PROPERTY_ID = PCD_ID;
		break;

		case DIA_ID:
			$PROPERTY_ID = DIA_ID;	
		break;

		case VYLET_ET_ID:
			$PROPERTY_ID = VYLET_ET_ID;	
		break;

		case SHIRINA_DISKA_ID:
			$PROPERTY_ID = SHIRINA_DISKA_ID;
		break;

		case DIAMETR_OBODA_ID:
			$PROPERTY_ID = DIAMETR_OBODA_ID;	
		break;
	}

	if($PROPERTY_ID){
		CModule::IncludeModule('iblock');
		$ibpenum = new CIBlockPropertyEnum;
		if($PropID = $ibpenum->Add(Array('PROPERTY_ID'=>$PROPERTY_ID, 'VALUE'=>$prop_value))){
			return $PropID;
		}
	}
	return false;
}
// удаление значение свойства 
// CIBlockPropertyEnum::Delete(1181);


// генерация ссылки для GET запроса в каталог дисков
function makeFilterParam($key_prop_value, $prop_id, $prop_value = false){
	  $safeFilterName = htmlspecialcharsbx('arrFilter');
	  $filterPropertyID = $safeFilterName.'_'.$prop_id;
	  $htmlKey = htmlspecialcharsbx($key_prop_value); // ?
	  $keyCrc = abs(crc32($htmlKey));
	  return $filterPropertyIDKey = $filterPropertyID.'_'.$keyCrc;
}
// $result = makeFilterParam($key_prop_value = 717, $prop_id = 197, $prop_value = false);
// var_dump($result); 
// arrFilter_197_1601861446


// ключь значения свойства инфоблока
function getKeyPropertyValue($prop_code, $prop_value){
	CModule::IncludeModule('iblock');
		$property_enums = CIBlockPropertyEnum::GetList(
		  Array("DEF"=>"DESC", "SORT"=>"ASC"), 
		  Array("IBLOCK_ID"=>CATALOG_FINAL, "CODE"=>$prop_code, "VALUE"=>$prop_value)
		);

		$result = false;
		while($enum_fields = $property_enums->GetNext()){
		    $param = array();
		    $param['KEY'] = $enum_fields["ID"];
		    $param['VALUE'] = $enum_fields["VALUE"];
		    $result = $param;
		    // $result = $enum_fields["ID"];
		}

		if(!empty($result)){
			return $result;
		}

		return false;
}

// $result = getKeyPropertyValue(PCD_CODE, "4х114,3");
// var_dump($result); 





// ---  ЗАМЕНА
function parseWheelsReplace($weels_replace){

	// echo '--- parseWheelsReplace ---';

            $needle = stripos($weels_replace,"|");
            $replace_data = array();
            if($needle){
               
                $disk_data = explode("|", $weels_replace);

                foreach ($disk_data as $key => $item){
                    $var  = explode(' ', $item);   
                    $et_data = explode('ET',$var[3]); 
                    $replace = [];
                    $replace['VYLET_ET'] = $et_data[1];
                    $replace['SHIRINA_DISKA'] = $var[0];
                    $replace['DIAMETR_OBODA']  = $var[2];
                    $replace['NAME'] = $item;

                    $replace_data[] = $replace;
                }            

            }else{

            	$var  = explode(' ', $weels_replace); 
                $et_data = explode('ET',$var[3]); 

                $replace = [];
                $replace['VYLET_ET'] = $et_data[1];
                $replace['SHIRINA_DISKA'] = $var[0];
                $replace['DIAMETR_OBODA'] = $var[2];
               	$replace['NAME'] = $weels_replace;
                $replace_data[] = $replace;
            }

    return $replace_data;
}


function replaceLinks($replace_data, $pcd, $dia){

	// echo '-- replaceLinks --'.'<br />';

    $replaceLinks = array();
    
    foreach ($replace_data as $key => $item) {

	    $result = checkDisksByModel($item['VYLET_ET'],$item['SHIRINA_DISKA'],$item['DIAMETR_OBODA'], $pcd, $dia);

	    if(!empty($result['ROWS'])){
	    	// var_dump($result['ROWS']);   	

		    // гененируем ключи для ссылку в URL
	        $vilet_et_key =  getKeyPropertyValue(VYLET_ET_CODE, $result['VALID']['VYLET_ET']);
	        $shirina_diska_key = getKeyPropertyValue(SHIRINA_DISKA_CODE, $result['VALID']['SHIRINA_DISKA']);
	        $diametr_oboda_key = getKeyPropertyValue(DIAMETR_OBODA_CODE, $result['VALID']['DIAMETR_OBODA']);
	        $pcd_key = getKeyPropertyValue(PCD_CODE,  $result['VALID']['PCD']);
	        $dia_key = getKeyPropertyValue(DIA_CODE, $result['VALID']['DIA']);

	        if(!empty($shirina_diska_key['KEY'])){
			    $shirina_diska_fp = makeFilterParam($shirina_diska_key['KEY'], SHIRINA_DISKA_ID);
			    $FilterParam['SHIRINA_DISKA'] = $shirina_diska_fp;
			}else{
			    // 

			}


			if(!empty($diametr_oboda_key['KEY'])){
			    $diametr_oboda_fp = makeFilterParam($diametr_oboda_key['KEY'], DIAMETR_OBODA_ID);
			    $FilterParam['DIAMETR_OBODA'] = $diametr_oboda_fp;

			}else{
			    // 
			}

			if(!empty($pcd_key['KEY'])){
			    $pcd_fp = makeFilterParam($pcd_key['KEY'], PCD_ID);
			    $FilterParam['PCD'] = $pcd_fp;
			}else{
			    // 
			}

			if(!empty($dia_key['KEY'])){
			    $dia_fp = makeFilterParam($dia_key['KEY'], DIA_ID);
			    $FilterParam['DIA'] = $dia_fp;
			}else{
			    // 
			}

			if(!empty($vilet_et_key['KEY'])){
			    $vilet_et_fp = makeFilterParam($vilet_et_key['KEY'], VYLET_ET_ID);
			    $FilterParam['VYLET_ET'] = $vilet_et_fp;
			}else{
			    // 
			}


			$num = count($FilterParam);
	        $i = 0;
	        //$link = '/catalog/diski/?clear_cache=Y';
	        $link = 'catalog/diski/?';
	        
	        foreach ($FilterParam as $key => $value) {
	            $i++;

	           if($i != $num){
	              $link .='&'.$value.'=Y';

	           }else{

	              $link .='&'.$value.'=Y&set_filter=Y';
	           }
	        }

		    // var_dump($link);
		    if(!empty($num)){
			    $urlData = array(
			    	'HREF' => $link,
			    	'NAME' => $item['NAME'],
			    );
		        $replaceLinks[] = $urlData;
		    }
	    }else{
	    	$urlData = array('HREF' => false,'NAME' => $item['NAME']);
		    $replaceLinks[] = $urlData;
	    }
	}


	// var_dump($replaceLinks);
	// die();

	return $replaceLinks;

}



function parse_wheel_factory_variants($wheels_factory){

    // var_dump($wheel_factory);

     if(stripos($wheels_factory,",")){
        $wheels_factory = str_replace(",","|", $wheels_factory);
     }
	$needle = stripos($wheels_factory,"|");
    $wheel_data = array();
            if($needle){
               
                $disk_data = explode("|", $wheels_factory);

                foreach ($disk_data as $key => $item){
                    $var  = explode(' ', $item);   
                    $et_data = explode('ET',$var[3]); 
                    $wheel = [];
                    $wheel['VYLET_ET'] = $et_data[1];
                    $wheel['SHIRINA_DISKA'] = $var[0];
                    $wheel['DIAMETR_OBODA']  = $var[2];
                    $wheel['NAME'] = $item;
                    $wheel_data[] = $wheel;
                }            

            }else{

            	$var  = explode(' ', $wheels_factory); 
                $et_data = explode('ET',$var[3]); 

                $wheel = [];
                $wheel['VYLET_ET'] = $et_data[1];
                $wheel['SHIRINA_DISKA'] = $var[0];
                $wheel['DIAMETR_OBODA'] = $var[2];
               	$wheel['NAME'] = $wheels_factory;
                $wheel_data[] = $wheel;
            }

return $wheel_data;

}

function variantsLinks($wheels_factory_variants, $pcd, $dia){

	$variantsLinks = array();
	foreach ($wheels_factory_variants as $key => $item) {
		$result = checkDisksByModel($item['VYLET_ET'],$item['SHIRINA_DISKA'],$item['DIAMETR_OBODA'], $pcd, $dia);

		// var_dump($item);

		if(!empty($result['ROWS'])){

			// гененируем ключи для ссылку в URL
	        $vilet_et_key =  getKeyPropertyValue(VYLET_ET_CODE, $result['VALID']['VYLET_ET']);
	        $shirina_diska_key = getKeyPropertyValue(SHIRINA_DISKA_CODE, $result['VALID']['SHIRINA_DISKA']);
	        $diametr_oboda_key = getKeyPropertyValue(DIAMETR_OBODA_CODE, $result['VALID']['DIAMETR_OBODA']);
	        $pcd_key = getKeyPropertyValue(PCD_CODE,  $result['VALID']['PCD']);
	        $dia_key = getKeyPropertyValue(DIA_CODE, $result['VALID']['DIA']);

	        if(!empty($shirina_diska_key['KEY'])){
			    $shirina_diska_fp = makeFilterParam($shirina_diska_key['KEY'], SHIRINA_DISKA_ID);
			    $FilterParam['SHIRINA_DISKA'] = $shirina_diska_fp;
			}else{
			    // 

			}


			if(!empty($diametr_oboda_key['KEY'])){
			    $diametr_oboda_fp = makeFilterParam($diametr_oboda_key['KEY'], DIAMETR_OBODA_ID);
			    $FilterParam['DIAMETR_OBODA'] = $diametr_oboda_fp;

			}else{
			    // 
			}

			if(!empty($pcd_key['KEY'])){
			    $pcd_fp = makeFilterParam($pcd_key['KEY'], PCD_ID);
			    $FilterParam['PCD'] = $pcd_fp;
			}else{
			    // 
			}

			if(!empty($dia_key['KEY'])){
			    $dia_fp = makeFilterParam($dia_key['KEY'], DIA_ID);
			    $FilterParam['DIA'] = $dia_fp;
			}else{
			    // 
			}

			if(!empty($vilet_et_key['KEY'])){
			    $vilet_et_fp = makeFilterParam($vilet_et_key['KEY'], VYLET_ET_ID);
			    $FilterParam['VYLET_ET'] = $vilet_et_fp;
			}else{
			    // 
			}


			$num = count($FilterParam);
	        $i = 0;
	        //$link = '/catalog/diski/?clear_cache=Y';
	        $link = 'catalog/diski/?';
	        
	        foreach ($FilterParam as $key => $value) {
	            $i++;

	           if($i != $num){
	              $link .='&'.$value.'=Y';

	           }else{

	              $link .='&'.$value.'=Y&set_filter=Y';
	           }
	        }

		    // var_dump($link);
		    if(!empty($num)){
			    $urlData = array(
			    	'HREF' => $link,
			    	'NAME' => $item['NAME'],
			    	'ROWS'  => $result['ROWS'],
			    	'VALID' => $result['VALID']
			    );
		        $variantsLinks[] = $urlData;
		    }
		}else{
	    	$urlData = array('HREF' => false,'NAME' => $item['NAME'], 'ROWS'=>0, 'VALID' => $result['VALID']);
		    $variantsLinks[] = $urlData;
	    }
	}

	return $variantsLinks;

}



