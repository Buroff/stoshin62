<?php
use Bitrix\Main\Application;

	class DBVehicleSample
	{
		public $models_by_vendor;
		public $connection;

		public function __construct(){
			$this->connection = Application::getConnection();
		}


		// подробная иформация о модели
		function get_model_data($model = null){
			if(!$model){
				$sql = "SELECT vendor, model_seria, car FROM search_by_vehicle LIMIT 1";
				$first_row = $this->_query($sql);
				$model = $first_row[0]['car'];
			}
			$sql = "SELECT * FROM search_by_vehicle WHERE car = '" . $model . "' GROUP BY ID";
			return $this->_query($sql);
		}


    	function _get_data($vendor, $car, $year, $modification)
		{
			$sql = "SELECT * FROM search_by_vehicle 
					WHERE vendor = '" . $vendor . "' 
						AND car = '" . $car . "' 
						AND year = " . $year . " 
						AND modification = '" . $modification . "'";
			
			$result = $this->_query($sql);

			if (isset($result[0]))
			{
				$result[0]['tyres_factory_list'] 	= explode("|", $result[0]['tyres_factory']);
				$result[0]['tyres_tuning_list'] 	= explode("|", $result[0]['tyres_tuning']);
				$result[0]['wheels_factory_list'] 	= explode("|", $result[0]['wheels_factory']);
				$result[0]['wheels_replace_list'] 	= explode("|", $result[0]['wheels_replace']);
				$result[0]['wheels_tuning_list'] 	= explode("|", $result[0]['wheels_tuning']);

				$result[0]['tyres_replace_list'] 	= explode("|", $result[0]['tyres_replace']);


			}

			return $result[0];
		}


// --- BASE

		function get_vendors()
		{
			$sql = "SELECT vendor FROM search_by_vehicle GROUP BY vendor";
			return $this->_query($sql);
		}

		function get_data($vendor, $car, $year, $modification)
		{
			$sql = "SELECT * FROM search_by_vehicle 
					WHERE vendor = '" . $vendor . "' 
						AND car = '" . $car . "' 
						AND year = " . $year . " 
						AND modification = '" . $modification . "'";
			
			$result = $this->_query($sql);

			if (isset($result[0]))
			{
				$result[0]['tyres_factory_list'] 	= explode("|", $result[0]['tyres_factory']);
				$result[0]['tyres_replace_list'] 	= explode("|", $result[0]['tyres_replace']);
				$result[0]['tyres_tuning_list'] 	= explode("|", $result[0]['tyres_tuning']);

				$result[0]['wheels_factory_list'] 	= explode("|", $result[0]['wheels_factory']);
				$result[0]['wheels_replace_list'] 	= explode("|", $result[0]['wheels_replace']);
				$result[0]['wheels_tuning_list'] 	= explode("|", $result[0]['wheels_tuning']);
			}

			return $result[0];
		}


		function _query($query){
			$recordset = $this->connection->query($query);
			$rows = array();
			while ($record = $recordset->fetch())
			{
			  $rows[] = $record;
			}

			return $rows;
		}

		function get_models_by_vendor($vendor, $order = "ORDER by car"){
			$sql = "SELECT * FROM search_by_vehicle WHERE vendor = '" . $vendor . "' ".$order;
			return $this->_query($sql);
		}

		function get_years_by_model($model)
		{
			$sql = "SELECT year FROM search_by_vehicle 
					WHERE car = '" . $model . "' GROUP BY year";
			return $this->_query($sql);
		}

		function get_modifications_by_model($model)
		{
			$sql = "SELECT modification FROM search_by_vehicle 
					WHERE car = '" . $model . "' GROUP BY modification";
			return $this->_query($sql);
		}


    // --- DEFAULT

        // список моделей
        function get_cars($vendor){
            $sql = "SELECT car FROM search_by_vehicle WHERE vendor='" . $vendor . "' GROUP BY car";
            return $this->_query($sql);
        }


        // список годов по модели
        function get_years($vendor, $car = null, $year = null){

        	// var_dump($car);

            if($car){ $order = " AND car = '".$car."' GROUP BY year";
            }else{ $order = " GROUP BY year";}

            $sql = "SELECT year FROM search_by_vehicle WHERE vendor = '".$vendor."' ".$order;
            $data = [];
            $years = $this->_query($sql);

            $selectedYear = false;

            foreach ($years as $key => $value){
	            $data[$key] = $value['year'];
	            if($year){
	            	if($value['year'] == $year){
	            		$selectedYear = $value['year'];
	            		unset($data[$key]);
	            	}
	            		
	            }
            }
            if($selectedYear){ 
            	array_unshift($data, $selectedYear); 
            }

            return $data;
            //return  $years;
        }

        // список модификаций
        function get_modifications($vendor, $car, $year = null, $modification = null){
            if($year){
                $order = " AND year = " . $year . " GROUP BY modification";}
            else{ $order = " GROUP BY modification";}

            $sql = "SELECT modification FROM search_by_vehicle  WHERE vendor = '" . $vendor . "' AND car = '" . $car . "' ".$order;
            
            $selectedModification = false;
			$data = [];
			$modifications = $this->_query($sql);

            foreach ($modifications as $key => $value){
	            $data[$key] = $value['modification'];
	            if($modification){
	            		if($value['modification'] == $modification){
	            			$selectedModification = $value['modification'];
	            			unset($data[$key]);
	            		}
	            		
	            }
            }
            if($selectedModification){ 
            	array_unshift($data, $selectedModification); 
            }
            return $data;
        }


        // список моделей по умолчанию
        function get_models_default($vendor, $modelName = null, $year = null, $modification = null){

            // получаем модели по производителю для поля "Модель"
            $models =  $this->get_models_by_vendor($vendor);
            $data = null;

            if(!$modelName){
                $modelName = $models[0]['car'];
            }

            foreach ($models as $key => $model) {
                    if($model['car'] == $modelName){
                        $data = array(
                            'VENDOR'=>$vendor,
                            'NAME'=>$model['car'],
                            'YEARS'=>$this->get_years_by_model($model['car']), // получаем данные по моделям для поля "Год"
                            'MODIFICATIONS' => $this->get_modifications_by_model($model['car']), // получаем данные по моделям для поля "Модификация"
                            'TYRES_FACTORY' =>$model['tyres_factory']
                        );
                        unset($models[$key]);
                    }else{
                        $models[$key] = array('NAME'=>$model['car']);

                    }
            }

            array_unshift($models, $data);

            //  var_dump($models);
            //  die();

            return $models;
        }

		// параметры данных для вывода в фильтр по умолчанию
		function get_default_data(){
			
			$arr = $this->get_vendors(); // получаем список производителей
			$vendors = [];
		    foreach ($arr as $key => $value) {
		          $vendors[] = $value['vendor'];
		    }	

		    $models = $this->get_models_default($vendors[0]);
            $names = $this->get_cars($vendors[0]);
            $years = $this->get_years($vendors[0], $models[0]['NAME']);
            $modifications = $this->get_modifications($vendors[0], $models[0]['NAME'], $years[0]);

            //var_dump($modifications);

            $result = array(
                'MODELS' => $models,
                'VENDORS' =>$vendors,
                'NAMES' =>$names,
                'YEARS'=>$years,
                'MODIFICATIONS'=>$modifications
            );

			return $result;
		}


		// --- AJAX

		// параметры данных для подстановки в блок автомобилей
		function get_ajax_data($vendor, $modelName = null, $year = null, $modification = null){
			
			// получаем все модели по производителю и сохраняем их в поле
			if(!empty($year) || !empty($modification)){
				$this->models_by_vendor =  $this->get_models_by_vendor($vendor, $order = 'ORDER BY id'); 
			}else{
				$this->models_by_vendor =  $this->get_models_by_vendor($vendor);
			}
			$models = $this->models_by_vendor;
			$first = $models[0]; // первая модель


			// получаем список производителей и ставим выбранного первым (selected)
			$arr = $this->get_vendors($vendor);
			$vendors = [];
		    foreach ($arr as $key => $value) {
		          $vendors[] = $value['vendor'];
		    }
		    if($vendor){
		    	$selectedVendor = null;
		 		foreach ($vendors as $key => $item) {
		    		if($item==$vendor){
		    			$selectedVendor = $item;
		    			unset($vendors[$key]);
		    		}
		    	}
		    	if($selectedVendor){
					array_unshift($vendors, $selectedVendor);
				}
		    }

		    // фильтр по параметрам год если есть имя модели
		    if(!empty($year)){
				$years = $this->get_years($vendor, $modelName, $year);
			}else{
				$years = $this->get_years($vendor, $modelName);
			}


		    // фильтр по параметрам модель/год/модификация
		    if(!empty($modelName)){

			    if($year && $modification){
			    	$models = $this->select_by_year_modef($vendor, $modelName, $year, $modification);
	                $modifications = $this->get_modifications($vendor, $modelName, $year, $modification);
			    }
			    elseif($year){
			    	$models = $this->select_by_year_modef($vendor, $modelName, $year);
	                $modifications = $this->get_modifications($vendor, $modelName, $year);
			    }
			    else{
			    	$models = $this->select_by_model($vendors[0], $modelName, $years[0]);
	                $modifications = $this->get_modifications($vendor, $modelName);
			    }
		
		    }else{
		    	$years = $this->get_years($vendor, $first['car']);
		    	$models = $this->select_by_model($vendors[0], $first['car'], $years[0]);
		    	$modifications = $this->get_modifications($vendor, $first['car']);
		    }

			$modelNames = $this->get_model_names($models);
    
		    // получаем подробные данные по модели
		    // get_data_by_model($vendor, $modelName, $year, $modification)
			return $result = array(
				'MODEL' => $models[0], // 1-я модель автомобиля, из нее берем параметры в поиск по шинам и дискам
				'VENDORS' =>$vendors,
				'MODELS'=>$modelNames,
				'YEARS'=>$years,
                'MODIFICATIONS'=>$modifications,

			);
	
		}

		// список моделей
		function  select_by_model($vendor, $modelName, $year, $modification = null){
			// получаем модели по производителю для поля "Модель"
			// $models =  $this->get_models_by_vendor($vendor); 
			$models = $this->models_by_vendor;
			$result = [];
			$data = null;

			foreach ($models as $key => $model) {

		    	if($model['car'] == $modelName && $model['year'] == $year){
		    		$data = array(
								'NAME'=>$model['car'],
								'tyres_factory_db'=>$model['tyres_factory'],
                                'TYRES_FACTORY' =>$this->parse_tyre_factory($model['tyres_factory']),
                                'SELECTED_CAR_ID'=>$model['id']

					);
		    		unset($models[$key]);
		    	}else{
		    		$models[$key] = array('NAME'=>$model['car']);

		    	}
		    }

		    array_unshift($models, $data);
			return $models;
		}

		function select_by_year_modef($vendor, $modelName, $year, $modification = null){
			// $models =  $this->get_models_by_vendor($vendor, $order = 'ORDER BY id');
			$models =  $this->models_by_vendor;
			$data = null;
		

			if($modification){
				foreach ($models as $key => $model) {

					$modelNames[$model['car']] = $model['car'];

					if($model['year'] == $year && $model['car'] == $modelName && $model['modification'] == $modification){
						
							$data = array(
								'NAME'=>$model['car'],
                                'TYRES_FACTORY' =>$this->parse_tyre_factory($model['tyres_factory']),
                                'SELECTED_CAR_ID'=>$model['id']
							);
							unset($models[$key]);
					
					}else{
						$models[$key] = array('NAME'=>$model['car']);
					}
				}
			}else{
				$step = 0;
				foreach ($models as $key => $model) {
					if($model['year'] == $year && $model['car'] == $modelName && $step == 0){
						
							$data = array(
								'NAME'=>$model['car'],
                                'TYRES_FACTORY' =>$this->parse_tyre_factory($model['tyres_factory']),
                                'SELECTED_CAR_ID'=>$model['id']

							);
							unset($models[$key]);
							$step++;
					}else{
						$models[$key] = array('NAME'=>$model['car']);
					}
				}
			}
			
			array_unshift($models, $data);
			return $models;
		}

		// парсинг данных по шинам полученные от модели автомобиля
        function parse_tyre_factory($tyre_data){

            $data = array(
                'SHIRINA_PROFILA'=>[],
                'VISOTA_PROFILA'=>[],
                'DIAMETR'=>[]
            );

            $needle = stripos($tyre_data,"|");
            if($needle){
                $tyres_data = explode("|", $tyre_data);
                // foreach ($tyres_data as $key => $item){
                		
                // 		// ! регулярное выраженние 
                // 		// не меняй  без проверки на тестовой строке
                //         $var = preg_split('~[^\da-zA-Z]+~', $item); 
                //         $data['SHIRINA_PROFILA'][] = $var[0];
                //         $data['VISOTA_PROFILA'][] = $var[1];
                //         $data['DIAMETR'][] = substr($var[2], strpos($var[2], 'R')+1);

                // }

                // ! регулярное выраженние 
                // не меняй  без проверки на тестовой строке
                $var = preg_split('~[^\da-zA-Z]+~', $tyres_data[0]); 
                $data['SHIRINA_PROFILA'][] = $var[0];
                $data['VISOTA_PROFILA'][] = $var[1];
                $data['DIAMETR'][] = substr($var[2], strpos($var[2], 'R')+1);


            }else{
                $var = preg_split('~[^\da-zA-Z]+~', $tyre_data);
                $data['SHIRINA_PROFILA'][] = $var[0];
                $data['VISOTA_PROFILA'][] = $var[1];
                $data['DIAMETR'][] = substr($var[2], strpos($var[2], 'R')+1);
            }

            return $data;
        }

		function get_model_names($models){
			$modelNames = array();
			
			foreach ($models as $key => $model) {
				if(!(isset($modelNames[$model['NAME']]))){
					$modelNames[$model['NAME']] = $model['NAME'];
				}
			}

			$data = [];
			foreach ($modelNames as $key => $value) {
					$data[] = $value;
			}
			return $data;
		}

		

		// получение данных от модели автомобиля
		function get_data_by_model($vendor, $car, $year, $modification){
			$sql = "SELECT * FROM search_by_vehicle 
					WHERE vendor = '" . $vendor . "' 
						AND car = '" . $car . "' 
						AND year = " . $year . " 
						AND modification = '" . $modification . "'";
			
			$result = $this->_query($sql);

			if (isset($result[0]))
			{
				$result[0]['tyres_factory_list'] 	= explode("|", $result[0]['tyres_factory']);
				$result[0]['tyres_replace_list'] 	= explode("|", $result[0]['tyres_replace']);
				$result[0]['tyres_tuning_list'] 	= explode("|", $result[0]['tyres_tuning']);
			}

			return $result[0];
		}




	}

// --- функции для работы классом DBVehicleSample (таблицы БД по автомобилям)

function getDefaultData(){ // первый производитель по нему устававливаются остальные поля
    $db = new DBVehicleSample();
    return $db->get_default_data();
}

function getVendors(){ // список всех производителей для поля "Марка"
    $db = new DBVehicleSample();
    $arr = $db->get_vendors();
    $vendors = [];
    foreach ($arr as $key => $value) {
          $vendors[] = $value['vendor'];
    }
    return  $vendors;
}


function getModifications($vendor, $car, $year = null, $modification = null){ // список модификаций
    //var_dump($modification);
    $db = new DBVehicleSample();
    return $db->get_modifications($vendor, $car, $year, $modification);
}

function parseTyreData($tyre_data){
    $db = new DBVehicleSample();
    return $db->parse_tyre_factory($tyre_data);
}

function getAjaxData($strVendor = null, $strModel = null, $strYear = null, $strModif = null){ // получаеам Ajax массив по Марка/Модель/Год/Модификация
    // echo '-- getAjaxData -- <br>';
    $db = new DBVehicleSample();
    return $db->get_ajax_data($strVendor, $strModel, $strYear, $strModif);
}

// получаеам Ajax массив по Марка/Модель/Год/Модификация
function getYears($strVendor = null, $strModel = null, $strYear = null, $strModif = null){ 
    // echo '-- getAjaxData -- <br>';
    $db = new DBVehicleSample();
    return $db->get_years($strVendor, $strModel, $strYear, $strModif);
}


// ---  ФИЛЬТР ПО МОДЕЛИ

// поиск товаров по параметерам
function checkProductsByModel($shirina_profila = null, $visota_profila = null, $diametr = null){
  
  CModule::IncludeModule('iblock');

    // $arSort = Array("NAME"=>"ASC");
    $arSelect = Array("ID","NAME", "PROPERTY_SHIRINA_PROFILA");

    // var_dump($arSelect);
    // $arFilter = Array("IBLOCK_ID" => CATALOG_FINAL, 'PROPERTY_SHIRINA_PROFILA'=>175, 'PROPERTY_VISOTA_PROFILA'=>60, 'PROPERTY_DIAMETR'=>16, "SECTION_ID"=>SHINY);
    // $arFilter = Array("IBLOCK_ID" => 23, "SECTION_ID"=>52);
    //$arFilter = Array("NAME"=>'BFGoodrich 185/75 R16 C 104/102R ACTIVAN');
    // $res =  CIBlockElement::GetList($arSort, $arFilter, false, Array("nPageSize"=>100), $arSelect);
    // $products = array();
      
    // while($ob = $res->GetNextElement()){
    //     $arItem = $ob->GetFields();
    //     $products[] = $arItem;
    // }

    // var_dump($products); //
    // if(!empty($products)){
    //     return $products;
    // }else{
    //     return false;
    // }

    $arFilter = Array(
    	"IBLOCK_ID"=>23, 
    	"ACTIVE"=>"Y", 
    	"SECTION_ID"=>array(53,58,59,67), 
    	'PROPERTY_SHIRINA_PROFILA_VALUE'=>array(175),  
    	'PROPERTY_VISOTA_PROFILA_VALUE'=>array(60), 
    	'PROPERTY_DIAMETR_VALUE'=>array(16)
    ); 
    $arNavStartParams = Array("nPageSize"=>100);
  

    $res = CIBlockElement::GetList(Array("ID"=>"ASC"), $arFilter, false, false, $arSelect);   
    
    $array_line_full = array();

	// while($ar = $res->GetNext()) {
	//   	$array_line_full[]  = $ar;
	// }
	echo intval($res->SelectedRowsCount());
}


