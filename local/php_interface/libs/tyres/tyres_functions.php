<?php
use Bitrix\Main\Application;


// ---  ФИЛЬТР ПО МОДЕЛИ


// проверка на наличие товара в магазине по сочетанию параметров
// function checkTyresByModel($vylet_et = null, $shirina_diska = null, $diametr_oboda  = null, $pcd = null, $dia = null){
function checkTyresByModel($shirina_profila = null, $visota_profila = null, $diametr = null){

	CModule::IncludeModule('iblock');
	$arSelect = Array("ID","NAME", "PROPERTY_*");

	$arFilter = Array(
    	"IBLOCK_ID"=>23, 
    	"ACTIVE"=>"Y", 
    	"SECTION_ID"=>array(52,53,67,58,59,71)
    );

    $valid = array();

	if($shirina_profila){
		$arFilter['PROPERTY_SHIRINA_PROFILA_VALUE'] = $shirina_profila;
		$valid['SHIRINA_PROFILA'] = $shirina_profila;
	}

	if($visota_profila){
		$arFilter['PROPERTY_VISOTA_PROFILA_VALUE'] = $visota_profila;
		$valid['VISOTA_PROFILA'] = $visota_profila;
	}


	if($diametr){
		$arFilter['PROPERTY_DIAMETR_VALUE'] = $diametr;
		$valid['DIAMETR'] = $diametr;
	}
	
	// pr($arFilter);
	
    $res = CIBlockElement::GetList(Array("ID"=>"ASC"), $arFilter, false, false, $arSelect);  

	$result['ROWS'] = intval($res->SelectedRowsCount());

// 	while($ob = $res->GetNextElement()){ 
//  $arFields = $ob->GetFields();  
// pr($arFields);
// }
	
	
	$result['VALID'] = $valid;
	return $result;
}




// добавление значения свойства
function setTyresPropertyValue($prop_id, $prop_value){

	$PROPERTY_ID = false;

	switch (intval($prop_id)) {
		case PCD_ID:
			$PROPERTY_ID = PCD_ID;
		break;

		case DIAMETR_OBODA_ID:
			$PROPERTY_ID = DIAMETR_OBODA_ID;	
		break;
	}

	if($PROPERTY_ID){
		CModule::IncludeModule('iblock');
		$ibpenum = new CIBlockPropertyEnum;
		if($PropID = $ibpenum->Add(Array('PROPERTY_ID'=>$PROPERTY_ID, 'VALUE'=>$prop_value))){
			return $PropID;
		}
	}
	return false;
}
// удаление значение свойства 
// CIBlockPropertyEnum::Delete(1181);


// генерация ссылки для GET запроса в каталог дисков
function makeTyresFilterParam($key_prop_value, $prop_id, $prop_value = false){
	  $safeFilterName = htmlspecialcharsbx('arrFilter');
	  $filterPropertyID = $safeFilterName.'_'.$prop_id;
	  $htmlKey = htmlspecialcharsbx($key_prop_value); // ?
	  $keyCrc = abs(crc32($htmlKey));
	  return $filterPropertyIDKey = $filterPropertyID.'_'.$keyCrc;
}
// $result = makeFilterParam($key_prop_value = 717, $prop_id = 197, $prop_value = false);
// var_dump($result); 
// arrFilter_197_1601861446


// ключь значения свойства инфоблока
function getTyresKeyPropertyValue($prop_code, $prop_value){
	CModule::IncludeModule('iblock');
		$property_enums = CIBlockPropertyEnum::GetList(
		  Array("DEF"=>"DESC", "SORT"=>"ASC"), 
		  Array("IBLOCK_ID"=>CATALOG_FINAL, "CODE"=>$prop_code, "VALUE"=>$prop_value)
		);

		$result = false;
		while($enum_fields = $property_enums->GetNext()){
		    $param = array();
		    $param['KEY'] = $enum_fields["ID"];
		    $param['VALUE'] = $enum_fields["VALUE"];
		    $result = $param;
		    // $result = $enum_fields["ID"];
		}

		if(!empty($result)){
			return $result;
		}

		return false;
}

// $result = getKeyPropertyValue(PCD_CODE, "4х114,3");
// var_dump($result); 





// ---  ЗАМЕНА
function parseTyresReplace($tyres_replace){

	// echo '--- parsetyresReplace ---';

        $needle = stripos($tyres_replace,"|");
        $replace_data = array();
        
        if($needle){
               
            $disk_data = explode("|", $tyres_replace);

            foreach ($disk_data as $key => $item){
                    $var  = explode(' ', $item);   
                    $et_data = explode('ET',$var[3]); 
                    $replace = [];
                    $replace['VYLET_ET'] = $et_data[1];
                    $replace['SHIRINA_DISKA'] = $var[0];
                    $replace['DIAMETR_OBODA']  = $var[2];
                    $replace['NAME'] = $item;

                    $replace_data[] = $replace;
            }            

        }
        else{

            	$var  = explode(' ', $tyres_replace); 
                $et_data = explode('ET',$var[3]); 

                $replace = [];
                $replace['VYLET_ET'] = $et_data[1];
                $replace['SHIRINA_DISKA'] = $var[0];
                $replace['DIAMETR_OBODA'] = $var[2];
               	$replace['NAME'] = $tyres_replace;
                $replace_data[] = $replace;
        }

    return $replace_data;
}


function replaceTyresLinks($replace_data, $pcd = null, $dia  = null){

    $replaceLinks = array();
    
    foreach ($replace_data as $key => $item) {

	    $result = checkTyresByModel($item['SHIRINA_PROFILA'], $item['VISOTA_PROFILA'], $item['DIAMETR']);


	    if(!empty($result['ROWS'])){

			$shirina_profila = getKeyPropertyValue(SHIRINA_PROFILA_CODE, $result['VALID']['SHIRINA_PROFILA']);
			if(!empty($shirina_profila['KEY'])){
			    $shirina_profila_fp = makeFilterParam($shirina_profila['KEY'], SHIRINA_PROFILA_ID);
			    $FilterParam['SHIRINA_PROFILA'] = $shirina_profila_fp;
			}

			$visota_profila = getKeyPropertyValue(VISOTA_PROFILA_CODE, $result['VALID']['VISOTA_PROFILA']);
			if(!empty($visota_profila['KEY'])){
			    $visota_profila_fp = makeFilterParam($visota_profila['KEY'], SHIRINA_PROFILA_ID);
			    $FilterParam['VISOTA_PROFILA'] = $visota_profila_fp;
			}


			$diametr = getKeyPropertyValue(DIAMETR_CODE, $result['VALID']['DIAMETR']);
			if(!empty($diametr['KEY'])){
			    $diametr_fp = makeFilterParam($diametr['KEY'], DIAMETR_ID);
			    $FilterParam['DIAMETR'] = $diametr_fp;
			}

			$num = count($FilterParam);
	        $i = 0;
	        $link = 'catalog/shiny/?';
	        
	        foreach ($FilterParam as $key => $value) {
	            $i++;

	           if($i != $num){
	              $link .='&'.$value.'=Y';

	           }else{

	              $link .='&'.$value.'=Y&set_filter=Y';
	           }
	        }

		    // var_dump($link);
		    if(!empty($num)){
			    $urlData = array(
			    	'HREF' => $link,
			    	'NAME' => $item['NAME'],
			    );
		        $replaceLinks[] = $urlData;
		    }
	    }else{
	    	$urlData = array('HREF' => false,'NAME' => $item['NAME']);
		    $replaceLinks[] = $urlData;
	    }
	}


	// pr($replaceLinks);
	// die();

	return $replaceLinks;

}



function parse_tyre_factory_variants($tyres_factory){

    // var_dump($tyre_factory);

     if(stripos($tyres_factory,",")){
        $tyres_factory = str_replace(",","|", $tyres_factory);
     }
	$needle = stripos($tyres_factory,"|");
    $tyre_data = array();
            if($needle){
               
                $parse_data = explode("|", $tyres_factory);

                foreach ($parse_data as $key => $item){
                    $var = preg_split('~[^\da-zA-Z]+~', $item);  
                    $tyre = [];
                    $tyre['SHIRINA_PROFILA'] = $var[0];
	                $tyre['VISOTA_PROFILA'] = $var[1];
	                $tyre['DIAMETR'] = substr($var[2], strpos($var[2], 'R')+1);
                    $tyre['NAME'] = $item;
                    $tyre_data[] = $tyre;
                }            

            }else{

            	$var = preg_split('~[^\da-zA-Z]+~', $tyres_factory);
  
                $tyre = [];
                $tyre['SHIRINA_PROFILA'] = $var[0];
                $tyre['VISOTA_PROFILA'] = $var[1];
                $tyre['DIAMETR'] = substr($var[2], strpos($var[2], 'R')+1);


               	$tyre['NAME'] = $tyres_factory;
                $tyre_data[] = $tyre;
            }

return $tyre_data;

}

function variantsTyresLinks($tyres_factory_variants, $pcd = null, $dia = null){

	$variantsTyresLinks = array();
	foreach ($tyres_factory_variants as $key => $item) {
		$result = checkTyresByModel($item['SHIRINA_PROFILA'], $item['VISOTA_PROFILA'], $item['DIAMETR']);

		// pr($item);
		// die();

		if(!empty($result['ROWS'])){

			// гененируем ключи для ссылку в URL
	        // $vilet_et_key =  getTyresKeyPropertyValue(VYLET_ET_CODE, $result['VALID']['VYLET_ET']);
			// if(!empty($vilet_et_key['KEY'])){
			//     $vilet_et_fp = makeFilterParam($vilet_et_key['KEY'], VYLET_ET_ID);
			//     $FilterParam['VYLET_ET'] = $vilet_et_fp;
			// }

			$shirina_profila = getKeyPropertyValue(SHIRINA_PROFILA_CODE, $result['VALID']['SHIRINA_PROFILA']);
			if(!empty($shirina_profila['KEY'])){
			    $shirina_profila_fp = makeFilterParam($shirina_profila['KEY'], SHIRINA_PROFILA_ID);
			    $FilterParam['SHIRINA_PROFILA'] = $shirina_profila_fp;
			}

			$visota_profila = getKeyPropertyValue(VISOTA_PROFILA_CODE, $result['VALID']['VISOTA_PROFILA']);
			if(!empty($visota_profila['KEY'])){
			    $visota_profila_fp = makeFilterParam($visota_profila['KEY'], VISOTA_PROFILA_ID);
			    $FilterParam['VISOTA_PROFILA'] = $visota_profila_fp;
			}

			$diametr = getKeyPropertyValue(DIAMETR_CODE, $result['VALID']['DIAMETR']);
			if(!empty($diametr['KEY'])){
			    $diametr_fp = makeFilterParam($diametr['KEY'], DIAMETR_ID);
			    $FilterParam['DIAMETR'] = $diametr_fp;
			}

			$num = count($FilterParam);
	        $i = 0;
	        $link = 'catalog/shiny/?';
	        
	        foreach ($FilterParam as $key => $value) {
	            $i++;
	           if($i != $num){
	              $link .='&'.$value.'=Y';
	           }else{
	              $link .='&'.$value.'=Y&set_filter=Y';
	           }
	        }


		    if(!empty($num)){
			    $urlData = array(
			    	'HREF' => $link,
			    	'NAME' => $item['NAME'],
			    	'ROWS'  => $result['ROWS'],
			    	'VALID' => $result['VALID']
			    );
		        $variantsTyresLinks[] = $urlData;
		    }

		    // pr($variantsTyresLinks);
		    // die();

		}else{
	    	$urlData = array('HREF' => false,'NAME' => $item['NAME'], 'ROWS'=>0, 'VALID' => $result['VALID']);
		    $variantsTyresLinks[] = $urlData;
	    }
	}

	return $variantsTyresLinks;

}



