<?php
use Bitrix\Main\Application;

	class TyreDBVehicleSample
	{

		public $models_by_vendor;
		public $connection;

		public function __construct(){
			$this->connection = Application::getConnection();
		}


		 

		function _get_data($vendor, $car, $year, $modification)
		{
			$sql = "SELECT * FROM search_by_vehicle 
					WHERE vendor = '" . $vendor . "' 
						AND car = '" . $car . "' 
						AND year = " . $year . " 
						AND modification = '" . $modification . "'";
			
			$result = $this->_query($sql);

			if (isset($result[0]))
			{
				$result[0]['tyres_factory_list'] 	= explode("|", $result[0]['tyres_factory']);
				$result[0]['tyres_tuning_list'] 	= explode("|", $result[0]['tyres_tuning']);
				$result[0]['wheels_factory_list'] 	= explode("|", $result[0]['wheels_factory']);
				$result[0]['wheels_replace_list'] 	= explode("|", $result[0]['wheels_replace']);
				$result[0]['wheels_tuning_list'] 	= explode("|", $result[0]['wheels_tuning']);
				$result[0]['tyres_replace_list'] 	= explode("|", $result[0]['tyres_replace']);


			}

			return $result[0];
		}


// --- BASE

		function get_vendors()
		{
			$sql = "SELECT vendor FROM search_by_vehicle GROUP BY vendor";
			return $this->_query($sql);
		}

		function get_data($vendor, $car, $year, $modification)
		{
			$sql = "SELECT * FROM search_by_vehicle 
					WHERE vendor = '" . $vendor . "' 
						AND car = '" . $car . "' 
						AND year = " . $year . " 
						AND modification = '" . $modification . "'";
			
			$result = $this->_query($sql);

			if (isset($result[0]))
			{
				$result[0]['tyres_factory_list'] 	= explode("|", $result[0]['tyres_factory']);
				$result[0]['tyres_replace_list'] 	= explode("|", $result[0]['tyres_replace']);
				$result[0]['tyres_tuning_list'] 	= explode("|", $result[0]['tyres_tuning']);

				$result[0]['wheels_factory_list'] 	= explode("|", $result[0]['wheels_factory']);
				$result[0]['wheels_replace_list'] 	= explode("|", $result[0]['wheels_replace']);
				$result[0]['wheels_tuning_list'] 	= explode("|", $result[0]['wheels_tuning']);
			}

			return $result[0];
		}


		function _query($query){
			$recordset = $this->connection->query($query);
			$rows = array();
			while ($record = $recordset->fetch())
			{
			  $rows[] = $record;
			}

			return $rows;
		}

		function get_models_by_vendor($vendor, $order = "ORDER by car"){
			// echo $sql = "SELECT * FROM search_by_vehicle WHERE vendor = '" . $vendor . "' ".$order;
			$sql = "SELECT * FROM search_by_vehicle WHERE vendor = '" . $vendor . "' ".$order;
			return $this->_query($sql);
		}

		function get_years_by_model($model)
		{
			$sql = "SELECT year FROM search_by_vehicle 
					WHERE car = '" . $model . "' GROUP BY year";
			return $this->_query($sql);
		}

		function get_modifications_by_model($model)
		{
			$sql = "SELECT modification FROM search_by_vehicle 
					WHERE car = '" . $model . "' GROUP BY modification";
			return $this->_query($sql);
		}



    // --- DEFAULT

        // список моделей
        function get_cars($vendor){
            $sql = "SELECT car FROM search_by_vehicle WHERE vendor='" . $vendor . "' GROUP BY car";
            return $this->_query($sql);
        }

        // список годов по модели
        function get_years($vendor, $car = null, $year = null){

        	// var_dump($car);

            if($car){ $order = " AND car = '".$car."' GROUP BY year";
            }else{ $order = " GROUP BY year";}

            $sql = "SELECT year FROM search_by_vehicle WHERE vendor = '".$vendor."' ".$order;
            $data = [];
            $years = $this->_query($sql);

            $selectedYear = false;

            foreach ($years as $key => $value){
	            $data[$key] = $value['year'];
	            if($year){
	            	if($value['year'] == $year){
	            		$selectedYear = $value['year'];
	            		unset($data[$key]);
	            	}
	            		
	            }
            }
            if($selectedYear){ 
            	array_unshift($data, $selectedYear); 
            }

            return $data;
            //return  $years;
        }


        function get_default_models_by_vendor($vendor, $order = "ORDER by car"){
			$sql = "SELECT car FROM search_by_vehicle WHERE vendor = '" . $vendor ."' GROUP BY car ";
			// return $this->_query($sql);

			$models = $this->_query($sql);
			$data = [];

			foreach ($models as $key => $value){
	            $data[$key] = $value['car'];
            }

            return $data;
		}


        // список модификаций
        function get_modifications($vendor = null, $car = null, $year = null, $modification = null){

            if($year){
                $order = " AND year = " . $year . " GROUP BY modification";}
            else{ $order = " GROUP BY modification";}

            $sql = "SELECT modification FROM search_by_vehicle  WHERE vendor = '" . $vendor . "' AND car = '" . $car . "' ".$order;

            $selectedModification = false;
			$data = [];
			$modifications = $this->_query($sql);

            foreach ($modifications as $key => $value){
	            $data[$key] = $value['modification'];
	            if($modification){
	            		if($value['modification'] == $modification){
	            			$selectedModification = $value['modification'];
	            			unset($data[$key]);
	            		}
	            		
	            }
            }
            if($selectedModification){ 
            	array_unshift($data, $selectedModification); 
            }
            return $data;
        }


        // список моделей по умолчанию // !!!
        function get_models_default($vendor, $modelName = null, $year = null, $modification = null){

            // получаем модели по производителю для поля "Модель"
            $models =  $this->get_models_by_vendor($vendor);
            $data = null;

            if(!$modelName){
                $modelName = $models[0]['car'];
            }

            foreach ($models as $key => $model) {
                    if($model['car'] == $modelName){
                        $data = array(
                            'VENDOR'=>$vendor,
                            'NAME'=>$model['car'],
                            'YEARS'=>$this->get_years_by_model($model['car']), // получаем данные по моделям для поля "Год"
                            'MODIFICATIONS' => $this->get_modifications_by_model($model['car']), // получаем данные по моделям для поля "Модификация"
                            'TYRES_FACTORY' =>$model['tyres_factory']
                            
                        );
                        unset($models[$key]);
                    }else{
                        $models[$key] = array('NAME'=>$model['car']);

                    }
            }

            array_unshift($models, $data);

            //  var_dump($models);
            //  die();

            return $models;
        }


		// параметры данных для вывода в фильтр по умолчанию
		function _get_default_data(){
			
			$arr = $this->get_vendors(); // получаем список производителей
			$vendors = [];
		    foreach ($arr as $key => $value) {
		          $vendors[] = $value['vendor'];
		    }	

		    $models = $this->get_models_default($vendors[0]);
            $names = $this->get_cars($vendors[0]);
            $years = $this->get_years($vendors[0], $models[0]['NAME']);
            $modifications = $this->get_modifications($vendors[0], $models[0]['NAME'], $years[0]);

            //var_dump($modifications);

            $result = array(
                'MODELS' => $models,
                'VENDORS' =>$vendors,
                'NAMES' =>$names,
                'YEARS'=>$years,
                'MODIFICATIONS'=>$modifications
            );

			return $result;
		}


		function get_default_data(){

			$arr = $this->get_vendors(); // получаем список производителей
			$vendors = [];
		    foreach ($arr as $key => $value) {
		          $vendors[] = $value['vendor'];
		    }	

		    $this->models_by_vendor =  $this->get_models_by_vendor($vendors[0], $order = 'ORDER BY id ASC');
		    $models = $this->models_by_vendor;
		    //$models = $this->get_models_default($vendors[0], );
			$first = $models[0]; // первая модель
			$years = $this->get_years($vendors[0], $first['car']);
		    $modifications = $this->get_modifications($vendors[0], $first['car'], $first['year']);
		    $modelNames = $this->get_default_models_by_vendor($vendors[0]);

		    $models = $this->select_by_year_modef($first['vendor'], $first['car'], $first['year'], $modifications[0]);
	
		    // параметры для подсчета товаров в каталоге по выбранному сочетанию (модели автомобиля)
			$tyres_factory_variants = $this->tyres_factory_variants($models[0]['TYRES_FACTORY_VARIANTS']);

			// замена дисков 
			$tyres_replace = $this->tyres_replace($models[0]['TYRES_REPLACE']);

		    // получаем подробные данные по модели
			return $result = array(
				'MODEL' => $models[0], // 1-я модель автомобиля, из нее берем параметры в поиск по шинам и дискам
				'VENDORS' =>$vendors,
				'MODELS'=>$modelNames,
				'YEARS'=>$years,
                'MODIFICATIONS'=>$modifications,
                'TYRES_REPLACE'=>$tyres_replace, // здесь ссылки по сочетаниям замены
                'TYRES_FACTORY_VARIANTS'=>$tyres_factory_variants
			);

		}



		// --- AJAX 

		
		function get_ajax_data($vendor, $modelName = null, $year = null, $modification = null){

			// получаем все модели по производителю и сохраняем их в поле
			if(!empty($year) || !empty($modification)){
				$this->models_by_vendor =  $this->get_models_by_vendor($vendor, $order = 'ORDER BY id'); 
			}else{
				$this->models_by_vendor =  $this->get_models_by_vendor($vendor, $order = 'ORDER BY id ASC');
			}
			$models = $this->models_by_vendor;
			$first = $models[0]; // первая модель
			// pr($first);
			// exit();

			// получаем список производителей и ставим выбранного первым (selected)
			$arr = $this->get_vendors($vendor);
			$vendors = [];
		    foreach ($arr as $key => $value) {
		          $vendors[] = $value['vendor'];
		    }
		    if($vendor){
		    	$selectedVendor = null;
		 		foreach ($vendors as $key => $item) {
		    		if($item==$vendor){
		    			$selectedVendor = $item;
		    			unset($vendors[$key]);
		    		}
		    	}
		    	if($selectedVendor){
					array_unshift($vendors, $selectedVendor);
				}
		    }
		    // фильтр по параметрам год если есть имя модели
		    if(!empty($year)){
				$years = $this->get_years($vendor, $modelName, $year);
			}else{
				$years = $this->get_years($vendor, $modelName);
			}
			// pr($year);
			// exit();

		    // фильтр по параметрам модель/год/модификация
		    if(!empty($modelName)){

			    if($year && $modification){
			    	$models = $this->select_by_year_modef($vendor, $modelName, $year, $modification);
	                $modifications = $this->get_modifications($vendor, $modelName, $year, $modification);
			    }
			    elseif($year){
			    	$models = $this->select_by_year_modef($vendor, $modelName, $year);
	                $modifications = $this->get_modifications($vendor, $modelName, $year);
			    }
			    else{
			    	$models = $this->select_by_model($vendors[0], $modelName, $years[0]);
	                $modifications = $this->get_modifications($vendor, $modelName);
			    }
		
		    }else{
		    	$years = $this->get_years($vendor, $first['car']);
		    	$modifications = $this->get_modifications($vendor, $first['car'], $first['year']);
		    	$models = $this->select_by_year_modef($first['vendor'], $first['car'], $first['year'], $modification['modification']); // !!!
		    }

			$modelNames = $this->get_model_names($models);

			// параметры для подсчета товаров в каталоге по выбранному сочетанию (модели автомобиля)
			// $vylet_et = $models[0]['TYRES_FACTORY']['VYLET_ET'][0];
			// $shirina_diska = $models[0]['TYRES_FACTORY']['SHIRINA_DISKA'][0];
			// $diametr_oboda = $models[0]['TYRES_FACTORY']['DIAMETR_OBODA'][0];
			// $pcd = $models[0]['DIAMETR_RASPOLOZHENIYA_OTVERSTIY_PCD'];
			// $dia = $models[0]['DIAMETR_TSENTRALNOGO_OTVERSTIYA'];

			// количестов товаров по данному сочетанию параметров 
			// $result = checkDisksByModel($vylet_et, $shirina_diska, $diametr_oboda, $pcd, $dia);
			// $models[0]['ROWS'] = $result['ROWS']; 	
			// $models[0]['VALID'] = $result['VALID'];
			// $models[0]['CHECK_DISK_BY_MODEL'] = array(
			// 	// 'vylet_et'=>$vylet_et,
			// 	// 'shirina_diska'=>$shirina_diska,
			// 	// 'diametr_oboda'=>$diametr_oboda,
			// 	// 'pcd'=>$pcd,
			// 	// 'dia'=>$dia
			// );

			// замена дисков 
			$tyres_replace = $this->tyres_replace($models[0]['TYRES_REPLACE'], $pcd, $dia);

			// дополнительные заводские размеры
            $tyres_factory_variants = $this->tyres_factory_variants($models[0]['TYRES_FACTORY_VARIANTS']);
            // $tyres_factory_variants = $this->tyres_factory_variants('6 x 14 ET35|8.5 x 20 ET36, 10 x 20 ET30', $pcd, $dia);

            foreach ($tyres_factory_variants as $key => $value) {
				if(!empty($value['ROWS'])){
						$models[0]['ROWS'] = $value['ROWS']; 	
						$models[0]['VALID'] = $value['VALID'];
						break;
				}else{
					if($key == 0){
						$models[0]['ROWS'] = $value['ROWS']; 	
						$models[0]['VALID'] = $value['VALID'];
					}
				}
			}
	  
		    // получаем подробные данные по модели
			return $result = array(
				'MODEL' => $models[0], // 1-я модель автомобиля, из нее берем параметры в поиск по шинам и дискам
				'VENDORS' =>$vendors,
				'MODELS'=>$modelNames,
				'YEARS'=>$years,
                'MODIFICATIONS'=>$modifications,
                'TYRES_REPLACE'=>$tyres_replace, // здесь ссылки по сочетаниям замены
                'TYRES_FACTORY_VARIANTS'=>$tyres_factory_variants
			);
	
		}

		// замена
		function tyres_replace($tyres_replace, $pcd, $dia){

				$replace_data =  parseTyresReplace($tyres_replace); // парсим
				// var_dump($replace_data);
				$links = replaceLinks($replace_data, $pcd, $dia); // генерим ссылки
				if(!empty($links)){
					return $links;
				}
				return false;
		}

		// варианты
		function tyres_factory_variants($tyres_factory_variants, $pcd = null, $dia = null){
				$tyres_factory_data = parse_tyre_factory_variants($tyres_factory_variants);
				$links = variantsTyresLinks($tyres_factory_data, $pcd = null, $dia = null); // генерим ссылки
				// pr($links);
				// die();
				if(!empty($links)){
					return $links;
				}
				return false;
		}

		// список моделей
		function select_by_model($vendor, $modelName = null, $year = null, $modification = null){
			// сохраненный список моделей по производителю
			$models = $this->models_by_vendor;
			$result = [];
			$data = null;

			foreach ($models as $key => $model) {

		    	if($model['car'] == $modelName && $model['year'] == $year){
		    		$data = array(
								'NAME'=>$model['car'],
								'SELECTED_CAR_ID'=>$model['id'],
								'NAME'=>$model['car'],
								'YEAR'=>$model['year'],
                                'MODIFICATION'=>$model['modification'],
                                'TYRES_FACTORY' =>$this->parse_tyre_factory($model['tyres_factory']),
                                'TYRES_REPLACE' =>$model['tyres_replace'],
                                'TYRES_FACTORY_VARIANTS'=>$model['tyres_factory']
					);
		    		unset($models[$key]);
		    	}else{
		    		$models[$key] = array('NAME'=>$model['car']);

		    	}
		    }

		    array_unshift($models, $data);
			return $models;
		}

		function select_by_year_modef($vendor, $modelName, $year, $modification = null){

			$models =  $this->models_by_vendor; // сохраненный список моделей по производителю
			$data = null;
		

			if($modification){
				foreach ($models as $key => $model) {

					$modelNames[$model['car']] = $model['car'];

					if($model['year'] == $year && $model['car'] == $modelName && $model['modification'] == $modification){
						
							$data = array(
								'SELECTED_CAR_ID'=>$model['id'],
								'NAME'=>$model['car'],
								'YEAR'=>$model['year'],
                                'MODIFICATION'=>$model['modification'],
                                'TYRES_FACTORY' =>$this->parse_tyre_factory($model['tyres_factory']),
                                'TYRES_REPLACE' =>$model['tyres_replace'],
                                'TYRES_FACTORY_VARIANTS'=>$model['tyres_factory']
							);
							unset($models[$key]);
					
					}else{
						$models[$key] = array('NAME'=>$model['car']);
					}
				}
			}else{
				$step = 0;
				foreach ($models as $key => $model) {
					if($model['year'] == $year && $model['car'] == $modelName && $step == 0){
						
							$data = array(
								'SELECTED_CAR_ID'=>$model['id'],
								'NAME'=>$model['car'],
								'YEAR'=>$model['year'],
                                'MODIFICATION'=>$model['modification'],
                                'TYRES_FACTORY' =>$this->parse_tyre_factory($model['tyres_factory']),
                                'TYRES_REPLACE' =>$model['tyres_replace'],
                                'TYRES_FACTORY_VARIANTS'=>$model['tyres_factory']
							);
							unset($models[$key]);
							$step++;
					}else{
						$models[$key] = array('NAME'=>$model['car']);
					}
				}
			}
			
			array_unshift($models, $data);
			return $models;
		}

        // парсинг данных по шинам полученные от модели автомобиля
        function parse_tyre_factory($tyre_data){

            $data = array(
                'SHIRINA_PROFILA'=>[],
                'VISOTA_PROFILA'=>[],
                'DIAMETR'=>[]
            );

            $needle = stripos($tyre_data,"|");
            if($needle){
                
                $tyres_data = explode("|", $tyre_data);
   
                // не меняй  без проверки на тестовой строке
                $var = preg_split('~[^\da-zA-Z]+~', $tyres_data[0]); 
                $data['SHIRINA_PROFILA'][] = $var[0];
                $data['VISOTA_PROFILA'][] = $var[1];
                $data['DIAMETR'][] = substr($var[2], strpos($var[2], 'R')+1);
               

            }else{
                $var = preg_split('~[^\da-zA-Z]+~', $tyre_data);
                $data['SHIRINA_PROFILA'][] = $var[0];
                $data['VISOTA_PROFILA'][] = $var[1];
                $data['DIAMETR'][] = substr($var[2], strpos($var[2], 'R')+1);
            }

            return $data;

        }


        function get_model_names($models){
			$modelNames = array();
			
			foreach ($models as $key => $model) {
				if(!(isset($modelNames[$model['NAME']]))){
					$modelNames[$model['NAME']] = $model['NAME'];
				}
			}

			$data = [];
			foreach ($modelNames as $key => $value) {
					$data[] = $value;
			}
			return $data;
		}

		// получение данных от модели автомобиля
		function get_data_by_model($vendor, $car, $year, $modification){
			$sql = "SELECT * FROM search_by_vehicle 
					WHERE vendor = '" . $vendor . "' 
						AND car = '" . $car . "' 
						AND year = " . $year . " 
						AND modification = '" . $modification . "'";
			
			$result = $this->_query($sql);

			if (isset($result[0]))
			{
				$result[0]['wheels_factory_list'] 	= explode("|", $result[0]['wheels_factory']);
				$result[0]['wheels_replace_list'] 	= explode("|", $result[0]['wheels_replace']);
				$result[0]['wheels_tuning_list'] 	= explode("|", $result[0]['wheels_tuning']);
			}

			return $result[0];
		}
}

// --- функции для работы классом TyreDBVehicleSample (таблицы БД по автомобилям)

function tyreDefaultData(){ // первый производитель по нему устававливаются остальные поля
    $db = new TyreDBVehicleSample();
    return $db->get_default_data();
}

function testTyreDefaultData(){ // первый производитель по нему устававливаются остальные поля
    $db = new TyreDBVehicleSample();
    return $db->test_get_default_data();
}

function tyreVendors(){ // список всех производителей для поля "Марка"
    $db = new TyreDBVehicleSample();
    $arr = $db->get_vendors();
    $vendors = [];
    foreach ($arr as $key => $value) {
          $vendors[] = $value['vendor'];
    }
    return  $vendors;
}
 

function tyreModifications($vendor, $car, $year = null, $modification = null){ // список модификаций
    //var_dump($modification);

    $db = new TyreDBVehicleSample();
    return $db->get_modifications($vendor, $car, $year, $modification);
}

// !!!
function parseTyre($disk_data){
	// var_dump($disk_data);
    $db = new TyreDBVehicleSample();
    return $db->parse_tyre_factory($disk_data); 
}

function tyreAjaxData($strVendor = null, $strModel = null, $strYear = null, $strModif = null){ // получаеам Ajax массив по Марка/Модель/Год/Модификация
    // echo '-- getAjaxData -- <br>';
    $db = new TyreDBVehicleSample();
    return $db->get_ajax_data($strVendor, $strModel, $strYear, $strModif);
}


function tyreYears($strVendor = null, $strModel = null){ // получаеам Ajax массив по Марка/Модель/Год/Модификация
    // echo '-- getAjaxData -- <br>';
    $db = new DiskDBVehicleSample();
    return $db->get_years($strVendor, $strModel, $strYear, $strModif);
}


function _tyreModifications($strVendor, $strModel, $strYear = null, $strModif = null){ 
    // echo '-- getDiskModifications -- <br>';
    // var_dump($strVendor);
    $db = new DiskDBVehicleSample();
    return $db->get_modifications($strVendor, $strModel, $strYear, $strModif);
}
