<?php
// DIAMETR
define('DIAMETR_13', 711);
define('DIAMETR_14', 712);
define('DIAMETR_15', 713);
define('DIAMETR_16', 714);

define('DIAMETR_18', 715);
define('DIAMETR_17', 716);
define('DIAMETR_19', 717);
define('DIAMETR_20', 718);

define('DIAMETR_21', 719);
define('DIAMETR_22', 720);
define('DIAMETR_23', 721);
define('DIAMETR_24', 722);
define('DIAMETR_25', 723);
define('DIAMETR_22_5', 1551);
define('DIAMETR_19_5', 1552);
define('DIAMETR_17_5', 1553);

// SHIRINA_PROFILA
define('SHIRINA_PROFILA_285', 673);
define('SHIRINA_PROFILA_325', 674);
define('SHIRINA_PROFILA_135', 675);
define('SHIRINA_PROFILA_215', 676);
define('SHIRINA_PROFILA_295', 677);
define('SHIRINA_PROFILA_205', 678);

define('SHIRINA_PROFILA_185', 679);
define('SHIRINA_PROFILA_165', 680);
define('SHIRINA_PROFILA_305', 681);
define('SHIRINA_PROFILA_235', 682);
define('SHIRINA_PROFILA_245', 683);
define('SHIRINA_PROFILA_275', 684);

define('SHIRINA_PROFILA_345', 686);
define('SHIRINA_PROFILA_315', 687);
define('SHIRINA_PROFILA_145', 688);
define('SHIRINA_PROFILA_175', 689);
// define('SHIRINA_PROFILA_165', 690);
define('SHIRINA_PROFILA_225', 691);

define('SHIRINA_PROFILA_155', 692);
define('SHIRINA_PROFILA_335', 693);
define('SHIRINA_PROFILA_255', 694);
define('SHIRINA_PROFILA_195', 695);
define('SHIRINA_PROFILA_265', 696);
define('SHIRINA_PROFILA_445', 1554);
define('SHIRINA_PROFILA_385', 1555);


// VISOTA_PROFILA

define('VISOTA_PROFILA_65', 697);
define('VISOTA_PROFILA_85', 698);
define('VISOTA_PROFILA_25', 699);
define('VISOTA_PROFILA_55', 700);
define('VISOTA_PROFILA_50', 701);

define('VISOTA_PROFILA_30', 702);
define('VISOTA_PROFILA_75', 703);
define('VISOTA_PROFILA_35', 704);

define('VISOTA_PROFILA_60', 705);
define('VISOTA_PROFILA_40', 706);
define('VISOTA_PROFILA_70', 707);
define('VISOTA_PROFILA_80', 708);
define('VISOTA_PROFILA_90', 709);
define('VISOTA_PROFILA_45', 710);
define('VISOTA_PROFILA_95', 1560);


// разделы каталога

define('SHINMARKET_CATALOG_NEW', 26);

// Грузовые
define('STANDART', 103);
define('DIAMETER', 104);
define('INCH', 105);
define('INCH_UNSTANDART', 107);


// Легковые
define('LEGKOVIE_STANDART', 108);
define('LEGKOVIE_Z', 109);
define('LEGKOVIE_LT', 111);
define('LEGKOVIE_INCH', 112);
define('LEGKOVIE_NONAME', 113);
define('LEGKOVIE_INCH_STRANGE', 114);
define('LEGKOVIE_LT_INCH', 116);

// Легкогрузовые
define('LEGKOGRUZ_NONAME', 115);
define('LEGKOGRUZ_STANDART', 117);
define('LEGKOGRUZ_INCH', 118);

// Грузовые/STANDART 
// Легковые/LEGKOVIE_STANDART 
// Легковые/LEGKOVIE_Z
// Легковые/LEGKOVIE_LT
// Легкогрузовые/LEGKOGRUZ_STANDART
function upSTANDART($elementID, $data){ 
    CModule::IncludeModule('iblock');

    $diameter = getDiameter($data['diameter']);
    $width = getWidth($data['width']);
    $height = getHeight($data['height']);


    $PROP = array(
      'DIAMETR'=>$diameter,
      'SHIRINA_PROFILA'=>$width,
      'VISOTA_PROFILA'=>$height
    );

    $result = CIBlockElement::SetPropertyValuesEx($elementID, false, $PROP);
    return $elementID;
}

// Грузовые/INCH
function upINCH($elementID, $data){
  CModule::IncludeModule('iblock');
  $diameter = getDiameter($data['diameter']);
 
  $PROP = array(
      'DIAMETR'=>$diameter,
      'WIDTH_INCH'=>$data['width_inch']
    );

  $result = CIBlockElement::SetPropertyValuesEx($elementID, false, $PROP);
  return $elementID;
}


function getDiameter($diameter){

  $result = null;

  switch ($diameter) {
        case 13:
            $result = DIAMETR_13;
            break;
        case 14:
            $result = DIAMETR_14;
            break;
        case 15:
            $result = DIAMETR_15;
            break;

        case 16:
            $result = DIAMETR_16;
            break;

        case 17:
            $result = DIAMETR_17;
            break;

        case 18:
           $result = DIAMETR_18;
            break;

        case 19:
            $result = DIAMETR_19;
            break;

        case 20:
            $result = DIAMETR_20;
            break;

        case 21:
            $result = DIAMETR_21;
            break;

         case 22:
            $result = DIAMETR_22;
            break;

         case 23:
            $result = DIAMETR_23;
            break;

         case 24:
            $result = DIAMETR_24;
            break;

         case 25:
            $result = DIAMETR_25;
            break;

        case 22.5:
            $result = DIAMETR_22_5;
            break;

        case 19.5:
            $result = DIAMETR_19_5;
            break;

        case 17.5:
            $result = DIAMETR_17_5;
            break;
    }

    return $result;
}

function getWidth($width){

    $result = null;

    switch ($width) {
            case 285:
                $result = SHIRINA_PROFILA_285;
                break;

            case 325:
                $result = SHIRINA_PROFILA_325;
                break;
            case 135:
                $result = SHIRINA_PROFILA_135;
                break;

            case 215:
                $result = SHIRINA_PROFILA_215;
                break;

            case 295:
                $result = SHIRINA_PROFILA_295;
                break;

            case 205:
                $result = SHIRINA_PROFILA_205;
                break;

            case 185:
                $result = SHIRINA_PROFILA_185;
                break;

            case 165:
                $result = SHIRINA_PROFILA_165;
                break;

            case 305:
                $result = SHIRINA_PROFILA_305;
                break;


            case 235:
                $result = SHIRINA_PROFILA_235;
                break;

            case 245:
                $result = SHIRINA_PROFILA_245;
                break;


            case 275:
                $result = SHIRINA_PROFILA_275;
                break;

            case 345:
                $result = SHIRINA_PROFILA_345;
                break;

            case 315:
                $result = SHIRINA_PROFILA_315;
                break;

            case 145:
                $result = SHIRINA_PROFILA_145;
                break;

            case 175:
                $result = SHIRINA_PROFILA_175;
                break;

            case 225:
                $result = SHIRINA_PROFILA_225;
                break;

            case 155:
                $result = SHIRINA_PROFILA_155;
                break;
            case 335:
                $result = SHIRINA_PROFILA_335;
                break;

            case 255:
                $result = SHIRINA_PROFILA_255;
                break;

            case 195:
                $result = SHIRINA_PROFILA_195;
                break;
            case 265:
                $result = SHIRINA_PROFILA_265;
                break;

            case 445:
                $result = SHIRINA_PROFILA_445;
                break;

            case 385:
                $result = SHIRINA_PROFILA_385;
                break;
    }

    return $result;

}


function getHeight($height){

      $result = null;

      switch ($height) {


            case 65:
                $result = VISOTA_PROFILA_65;
                break;

            case 85:
                $result = VISOTA_PROFILA_85;
                break;

            case 25:
                $result = VISOTA_PROFILA_25;
                break;

            case 55:
                $result = VISOTA_PROFILA_55;
                break;

            case 50:
                $result = VISOTA_PROFILA_50;
                break;

            case 30:
                $result = VISOTA_PROFILA_30;
                break;

            case 75:
                $result = VISOTA_PROFILA_75;
                break;

            case 35:
                $result = VISOTA_PROFILA_35;
                break;

            case 60:
                $result = VISOTA_PROFILA_60;
                break;

            case 40:
                $result = VISOTA_PROFILA_40;
                break;

            case 70:
                $result = VISOTA_PROFILA_70;
                break;

            case 80:
                $result = VISOTA_PROFILA_80;
                break;

            case 90:
                $result = VISOTA_PROFILA_90;
                break;

            case 45:
                $result = VISOTA_PROFILA_45;
                break;
      }

      return $result;
}  


// --- КАРТИНКИ

function _getElementsData($iblockID, $sectionID, $mask, $gropName = null, $nPageSize = 200, $iNumPage = 1){
      CModule::IncludeModule('iblock');
      $arSort = Array("ID"=>"ASC");
      $arSelect = Array("ID", "NAME");
      if($gropName){
          $searchMask = Array("?NAME"=>"$gropName && $mask");
      }else{
        $searchMask = Array("?NAME"=>$mask);
      }
      

      $arFilter = Array(
          "IBLOCK_ID" => $iblockID ,
          "SECTION_ID"=>$sectionID, 
          $searchMask
        );
      $arNavStartParams = Array(
        "nPageSize" => $nPageSize, 
        "iNumPage"=>$iNumPage
      );
 
      $res =  CIBlockElement::GetList($arSort, $arFilter, false, $arNavStartParams, $arSelect);
      $arr = [];
      
      while($ob = $res->GetNextElement()){
          $arr[] = $ob->GetFields();
      }
      return $arr;
}


function getPictureByMask($mask, $needle = null){


    // BFGoodrich 215/75R17.5 ROUTE CONTROL D 126/124 M M+S/3PMSF
    // BFGoodrich LT33X10.50 R15 114R ALL-TERRAIN KO2 RWL
    $dir = $_SERVER['DOCUMENT_ROOT'].'/upload/Shins/';
    $subfolder = '';

    if($mask == 'CARGO SPEED'){
        $subfolder = 'TIGAR/CARGO/SPEED/';
    }

    if($mask == 'HIGH PERFORMANCE'){
        $subfolder = 'TIGAR/High Performance/';
    }

    if($mask == 'TIGAR PRIMA'){
        $subfolder = 'TIGAR/PRIMA/';
    }

    if($mask == 'SIGURA'){
        $subfolder = 'TIGAR/SIGURA/';
    }

    if($mask == 'SUV SUMMER'){
       $subfolder = 'TIGAR/SUV/Summer/';
    }

    if($mask == 'SYNERIS'){
       $subfolder = 'TIGAR/SYNERIS/';
    }

    if($mask == 'TOURING'){
       $subfolder = 'TIGAR/TOURING/';
    }

    if($mask == 'TOURING'){
       $subfolder = 'TIGAR/TOURING/';
    }
  
    if($mask == 'Ultra High Performance'){
      $subfolder = 'TIGAR/Ultra High Performance/';
    }

    if($mask == 'AGILIS+'){
      $subfolder = 'MICHELIN/Agilis+/';
    }

    if($mask == 'CROSSCLIMATE+'){
      $subfolder = 'MICHELIN/CrossClimate+/';
    }

    if($mask == 'CROSSCLIMATE'){
      $subfolder = 'MICHELIN/CrossClimate/';
    }

    if($mask == 'ENERGY XM2'){
      $subfolder = 'MICHELIN/Energy/XM2/';
    }





    $path = $dir.$subfolder;
    $files = scandir($path);
    $result = array();

    foreach($files as $file){
        if (!in_array($file,array(".", "..")) && !is_dir($path.DIRECTORY_SEPARATOR.$file)) {
            $result[$i]['name'] = $file;
            $result[$i]['path'] = $path;
            $result[$i]['size'] = filesize($path.DIRECTORY_SEPARATOR.$file);
            $i++;
        }
    }

    return $result;
  
}

function setPicture($elementID, $files){
  CModule::IncludeModule('iblock');
  $arLoadProductArray = [];
  $arProp = [];

  foreach ($files as $key => $photo) {
       
        if(file_exists($photo['path'].$photo['name'])){
            if($key == 0){
                $arLoadProductArray['PREVIEW_PICTURE'] = CFile::MakeFileArray($photo['path'].$photo['name']);
                $arLoadProductArray['DETAIL_PICTURE'] = CFile::MakeFileArray($photo['path'].$photo['name']);
            }else{
                $arProp['MORE_PHOTO'][] = array('VALUE' => CFile::MakeFileArray($photo['path'].$photo['name']), 'DESCRIPTION' => '');
            }
        }
  }

  if(!empty($arProp)){
      $arLoadProductArray["PROPERTY_VALUES"] = $arProp;
  }

  $el = new CIBlockElement; 
  return $res = $el->Update($elementID, $arLoadProductArray);

}

function setPictureToGrop($iblockID, $sectionID, $mask, $gropName = null, $printMask = null,  $nPageSize = 200){
  if($printMask){
    return $elements = _getElementsData($iblockID, $sectionID, $mask, $gropName, $nPageSize);
  }else{
   
        $elements = _getElementsData($iblockID, $sectionID, $mask, $gropName, $nPageSize);
        $files = getPictureByMask($mask);
        var_dump($files);
        foreach ($elements as $key => $element) {
          setPicture($element['ID'], $files);
        }
  }


}


function getTyreDataFromName($name, $elementID, $sectionID, $needle = " "){
    
  if($needle){

      if($sectionID == STANDART){ // Грузовые/STANDART
          
          $name = explode(" ", $name);
          $data = [];
          if($name[1]){
              $arr = explode("R", $name[1]);
              $diameter = $arr[1];
              $width_height =  explode("/",$arr[0]);

              $data = array(
                'diameter' => $diameter, 
                'width' => $width_height[0], 
                'height' => $width_height[1], 
                'id' => $elementID
              );
          }

                      echo '<pre>';
          var_dump($diameter);
          echo '</pre>';


          return $data;
      }

      if($sectionID == INCH || $sectionID == INCH_UNSTANDART){ // Грузовые/INCH
          // 'BFGoodrich 8.25R15 ROUTE CONTROL T 143/141 G M+S'
          $name = explode(" ", $name);
          $data = [];
           if($name[1]){
              // var_dump($name);
              $arr = explode("R", $name[1]);
              $diameter = $arr[1];
              // var_dump($diameter);
              $width_inch =  $arr[0];
              var_dump($width_inch);

              $data = array(
                'width_inch' => $arr[0],
                'diameter' =>$diameter,
                'id' => $elementID 
              );
           }

           return $data;
      } 

      if($sectionID == LEGKOVIE_STANDART){ // Легковые/LEGKOVIE_STANDART
          // BFGoodrich 225/45 R17 94V XL G-GRIP
          $name = explode(" ", $name);
          $data = [];
          if($name[1]){

              $arr = explode("R", $name[2]);
              $diameter = $arr[1];
              if(strripos($diameter,'C')){
                $diameter = explode("C", $diameter);
                $diameter = $diameter[0];
              }



              $width_height =  explode("/",$name[1]);
              

              $data = array(
                'diameter' => $diameter, 
                'width' => $width_height[0], 
                'height' => $width_height[1], 
                'id' => $elementID
              );
          }  

          echo '<pre>';
          var_dump($diameter);
          echo '</pre>';

          return $data;
      }

      if($sectionID == LEGKOVIE_Z){ // Легковые/LEGKOVIE_Z
          // Michelin 325/30 Z R19 (105Y) XL PILOT SPORT 4 S
          $name = explode(" ", $name);
          $data = [];
          
          if($name[1]){

              $arr = explode("R", $name[3]);
              $diameter = $arr[1];
              $width_height =  explode("/",$name[1]);

              $data = array(
                'diameter' => $diameter, 
                'width' => $width_height[0], 
                'height' => $width_height[1], 
                'id' => $elementID
              );
          }

            echo '<pre>';
          var_dump($diameter);
          echo '</pre>';


          return $data;

      }

      if($sectionID == LEGKOVIE_LT){ // Легковые/LEGKOVIE_LT
          // BFGoodrich LT215/75 R15 100/97Q MUD TERRAIN KM2 RWL 
          $name = explode(" ", $name);
          $data = [];
          
          if($name[1]){

              $name[1] = substr($name[1], 2);
              $width_height =  explode("/",$name[1]);
              $arr = explode("R", $name[2]);
              $diameter = $arr[1];

              $data = array(
                    'diameter' => $diameter, 
                    'width' => $width_height[0], 
                    'height' => $width_height[1], 
                    'id' => $elementID
              );
          } 

          return $data;
      }

      if($sectionID == LEGKOVIE_INCH){ // Легковые/LEGKOVIE_INCH
          // Tigar 6.50R16 C 108/107L CARGO SPEED
          $name = explode(" ", $name);
          $data = [];


          if($name[1]){
            $arr = explode("R", $name[1]);            
            $data = array(
                'width_inch' => $arr[0],
                'diameter' =>$arr[1],
                'id' => $elementID 
            );
          }

          return $data;
      }

      if($sectionID == LEGKOVIE_LT_INCH){ // Легковые/LEGKOVIE_LT_INCH
        // BFGoodrich LT33X10.50 R15 114R ALL-TERRAIN KO2 RWL
        $name = explode(" ", $name);
        $data = [];
        if($name[1]){
            $arr = explode("X", $name[1]);
            $diameter = explode("R", $name[2])[1];

            $data = array(
              'width_inch' => $arr[1],
              'diameter' =>$diameter,
              'id' => $elementID 
            );
        }

        return $data;

      }


      if($sectionID == LEGKOGRUZ_STANDART){ // Легкогрузовые/LEGKOGRUZ_STANDART
          // Michelin 175/65 R14 C 90/88T AGILIS 51
          $name = explode(" ", $name);
          $data = [];
          if($name[1]){

              $diameter = explode("R", $name[2])[1];
              $width_height =  explode("/",$name[1]);

              $data = array(
                    'diameter' => $diameter, 
                    'width' => $width_height[0], 
                    'height' => $width_height[1], 
                    'id' => $elementID
              );

          }

                    echo '<pre>';
          var_dump($diameter);
          echo '</pre>';

          return $data;
      }

      if($sectionID == LEGKOGRUZ_NONAME){ // Легкогрузовые/LEGKOGRUZ_NONAME
        // 205/75 R16 С НК-131 КАМА EURO
        $name = explode(" ", $name);
        $data = [];

        if($name[1]){
            $width_height =  explode("/",$name[0]);
            $diameter = explode("R", $name[1])[1];

            $data = array(
                    'diameter' => $diameter, 
                    'width' => $width_height[0], 
                    'height' => $width_height[1], 
                    'id' => $elementID
            );
        }

        return $data;
      }
  }



}

function setTyreToGrop($iblockID, $sectionID, $mask, $gropName = null, $printMask = null,  $nPageSize = 200){

        $elements = _getElementsData($iblockID, $sectionID, $mask, $gropName, $nPageSize);
       
        foreach ($elements as $key => $element) {

              $data =  getTyreDataFromName($element['NAME'], $element['ID'], $sectionID, $needle = " ");

              if($printMask){
                   var_dump($data);
                   var_dump($element['ID']);

              }
              else{

                    if($sectionID == STANDART || $sectionID == LEGKOVIE_STANDART|| $sectionID == LEGKOVIE_Z || $sectionID == LEGKOVIE_LT ||  $sectionID == LEGKOGRUZ_STANDART || $sectionID == LEGKOGRUZ_NONAME){
                      $result = upSTANDART($data['id'], $data);
                    }

                    if($sectionID == INCH || $sectionID == LEGKOVIE_INCH || $sectionID == LEGKOVIE_LT_INCH){
                        $result = upINCH($data['id'], $data);
                    }

                    var_dump($element['ID']);
              }

        }

}


// -------- ПРИМЕР РАБОТЫ ПАРСЕРА КАТИНОК и ПАРАМЕТРОВ

// --- ШИНЫ 
// $data =  setTyreToGrop($iblockID = 26, $sectionID = LEGKOVIE_STANDART, $mask = 'CARGO SPEED', $gropName = 'Tigar', $printMask = false);
// $data = setTyreToGrop($iblockID = 26, $sectionID = LEGKOVIE_STANDART, $mask = 'HIGH PERFORMANCE',  $gropName = 'Tigar', $printMask = true);

// $data = setTyreToGrop($iblockID = 26, $sectionID = LEGKOVIE_STANDART, $mask = 'PRIMA',  $gropName = 'Tigar', $printMask = false);

// $data = setTyreToGrop($iblockID = 26, $sectionID = STANDART, $mask = 'ROAD AGILE',  $gropName = 'Tigar', $printMask = false);
// $data = setTyreToGrop($iblockID = 26, $sectionID = LEGKOVIE_STANDART, $mask = 'SIGURA',  $gropName = 'Tigar', $printMask = false);
// $data = setTyreToGrop($iblockID = 26, $sectionID = LEGKOVIE_STANDART, $mask = 'SUV SUMMER',  $gropName = 'Tigar', $printMask = false);
// $data = setTyreToGrop($iblockID = 26, $sectionID = LEGKOVIE_STANDART, $mask = 'SYNERIS',  $gropName = 'Tigar', $printMask = false);
// $data = setTyreToGrop($iblockID = 26, $sectionID = LEGKOVIE_Z, $mask = 'SYNERIS',  $gropName = 'Tigar', $printMask = false);
// $data = setTyreToGrop($iblockID = 26, $sectionID = LEGKOVIE_STANDART, $mask = 'TOURING',  $gropName = 'Tigar', $printMask = false);
// $data = setTyreToGrop($iblockID = 26, $sectionID = LEGKOVIE_STANDART, $mask = 'Ultra High Performance',  $gropName = 'Tigar', $printMask = false);
// $data = setTyreToGrop($iblockID = 26, $sectionID = LEGKOVIE_Z, $mask = 'Ultra High Performance',  $gropName = 'Tigar', $printMask = false);
// $data =  setTyreToGrop($iblockID = 26, $sectionID = LEGKOGRUZ_STANDART, $mask = 'AGILIS+', $gropName = 'Michelin', $printMask = false);
// $data =  setTyreToGrop($iblockID = 26, $sectionID = LEGKOVIE_STANDART, $mask = 'CROSSCLIMATE', $gropName = 'Michelin', $printMask = false);
// $data =  setTyreToGrop($iblockID = 26, $sectionID = LEGKOVIE_STANDART, $mask = 'ENERGY SAVER', $gropName = 'Michelin', $printMask = false);
// $data =  setTyreToGrop($iblockID = 26, $sectionID = LEGKOVIE_STANDART, $mask = 'XM2', $gropName = 'Michelin', $printMask = false);
// if($data){
// 	$i = 0;
// 	foreach ($data as $key => $value) {
// 		   echo '<pre>';
// 		   		var_dump($value);  
// 		   echo '<pre>';   
// 	   $i++;
// 	}
// 	echo "<h2>".$i."</h2>";
// }


// --- КАРТИНКИ


// $data = _getElementsData(SHINMARKET_CATALOG_NEW, LEGKOVIE_STANDART,'CARGO SPEED', 10);
// $data = _getElementsData(SHINMARKET_CATALOG_NEW, LEGKOVIE_STANDART,'CARGO SPEED', 5,1);
// $data = _getElementsData(SHINMARKET_CATALOG_NEW, LEGKOVIE_STANDART,'CARGO SPEED', 5,2);
// foreach ($data as $key => $value) {
//    echo '<pre>';
//    var_dump($value);  
//    echo '<pre>';   
// }

// $data =  setPictureToGrop($iblockID = 26, $sectionID = LEGKOVIE_STANDART, $mask = 'SUV SUMMER', $gropName = 'Tigar', $printMask = true);

// $data =  setPictureToGrop($iblockID = 26, $sectionID = 108, $mask = 'TIGAR PRIMA', $gropName = 'Tigar');

// $data =  setPictureToGrop($iblockID = 26, $sectionID = 108, $mask = 'CARGO SPEED',  $gropName = 'Tigar', $printMask = false);
// $data =  setPictureToGrop($iblockID = 26, $sectionID = 108, $mask = 'HIGH PERFORMANCE',  $gropName = 'Tigar', $printMask = false);
// $data =  setPictureToGrop($iblockID = 26, $sectionID = LEGKOVIE_STANDART, $mask = 'SIGURA', $gropName = 'Tigar', $printMask = false);
// $data =  setPictureToGrop($iblockID = 26, $sectionID = LEGKOVIE_STANDART, $mask = 'SUV SUMMER', $gropName = 'Tigar', $printMask = false);
// $data =  setPictureToGrop($iblockID = 26, $sectionID = LEGKOVIE_STANDART, $mask = 'SYNERIS', $gropName = 'Tigar', $printMask = false);
// $data =  setPictureToGrop($iblockID = 26, $sectionID = LEGKOVIE_STANDART, $mask = 'TOURING', $gropName = 'Tigar', $printMask = false);
// $data =  setPictureToGrop($iblockID = 26, $sectionID = LEGKOVIE_STANDART, $mask = 'Ultra High Performance', $gropName = 'Tigar', $printMask = false);
// $data =  setPictureToGrop($iblockID = 26, $sectionID = LEGKOGRUZ_STANDART, $mask = 'AGILIS+', $gropName = 'Michelin', $printMask = false);
// $data =  setPictureToGrop($iblockID = 26, $sectionID = LEGKOGRUZ_STANDART, $mask = 'AGILIS+', $gropName = 'Michelin', $printMask = false);
// $data =  setPictureToGrop($iblockID = 26, $sectionID = LEGKOVIE_STANDART, $mask = 'CROSSCLIMATE+', $gropName = 'Michelin', $printMask = false);
// $data =  setPictureToGrop($iblockID = 26, $sectionID = 79, $mask = 'CROSSCLIMATE', $gropName = 'Michelin', $printMask = false);
// $data =  setPictureToGrop($iblockID = 26, $sectionID = LEGKOVIE_STANDART, $mask = 'ENERGY SAVER+', $gropName = 'Michelin', $printMask = false);
// $data =  setPictureToGrop($iblockID = 26, $sectionID = LEGKOVIE_STANDART, $mask = 'ENERGY XM2', $gropName = 'Michelin', $printMask = false);
// if($data){
// 	$i = 0;
// 	foreach ($data as $key => $value) {
// 	   echo '<pre>';
// 	   var_dump($value);  
// 	   echo '<pre>';   
// 	   $i++;
// 	}
// 	echo "<h2>".$i."</h2>";
// }







