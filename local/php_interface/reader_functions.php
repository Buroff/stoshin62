<?php

// читаем CSV файл
function getCSV($csv_file){        
  $handle = fopen($_SERVER['DOCUMENT_ROOT'].'/upload/csv/'.$csv_file, "r"); //Открываем csv для чтения 
  $array_line_full = array(); //Массив будет хранить данные из csv

  //Проходим весь csv-файл, и читаем построчно. 3-ий параметр разделитель поля
  while (($line = fgetcsv($handle, 0, ",")) !== FALSE) { 
      $array_line_full[] = $line; //Записываем строчки в массив
  }
  fclose($handle); //Закрываем файл
  unset($array_line_full[0]); // удаляем шапку таблицы
  return $array_line_full; //Возвращаем прочтенные данные
}

// $arrayFile = getCSV('pb-new/Vladimir-PB-8-9.csv'); 
// CreateUpdateSKU($arrayFile, $region, $intSKUIBlock = 12);

function read_disk_data_from_name($name){
    
    $arr_from_name = explode("ГАЗ ", $name);
    if(count($arr_from_name)<2){
        $arr_from_name = explode(") ", $name);
    }

    $params = explode(" ", $arr_from_name[1]);

    if(!empty(stripos($params[0],"CH")) || !empty(stripos($params[0],"H2"))){
        $DIAMETR_OBODA  = substr(substr($params[0], -4),-4,2); 
    }else{
      $DIAMETR_OBODA = substr($params[0],-2,2);
    }

    // var_dump($DIAMETR_OBODA);
    // die();

    $SHIRINA_DISKA = substr($params[0],0,3);
    $VYLET_ET = substr($params[2],2);
    $PCD = $params[1];
    $DIA = substr($params[3],2);

    // var_dump($params[3],2);
    // echo $DIA.' - '.$name."\n";
    // echo $PCD.' - '.$name."\n";

    // символьный код
    $arParams = array("replace_space"=>"_","replace_other"=>"_", 'change_case' => 'U', 'max_len' => 20);
    $characterCode = Cutil::translit($name,"ru",$arParams);

    // свойства
    $arProp = array();
    $arProp['SHIRINA_DISKA'] = SHIRINA_DISKA($SHIRINA_DISKA);
    $arProp['VYLET_ET'] = VYLET_ET($VYLET_ET);
    $arProp['DIAMETR_RASPOLOZHENIYA_OTVERSTIY_PCD'] = PCD($PCD);
    $arProp['DIAMETR_TSENTRALNOGO_OTVERSTIYA'] = DIA($DIA);
    $arProp['DIAMETR_OBODA'] = DIAMETR_OBODA($DIAMETR_OBODA);

    // var_dump($arProp['DIAMETR_OBODA']);
    // exit();

    CModule::IncludeModule("iblock");    
    $el = new CIBlockElement;
    global $USER;
    
    $arLoadProductArray = Array(
        "MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
        "IBLOCK_SECTION_ID" => DISKI_TRASH, // элемент лежит в корне раздела
        "IBLOCK_ID"      =>CATALOG_FINAL,
        "PROPERTY_VALUES"=> $arProp,
        "NAME"           =>$name,
        "ACTIVE"         => "Y",            // активен
        "CODE"=> $characterCode, // символьный код элемента
    );    

    if($PRODUCT_ID = $el->Add($arLoadProductArray)){
      echo $PRODUCT_ID."\n";
    }
    else{
      echo "Error: ".$el->LAST_ERROR."\n";      
    }


}


function DIAMETR_OBODA($DIAMETR_OBODA){
    echo $DIAMETR_OBODA;

    $result = null;
    switch ($DIAMETR_OBODA) {
        case "13":
            $result = 729;
        break;

         case "14":
            $result = 730;
        break;

         case "15":
            $result = 605;
        break;

        case "16":
            $result = 606;
        break;

        case "17":
            $result = 609;
        break;

        default:
          $result = null;
        break;
    }

    return $result;
}

function SHIRINA_DISKA($SHIRINA_DISKA){
    $result = null;
  switch ($SHIRINA_DISKA) {
        case "5,0":
            $result = 717;
            break;

        case "5,5":
          $result = 577;
            break;

        case "6,0":
            $result = 587;
            break;

        case "6,5":
          $result = 718;
            break;

        case "7,0":
          $result = 584;
            break;

        case "7,5":
          $result = 583;
            break;

        default:
          $result = null;
        break;
    }

  return $result;
}


function VYLET_ET ($VYLET_ET){
    $result = null;
    switch ($VYLET_ET) {
        case "29":
          $result = 670;
            break;

        case "32":
          $result = 779;
            break;

         case "33":
          $result = 780;
            break;

        case "35":
          $result = 782;
            break;

        case "38":
        $result = 785;
          break;

        case "40":
         $result = 787;
            break;

        case "41":
          $result = 788;
            break;

        case "43":
          $result = 790;
            break;

        case "47":
          $result = 794;
            break;

         case "48":
          $result = 795;
            break;

        case "49":
            $result = 796;
            break;

         case "50":
         $result = 797;
            break;

        case "51":
          $result = 798;
            break;

        case "62":
            $result = 809;
            break;

        case "105":
            $result = 852;
            break;

        default:
          $result = null;
        break;
    }

    return $result;

}


function PCD($PCD){

     $result = null;

    switch ($PCD) { // 5/139,7

        case "4/98":
          $result = 735;
        break;

        case "4/100":
          $result = 736;
        break;

        case "5/100":
          $result = 739;
        break;

        case "5/108":
          $result = 741;
        break;
      
        case "5/112":
          $result = 743;
        break;

        case "5/139,7":
          $result= 750;
        break;

        case "5/114,3":
          $result = 744;
        break;

        case "4/107,95":
           $result = 898; 
        break;

        case "5/108":
         $result = 741;
        break;

        case "6/130":
          $result = 757;
        break;

        case "5/115":
          $result = 745;
        break;

        default:
          $result = null;
        break;


    }

    return $result;
}

function DIA($DIA){
    
    $result = null;
    switch ($DIA) { // 5/139,7

        case "54,1":
          $result = 899;
        break;
        
        case "56,6":
          $result = 900;
        break;

        case "57,1":
           $result = 620;
        break;

         case "58":
            $result = 901;
        break;

        case "58,5":
           $result = 902;
        break;

        case "60,1":
          $result = 903;
        break;  
        
        case "63,3":
          $result = 904;
        break;

        case "63,35":
          $result = 905;
        break;

        case "66,1":
         $result =  906;
        break;

        case "66,6":
           $result = 629;
        break;

        case "67":
         $result =  908;
        break;

        case "67,1":
           $result = 630;
        break;

        case "69,1":
          $result = 910;
        break;

        case "70,3":
           $result = 911;
        break;

        case "78,1":
           $result = 912;
        break;

        case "84,1":
          $result = 913;
        break;

         case "98,5":
         $result = 914;
        break;

        case "98,6":
           $result = 915;
        break;

        case "130,1":
            $result = 916;
        break;

        case "108":
            $result = 917;
        break;

         case "130":
            $result = 918;
        break;

        default:
          $result = null;
        break;
        
    }

    return $result;
}


