<? global $bRzStoreOne;
$isAjax = !empty($_SERVER['HTTP_X_REQUESTED_WITH']);
if (!is_bool($bRzStoreOne)) {
	$bRzStoreOne = false;
} ?>
<? if (!$bRzStoreOne && !$isAjax): ?>
	<?
	CJSCore::RegisterExt("bs_popover", Array(
		'js' => $templateFolder . '/popover.js',
		'css' => $templateFolder . '/popover.css',
		'rel' => array('jquery'),
		'skip_core' => 'true'
	));
	CJSCore::Init(array("bs_popover"));
	$arParams['WIDTH'] = (empty($arParams['WIDTH'])) ? '450' : $arParams['WIDTH'];
	$arTriggerModes = array('CLICK' => 0, 'HOVER' => 0);
	$arParams['POPUP_TRIGGER'] = (isset($arTriggerModes[$arParams['POPUP_TRIGGER']])) ? $arParams['POPUP_TRIGGER'] : 'CLICK';
	?>
	<script type="text/javascript">
		if (typeof rzStoreOne == 'undefined') {
			rzStoreOne = {
				'AJAX_URL': "<?= $this->__path , '/component.php'?>",
				'URL': "<?=$APPLICATION->GetCurPage(true)?>",
				'WIDTH': "<?= $arParams['WIDTH']?>",
				'TRIGGER': "<?= strtolower($arParams['POPUP_TRIGGER'])?>"
			};
		}
	</script>
	<?
	\Bitrix\Main\Page\Asset::getInstance()->addString('<script src="//api-maps.yandex.ru/2.1.17/?lang=ru_RU" type="text/javascript"></script>',true);
	 $bRzStoreOne = true; ?>
<? endif ?>