<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
if (!CModule::IncludeModule('yenisite.storeamount')) {
	echo GetMessage("RZ_MODULE_NOT_INSTALLED");
	return;
}
if (method_exists($this, 'setFrameMode')) $this->setFrameMode(true); ?>
	<?
	$arParams['COLOR_NONE_BG'] = (strlen($arParams['COLOR_NONE_BG']) > 0) ? $arParams['COLOR_NONE_BG'] : '#C0C0C0';
	$arParams['COLOR_NONE_TEXT'] = (strlen($arParams['COLOR_NONE_TEXT']) > 0) ? $arParams['COLOR_NONE_TEXT'] : '#000000';
	$arParams['COLOR_MANY_BG'] = (strlen($arParams['COLOR_MANY_BG']) > 0) ? $arParams['COLOR_MANY_BG'] : '#328D00';
	$arParams['COLOR_MANY_TEXT'] = (strlen($arParams['COLOR_MANY_TEXT']) > 0) ? $arParams['COLOR_MANY_TEXT'] : '#FFFFFF';
	$arParams['COLOR_AVERAGE_BG'] = (strlen($arParams['COLOR_AVERAGE_BG']) > 0) ? $arParams['COLOR_AVERAGE_BG'] : '#FFA500';
	$arParams['COLOR_AVERAGE_TEXT'] = (strlen($arParams['COLOR_AVERAGE_TEXT']) > 0) ? $arParams['COLOR_AVERAGE_TEXT'] : '#FFFFFF';
	$arParams['COLOR_FEW_BG'] = (strlen($arParams['COLOR_FEW_BG']) > 0) ? $arParams['COLOR_FEW_BG'] : '#FF0000';
	$arParams['COLOR_FEW_TEXT'] = (strlen($arParams['COLOR_FEW_TEXT']) > 0) ? $arParams['COLOR_FEW_TEXT'] : '#FFFFFF';
	?>
	<style type="text/css" xmlns="http://www.w3.org/1999/html">
		.RzStore__indicator .none {
			background: <?= $arParams['COLOR_NONE_BG'] ?>;
			color:      <?= $arParams['COLOR_NONE_TEXT'] ?>;
		}

		.RzStore__indicator.many .many {
			background: <?= $arParams['COLOR_MANY_BG'] ?>;
			color:      <?= $arParams['COLOR_MANY_TEXT'] ?>;
		}

		.RzStore__indicator.average .average {
			background: <?= $arParams['COLOR_AVERAGE_BG'] ?>;
			color:      <?= $arParams['COLOR_AVERAGE_TEXT'] ?>;
		}

		.RzStore__indicator.few .few {
			background: <?= $arParams['COLOR_FEW_BG'] ?>;
			color:      <?= $arParams['COLOR_FEW_TEXT'] ?>;
		}
	</style>
<?

$arParams['MANY_VAL'] = (empty($arParams['MANY_VAL'])) ? 10 : $arParams['MANY_VAL'];
$arParams['AVERAGE_VAL'] = (empty($arParams['AVERAGE_VAL'])) ? 5 : $arParams['AVERAGE_VAL'];

$arParams['MANY_NAME'] = (empty($arParams['MANY_NAME'])) ? GetMessage('MANY_NAME_DEFAULT') : $arParams['MANY_NAME'];
$arParams['AVERAGE_NAME'] = (empty($arParams['AVERAGE_NAME'])) ? GetMessage('AVERAGE_NAME_DEFAULT') : $arParams['AVERAGE_NAME'];
$arParams['FEW_NAME'] = (empty($arParams['FEW_NAME'])) ? GetMessage('FEW_NAME_DEFAULT') : $arParams['FEW_NAME'];
$arParams['NONE_NAME'] = (empty($arParams['NONE_NAME'])) ? GetMessage('NONE_NAME_DEFAULT') : $arParams['NONE_NAME'];

$arParams['IMG_WIDTH'] = (empty($arParams['IMG_WIDTH'])) ? '400' : $arParams['IMG_WIDTH'];
$arParams['IMG_HEIGHT'] = (empty($arParams['IMG_HEIGHT'])) ? '200' : $arParams['IMG_HEIGHT'];

$arParams['VIEW_MODE'] = trim($arParams['VIEW_MODE']);
$arParams['ONE_POPUP'] = ($arParams['ONE_POPUP'] == 'Y') ? 'Y' : 'N';

$arParams['ONLY_NAME_IN_LIST'] = ($arParams['ONLY_NAME_IN_LIST'] == 'Y') ? 'Y' : 'N';
$arParams['HAS_NAME'] = (empty($arParams['HAS_NAME'])) ? GetMessage('HAS_NAME_DEFAULT') : $arParams['HAS_NAME'];
//also uses in component_epilog.php
$arParams['WIDTH'] = (empty($arParams['WIDTH'])) ? '450' : $arParams['WIDTH'];

$arViewModes = array('TEXT' => 0, 'COLS' => 0, 'NUMB' => 0);
if (!isset($arViewModes[$arParams['VIEW_MODE']])) {
	$arParams['VIEW_MODE'] = 'COLS';
}
if(!function_exists('rzSwitchAmount')) {
	function rzSwitchAmount($amount, $arParams) {
		$amount = intval($amount);
		switch (true) {
			case ($amount >= $arParams['MANY_VAL']):
				$class = 'many';
				$title = $arParams['MANY_NAME'];
				break;
			case ($amount >= $arParams['AVERAGE_VAL']):
				$class = 'average';
				$title = $arParams['AVERAGE_NAME'];
				break;
			case (($amount < $arParams['AVERAGE_VAL'] && $amount != 0)):
				$class = 'few';
				$title = $arParams['FEW_NAME'];
				break;
			default:
				$class = 'none';
				$title = $arParams['NONE_NAME'];
		}
		return array($class,$title);
	}
}
$rnd = $this->randString();
if (!empty($arResult["STORES"])): ?>
	<div class="RzStore" id="RzStore_<?= $rnd ?>">
		<? if (strlen($arParams["MAIN_TITLE"]) > 0): ?>
			<div class="title"><?= $arParams["MAIN_TITLE"] ?></div>
		<? endif ?>
		<? //************************    DETAIL VIEW    ************************/?>
		<? foreach ($arResult["STORES"] as $arStore): ?>
			<? if ($arParams['SHOW_EMPTY_STORE'] == 'N' && intval($arStore['AMOUNT']) == 0) continue; ?>
			<?
			if ($arParams['ONE_POPUP'] == 'Y') {
				$arResult['TOTAL_AMOUNT'] += $arStore['AMOUNT'];
				continue;
			} ?>
			<div class="RzStore__item">
				<? if (!empty($arStore['TITLE'])): ?>
					<span class="RzStore__popupUrl" data-title="<?= $arStore['TITLE'] ?>"><?= trim($arStore['TITLE']) ?></span>&nbsp;
				<? endif ?>
				<?
				list($class, $title) = rzSwitchAmount($arStore['AMOUNT'],$arParams);
				?>
				<? // painted columns are set by class on parent: many / average / few ?>
				<? include(__DIR__ . '/view_mode.php') ?>
				<div class="RzStore__popupBody">
					<? if (!empty($arStore['PHONE'])): ?>
						<p><b><?=GetMessage("RZ_TELEFON")?>:</b>&nbsp;<?= $arStore['PHONE'] ?></p>
					<? endif ?>
					<? if (!empty($arStore['SCHEDULE'])): ?>
						<p><b><?=GetMessage("RZ_REZHIM_RABOTI")?>:</b>&nbsp;<?= $arStore['SCHEDULE'] ?></p>
					<? endif ?>
					<? if (!empty($arStore['EMAIL'])): ?>
						<p><b>Email:</b>&nbsp;<a href="mailto:<?= $arStore['EMAIL'] ?>"><?= $arStore['EMAIL'] ?></a></p>
					<? endif ?>
					<?
					if (!empty($arStore['COORDINATES']['GPS_N']) && !empty($arStore['COORDINATES']['GPS_S'])): ?>
						<?
						$mapHeight = '';
						if(intval($arParams['WIDTH']) > 0) {
							$mapHeight = 'height: ' . ceil($arParams['WIDTH'] / 2) . 'px;';
						} else {
							$mapHeight = 'min-height : 100px';
						}
						?>
						<div class="store_map" data-gpsn="<?= $arStore['COORDINATES']['GPS_N']?>" data-gpss="<?= $arStore['COORDINATES']['GPS_S']?>"
							 style="width: 100%; <?=$mapHeight?>">
						</div>
					<? endif ?>
					<? if (intval($arStore['IMAGE_ID']) > 0): ?>
						<div class="RzStore__imgWrapper">
							<img class="img img-responsive" src="<?= CRZ\StoreAmount\Resize::GetResizedImg($arStore['IMAGE_ID'], array('WIDTH' => $arParams['IMG_WIDTH'], 'HEIGHT' => $arParams['IMG_HEIGHT'], 'SET_ID' => intval($arParams['RESIZER_SET']))) ?>"/>
						</div>
					<? endif ?>
					<? if (!empty($arStore['DESCRIPTION'])): ?>
						<p class="help-block"><?= $arStore['DESCRIPTION'] ?></p>
					<? endif ?>
				</div>
			</div>
		<? endforeach ?>
		<? //************************    LIST VIEW    ************************/?>
		<? if ($arParams['ONE_POPUP'] == 'Y'): ?>
			<div class="RzStore__item inline">
				<?
				list($class, $title) = rzSwitchAmount($arResult['TOTAL_AMOUNT'],$arParams);
				$arStore['AMOUNT'] = $arResult['TOTAL_AMOUNT'];
				?>
				<div class="RzStore__popupUrl" data-title="<?= GetMessage('RZ_STORE_QUANTITY') ?>">
					<?
					if($arParams['ONLY_NAME_IN_LIST'] == 'Y') {
						if($arResult['TOTAL_AMOUNT'] > 0) {
							echo '<div class="RzStore__indicator text many"><span class="many">', $arParams['HAS_NAME'], '</span></div>';
						} else {
							echo '<div class="RzStore__indicator text none"><span class="none">', $arParams['NONE_NAME'], '</span></div>';
						}
					} else {
						include(__DIR__ . '/view_mode.php');
					}
					?>
					<div class="RzStore__popupBody">
						<? $bNoStore = true; foreach ($arResult["STORES"] as $arStore): ?>
							<? if ($arParams['SHOW_EMPTY_STORE'] == 'N' && intval($arStore['AMOUNT']) == 0) continue; ?>
							<? list($class, $title) = rzSwitchAmount($arStore['AMOUNT'],$arParams); $bNoStore = false;?>
							<div class="RzStore__popupInline">
								<b><?= $arStore['TITLE'] ?>: </b>
								<? include(__DIR__ . '/view_mode.php') ?>
								<br/>
							</div>
						<? endforeach ?>
						<?if($bNoStore) echo GetMessage("RZ_NET_INFORMATCII_PO_OSTATKAM") ?>
					</div>
				</div>
			</div>
		<? endif ?>
	</div>
<? endif ?>