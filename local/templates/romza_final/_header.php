<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? use Yenisite\Core\Tools;
use Bitrix\Main\Page\Asset;?>
<? include $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/tools/include_module.php' ?>
<?
CRZEventHandlers::SetSiteID(SITE_ID);
CJSCore::RegisterExt('rz_main', array(
	'lang' => SITE_TEMPLATE_PATH.'/lang/'.LANGUAGE_ID.'/javascript.php',
	'skip_core' => true
));
CJSCore::Init(array('rz_main', 'jquery'));
\Bitrix\Main\Localization\Loc::loadMessages(__FILE__);
?>
	<!doctype html>
<html lang="<?=LANGUAGE_ID?>">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=<?=LANG_CHARSET?>">
		<meta name="theme-color" content="#302b37">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<title><?$APPLICATION->ShowTitle()?></title>
		<?
		// setup site name constant from site options
		$dbSite = CSite::GetByID(SITE_ID);
		$arSite = $dbSite->GetNext();
		@define("SITE_NAME",$arSite['NAME']);
		unset($dbSite);unset($arSite);
		global $rz_options, $demoIBlock, $demoPanel;
		?>
		<?ob_start()?>
		<? Tools::IncludeArea('header', 'settings', array(), true) ?>
		<?$panelSettings = ob_get_clean();
		global $USER;
		$Asset = Asset::getInstance();

		$APPLICATION->ShowMeta("robots", false, true);
		$APPLICATION->ShowMeta("keywords", false, true);
		$APPLICATION->ShowMeta("description", false, true);
		$APPLICATION->ShowCSS(true, true);
		$APPLICATION->ShowHeadStrings();
		$APPLICATION->ShowHeadScripts();
		?>
		<link rel="shortcut icon" type="image/x-icon" href="<?= SITE_TEMPLATE_PATH ?>/favicon.ico">
		<?
		$Asset->addString('<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:400,700,900&amp;subset=latin,cyrillic-ext">');
		?>
        <?if ($rz_options['show_stores_in_section'] == 'Y' || $rz_options['show_stores_in_element'] == 'Y'):?>
            <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/js/popover/popover.css')?>
            <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/js/popover/style.css')?>
        <?endif?>
		<?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/fonts/b-rub_arial/stylesheet.css')?>
		<?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/fonts/flaticon/flaticon.css')?>
		<?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/fonts/icons/icons.css')?>
		<?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/fonts/websymbols/websymbols.css')?>
		<?//$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/css/style.min.css?v='.filemtime($_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/css/style.min.css'))?>

		<?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/css/style.min.css?v='.filemtime($_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/css/style.css'))?>


		<? $Asset->addString("<link id='theme-css-link' href='" . SITE_TEMPLATE_PATH .  "/css/themes/".$rz_options['COLOR_SCHEME'] . ".min.css' rel='stylesheet' type='text/css' data-theme='" . $rz_options['COLOR_SCHEME'] . "'>");?>

		<?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/templates_addon.css')?>
		
		<script type="text/javascript">
			<?
            $wishListFile = $_SERVER['DOCUMENT_ROOT'].SITE_DIR.'/ajax/wishList.php';
            $AJAX_DIR = (SITE_DIR == "/")?'/ajax/':SITE_DIR.'/ajax/';
            $AJAX_DIR = str_replace('//','/',$AJAX_DIR);
            ?>
			rz = {
				SITE_ID : '<?=SITE_ID?>',
				SITE_TEMPLATE_PATH  : '<?=SITE_TEMPLATE_PATH?>',
				SITE_DIR: '<?=SITE_DIR?>',
				AJAX_DIR: '<?=$AJAX_DIR?>',
				COOKIE_PREFIX: '<?=COption::GetOptionString("main", "cookie_name", "BITRIX_SM")."_"?>',
				WISH_LIST: <?=CUtil::PhpToJSObject(Array());?>,
				<?if(strlen($arCompareWish['COMPARE']) > 0):?>
				COMPARE_LIST: <?=$arCompareWish['COMPARE']?>,
				<?endif?>
				NOP: ''
			}
		</script>
        <?
        if (!Tools::isEditModeOn()){
            ob_start();
            Tools::IncludeArea('universal','email');
            $mail = ob_get_clean();
        }
        ?>
	</head>
<body>
	<div id="panel"><?$APPLICATION->ShowPanel();?></div>
<?=$panelSettings?>
<?$APPLICATION->IncludeComponent("bitrix:main.include", "", Array("AREA_FILE_SHOW" => "file",	"PATH" => SITE_DIR."include_areas/footer/demoswitcher.php",	"EDIT_TEMPLATE" => "include_areas_template.php"	), false, array("HIDE_ICONS"=>"Y"));?>
<div class="page_wrapper" data-menu-type="<?=$rz_options['switch_main_menu']?>">
	<header class="page_header">
        <?$APPLICATION->IncludeComponent("bitrix:main.include", "", Array("AREA_FILE_SHOW" => "file",	"PATH" => SITE_DIR."include_areas/footer/flashmessage.php",	"EDIT_TEMPLATE" => "include_areas_template.php"	), false);?>
		<div class="page_header__top_line">
			<div class="container">
				<div class="page_header__top_line__inner">
					<div class="column-1">
						<nav class="navbar navbar-default info-navbar" role="navigation">
							<div class="navbar-header">
								<button type="button" class="navbar-toggle flaticon-view2" data-toggle="collapse" data-target=".navbar-ex1-collapse"></button>
							</div>

							<div class="collapse navbar-collapse navbar-ex1-collapse">
								<? Tools::IncludeArea('header', 'menu_header', array(), true) ?>
							</div>
						</nav>
					</div>
					<div class="column-2">
						<div class="btn-group btn-group-auth">
							<?//$APPLICATION->IncludeComponent("bitrix:main.include", "", Array("AREA_FILE_SHOW" => "file",	"PATH" => SITE_DIR."include_areas/site_template/auth.php",	"EDIT_TEMPLATE" => "include_areas_template.php"	), false, array("HIDE_ICONS"=>"Y"));?>
							<?//$APPLICATION->IncludeComponent("bitrix:main.include", "", Array("AREA_FILE_SHOW" => "file",	"PATH" => SITE_DIR."include_areas/site_template/mobile_search.php",	"EDIT_TEMPLATE" => "include_areas_template.php"	), false, array("HIDE_ICONS"=>"Y"));?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="page_header__main">
			<div class="container">
				<div class="page_header__main__inner">
					<div class="column-1">
						<a href="<?=SITE_DIR?>" style="text-decoration: none;">
							<?$APPLICATION->IncludeComponent("bitrix:main.include", "",
								Array("PATH" => "/include_areas/universal/new-logo.php", "AREA_FILE_SHOW" => "file", "EDIT_TEMPLATE" => ""), false);?>
						</a>
						<!-- <a class="logo_wrapper" href="<?=SITE_DIR?>">
							<?
							if(empty($demoIBlock['INCLUDE_PATH'])) {
								$demoIBlock['INCLUDE_PATH'] = 'include_areas/universal/';
							}
							?>
							<?$APPLICATION->IncludeComponent("bitrix:main.include", "",
								Array("PATH" => SITE_DIR.$demoIBlock['INCLUDE_PATH']."main-logo.php", "AREA_FILE_SHOW" => "file", "EDIT_TEMPLATE" => ""), false);?>
						</a>
						<div class="title_wrapper">
							<div class="title_inner">
								<a class="navbar-brand" href="<?=SITE_DIR?>">
									<?$APPLICATION->IncludeComponent("bitrix:main.include", "",
										Array("PATH" => SITE_DIR."include_areas/universal/main-title.php", "AREA_FILE_SHOW" => "file", "EDIT_TEMPLATE" => ""), false);?>
								</a>
							</div>
							<?$APPLICATION->IncludeComponent("bitrix:main.include", "", Array("AREA_FILE_SHOW" => "file",	"PATH" => SITE_DIR."include_areas/site_template/geoip.php",	"EDIT_TEMPLATE" => "include_areas_template.php"	), false, array("HIDE_ICONS"=>"Y"));?>
						</div> -->
					</div>
					<div class="column-2">
						<div class="contacts_block">
							<address>
								<a class="phone" href="#" data-toggle="modal" data-target="#modal-callme">
									<span class="contacts__icon flaticon-tablet5"></span>
									<?$APPLICATION->IncludeComponent("bitrix:main.include", "", Array("PATH" => SITE_DIR."include_areas/universal/phone.php", "AREA_FILE_SHOW" => "file", "EDIT_TEMPLATE" => ""), false);?>
								</a>
							</address>
							<address>
								<a class="phone" href="mailto:<?=$mail?>">
									<span class="contacts__icon flaticon-email20"></span>
									<?$APPLICATION->IncludeComponent("bitrix:main.include", "", Array("PATH" => SITE_DIR."include_areas/universal/email.php", "AREA_FILE_SHOW" => "file", "EDIT_TEMPLATE" => ""), false);?>
								</a>
							</address>
						</div>
						<?$APPLICATION->IncludeComponent("bitrix:main.include", "", Array("AREA_FILE_SHOW" => "file",	"PATH" => SITE_DIR."include_areas/site_template/search.php",	"EDIT_TEMPLATE" => "include_areas_template.php"	), false, array("HIDE_ICONS"=>"Y"));?>
					</div>
					<div class="column-3">
						<? // Tools::IncludeArea('header', 'basket', array(), true) ?>
						<!-- <span class="-js-modal-basket btn btn-primary btn-cart" style="max-width:none; width:80px;  left: 0px;">
                            <span class="-basket-title" style="font-size: 10px; text-align: center;"> 
                            	<a href="/order/" style="color: #fff;text-decoration: none;display:  inline;border:  none;">Под заказ</a>
                            </span>
                        </span> -->

                        <? 
						    $APPLICATION->IncludeComponent("bitrix:sale.basket.basket.line", ".default", Array(
						        "PATH_TO_BASKET" => SITE_DIR."personal/cart/",
						        "PATH_TO_PERSONAL" => SITE_DIR."personal/",
						        "SHOW_PERSONAL_LINK" => "N",
						        "SHOW_NUM_PRODUCTS" => "Y",
						        "SHOW_TOTAL_PRICE" => "Y",
						        "SHOW_PRODUCTS" => "Y",
						        "POSITION_FIXED" => "N",
						        "SHOW_DELAY" => "N",
						        "SHOW_NOTAVAIL" => "N",
						        "SHOW_SUBSCRIBE" => "N",
						        "SHOW_IMAGE" => "N",
						        "SHOW_PRICE" => "N",
						    ),
						        false
						    );

						?>

						<span class="-js-modal-basket btn btn-primary btn-cart" style="max-width: 120px; margin-right: 10px;">
                            <span class="-basket-title" style="text-align: center;"> 
                            	<a href="/order/" style="color: #fff;text-decoration: none;display: block;border:  none;">Под заказ</a>
                            </span>
                        </span>

					</div>
				</div>
			</div>
		</div>
		<div class="page_header__nav">
			<div class="container">
				<?$APPLICATION->ShowViewContent('default_menu')?>
			</div>
		</div>
	</header>
	<div class="page_content_wrap">
<?if($APPLICATION->GetCurPage(false) === SITE_DIR):?>
	<?$APPLICATION->IncludeComponent("bitrix:main.include", "", Array("AREA_FILE_SHOW" => "file",	"PATH" => SITE_DIR."include_areas/site_template/slider_on_main.php",	"EDIT_TEMPLATE" => "include_areas_template.php"	), false, array("HIDE_ICONS"=>"Y"));?>

	<?
	$APPLICATION->IncludeComponent("bitrix:main.include", "", Array("AREA_FILE_SHOW" => "file",	"PATH" => SITE_DIR."include_areas/site_template/filter_on_main.php",	"EDIT_TEMPLATE" => "include_areas_template.php"	), false, array("HIDE_ICONS"=>"Y"));?>
<? endif; ?>
<?Tools::showViewContent('header_container');?>
<?Tools::showViewContent('aside');?>
<?if(!Tools::isAjax()):?>
	<div class="main_content">
<?endif;?>
<?Tools::showViewContent('heading')?>