<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?global $rz_options;
$oneColumn = $APPLICATION->GetProperty('SINGLE_COLUMN','N');
if($oneColumn == "N"):?>
	<div class="page_content container">
<?else:?>
	<div class="page_content container single_column">
<?endif;?>
		<div class="page_content__inner" data-filter="<?=$rz_options['switch_filter']?>">