<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use \Yenisite\Shinmarket\ManagerCalc;
use \Yenisite\Core\Ajax;

$bAjax = Ajax::isAjax();

if (isset($arParams['SECTION_ID']) && intval($arParams['SECTION_ID']) > 0) {
	$dbSection = CIBlockSection::GetByID($arParams['SECTION_ID']);
	$dbSection->SetUrlTemplates();
	$arSection = $dbSection->GetNext();
	$arResult['FORM_ACTION'] = $arSection['SECTION_PAGE_URL'];
}
$arPropsID = array();
foreach ($arResult['ITEMS'] as $key => &$arItem) {
	$arPropsID[$arItem['ID']] = $key;
	if ($arItem['PROPERTY_TYPE'] == "E") {
		foreach ($arItem['VALUES'] as $id => &$arValue) {
			$arValue['ICON_CLASS'] = \Yenisite\Core\Catalog::getIconClassFromPropId($arItem['ID'], $id);
		}
		unset($arValue);
	}
}
unset($arItem);

$dbProp = CIBlockProperty::GetList(array(), array(
	'ACTIVE' => 'Y',
	'IBLOCK_ID' => $arParams['IBLOCK_ID']
));

while ($arProp = $dbProp->Fetch()) {
	if (!isset($arPropsID[$arProp['ID']])) continue;
	if (isset($arProp['HINT']) && strlen($arProp['HINT']) > 0) {
		$decoded = json_decode($arProp['HINT'], true);
		if (isset($decoded)) {
			$arResult['ITEMS'][$arPropsID[$arProp['ID']]]['HINT'] = $decoded;
		}
	}
}

if (CRZShinmarketSettings::isTiersSolution() && ManagerCalc::tablesExist() && !$bAjax){
    $arResult['VENDORS'] = ManagerCalc::processOnlyVendors();
    $arResult['OTHERS_PROPS'] = ManagerCalc::processItemsByVendor($arResult['VENDORS'][0]);
    $arOptionsBDIds = CRZShinmarket::getPropsIDOfTiersWheels();
    CRZShinmarket::setValuesForTXProps($arResult['OTHERS_PROPS']['MODELS'][0],$arOptionsBDIds);
    CRZShinmarket::filterValuesOfFilterByTXProps($arResult,$arOptionsBDIds);
}

foreach($arResult["ITEMS"] as $key=>$arItem){
		if($arItem["CODE"] == 'DIAMETR'){
			unset($arResult["ITEMS"][$key]);
		}
	}

// ДИСКИ
//if($arResult['SECTION']['ID'] == DISKI || $arResult['SECTION']['ID'] == STALNYE_DISKI){
	foreach($arResult["ITEMS"] as $key=>$arItem){
		if($arItem["CODE"] == 'DIAMETR'  || $arItem["CODE"] == 'MANUFACTURER'|| $arItem["CODE"] == 'SHIRINA_PROFILA' && $arItem["CODE"] == 'VISOTA_PROFILA' || $arItem["CODE"] == 'AXLE'){
			// pr($arItem);
			unset($arResult["ITEMS"][$key]);
		}
		
	}
//}	

// ---- 
// foreach($arResult["ITEMS"] as $key=>$arItem){

// 		$TRIM_PARAM_VALUES = array(
// 			'DIAMETR_OBODA' =>'',
// 			'VYLET_ET'=>'',
// 			'DIAMETR_TSENTRALNOGO_OTVERSTIYA'=>'',
// 			'SHIRINA_DISKA'=>''
// 		);

// 		foreach($arItem["VALUES"] as $val => $ar){

// 			if($arItem['CODE']=="DIAMETR_OBODA"){
// 					$needle = strpos($ar["VALUE"], ' ');
// 					$res = substr($ar["VALUE"], 0, $needle);
// 					//var_dump($res);
// 					$TRIM_PARAM_VALUES['DIAMETR_OBODA'] = $res;
// 			}


// 			if($arItem['CODE']=="SHIRINA_DISKA"){
// 					$needle = strpos($ar["VALUE"], '.');
// 					$res = substr($ar["VALUE"], 0, $needle+2);
// 					$TRIM_PARAM_VALUES['SHIRINA_DISKA'] = $res;
// 					// var_dump($res);
// 			}

// 			if($arItem['CODE']=="DIAMETR_TSENTRALNOGO_OTVERSTIYA"){
// 					$needle = strpos($ar["VALUE"], 'м');
// 					$res = trim(substr($ar["VALUE"], 0, $needle-1));
// 					$TRIM_PARAM_VALUES['DIAMETR_TSENTRALNOGO_OTVERSTIYA'] = $res;
// 			}


// 			if($arItem['CODE']=="VYLET_ET"){
// 					$needle = strpos($ar["VALUE"], 'м');
// 					$res = trim(substr($ar["VALUE"], 0, $needle-1));
// 					$TRIM_PARAM_VALUES['VYLET_ET'] = $res;
// 			}
		
			
// 		}
		
// 		// var_dump($TRIM_PARAM_VALUES['DIAMETR_OBODA']);
// 		// $arResult["ITEMS"][$key]["TRIM_PARAM_VALUES"] = 'test';
// 		$arResult["ITEMS"][$key]["TRIM_PARAM_VALUES"] = $TRIM_PARAM_VALUES;


// }

// $arResult["ITEMS"][200]["TRIM_PARAMS"][]
// pr($arResult["ITEMS"][200]["TRIM_PARAM_VALUES"]);
// pr($arResult["ITEMS"]);