(typeof(jQuery) != 'undefined')
&& (function ($) {
	var $diskRangeSlider = $(".js-price-range-slider");
	var elementNums = false;

	// параметры по выбранной модели авто
	var $selectedDiskData = {
		'VYLET_ET':false, // id параметров
		'DIAMETR_OBODA':false,
		'SHIRINA_DISKA':false,
		'PCD':false,
		'DIA':false,
		'VYLET_ET_VALUE':false,  
		'DIAMETR_OBODA_VALUE':false, // значения параметров
		'SHIRINA_DISKA_VALUE':false,
		'PCD_VALUE':false,
		'DIA_VALUE':false
	} 


	$diskRangeSlider.each(function () {
		var $this = $(this),
			$parent = $this.closest('.seach_form__inner_price'),
			$priceTo = $parent.find('.price_to:eq(0)'),
			$priceFrom = $parent.find('.price_from:eq(0)'),
			priceMinVal = Math.floor(parseFloat($priceFrom.data('val'))),
			priceMaxVal = Math.ceil(parseFloat($priceTo.data('val'))),
			priceToVal = $priceTo.val(),
			priceFromVal = $priceFrom.val();
		if (priceToVal.length == 0) {
			priceToVal = priceMaxVal;
		}
		if (priceFromVal.length == 0) {
			priceFromVal = priceMinVal;
		} 
		
		$this.noUiSlider({
			start: [priceFromVal, priceToVal],
			connect: true,
			step: 1,
			range: {
				'min': priceMinVal,
				'max': priceMaxVal
			},
			format: wNumb({
				decimals: 0
			})
		});
		$this.on({
			set: function (e, vals) {
				$priceFrom.val('');
				$priceTo.val('');
			},
			change: function (e, vals) {
				if ($priceFrom.val().length == 0 || $priceTo.val().length == 0) {
					$priceFrom.val(vals[0]);
					$priceTo.val(vals[1]);
					$priceTo.trigger('change');
				}
			}
		});  

		var triggerSliderInput = function (val) {
			var $this = $(this);
			if ($this.val() != val && $this.val().length > 0) {
			$this.val(val);
				if (sliderTrigger != null) {
					clearTimeout(sliderTrigger);
				}
				sliderTrigger = setTimeout(function () {
					$this.trigger('change');
				}, 700);
			}
		};
		$this.Link('lower').to($priceFrom, function (val) {
			triggerSliderInput.bind(this, val);
		});
		$this.Link('upper').to($priceTo, function (val) {

			triggerSliderInput.bind(this, val);
		});

		$this.Link('lower').to('-inline-<div class="tooltip"></div>', function (value) {
			$(this).html('<span class="tooltip_inner">' + value + '</span>');
		});

		$this.Link('upper').to('-inline-<div class="tooltip"></div>', function (value) {
			$(this).html('<span class="tooltip_inner">' + value + '</span>');
		});
	});

	var $diskSectionFilters = $('.filterHorizontal');
	
	$diskSectionFilters.on('change', 'input', function (e) {
		// console.log('--- sectionFilters ---');
		var form_id = $(this).closest('form.filterHorizontal').attr('id');
		// console.log(form_id);

		if(form_id == 'search_disks'){
			
			var $diskSectionFilter = $(this).closest('form.filterHorizontal');
			var rnd = $diskSectionFilter.data('rnd');
			var data = $diskSectionFilter.serializeArray();

			data.push({name: 'arParams', value: $diskSectionFilter.data('arparams')});
			$diskSectionFilter.setAjaxLoading();
			
			$.ajax({
				// url: rz.AJAX_DIR + "catalog_Filter.php",
				url: rz.AJAX_DIR + "catalog_Filter_alex.php",
				data: data,
				dataType: "json",
				success: function (json) {
					rz.FILTER_AJAX = [];

					let disks_url_filter = json.FILTER_URL.substring(json.FILTER_URL.indexOf('set_filter'));
					$('#disks_url_filter').text(tires_url_filter);
					// console.log(disks_url_filter);

					$.each(json.ITEMS, function (key, val) {
						$.each(val.VALUES, function (valKey, valVal) {
							diskAjaxValuePrepare(valVal, rnd);
						});
					});

					$diskSectionFilter.stopAjaxLoading();
					$(document).trigger('catalog_applyFilter');

					// 
				}
			});

		}

	});

	// сброс значения фильтров
	$diskSectionFilters.on('click', '.form_reset_button', function (e) {

		e.preventDefault();
		var $diskSectionFilter = $(this).closest('form.filterHorizontal');
		$diskSectionFilter[0].reset();
		var rnd = $diskSectionFilter.data('rnd');
		var $jsSlider = $diskSectionFilter.find('.js-price-range-slider');
		var priceToVal = $diskSectionFilter.find('.price_to:eq(0)').val(),
			priceFromVal = $diskSectionFilter.find('.price_from:eq(0)').val();
		$jsSlider.val([priceFromVal,priceToVal]);
		$diskSectionFilter.find('input:visible:eq(0)').trigger('change');
		
		resetDiskFilter();

	});

	var diskAjaxValuePrepare = function (val, rnd) {
		var $curItem = $('#' + rnd + val.CONTROL_ID);
		var $itemStyler = $('#' + rnd + val.CONTROL_ID + "-styler");
		var lastsymb = val.CONTROL_NAME.substr(-3);
		var isNum = false;
		if (lastsymb == 'MAX' || lastsymb == "MIN") {
			isNum = true;
		}
		if ('DISABLED' in val && val.DISABLED == 1) {
			if ($itemStyler.length > 0) {
				$itemStyler.addClass('disabled');
			}
			$curItem.attr('disabled', 'disabled');
		} else {
			if ($itemStyler.length > 0) {
				$itemStyler.removeClass('disabled');
			}
			$curItem.removeAttr('disabled');
		}
	};


	// --- событие при изменение данных в списке выбора автомобиля
	var disks_selects = {
		'mark': '[name="disks_makr_filter"]', 
		'model': '[name="disks_model_filter"]',
		'year':'[name="disks_year_filter"]',
		'modif':'[name="disks_modif_filter"]'
	};
	var vendor = false, model = false, year = false, modif = false;
			
	// var $diskdiskSectionFilters = $('.filterHorizontal');
    $.each(disks_selects, function (key, val) {
        disks_selects[key] = $diskSectionFilters.find(val);
    });
    
	disks_selects['mark'].on('change', function () {
        // var vendor = $(this).find('option:selected').text();
        // console.log('--- change ---');
        // console.log(vendor);
        // diskAjax('by_vendor',$(this),vendor);

        vendor = $(this).find('option:selected').text();
        setFilterAjax('by_vendor', $(this), vendor);
    });

    disks_selects['model'].on('change', function () {
        // var $this = $(this),
        //     vendor = $this.getSelOptFromForm(disks_selects['mark']).text(),
        //     model = $this.find('option:selected').text();
        // diskAjax('by_vendor_model',$(this),vendor,model);
        var $this = $(this);
            vendor = $this.getSelOptFromForm(disks_selects['mark']).text();
            model = $this.find('option:selected').text();
        setFilterAjax('by_vendor_model',$(this),vendor,model);
    });

    disks_selects['year'].on('change', function () {
        // var $this = $(this),
        //     vendor = $this.getSelOptFromForm(disks_selects['mark']).text(),
        //     model = $this.getSelOptFromForm(disks_selects['model']).text(),
        //     year = $this.find('option:selected').text();
        //diskAjax('by_vendor_model_year',$(this),vendor,model,year);
		var $this = $(this);
            vendor = $this.getSelOptFromForm(disks_selects['mark']).text();
            model = $this.getSelOptFromForm(disks_selects['model']).text();
            year = $this.find('option:selected').text();
        setFilterAjax('by_vendor_model_year',$(this),vendor,model,year);
    });

    disks_selects['modif'].on('change', function () {
        // var $this = $(this),
        //     vendor = $this.getSelOptFromForm(disks_selects['mark']).text(),
        //     model = $this.getSelOptFromForm(disks_selects['model']).text(),
        //     year = $this.getSelOptFromForm(disks_selects['year']).text(),
        //     modif = $this.find('option:selected').text();
        // diskAjax('by_vendor_model_year_modif',$(this),vendor,model,year,modif);
        var $this = $(this);
            vendor = $this.getSelOptFromForm(disks_selects['mark']).text();
            model = $this.getSelOptFromForm(disks_selects['model']).text();
            year = $this.getSelOptFromForm(disks_selects['year']).text();
            modif = $this.find('option:selected').text();
        setFilterAjax('by_vendor_model_year_modif',$(this),vendor,model,year,modif);
    });


    var initdDiskFilter = function (event){
    	console.log('-- initdDiskFilter --');

	    if(vendor !== false && model === false && year == false && modif == false){
	    	console.log(vendor);

	    	let mode = true, _this = false;
	    	setFilterAjax('by_vendor', _this, vendor, model, year, modif, mode);
	    }


	    if(vendor !== false && model !== false && year == false && modif == false){
	    	console.log(vendor);
	    	console.log(model);

	    	let mode = true, _this = false;
	    	setFilterAjax('by_vendor_model', _this, vendor, model, year, modif, mode);
	    }

	    if(vendor !== false && model !== false && year !== false && modif == false){
	    	console.log(vendor);
	    	console.log(model);
	    	console.log(year);

	    	let mode = true, _this = false;
	    	setFilterAjax('by_vendor_model_year', _this, vendor, model, year, modif, mode);
	    }


	    if(vendor !== false && model !== false && year !== false && modif !== false){
	    	console.log(vendor);
	    	console.log(model);
	    	console.log(year);
	    	console.log(modif);

	    	let mode = true, _this = false;
	    	setFilterAjax('by_vendor_model_year_modif', _this, vendor, model, year, modif, mode);
	    }


    }


    // --- устаналиваем значения в HTML
	var _setHtmlOptions = function(elements,apendTo){
		// console.log('--- _setHtmlOptions ---');
	    var options = '';
	    if (typeof elements != 'undefined'){
	        $.each(elements,function(index,value){
	        	// console.log(value);
	            if (value) {
	                options += '<option value="' + value + '">' + value + '</option>'
	            }
	        });
	        apendTo.children().remove();
	        apendTo.append($(options));
	    }
	};

	var _getModels = function(elements){
		// console.log(elements);
	    var models = [];
	    $.each(elements,function(){
	        if (this.NAME){
	            models.push(this.NAME);
	        }
	    });
	    return models;
	};

	// ищем элементы по параметрам из БД автомобилей
	var findDiskElements = function(model, type){
	    	console.log('--- findDiskElements ---');
	    	// console.log(model);
	    	// console.log(type);

	    	if(type == 'disky'){
	    		var elems = $('#disks_by_param input');
	    	}
	    	// console.log(elems);
	    	// console.log(elems.length);
	    	var i = 0;


			$.each(elems, function(index, value){
					// console.log("INDEX: " + index + "\n");
					// console.log(value);

					let element = $('#'+value.id + '-styler').siblings('.text');
					let typeCheckbox = false;
					switch (element.data('property')){
						case 'VYLET_ET':
							let $resVyletET = setDiskCheckbox(model.WHEELS_FACTORY.VYLET_ET, element.data('trimparamvalue'), value.id, element.data('property'));
							if($resVyletET !== false){
								$selectedDiskData.VYLET_ET = $resVyletET;
								// $selectedDiskData.VYLET_ET_VALUE = model.WHEELS_FACTORY.VYLET_ET;
								$selectedDiskData.VYLET_ET_VALUE = element.data('trimparamvalue');
							}
						break;

						case 'DIAMETR_OBODA':
							let $resDiametrOboda =  setDiskCheckbox(model.WHEELS_FACTORY.DIAMETR_OBODA, element.data('trimparamvalue'), value.id, element.data('property'));
							if($resDiametrOboda !== false){
								$selectedDiskData.DIAMETR_OBODA = $resDiametrOboda;
								// $selectedDiskData.DIAMETR_OBODA_VALUE = model.WHEELS_FACTORY.DIAMETR_OBODA;
								$selectedDiskData.DIAMETR_OBODA_VALUE = element.data('trimparamvalue');

							}
						break;

						case 'SHIRINA_DISKA': // Ширина (обода)
							let $resShirinaDiska = setDiskCheckbox(model.WHEELS_FACTORY.SHIRINA_DISKA, element.data('trimparamvalue'), value.id, element.data('property'));
				
							if($resShirinaDiska !== false){
								$selectedDiskData.SHIRINA_DISKA = $resShirinaDiska;
								// $selectedDiskData.SHIRINA_DISKA_VALUE = model.WHEELS_FACTORY.SHIRINA_DISKA;
								$selectedDiskData.SHIRINA_DISKA_VALUE = element.data('trimparamvalue');
							}

						break;

						case 'DIAMETR_RASPOLOZHENIYA_OTVERSTIY_PCD':  // PCD
							let $resPCD = setPCDCheckbox(model.DIAMETR_RASPOLOZHENIYA_OTVERSTIY_PCD, element.text(), value.id);
							if($resPCD !== false){
								$selectedDiskData.PCD = $resPCD;
								// $selectedDiskData.PCD_VALUE = model.DIAMETR_RASPOLOZHENIYA_OTVERSTIY_PCD;
								$selectedDiskData.PCD_VALUE = element.data('trimparamvalue');
							}

						break;

						case 'DIAMETR_TSENTRALNOGO_OTVERSTIYA':  // DIA
							let $resDIA = setDIACheckbox(model.DIAMETR_TSENTRALNOGO_OTVERSTIYA, element.text(), value.id);
							if($resDIA !== false){
								$selectedDiskData.DIA = $resDIA;
								// $selectedDiskData.DIA_VALUE = model.DIAMETR_TSENTRALNOGO_OTVERSTIYA;
								$selectedDiskData.DIA_VALUE = element.data('trimparamvalue');
							}
						break;

					}
				
				i++;

				if(i == elems.length){
					elementNums = true;
				}

				
			});
	}

	var setPCDCheckbox = function(param_pcd, trimparamvalue, id){
	    	// console.log('--- setPCDCheckbox ---');
	    	// console.log(trimparamvalue);
	    	// console.log(param_pcd);
	    	let res = false;
	    	if(param_pcd == trimparamvalue){
	    		$('#'+id).prop('checked', true);
	         	$('#'+id+'-styler').addClass('checked');

	         	res = id;
	    	}
	    	// console.log(res)
	    	return res;
	}


	var setDIACheckbox = function(param_dia, trimparamvalue, id){
	    	// console.log('--- setDIACheckbox ---');
	    	// console.log(trimparamvalue);
	    	// console.log(param_dia);
	    	let res = false;
	    	if(param_dia == trimparamvalue){
	    		// console.log('--- value == text ---');
	    		$('#'+id).prop('checked', true);
	         	$('#'+id+'-styler').addClass('checked');
	         	res = id;
	    	}

	    	 return res;
	}

	// устанавливаем чекбоксы
	var setDiskCheckbox = function(data, trimparamvalue, id, typeCheckbox){

			// console.log('--- setDiskCheckbox ---');
			// console.log(data);
			// console.log(trimparamvalue);
			// console.log(id);
			// console.log(typeCheckbox);

			let res = false;

			data.forEach(function (value, i) {

				if(typeCheckbox == 'SHIRINA_DISKA'){
						value = value + ' "';
						trimparamvalue = String(trimparamvalue);
						trimparamvalue = trimparamvalue + ' "';
				}

					
	         	if(value == trimparamvalue){

	         		// if(typeCheckbox == 'SHIRINA_DISKA'){
		         	// 	console.log('--- value == text ---');
		         	// 	console.log(value);
		         	// 	console.log(id);

		         	// }

	         	 	$('#'+id).prop('checked', true);
	         	 	$('#'+id+'-styler').addClass('checked');
	         	 	res = id;
	         	}

	        });

	        return res;
	}

	var resetDiskFilter = function (){
		console.log('--- resetDiskFilter ---');

			elementNums = false;

		    $selectedDiskData.VYLET_ET = false;
		    $selectedDiskData.DIAMETR_OBODA = false;
		    $selectedDiskData.SHIRINA_DISKA = false;
		    $selectedDiskData.PCD = false;
		    $selectedDiskData.DIA = false;

		    $selectedDiskData.VYLET_ET_VALUE = false;
		    $selectedDiskData.DIAMETR_OBODA_VALUE = false;
		    $selectedDiskData.SHIRINA_DISKA_VALUE = false;
		    $selectedDiskData.PCD_VALUE = false;
		    $selectedDiskData.DIA_VALUE = false;

	    	// $('#set_disk_filter').css({'display':'block'});
	    	// $('#set_fake_disk_filter').css({'display':'none'});

	    	$('#print_not_found').empty();
			$('#print_not_found').css({'display':'none'});

			$('#weel_replace').empty();
			$('#weel_replace').css({'display':'none'});

				$('#print_found').empty();
			$('#print_found').css({'display':'none'});

			skipSelectedAllDisk(); 

			console.log('--- / resetDiskFilter ---');
	}


	// --- setFilterAjax
	var setFilterAjax = function(action, $this = false, strVendor, strModel, strYear, strModif, mode = false){
		console.log('--- setFilterAjax ---');
		// console.log($this);
		// let $container_ = $this.closest('.filterHorizontal');
		// console.log($container_.attr('id'));
		// console.log($container_);
		
		//if(mode){
			resetDiskFilter(); 
		// }
		
		// тип шины или диски
	    var type = $('#type_disks').data('type');

	    var data = {}, 
	    // $container = $this.closest('.filterHorizontal'),
	    $container = $('#search_disks'),
	    bHasInput = $container.find('[name="car-id"]').length ? true : false,
	    $inputCar = bHasInput ? $container.find('[name="car-id"]') : $('<input type="hidden" name="car-id"/>');

		if (typeof strVendor != 'undefined' || strVendor !== false){
		    data['vendor'] = strVendor;
		}
		if (typeof strModel != 'undefined'|| strModel !== false){
		    data['model'] = strModel;
		}
		
		if (typeof strYear != 'undefined'|| strYear !== false){
		    data['year'] = strYear;
		}
		
		if (typeof strModif != 'undefined'|| strModif !== false){
		    data['modif'] = strModif;
		}

	    data['action_get_info'] = action;
	    data['type'] = type;
	        
	    $container.setAjaxLoading();


	    // получаем данные по дискам (ширина, диаметр обода, PCD, DIA, вылет) по выбранному автомобилю 
	        $.ajax({
	        	url: rz.AJAX_DIR + '/car_filter/car_ajax.php',
	        	data:data,
	            dataType:'json',
	            method: 'POST',
	            success: function(result){


		            // var modif,years,carID,models,objData;
		            var car, models, years, modif;
			            	models = result.data_base.MODELS;
			            	modif = result.data_base.MODIFICATIONS;
			            	years = result.data_base.YEARS;

		            // выводим данные в HTML
				    _setHtmlOptions(models,disks_selects['model']);
				    _setHtmlOptions(modif,disks_selects['modif']);
				    _setHtmlOptions(years,disks_selects['year']);

				    // console.log(result.data_base.WHEELS_FACTORY_VARIANTS);


			        if(mode == false){

			        		console.log('--- Меняем значения фильтра ---');

				            	let url  = false;
						        buttonHide(url);
								$container.find('select').trigger('refresh');
					            $container.stopAjaxLoading();
					            $container.find('input').eq(0).trigger('change');
			        }else{

				        	console.log('--- Получаем товары по сочетанию ---');
				      		console.log(result.data_base.MODEL);
						    // console.log(result.data_base.WHEELS_REPLACE);
				        	// console.log($selectedDiskData);

				        	// выставляем новые значения
		                	findDiskElements(result.data_base.MODEL, type); // ищем в фильрах совпадения по параметрам
				        
		                	// если товаров по сочетанию не найдено
					        if(result.data_base.MODEL.ROWS == 0){
		     		        	printNOTfound(result.data_base.MODEL.VALID, $selectedDiskData);
							    // let location = 'catalog/diski/?&arrFilter_197_339036633=Y&arrFilter_198_1608842955=Y&arrFilter_199_34404265=Y&arrFilter_200_219485612=Y&arrFilter_201_1761665292=Y&set_filter=Y';
								// let url = buildURL($selectedDiskData);
								// console.log(url);
								// buttonHide(url);

		     		        }else{ 
		     		        	// let url = buildURL($selectedDiskData);
								// console.log(url);
		     		        	// printFound(result.data_base.MODEL.VALID, url);
		     		        	// printWheels(result.data_base.WHEELS_FACTORY_VARIANTS);

		     		        }

							// console.log('--- WHEELS_FACTORY_VARIANTS ---');
							// console.log(result.data_base.WHEELS_FACTORY_VARIANTS);

							printWheels(result.data_base.WHEELS_FACTORY_VARIANTS);


		     		        //  ссылки по замене
		     		        if(result.data_base.WHEELS_REPLACE !== false){
		     		        	printReplace(result.data_base.WHEELS_REPLACE);
		     		        }

			                if(elementNums){
				                $container.find('select').trigger('refresh');
				                $container.stopAjaxLoading();
				                $container.find('input').eq(0).trigger('change');
				                
				                // снимаем неактивность с кнопки Подобрать
				                setTimeout(function(){
									  $( "#set_disks_filter" ).prop( "disabled", false);
								}, 3000);
			                }

			        }

	               
	            }

	        });

	}

	

var printNOTfound = function(valid, selectedData){
	console.log('-- printNOTfound --');
	let combination = '', params = '', html  = '', errors = false;

	for(key in valid){
				// console.log(valid[key]);
				//  console.log(key);
			switch (key) {

			  	case "DIA":
				    	console.log('DIA:' + valid[key].toString());
				    	combination = combination + '<li>' + ' DIA : ' + valid[key].toString() + '</li>';
				    	if(selectedData['DIA_VALUE'] == false){
				    	 	params = params + '<li>' + 'DIA: '+ valid[key].toString() +'</li>';
				    	 	errors = true;
				    	}
			    break;


			    case "PCD":
				    	 console.log('PCD:' + valid[key].toString());
				    	 combination = combination + '<li>' + ' PCD:' + valid[key].toString() + '</li>';
				    	if(selectedData['PCD_VALUE'] == false){
				    	 	params = params + '<li>' + 'PCD: '+ valid[key].toString() +'</li>';
				    	 	errors = true;
				    	}
			    break;

				
				case "DIAMETR_OBODA":
						console.log('Диаметр (обода):' + valid[key].toString());
						combination = combination + '<li>' + ' Диаметр (обода):' + valid[key].toString() + '</li>';
						if(selectedData['DIAMETR_OBODA_VALUE'] == false){
				    	 	params = params + '<li>' + 'Диаметр (обода): '+ valid[key].toString() +'</li>';
				    	 	errors = true;
				    	}
				break;


				case "SHIRINA_DISKA":
						console.log('Ширина (обода):' + valid[key].toString());
						combination = combination + '<li>' + ' Ширина (обода):' + valid[key].toString() + '</li>';
						if(selectedData['SHIRINA_DISKA_VALUE'] == false){
				    	 	params = params + '<li>' + 'Ширина (обода): '+ valid[key].toString() +'</li>';
				    	 	errors = true;
				    	}
				break;


				case "VYLET_ET":
						console.log('Вылет (ET):' + valid[key].toString());
						combination = combination + '<li>' + ' Вылет (ET):' + valid[key].toString() + '</li>';
						if(selectedData['VYLET_ET_VALUE'] == false){
				    	 	params = params + '<li>' + 'Вылет (ET): '+ valid[key].toString() +'</li>';
				    	 	errors = true;
				    	}
				break;

			}


	}

	// если один из параметров отсутсвует
	if(errors == true){
			// if(combination !==''){
			// 	combination = '<p>Извините, у нас нет товаров с выбранным вами combination: <br/>' + combination;
			// }

			if(params !==''){
				// params = '<b>Извините, в нашем магазине пока нет товаров c выбранным вами сочетанием параметров: </b>' + '<ul style="list-style-type:none;">' + params + '</ul>';
				params = '<b>Извините, в нашем магазине пока нет товаров c выбранным вами сочетанием параметров! </b><br/><br/>';
			}
			// html = params + ' <b>попробуйте изменить критерии поиска</b>';
			html = params;
			$('#print_not_found').append(html);
			$('#print_not_found').css({'display':'block'});
	}

}

var printReplace = function(weels_replace){
	console.log('-- printReplace --');


	let html = '<b>Подходящие диски (замена): </b>';
		html = html + '<ul style="list-style-type:none;">';
		weels_replace.forEach(function(element) {
			if(element['HREF'] !== false){
				html = html + '<li style="margin:5px 0 0 -10%;"><a href="'+ element.HREF +'">'+ element.NAME + '</a></li>';

			}else{
				html = html + '<li style="margin:5px 0 0 -10%;">'+ element.NAME + '</li>';

			}
		});
		html = html + '</ul>';
		$('#weel_replace').append(html);
			$('#weel_replace').css({'display':'block'});
	// console.log(html);
}


var printFound = function(valid, url){
	let html = '<b>По вашему запросу найдены товары!</b><br/>';
	html = html + 'перейдите пожалуйста в каталог нажав на ссылку';
		html = html + '<ul style="list-style-type:none;">';
		html = html + '<li style="margin:5px 0 0 -10%;"><a href="'+ url.location +'">Товары</a></li>';
		html = html + '</ul>';
	$('#print_found').append(html);
			$('#print_found').css({'display':'block'});

}

var printWheels = function(wheels_factory_variants){
		console.log('--- printWeels ---');
		console.log(wheels_factory_variants);

		let html = '<b>Для вашего автомобиля подходят диски с параметрами:</b>';
		html = html + '<ul style="list-style-type:none;">';
		wheels_factory_variants.forEach(function(element) {
			if(element['HREF'] !== false){
				html = html + '<li style="margin:5px 0 0 -10%;"><a href="'+ element.HREF +'">'+ element.NAME + '</a></li>';

			}else{
				html = html + '<li style="margin:5px 0 0 -10%;">'+ element.NAME + '</li>';

			}
		});
		html = html + '</ul>';

	$('#print_found').append(html);
	$('#print_found').css({'display':'block'});

}

var buildURL = function($diskData){
		// console.log('--- buildURL ---');

		let enable = true;

		// параметры для GET запроса в каталог товаров
		let diametrObodaString, shirinaDiskaString, viletString, PCDString, DIAString;

		if($diskData.DIAMETR_OBODA !== false){
			diametrObodaString = makeString($diskData.DIAMETR_OBODA, 'arrFilter');
			diametrObodaString = diametrObodaString+'=Y&';
		}else{
			diametrObodaString = 'diametrObodaString'+ '=Y&';
			enable = false;
			
		}

		if($diskData.SHIRINA_DISKA !== false){
			shirinaDiskaString = makeString($diskData.SHIRINA_DISKA, 'arrFilter');
			shirinaDiskaString = shirinaDiskaString + '=Y&';
		}else{
			shirinaDiskaString = 'shirinaDiskaString'+ '=Y&';
			enable = false;
			
		}

		if($diskData.VYLET_ET !== false){
			viletString = makeString($diskData.VYLET_ET, 'arrFilter');
			viletString = viletString + '=Y&';
		}else{
			viletString = 'viletString'+ '=Y&';
			enable = false;
			
		}

		if($diskData.PCD !== false){
			PCDString = makeString($diskData.PCD, 'arrFilter');
			PCDString = PCDString + '=Y&';
		}else{
			PCDString = 'PCDString' + '=Y&';
			enable = false;
			
		}

		if($diskData.DIA !== false){
			DIAString = makeString($diskData.DIA, 'arrFilter');
			DIAString = DIAString + '=Y&';
		}else{
			DIAString = 'DIAString'+ '=Y&';
			enable = false;

		}

		let action = $("#search_disks").attr('action'); // 
		let location = action + '?clear_cache=Y&' + diametrObodaString + shirinaDiskaString + viletString + DIAString + PCDString + 'disks_makr_filter=2&set_filter=Y';
		
		let url = {
			'enable':enable,
			'location':location
		}
		return url;
	}

	var buttonHide = function(url = false){
		console.log('--- buttonHide ---');
		if(url == false){
			$('#set_disk_filter').css({'display':'none'});
			$('#set_fake_disk_filter').css({'display':'none'});
		}else{
			// $('#set_disk_filter').css({'display':'none'});

			// if(url.enable == true){
			// 	$('#set_fake_disk_filter').attr('href', url.location);
	  //   		$('#set_fake_disk_filter').css({'display':'block'});
			// }
		}
		

	}

	var makeString = function($string, $needle){
		let array = $string.split($needle);
		let output = $needle + array[1];
		return output;
	}


	// кнопка убрать фильтр (чтобы надпись менялась)
	$('.collapse_btn_wrapper').on('click', function(event){
		$('.seach_form__submit').find(".btn-inner").text(function(i, text){
		          return text === "ПОКАЗАТЬ ФИЛЬТР" ? "СКРЫТЬ ФИЛЬТР" : "ПОКАЗАТЬ ФИЛЬТР";
		 });
	});



var skipSelectedAllDisk = function(){
 	// console.log('--- skipSelectedAllDisk ---');
 	var elems = $('#disks_by_param input');
  	$.each(elems, function(index, value){
			// console.log(value.id);
			$('#'+value.id).prop('checked', false);
			$('#'+value.id+'-styler').removeClass('checked');
 	});
}


// сбрасываем значение при переключении на вкладку Подбор по автомобилю
$("a[href='#disks_by_car']").on('click', function () {
	// console.log('--- disks_by_car ---')
	$('#disks [name="disks_makr_filter"]').trigger("change");
});


$("a[href='#disks_by_param']").on('click', function (e) {
	
	// e.preventDefault();
	console.log('--- #disks_by_param ---');
		var $diskSectionFilter = $(this).closest('form.filterHorizontal');
		console.log($diskSectionFilter);
		console.log($diskSectionFilter[0]);
		
		$diskSectionFilter[0].reset();
		// var rnd = $diskSectionFilter.data('rnd');
		// var $jsSlider = $diskSectionFilter.find('.js-price-range-slider');
		// var priceToVal = $diskSectionFilter.find('.price_to:eq(0)').val(),
		// priceFromVal = $diskSectionFilter.find('.price_from:eq(0)').val();
		// $jsSlider.val([priceFromVal,priceToVal]);
		// $diskSectionFilter.find('input:visible:eq(0)').trigger('change');
		
		resetDiskFilter();
});

// --- TEST 
var $skipParam = $('#skip_param').on('click', function(event){
	console.log('--- skip_param ---');
	skipSelectedAllDisk();
});


// ---
$("#init_disk_filter").on('click', function (event) {
	console.log('---  init_disk_filter ---');
		initdDiskFilter(event);
});


})(jQuery);