<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
// $arYears = $arResult['OTHERS_PROPS']['MODELS'][0]['YEARS'];
// $arModels = $arResult['OTHERS_PROPS']['MODELS'];
// $arModifications = $arResult['OTHERS_PROPS']['MODELS'][0]['MODIFICATIONS'];


$result = getDiskDefaultData();
// $arResult['VENDORS'] = $result['VENDORS'];
// $arYears = $result['MODELS'][0]['YEARS'][0];
// $arModels = $result['MODELS'];
// $arModifications = $result['MODELS'][0]['MODIFICATIONS'][0];

$arResult['VENDORS'] = $result['VENDORS'];
$arYears = $result['YEARS'];
$arModels = $result['MODELS'];
$arModifications = $result['MODIFICATIONS'];

/*pr($arYears);
pr($arModels);
pr($arModifications);*/
// die();
  
?>

<div class="search_form_param_car_wrap">
    <div class="selects_car_wrap">
        <div class="sel-car-wrap">
            <div class="descr"><?=GetMessage('RZ_MARK')?></div>
            <select name="disks_makr_filter" <?=empty($arResult['VENDORS']) ? 'class="disabled"' : ''?>>
                <?foreach ($arResult['VENDORS'] as $keyVendor => $value):?>
                    <option value="<?=$keyVendor?>" <?=$keyVendor == 0 ? 'selected' : ''?>><?=$value?></option>
                <?endforeach;?>
            </select>
        </div>
        <div class="sel-car-wrap">
            <div class="descr"><?=GetMessage('RZ_MODEL')?></div>
            <select name="disks_model_filter" <?=empty($arModels) ? 'class="disabled"' : ''?>>
                <?foreach ($arModels as $keyModel => $arModel):?>
                    <option value="<?=$keyModel?>" <?=$keyModel == 0 ? 'selected' : ''?>><?=$arModel?></option>
                <?endforeach;?>
            </select>
        </div>
        <div class="sel-car-wrap">
            <div class="descr"><?=GetMessage('RZ_YEAR')?></div>
            <select name="disks_year_filter" <?=empty($arModifications) ? 'class="disabled"' : ''?>>
                <?foreach ($arYears as $keyYear => $value):?>
                    <option value="<?=$keyYear?>" <?=$keyYear == 0 ? 'selected' : ''?>><?=$value?></option>
                <?endforeach;?>
            </select>
        </div>
        <div class="sel-car-wrap">
            <div class="descr"><?=GetMessage('RZ_MODIF')?></div>
            <select name="disks_modif_filter" <?=empty($arYears) ? 'class="disabled"' : ''?>>
                <?foreach ($arModifications as $keyModif => $value):?>
                    <option value="<?=$keyModif?>" <?=$keyModif == 0 ? 'selected' : ''?>><?=$value?></option>
                <?endforeach;?>
            </select>
        </div>
    </div>
</div> 

