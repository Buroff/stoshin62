(typeof(jQuery) != 'undefined')
&& (function ($) {
	var $rangeSlider = $(".js-price-range-slider");
	var elementNums = false;
	var $modelData = false;
	var $selectedTireData = {
		'DIAMETR':false,
		'SHIRINA_PROFILA':false,
		'VISOTA_PROFILA':false,  
		'DIAMETR_VALUE':false,
		'SHIRINA_PROFILA_VALUE':false,
		'VISOTA_PROFILA_VALUE':false,  
	}


	var checkData = false;
	var location = false;
	// $('#set_filter').css({'display':'block'});
 //    $('#set_fake_filter').css({'display':'none'});

   


	$rangeSlider.each(function () {
		var $this = $(this),
			$parent = $this.closest('.seach_form__inner_price'),
			$priceTo = $parent.find('.price_to:eq(0)'),
			$priceFrom = $parent.find('.price_from:eq(0)'),
			priceMinVal = Math.floor(parseFloat($priceFrom.data('val'))),
			priceMaxVal = Math.ceil(parseFloat($priceTo.data('val'))),
			priceToVal = $priceTo.val(),
			priceFromVal = $priceFrom.val();
		if (priceToVal.length == 0) {
			priceToVal = priceMaxVal;
		}
		if (priceFromVal.length == 0) {
			priceFromVal = priceMinVal;
		} 
		
		$this.noUiSlider({
			start: [priceFromVal, priceToVal],
			connect: true,
			step: 1,
			range: {
				'min': priceMinVal,
				'max': priceMaxVal
			},
			format: wNumb({
				decimals: 0
			})
		});
		$this.on({
			set: function (e, vals) {
				$priceFrom.val('');
				$priceTo.val('');
			},
			change: function (e, vals) {
				if ($priceFrom.val().length == 0 || $priceTo.val().length == 0) {
					$priceFrom.val(vals[0]);
					$priceTo.val(vals[1]);
					$priceTo.trigger('change');
				}
			}
		});

		var triggerSliderInput = function (val) {
			var $this = $(this);
			if ($this.val() != val && $this.val().length > 0) {
			$this.val(val);
				if (sliderTrigger != null) {
					clearTimeout(sliderTrigger);
				}
				sliderTrigger = setTimeout(function () {
					$this.trigger('change');
				}, 700);
			}
		};
		$this.Link('lower').to($priceFrom, function (val) {
			triggerSliderInput.bind(this, val);
		});
		$this.Link('upper').to($priceTo, function (val) {

			triggerSliderInput.bind(this, val);
		});

		$this.Link('lower').to('-inline-<div class="tooltip"></div>', function (value) {
			$(this).html('<span class="tooltip_inner">' + value + '</span>');
		});

		$this.Link('upper').to('-inline-<div class="tooltip"></div>', function (value) {
			$(this).html('<span class="tooltip_inner">' + value + '</span>');
		});
	});

	var $sectionFilters = $('.filterHorizontal');

	$sectionFilters.on('change', 'input', function (e) {
		// console.log('--- sectionFilters ---');
		var form_id = $(this).closest('form.filterHorizontal').attr('id');
		
		if(form_id == 'search_tires'){
			var $sectionFilter = $(this).closest('form.filterHorizontal');
			var rnd = $sectionFilter.data('rnd');
			var data = $sectionFilter.serializeArray();
			data.push({name: 'arParams', value: $sectionFilter.data('arparams')});
			$sectionFilter.setAjaxLoading();
			$.ajax({
				// url: rz.AJAX_DIR + "catalog_Filter.php",
				url: rz.AJAX_DIR + "catalog_Filter_alex.php",
				data: data,
				dataType: "json",
				success: function (json) {

					console.log('--- catalog_Filter ---');
					// console.log(json); !!!
					// console.log(json.FILTER_URL);

					// resetFilter(); // сбрасываем фильтр

					// занченние фильтра чтобы потом его получить !!!
					let tires_url_filter = json.FILTER_URL.substring(json.FILTER_URL.indexOf('set_filter'));
					$('#tires_url_filter').text(tires_url_filter);
					console.log(tires_url_filter);

					rz.FILTER_AJAX = [];
					$.each(json.ITEMS, function (key, val) {
						$.each(val.VALUES, function (valKey, valVal) {
								ajaxValuePrepare(valVal, rnd);
						});
					});

					$sectionFilter.stopAjaxLoading();
					$(document).trigger('catalog_applyFilter');

					// --- запуск проверки параметров фильтра !!!
					// filterData();
					
				}
			});
		}
	
	});

	// сброс значения фильтров
	/*$sectionFilters.on('click', '.form_reset_button', function (e) {
		e.preventDefault();
		var $sectionFilter = $(this).closest('form.filterHorizontal');
		$sectionFilter[0].reset();
		var rnd = $sectionFilter.data('rnd');
		var $jsSlider = $sectionFilter.find('.js-price-range-slider');
		var priceToVal = $sectionFilter.find('.price_to:eq(0)').val(),
			priceFromVal = $sectionFilter.find('.price_from:eq(0)').val();
		$jsSlider.val([priceFromVal,priceToVal]);

		$sectionFilter.find('input:visible:eq(0)').trigger('change');
		$('#tires [name="tiers_makr_filter"]').trigger("change");

		console.log('-- form_reset_button --');
		console.log(rnd);
		console.log($jsSlider);
		console.log(priceToVal);
		console.log($sectionFilter);

	});*/

	$sectionFilters.on('click', '.reset_tyres', function (e) {
		e.preventDefault();
		var $sectionFilter = $(this).closest('form.filterHorizontal');
		$sectionFilter[0].reset();
		var rnd = $sectionFilter.data('rnd');
		var $jsSlider = $sectionFilter.find('.js-price-range-slider');
		var priceToVal = $sectionFilter.find('.price_to:eq(0)').val(),
			priceFromVal = $sectionFilter.find('.price_from:eq(0)').val();
		$jsSlider.val([priceFromVal,priceToVal]);
		$sectionFilter.find('input:visible:eq(0)').trigger('change');
		resetFilter();
		$('#tires [name="tiers_makr_filter"]').trigger("change");

	});

	var ajaxValuePrepare = function (val, rnd) {
		// console.log('--- ajaxValuePrepare ---');
		var $curItem = $('#' + rnd + val.CONTROL_ID);
		var $itemStyler = $('#' + rnd + val.CONTROL_ID + "-styler");
		var lastsymb = val.CONTROL_NAME.substr(-3);
		var isNum = false;
    	// console.log($curItem);
		// console.log($itemStyler);
		// console.log(val);
		if (lastsymb == 'MAX' || lastsymb == "MIN") {
			isNum = true;
		}
		if ('DISABLED' in val && val.DISABLED == 1) {
			if ($itemStyler.length > 0) {
				$itemStyler.addClass('disabled');
			}
			$curItem.attr('disabled', 'disabled');
			// console.log(val.DISABLED + '  ' + val.CONTROL_ID);
		} else {
			if ($itemStyler.length > 0) {
				$itemStyler.removeClass('disabled');
			}
			$curItem.removeAttr('disabled');
		}
	};


	// --- событие при изменение данных в списке выбора автомобиля
	var cars_selects = {
		'mark': '[name="tiers_makr_filter"]', 
		'model': '[name="tiers_model_filter"]',
		'year':'[name="tiers_year_filter"]',
		'modif':'[name="tiers_modif_filter"]'
	};
	// var $sectionFilters = $('.filterHorizontal');
	var vendor = false, model = false, year = false, modif = false;

    $.each(cars_selects, function (key, val) {
        cars_selects[key] = $sectionFilters.find(val);
    });
    
	cars_selects['mark'].on('change', function () {
        // var vendor = $(this).find('option:selected').text();
        // console.log('--- change ---');
        // console.log(vendor);
        vendor = $(this).find('option:selected').text();

        // doAjaxGetValsOfCars('by_vendor',$(this),vendor);
        setTyreFilterAjax('by_vendor',$(this),vendor);
    });

    cars_selects['model'].on('change', function () {
        // var $this = $(this),
        //     vendor = $this.getSelOptFromForm(cars_selects['mark']).text(),
        //     model = $this.find('option:selected').text();
        var $this = $(this);
            vendor = $this.getSelOptFromForm(cars_selects['mark']).text();
            model = $this.find('option:selected').text();

        // doAjaxGetValsOfCars('by_vendor_model',$(this),vendor,model);
        setTyreFilterAjax('by_vendor_model', $(this), vendor, model);
    });
    cars_selects['year'].on('change', function () {
        // var $this = $(this),
        //     vendor = $this.getSelOptFromForm(cars_selects['mark']).text(),
        //     model = $this.getSelOptFromForm(cars_selects['model']).text(),
        //     year = $this.find('option:selected').text();
        var $this = $(this);
            vendor = $this.getSelOptFromForm(cars_selects['mark']).text();
            model = $this.getSelOptFromForm(cars_selects['model']).text();
            year = $this.find('option:selected').text();

        // doAjaxGetValsOfCars('by_vendor_model_year',$(this),vendor,model,year);
        setTyreFilterAjax('by_vendor_model_year',$(this),vendor,model,year);
    });
    cars_selects['modif'].on('change', function () {
        // var $this = $(this),
        //     vendor = $this.getSelOptFromForm(cars_selects['mark']).text(),
        //     model = $this.getSelOptFromForm(cars_selects['model']).text(),
        //     year = $this.getSelOptFromForm(cars_selects['year']).text(),
        //     modif = $this.find('option:selected').text();

        var $this = $(this);
            vendor = $this.getSelOptFromForm(cars_selects['mark']).text();
            model = $this.getSelOptFromForm(cars_selects['model']).text();
            year = $this.getSelOptFromForm(cars_selects['year']).text();
            modif = $this.find('option:selected').text();
        // doAjaxGetValsOfCars('by_vendor_model_year_modif',$(this),vendor,model,year,modif);
        setTyreFilterAjax('by_vendor_model_year_modif',$(this),vendor,model,year,modif);
    });


     var initTyreFilter = function (event){
    	console.log('-- initdTyreFilter --');

	    if(vendor !== false && model === false && year == false && modif == false){
	    	// console.log(vendor);

	    	let mode = true, _this = false;
	    	setTyreFilterAjax('by_vendor', _this, vendor, model, year, modif, mode);
	    }


	    if(vendor !== false && model !== false && year == false && modif == false){
	    	// console.log(vendor);
	    	// console.log(model);

	    	let mode = true, _this = false;
	    	setTyreFilterAjax('by_vendor_model', _this, vendor, model, year, modif, mode);
	    }

	    if(vendor !== false && model !== false && year !== false && modif == false){
	    	// console.log(vendor);
	    	// console.log(model);
	    	// console.log(year);

	    	let mode = true, _this = false;
	    	setTyreFilterAjax('by_vendor_model_year', _this, vendor, model, year, modif, mode);
	    }


	    if(vendor !== false && model !== false && year !== false && modif !== false){
	    	// console.log(vendor);
	    	// console.log(model);
	    	// console.log(year);
	    	// console.log(modif);

	    	let mode = true, _this = false;
	    	setTyreFilterAjax('by_vendor_model_year_modif', _this, vendor, model, year, modif, mode);
	    }


    }



    // --- устаналиваем значения в HTML
	var _setHtmlOptions = function(elements,apendTo){
		// console.log('--- _setHtmlOptions ---');
		// console.log(elements);
		// console.log(apendTo);

	    var options = '';
	    if (typeof elements != 'undefined'){
	        $.each(elements,function(index,value){
	        	// console.log(value);
	            if (value) {
	                options += '<option value="' + value + '">' + value + '</option>'
	            }
	        });
	        apendTo.children().remove();
	        apendTo.append($(options));
	    }
	};

	var _getModels = function(elements){

		// console.log(elements);

	    var models = [];
	    $.each(elements,function(){
	        if (this.NAME){
	            models.push(this.NAME);
	        }
	    });
	    return models;
	};

 // ищем элементы по параметрам из БД автомобилей
    var findElements = function(model, type){
    	console.log('--- findElements ---');
    	// 

    	console.log(model);
    	// console.log(type);

    	if(type == 'shiny'){
    		var elems = $('#tires_by_param input');
    	}
    	
    	var i = 0;
    	var id_diametr = false, id_shirina = false, id_visota = false;
    	var type_diametr_id = false, type_shirina_id = false, type_visota_id = false;

		$.each(elems, function(index, value){
				// console.log("INDEX: " + index + "\n");
				// console.log(value);

				if(typeof model.TYRES_FACTORY !== 'undefined'){

						let element = $('#'+value.id + '-styler').siblings('.text');
						let typeCheckbox = false;
						
						switch (element.data('property')){
							case 'DIAMETR':
								let $resDiametr = setCheckbox(model.TYRES_FACTORY.DIAMETR, element.text(), value.id, element.data('property'));
						    	if($resDiametr !== false){
						    		$selectedTireData.DIAMETR = $resDiametr;
						    		$selectedTireData.DIAMETR_VALUE = model.TYRES_FACTORY.DIAMETR;
						    	}
						   
						    break;

						    case 'SHIRINA_PROFILA':
						  //   	console.log('-- SHIRINA_PROFILA --');
								// console.log(model.TYRES_FACTORY);
								// //if('SHIRINA_PROFILA' in model.TYRES_FACTORY){
					
						    		let $resSP = setCheckbox(model.TYRES_FACTORY.SHIRINA_PROFILA, element.text(), value.id, element.data('property'));
							    	if($resSP !== false){
							    		$selectedTireData.SHIRINA_PROFILA = $resSP;
							    		$selectedTireData.SHIRINA_PROFILA_VALUE = model.TYRES_FACTORY.SHIRINA_PROFILA;
							    	}

						    break;    

						    case 'VISOTA_PROFILA':
						    	let $resVP = setCheckbox(model.TYRES_FACTORY.VISOTA_PROFILA, element.text(), value.id, element.data('property'));
						    	if($resVP !== false){
						    		$selectedTireData.VISOTA_PROFILA = $resVP;
						    		$selectedTireData.VISOTA_PROFILA_VALUE = model.TYRES_FACTORY.VISOTA_PROFILA;
						    	}
						    break;
						}

					
					i++;

					if(i == elems.length){
						elementNums = true;
					}

				}

			
		});

    }

	// устанавливаем чекбоксы
	var setCheckbox = function(data, text, id, typeCheckbox){
		let res = false;
		data.forEach(function (value, i) {
			if(typeCheckbox == 'DIAMETR'){
				value = 'R' + value;
			}
         	if(value == text){
         	 	// $('#'+id).prop('checked', true);
         	 	// $('#'+id+'-styler').addClass('checked');
         	 	res = id;
         	}
        });

        return res;
	}

	var resetFilter = function (){
		console.log('-- resetFilter --');
		elementNums = false;
	    $selectedTireData.DIAMETR = false;
	    $selectedTireData.SHIRINA_PROFILA = false;
	    $selectedTireData.VISOTA_PROFILA = false;
	    location = false;
	    checkData = false;
	    FILTER_URL = false;


		$selectedTireData.SHIRINA_PROFILA_VALUE = false;
		$selectedTireData.VISOTA_PROFILA_VALUE = false;
		$selectedTireData.DIAMETR_VALUE = false;

    	// $('#set_filter').css({'display':'block'});
    	// $('#set_fake_filter').css({'display':'none'});

    	$('#print_not_found_tyre').empty();
	    $('#print_not_found_tyre').css({'display':'none'});

		$('#tyre_replace').empty();
		$('#tyre_replace').css({'display':'none'});

		$('#print_found_tyre').empty();
		$('#print_found_tyre').css({'display':'none'});
	}

    // --- Ajax на PHP скрипт
    // var doAjaxGetValsOfCars = function(action,$this,strVendor,strModel,strYear,strModif){
    var setTyreFilterAjax = function(action,$this,strVendor,strModel,strYear,strModif, mode = false){

	   	// сбрасываем фильтр по параметрам
        skipSelectedAll();
        resetFilter();



        // тип шины или диски
        var type = $('#type').data('type');

        var data = {},
         // $container = $this.closest('.filterHorizontal'),
            $container = $('#search_tires'),
            bHasInput = $container.find('[name="car-id"]').length ? true : false,
            $inputCar = bHasInput ? $container.find('[name="car-id"]') : $('<input type="hidden" name="car-id"/>');

	       if (typeof strVendor != 'undefined'){
	            data['vendor'] = strVendor;
	        }
	        if (typeof strModel != 'undefined'){
	            data['model'] = strModel;
	        }
	        if (typeof strYear != 'undefined'){
	            data['year'] = strYear;
	        }
	        if (typeof strModif != 'undefined'){
	            data['modif'] = strModif;
	        }

        data['action_get_info'] = action;
        data['type'] = type;
        $container.setAjaxLoading();

        // $( "#set_filter" ).prop( "disabled", true); // hide button
        // $this.prop( "disabled", true);
 
        
        // получаем данные по шинам (ширина, высота и диаметр) по выбранному автомобилю 
        $.ajax({
        	url: rz.AJAX_DIR + '/car_filter/car_ajax.php',
        	data:data,
            dataType:'json',
            method: 'POST',
            success: function(result){

            	console.log('--- Ajax ---');

            	// var modif,years,carID,models,objData;
            	var car, models, years, modif;
	            	models = result.data_base.MODELS;
	            	modif = result.data_base.MODIFICATIONS;
	            	years = result.data_base.YEARS;
         	
                // выставляем новые значения
                // $disabled_field = findElements(result.data_base.MODEL, type); // ищем в фильрах совпадения по параметрам
                $modelData = result.data_base.MODEL;


                // выводим данные в HTML
		        _setHtmlOptions(models,cars_selects['model']);
		        _setHtmlOptions(modif,cars_selects['modif']);
		        _setHtmlOptions(years,cars_selects['year']);

		         if(mode == false){

			        console.log('--- Меняем значения фильтра ---');

				    let url  = false;
					// buttonHide(url);
					$container.find('select').trigger('refresh');
					$container.stopAjaxLoading();
					$container.find('input').eq(0).trigger('change');

			    }else{

			    	console.log('--- Получаем товары по сочетанию ---');
				    console.log(result.data_base.TYRES_FACTORY_VARIANTS);
				    // console.log(result.data_base);


				       	// выставляем новые значения
		                // findElements(result.data_base.MODEL, type); // ищем в фильрах совпадения по параметрам
		                
		                // если товаров по сочетанию не найдено
					        if(result.data_base.MODEL.ROWS == 0){
		     		        	// printTyresNotfound(result.data_base.MODEL.VALID, $selectedDiskData);
							    // let location = 'catalog/diski/?&arrFilter_197_339036633=Y&arrFilter_198_1608842955=Y&arrFilter_199_34404265=Y&arrFilter_200_219485612=Y&arrFilter_201_1761665292=Y&set_filter=Y';
								// let url = buildURL($selectedDiskData);
								// console.log(url);
								// buttonHide(url);

								printTyreNotfound(result.data_base.MODEL.VALID, $selectedTireData)

		     		        }else{ 
		     		        	// let url = buildURL($selectedDiskData);
								// console.log(url);
		     		        	// printFound(result.data_base.MODEL.VALID, url);
		     		        	// printWheels(result.data_base.WHEELS_FACTORY_VARIANTS);

		     		        	// printTyreFound(result.data_base.MODEL.VALID, url);

		     		        	// if(result.data_base.TYRES_FACTORY_VARIANTS !== 'undefined'){
		     		        	//  	printTyres(result.data_base.TYRES_FACTORY_VARIANTS);
		     		        	// } 

		     		        }

		     		      
		     		        console.log(result.data_base.TYRES_REPLACE[0]);
		     		        //  ссылки по замене
		     		        // if(result.data_base.TYRES_REPLACE[0].HREF !== false){
		     		        // 	printTyresReplace(result.data_base.TYRES_REPLACE);
		     		        // }

		     		        printTyres(result.data_base.TYRES_FACTORY_VARIANTS);
		     		        printTyresReplace(result.data_base.TYRES_REPLACE);


		     		        //if(elementNums){
				                $container.find('select').trigger('refresh');
				                $container.stopAjaxLoading();
				                $container.find('input').eq(0).trigger('change');
				                
				                // снимаем неактивность с кнопки Подобрать
				                setTimeout(function(){
									  $( "#set_disks_filter" ).prop( "disabled", false);
								}, 3000);
			                // }

			    }

			    
                

            	
               
            }

        });
    };

// --- NEW FUNCIONS

var printTyres = function(tyres_factory_variants){
		console.log('---printTyres ---');
		console.log(tyres_factory_variants);

		let html = '<b>Для вашего автомобиля подходят шины с параметрами:</b>';
		html = html + '<ul style="list-style-type:none;">';
		tyres_factory_variants.forEach(function(element) {
			if(element['HREF'] !== false){
				html = html + '<li style="margin:5px 0 0 -10%;"><a href="'+ element.HREF +'">'+ element.NAME + '</a></li>';

			}else{
				html = html + '<li style="margin:5px 0 0 -10%;">'+ element.NAME + '</li>';

			}
		});
		html = html + '</ul>';

	$('#print_found_tyre').append(html);
	$('#print_found_tyre').css({'display':'block'});

}


var printTyresReplace = function(tyres_replace){
	console.log('-- print Tyres Replace --');


	let html = '<b>Подходящие шины (замена): </b>';
		html = html + '<ul style="list-style-type:none;">';
		tyres_replace.forEach(function(element) {
			if(element['HREF'] !== false){
				html = html + '<li style="margin:5px 0 0 -10%;"><a href="'+ element.HREF +'">'+ element.NAME + '</a></li>';

			}else{
				html = html + '<li style="margin:5px 0 0 -10%;">'+ element.NAME + '</li>';

			}
		});
		html = html + '</ul>';
		$('#tyre_replace').append(html);
		$('#tyre_replace').css({'display':'block'});
	// console.log(html);
}





var printTyreFound = function(valid, url){
	let html = '<b>По вашему запросу найдены товары!</b><br/>';
	html = html + 'перейдите пожалуйста в каталог нажав на ссылку';
		html = html + '<ul style="list-style-type:none;">';
		html = html + '<li style="margin:5px 0 0 -10%;"><a href="'+ url.location +'">Товары</a></li>';
		html = html + '</ul>';
	$('#print_found_tyre').append(html);
			$('#print_found_tyre').css({'display':'block'});

}



var printTyreNotfound = function(valid, selectedData){
	console.log('-- printNOTfound --');
	let combination = '', params = '', html  = '', errors = false;

	for(key in valid){
				// console.log(valid[key]);
				//  console.log(key);
			switch (key) {

			  	// case "DIA":
				  //   	console.log('DIA:' + valid[key].toString());
				  //   	combination = combination + '<li>' + ' DIA : ' + valid[key].toString() + '</li>';
				  //   	if(selectedData['DIA_VALUE'] == false){
				  //   	 	params = params + '<li>' + 'DIA: '+ valid[key].toString() +'</li>';
				  //   	 	errors = true;
				  //   	}
			   //  break;


			    case "VISOTA_PROFILA":
				    	 console.log('VISOTA_PROFILA:' + valid[key].toString());
				    	 combination = combination + '<li>' + ' VISOTA_PROFILA:' + valid[key].toString() + '</li>';
				    	if(selectedData['VISOTA_PROFILA_VALUE'] == false){
				    	 	params = params + '<li>' + 'PCD: '+ valid[key].toString() +'</li>';
				    	 	errors = true;
				    	}
			    break;

				
				 case "SHIRINA_PROFILA":
				    	 console.log('SHIRINA_PROFILA:' + valid[key].toString());
				    	 combination = combination + '<li>' + ' SHIRINA_PROFILA:' + valid[key].toString() + '</li>';
				    	if(selectedData['SHIRINA_PROFILA_VALUE'] == false){
				    	 	params = params + '<li>' + 'PCD: '+ valid[key].toString() +'</li>';
				    	 	errors = true;
				    	}
			    break;


			     case "DIAMETR":
				    	 console.log('DIAMETR:' + valid[key].toString());
				    	 combination = combination + '<li>' + ' DIAMETR:' + valid[key].toString() + '</li>';
				    	if(selectedData['DIAMETR_VALUE'] == false){
				    	 	params = params + '<li>' + 'PCD: '+ valid[key].toString() +'</li>';
				    	 	errors = true;
				    	}
			    break;


				

				
			}


	}

	// если один из параметров отсутсвует
	if(errors == true){
			// if(combination !==''){
			// 	combination = '<p>Извините, у нас нет товаров с выбранным вами combination: <br/>' + combination;
			// }

			if(params !==''){
				// params = '<b>Извините, в нашем магазине пока нет товаров c выбранным вами сочетанием параметров: </b>' + '<ul style="list-style-type:none;">' + params + '</ul>';
				params = '<b>Извините, в нашем магазине пока нет товаров c выбранным вами сочетанием параметров! </b><br/><br/>';
			}
			// html = params + ' <b>попробуйте изменить критерии поиска</b>';
			html = params;
			$('#print_not_found').append(html);
			$('#print_not_found').css({'display':'block'});
	}

}


// --- OLD FUNCTIONS


var makeString = function($string, $needle){
	let array = $string.split($needle);
	let output = $needle + array[1];
	return output;
}


// данные по фильтру
var filterData = function (){
	
	console.log('--- filterData ---');	

	// $("#set_filter").attr('type','submit');
	// $("#set_filter").removeClass('window_redirect');	
	
	if($modelData !== false){


		let filter = $('#tires_url_filter').text();
		let filter_num = filter.split("arrFilter").length - 1;
		console.log(filter);
		// console.log(filter_num);	
		// console.log($modelData);	


		if(filter_num < 3){

			if(filter_num > 0){

				console.log('--- skipDisable ---');
				skipDisable($selectedTireData);
				/// changeButton();

			}
			else{

				console.log('--- submit ---');
				$("#set_filter").attr('type','submit');
			    $("#set_filter").removeClass('window_redirect');	

			}

		}else{

			console.log('--- submit ---');
			$("#set_filter").attr('type','submit');
			$("#set_filter").removeClass('window_redirect');	

		}
	}
}


// сбросить неаактивные чекбоксы
var skipDisable = function($selectedTireData){

		console.log('--- skipDisable ---');
		console.log($selectedTireData.DIAMETR + ' ' + $selectedTireData.DIAMETR_VALUE);
		console.log($selectedTireData.SHIRINA_PROFILA + ' ' + $selectedTireData.SHIRINA_PROFILA_VALUE);
		console.log($selectedTireData.VISOTA_PROFILA + ' ' + $selectedTireData.VISOTA_PROFILA_VALUE);
	

		$diametrString = makeString($selectedTireData.DIAMETR, 'arrFilter');
		$shirinaProfilaString = makeString($selectedTireData.SHIRINA_PROFILA, 'arrFilter');
		$visotaProfilaString = makeString($selectedTireData.VISOTA_PROFILA, 'arrFilter');

		console.log($diametrString);
		console.log($shirinaProfilaString);
		console.log($visotaProfilaString);

		var action = $("#search_tires").attr('action');
		console.log(action);
		// window.location = action + '?clear_cache=Y&' + $shirinaProfilaString + '=Y&' + $visotaProfilaString + '=Y&' + $diametrString + '=Y&tiers_makr_filter=2&set_filter=Y';
		$("#set_filter").attr('type','button');

		location = action + '?clear_cache=Y&' + $shirinaProfilaString + '=Y&' + $visotaProfilaString + '=Y&' + $diametrString + '=Y&tiers_makr_filter=2&set_filter=Y';
		$("#set_filter").attr('type','button');
		$("#set_filter").addClass('window_redirect');
}


// кнопка убрать фильтр (чтобы надпись менялась)
$('.collapse_btn_wrapper').on('click', function(event){
	$('.seach_form__submit').find(".btn-inner").text(function(i, text){
	          return text === "ПОКАЗАТЬ ФИЛЬТР" ? "СКРЫТЬ ФИЛЬТР" : "ПОКАЗАТЬ ФИЛЬТР";
	 });
});

// сбрасываем все
var $skipParam = $('#skip_param').on('click', function(event){
	console.log('--- skip_param ---');
	skipSelectedAll();
});

var skipSelectedAll = function(){
 	console.log('--- skipSelected ---');
 	var elems = $('#tires_by_param input');

  	$.each(elems, function(index, value){
			// console.log(value.id);
			$('#'+value.id).prop( "disabled", false);
			$('#'+value.id).prop('checked', false);
			$('#'+value.id+'-styler').removeClass('checked');
			$('#'+value.id+'-styler').removeClass('disabled');
 	});
}

// сбрасываем значение при переключении на вкладку Подбор по автомобилю
$("a[href='#tires_by_car']").on('click', function () {
	$('#tires [name="tiers_makr_filter"]').trigger("change");
	hideSubmit(); // прячем кнопку submit
	showInit();
});



// запускаем Ajax запрос (по модели) и генерим ссылки 
$("#init_tyre_filter").on('click', function (event) {
	console.log('---  init_tyre_filter ---');
	initTyreFilter(event);
});


var hideSubmit = function(){
	console.log('--  hideSubmit --');
	// $("#set_filter").prop( "disabled", true);
	$('#set_filter').css({'display':'none'});
}

var hideInit = function(){
	console.log('--  hideInit --');
	$('#init_tyre_filter').css({'display':'none'});
}

var showSubmit = function(){
	$('#set_filter').css({'display':'block'});
}

var showInit = function(){
	$('#init_tyre_filter').css({'display':'block'});
}


$("a[href='#tires_by_param']").on('click', function () {
	hideInit(); 
	showSubmit();
	// $('.filterHorizontal').trigger('click');
	// $('.filterHorizontal').click();
	// let $sectionTyreFilter = $('#search_tires');
	// 	$sectionTyreFilter[0].reset();
	// 	$sectionTyreFilter.find('input:visible:eq(0)').trigger('change');
	resetFilter();
});



})(jQuery);