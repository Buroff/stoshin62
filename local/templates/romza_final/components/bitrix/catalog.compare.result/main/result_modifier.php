<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
foreach ($arResult["ITEMS"] as &$arElement) {
	$curPrice = false;
	if (!empty($arElement['PRICES'])) {
		$arPrice = reset($arElement['PRICES']);
		$curPrice = $arPrice['PRINT_DISCOUNT_VALUE'];
	} elseif (!empty($arElement['PROPERTIES']['MINIMUM_PRICE']['VALUE'])) {
		$currency = 'RUB';
		if (!empty($arResult['CONVERT_CURRENCY']['CURRENCY_ID'])) {
			$currency = $arResult['CONVERT_CURRENCY']['CURRENCY_ID'];
		}
		if(function_exists('FormatCurrency')) {
		$curPrice = FormatCurrency($arElement['PROPERTIES']['MINIMUM_PRICE']['VALUE'], $currency);
		} else {
			$curPrice = $arElement['PROPERTIES']['MINIMUM_PRICE']['VALUE'];
		}

	}
	$arElement['CUR_PRICE'] = $curPrice;
} unset($arElement);
