(typeof(jQuery) != 'undefined')
&& (function ($) {
	var $compareBlock = $('#compare_Block'),
		defData = [
			{name: "arParams", value: $compareBlock.data('arparams')},
			{name: "template", value: $compareBlock.data('template').toString()}
		];
	$compareBlock.on('click','.goods_item_delete', function () {
		var $this = $(this),
			$form = $this.closest('form');
		var data = $form.serializeArray();
		$.merge(data, defData);
		$compareBlock.setAjaxLoading();
		$.ajax({
			type: "POST",
			url: rz.AJAX_DIR + "compare_Block.php",
			data: data,
			success: function(msg) {
				$compareBlock.html(msg);
				$compareBlock.reRate();
				$compareBlock.stopAjaxLoading();
				$(document).trigger('compare_Refresh');
			}
		})
	});
})(jQuery);