<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$title = $arResult['NAME'];

if(isset($arResult['IPROPERTY_VALUES']['ELEMENT_META_TITLE'])) {
	$title = $arResult['IPROPERTY_VALUES']['ELEMENT_META_TITLE'];
}
if(isset($arResult['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'])) {
	$title = $arResult['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'];
}
$arCatchBuy = $arParams['CATCHBUY'][$arResult['ID']];
$bTimer = !empty($arCatchBuy['ACTIVE_TO']);
$bProgressBar = $arCatchBuy['MAX_USES'] > 0;

if (!$arParams['SHOW_CATCH_BUY']) {
	$bTimer = false;
	$bProgressBar = false;
	$arParams['CATCHBUY']['HAS_OFFERS'] = false;
};
$arCatchBuy['PERCENT'] = ($bProgressBar) ? $arCatchBuy['COUNT_USES'] / $arCatchBuy['MAX_USES'] * 100 : 0;
?>
<!-- <h2><?=$title?></h2> -->
<h2><?=$arResult['NAME']?></h2>
<div class="item_page_wrapper">
	<div class="item_brief_info_block">
		<div class="item_gallery_wrapper">
			<div class="item_gallery__big_wrapper">
				<div class="img-wrap">
					<?$firstPhoto = reset($arResult["MORE_PHOTO"]) ?>
					<img src="<?=$firstPhoto['SRC']?>" alt="<?=$arResult['NAME']?>" title="" data-toggle="modal" data-target="#modal-lightbox" id="gallery-img">
				</div>
			</div>
			<?if(count($arResult["MORE_PHOTO"]) > 1):?>
				<div class="item_gallery__tumbs_wrapper">
					<div class="frame" id="thumbs">
						<div class="item_gallery__tumbs">
							<?foreach ($arResult["MORE_PHOTO"] as $arPhoto):?>
								<div class="item">
									<img src="<?=$arPhoto['THUMB_SRC']?>"
										 alt="<?=$arResult['NAME']//=$arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_ALT']?>"
										 title="<?=$arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_TITLE']?>"
										 data-src-big="<?=$arPhoto['SRC']?>"/>
								</div>
							<?endforeach?>
						</div>
					</div>
					<div class="item_gallery__tumbs__navigate">
						<span class="btn btn-default btn-arrow flaticon-arrow133 btn-prev"></span>
						<span class="data">
							<?=GetMessage("RZ_FOTO")?>
							<span class="current"></span>/<span class="total"></span>
						</span>
						<span class="btn btn-default btn-arrow flaticon-right20 btn-next"></span>
					</div>
				</div>
			<?endif?>
		</div>

		<div class="item_manage_wrapper">
			<div class="item_manage_block" style="padding-top: 10px;">
				<!-- <div class="item_manage__inner item_manage__rating">
					<?
					// $APPLICATION->IncludeComponent(
					// 	"bitrix:iblock.vote",
					// 	"stars",
					// 	array(
					// 		"IBLOCK_TYPE" => $arParams['IBLOCK_TYPE'],
					// 		"IBLOCK_ID" => $arParams['IBLOCK_ID'],
					// 		"ELEMENT_ID" => $arResult['ID'],
					// 		"ELEMENT_CODE" => "",
					// 		"MAX_VOTE" => "5",
					// 		"VOTE_NAMES" => array("1", "2", "3", "4", "5"),
					// 		"SET_STATUS_404" => "N",
					// 		"DISPLAY_AS_RATING" => $arParams['VOTE_DISPLAY_AS_RATING'],
					// 		"CACHE_TYPE" => $arParams['CACHE_TYPE'],
					// 		"CACHE_TIME" => $arParams['CACHE_TIME']
					// 	),
					// 	$component,
					// 	array("HIDE_ICONS" => "Y")
					// );
					?>
				</div> -->
				<?if($bProgressBar || $bTimer || $arParams['CATCHBUY']['HAS_OFFERS']):?>
					<div class="item_manage__inner countdown-block">
						<div class="countdown-title"><?=GetMessage('RZ_CATCH_BUY')?></div>
						<?if($bTimer || $arParams['CATCHBUY']['HAS_OFFERS']):?>
							<div class="countdown">
								<div class="timer" data-until="<?=str_replace('XXX', 'T', ConvertDateTime($arCatchBuy['ACTIVE_TO'], 'YYYY-MM-DDXXXhh:mm:ss'))?>"></div>
							</div>
						<?endif?>
						<?if($bProgressBar || $arParams['CATCHBUY']['HAS_OFFERS']):?>
							<div class="already-sold__block">
								<div class="already-sold">
									<div class="already-sold-track">
										<div class="bar" style="width: <?=intVal($arCatchBuy['PERCENT'])?>%"></div>
									</div>
								</div>
								<span class="text__block"><span class="text"><?=intVal($arCatchBuy['PERCENT'])?>%</span> <?=GetMessage('RZ_SOLD')?></span>
							</div>
						<?endif?>
					</div>
				<?endif?>
				<form class="item_manage__inner item_manage__buying add2basket_form with_modal" method="get"
					  action="<?=$APPLICATION->GetCurPage()?>"
					  data-target="#modal-buy"
					  data-varQ="<?= $arParams["PRODUCT_QUANTITY_VARIABLE"]; ?>"
					  data-varId="<?= $arParams['PRODUCT_ID_VARIABLE'] ?>"
					  data-varAct="<?=$arParams['ACTION_VARIABLE']?>">
					<input type="hidden" name="action" value="ADD2BASKET"/>
					<input type="hidden" name="product_name" value="<?=$arResult['NAME']?>"/>
					<input type="hidden" name="product_img" value="<?=$arResult['PICTURE']?>"/>
					<div class="item_manage__price">
						<?if(method_exists($this, 'createFrame')) $frame = $this->createFrame()->begin();?>
							<?if ($arResult['MIN_PRICE']['PRINT_VALUE'] != $arResult['MIN_PRICE']['PRINT_DISCOUNT_VALUE']):?>
								<del class="old_price"><?= $arResult['MIN_PRICE']['PRINT_VALUE']; ?></del>
							<?endif?>
							<ins class="new_price"><?=$arResult['MIN_PRICE']['PRINT_DISCOUNT_VALUE']?></ins>
						<?if(method_exists($this, 'createFrame')) $frame->end();?>
					</div>
					<div class="item_manage__price_note">*
						<?=GetMessage("RZ_TCENA_UKAZANA_ZA")?>
						<?= (intval($arResult['CATALOG_MEASURE_RATIO']) > 0) ? $arResult['CATALOG_MEASURE_RATIO']: 1 ?>
						<?= (isset($arResult['CATALOG_MEASURE_NAME'])) ? $arResult['CATALOG_MEASURE_NAME'] : GetMessage("RZ_SHT") ?>.
					</div>
					<div class="item_manage__amount">
						<input class="js-touchspin" type="text" value="<?=$arResult['DEFAULT_QUANT']?>" name="<?= $arParams["PRODUCT_QUANTITY_VARIABLE"]; ?>" >
					</div>
					<div class="item_manage__submit">
						<?
						$hasOfferSelect = false;
						if(isset($arResult['FLAT_OFFERS']) && count($arResult['FLAT_OFFERS']) > 0 ) $hasOfferSelect = true;
						?>
						<? if($hasOfferSelect):?>
							<select class="offer_detail_select offer_select" name="<?= $arParams['PRODUCT_ID_VARIABLE'] ?>" title="">
								<?
								$f = true;
								if (!empty($arResult['CUR_OFFER'])) {
									$f = $arResult['CUR_OFFER'];
								}
								foreach($arResult['FLAT_OFFERS'] as $id => $arOffer):?>
									<option
										<?if(isset($arOffer['PRICE'])):?>
											data-price="<?=$arOffer['PRICE']?>"
										<?endif?>
										<?if(isset($arOffer['PRICE_OLD'])):?>
											data-priceold="<?=$arOffer['PRICE_OLD']?>"
										<?endif?>
										<?if(isset($arOffer['NAME'])):?>
											data-name="<?=$arOffer['NAME']?>"
										<?endif?>
										<?if(isset($arOffer['CATCH_BUY']['TIMER'])):?>
											data-timer="<?=$arOffer['CATCH_BUY']['TIMER']?>"
										<?endif?>
										<?if(isset($arOffer['CATCH_BUY']['PROGRESS'])):?>
											data-progres="<?=$arOffer['CATCH_BUY']['PROGRESS']?>"
										<?endif?>
										data-canbuy="<?= ($arOffer['CAN_BUY']) ? '1' : '0' ?>"
										value="<?= $id ?>"
										<?if($f === true):?>
											selected
										<?$f = false; elseif(is_numeric($f) && $f == $id):?> selected
										<?$OfferId = $id;?>
										<? endif;?>>
										<?=trim($arOffer['NAME'])?>
									</option>
								<?endforeach?>
							</select>
						<?else:?>
							<input type="hidden" name="<?=$arParams['PRODUCT_ID_VARIABLE']?>" value="<?=$arResult['ID']?>"/>
							<? CRZShinmarket::printParamsOfferList($arResult, $arParams['PRODUCT_PROPERTIES']) ?>
						<?endif;?>

							<?if(method_exists($this, 'createFrame')) $frame = $this->createFrame()->begin();?>
							<button class="btn btn-primary btn-buy" <?if($arResult['CAN_BUY'] != 'Y'):?> disabled<?endif?> type="submit"><?=GetMessage("CT_BCE_CATALOG_ADD")?></button>
							<? if ($arResult['CAN_BUY'] == 'Y' && $arParams['USE_ONE_CLICK'] == 'Y' && CModule::IncludeModule('sale') && CModule::IncludeModule('yenisite.oneclick')): ?>
							<?
							/*$APPLICATION->IncludeComponent(
								"yenisite:oneclick.buy",
								"ajax",
								array(
									"PERSON_TYPE_ID" => $arParams["ONECLICK_PERSON_TYPE_ID"],
									"SHOW_FIELDS" => $arParams["ONECLICK_SHOW_FIELDS"],
									"REQ_FIELDS" => $arParams["ONECLICK_REQ_FIELDS"],
									"ALLOW_AUTO_REGISTER" => $arParams["ONECLICK_ALLOW_AUTO_REGISTER"],
									"IBLOCK_TYPE" => $arParams['IBLOCK_TYPE'],
									"QUANTITY" => $arResult['DEFAULT_QUANT'],
									"IBLOCK_ID" => $arParams['IBLOCK_ID'],
									"IBLOCK_ELEMENT_ID" =>($OfferId ? $OfferId : $arResult['ID']),
									"USE_CAPTCHA" => $arParams["ONECLICK_USE_CAPTCHA"],
									"MESSAGE_OK" => $arParams["~ONECLICK_MESSAGE_OK"],
									"PAY_SYSTEM_ID" => $arParams["ONECLICK_PAY_SYSTEM_ID"],
									"DELIVERY_ID" => $arParams["ONECLICK_DELIVERY_ID"],
									"AS_EMAIL" => $arParams["ONECLICK_AS_EMAIL"],
									"AS_NAME" => $arParams["ONECLICK_AS_NAME"],
									"BUTTON_TYPE" => "HREF",
									"SEND_REGISTER_EMAIL" => $arParams["ONECLICK_SEND_REGISTER_EMAIL"],
									"FIELD_CLASS" => "form-control",
									"FIELD_PLACEHOLDER" => $arParams["ONECLICK_FIELD_PLACEHOLDER"],
									"FIELD_QUANTITY" => $arParams["ONECLICK_FIELD_QUANTITY"],
									'OFFER_PROPS' => $arParams['OFFER_TREE_PROPS'],
									'NAME' => $arResult['NAME']
								),
								$component
							);
*/
							?>
						<? endif ?>
						<br/>
						<br/>
						<?if(method_exists($this, 'createFrame')) $frame->end();?>
					</div>

				</form>
				<!-- <div class="item_manage__inner item_manage__manage">
					<div>
						<a href="<?//=$arResult['COMPARE_URL']?>" class="compare_link compare_id_<?=$arResult['ID']?>">
							<span class="icon icon_measuring7"></span>
							<span class="text in"><?//=GetMessage("RZ_SRAVNIT_")?></span>
							<span class="text out" title="<?//=GetMessage("RZ_UBRAT__IZ_SRAVNENIYA")?>"><?//=GetMessage("RZ_V_SRAVNENII")?></span>
						</a>
						<a href="javascript:" class="favourite_link favourite_link_item fav_id_<?//=$arResult['ID']?>" data-id="<?//=$arResult['ID']?>">
							<span class="icon icon_new6"></span>
							<span class="text in"><?//=GetMessage('RZ_V_IZBRANNOE')?></span>
							<span class="text out" title="<?//=GetMessage("RZ_UBRAT__IZ_IZBRANNOGO")?>"><?//=GetMessage("RZ_V_IZBRANNOM")?></span>
						</a>
					</div>
					<div>
						<span class="inform_link" data-toggle="modal" data-target="#modal-follow_price">
							<span class="icon icon_arrow97"></span>
							<?//=GetMessage("RZ_SOOBSHIT__O_SNIZHENII_TCENI")?>
						</span>
					</div>
					<div>
						<span class="complain_link" data-toggle="modal" data-target="#modal-complain">
							<span class="icon icon_comment2"></span>
							<?//=GetMessage("RZ_POZHALOVAT_SYA_NA_TCENU")?>
						</span>
					</div>
				</div> -->
				<div class="item_manage__inner item_manage__avalible" style="line-height: 50px;">
				<!--<?=GetMessage("RZ_TOVAR_V_NALICHII")?> column & space : column & space -->
					<?if(method_exists($this, 'createFrame')) $frame = $this->createFrame()->begin();?>
					<?if($arResult['CAN_BUY'] == "Y"):?>
						<?=GetMessage("RZ_TOVAR_V_NALICHII")?>
						<!-- <span class="avalible"><span class="da"><?//=GetMessage("RZ_DA")?></span><span class="net"><?//=GetMessage("RZ_NET")?></span></span> -->
						<!-- <br/> -->
						<?
						if ($arParams["USE_STORE"] == "Y" && \Bitrix\Main\ModuleManager::isModuleInstalled("catalog")):
							$storeTpl = (CModule::IncludeModule('yenisite.storeamount')) ? 'romza_unic' : '.default';
							?>
							<?
						// 	$APPLICATION->IncludeComponent(
						// 	"bitrix:catalog.store.amount",
						// 	$storeTpl,
						// 	array(
						// 		"PER_PAGE" => "10",
						// 		"USE_STORE_PHONE" => $arParams["USE_STORE_PHONE"],
						// 		"SCHEDULE" => $arParams["USE_STORE_SCHEDULE"],
						// 		"ELEMENT_ID" => $arResult['ID'],
						// 		"CACHE_TYPE" => $arParams['CACHE_TYPE'],
						// 		"CACHE_TIME" => $arParams['CACHE_TIME'],
						// 		"FIELDS" => array(
						// 			0 => "TITLE",
						// 			1 => "DESCRIPTION",
						// 			2 => "PHONE",
						// 			3 => "EMAIL",
						// 			4 => "IMAGE_ID",
						// 			5 => "COORDINATES",
						// 			6 => "SCHEDULE",
						// 			7 => "ADDRESS"
						// 		),
						// 		"SHOW_EMPTY_STORE" => $arParams["SHOW_EMPTY_STORE"],
						// 		"WIDTH" => "450",
						// 		"MANY_VAL" => $arParams["STORE_MANY_VAL"], // 10
						// 		"AVERAGE_VAL" => $arParams["STORE_AVERAGE_VAL"], // 5,
						// 		"ONE_POPUP" => "Y",
						// 		'ONLY_NAME_IN_LIST' => 'Y',
						// 		"USE_MIN_AMOUNT" => "N",
						// 		'IMG_WIDTH' => '400',
						// 		'IMG_HEIGHT' => '200',
						// 		'STORES' => $arParams['STORES'],
						// 		'CUR_OFFER' => $arResult['CUR_OFFER'],
						// 	),
						// 	$component
						// );
						?>
						<?endif?>
					<?else:?>
						<?=GetMessage("RZ_TOVAR_POD_ZAKAZ")?>
						<!-- Под заказ -->
						<!-- Передать ID или иное описание товара в форму заказа -->
						<!-- <a href="/order/">Под заказ</a> -->
					<?endif;?>
					<?if(method_exists($this, 'createFrame')) $frame->end();?>
				</div>
			</div>
		</div>

		<div class="item_specification_wrapper">
            <?if(Bitrix\Main\Loader::includeModule('yenisite.infoblockpropsplus')): ?>
                    <?$APPLICATION->IncludeComponent(
                        "yenisite:ipep.props_groups",
                        "",
                        Array(
                            "COLOR_SCHEME" => "",
                            "DISPLAY_PROPERTIES" => $arResult["DISPLAY_PROPERTIES"],
                            "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                            "SHOW_PROPERTY_VALUE_DESCRIPTION" => "Y",
                            "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                            "CACHE_TIME" => $arParams["CACHE_TIME"]
                        ),
                        $component
                    );?>
            <?else:?>
                <ul class="item_specification__list">
                        <?foreach($arResult["DISPLAY_PROPERTIES"] as $arProp):?>
                            <li>
                                <?if(empty($arProp['HINT'])):?>
                                    <?= $arProp["NAME"] ?>
                                <?else:?>
                                    <span <?if(!empty($arProp["HINT"])):?> class="hint-have"<?endif?> title="<?=$arProp["HINT"]?>"><?= $arProp["NAME"] ?></span>
                                <?endif?>
                                <span class="divider">---------</span>
                                <b>
                                    <?
                                    if ($arProp['PROPERTY_TYPE'] == 'L'):
                                        if (is_array($arProp['DISPLAY_VALUE'])):
                                            foreach ($arProp['DISPLAY_VALUE'] as $n => $value):
                                                echo $n > 0 ? ', ' : '';
                                                echo $arProp['DISPLAY_VALUE'][$n];
                                            endforeach;
                                        else:
                                            echo $arProp['DISPLAY_VALUE'];
                                        endif;
                                    else:
                                        if (is_array($arProp["DISPLAY_VALUE"]) && $arProp['PROPERTY_TYPE'] != 'F'):
                                            foreach ($arProp["DISPLAY_VALUE"] as &$p) {
                                                if (substr_count($p, "a href") > 0) {
                                                    $p = strip_tags($p);
                                                }
                                            }
                                            echo implode("&nbsp;/&nbsp;", $arProp["DISPLAY_VALUE"]);
                                        elseif ($pid && $pid == "MANUAL"):
                                            ?>
                                            <a href="<?= $arProp["VALUE"] ?>"><?= GetMessage("CATALOG_DOWNLOAD") ?></a><?
                                        elseif ($arProp['PROPERTY_TYPE'] == 'F'):
                                            if ($arProp['MULTIPLE'] == 'Y'):
                                                if (is_array($arProp['DISPLAY_VALUE'])):
                                                    foreach ($arProp['DISPLAY_VALUE'] as $n => $value):
                                                        echo $n > 0 ? ', ' : '';
                                                        echo str_replace('</a>', ' ' . $arProp['DESCRIPTION'][$n] . '</a>', $value);
                                                    endforeach;
                                                else:
                                                    echo str_replace('</a>', ' ' . $arProp['DESCRIPTION'][0] . '</a>', $arProp['DISPLAY_VALUE']);
                                                endif;
                                            else:
                                                echo str_replace('</a>', ' ' . $arProp['DESCRIPTION'] . '</a>', $arProp['DISPLAY_VALUE']);
                                            endif;
                                        else:
                                            if (substr_count($arProp["DISPLAY_VALUE"], "a href") > 0) {
                                                $arProp["DISPLAY_VALUE"] = strip_tags($arProp["DISPLAY_VALUE"]);
                                            }
                                            if (isset($arProp['ICON_CLASS']) && strlen($arProp['ICON_CLASS']) > 0) {
                                                echo '<span class="icon ',$arProp['ICON_CLASS'],'" title="',$arProp["DISPLAY_VALUE"],'"></span>';
                                            } else {
                                                echo $arProp["DISPLAY_VALUE"];
                                            }
                                            if (false && $arParams['SHOW_PROPERTY_VALUE_DESCRIPTION'] != 'N') {
                                                echo ' ', $arProp['DESCRIPTION'];
                                            }
                                        endif;
                                    endif;
                                    ?>
                                </b>
                            </li>
                        <?endforeach;?>
                    </ul>
            <? endif ?>
		</div>
	</div>

	<div class="item_full_info_block">
		<ul class="item_full_info__tabs nav nav-tabs" role="tablist">
			<?
			$hasActiveTab = false;
			if(strlen($arResult['DETAIL_TEXT']) > 0): ?>
				<li class="item_full_info__tabs_item<?if(!$hasActiveTab):?> active<?endif?>"><a href="#description" role="tab" data-toggle="tab"><?=GetMessage("RZ_OPISANIE")?></a></li>
			<? $hasActiveTab = 'description'; endif;?>
			<? if ('Y' == $arParams['USE_COMMENTS']): ?>
				<li class="item_full_info__tabs_item<?if(!$hasActiveTab):?> active<?endif?>"><a href="#feedbacks" role="tab" data-toggle="tab"><?=GetMessage("RZ_OTZIVI_POKUPATELEJ")?></a></li>
			<? if(!$hasActiveTab) {
					$hasActiveTab = 'feedbacks';
				}
			endif;?>
			<?/*?>
			<li class="item_full_info__tabs_item"><a href="#reviews" role="tab" data-toggle="tab"><?=GetMessage("RZ_STAT_I_I_OBZORI")?></a></li>
			<?*/?>
		</ul>

		<div class="item_full_info__tabs_content tab-content">
			<?if(strlen($arResult['DETAIL_TEXT']) > 0):?>
				<div class="tab-pane<?if($hasActiveTab == 'description'):?> active<?endif?>" id="description">
					<?=$arResult['DETAIL_TEXT']?>
				</div>
			<?endif;?>
			<? if ('Y' == $arParams['USE_COMMENTS']):?>
				<div class="tab-pane<?if($hasActiveTab == 'feedbacks'):?> active<?endif?>" id="feedbacks">
					<h3><?=GetMessage("RZ_OTZIVI_POKUPATELEJ")?></h3>
					<? if (CModule::IncludeModule('forum')): ?>
					<?$APPLICATION->IncludeComponent(
						"bitrix:forum.topic.reviews",
						"reviews",
						Array(
							"CACHE_TYPE" => $arParams["CACHE_TYPE"],
							"CACHE_TIME" => $arParams["CACHE_TIME"],
							"MESSAGES_PER_PAGE" => $arParams["MESSAGES_PER_PAGE"],
							"USE_CAPTCHA" => $arParams["USE_CAPTCHA"],
							"PATH_TO_SMILE" => $arParams["PATH_TO_SMILE"],
							"FORUM_ID" => $arParams["FORUM_ID"],
							"URL_TEMPLATES_READ" => $arParams["URL_TEMPLATES_READ"],
							"SHOW_LINK_TO_FORUM" => $arParams["SHOW_LINK_TO_FORUM"],
							"ELEMENT_ID" => $arResult['ID'],
							"IBLOCK_ID" => $arParams["IBLOCK_ID"],
							"AJAX_POST" => $arParams["REVIEW_AJAX_POST"],
							"POST_FIRST_MESSAGE" => $arParams["POST_FIRST_MESSAGE"],
							"URL_TEMPLATES_DETAIL" => $arParams["POST_FIRST_MESSAGE"]==="Y"? $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"] :"",
						),
						$component
					);?>
					<? else: ?>
						<? $APPLICATION->IncludeComponent(
							"yenisite:feedback",
							"product",
							array(
								"IBLOCK_TYPE" => $arParams['RZ_COMMENTS_IBLOCK_TYPE'],
								"IBLOCK" => $arParams['RZ_COMMENTS_IBLOCK_ID'],
								"NAME_FIELD" => $arParams['RZ_COMMENTS_NAME_FIELD'],
								"SECTION_CODE" => "ITEM_REVIEW_" . $arResult['ID'],
								"TITLE" => "",
								"SUCCESS_TEXT" => $arParams['RZ_COMMENTS_SUCCESS_TEXT'],
								"ACTIVE" => "N",
								"ALLOW_RESPONSE" => "Y",
								"ALWAYS_SHOW_PAGES" => "N",
								"USE_CAPTCHA" => "Y",
								"TEXT_SHOW" => "Y",
								"TEXT_REQUIRED" => "Y",
								"PRINT_FIELDS" => $arParams['RZ_COMMENTS_PRINT_FIELDS'],
								"MESS_PER_PAGE" => "10",
								"ELEMENT_ID" => $arResult['ID'],
								"SEF_MODE" => "Y",
								"SEF_FOLDER" => $APPLICATION->GetCurDir(),
								"AJAX_MODE" => "N",
								"AJAX_OPTION_JUMP" => "N",
								"AJAX_OPTION_STYLE" => "N",
								"AJAX_OPTION_HISTORY" => "N",
								"CACHE_TYPE" => "A",
								"CACHE_TIME" => "300",
								"NAME" => $arParams['RZ_COMMENTS_NAME'],
								"EMAIL" => $arParams['RZ_COMMENTS_EMAIL'],
								"PHONE" => $arParams['RZ_COMMENTS_PHONE'],
							),
							$component
						); ?>
					<? endif ?>
				</div>
			<?endif;?>
			<?/*?>
			<div class="tab-pane" id="reviews">
				<h3><?=GetMessage("RZ_STAT_I_I_OBZORI")?></h3>
			</div>
			<?*/?>
		</div>
	</div>
	<?if(CBXFeatures::IsFeatureEnabled('CatCompleteSet')):?>
		<div class="benefits_goods_wrapper">
			<?
			if ($arResult['MODULES']['catalog']) {?>
				<?$APPLICATION->IncludeComponent("bitrix:catalog.set.constructor",
					".default",
					array(
						"IBLOCK_ID" => $arParams["IBLOCK_ID"],
						"ELEMENT_ID" => $arResult["ID"],
						"PRICE_CODE" => $arParams["PRICE_CODE"],
						"BASKET_URL" => $arParams["BASKET_URL"],
						"CACHE_TYPE" => $arParams["CACHE_TYPE"],
						"CACHE_TIME" => $arParams["CACHE_TIME"],
						"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
						'VOTE_DISPLAY_AS_RATING' => $arParams['VOTE_DISPLAY_AS_RATING'],
						'RESIZER_PRODUCT' => $arParams['RESIZER_PRODUCT'],
					),
					$component,
					array("HIDE_ICONS" => "Y")
				);?><?
			}
			?>
	</div>
	<?endif;?>
	<?if(!empty($arResult['OFFERS'])):?>
		<?include(__DIR__.'/offers_table.php')?>
	<?endif?>
	<div class="item_similar_goods">

		<?if(!empty($arResult['PROPERTIES']['MANUFACTURER']['VALUE'])):?>
			<?
			global ${$arParams['FILTER_NAME']};
			${$arParams['FILTER_NAME']} = array('!ID' => $arResult['ID']);
			?>
			<?

			// $APPLICATION->IncludeComponent("romza:catalog.top", "in_element", array(
			// 		"IBLOCK_TYPE" => $arParams['IBLOCK_TYPE'],
			// 		"IBLOCK_ID" => $arParams['IBLOCK_ID'],
			// 		"ELEMENT_SORT_FIELD" => "name",
			// 		"PROPERTY_FILTER_CODE" => "MANUFACTURER",
			// 		"PROPERTY_FILTER_VALUE" => $arResult['PROPERTIES']['MANUFACTURER']['VALUE'],
			// 		"PROPERTY_TITLE" => GetMessage('RZ_PROPERTY_TITLE_MANUF'),
			// 		"SECTIONS_TO_DISPLAY" => array(),
			// 		"ELEMENT_SORT_ORDER" => "asc",
			// 		"ELEMENT_SORT_FIELD2" => "name",
			// 		"ELEMENT_SORT_ORDER2" => "asc",
			// 		"FILTER_NAME" => $arParams['FILTER_NAME'],
			// 		"HIDE_NOT_AVAILABLE" => "N",
			// 		"ELEMENT_COUNT" => "24",
			// 		"LINE_ELEMENT_COUNT" => "4",
			// 		"PROPERTY_CODE" => $arParams['PROPERTY_CODE'],
			// 		"OFFERS_FIELD_CODE" => $arParams['OFFERS_FIELD_CODE'],
			// 		"OFFERS_PROPERTY_CODE" => $arParams['OFFERS_PROPERTY_CODE'],
			// 		"OFFERS_SORT_FIELD" => "sort",
			// 		"OFFERS_SORT_ORDER" => "asc",
			// 		"OFFERS_SORT_FIELD2" => "id",
			// 		"OFFERS_SORT_ORDER2" => "desc",
			// 		"OFFERS_LIMIT" => "0",
			// 		"ADD_PICT_PROP" => $arParams['ADD_PICT_PROP'],
			// 		"LABEL_PROP" => $arParams['NEWPRODUCT'],
			// 		"SHOW_DISCOUNT_PERCENT" => "Y",
			// 		"SHOW_OLD_PRICE" => "Y",
			// 		"MESS_BTN_BUY" => $arParams['MESS_BTN_BUY'],
			// 		"MESS_BTN_ADD_TO_BASKET" => $arParams['MESS_BTN_ADD_TO_BASKET'],
			// 		"MESS_BTN_DETAIL" => $arParams['MESS_BTN_DETAIL'],
			// 		"MESS_NOT_AVAILABLE" => $arParams['MESS_NOT_AVAILABLE'],
			// 		"SECTION_URL" => $arParams['SECTION_URL'],
			// 		"DETAIL_URL" => "",
			// 		"SECTION_ID_VARIABLE" => "SECTION_ID",
			// 		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
			// 		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
			// 		"CACHE_TIME" => $arParams["CACHE_TIME"],
			// 		"CACHE_GROUPS" => $arParams['CACHE_GROUPS'],
			// 		"DISPLAY_COMPARE" => "N",
			// 		"CACHE_FILTER" => "Y",
			// 		"PRICE_CODE" => $arParams['PRICE_CODE'],
			// 		"USE_PRICE_COUNT" => $arParams['USE_PRICE_COUNT'],
			// 		"SHOW_PRICE_COUNT" => $arParams['SHOW_PRICE_COUNT'],
			// 		"PRICE_VAT_INCLUDE" => $arParams['PRICE_VAT_INCLUDE'],
			// 		"CONVERT_CURRENCY" => $arParams['CONVERT_CURRENCY'],
			// 		"BASKET_URL" => $arParams['BASKET_URL'],
			// 		"ACTION_VARIABLE" => $arParams['ACTION_VARIABLE'],
			// 		"PRODUCT_ID_VARIABLE" => $arParams['PRODUCT_ID_VARIABLE'],
			// 		"USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
			// 		"ADD_PROPERTIES_TO_BASKET" => $arParams['ADD_PROPERTIES_TO_BASKET'],
			// 		"PRODUCT_PROPS_VARIABLE" => $arParams['PRODUCT_PROPS_VARIABLE'],
			// 		"PARTIAL_PRODUCT_PROPERTIES" => $arParams['PARTIAL_PRODUCT_PROPERTIES'],
			// 		"PRODUCT_PROPERTIES" => $arParams['PRODUCT_PROPERTIES'],
			// 		"OFFERS_CART_PROPERTIES" => $arParams['OFFERS_CART_PROPERTIES'],
			// 		'RESIZER_PRODUCT' => $arParams['RESIZER_PRODUCT'],
			// 		'ONECLICK_PERSON_TYPE_ID' => $arParams["ONECLICK_PERSON_TYPE_ID"],
			// 		'ONECLICK_SHOW_FIELDS' => $arParams["ONECLICK_SHOW_FIELDS"],
			// 		'ONECLICK_REQ_FIELDS' => $arParams["ONECLICK_REQ_FIELDS"],
			// 		'ONECLICK_ALLOW_AUTO_REGISTER' => $arParams["ONECLICK_ALLOW_AUTO_REGISTER"],
			// 		'ONECLICK_USE_CAPTCHA' => $arParams["ONECLICK_USE_CAPTCHA"],
			// 		'ONECLICK_MESSAGE_OK' => $arParams["ONECLICK_MESSAGE_OK"],
			// 		'ONECLICK_PAY_SYSTEM_ID' => $arParams["ONECLICK_PAY_SYSTEM_ID"],
			// 		'ONECLICK_DELIVERY_ID' => $arParams["ONECLICK_DELIVERY_ID"],
			// 		'ONECLICK_AS_EMAIL' => $arParams["ONECLICK_AS_EMAIL"],
			// 		'ONECLICK_AS_NAME' => $arParams["ONECLICK_AS_NAME"],
			// 		'ONECLICK_SEND_REGISTER_EMAIL' => $arParams["ONECLICK_SEND_REGISTER_EMAIL"],
			// 		'ONECLICK_FIELD_PLACEHOLDER' => $arParams["ONECLICK_FIELD_PLACEHOLDER"],
			// 		'ONECLICK_FIELD_QUANTITY' => $arParams["ONECLICK_FIELD_QUANTITY"],
			// 		"CURRENCY_ID" => $arParams['CURRENCY_ID'],
			// 		'SHOW_CATCH_BUY' => $arParams['SHOW_CATCH_BUY'],
			// 		'USE_ONE_CLICK' => $arParams['USE_ONE_CLICK'],
			// 	),
			// 	$component
			// );

			?>
		<?endif;?>
	</div>
</div>
<?$this->SetViewTarget('modal_lightbox')?>
<div class="modal" id="modal-lightbox" tabindex="-1" role="dialog"
<?if(count($arResult["MORE_PHOTO"]) > 1):?>
	 <?//aria-labelledby="lightbox_modalLabel"?>
<?endif?>
	 aria-hidden="true">
	<div class="popup modal-dialog popup_lightbox">
		<span class="close flaticon-delete30" data-dismiss="modal"></span>
		<div class="item_gallery__big_wrapper">
			<div class="img-wrap" data-dismiss="modal">
				<img src="<?=$firstPhoto['SRC']?>" alt="" title="">
			</div>
		</div>
		<?if(count($arResult["MORE_PHOTO"]) > 1):?>
			<div class="item_gallery__tumbs_wrapper">
				<div class="frame" id="modal-thumbs">
					<div class="item_gallery__tumbs">
						<?foreach ($arResult["MORE_PHOTO"] as $arPhoto):?>
							<div class="item">
								<img src="<?=$arPhoto['THUMB_SRC']?>"
									 alt="<?=$arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_ALT']?>"
									 title="<?=$arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_TITLE']?>"
									 data-src-big="<?=$arPhoto['GALLERY_SRC']?>"/>
							</div>
						<?endforeach?>
					</div>
				</div>
				<div class="item_gallery__tumbs__navigate">
					<span class="btn btn-default btn-arrow flaticon-arrow133 btn-prev"></span>
					<span class="data">
						<?=GetMessage("RZ_FOTO")?>
						<span class="current"></span>/<span class="total"></span>
					</span>
					<span class="btn btn-default btn-arrow flaticon-right20 btn-next"></span>
				</div>
			</div>
		<?endif?>
	</div>
</div>
<?$this->EndViewTarget()?>
<?$this->SetViewTarget('modal_constructor')?>
	<div class="modal fade" id="CatalogSetConstructor_<?=$arResult['ID']?>" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="popup modal-dialog">
			<span class="close flaticon-delete30" data-dismiss="modal"></span>
			<div class="modal-container"></div>
		</div>
	</div>
<?$this->EndViewTarget()?>