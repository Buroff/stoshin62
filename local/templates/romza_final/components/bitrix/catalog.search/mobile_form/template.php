<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?if (method_exists($this, 'setFrameMode')) $this->setFrameMode(true); ?>
<?$curSectionId = intval($_GET['SECTION_ID'])?>
<form class="mobile_search_form" action="<?=$arParams['SEARCH_PAGE_URL']?>" method="get">
	<input name="q"  value="<?=htmlspecialcharsbx($_GET['q'])?>" class='mobile_search_form__input form-control' type="search">
	<span class="flaticon-delete30 search-clear"></span>
	<input type="hidden" name="how" value="<?echo $arResult["REQUEST"]["HOW"]=="d"? "d": "r"?>" />
</form>
<a href="#" class="btn  mobile-serach-btn btn-search">
	<span class="icon icon_magnifier13"></span>
</a>
