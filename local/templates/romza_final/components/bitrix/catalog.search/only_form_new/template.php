<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?if (method_exists($this, 'setFrameMode')) $this->setFrameMode(true); ?>

<?$curSectionId = intval($_GET['SECTION_ID'])?>
<form class="page_header__search_form" style="" action="<?=$arParams['SEARCH_PAGE_URL']?>" method="get">

    <button class="btn btn-primary find-btn" type="submit">
		<span class="find-btn-title"><?=GetMessage("SEARCH_GO")?></span>
		<span class="flaticon-magnifying42"></span>
	</button>

	<div class="page_header__search_form__field_wrapper">
		<input class="page_header__search_form__field" type="search" name="q" placeholder="<?=GetMessage("RZ_CHTO_ISHEM___")?>"
			   value="<?=htmlspecialcharsbx($_GET['q'])?>">
		<span class="flaticon-delete30 search-clear" data-toggle="tooltip" title="<?=GetMessage("RZ_OCHISTIT__POISK")?>" data-placement="left"></span>
	</div>
	<input type="hidden" name="how" value="<?echo $arResult["REQUEST"]["HOW"]=="d"? "d": "r"?>" />

</form> 

