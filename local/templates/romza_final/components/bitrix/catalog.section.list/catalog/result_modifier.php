<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); 

// echo "<pre style='text-align:left;'>";print_r($arResult);echo "</pre>";

$arParams['RESIZER_SET'] = $arParams['VIEW_MODE'] == 'inline' ? $arParams['RESIZER_LINE_SECTIONS_IMG'] : $arParams['RESIZER_SECTIONS_IMG'];

foreach ($arResult['SECTIONS'] as &$arSection) {
	$arSection['PICTURE'] = CRZShinmarket::getSectionPictureById($arSection['ID'], $arParams['RESIZER_SET']);
}

unset($arSection);
if(!empty($arResult['SECTION']['PICTURE'] )) {
	$arResult['SECTION']['PICTURE'] = CRZShinmarket::getSectionPictureById($arResult['SECTION']['ID'], $arParams['RESIZER_SECTION_IMG']);
}