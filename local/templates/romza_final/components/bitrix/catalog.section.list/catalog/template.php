<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
if (method_exists($this, 'setFrameMode')) $this->setFrameMode(true);

// if(empty($arResult['SECTIONS']))
// {
// return false;
// }
include $_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH.'/include/debug_info_dynamic.php';
?>
<?if ($arParams['SHOW_DESCRIPTION']): ?>
	<div class="catalog-description-block">
		<div class="catalog-description clearfix <? if(!$arParams['FULL_DESCTRIPTION']):?>not-full<?endif?> minified">
			<?if(!empty($arResult['SECTION']['PICTURE'])):?>
				<div class="description-image">
					<img src="<?=$arResult['SECTION']['PICTURE']?>" alt="<?=$arResult['SECTION']['NAME']?>">
				</div>
			<?endif?>
			<?= $arResult['SECTION']['DESCRIPTION'] ?>
			<?if(!empty($arResult['SECTION']['DESCRIPTION'])):?>
				<button type="button" class="height-toggle"><span><? if(!$arParams['FULL_DESCTRIPTION']):?><?=GetMessage('FURNITURE_SHOW_MORE')?><?else:?><?=GetMessage('FURNITURE_DESC_HIDE_FULL')?><?endif?></span></button>
			<?endif?>
		</div>
	</div>
<? endif ?>
<?if ($arParams['SHOW_SUBSECTIONS']): ?>
	<div class="sub-categories__block">
		<div class="sub-categories" data-sub-view="<?=$arParams['VIEW_MODE']?>">
			<? foreach ($arResult['SECTIONS'] as $arSection): ?>
				<a class="item__sub-category" <?if(!empty($arSection['PICTURE'])):?> data-toggle="tooltip" data-placement="top" <?endif?> href="<?= $arSection['SECTION_PAGE_URL'] ?>">
	<? if($arParams['VIEW_MODE'] != 'TEXT'):?>
			<div class="img__block">
		<?if(!empty($arSection['PICTURE'])): ?>
						<img src="<?=$arSection['PICTURE']?>" alt="<?=$arSection['NAME']?>" title="<?=$arSection['NAME']?>" class="subcategory-img">
		<?endif?>
					</div>
	<? endif ?>

	<? if($arParams['VIEW_MODE'] != 'PICTURE'): ?>
			<span class="text">
				<span><?= $arSection['NAME'] ?></span>
				<sup><?= $arSection['ELEMENT_CNT'] ?></sup>
			</span>
	<? endif ?>
				</a>
			<? endforeach ?>
		</div>
	</div>
<? endif ?>

