<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Yenisite\Core\Tools;
$isAjax = Tools::isAjax();
$this->setFrameMode(true);

if (!$isAjax && method_exists($this, 'createFrame')) $frame = $this->createFrame()->begin(''); ?>
		<span class="count"><?=$arResult['COUNT']?></span>

<script type="text/javascript">
	<?foreach($arResult['ITEMS'] as $arItem):?>
		if ('WISH_LIST' in rz) {
			rz.WISH_LIST.push(<?=$arItem['ID']?>);
		} else {
			rz.WISH_LIST = [<?=$arItem['ID']?>];
		}
	<?endforeach?>
</script>
<? if (!$isAjax && method_exists($this, 'createFrame')) $frame->end(); ?>

