<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $templateData */
/** @var @global CMain $APPLICATION */
use \Yenisite\Core\Tools;
include_once $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/tools/include_module.php';
Tools::addComponentDeferredJS($templateFile);
CJSCore::RegisterExt('rz_main_list', array(
	'lang' => $templateFolder.'/lang/'.LANGUAGE_ID.'/script_deferred.php',
	'skip_core' => true
));
CJSCore::Init(array('rz_main_list'));