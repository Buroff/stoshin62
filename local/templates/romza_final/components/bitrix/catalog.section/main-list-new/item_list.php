<?if(!$arParams['PERSONAL_PAGE']):?>
	<div class="goods_list <?= $viewMode ?>">
<?endif?>
	<?
	$boolFirst = true;
	$arRowIDs = array();
	foreach ($arItems as $keyItem => $arItem) {
		$arCatchBuy = $arParams['CATCHBUY'][$arItem['ID']];
		$bTimer = !empty($arCatchBuy['ACTIVE_TO']);
		$bProgressBar = $arCatchBuy['MAX_USES'] > 0;

		if (!$arParams['SHOW_CATCH_BUY']) {
			$bTimer = false;
			$bProgressBar = false;
			$arItem['CATCHBUY']['HAS_OFFERS'] = false;
		};
		$arCatchBuy['PERCENT'] = ($bProgressBar) ? $arCatchBuy['COUNT_USES'] / $arCatchBuy['MAX_USES'] * 100 : 0;

		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
		$strMainID = $this->GetEditAreaId($arItem['ID']);

		$strTitle = (
		isset($arItem["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_TITLE"]) && '' != isset($arItem["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_TITLE"])
			? $arItem["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_TITLE"]
			: $arItem['NAME']
		);
		$strTitle = htmlspecialcharsbx($strTitle);
		$hasOfferSelect = false;
		if(isset($arItem['FLAT_OFFERS']) && count($arItem['FLAT_OFFERS']) > 0 ) $hasOfferSelect = true;
		$arPropsHtml = CRZShinmarket::printParamsOfferList($arItem, $arParams['PRODUCT_PROPERTIES'], false, false, true);
		?>
		<div class="goods_item<? if ($arItem['SECOND_PICT']):?> with_second_pic<?endif;?><?if($hasOfferSelect || $arPropsHtml['COUNT'] > 0):?> has_offer_select<?endif?>">
			<div class="goods_item__inner">
				<div class="goods_item__top">
					<? $APPLICATION->IncludeComponent("yenisite:stickers", "", array(
						"ELEMENT" => $arItem,
						"STICKER_NEW" => $arParams['STICKER_NEW'],
						"STICKER_HIT" => $arParams['STICKER_HIT'],
						"TAB_PROPERTY_NEW" => $arParams['TAB_PROPERTY_NEW'],
						"TAB_PROPERTY_HIT" => $arParams['TAB_PROPERTY_HIT'],
						"TAB_PROPERTY_SALE" => $arParams['TAB_PROPERTY_SALE'],
						"TAB_PROPERTY_BESTSELLER" => $arParams['TAB_PROPERTY_BESTSELLER'],
						"MAIN_SP_ON_AUTO_NEW" => $arParams['MAIN_SP_ON_AUTO_NEW'],
						"SHOW_DISCOUNT_PERCENT" => $arParams['SHOW_DISCOUNT_PERCENT'],
					),
						$component, array("HIDE_ICONS" => "Y")
					); ?>
					<a href="<?=$arItem['DETAIL_PAGE_URL']; ?>"
					   class="goods_item__img_wrapper" >
						<img src="<?=$arItem['PICTURE'] ?>"
							 alt="<?= $strTitle; ?>" title="<?= $strTitle; ?>"
							 class="goods_item__img" >
						<? if ($arItem['SECOND_PICT']):?>
							<? $secondPicUrl = !empty($arItem['PICTURE_SECOND']) ? $arItem['PICTURE_SECOND'] : $arItem['PICTURE'];?>
							<img src="<?=$secondPicUrl ?>"
								 alt="<?= $strTitle; ?>" title="<?= $strTitle; ?>"
								 class="goods_item__img goods_item__img_second" >
						<?endif;?>
					</a>
				</div>
				<div class="goods_item__info">
					<div class="goods_item__description_wrapper">
						<div class="goods_item__title">
							<div class="item__title">
								<a href="<?= $arItem['DETAIL_PAGE_URL']; ?>" title="<?= /*htmlspecialcharsbx(*/$arItem['NAME']/*)*/ ?>">
									<?= /*htmlspecialcharsbx(*/$arItem['NAME']/*)*/ ?>
								</a>
							</div>
						</div>
						<div class="goods_item__description"><?=$arItem['PREVIEW_TEXT']?></div>
						<div class="goods_item__price">
							<?if(method_exists($this, 'createFrame')) $frame = $this->createFrame()->begin();?>
							<?if ($arItem['MIN_PRICE']['DISCOUNT_VALUE'] < $arItem['MIN_PRICE']['VALUE']):?>
								<del class="old_price"><?= $arItem['MIN_PRICE']['PRINT_VALUE']; ?></del>
							<?endif?>
							<ins class="new_price">
								<? if (!empty($arItem['MIN_PRICE'])) {
									if ('N' == $arParams['PRODUCT_DISPLAY_MODE'] && isset($arItem['OFFERS']) && !empty($arItem['OFFERS'])) {
										echo GetMessage( 'CT_BCT_TPL_MESS_PRICE_SIMPLE_MODE',
											array (
												'#PRICE#' => $arItem['MIN_PRICE']['PRINT_DISCOUNT_VALUE'],
											)
										);
									} else {
										echo $arItem['MIN_PRICE']['PRINT_DISCOUNT_VALUE'];
									}
								}?>
							</ins>
							<?if(method_exists($this, 'createFrame')) $frame->end();?>
						</div>
					</div>
					<div class="goods_item__buying">
						<!-- <div class="goods_item__rating">
							<?
							// $APPLICATION->IncludeComponent(
							// 	"bitrix:iblock.vote",
							// 	"stars",
							// 	array(
							// 		"IBLOCK_TYPE" => $arParams['IBLOCK_TYPE'],
							// 		"IBLOCK_ID" => $arParams['IBLOCK_ID'],
							// 		"ELEMENT_ID" => $arItem['ID'],
							// 		"ELEMENT_CODE" => "",
							// 		"MAX_VOTE" => "5",
							// 		"VOTE_NAMES" => array("1", "2", "3", "4", "5"),
							// 		"SET_STATUS_404" => "N",
							// 		"DISPLAY_AS_RATING" => $arParams['VOTE_DISPLAY_AS_RATING'],
							// 		"CACHE_TYPE" => $arParams['CACHE_TYPE'],
							// 		"CACHE_TIME" => $arParams['CACHE_TIME']
							// 	),
							// 	$component,
							// 	array("HIDE_ICONS" => "Y")
							// );
							?>
						</div> -->
						<?if($bProgressBar || $arItem['CATCHBUY']['HAS_OFFERS']):?>
							<div class="already-sold__block">
								<div class="already-sold">
									<div class="already-sold-track">
										<div class="bar" style="width: <?=intVal($arCatchBuy['PERCENT'])?>%"></div>
									</div>
								</div>
								<span class="text__block"><span class="text"><?=intVal($arCatchBuy['PERCENT'])?>% </span><?=GetMessage('RZ_SOLD')?></span>
							</div>
						<?endif?>
						<div class="goods_item__price">
							<?if(method_exists($this, 'createFrame')) $frame = $this->createFrame()->begin();?>
							<?if ($arItem['MIN_PRICE']['PRINT_DISCOUNT_VALUE'] != $arItem['MIN_PRICE']['PRINT_VALUE']):?>
								<del class="old_price"><?= $arItem['MIN_PRICE']['PRINT_VALUE']; ?></del>
							<?endif?>
							<ins class="new_price">
								<? if (!empty($arItem['MIN_PRICE'])) {
									if ('N' == $arParams['PRODUCT_DISPLAY_MODE'] && isset($arItem['OFFERS']) && !empty($arItem['OFFERS'])) {
										echo GetMessage( 'CT_BCT_TPL_MESS_PRICE_SIMPLE_MODE',
											array (
												'#PRICE#' => $arItem['MIN_PRICE']['PRINT_DISCOUNT_VALUE'],
											)
										);
									} else {
										echo $arItem['MIN_PRICE']['PRINT_DISCOUNT_VALUE'];
									}
								}?>
							</ins>
							<?if(method_exists($this, 'createFrame')) $frame->end();?>
						</div>
						<?if($arItem['CAN_BUY'] == "Y"):?>
							<?if ($arParams["USE_STORE"] == "Y" && \Bitrix\Main\Loader::includeModule('catalog')):
								$storeTpl = (CModule::IncludeModule('yenisite.storeamount')) ? 'romza_unic' : '.default';
								if (empty($arParams['STORES'])) {
									unset($arParams['STORES']);
								}
								?>
								<?
								$APPLICATION->IncludeComponent(
								"bitrix:catalog.store.amount",
								$storeTpl,
								array(
									"PER_PAGE" => "10",
									"USE_STORE_PHONE" => $arParams["USE_STORE_PHONE"],
									"SCHEDULE" => $arParams["USE_STORE_SCHEDULE"],
									"ELEMENT_ID" => $arItem['ID'],
									"CACHE_TYPE" => $arParams['CACHE_TYPE'],
									"CACHE_TIME" => $arParams['CACHE_TIME'],
									"FIELDS" => array(
										0 => "TITLE",
										1 => "DESCRIPTION",
										2 => "PHONE",
										3 => "EMAIL",
										4 => "IMAGE_ID",
										5 => "COORDINATES",
										6 => "SCHEDULE",
										7 => "ADDRESS"
									),
									"SHOW_EMPTY_STORE" => $arParams["SHOW_EMPTY_STORE"],
									"WIDTH" => "450",
									"MANY_VAL" => $arParams["STORE_MANY_VAL"], // 10
									"AVERAGE_VAL" => $arParams["STORE_AVERAGE_VAL"], // 5,
									"ONE_POPUP" => "Y",
									'ONLY_NAME_IN_LIST' => 'Y',
									"USE_MIN_AMOUNT" => "N",
									'STORES' => $arParams['STORES'],
									'CUR_OFFER' => $arItem['CUR_OFFER'],
								),
								$component
							);

							?>
							<?endif?>
						<?else:?>
							<div class="RzStore not-avaible-item">
								<div class="RzStore__item inline">
									<div class="RzStore__indicator text none">
										<span class="none">Под заказ</span>
									</div>
								</div>
							</div>
						<?endif?>

						<div class="goods_item__manage_wrapper">
							<?if($bTimer || $arItem['CATCHBUY']['HAS_OFFERS']):?>
								<div class="countdown-block">
									<div class="countdown-title"><?=GetMessage('RZ_CATCH_BUY')?></div>
									<div class="countdown">
										<div class="timer" data-until="<?=str_replace('XXX', 'T', ConvertDateTime($arCatchBuy['ACTIVE_TO'], 'YYYY-MM-DDXXXhh:mm:ss'))?>"></div>
									</div>
								</div>
							<?endif;?>

							<!-- <div class="goods_item__manage">
								<?if($arParams['DISPLAY_COMPARE'] == 'Y' || $arParams['DISPLAY_COMPARE'] == true ):?>
									<span class="compare_link addable">
										<a href="<?=$arItem['COMPARE_URL']?>" class="compare_link_item compare_id_<?=$arItem['ID']?>">
											<span class="icon icon_measuring7"></span>
											<span class="text in"><?=GetMessage('CT_BCS_TPL_MESS_BTN_COMPARE')?></span>
											<span class="text out" title="<?=GetMessage("RZ_UBRAT__IZ_SRAVNENIYA")?>"><?=GetMessage("RZ_V_SRAVNENII")?></span>
										</a>
									</span>
								<?endif?>
								<span class="favourite_link addable">
									<a href="javascript:" class="<?if($arParams['FAVORITE']):?>favourite_link_pesonal<?endif?> favourite_link_item fav_id_<?=$arItem['ID']?>" data-id="<?=$arItem['ID']?>">
									<?if(!$arParams['PERSONAL_PAGE']):?><span class="icon icon_new6"></span><?endif?>
										<span class="text in"><?if(!$arParams['PERSONAL_PAGE']):?><?=GetMessage('RZ_ADD_TO_FAVORITE')?><?else:?><?=GetMessage('RZ_UDALIT_')?><?endif?></span>
										<span class="text out" title="<?=GetMessage("RZ_UBRAT__IZ_IZBRANNOGO")?>">
											<span class="inner-text in-fav"><?=GetMessage("RZ_UDALIT_")?></span>
											<span class="inner-text"><?=GetMessage("RZ_V_IZBRANNOM")?></span>
										</span>
									</a>
								</span>
							</div> -->

							<form class="add2basket_form" method="get" action="<?=$APPLICATION->GetCurPage()?>"
								  data-varQ="<?= $arParams["PRODUCT_QUANTITY_VARIABLE"]; ?>"
								  data-varId="<?= $arParams['PRODUCT_ID_VARIABLE'] ?>"
								  data-varAct="<?=$arParams['ACTION_VARIABLE']?>"
								>
								<div class="goods_item__buy_button">
									<input type="hidden" name="<?=$arParams['ACTION_VARIABLE']?>" value="ADD2BASKET"/>
									<?if(method_exists($this, 'createFrame')) $frame = $this->createFrame()->begin();?>
									<? if($hasOfferSelect):?>
										<select class="offer_select" name="<?= $arParams['PRODUCT_ID_VARIABLE'] ?>" title="">
											<?
											$f = true;
											if (!empty($arItem['CUR_OFFER'])) {
												$f = $arItem['CUR_OFFER'];
											}
											foreach($arItem['FLAT_OFFERS'] as $id => $arOffer):?>
												<option
													<?if(isset($arOffer['PRICE'])):?>
														data-price="<?=$arOffer['PRICE']?>"
													<?endif?>
													<?if(isset($arOffer['NAME'])):?>
														data-name="<?=$arOffer['NAME']?>"
													<?endif?>
													<?if(isset($arOffer['PRICE_OLD'])):?>
														data-priceold="<?=$arOffer['PRICE_OLD']?>"
													<?endif?>
													<?if(isset($arOffer['CATCH_BUY']['TIMER'])):?>
														data-timer="<?=$arOffer['CATCH_BUY']['TIMER']?>"
													<?endif?>
													<?if(isset($arOffer['CATCH_BUY']['PROGRESS'])):?>
														data-progres="<?=$arOffer['CATCH_BUY']['PROGRESS']?>"
													<?endif?>
													data-canbuy="<?= ($arOffer['CAN_BUY']) ? '1' : '0' ?>"
													value="<?= $id ?>"
													<?if($f === true):?>
														selected
														<?$f = false; elseif(is_numeric($f) && $f == $id):?> selected
														<?$OfferId = $id;?>
													<? endif;?>>
													<?=trim($arOffer['NAME'])?></option>
											<?endforeach?>
										</select>
									<?else:?>
										<input type="hidden" name="<?=$arParams['PRODUCT_ID_VARIABLE']?>" value="<?=$arItem['ID']?>"/>
										<?= $arPropsHtml['HTML'] ?>
									<?endif;?>
									<?if(method_exists($this, 'createFrame')) $frame->end();?>
								</div>
								<div class="goods_item__amount">
									<input class="js-touchspin" type="text"<?if($arItem['CAN_BUY'] != 'Y'):?> disabled<?endif?>
										   name="<?= $arParams["PRODUCT_QUANTITY_VARIABLE"]; ?>" data-value="<?= $arItem['DEFAULT_QUANT'] ?>"
										   value="<?= ($viewMode == 'table_view') ? 0 : $arItem['DEFAULT_QUANT'] ?>"
										   title="">
									<? if ($arItem['CAN_BUY'] == 'Y' && $arParams['USE_ONE_CLICK'] == 'Y' && CModule::IncludeModule('sale') && CModule::IncludeModule('yenisite.oneclick')): ?>
										<?$APPLICATION->IncludeComponent(
											"yenisite:oneclick.buy",
											"ajax",
											array(
												"PERSON_TYPE_ID" => $arParams["ONECLICK_PERSON_TYPE_ID"],
												"SHOW_FIELDS" => $arParams["ONECLICK_SHOW_FIELDS"],
												"REQ_FIELDS" => $arParams["ONECLICK_REQ_FIELDS"],
												"ALLOW_AUTO_REGISTER" => $arParams["ONECLICK_ALLOW_AUTO_REGISTER"],
												"IBLOCK_TYPE" => $arParams['IBLOCK_TYPE'],
												"QUANTITY" => $arItem['DEFAULT_QUANT'],
												"IBLOCK_ID" => $arParams['IBLOCK_ID'],
												"IBLOCK_ELEMENT_ID" => ($OfferId ? $OfferId : $arItem['ID']),
												"USE_CAPTCHA" => $arParams["ONECLICK_USE_CAPTCHA"],
												"MESSAGE_OK" => $arParams["~ONECLICK_MESSAGE_OK"],
												"PAY_SYSTEM_ID" => $arParams["ONECLICK_PAY_SYSTEM_ID"],
												"DELIVERY_ID" => $arParams["ONECLICK_DELIVERY_ID"],
												"AS_EMAIL" => $arParams["ONECLICK_AS_EMAIL"],
												"AS_NAME" => $arParams["ONECLICK_AS_NAME"],
												"SEND_REGISTER_EMAIL" => $arParams["ONECLICK_SEND_REGISTER_EMAIL"],
												"FIELD_CLASS" => "form-control",
												"FIELD_PLACEHOLDER" => $arParams["ONECLICK_FIELD_PLACEHOLDER"],
												"FIELD_QUANTITY" => $arParams["ONECLICK_FIELD_QUANTITY"],
												'OFFER_PROPS' => $arParams['OFFER_TREE_PROPS'],
												'NAME' => $arItem['NAME']
											),
											$component
										);?>
									<? endif ?>
									<input type="submit" class="btn btn-primary btn-buy" <?if($arItem['CAN_BUY'] != 'Y'):?> disabled<?endif?>
										   value="<?=$arParams['MESS_BTN_ADD_TO_BASKET']?>" title="<?=$arParams['MESS_BTN_ADD_TO_BASKET']?>">
								</div>
							</form>
						</div>

					</div>
				</div>
			</div>
		</div>
	<? } // endforeach ?>
<?if(!$arParams['PERSONAL_PAGE']):?>
	</div>
<?endif?>