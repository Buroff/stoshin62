<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
use Yenisite\Core\Catalog;
use Yenisite\Core\Resize;
use Yenisite\Core\Tools;
include_once $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/tools/include_module.php';
if (!CModule::IncludeModule('catalog') && CModule::IncludeModule('yenisite.market')) {
	$arResult['CHECK_QUANTITY'] = (CMarketCatalog::UsesQuantity($arParams['IBLOCK_ID']) == 1);
}
if(empty($arResult['SECTION_PAGE_URL'])) {
	$arResult['SECTION_PAGE_URL'] = $APPLICATION->GetCurPage(false);
}
$arDefaultParams = array(
	'ADD_PICT_PROP' => '-',
	'OFFER_ADD_PICT_PROP' => '-',
	'SHOW_DISCOUNT_PERCENT' => 'N',
	'SHOW_OLD_PRICE' => 'N',
	'SHOW_PAGINATION' => 'Y',
	'MESS_BTN_BUY' => '',
	'MESS_BTN_ADD_TO_BASKET' => 'В корзину',
	'MESS_BTN_DETAIL' => '',
	'MESS_NOT_AVAILABLE' => ''
);
$resizeParams = array('WIDTH' => 168, 'HEIGHT' => 170, 'SET_ID' => intval($arParams['RESIZER_PRODUCT']));
$arParams = array_merge($arDefaultParams, $arParams);

if(CRZShinmarket::isCatchBuy()){
	$arParams['CATCHBUY'] = CRZShinmarket::getCatchBuyList();
}

$arParams['ADD_PICT_PROP'] = trim($arParams['ADD_PICT_PROP']);
if ('-' == $arParams['ADD_PICT_PROP'])
	$arParams['ADD_PICT_PROP'] = '';
$arParams['OFFER_ADD_PICT_PROP'] = trim($arParams['OFFER_ADD_PICT_PROP']);
if ('-' == $arParams['OFFER_ADD_PICT_PROP'])
	$arParams['OFFER_ADD_PICT_PROP'] = '';
if ('Y' != $arParams['SHOW_DISCOUNT_PERCENT'])
	$arParams['SHOW_DISCOUNT_PERCENT'] = 'N';
if ('Y' != $arParams['SHOW_OLD_PRICE'])
	$arParams['SHOW_OLD_PRICE'] = 'N';

$arParams['MESS_BTN_BUY'] = trim($arParams['MESS_BTN_BUY']);
$arParams['MESS_BTN_ADD_TO_BASKET'] = trim($arParams['MESS_BTN_ADD_TO_BASKET']);
$arParams['MESS_BTN_DETAIL'] = trim($arParams['MESS_BTN_DETAIL']);
$arParams['MESS_NOT_AVAILABLE'] = trim($arParams['MESS_NOT_AVAILABLE']);


if (!empty($arResult['ITEMS'])) {
	$boolSKU = false;
	$strBaseCurrency = '';
	$boolConvert = isset($arResult['CONVERT_CURRENCY']['CURRENCY_ID']);

	if ($arResult['MODULES']['catalog']) {
		if (!$boolConvert)
			$strBaseCurrency = CCurrency::GetBaseCurrency();

		$arSKU = CCatalogSKU::GetInfoByProductIBlock($arParams['IBLOCK_ID']);
		$boolSKU = !empty($arSKU) && is_array($arSKU);
	}

	$arNewItemsList = array();
	$arResult['TOTAL_IN_LIST'] = 0;
	foreach ($arResult['ITEMS'] as $key => $arItem) {
		$arItem['CATCHBUY']['HAS_OFFERS'] = false;
		$arItem['CHECK_QUANTITY'] = false;
		if (!isset($arItem['CATALOG_MEASURE_RATIO']))
			$arItem['CATALOG_MEASURE_RATIO'] = 1;
		if (!isset($arItem['CATALOG_QUANTITY']))
			$arItem['CATALOG_QUANTITY'] = 0;
		$arItem['CATALOG_QUANTITY'] = (
		0 < $arItem['CATALOG_QUANTITY'] && is_float($arItem['CATALOG_MEASURE_RATIO'])
			? floatval($arItem['CATALOG_QUANTITY'])
			: intval($arItem['CATALOG_QUANTITY'])
		);
		$arItem['CATALOG'] = false;
		if (!isset($arItem['CATALOG_SUBSCRIPTION']) || 'Y' != $arItem['CATALOG_SUBSCRIPTION'])
			$arItem['CATALOG_SUBSCRIPTION'] = 'N';

		$productPictures = CIBlockPriceTools::getDoublePicturesForItem($arItem, $arParams['ADD_PICT_PROP']);

		if (empty($productPictures['SECOND_PICT']))
			$productPictures['SECOND_PICT'] = $productPictures['PICT'];

		$arItem['SECOND_PICT'] = true;

		if ($arResult['MODULES']['catalog']) {
			$arItem['CATALOG'] = true;
			if (!isset($arItem['CATALOG_TYPE']))
				$arItem['CATALOG_TYPE'] = CCatalogProduct::TYPE_PRODUCT;
			if (
				(CCatalogProduct::TYPE_PRODUCT == $arItem['CATALOG_TYPE'] || CCatalogProduct::TYPE_SKU == $arItem['CATALOG_TYPE'])
				&& !empty($arItem['OFFERS'])
			) {
				$arItem['CATALOG_TYPE'] = CCatalogProduct::TYPE_SKU;
			}
			switch ($arItem['CATALOG_TYPE']) {
				case CCatalogProduct::TYPE_SET:
					$arItem['OFFERS'] = array();
					$arItem['CATALOG_MEASURE_RATIO'] = 1;
					$arItem['CATALOG_QUANTITY'] = 0;
					$arItem['CHECK_QUANTITY'] = false;
					break;
				case CCatalogProduct::TYPE_SKU:
					break;
				case CCatalogProduct::TYPE_PRODUCT:
				default:
					$arItem['CHECK_QUANTITY'] = ('Y' == $arItem['CATALOG_QUANTITY_TRACE'] && 'N' == $arItem['CATALOG_CAN_BUY_ZERO']);
					break;
			}
		} else {
			$arItem['CATALOG_TYPE'] = 0;
			$arItem['OFFERS'] = array();
		}

		if ($arItem['CATALOG'] && isset($arItem['OFFERS']) && !empty($arItem['OFFERS'])) {
			$arItem['MIN_PRICE'] = CIBlockPriceTools::getMinPriceFromOffers(
				$arItem['OFFERS'],
				$boolConvert ? $arResult['CONVERT_CURRENCY']['CURRENCY_ID'] : $strBaseCurrency
			);
		}

		if ($arResult['MODULES']['catalog'] && $arItem['CATALOG'] && CCatalogProduct::TYPE_PRODUCT == $arItem['CATALOG_TYPE']) {
			CIBlockPriceTools::setRatioMinPrice($arItem, true);
		}

		if (!empty($arItem['DISPLAY_PROPERTIES'])) {
			foreach ($arItem['DISPLAY_PROPERTIES'] as $propKey => $arDispProp) {
				if ('F' == $arDispProp['PROPERTY_TYPE'])
					unset($arItem['DISPLAY_PROPERTIES'][$propKey]);
			}
		}
		$arItem['LAST_ELEMENT'] = 'N';
		$arItem['PICTURE'] = Resize::GetResizedImg($arItem, $resizeParams);
		$arItem['PICTURE_SECOND'] = Resize::GetResizedImg($productPictures['SECOND_PICT']['ID'], $resizeParams);
		if (count($arItem['OFFERS']) > 0) {
			/** @noinspection PhpVoidFunctionResultUsedInspection */
			foreach ($arItem['OFFERS'] as &$arOffer){
				$arOffer = CRZShinmarket::getDisplayValueForProps($arOffer);
				$arCatchBuy = $arParams['CATCHBUY'][$arOffer['ID']];
				$bTimer = !empty($arCatchBuy['ACTIVE_TO']);
				$bProgressBar = $arCatchBuy['MAX_USES'] > 0;
				$arCatchBuy['PERCENT'] = ($bProgressBar) ? $arCatchBuy['COUNT_USES'] / $arCatchBuy['MAX_USES'] * 100 : 0;
				if(($bTimer || $bProgressBar) && $arOffer['CAN_BUY']){
					$arItem['CATCHBUY']['HAS_OFFERS'] = true;
					if($bTimer){
						$arOffer['CATCH_BUY']['TIMER'] = str_replace('XXX', 'T', ConvertDateTime($arCatchBuy['ACTIVE_TO'], 'YYYY-MM-DDXXXhh:mm:ss'));
					}
					if ($bProgressBar){
						$arOffer['CATCH_BUY']['PROGRESS'] = intVal($arCatchBuy['PERCENT']);
					}
				}
			}
			unset($arOffer);

			$arItem['FLAT_OFFERS'] = Catalog::getFlatOffersList($arItem, $arParams['OFFERS_PROPERTY_CODE']);
		}
		if (isset($arItem['FLAT_OFFERS']) && !empty($arItem['FLAT_OFFERS'])) {
			foreach($arItem['FLAT_OFFERS'] as &$arFlat) {
				if($arFlat['CAN_BUY'] == true) {
					$arItem['MIN_PRICE']['PRINT_VALUE'] = htmlspecialcharsback($arFlat['PRICE_OLD']);
					$arItem['MIN_PRICE']['PRINT_DISCOUNT_VALUE'] = htmlspecialcharsback($arFlat['PRICE']);
					$arItem['CUR_OFFER'] = $arFlat['ID'];
					$arItem['CAN_BUY'] = $arFlat['CAN_BUY'];
					break;
				}
			} unset($arFlat);
		}
		// $arItem['DEFAULT_QUANT'] = Catalog::getUFDefaultValue($arItem,'UF_DEFAULT_QUANTITY');
		$arItem['DEFAULT_QUANT'] = 1;
		if($arItem['CAN_BUY'] == 'Y' || $arItem['CAN_BUY'] == true) {
			$arResult['TOTAL_IN_LIST'] += $arItem['DEFAULT_QUANT'];
		}
		//Prices for MARKET
		if (!CModule::IncludeModule('catalog') && CModule::IncludeModule('yenisite.market')) {
			//CRZTools::IncludeMainLang();

			$prices = CMarketPrice::GetItemPriceValues($arItem['ID'], $arItem['PRICES']);
			if (count($prices) > 0) {
				unset($arItem['PRICES']);
			}
			$minPrice = false;
			foreach ($prices as $k => $pr) {
				$pr = floatval($pr);
				$arItem['PRICES'][$k]['VALUE'] = $pr;
				$arItem['PRICES'][$k]['PRINT_VALUE'] = $pr;
				if ((empty($minPrice) || $minPrice > $pr) && $pr > 0) {
					$minPrice = $pr;
				}
			}
			if ($minPrice !== false) {
				$arItem['MIN_PRICE']['VALUE'] = $minPrice;
				$arItem['MIN_PRICE']['PRINT_VALUE'] = Tools::FormatPrice($minPrice);
				$arItem['MIN_PRICE']['DISCOUNT_VALUE'] = $minPrice;
				$arItem['MIN_PRICE']['PRINT_DISCOUNT_VALUE'] = Tools::FormatPrice($minPrice);
				$arItem['CATALOG_MEASURE_RATIO'] = 1;
				$arItem['CAN_BUY'] = true;
			}
			$arItem['CHECK_QUANTITY'] = $arResult['CHECK_QUANTITY'];
			$arItem['CATALOG_QUANTITY'] = CMarketCatalogProduct::GetQuantity($arItem['ID'], $arItem['IBLOCK_ID']);

			if ($arItem['CHECK_QUANTITY'] && $arItem['CATALOG_QUANTITY'] <= 0) {
				$arItem['CAN_BUY'] = false;
			}
			$arItem['CATALOG_TYPE'] = 1; //simple product
		}
		//end Prices for MARKET
		if(is_bool($arItem['CAN_BUY'])) {
			if($arItem['CAN_BUY'] === true) {
				$arItem['CAN_BUY'] = 'Y';
			} else {
				$arItem['CAN_BUY'] = 'N';
			}
		}
		$arNewItemsList[$key] = $arItem;
	}
	$arNewItemsList[$key]['LAST_ELEMENT'] = 'Y';
	$arResult['ITEMS'] = $arNewItemsList;
}

$bFilterByProp = (!empty($arParams['PROPERTY_FILTER_CODE']) && !empty($arParams['PROPERTY_FILTER_VALUE']));
if ($bFilterByProp) {
	/** @noinspection PhpDynamicAsStaticMethodCallInspection */
	$cnt = CIBlockElement::GetList(array(),
		array(
			'IBLOCK_ID' => $arParams['IBLOCK_ID'],
			'PROPERTY_' . $arParams['PROPERTY_FILTER_CODE'] => $arParams['PROPERTY_FILTER_VALUE']
		),
		array()
	);
	$arResult['CNT'] = $cnt;
}
$arResult["IS_SKU"] = $boolSKU;


