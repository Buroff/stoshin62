<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?
$APPLICATION->IncludeComponent("bitrix:catalog.section.list", "catalog", array(
	"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
	"IBLOCK_ID" => $arParams["IBLOCK_ID"],
	"SECTION_ID" => intval($arParams["SECTION_ID"]),
	"SECTION_CODE" => $arParams["SECTION_CODE"],
	"COUNT_ELEMENTS" => $arParams["SECTION_COUNT_ELEMENTS"],
	"TOP_DEPTH" => $arParams['SECTION_TOP_DEPTH'],
	"SECTION_URL" => "",
	"CACHE_TYPE" => $arParams["CACHE_TYPE"],
	"CACHE_TIME" => $arParams["CACHE_TIME"],
	"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
	"ADD_SECTIONS_CHAIN" => "N",
	"SHOW_PARENT_NAME" => "N",
	"RESIZER_SET" => $arParams['RESIZER_SUBSECTION'],
	"RESIZER_SECTION_IMG" => $arParams['RESIZER_SECTION_IMG'],
	"RESIZER_SECTIONS_IMG" => $arParams['RESIZER_SECTIONS_IMG'],
	"SHOW_SUBSECTIONS" => $arParams['SHOW_SUBSECTIONS'],
	"SHOW_DESCRIPTION" => $arParams['SHOW_DESCRIPTION'],
	"FULL_DESCTRIPTION" => $arParams['FULL_DESCTRIPTION'],
	"VIEW_MODE" => $arParams['VIEW_MODE_SECTION_LIST']
	),
	$component
);
?>