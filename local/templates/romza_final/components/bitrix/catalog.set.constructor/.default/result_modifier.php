<? use Yenisite\Core\Resize;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$arImgParam = array('WIDTH' => 150, 'HEIGHT' => 150, 'SET_ID' => intval($arParams['RESIZER_PRODUCT']));
$arResult['IMG_PARAMS'] = $arImgParam;
$arDefaultSetIDs = array($arResult["ELEMENT"]["ID"]);
foreach (array("DEFAULT", "OTHER") as $type) {
	foreach ($arResult["SET_ITEMS"][$type] as $key => $arItem) {
		$arElement = array(
			"ID" => $arItem["ID"],
			"NAME" => $arItem["NAME"],
			"DETAIL_PAGE_URL" => $arItem["DETAIL_PAGE_URL"],
			"DETAIL_PICTURE" => $arItem["DETAIL_PICTURE"],
			"PREVIEW_PICTURE" => $arItem["PREVIEW_PICTURE"],
			"PRICE_CURRENCY" => $arItem["PRICE_CURRENCY"],
			"PRICE_DISCOUNT_VALUE" => $arItem["PRICE_DISCOUNT_VALUE"],
			"PRICE_PRINT_DISCOUNT_VALUE" => $arItem["PRICE_PRINT_DISCOUNT_VALUE"],
			"PRICE_VALUE" => $arItem["PRICE_VALUE"],
			"PRICE_PRINT_VALUE" => $arItem["PRICE_PRINT_VALUE"],
			"PRICE_DISCOUNT_DIFFERENCE_VALUE" => $arItem["PRICE_DISCOUNT_DIFFERENCE_VALUE"],
			"PRICE_DISCOUNT_DIFFERENCE" => $arItem["PRICE_DISCOUNT_DIFFERENCE"],
		);
		if ($arItem["PRICE_CONVERT_DISCOUNT_VALUE"])
			$arElement["PRICE_CONVERT_DISCOUNT_VALUE"] = $arItem["PRICE_CONVERT_DISCOUNT_VALUE"];
		if ($arItem["PRICE_CONVERT_VALUE"])
			$arElement["PRICE_CONVERT_VALUE"] = $arItem["PRICE_CONVERT_VALUE"];
		if ($arItem["PRICE_CONVERT_DISCOUNT_DIFFERENCE_VALUE"])
			$arElement["PRICE_CONVERT_DISCOUNT_DIFFERENCE_VALUE"] = $arItem["PRICE_CONVERT_DISCOUNT_DIFFERENCE_VALUE"];

		if ($type == "DEFAULT")
			$arDefaultSetIDs[] = $arItem["ID"];
		$imgSrc = '';
		$ofResult = CCatalogSKU::GetInfoByOfferIBlock($arItem['IBLOCK_ID']);
		if($ofResult !== false) {
			$parentItem = CCatalogSku::GetProductInfo($arItem['ID'],$arItem['IBLOCK_ID'] );
			if($parentItem !== false) {
				$rsParent = CIBlockElement::GetByID($parentItem['ID']);
				$arParent = $rsParent->GetNext();
				if(!empty($arParent)) {
					$imgSrc = Resize::GetResizedImg($arParent,$arImgParam);
				}
			}
		}
		if (strlen($imgSrc) == 0) {
			$imgSrc = Resize::GetResizedImg($arItem,$arImgParam);
		}
		$arElement['PICTURE'] = $imgSrc;

		$arResult["SET_ITEMS"][$type][$key] = $arElement;
	}
}
$arResult["DEFAULT_SET_IDS"] = $arDefaultSetIDs;
$arResult['ELEMENT']['PICTURE'] = Resize::GetResizedImg($arResult['ELEMENT'],$arResult['IMG_PARAMS']);