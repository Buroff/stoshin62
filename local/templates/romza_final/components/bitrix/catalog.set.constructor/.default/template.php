<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$intElementID = intval($arParams["ELEMENT_ID"]);
$countDefSetItems = count($arResult["SET_ITEMS"]["DEFAULT"]);
$arSetId = array($intElementID);
?>
<h2><?= GetMessage("RZ_VMESTE_VIGODNEE") ?>:</h2>
<div class="benefits_goods_block">
	<div class="goods_item">
		<div class="goods_item__inner">
			<a href="<?= $arResult['ELEMENT']["DETAIL_PAGE_URL"] ?>" class="goods_item__img_wrapper">
				<img src="<?= $arResult['ELEMENT']['PICTURE'] ?>" alt=""/>
			</a>

			<div class="goods_item__info">
				<div class="goods_item__title">
					<a href="<?= $arResult['ELEMENT']["DETAIL_PAGE_URL"] ?>"><?= $arResult['ELEMENT']["NAME"] ?></a>
				</div>
				<div class="goods_item__rating">
					<? $APPLICATION->IncludeComponent(
						"bitrix:iblock.vote",
						"stars",
						array(
							"IBLOCK_TYPE" => $arParams['IBLOCK_TYPE'],
							"IBLOCK_ID" => $arParams['IBLOCK_ID'],
							"ELEMENT_ID" => $arResult['ELEMENT']['ID'],
							"ELEMENT_CODE" => "",
							"MAX_VOTE" => "5",
							"VOTE_NAMES" => array("1", "2", "3", "4", "5"),
							"SET_STATUS_404" => "N",
							"DISPLAY_AS_RATING" => $arParams['VOTE_DISPLAY_AS_RATING'],
							"CACHE_TYPE" => $arParams['CACHE_TYPE'],
							"CACHE_TIME" => $arParams['CACHE_TIME']
						),
						$component,
						array("HIDE_ICONS" => "Y")
					); ?>
				</div>
				<?if(method_exists($this, 'createFrame')) $frame = $this->createFrame()->begin();?>
					<div class="goods_item__price"><?= $arResult['ELEMENT']["PRICE_PRINT_DISCOUNT_VALUE"] ?></div>
				<?if(method_exists($this, 'createFrame')) $frame->end();?>
			</div>
		</div>
	</div>
	<span class="sign">+</span>

	<? $curCount = 0;
	foreach ($arResult["SET_ITEMS"]["DEFAULT"] as $key => $arItem): $curCount++; ?>
		<div class="goods_item">
			<div class="goods_item__inner">
				<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="goods_item__img_wrapper">
					<img src="<?= $arItem["PICTURE"] ?>" alt=""/>
				</a>

				<div class="goods_item__info">
					<div class="goods_item__title">
						<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><?= $arItem["NAME"] ?></a>
					</div>
					<div class="goods_item__rating">
						<? $APPLICATION->IncludeComponent(
							"bitrix:iblock.vote",
							"stars",
							array(
								"IBLOCK_TYPE" => $arParams['IBLOCK_TYPE'],
								"IBLOCK_ID" => $arParams['IBLOCK_ID'],
								"ELEMENT_ID" => $arItem['ID'],
								"ELEMENT_CODE" => "",
								"MAX_VOTE" => "5",
								"VOTE_NAMES" => array("1", "2", "3", "4", "5"),
								"SET_STATUS_404" => "N",
								"DISPLAY_AS_RATING" => $arParams['VOTE_DISPLAY_AS_RATING'],
								"CACHE_TYPE" => $arParams['CACHE_TYPE'],
								"CACHE_TIME" => $arParams['CACHE_TIME']
							),
							$component,
							array("HIDE_ICONS" => "Y")
						); ?>
					</div>
					<?if(method_exists($this, 'createFrame')) $frame = $this->createFrame()->begin();?>
						<div class="goods_item__price"><?= $arItem["PRICE_PRINT_DISCOUNT_VALUE"] ?></div>
					<?if(method_exists($this, 'createFrame')) $frame->end();?>
				</div>
			</div>
		</div>
		<? if ($curCount < $countDefSetItems): ?>
			<span class="sign">+</span>
		<? endif; ?>
		<? $arSetId[] = $arItem['ID'] ?>
	<? endforeach ?>

	<div class="benefits_goods__discount">
		<div class="benefits_goods__discount_head"><?= GetMessage("RZ_VMESTE") ?><br> <span
				class="bigger"><?= GetMessage("RZ_DESHEVLE") ?></span></div>
		<div class="benefits_goods__discount_block">
			<div class="ordinary_price">
				<? if ($arResult["SET_ITEMS"]["OLD_PRICE"]): ?>
					<div><?= GetMessage("RZ_OBICHNAYA_TCENA") ?>: <?= $arResult["SET_ITEMS"]["OLD_PRICE"] ?></div>
				<? endif; ?>
			</div>
			<div class="special_price">
				<div class="special_price__head"><?= GetMessage("RZ_VASHA_SPETCIAL_NAYA_TCENA") ?>:</div>
				<div class="special_price__price"><?= $arResult["SET_ITEMS"]["PRICE"] ?></div>
				<? if ($arResult["SET_ITEMS"]["PRICE_DISCOUNT_DIFFERENCE"]): ?>
					<div
						class="special_price__discount"><?= GetMessage("CATALOG_SET_DISCOUNT_DIFF", array("#PRICE#" => $arResult["SET_ITEMS"]["PRICE_DISCOUNT_DIFFERENCE"])) ?></div>
				<? endif; ?>
			</div>
			<div class="buy_button_wrapper">
				<a href="javascript:;" class="btn btn-primary" data-id="<?= implode(',', $arSetId) ?>">
					<?= GetMessage("CATALOG_SET_BUY") ?>
				</a>
			</div>
		</div>
		<br/><br/>
		<a href="javascript:;" class="hidden-xs pupup-set-constr-href" onclick="OpenCatalogSetPopup(<?=$intElementID?>)" data-toggle="modal" data-target="#CatalogSetConstructor_<?=$intElementID; ?>"><?= GetMessage("CATALOG_SET_CONSTRUCT") ?></a>
	</div>
</div>
<?
$popupParams["AJAX_PATH"] = $this->GetFolder() . "/ajax.php";
$popupParams["SITE_ID"] = SITE_ID;
$popupParams["CURRENT_TEMPLATE_PATH"] = $this->GetFolder();
$popupParams["MESS"] = array(
	"CATALOG_SET_POPUP_TITLE" => GetMessage("CATALOG_SET_POPUP_TITLE"),
	"CATALOG_SET_POPUP_DESC" => GetMessage("CATALOG_SET_POPUP_DESC"),
	"CATALOG_SET_BUY" => GetMessage("CATALOG_SET_BUY"),
	"CATALOG_SET_SUM" => GetMessage("CATALOG_SET_SUM"),
	"CATALOG_SET_DISCOUNT" => GetMessage("CATALOG_SET_DISCOUNT"),
	"CATALOG_SET_WITHOUT_DISCOUNT" => GetMessage("CATALOG_SET_WITHOUT_DISCOUNT"),
	"APR_RUB_SYMBOL" => GetMessage("APR_RUB_SYMBOL"),
);
$popupParams["ELEMENT"] = $arResult["ELEMENT"];
$popupParams["SET_ITEMS"] = $arResult["SET_ITEMS"];
$popupParams["DEFAULT_SET_IDS"] = $arResult["DEFAULT_SET_IDS"];
$popupParams["ITEMS_RATIO"] = $arResult["ITEMS_RATIO"];
?>
<script type="text/javascript">
	BX.message({
		setItemAdded2Basket: '<?=GetMessageJS("CATALOG_SET_ADDED2BASKET")?>',
		setButtonBuyName: '<?=GetMessageJS("CATALOG_SET_BUTTON_BUY")?>',
		setButtonBuyUrl: '<?=$arParams["BASKET_URL"]?>',
		setIblockId: '<?=$arParams["IBLOCK_ID"]?>',
		setOffersCartProps: <?=CUtil::PhpToJSObject($arParams["OFFERS_CART_PROPERTIES"])?>
	});

	BX.ready(function () {
		catalogSetDefaultObj_<?=$intElementID; ?> = new catalogSetConstructDefault(
			<?=CUtil::PhpToJSObject($arResult["DEFAULT_SET_IDS"])?>,
			'<? echo $this->GetFolder(); ?>/ajax.php',
			'<?=$arResult["ELEMENT"]["PRICE_CURRENCY"]?>',
			'<?=SITE_ID?>',
			'<?=$intElementID?>',
			'<?=($arResult["ELEMENT"]["DETAIL_PICTURE"]["src"] ? $arResult["ELEMENT"]["DETAIL_PICTURE"]["src"] : $this->GetFolder().'/images/no_foto.png')?>',
			<?=CUtil::PhpToJSObject($arResult["ITEMS_RATIO"])?>
		);

	});

		function OpenCatalogSetPopup(element_id) {
			if (window.arSetParams) {
				for (var obj in window.arSetParams) {
					if (window.arSetParams.hasOwnProperty(obj)) {
						for (var obj2 in window.arSetParams[obj]) {
							if (window.arSetParams[obj].hasOwnProperty(obj2)) {
								if (obj2 == element_id)
									var curSetParams = window.arSetParams[obj][obj2]
							}
						}
					}
				}
			}

			BX.CatalogSetConstructor =
			{
				bInit: false,
				popup: null,
				arParams: {}
			};
			var modal_id = "CatalogSetConstructor_" + element_id;
			var modal = $('#' + modal_id);
			var modalContent = modal.find('.modal-container');
			BX.CatalogSetConstructor.popup = modal;

			modalContent.addClass('ajax_loader').html('');
			BX.ajax.post(
				'<? echo $this->GetFolder(); ?>/popup.php',
				{
					lang: BX.message('LANGUAGE_ID'),
					site_id: BX.message('SITE_ID') || '',
					arParams: curSetParams
				},
				BX.delegate(function (result) {
						modalContent.removeClass('ajax_loader').html(result);
					},
					this)
			);

		//	modal.modal('show');
		}

	if (!window.arSetParams) {
		window.arSetParams = [{'<?=$intElementID?>': <?echo CUtil::PhpToJSObject($popupParams)?>}];
	}
	else {
		window.arSetParams.push({'<?=$intElementID?>': <?echo CUtil::PhpToJSObject($popupParams)?>});
	}

</script>