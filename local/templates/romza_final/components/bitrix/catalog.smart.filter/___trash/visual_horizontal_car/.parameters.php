<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

// дополнительный параметр
$arTemplateParameters['TYPE'] = array(
		'PARENT' => 'ADDITIONAL_SETTINGS ',
		'NAME' => 'Тип',
		'TYPE' => 'LIST',
		'MULTIPLE' => 'N',
		'VALUES' => array(
			'shiny'=>'shiny',
			'diski'=>'diski',
		)
	);
?>