<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$rnd = $this->randString();
use Yenisite\Core\Tools;
use \Yenisite\Shinmarket\ManagerCalc;

$bHasTablTX = CRZShinmarketSettings::isTiersSolution() && ManagerCalc::tablesExist();


?>
<!--suppress Annotator -->
<form name="<?= $arResult["FILTER_NAME"]."_form"?>" action="<?= $arResult["FORM_ACTION"]?>" method="get"
	  class="seach_form tyres filterHorizontal" data-rnd="<?=$rnd?>" data-arparams="<?=Tools::GetEncodedArParams($arParams)?>">
		<div class="collapsed_area">
				<div class="seach_form__inner seach_form__inner_type">
		            <?if ($bHasTablTX){
		                // include 'tx_selects_fo_base.php';
		                include 'vehicle.php';
		            }?>
					
				</div>
		</div>
		<div class="seach_form__submit">
			<div class="collapse_btn_wrapper">
					<span class="btn btn-default btn-collapse">
						<span class="hide-filter">
							<span class="btn-inner"><?=GetMessage("RZ_SKRIT__FIL_TR")?></span>
							<span class="flaticon-arrow215"></span>
						</span>
						<span class="show-filter">
							<span class="btn-inner"><?=GetMessage("RZ_POKAZAT__FIL_TR")?></span>
							<span class="flaticon-arrow222"></span>
						</span>
					</span>
			</div>
			<div class="submit">
				<!-- <div class="submit_inner">
					<?if(!empty($arParams['ADDITIONAL_URL']) && !empty($arParams['ADDITIONAL_URL_TEXT'])):?>
						<div><a href="<?=$arParams['ADDITIONAL_URL']?>"><?=$arParams['ADDITIONAL_URL_TEXT']?></a></div>
					<?endif?>
					<div><button class="form_reset_button" type="reset"><?=GetMessage("RZ_SBROSIT__VSE_PARAMETRI_POISKA")?></button></div>
				</div> -->
			<div class="submit_inner" id="search_by_car">
				<button class="btn btn-default btn-find" type="submit" name="set_filter" value="Y"><?=GetMessage("RZ_PODOBRAT_")?></button>
			</div>
			</div>
            <p class="backend-cnt-products found-product hidden"><?=GetMessage('RZ_FOUNDED')?>: <span class="text"></span></p>
		</div>
</form>
