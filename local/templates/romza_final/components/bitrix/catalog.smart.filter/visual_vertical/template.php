<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use \Yenisite\Shinmarket\ManagerCalc;
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
use Yenisite\Core\Tools;
$this->setFrameMode(true);
$bHasTablTX = CRZShinmarketSettings::isTiersSolution() && ManagerCalc::tablesExist();
$bInRightSec = $arResult['IN_RIGHT_SECTION'];?>
<aside class="page_aside page_aside__filter catalog_filter">
	<div class="page_vertical__nav">
		<?if($arParams['SHOW_MENU'] == 'vertical' && $arParams['SHOW_FILTER'] != 'top'):?>
			<? Tools::IncludeArea('header', 'menu_horizon', array(), true) ?>
		<?endif?>
	</div>
	<!--suppress Annotator -->
	<form name="<?= $arResult["FILTER_NAME"]."_form"?>" action="<?= $arResult["FORM_ACTION"]?>" method="get"
		  class="sidebar_filter filter_form" id="sectionFilter" data-arparams="<?=Tools::GetEncodedArParams($arParams)?>">
		<div class="collapsed_area">
            <?if ($bHasTablTX && $bInRightSec){
                include 'tx_selects_fo_base.php';
            }?>
			<?/*?>
			<div class="sidebar_filter__inner toggle_slider_wrapper">
				<div class="switcher_wrapper">
					<div class="switcher type_2 state_1">
						<span class="js-switcher-label switcher_outside_label label_state_1" data-state="1"><?=GetMessage("RZ_PO_PARAMETRAM")?></span>

						<div class="slider-toggle switcher_inner_1"></div>
						<span class="js-switcher-label switcher_outside_label label_state_2" data-state="2"><?=GetMessage("RZ_PO_AVTOMOBILYU")?></span>
						<input type="hidden" class="switcher__value">
					</div>
				</div>
			</div>
			<?*/?>
				<?foreach($arResult["HIDDEN"] as $arItem):?>
					<input
						type="hidden"
						name="<?= $arItem["CONTROL_NAME"]?>"
						id="<?= $arItem["CONTROL_ID"]?>"
						value="<?= $arItem["HTML_VALUE"]?>"
					/>
				<?endforeach;?>
			<div class="sidebar_filter__inner tags_wrapper<?if(count($arResult['CURRENT_VALUES']) == 0):?> hidden<?endif?>">
				<div class="tags__head"><?=GetMessage("RZ_VI_VIBRALI")?></div>
				<div class="tags__list">
					<?if(!empty($arResult['CURRENT_VALUES'])):?>
						<?foreach($arResult['CURRENT_VALUES'] as &$arVals):?>
						<span class="tag" data-id="<?=$arVals['CONTROL_NAME']?>">
							<?=$arVals['VALUE']?>
							<span class="close flaticon-delete30"></span>
						</span>
						<?endforeach; unset($arVals)?>
					<?endif?>
				</div>
			</div>
				<?foreach($arResult["ITEMS"] as $arItem):?>
						<?if($arItem['CODE'] != "SEASON") continue;?>
						<?if(empty($arItem["VALUES"])) continue;?>
						<div class="sidebar_filter__inner tires-types-selector_wrapper">
							<div class="tires-types-selector__head"><?=GetMessage("RZ_SEZON_I_TIP")?>:</div>
							<div class="tires-types-selector">
								<?foreach($arItem["VALUES"] as $val => $ar):?>
								<div class="tires-types-selector__box">
									<input <?= $ar["DISABLED"] ? 'disabled': ''?>
										class="tires-types-selector__input" type="checkbox"
										value="<?= $ar["HTML_VALUE"]?>"
										name="<?= $ar["CONTROL_NAME"]?>"
										id="<?= $ar["CONTROL_ID"]?>"
										<?= $ar["CHECKED"]? 'checked="checked"': ''?>>
									<?
									switch ($ar['ICON_CLASS']) {
										case "icon_clear3":
											$seasonName = "summer";
											break;
										case "icon_snowflake":
											$seasonName = "winter";
											break;
										case "icon_screwsnow":
											$seasonName = "winter-pinned";
											break;
										case "icon_cloudy1":
											$seasonName = "all-seasonal";
											break;
										default:
											$seasonName = "";
									}
									?>
									<label class="tires-types-selector__item <?=$seasonName?>" for="<?= $ar["CONTROL_ID"]?>">
										<span class="item_icon icon <?=$ar['ICON_CLASS']?>"></span>
										<span class="item_title"><?= $ar["VALUE"];?></span>
									</label>
								</div>
								<?endforeach?>
							</div>
						</div>
				<?endforeach;?>
				<?
				$counter = 0;
				$delim = 2;
				?>
				<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
					<?if($arItem["PROPERTY_TYPE"] == "N" ):?>
						<?
						if (!$arItem["VALUES"]["MIN"]["VALUE"] || !$arItem["VALUES"]["MAX"]["VALUE"] || $arItem["VALUES"]["MIN"]["VALUE"] == $arItem["VALUES"]["MAX"]["VALUE"])
							continue;
						$minVal = intval($arItem["VALUES"]["MIN"]["VALUE"]);
						if (isset($arItem["VALUES"]["MIN"]["HTML_VALUE"])) {
							$minVal = intval($arItem["VALUES"]["MIN"]["HTML_VALUE"]);
						}
						$maxVal = intval($arItem["VALUES"]["MAX"]["VALUE"]);
						if (isset($arItem["VALUES"]["MAX"]["HTML_VALUE"])) {
							$maxVal = intval($arItem["VALUES"]["MAX"]["HTML_VALUE"]);
						}
						?>
						<div class="clearfix"></div>
						<div class="sidebar_filter__inner seach_form__inner_price">
							<div class="seach_form__inner_price__head"><?=$arItem["NAME"]?></div>
							<div class="range_prices_wrapper">
								<?=GetMessage("RZ_OT")?>
								<input name="<?= $arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>"
									   id="<?= $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>"
									   data-val="<?=$arItem["VALUES"]["MIN"]["VALUE"]?>"
									   value="<?= $arItem["VALUES"]["MIN"]["HTML_VALUE"] ?>"
									   type="text" class="form-control form-control-small price_from">
								<?=GetMessage("RZ_DO")?>
								<input name="<?= $arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>"
									   id="<?= $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>"
									   data-val="<?=$arItem["VALUES"]["MAX"]["VALUE"]?>"
									   value="<?= $arItem["VALUES"]["MAX"]["HTML_VALUE"] ?>"
									   type="text" class="form-control form-control-small price_to">
							</div>
							<div class="range_slider_wrapper">
								<div class="js-price-range-slider range_slider"></div>
							</div>
						</div>
						<?endif?>
					<?endforeach?>
					<div class="sidebar_filter__inner sidebar_filter__inner_group">
						<?foreach($arResult["ITEMS"] as $key=>$arItem):?>
						<?if(!empty($arItem["VALUES"]) && !isset($arItem["PRICE"]) && $arItem['CODE'] != "SEASON" && $arItem["PROPERTY_TYPE"] != "N" ):?>
								<div class="column <?=$counter % $delim ==0 ? 'col-left' : 'col-right';?> checked">
									<div class="field_head">
										<div class="field_head__main main__inner">
											<span class="field_head__main__inner">
												<?=$arItem["NAME"]?>
												<?if(isset($arItem['HINT']['TEXT']) || !empty($arItem['FILTER_HINT'])):?>
													<button class="brand-primary-color show_tip" type="button" data-toggle="popover"
															data-tip="#filter_pop_<?=$arItem['ID']?>">?</button>
													<div id="filter_pop_<?=$arItem['ID']?>" class="popover fade bottom in" style="display: none" >
														<div class="arrow"></div>
														<div class="popover-content">
															<div class="popover_inner <?=$arItem['HINT']['CLASS']?>">
																<div class="popover_tip"><?=$arItem['HINT']['TEXT'] ? : $arItem['FILTER_HINT']?></div>
																<?if(isset($arItem['HINT']['SUBTEXT'])):?>
																	<div class="popover_tip__note">
																		<?=$arItem['HINT']['SUBTEXT']?>
																	</div>
																<?endif;?>
															</div>
														</div>
													</div>
												<?endif;?>
											</span>
										</div>
										<div class="field_head__sub"></div>
									</div>
									<div class="field_wrapper">
										<div class="jq-selectbox-multy jqselect">
											<div class="jq-selectbox-multy__select">
												<div class="jq-selectbox-multy__select-text">
													<label>
														<?foreach($arItem["VALUES"] as $val => $ar):?>
															<input type="checkbox"
																   value="<?= $ar["HTML_VALUE"]?>"
																   name="<?= $ar["CONTROL_NAME"]?>"
																   id="<?= $ar["CONTROL_ID"]?>_select"
																<?= $ar["CHECKED"]? 'checked="checked"': ''?>
																   <?if ($ar["DISABLED"] && !$ar["CHECKED"]):?>disabled<?endif?> >
															<?= $ar["VALUE"];?>
															<?break;?>
														<?endforeach?>
													</label>
												</div>
												<div class="jq-selectbox-multy__trigger">
													<div class="jq-selectbox-multy__trigger-arrow"></div>
												</div>
											</div>
											<div class="jq-selectbox-multy__dropdown">
												<ul>
													<?foreach($arItem["VALUES"] as $val => $ar):?>
													<li>
														<label for="<?= $ar["CONTROL_ID"]?>">
															<input type="checkbox"
																   value="<?= $ar["HTML_VALUE"]?>"
																   name="<?= $ar["CONTROL_NAME"]?>"
																   id="<?= $ar["CONTROL_ID"]?>"
																	<?= $ar["CHECKED"]? 'checked="checked"': ''?>
																   <?if ($ar["DISABLED"] && !$ar["CHECKED"]):?>disabled<?endif?>
																/>
															<span class="text"><?= $ar["VALUE"];?></span>
														</label>
													</li>
													<?endforeach;?>
												</ul>
											</div>
										</div>
									</div>
									<?/*
									<div class="btn_show_wrapper">
										<input class="btn btn-primary btn-show" type="submit" name="set_filter" value="<?=GetMessage("RZ_PODOBRAT_")?>" />
									</div>
									*/?>
								</div>
							<?++$counter;?>
						<?endif;?>
				<?endforeach;?>
				</div><!--.sidebar_filter__inner_group-->

			<?foreach($arResult["ITEMS"] as $key=>$arItem):
			$key = md5($key);
			?>
			<?if(isset($arItem["PRICE"])):?>
				<?
				if (!$arItem["VALUES"]["MIN"]["VALUE"] || !$arItem["VALUES"]["MAX"]["VALUE"] || $arItem["VALUES"]["MIN"]["VALUE"] == $arItem["VALUES"]["MAX"]["VALUE"])
					continue;
				$minVal = intval($arItem["VALUES"]["MIN"]["VALUE"]);
				if (isset($arItem["VALUES"]["MIN"]["HTML_VALUE"])) {
					$minVal = intval($arItem["VALUES"]["MIN"]["HTML_VALUE"]);
				}
				$maxVal = intval($arItem["VALUES"]["MAX"]["VALUE"]);
				if (isset($arItem["VALUES"]["MAX"]["HTML_VALUE"])) {
					$maxVal = intval($arItem["VALUES"]["MAX"]["HTML_VALUE"]);
				}
				?>
				<div class="sidebar_filter__inner seach_form__inner_price">
					<div class="seach_form__inner_price__head"><?=GetMessage("RZ_TCENOVOJ_DIAPAZON")?></div>
					<div class="range_prices_wrapper">
						<?=GetMessage("RZ_OT")?>
						<input name="<?= $arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>"
							   id="<?= $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>"
							   data-val="<?=$arItem["VALUES"]["MIN"]["VALUE"]?>"
							   value="<?= $arItem["VALUES"]["MIN"]["HTML_VALUE"] ?>"
							   type="text" class="form-control form-control-small price_from">
						<?=GetMessage("RZ_DO")?>
						<input name="<?= $arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>"
							   id="<?= $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>"
							   data-val="<?=$arItem["VALUES"]["MAX"]["VALUE"]?>"
							   value="<?= $arItem["VALUES"]["MAX"]["HTML_VALUE"] ?>"
							   type="text" class="form-control form-control-small price_to">
						<span class="b-rub"><?=GetMessage('SHIN_RUB_SYMBOL')?></span>
					</div>
					<div class="range_slider_wrapper">
						<div class="js-price-range-slider range_slider"></div>
					</div>
				</div>
			<?endif?>
		<?endforeach?>
		</div>
			<div class="sidebar_filter__submit_wrapper">
				<div class="collapse_btn_wrapper">
					<span class="btn btn-default btn-collapse">
						<span class="hide-filter">
							<span class="btn-inner"><?=GetMessage("RZ_SKRIT_FILTR")?></span>
							<span class="flaticon-arrow215"></span>
						</span>
						<span class="show-filter">
							<span class="btn-inner"><?=GetMessage("RZ_POKAZAT_FILTR")?></span>
							<span class="flaticon-arrow222"></span>
						</span>
					</span>
				</div>
				<noscript>
					<div class="sidebar_filter__submit">
						<input class="btn btn-default btn-match" type="submit" name="set_filter" value="<?=GetMessage("RZ_PODOBRAT_")?>" />
					</div>
				</noscript>
				<div>
					<button class="form_reset_button" type="reset"><?=GetMessage("RZ_SBROSIT__VSE_PARAMETRI_POISKA")?></button>
				</div>
				<div class="clearfix"></div>
			</div>
		</form>
</aside>
