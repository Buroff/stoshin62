<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use \Yenisite\Shinmarket\ManagerCalc;
use \Yenisite\Core\Ajax;
use \Yenisite\Core\Tools;

$bAjax = Ajax::isAjax();
$bHasTxDB = CRZShinmarketSettings::isTiersSolution() && ManagerCalc::tablesExist();

if ($bHasTxDB) {
    $arParamsOfCalc = Tools::getPararmsOfCMP($_SERVER['DOCUMENT_ROOT'] . SITE_DIR . 'include_areas/site_template/filter_on_main.php', true);
    $arSubTiers = CRZShinmarket::getSubSections($arParamsOfCalc['F1_IBLOCK_SECTION_ID']);
    $arSubWheels = CRZShinmarket::getSubSections($arParamsOfCalc['F2_IBLOCK_SECTION_ID']);
    $arSections = array($arParamsOfCalc['F1_IBLOCK_SECTION_ID'], $arParamsOfCalc['F2_IBLOCK_SECTION_ID']);
    $arSections = array_merge($arSections, $arSubTiers);
    $arSections = array_merge($arSections, $arSubWheels);
}

$bInNeededSection = in_array($arParams['SECTION_ID'],$arSections);
$arResult['IN_RIGHT_SECTION'] = $bInNeededSection;

if (isset($arParams['SECTION_ID']) && intval($arParams['SECTION_ID']) > 0) {
	$dbSection = CIBlockSection::GetByID($arParams['SECTION_ID']);
	$dbSection->SetUrlTemplates();
	$arSection = $dbSection->GetNext();
	$arResult['FORM_ACTION'] = $arSection['SECTION_PAGE_URL'];
}
$arPropsID = array();
foreach ($arResult['ITEMS'] as $key => &$arItem) {
	$arPropsID[$arItem['ID']] = $key;
	if ($arItem['PROPERTY_TYPE'] == "E") {
		foreach ($arItem['VALUES'] as $id => &$arValue) {
			$arValue['ICON_CLASS'] = \Yenisite\Core\Catalog::getIconClassFromPropId($arItem['ID'], $id);
		}
		unset($arValue);
	}
}
unset($arItem);

$dbProp = CIBlockProperty::GetList(array(), array(
	'ACTIVE' => 'Y',
	'IBLOCK_ID' => $arParams['IBLOCK_ID']
));

while ($arProp = $dbProp->Fetch()) {
	if (!isset($arPropsID[$arProp['ID']])) continue;
	if (isset($arProp['HINT']) && strlen($arProp['HINT']) > 0) {
		$decoded = json_decode($arProp['HINT'], true);
		if (isset($decoded)) {
			$arResult['ITEMS'][$arPropsID[$arProp['ID']]]['HINT'] = $decoded;
		}
	}
}

if ($bHasTxDB && !$bAjax && $bInNeededSection){
    $arResult['VENDORS'] = ManagerCalc::processOnlyVendors();
    $carId = htmlspecialcharsbx($_REQUEST['car-id']);
    if (!empty($carId)){
        $arResult['OTHERS_PROPS'] = ManagerCalc::proccesItemsByIdOfCar($carId);
    } else {
        $arResult['OTHERS_PROPS'] = ManagerCalc::processItemsByVendor($arResult['VENDORS'][0]);
    }
    $arOptionsBDIds = CRZShinmarket::getPropsIDOfTiersWheels();
    CRZShinmarket::setValuesForTXProps($arResult['OTHERS_PROPS']['MODELS'][0],$arOptionsBDIds);
    CRZShinmarket::filterValuesOfFilterByTXProps($arResult,$arOptionsBDIds);
}

$arrFilter = $GLOBALS[$arParams['FILTER_NAME']];
if (is_array($arrFilter) && count($arrFilter) > 0) {
	foreach ($arrFilter as $key => $val) {
		if (strpos($key,'=PROPERTY_') !== false) {
			$propId = str_replace('=PROPERTY_','',$key);
			foreach ($val as $valueId) {
			    if (empty($arResult['ITEMS'][$propId]['VALUES'][$valueId]['CHECKED'])) continue;
				$arResult['CURRENT_VALUES'][] = array(
					'CONTROL_NAME' => $arResult['ITEMS'][$propId]['VALUES'][$valueId]['CONTROL_NAME'],
					'VALUE' => $arResult['ITEMS'][$propId]['VALUES'][$valueId]['VALUE'],
					'HTML_VALUE' => $arResult['ITEMS'][$propId]['VALUES'][$valueId]['HTML_VALUE'],
				);
			};
		}
	}
}

// --- удаляем лишние парамтры для умного фильра
// раздел диски
if($arResult['SECTION']['ID'] == STALNYE){
	foreach($arResult["ITEMS"] as $key=>$arItem){
		if($arItem["CODE"] == 'CAR_MARK' || $arItem["CODE"] == 'CAR_MODEL' || $arItem["CODE"] == 'YEAR' || $arItem["CODE"] == 'CAR_MODIFICATION'){
			unset($arResult["ITEMS"][$key]);
		}
	}
}


 

// --- ШИНЫ

// легковые, легкогрузовые, 4x4
if($arResult['SECTION']['ID'] == SHINY || $arResult['SECTION']['ID'] == LEGKOVYE || $arResult['SECTION']['ID'] == LEGKOGRUZOVYE || $arResult['SECTION']['ID'] == FOUR_FOUR){
	foreach($arResult["ITEMS"] as $key=>$arItem){

		if($arItem["CODE"] !== 'MANUFACTURER' && $arItem["CODE"] !== 'SEASON' && $arItem["CODE"] !== 'SHIRINA_PROFILA' && $arItem["CODE"] !== 'VISOTA_PROFILA' && $arItem["CODE"] !== 'DIAMETR'){
			unset($arResult["ITEMS"][$key]);
		}

	}
}

// грузовые шины
if($arResult['SECTION']['ID'] == GRUZOVYE){
	foreach($arResult["ITEMS"] as $key=>$arItem){

		if($arItem["CODE"] !== 'MANUFACTURER' && $arItem["CODE"] !== 'SEASON' && $arItem["CODE"] !== 'SHIRINA_PROFILA' && $arItem["CODE"] !== 'VISOTA_PROFILA' && $arItem["CODE"] !== 'DIAMETR' && $arItem["CODE"] !== 'AXLE'){
			unset($arResult["ITEMS"][$key]);
		}
	}
}





// --- ДИСКИ 
if($arResult['SECTION']['ID'] == DISKI || $arResult['SECTION']['ID'] == STALNYE_DISKI){
	foreach($arResult["ITEMS"] as $key=>$arItem){
		if($arItem["CODE"] == 'DIAMETR' || $arItem["CODE"] == 'SHIRINA_PROFILA' && $arItem["CODE"] == 'VISOTA_PROFILA' || $arItem["CODE"] == 'AXLE'){
			// pr($arItem);
			unset($arResult["ITEMS"][$key]);
		}
		
	}
}