(typeof(jQuery) != 'undefined') 
&& (function ($) {
    var $rangeSlider = $(".js-price-range-slider"),
    cars_selects = {'mark': 'select[name="tiers_makr_filter"]', 'model': 'select[name="tiers_model_filter"]','year':'select[name="tiers_year_filter"]','modif':'select[name="tiers_modif_filter"]'},
        arFilterTXControl = ['tiers_makr_filter','tiers_model_filter','tiers_year_filter','tiers_modif_filter', 'car_id'],
        refreshJustFilter = false;

    var sliderTrigger = null;
    if ($rangeSlider.length > 0) {
        $rangeSlider.each(function () {
            var $this = $(this),
                $parent = $this.closest('.seach_form__inner_price'),
                $priceTo = $parent.find('.price_to:eq(0)'),
                $priceFrom = $parent.find('.price_from:eq(0)'),
                priceMinVal = Math.floor(parseFloat($priceFrom.data('val'))),
                priceMaxVal = Math.ceil(parseFloat($priceTo.data('val'))),
                priceToVal = $priceTo.val(),
                priceFromVal = $priceFrom.val();
            if (priceToVal.length == 0) {
                priceToVal = priceMaxVal;
            }
            if (priceFromVal.length == 0) {
                priceFromVal = priceMinVal;
            }
            $this.noUiSlider({
                start: [priceFromVal, priceToVal],
                connect: true,
                step: 1,
                range: {
                    'min': priceMinVal,
                    'max': priceMaxVal
                },
                format: wNumb({
                    decimals: 0
                })
            });
            $this.on({
                set: function (e, vals) {
                    $priceFrom.val('');
                    $priceTo.val('');
                },
                change: function (e, vals) {
                    if ($priceFrom.val().length == 0 || $priceTo.val().length == 0) {
                        $priceFrom.val(vals[0]);
                        $priceTo.val(vals[1]);
                        $priceTo.trigger('change');
                    }
                }
            });

            var triggerSliderInput = function (val) {
                var $this = $(this);
                if ($this.val() != val && $this.val().length > 0) {
                    $this.val(val);
                    if (sliderTrigger != null) {
                        clearTimeout(sliderTrigger);
                    }
                    sliderTrigger = setTimeout(function () {
                        $this.trigger('change');
                    }, 700);
                }
            };
            $this.Link('lower').to($priceFrom, function (val) {
                triggerSliderInput.bind(this, val);
            });
            $this.Link('upper').to($priceTo, function (val) {
                triggerSliderInput.bind(this, val);
            });

            $this.Link('lower').to('-inline-<div class="tooltip"></div>', function (value) {
                $(this).html('<span class="tooltip_inner">' + value + '</span>');
            });

            $this.Link('upper').to('-inline-<div class="tooltip"></div>', function (value) {
                $(this).html('<span class="tooltip_inner">' + value + '</span>');
            });
        });
    }

    var $sectionFilter = $('#sectionFilter');

    $.each(cars_selects, function (key, val) {
        cars_selects[key] = $sectionFilter.find(val);
    });

    var $tagsWrapper = $sectionFilter.find('.tags_wrapper'),
        $tagsList = $tagsWrapper.find('.tags__list');
    $sectionFilter.on('click', '.tags__list .tag', function () {
        var $this = $(this),
            propID = $this.data('id'),
            $propInput = $sectionFilter.find('#' + propID),
            $stylerInput = $sectionFilter.find('#' + propID + '-styler');
        if ($stylerInput.length > 0) {
            $stylerInput.click();
        } else {
            $propInput.click();
        }
        $this.remove();
        if ($tagsWrapper.find('.tags__list .tag').length == 0) {
            $tagsWrapper.addClass('hidden');
        }
    });
    // $sectionFilter.on('click', '.btn-collapse', function () {
    //     var $this = $(this);

    //     $sectionFilter.find('.collapsed_area').slideToggle(function () {
    //         $this.toggleClass('collapsed');

    //         var submitBlock = $this.closest('.seach_form__submit').find('.submit');
    //         var buttons = submitBlock.find('.form_reset_button');
    //         buttons = buttons.add(submitBlock.find('.btn-find'));
    //         buttons.toggle();
    //     });
    // });
    
    $sectionFilter.on('change', 'input', function () {
        var idCheckbox = '',
            carId = $sectionFilter.find('[name="car-id"]').val();

        if ($(this).attr('id').indexOf('_select') != -1) {
            idCheckbox = $(this).attr('id').replace('_select', '') + '-styler';
        } else {
            idCheckbox = $(this).attr('id') + '_select-styler';
        }
        if (!refreshJustFilter) {
            if (!$sectionFilter.find('div#' + idCheckbox).hasClass('checked')) {
                $sectionFilter.find('div#' + idCheckbox).addClass('checked');
            } else {
                $sectionFilter.find('div#' + idCheckbox).removeClass('checked');
            }
        }
        var data = $sectionFilter.serializeArray();
        data.push({name: 'car_id', value: carId});
        data.push({name: 'arParams', value: $sectionFilter.data('arparams')});
        $.ajax({
            url: rz.AJAX_DIR + "catalog_Filter.php",
            data: data,
            dataType: "json",
            success: function (json) {
                rz.FILTER_AJAX = [];
                $.each(json.ITEMS, function (key, val) {
                    $.each(val.VALUES, function (valKey, valVal) {
                        ajaxValuePrepare(valVal);
                    });
                });
                $.each(json.HIDDEN, function (key, val) {
                    if (val.CONTROL_NAME.toUpperCase() != 'ARPARAMS' && $.inArray(val.CONTROL_NAME,arFilterTXControl) == -1) {
                        if (typeof(val.HTML_VALUE) != 'undefined' && val.HTML_VALUE.length > 0) {
                            rz.FILTER_AJAX.push({name: val.CONTROL_NAME, value: val.HTML_VALUE});
                        }
                    }
                });
                if (rz.FILTER_AJAX.length == 0) {
                    rz.FILTER_AJAX.push({name: 'resetFilter', value: 'Y'});
                }
                $(document).trigger('catalog_applyFilter');
                $tagsList.empty();
                $.each(rz.FILTER_AJAX, function (i, el) {
                    if ('html' in el) {
                        var $insNode = $('<span class="tag" data-id="' + el.name + '">' + el.html + '<span class="close flaticon-delete30"></span></span>');
                        $tagsList.append($insNode);
                    }
                });
                if ($tagsList.find('.tag').length == 0) {
                    $tagsWrapper.addClass('hidden');
                } else {
                    $tagsWrapper.removeClass('hidden');
                }
                refreshJustFilter = false;
            }
        })
    });

    $sectionFilter.on('click', '.form_reset_button', function (e) {
        e.preventDefault();
        var $this = $sectionFilter;
        var $jsSlider = $this.find('.js-price-range-slider');
        if ($jsSlider.length > 0) {
            $jsSlider.each(function () {
                var $this = $(this),
                    $parent = $this.closest('.sidebar_filter__inner'),
                    priceToVal = $parent.find('.price_to:eq(0)').data('val'),
                    priceFromVal = $parent.find('.price_from:eq(0)').data('val');
                $this.val([priceFromVal, priceToVal]);
            });
        }
        $this.clearForm();
        $this.find('input:visible:eq(0)').trigger('change');
    });

    var ajaxValuePrepare = function (val) {
        var $curItem = $('#' + val.CONTROL_ID);
        var $itemStyler = $('#' + val.CONTROL_ID + "-styler");
        var lastsymb = val.CONTROL_NAME.substr(-3);
        var isNum = false;
        if (lastsymb == 'MAX' || lastsymb == "MIN") {
            isNum = true;
        }
        if ('DISABLED' in val && val.DISABLED == 1 && !('CHECKED' in val) && val.CHECKED != 1) {
            if ($itemStyler.length > 0) {
                $itemStyler.addClass('disabled');
            }
            $curItem.attr('disabled', 'disabled');
        } else {
            if ($itemStyler.length > 0) {
                $itemStyler.removeClass('disabled');
            }
            $curItem.removeAttr('disabled');
        }
        if (('CHECKED' in val && val.CHECKED == 1) || isNum) {
            if (!isNum) {
                rz.FILTER_AJAX.push({name: val.CONTROL_NAME, value: val.HTML_VALUE, html: val.VALUE});
            } else {
                if (typeof(val.HTML_VALUE) != 'undefined' && val.HTML_VALUE.length > 0) {
                    rz.FILTER_AJAX.push({name: val.CONTROL_NAME, value: val.HTML_VALUE});
                }
            }
        } else{
            if ($itemStyler.length > 0) {
                $itemStyler.removeClass('checked');
            }
            $curItem.removeAttr('checked');
        }
    };

    cars_selects['mark'].on('change', function () {
        var vendor = $(this).find('option:selected').text();
        doAjaxGetValsOfCars('by_vendor', $(this), vendor);
    });
    cars_selects['model'].on('change', function () {
        var $this = $(this),
            vendor = $this.getSelOptFromForm(cars_selects['mark']).text(),
            model = $this.find('option:selected').text();
        doAjaxGetValsOfCars('by_vendor_model', $(this), vendor, model);
    });
    cars_selects['year'].on('change', function () {
        var $this = $(this),
            vendor = $this.getSelOptFromForm(cars_selects['mark']).text(),
            model = $this.getSelOptFromForm(cars_selects['model']).text(),
            year = $this.find('option:selected').text();
        doAjaxGetValsOfCars('by_vendor_model_year', $(this), vendor, model, year);
    });
    cars_selects['modif'].on('change', function () {
        var $this = $(this),
            vendor = $this.getSelOptFromForm(cars_selects['mark']).text(),
            model = $this.getSelOptFromForm(cars_selects['model']).text(),
            year = $this.getSelOptFromForm(cars_selects['year']).text(),
            modif = $this.find('option:selected').text();
        doAjaxGetValsOfCars('by_vendor_model_year_modif', $(this), vendor, model, year, modif);
    });

    var doAjaxGetValsOfCars = function (action, $this, strVendor, strModel, strYear, strModif) {
        var data = {},
            $container = $this.closest('#sectionFilter'),
            bHasInput = !!$container.find('[name="car-id"]').length,
            $inputCar = bHasInput ? $container.find('[name="car-id"]') : $('<input type="hidden" name="car-id"/>');

        if (typeof strVendor != 'undefined') {
            data['vendor'] = strVendor;
        }
        if (typeof strModel != 'undefined') {
            data['model'] = strModel;
        }
        if (typeof strYear != 'undefined') {
            data['year'] = strYear;
        }
        if (typeof strModif != 'undefined') {
            data['modif'] = strModif;
        }

        data['action_get_info'] = action;
        $container.setAjaxLoading();

        $.ajax({
            url: rz.AJAX_DIR + 'tiers_calc.php',
            data: data,
            dataType: 'json',
            method: 'POST',
            success: function (result) {
                var modif, years, carID, models, objData;
                if (!result.data_base.MODELS) {
                    objData = result.data_base;
                } else {
                    models = getModels(result.data_base.MODELS)
                    objData = result.data_base.MODELS[0];
                }
                modif = objData.MODIFICATIONS;
                years = objData.YEARS;
                carID = objData.SELECTED_CAR_ID;
                $inputCar.val(carID);
                if (!bHasInput) {
                    $container.append($inputCar);
                }
                setHtmlOptions(modif, cars_selects['modif']);
                setHtmlOptions(years, cars_selects['year']);
                setHtmlOptions(models, cars_selects['model']);
                $container.find('select').trigger('refresh');
                $container.stopAjaxLoading();
                refreshJustFilter = true;
                $container.find('input').eq(0).trigger('change');
            }
        });
    };
})(jQuery);