<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$arYears = $arResult['OTHERS_PROPS']['MODELS'][0]['YEARS'];
$strVendor = $arResult['OTHERS_PROPS']['MODELS'][0]['VENDOR'];
$arModels = $arResult['OTHERS_PROPS']['MODELS'];
$arModifications = $arResult['OTHERS_PROPS']['MODELS'][0]['MODIFICATIONS'];
?>
<div class="selects_car_wrap" style="display: none;">
    <div class="sel-car-wrap">
        <div class="descr"><?= GetMessage('RZ_MARK') ?></div>
        <select name="tiers_makr_filter" <?= empty($arResult['VENDORS']) ? 'class="disabled"' : '' ?>>
            <? foreach ($arResult['VENDORS'] as $keyVendor => $value): ?>
                <option value="<?= $keyVendor ?>" <?= $value == $strVendor ? 'selected' : '' ?>><?= $value ?></option>
            <? endforeach; ?>
        </select>
    </div>
    <div class="sel-car-wrap">
        <div class="descr"><?= GetMessage('RZ_MODEL') ?></div>
        <select name="tiers_model_filter" <?= empty($arModels) ? 'class="disabled"' : '' ?>>
            <? foreach ($arModels as $keyModel => $arModel): ?>
                <option value="<?= $keyModel ?>" <?= $keyModel == 0 ? 'selected' : '' ?>><?= $arModel['NAME'] ?></option>
            <? endforeach; ?>
        </select>
    </div>
    <div class="sel-car-wrap">
        <div class="descr"><?= GetMessage('RZ_YEAR') ?></div>
        <select name="tiers_year_filter" <?= empty($arModifications) ? 'class="disabled"' : '' ?>>
            <? foreach ($arYears as $keyYear => $value): ?>
                <option value="<?= $keyYear ?>" <?= $keyYear == 0 ? 'selected' : '' ?>><?= $value ?></option>
            <? endforeach; ?>
        </select>
    </div>
    <div class="sel-car-wrap">
        <div class="descr"><?= GetMessage('RZ_MODIF') ?></div>
        <select name="tiers_modif_filter" <?= empty($arYears) ? 'class="disabled"' : '' ?>>
            <? foreach ($arModifications as $keyModif => $value): ?>
                <option value="<?= $keyModif ?>" <?= $keyModif == 0 ? 'selected' : '' ?>><?= $value ?></option>
            <? endforeach; ?>
        </select>
    </div>
</div>