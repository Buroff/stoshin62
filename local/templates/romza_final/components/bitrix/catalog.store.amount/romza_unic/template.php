<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
if (!CModule::IncludeModule('yenisite.storeamount')) {
	echo GetMessage("RZ_MODULE_NOT_INSTALLED");
	return;
}
if (method_exists($this, 'setFrameMode')) $this->setFrameMode(true); ?>
	<?
	$arParams['COLOR_NONE_BG'] = (strlen($arParams['COLOR_NONE_BG']) > 0) ? $arParams['COLOR_NONE_BG'] : '#C0C0C0';
	$arParams['COLOR_NONE_TEXT'] = (strlen($arParams['COLOR_NONE_TEXT']) > 0) ? $arParams['COLOR_NONE_TEXT'] : '#000000';
	$arParams['COLOR_MANY_BG'] = (strlen($arParams['COLOR_MANY_BG']) > 0) ? $arParams['COLOR_MANY_BG'] : '#328D00';
	$arParams['COLOR_MANY_TEXT'] = (strlen($arParams['COLOR_MANY_TEXT']) > 0) ? $arParams['COLOR_MANY_TEXT'] : '#FFFFFF';
	$arParams['COLOR_AVERAGE_BG'] = (strlen($arParams['COLOR_AVERAGE_BG']) > 0) ? $arParams['COLOR_AVERAGE_BG'] : '#FFA500';
	$arParams['COLOR_AVERAGE_TEXT'] = (strlen($arParams['COLOR_AVERAGE_TEXT']) > 0) ? $arParams['COLOR_AVERAGE_TEXT'] : '#FFFFFF';
	$arParams['COLOR_FEW_BG'] = (strlen($arParams['COLOR_FEW_BG']) > 0) ? $arParams['COLOR_FEW_BG'] : '#FF0000';
	$arParams['COLOR_FEW_TEXT'] = (strlen($arParams['COLOR_FEW_TEXT']) > 0) ? $arParams['COLOR_FEW_TEXT'] : '#FFFFFF';
	?>
	<?
$arParams['MANY_VAL'] = (empty($arParams['MANY_VAL'])) ? 10 : $arParams['MANY_VAL'];
$arParams['AVERAGE_VAL'] = (empty($arParams['AVERAGE_VAL'])) ? 5 : $arParams['AVERAGE_VAL'];

$arParams['MANY_NAME'] = (empty($arParams['MANY_NAME'])) ? GetMessage('MANY_NAME_DEFAULT') : $arParams['MANY_NAME'];
$arParams['AVERAGE_NAME'] = (empty($arParams['AVERAGE_NAME'])) ? GetMessage('AVERAGE_NAME_DEFAULT') : $arParams['AVERAGE_NAME'];
$arParams['FEW_NAME'] = (empty($arParams['FEW_NAME'])) ? GetMessage('FEW_NAME_DEFAULT') : $arParams['FEW_NAME'];
$arParams['NONE_NAME'] = (empty($arParams['NONE_NAME'])) ? GetMessage('NONE_NAME_DEFAULT') : $arParams['NONE_NAME'];

$arParams['IMG_WIDTH'] = (empty($arParams['IMG_WIDTH'])) ? '400' : $arParams['IMG_WIDTH'];
$arParams['IMG_HEIGHT'] = (empty($arParams['IMG_HEIGHT'])) ? '200' : $arParams['IMG_HEIGHT'];

$arParams['VIEW_MODE'] = trim($arParams['VIEW_MODE']);
$arParams['ONE_POPUP'] = ($arParams['ONE_POPUP'] == 'Y') ? 'Y' : 'N';

$arParams['ONLY_NAME_IN_LIST'] = ($arParams['ONLY_NAME_IN_LIST'] == 'Y') ? 'Y' : 'N';
$arParams['HAS_NAME'] = (empty($arParams['HAS_NAME'])) ? GetMessage('HAS_NAME_DEFAULT') : $arParams['HAS_NAME'];
//also uses in component_epilog.php
$arParams['WIDTH'] = (empty($arParams['WIDTH'])) ? '450' : $arParams['WIDTH'];

$arViewModes = array('TEXT' => 0, 'COLS' => 0, 'NUMB' => 0);
if (!isset($arViewModes[$arParams['VIEW_MODE']])) {
	$arParams['VIEW_MODE'] = 'COLS';
}
if(!function_exists('rzSwitchAmount')) {
	function rzSwitchAmount($amount, $arParams) {
		$amount = intval($amount);
		switch (true) {
			case ($amount >= $arParams['MANY_VAL']):
				$class = 'many';
				$title = $arParams['MANY_NAME'];
				$style = "background:".$arParams['COLOR_MANY_BG'].";color:".$arParams['COLOR_MANY_BG'].";";
				break;
			case ($amount >= $arParams['AVERAGE_VAL']):
				$class = 'average';
				$title = $arParams['AVERAGE_NAME'];
				$style = "background:".$arParams['COLOR_AVERAGE_BG'].";color:".$arParams['COLOR_AVERAGE_BG'].";";
				break;
			case (($amount < $arParams['AVERAGE_VAL'] && $amount != 0)):
				$class = 'few';
				$title = $arParams['FEW_NAME'];
				$style = "background:".$arParams['COLOR_FEW_BG'].";color:".$arParams['COLOR_FEW_BG'].";";
				break;
			default:
				$class = 'none';
				$title = $arParams['NONE_NAME'];
				$style = "background:".$arParams['COLOR_NONE_BG'].";color:".$arParams['COLOR_NONE_TEXT'].";";
		}
		return array($class,$title,$style);
	}
}
$arParams['RND'] = $arParams['RND'] ? : $this->randString();
$rnd = $arParams['RND'].'_'.rand();
if (!empty($arResult["STORES"])): ?>
	<div class="RzStore" id="RzStore_<?= $rnd ?>" >
		<? if (strlen($arParams["MAIN_TITLE"]) > 0): ?>
			<div class="title"><?= $arParams["MAIN_TITLE"] ?></div>
		<? endif ?>
		<? if ($arParams['ONE_POPUP'] != 'Y'): ?>
			<? //************************    DETAIL VIEW    ************************/?>
			<? $arItems = $arResult['STORES']; ?>
			<? if ($arResult['IS_SKU']) : ?>
				<?foreach($arResult['JS']['SKU'] as $skuID => $arSKU):?>
					<div class="RzStore__sku<?= ($arParams['CUR_OFFER'] == $skuID) ? ' active' : '' ?>" id="RzStore__sku_<?= $skuID ?>">
						<?
						foreach ($arSKU as $storeID => $amount) {
							$sID = array_search($storeID, $arResult['JS']['STORES']);
							$arItems[$sID]['AMOUNT'] = $amount;
							$arItems[$sID]['REAL_AMOUNT'] = $amount;
						}
						?>
						<? include 'detail_view.php' ?>
					</div>
				<? endforeach ?>
			<? else :?>
				<? include 'detail_view.php' ?>
			<? endif ?>
		<? endif ?>
		<? //************************    LIST VIEW    ************************/?>
		<? if ($arParams['ONE_POPUP'] == 'Y'): ?>
			<?
			$arItems = $arResult;
			?>
			<?if($arResult['IS_SKU'] && !empty($arResult['JS']['SKU'])):?>
				<?foreach($arResult['JS']['SKU'] as $skuID => $arSKU):?>
					<div class="RzStore__sku<?= ($arParams['CUR_OFFER'] == $skuID) ? ' active' : '' ?>" id="RzStore__sku_<?= $skuID ?>">
						<?
						$arItems['TOTAL_AMOUNT'] = 0;
						foreach ($arSKU as $storeID => $amount) {
							$sID = array_search($storeID, $arResult['JS']['STORES']);
							$arItems['STORES'][$sID]['AMOUNT'] = $amount;
							$arItems['STORES'][$sID]['REAL_AMOUNT'] = $amount;
							$arItems['TOTAL_AMOUNT'] += $amount;
						}
						?>
						<? include 'list_view.php' ?>
					</div>
				<? endforeach ?>
			<? else: ?>
				<?
				foreach($arResult['STORES'] as $arStore) {
					$arItems['TOTAL_AMOUNT'] += $arStore['AMOUNT'];
				}
				include 'list_view.php';
				?>
			<? endif ?>
		<? endif ?>
	</div>
<? endif ?>