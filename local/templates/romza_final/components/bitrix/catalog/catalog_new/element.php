<? use Yenisite\Core\Resize;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

global $rz_options;
if(!empty($rz_options['GEOIP']['PRICES'])) {
	$arParams["PRICE_CODE"] = $rz_options['GEOIP']['PRICES'];
}

if(!empty($rz_options['GEOIP']['STORES'])) {
	$arParams['STORES'] = $rz_options['GEOIP']['STORES'];
}

$b404 = true;
if($arParams['SEF_MODE'] == 'Y' || $arParams['SEF_MODE'] == true){
	if (!empty($arResult['VARIABLES']['SECTION_CODE'])) {
		/** @noinspection PhpDynamicAsStaticMethodCallInspection */
		$rsSection = CIBlockSection::GetList(array(),
			array('CODE' => $arResult['VARIABLES']['SECTION_CODE'], 'IBLOCK_ID' => $arParams['IBLOCK_ID']),
			false,
			array("ID")
		);
		if($arSection = $rsSection->Fetch()) {
			if(!empty($arSection['ID'])) {
				$b404 = false;
			}
		}
	}
} else {
	if(!empty($arParams['SECTION_ID_VARIABLE'])
		&& isset($arResult['VARIABLES']['SECTION_CODE'][$arParams['SECTION_ID_VARIABLE']])
		&& intval($arResult['VARIABLES']['SECTION_CODE'][$arParams['SECTION_ID_VARIABLE']]) > 0
	) {
		$b404 = false;
	}
}
?>
<?if($b404):?>
	<?define('ERROR_404','Y')?>
<?else:?>
	<? $APPLICATION->SetPageProperty('SINGLE_COLUMN', 'Y') ?>
	<? $APPLICATION->IncludeComponent(
		"bitrix:breadcrumb",
		"default",
		Array(),
		false
	); ?>

	<? 
	/*$ElementID = $APPLICATION->IncludeComponent(
		"bitrix:catalog.element",
		"main",
		array(
			"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"PROPERTY_CODE" => $arParams["DETAIL_PROPERTY_CODE"],
			"META_KEYWORDS" => $arParams["DETAIL_META_KEYWORDS"],
			"META_DESCRIPTION" => $arParams["DETAIL_META_DESCRIPTION"],
			"BROWSER_TITLE" => $arParams["DETAIL_BROWSER_TITLE"],
			"BASKET_URL" => $arParams["BASKET_URL"],
			"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
			"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
			"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
			"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
			"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
			"CACHE_TYPE" => $arParams["CACHE_TYPE"],
			"CACHE_TIME" => $arParams["CACHE_TIME"],
			"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
			"SET_TITLE" => $arParams["SET_TITLE"],
			"SET_STATUS_404" => $arParams["SET_STATUS_404"],
			"PRICE_CODE" => $arParams["PRICE_CODE"],
			"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
			"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
			"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
			"PRICE_VAT_SHOW_VALUE" => $arParams["PRICE_VAT_SHOW_VALUE"],
			"USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
			"PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],
			"ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
			"PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
			"LINK_IBLOCK_TYPE" => $arParams["LINK_IBLOCK_TYPE"],
			"LINK_IBLOCK_ID" => $arParams["LINK_IBLOCK_ID"],
			"LINK_PROPERTY_SID" => $arParams["LINK_PROPERTY_SID"],
			"LINK_ELEMENTS_URL" => $arParams["LINK_ELEMENTS_URL"],

			"OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
			"OFFERS_FIELD_CODE" => $arParams["DETAIL_OFFERS_FIELD_CODE"],
			"OFFERS_PROPERTY_CODE" => array_merge($arParams["DETAIL_OFFERS_PROPERTY_CODE"],$arParams['OFFER_TREE_PROPS']),
			"OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
			"OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
			"OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
			"OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],

			"ELEMENT_ID" => $arResult["VARIABLES"]["ELEMENT_ID"],
			"ELEMENT_CODE" => $arResult["VARIABLES"]["ELEMENT_CODE"],
			"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
			"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
			"SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
			"DETAIL_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["element"],
			'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
			'CURRENCY_ID' => $arParams['CURRENCY_ID'],
			'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
			'USE_ELEMENT_COUNTER' => $arParams['USE_ELEMENT_COUNTER'],

			'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
			'LABEL_PROP' => $arParams['LABEL_PROP'],
			'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
			'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS'],
			'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
			'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
			'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
			'SHOW_MAX_QUANTITY' => $arParams['DETAIL_SHOW_MAX_QUANTITY'],
			'MESS_BTN_BUY' => $arParams['MESS_BTN_BUY'],
			'MESS_BTN_ADD_TO_BASKET' => $arParams['MESS_BTN_ADD_TO_BASKET'],
			'MESS_BTN_SUBSCRIBE' => $arParams['MESS_BTN_SUBSCRIBE'],
			'MESS_BTN_COMPARE' => $arParams['MESS_BTN_COMPARE'],
			'MESS_NOT_AVAILABLE' => $arParams['MESS_NOT_AVAILABLE'],
			'USE_VOTE_RATING' => $arParams['DETAIL_USE_VOTE_RATING'],
			'VOTE_DISPLAY_AS_RATING' => (isset($arParams['DETAIL_VOTE_DISPLAY_AS_RATING']) ? $arParams['DETAIL_VOTE_DISPLAY_AS_RATING'] : ''),
			'USE_COMMENTS' => $arParams['DETAIL_USE_COMMENTS'],
			'BLOG_USE' => (isset($arParams['DETAIL_BLOG_USE']) ? $arParams['DETAIL_BLOG_USE'] : ''),
			'VK_USE' => (isset($arParams['DETAIL_VK_USE']) ? $arParams['DETAIL_VK_USE'] : ''),
			'VK_API_ID' => (isset($arParams['DETAIL_VK_API_ID']) ? $arParams['DETAIL_VK_API_ID'] : 'API_ID'),
			'FB_USE' => (isset($arParams['DETAIL_FB_USE']) ? $arParams['DETAIL_FB_USE'] : ''),
			'FB_APP_ID' => (isset($arParams['DETAIL_FB_APP_ID']) ? $arParams['DETAIL_FB_APP_ID'] : ''),
			'BRAND_USE' => (isset($arParams['DETAIL_BRAND_USE']) ? $arParams['DETAIL_BRAND_USE'] : 'N'),
			'BRAND_PROP_CODE' => (isset($arParams['DETAIL_BRAND_PROP_CODE']) ? $arParams['DETAIL_BRAND_PROP_CODE'] : ''),
			'DISPLAY_NAME' => (isset($arParams['DETAIL_DISPLAY_NAME']) ? $arParams['DETAIL_DISPLAY_NAME'] : ''),
			'ADD_DETAIL_TO_SLIDER' => (isset($arParams['DETAIL_ADD_DETAIL_TO_SLIDER']) ? $arParams['DETAIL_ADD_DETAIL_TO_SLIDER'] : ''),
			'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
			"ADD_SECTIONS_CHAIN" => (isset($arParams["ADD_SECTIONS_CHAIN"]) ? $arParams["ADD_SECTIONS_CHAIN"] : ''),
			"ADD_ELEMENT_CHAIN" => (isset($arParams["ADD_ELEMENT_CHAIN"]) ? $arParams["ADD_ELEMENT_CHAIN"] : ''),
			"DISPLAY_PREVIEW_TEXT_MODE" => (isset($arParams['DETAIL_DISPLAY_PREVIEW_TEXT_MODE']) ? $arParams['DETAIL_DISPLAY_PREVIEW_TEXT_MODE'] : ''),
			"DETAIL_PICTURE_MODE" => (isset($arParams['DETAIL_DETAIL_PICTURE_MODE']) ? $arParams['DETAIL_DETAIL_PICTURE_MODE'] : ''),
			"FORUM_ID" => $arParams['FORUM_ID'],
			'RESIZER_PRODUCT' => $arParams['RESIZER_PRODUCT'],
			'RESIZER_PRODUCT_BIG' => $arParams['RESIZER_PRODUCT_BIG'],
			'RESIZER_PRODUCT_THUMB' => $arParams['RESIZER_PRODUCT_THUMB'],
			'DISPLAY_COMPARE' => $arParams['USE_COMPARE'],
			'FILTER_NAME' => $arParams['FILTER_NAME'],
			'RZ_COMMENTS_IBLOCK_TYPE' => $arParams['RZ_COMMENTS_IBLOCK_TYPE'],
			'RZ_COMMENTS_IBLOCK_ID' => $arParams['RZ_COMMENTS_IBLOCK_ID'],
			'RZ_COMMENTS_NAME_FIELD' => $arParams['RZ_COMMENTS_NAME_FIELD'],
			'RZ_COMMENTS_SUCCESS_TEXT' => $arParams['RZ_COMMENTS_SUCCESS_TEXT'],
			'RZ_COMMENTS_PRINT_FIELDS' => $arParams['RZ_COMMENTS_PRINT_FIELDS'],
			'RZ_COMMENTS_NAME' => $arParams['RZ_COMMENTS_NAME'],
			'RZ_COMMENTS_EMAIL' => $arParams['RZ_COMMENTS_EMAIL'],
			'RZ_COMMENTS_PHONE' => $arParams['RZ_COMMENTS_PHONE'],
			'ONECLICK_PERSON_TYPE_ID' => $arParams["ONECLICK_PERSON_TYPE_ID"],
			'ONECLICK_SHOW_FIELDS' => $arParams["ONECLICK_SHOW_FIELDS"],
			'ONECLICK_REQ_FIELDS' => $arParams["ONECLICK_REQ_FIELDS"],
			'ONECLICK_ALLOW_AUTO_REGISTER' => $arParams["ONECLICK_ALLOW_AUTO_REGISTER"],
			'ONECLICK_USE_CAPTCHA' => $arParams["ONECLICK_USE_CAPTCHA"],
			'ONECLICK_MESSAGE_OK' => $arParams["ONECLICK_MESSAGE_OK"],
			'ONECLICK_PAY_SYSTEM_ID' => $arParams["ONECLICK_PAY_SYSTEM_ID"],
			'ONECLICK_DELIVERY_ID' => $arParams["ONECLICK_DELIVERY_ID"],
			'ONECLICK_AS_EMAIL' => $arParams["ONECLICK_AS_EMAIL"],
			'ONECLICK_AS_NAME' => $arParams["ONECLICK_AS_NAME"],
			'ONECLICK_SEND_REGISTER_EMAIL' => $arParams["ONECLICK_SEND_REGISTER_EMAIL"],
			'ONECLICK_FIELD_PLACEHOLDER' => $arParams["ONECLICK_FIELD_PLACEHOLDER"],
			'ONECLICK_FIELD_QUANTITY' => $arParams["ONECLICK_FIELD_QUANTITY"],
			'MAIN_TITLE' => $arParams['MAIN_TITLE'],
			'USE_STORE' => $rz_options['show_stores_in_element'],
			'USE_STORE_PHONE' => $arParams['USE_STORE_PHONE'],
			'USE_STORE_SCHEDULE' => $arParams['USE_STORE_SCHEDULE'],
			'SHOW_EMPTY_STORE' => $arParams['SHOW_EMPTY_STORE'],
			'STORE_MANY_VAL' => $arParams['STORE_MANY_VAL'],
			'STORE_AVERAGE_VAL' => $arParams['STORE_AVERAGE_VAL'],
			'STORES' => $arParams['STORES'],
			'SHOW_CATCH_BUY' => $rz_options['show_catchbuy_in_element'] != 'N',
			'USE_ONE_CLICK' => $rz_options['show_one_click_in_element'],
		),
		$component
	); 
*/
 

	$ElementID = $APPLICATION->IncludeComponent(
		"bitrix:catalog.element",
		"main_new",
		array(
			"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"PROPERTY_CODE" => $arParams["DETAIL_PROPERTY_CODE"],
			"META_KEYWORDS" => $arParams["DETAIL_META_KEYWORDS"],
			"META_DESCRIPTION" => $arParams["DETAIL_META_DESCRIPTION"],
			"BROWSER_TITLE" => $arParams["DETAIL_BROWSER_TITLE"],
			"BASKET_URL" => $arParams["BASKET_URL"],
			"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
			"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
			"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
			"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
			"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
			"CACHE_TYPE" => $arParams["CACHE_TYPE"],
			"CACHE_TIME" => $arParams["CACHE_TIME"],
			"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
			"SET_TITLE" => $arParams["SET_TITLE"],
			"SET_STATUS_404" => $arParams["SET_STATUS_404"],
			"PRICE_CODE" => $arParams["PRICE_CODE"],
			"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
			"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
			"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
			"PRICE_VAT_SHOW_VALUE" => $arParams["PRICE_VAT_SHOW_VALUE"],
			"USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
			"PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],
			"ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
			"PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
			"LINK_IBLOCK_TYPE" => $arParams["LINK_IBLOCK_TYPE"],
			"LINK_IBLOCK_ID" => $arParams["LINK_IBLOCK_ID"],
			"LINK_PROPERTY_SID" => $arParams["LINK_PROPERTY_SID"],
			"LINK_ELEMENTS_URL" => $arParams["LINK_ELEMENTS_URL"],

			"OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
			"OFFERS_FIELD_CODE" => $arParams["DETAIL_OFFERS_FIELD_CODE"],
			"OFFERS_PROPERTY_CODE" => array_merge($arParams["DETAIL_OFFERS_PROPERTY_CODE"],$arParams['OFFER_TREE_PROPS']),
			"OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
			"OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
			"OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
			"OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],

			"ELEMENT_ID" => $arResult["VARIABLES"]["ELEMENT_ID"],
			"ELEMENT_CODE" => $arResult["VARIABLES"]["ELEMENT_CODE"],
			"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
			"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
			"SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
			"DETAIL_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["element"],
			'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
			'CURRENCY_ID' => $arParams['CURRENCY_ID'],
			'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
			'USE_ELEMENT_COUNTER' => $arParams['USE_ELEMENT_COUNTER'],

			'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
			'LABEL_PROP' => $arParams['LABEL_PROP'],
			'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
			'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS'],
			'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
			'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
			'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
			'SHOW_MAX_QUANTITY' => $arParams['DETAIL_SHOW_MAX_QUANTITY'],
			'MESS_BTN_BUY' => $arParams['MESS_BTN_BUY'],
			'MESS_BTN_ADD_TO_BASKET' => $arParams['MESS_BTN_ADD_TO_BASKET'],
			'MESS_BTN_SUBSCRIBE' => $arParams['MESS_BTN_SUBSCRIBE'],
			'MESS_BTN_COMPARE' => $arParams['MESS_BTN_COMPARE'],
			'MESS_NOT_AVAILABLE' => $arParams['MESS_NOT_AVAILABLE'],
			'USE_VOTE_RATING' => $arParams['DETAIL_USE_VOTE_RATING'],
			'VOTE_DISPLAY_AS_RATING' => (isset($arParams['DETAIL_VOTE_DISPLAY_AS_RATING']) ? $arParams['DETAIL_VOTE_DISPLAY_AS_RATING'] : ''),
			'USE_COMMENTS' => $arParams['DETAIL_USE_COMMENTS'],
			'BLOG_USE' => (isset($arParams['DETAIL_BLOG_USE']) ? $arParams['DETAIL_BLOG_USE'] : ''),
			'VK_USE' => (isset($arParams['DETAIL_VK_USE']) ? $arParams['DETAIL_VK_USE'] : ''),
			'VK_API_ID' => (isset($arParams['DETAIL_VK_API_ID']) ? $arParams['DETAIL_VK_API_ID'] : 'API_ID'),
			'FB_USE' => (isset($arParams['DETAIL_FB_USE']) ? $arParams['DETAIL_FB_USE'] : ''),
			'FB_APP_ID' => (isset($arParams['DETAIL_FB_APP_ID']) ? $arParams['DETAIL_FB_APP_ID'] : ''),
			'BRAND_USE' => (isset($arParams['DETAIL_BRAND_USE']) ? $arParams['DETAIL_BRAND_USE'] : 'N'),
			'BRAND_PROP_CODE' => (isset($arParams['DETAIL_BRAND_PROP_CODE']) ? $arParams['DETAIL_BRAND_PROP_CODE'] : ''),
			'DISPLAY_NAME' => (isset($arParams['DETAIL_DISPLAY_NAME']) ? $arParams['DETAIL_DISPLAY_NAME'] : ''),
			'ADD_DETAIL_TO_SLIDER' => (isset($arParams['DETAIL_ADD_DETAIL_TO_SLIDER']) ? $arParams['DETAIL_ADD_DETAIL_TO_SLIDER'] : ''),
			'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
			"ADD_SECTIONS_CHAIN" => (isset($arParams["ADD_SECTIONS_CHAIN"]) ? $arParams["ADD_SECTIONS_CHAIN"] : ''),
			"ADD_ELEMENT_CHAIN" => (isset($arParams["ADD_ELEMENT_CHAIN"]) ? $arParams["ADD_ELEMENT_CHAIN"] : ''),
			"DISPLAY_PREVIEW_TEXT_MODE" => (isset($arParams['DETAIL_DISPLAY_PREVIEW_TEXT_MODE']) ? $arParams['DETAIL_DISPLAY_PREVIEW_TEXT_MODE'] : ''),
			"DETAIL_PICTURE_MODE" => (isset($arParams['DETAIL_DETAIL_PICTURE_MODE']) ? $arParams['DETAIL_DETAIL_PICTURE_MODE'] : ''),
			"FORUM_ID" => $arParams['FORUM_ID'],
			'RESIZER_PRODUCT' => $arParams['RESIZER_PRODUCT'],
			'RESIZER_PRODUCT_BIG' => $arParams['RESIZER_PRODUCT_BIG'],
			'RESIZER_PRODUCT_THUMB' => $arParams['RESIZER_PRODUCT_THUMB'],
			'DISPLAY_COMPARE' => $arParams['USE_COMPARE'],
			'FILTER_NAME' => $arParams['FILTER_NAME'],
			'RZ_COMMENTS_IBLOCK_TYPE' => $arParams['RZ_COMMENTS_IBLOCK_TYPE'],
			'RZ_COMMENTS_IBLOCK_ID' => $arParams['RZ_COMMENTS_IBLOCK_ID'],
			'RZ_COMMENTS_NAME_FIELD' => $arParams['RZ_COMMENTS_NAME_FIELD'],
			'RZ_COMMENTS_SUCCESS_TEXT' => $arParams['RZ_COMMENTS_SUCCESS_TEXT'],
			'RZ_COMMENTS_PRINT_FIELDS' => $arParams['RZ_COMMENTS_PRINT_FIELDS'],
			'RZ_COMMENTS_NAME' => $arParams['RZ_COMMENTS_NAME'],
			'RZ_COMMENTS_EMAIL' => $arParams['RZ_COMMENTS_EMAIL'],
			'RZ_COMMENTS_PHONE' => $arParams['RZ_COMMENTS_PHONE'],
			'ONECLICK_PERSON_TYPE_ID' => $arParams["ONECLICK_PERSON_TYPE_ID"],
			'ONECLICK_SHOW_FIELDS' => $arParams["ONECLICK_SHOW_FIELDS"],
			'ONECLICK_REQ_FIELDS' => $arParams["ONECLICK_REQ_FIELDS"],
			'ONECLICK_ALLOW_AUTO_REGISTER' => $arParams["ONECLICK_ALLOW_AUTO_REGISTER"],
			'ONECLICK_USE_CAPTCHA' => $arParams["ONECLICK_USE_CAPTCHA"],
			'ONECLICK_MESSAGE_OK' => $arParams["ONECLICK_MESSAGE_OK"],
			'ONECLICK_PAY_SYSTEM_ID' => $arParams["ONECLICK_PAY_SYSTEM_ID"],
			'ONECLICK_DELIVERY_ID' => $arParams["ONECLICK_DELIVERY_ID"],
			'ONECLICK_AS_EMAIL' => $arParams["ONECLICK_AS_EMAIL"],
			'ONECLICK_AS_NAME' => $arParams["ONECLICK_AS_NAME"],
			'ONECLICK_SEND_REGISTER_EMAIL' => $arParams["ONECLICK_SEND_REGISTER_EMAIL"],
			'ONECLICK_FIELD_PLACEHOLDER' => $arParams["ONECLICK_FIELD_PLACEHOLDER"],
			'ONECLICK_FIELD_QUANTITY' => $arParams["ONECLICK_FIELD_QUANTITY"],
			'MAIN_TITLE' => $arParams['MAIN_TITLE'],
			'USE_STORE' => $rz_options['show_stores_in_element'],
			'USE_STORE_PHONE' => $arParams['USE_STORE_PHONE'],
			'USE_STORE_SCHEDULE' => $arParams['USE_STORE_SCHEDULE'],
			'SHOW_EMPTY_STORE' => $arParams['SHOW_EMPTY_STORE'],
			'STORE_MANY_VAL' => $arParams['STORE_MANY_VAL'],
			'STORE_AVERAGE_VAL' => $arParams['STORE_AVERAGE_VAL'],
			'STORES' => $arParams['STORES'],
			'SHOW_CATCH_BUY' => $rz_options['show_catchbuy_in_element'] != 'N',
			'USE_ONE_CLICK' => $rz_options['show_one_click_in_element'],
		),
		$component
	); 

?><?
	if (0 < $ElementID) {?>
		<?
		global $arModals;
		if(!is_array($arModals)) {
			$arModals = array();
		}
		ob_start();
		?>
		<div class="modal fade" id="modal-buy" tabindex="-1" role="dialog" aria-labelledby="buy_modalLabel" aria-hidden="true">
			<div class="popup modal-dialog popup_buy">
				<span class="close flaticon-delete30" data-dismiss="modal"></span>
				<div class="popup_buy__head">
					<div class="popup_buy__head__img_wrapper">
						<img class="product_img" src="<?= Resize::GetResizedImg(0)?>" alt="none">
					</div>
					<div class="popup_buy__head_inner">
						<div id="buy_modalLabel"><?=GetMessage("RZ_VI_DOBAVILI_V_KORZINU")?></div>
						<div><a class="brand-primary-color product_name" href="<?=$APPLICATION->GetCurPage(false)?>">#ITEM_NAME#</a> - <span class="product-quant">#ITEM_QUANT#</span><?=GetMessage("RZ_SHT_")?></div>
						<div class="popup_buy__head__make_order">
							<a class="btn btn-primary"  href="<?=$arParams['BASKET_URL']?>"><?=GetMessage("RZ_OFORMIT__ZAKAZ")?></a>
							<?=GetMessage("RZ_ILI")?>
							<span class="continue" data-dismiss="modal"><?=GetMessage("RZ_PRODOLZHIT__POKUPKI")?></span>
						</div>
					</div>
				</div>
		<?
		$arRecomData = array();
		$recomCacheID = array('IBLOCK_ID' => $arParams['IBLOCK_ID']);
		$obCache = new CPHPCache();
		if ($obCache->InitCache(36000, serialize($recomCacheID), "/catalog/recommended")) {
			$arRecomData = $obCache->GetVars();
		} elseif ($obCache->StartDataCache()) {
			if (\Bitrix\Main\Loader::includeModule("catalog")) {
				$arSKU = CCatalogSKU::GetInfoByProductIBlock($arParams['IBLOCK_ID']);
				$arRecomData['OFFER_IBLOCK_ID'] = (!empty($arSKU) ? $arSKU['IBLOCK_ID'] : 0);
				$arRecomData['IBLOCK_LINK'] = '';
				$arRecomData['ALL_LINK'] = '';
				$rsProps = CIBlockProperty::GetList(
					array('SORT' => 'ASC', 'ID' => 'ASC'),
					array('IBLOCK_ID' => $arParams['IBLOCK_ID'], 'PROPERTY_TYPE' => 'E', 'ACTIVE' => 'Y')
				);
				$found = false;
				while ($arProp = $rsProps->Fetch()) {
					if ($found) {
						break;
					}
					if ($arProp['CODE'] == '') {
						$arProp['CODE'] = $arProp['ID'];
					}
					$arProp['LINK_IBLOCK_ID'] = intval($arProp['LINK_IBLOCK_ID']);
					if ($arProp['LINK_IBLOCK_ID'] != 0 && $arProp['LINK_IBLOCK_ID'] != $arParams['IBLOCK_ID']) {
						continue;
					}
					if ($arProp['LINK_IBLOCK_ID'] > 0) {
						if ($arRecomData['IBLOCK_LINK'] == '') {
							$arRecomData['IBLOCK_LINK'] = $arProp['CODE'];
							$found = true;
						}
					} else {
						if ($arRecomData['ALL_LINK'] == '') {
							$arRecomData['ALL_LINK'] = $arProp['CODE'];
						}
					}
				}
				if ($found) {
					if (defined("BX_COMP_MANAGED_CACHE")) {
						global $CACHE_MANAGER;
						$CACHE_MANAGER->StartTagCache("/catalog/recommended");
						$CACHE_MANAGER->RegisterTag("iblock_id_" . $arParams["IBLOCK_ID"]);
						$CACHE_MANAGER->EndTagCache();
					}
				}
			}
			$obCache->EndDataCache($arRecomData);
		}
		if (!empty($arRecomData) && ($arRecomData['IBLOCK_LINK'] != '' || $arRecomData['ALL_LINK'] != '')) {
			?><?
			// $APPLICATION->IncludeComponent(
			// 	"bitrix:catalog.recommended.products",
			// 	"main",
			// 	array(
			// 		"LINE_ELEMENT_COUNT" => $arParams["ALSO_BUY_ELEMENT_COUNT"],
			// 		"TEMPLATE_THEME" => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
			// 		"ID" => $ElementID,
			// 		"PROPERTY_LINK" => ($arRecomData['IBLOCK_LINK'] != '' ? $arRecomData['IBLOCK_LINK'] : $arRecomData['ALL_LINK']),
			// 		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
			// 		"CACHE_TIME" => $arParams["CACHE_TIME"],
			// 		"BASKET_URL" => $arParams["BASKET_URL"],
			// 		"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
			// 		"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
			// 		"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
			// 		"ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
			// 		"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
			// 		"PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
			// 		"PAGE_ELEMENT_COUNT" => $arParams["ALSO_BUY_ELEMENT_COUNT"],
			// 		"SHOW_OLD_PRICE" => $arParams['SHOW_OLD_PRICE'],
			// 		"SHOW_DISCOUNT_PERCENT" => $arParams['SHOW_DISCOUNT_PERCENT'],
			// 		"PRICE_CODE" => $arParams["PRICE_CODE"],
			// 		"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
			// 		"PRODUCT_SUBSCRIPTION" => 'N',
			// 		"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
			// 		"USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
			// 		"SHOW_NAME" => "Y",
			// 		"SHOW_IMAGE" => "Y",
			// 		"MESS_BTN_BUY" => $arParams['MESS_BTN_BUY'],
			// 		"MESS_BTN_DETAIL" => $arParams["MESS_BTN_DETAIL"],
			// 		"MESS_NOT_AVAILABLE" => $arParams['MESS_NOT_AVAILABLE'],
			// 		"MESS_BTN_SUBSCRIBE" => $arParams['MESS_BTN_SUBSCRIBE'],
			// 		"SHOW_PRODUCTS_" . $arParams["IBLOCK_ID"] => "Y",
			// 		"HIDE_NOT_AVAILABLE" => $arParams["HIDE_NOT_AVAILABLE"],
			// 		"OFFER_TREE_PROPS_" . $arRecomData['OFFER_IBLOCK_ID'] => $arParams["OFFER_TREE_PROPS"],
			// 		"PROPERTY_CODE_" . $arRecomData['OFFER_IBLOCK_ID'] => array(),
			// 		"CONVERT_CURRENCY" => $arParams['CONVERT_CURRENCY'],
			// 		'CURRENCY_ID' => $arParams['CURRENCY_ID'],
			// 		'RESIZER_PRODUCT' => $arParams['RESIZER_PRODUCT'],
			// 		'SHOW_CATCH_BUY' => $rz_options['show_catchbuy_in_element'] != 'N',
			// 		'USE_STORE' => $rz_options['show_stores_in_element'],
			// 		'USE_ONE_CLICK' => $rz_options['show_one_click_in_element'],
			// 		'ONECLICK_PERSON_TYPE_ID' => $arParams["ONECLICK_PERSON_TYPE_ID"],
			// 		'ONECLICK_SHOW_FIELDS' => $arParams["ONECLICK_SHOW_FIELDS"],
			// 		'ONECLICK_REQ_FIELDS' => $arParams["ONECLICK_REQ_FIELDS"],
			// 		'ONECLICK_ALLOW_AUTO_REGISTER' => $arParams["ONECLICK_ALLOW_AUTO_REGISTER"],
			// 		'ONECLICK_USE_CAPTCHA' => $arParams["ONECLICK_USE_CAPTCHA"],
			// 		'ONECLICK_MESSAGE_OK' => $arParams["ONECLICK_MESSAGE_OK"],
			// 		'ONECLICK_PAY_SYSTEM_ID' => $arParams["ONECLICK_PAY_SYSTEM_ID"],
			// 		'ONECLICK_DELIVERY_ID' => $arParams["ONECLICK_DELIVERY_ID"],
			// 		'ONECLICK_AS_EMAIL' => $arParams["ONECLICK_AS_EMAIL"],
			// 		'ONECLICK_AS_NAME' => $arParams["ONECLICK_AS_NAME"],
			// 		'ONECLICK_SEND_REGISTER_EMAIL' => $arParams["ONECLICK_SEND_REGISTER_EMAIL"],
			// 		'ONECLICK_FIELD_PLACEHOLDER' => $arParams["ONECLICK_FIELD_PLACEHOLDER"],
			// 		'ONECLICK_FIELD_QUANTITY' => $arParams["ONECLICK_FIELD_QUANTITY"],
			// 	),
			// 	$component,
			// 	array("HIDE_ICONS" => "Y")
			// );
			?><?
		}

		if ($arParams["USE_ALSO_BUY"] == "Y" && \Bitrix\Main\ModuleManager::isModuleInstalled("sale") && !empty($arRecomData)) {
			?><?
			// $APPLICATION->IncludeComponent("bitrix:sale.recommended.products", "main", array(
			// 	"ID" => $ElementID,
			// 	"TEMPLATE_THEME" => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
			// 	"MIN_BUYES" => $arParams["ALSO_BUY_MIN_BUYES"],
			// 	"ELEMENT_COUNT" => $arParams["ALSO_BUY_ELEMENT_COUNT"],
			// 	"LINE_ELEMENT_COUNT" => $arParams["ALSO_BUY_ELEMENT_COUNT"],
			// 	"DETAIL_URL" => $arParams["DETAIL_URL"],
			// 	"BASKET_URL" => $arParams["BASKET_URL"],
			// 	"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
			// 	"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
			// 	"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
			// 	"PAGE_ELEMENT_COUNT" => $arParams["ALSO_BUY_ELEMENT_COUNT"],
			// 	"CACHE_TYPE" => $arParams["CACHE_TYPE"],
			// 	"CACHE_TIME" => $arParams["CACHE_TIME"],
			// 	"PRICE_CODE" => $arParams["PRICE_CODE"],
			// 	"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
			// 	"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
			// 	"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
			// 	'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
			// 	'CURRENCY_ID' => $arParams['CURRENCY_ID'],
			// 	'MESS_BTN_BUY' => $arParams['MESS_BTN_BUY'],
			// 	'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
			// 	"SHOW_PRODUCTS_" . $arParams["IBLOCK_ID"] => "Y",
			// 	"PROPERTY_CODE_" . $arRecomData['OFFER_IBLOCK_ID'] => array(),
			// 	"OFFER_TREE_PROPS_" . $arRecomData['OFFER_IBLOCK_ID'] => $arParams["OFFER_TREE_PROPS"],
			// 	'SHOW_CATCH_BUY' => $rz_options['show_catchbuy_in_element'] != 'N',
			// 	'USE_STORE' => $rz_options['show_stores_in_element'],
			// 	'USE_ONE_CLICK' => $rz_options['show_one_click_in_element'],
			// 	'ONECLICK_PERSON_TYPE_ID' => $arParams["ONECLICK_PERSON_TYPE_ID"],
			// 	'ONECLICK_SHOW_FIELDS' => $arParams["ONECLICK_SHOW_FIELDS"],
			// 	'ONECLICK_REQ_FIELDS' => $arParams["ONECLICK_REQ_FIELDS"],
			// 	'ONECLICK_ALLOW_AUTO_REGISTER' => $arParams["ONECLICK_ALLOW_AUTO_REGISTER"],
			// 	'ONECLICK_USE_CAPTCHA' => $arParams["ONECLICK_USE_CAPTCHA"],
			// 	'ONECLICK_MESSAGE_OK' => $arParams["ONECLICK_MESSAGE_OK"],
			// 	'ONECLICK_PAY_SYSTEM_ID' => $arParams["ONECLICK_PAY_SYSTEM_ID"],
			// 	'ONECLICK_DELIVERY_ID' => $arParams["ONECLICK_DELIVERY_ID"],
			// 	'ONECLICK_AS_EMAIL' => $arParams["ONECLICK_AS_EMAIL"],
			// 	'ONECLICK_AS_NAME' => $arParams["ONECLICK_AS_NAME"],
			// 	'ONECLICK_SEND_REGISTER_EMAIL' => $arParams["ONECLICK_SEND_REGISTER_EMAIL"],
			// 	'ONECLICK_FIELD_PLACEHOLDER' => $arParams["ONECLICK_FIELD_PLACEHOLDER"],
			// 	'ONECLICK_FIELD_QUANTITY' => $arParams["ONECLICK_FIELD_QUANTITY"],
			// ),
			// 	$component
			// );
			?><?
		}
		?>
			</div>
		</div>

		<?
		$arParams["USE_STORE"] = $rz_options['show_stores_in_element'];
		$arModals[] = ob_get_clean();
		if (false && $arParams["USE_STORE"] == "Y" && \Bitrix\Main\ModuleManager::isModuleInstalled("catalog")) {
			?><?

			// $APPLICATION->IncludeComponent("bitrix:catalog.store.amount", ".default", array(
			// 	"PER_PAGE" => "10",
			// 	"USE_STORE_PHONE" => $arParams["USE_STORE_PHONE"],
			// 	"SCHEDULE" => $arParams["USE_STORE_SCHEDULE"],
			// 	"USE_MIN_AMOUNT" => $arParams["USE_MIN_AMOUNT"],
			// 	"MIN_AMOUNT" => $arParams["MIN_AMOUNT"],
			// 	"ELEMENT_ID" => $ElementID,
			// 	"STORE_PATH" => $arParams["STORE_PATH"],
			// 	"MAIN_TITLE" => $arParams["MAIN_TITLE"],
			// ),
			// 	$component
			// );

			?><?
		}
	}?>
<?endif //$b404?>