<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use \Yenisite\Shinmarket\CacheProvider;
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
use \Yenisite\Core\Catalog;
define("IS_CATALOG_LIST",true);
define("IS_CATALOG",true);
$this->setFrameMode(true);
global $rz_options;
if(!empty($rz_options['GEOIP']['PRICES'])) {
	$arParams["PRICE_CODE"] = $rz_options['GEOIP']['PRICES'];
}
if(!empty($rz_options['GEOIP']['STORES'])) {
	$arParams['STORES'] = $rz_options['GEOIP']['STORES'];
}
$arParams['USE_FILTER'] = (isset($arParams['USE_FILTER']) && $arParams['USE_FILTER'] == 'Y' ? 'Y' : 'N');
if ($arParams['USE_FILTER'] == 'Y') {

	$arFilter = array(
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"ACTIVE" => "Y",
		"GLOBAL_ACTIVE" => "Y",
	);
	if (0 < intval($arResult["VARIABLES"]["SECTION_ID"])) {
		$arFilter["ID"] = $arResult["VARIABLES"]["SECTION_ID"];
	} elseif ('' != $arResult["VARIABLES"]["SECTION_CODE"]) {
		$arFilter["=CODE"] = $arResult["VARIABLES"]["SECTION_CODE"];
	}

	$obCache = new CPHPCache();
	if ($obCache->InitCache(36000, serialize($arFilter), "/iblock/catalog")) {
		$arCurSection = $obCache->GetVars();
	} elseif ($obCache->StartDataCache()) {
		$arCurSection = array();
		if (\Bitrix\Main\Loader::includeModule("iblock")) {
			$dbRes = CIBlockSection::GetList(array(), $arFilter, false, array("ID",'PICTURE','NAME'));

			if (defined("BX_COMP_MANAGED_CACHE")) {
				global $CACHE_MANAGER;
				$CACHE_MANAGER->StartTagCache("/iblock/catalog");

				if ($arCurSection = $dbRes->Fetch()) {
					$CACHE_MANAGER->RegisterTag("iblock_id_" . $arParams["IBLOCK_ID"]);
				}
				$CACHE_MANAGER->EndTagCache();
			} else {
				if (!$arCurSection = $dbRes->Fetch())
					$arCurSection = array();
			}
		}
		$obCache->EndDataCache($arCurSection);
	}
	if (!isset($arCurSection)) {
		$arCurSection = array();
	}
	?>
	<?
	global $filterContent;
	ob_start();
	?>
	<?
	$APPLICATION->IncludeComponent(
		"bitrix:catalog.smart.filter",
		"visual_vertical_up",
		Array(
			"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"SECTION_ID" => $arCurSection['ID'],
			"FILTER_NAME" => $arParams["FILTER_NAME"],
			"PRICE_CODE" => $arParams["PRICE_CODE"],
			"CACHE_TYPE" => $arParams["CACHE_TYPE"],
			"CACHE_TIME" => $arParams["CACHE_TIME"],
			"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
			"SAVE_IN_SESSION" => "N",
			"XML_EXPORT" => "Y",
			"SECTION_TITLE" => "NAME",
			"SECTION_DESCRIPTION" => "DESCRIPTION",
			'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
			"TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"],
			"SHOW_MENU" => $rz_options['switch_main_menu'],
			"SHOW_FILTER" => $rz_options['switch_filter'],

		),
		$component,
		array('HIDE_ICONS' => 'Y')
	);
	?>
	<?$filterContent = ob_get_clean();?>
<? } ?>
<?$APPLICATION->IncludeComponent(
	"bitrix:breadcrumb",
	"default",
	Array(),
	$component
);?>
<?
$arParams['SHOW_DESCRIPTION'] = $rz_options['section_description'] != 'N';
$arParams['SHOW_SUBSECTIONS'] = $rz_options['show_subsections'] != 'N';

$defDescription = $arParams['SHOW_DESCRIPTION'];
$defSubsection = $arParams['SHOW_SUBSECTIONS'];?>

<?if (empty($arParams['HIDE_SEBSECTIONS_DESC']) && $arParams['HIDE_SEBSECTIONS_DESC'] != 'Y'):?>
	<?if(!empty($arParams['SUBSECTION_POSITION']) && ($arParams['SUBSECTION_POSITION'] == 'TOP' || $arParams['SUBSECTION_POSITION'] == 'ALL')):?>
		<?$arParams['SHOW_SUBSECTIONS'] = false;?>
		<?include "section_list.php"?>
	<?endif?>

	<?$arParams['SHOW_DESCRIPTION'] = false;
	$arParams['SHOW_SUBSECTIONS'] = $defSubsection;?>
	<?include "section_list.php"?>
	<?$arParams['SHOW_DESCRIPTION'] = $defDescription?>
<?endif?>
<?
$intSectionID = 0;
$arSort = Catalog::getSort($arParams,NULL);
$count = Catalog::getCount();
$viewMode = Catalog::getViewMode();
\CHTMLPagesCache::setUserPrivateKey(CacheProvider::getCachePrefix(), 0);
?>

<?
$intSectionID = $APPLICATION->IncludeComponent(
	"bitrix:catalog.section",
	"main-list-new",
	array(
		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"ELEMENT_SORT_FIELD" => $arSort['BY'],
		"ELEMENT_SORT_ORDER" => $arSort['ORDER'],
		"ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
		"ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
		"PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
		"META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
		"META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
		"BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
		"INCLUDE_SUBSECTIONS" => $arParams["INCLUDE_SUBSECTIONS"],
		"BASKET_URL" => $arParams["BASKET_URL"],
		"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
		"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
		"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
		"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
		"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
		"FILTER_NAME" => $arParams["FILTER_NAME"],
		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
		"CACHE_TIME" => $arParams["CACHE_TIME"],
		"CACHE_FILTER" => $arParams["CACHE_FILTER"],
		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
		"SET_TITLE" => $arParams["SET_TITLE"],
		"SET_STATUS_404" => $arParams["SET_STATUS_404"],
		"DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
		"PAGE_ELEMENT_COUNT" => $count,
		"LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
		"PRICE_CODE" => $arParams["PRICE_CODE"],
		"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
		"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],

		"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
		"USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
		"ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
		"PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
		"PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],

		"DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
		"DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
		"PAGER_TITLE" => $arParams["PAGER_TITLE"],
		"PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
		"PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
		"PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
		"PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
		"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],

		"OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
		"OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
		"OFFERS_PROPERTY_CODE" => $arParams["LIST_OFFERS_PROPERTY_CODE"],
		"OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
		"OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
		"OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
		"OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
		"OFFERS_LIMIT" => $arParams["LIST_OFFERS_LIMIT"],
		'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
		'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS'],
		"OFFERS_PROPERTY_CODE" => array_merge($arParams["LIST_OFFERS_PROPERTY_CODE"],$arParams['OFFER_TREE_PROPS']),

		"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
		"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
		"SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
		"DETAIL_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["element"],
		'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
		'CURRENCY_ID' => $arParams['CURRENCY_ID'],
		'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],

		'LABEL_PROP' => $arParams['LABEL_PROP'],
		'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
		'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],

		'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
		'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
		'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
		'MESS_BTN_BUY' => $arParams['MESS_BTN_BUY'],
		'MESS_BTN_ADD_TO_BASKET' => $arParams['MESS_BTN_ADD_TO_BASKET'],
		'MESS_BTN_SUBSCRIBE' => $arParams['MESS_BTN_SUBSCRIBE'],
		'MESS_BTN_DETAIL' => $arParams['MESS_BTN_DETAIL'],
		'MESS_NOT_AVAILABLE' => $arParams['MESS_NOT_AVAILABLE'],

		'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
		"ADD_SECTIONS_CHAIN" => $arParams['ADD_SECTIONS_CHAIN'],
		'VIEW_MODE' => $viewMode,
		'RESIZER_PRODUCT' => $arParams['RESIZER_PRODUCT'],
		'RESIZER_PRODUCT_BIG' => $arParams['RESIZER_PRODUCT_BIG'],
		'RESIZER_PRODUCT_THUMB' => $arParams['RESIZER_PRODUCT_THUMB'],
		'USE_COMPARE' => $arParams['USE_COMPARE'],
		'ONECLICK_PERSON_TYPE_ID' => $arParams["ONECLICK_PERSON_TYPE_ID"],
		'ONECLICK_SHOW_FIELDS' => $arParams["ONECLICK_SHOW_FIELDS"],
		'ONECLICK_REQ_FIELDS' => $arParams["ONECLICK_REQ_FIELDS"],
		'ONECLICK_ALLOW_AUTO_REGISTER' => $arParams["ONECLICK_ALLOW_AUTO_REGISTER"],
		'ONECLICK_USE_CAPTCHA' => $arParams["ONECLICK_USE_CAPTCHA"],
		'ONECLICK_MESSAGE_OK' => $arParams["ONECLICK_MESSAGE_OK"],
		'ONECLICK_PAY_SYSTEM_ID' => $arParams["ONECLICK_PAY_SYSTEM_ID"],
		'ONECLICK_DELIVERY_ID' => $arParams["ONECLICK_DELIVERY_ID"],
		'ONECLICK_AS_EMAIL' => $arParams["ONECLICK_AS_EMAIL"],
		'ONECLICK_AS_NAME' => $arParams["ONECLICK_AS_NAME"],
		'ONECLICK_SEND_REGISTER_EMAIL' => $arParams["ONECLICK_SEND_REGISTER_EMAIL"],
		'ONECLICK_FIELD_PLACEHOLDER' => $arParams["ONECLICK_FIELD_PLACEHOLDER"],
		'ONECLICK_FIELD_QUANTITY' => $arParams["ONECLICK_FIELD_QUANTITY"],
		'USE_STORE_PHONE' => $arParams['USE_STORE_PHONE'],
		'USE_STORE_SCHEDULE' => $arParams['USE_STORE_SCHEDULE'],
		'USE_STORE' => $rz_options['show_stores_in_section'],
		'SHOW_EMPTY_STORE' => $arParams['SHOW_EMPTY_STORE'],
		'STORE_MANY_VAL' => $arParams['STORE_MANY_VAL'],
		'STORE_AVERAGE_VAL' => $arParams['STORE_AVERAGE_VAL'],
		'STORES' => $arParams['STORES'],

		'SECTION_COUNT_ELEMENTS' => $arParams['SECTION_COUNT_ELEMENTS'],
		'VIEW_MODE_SECTION_LIST' => $arParams['SECTIONS_VIEW_MODE'],
		'SUBSECTION_POSITION' => $arParams['SUBSECTION_POSITION'],
		'SECTION_TOP_DEPTH' => $arParams['SECTION_TOP_DEPTH'],
		'RESIZER_SECTION_IMG' => $arParams['RESIZER_SECTION_IMG'],
		'RESIZER_SECTIONS_IMG' => $arParams['RESIZER_SECTIONS_IMG'],
		'FULL_DESCTRIPTION' => $arParams['FULL_DESCTRIPTION'] == 'Y',

		'SHOW_DESCRIPTION' => $rz_options['section_description'] != 'N',
		'SHOW_SUBSECTIONS' => $rz_options['show_subsections'] != 'N',
		'SHOW_CATCH_BUY' => $rz_options['show_catchbuy_in_section'] != 'N',
		'USE_ONE_CLICK' => $rz_options['show_one_click_in_section'],
	),
	$component
);

?>
<?if (empty($arParams['HIDE_SEBSECTIONS_DESC']) && $arParams['HIDE_SEBSECTIONS_DESC'] != 'Y'):?>
	<?if(!empty($arParams['SUBSECTION_POSITION']) && ($arParams['SUBSECTION_POSITION'] == 'BOTTOM' || $arParams['SUBSECTION_POSITION'] == 'ALL')):?>
		<?$arParams['SHOW_SUBSECTIONS'] = false;?>
		<?include "section_list.php"?>
	<?endif?>
<?endif?>
<?$APPLICATION->IncludeComponent("bitrix:main.include", "adv-main", Array(
	"PATH" => SITE_DIR."include_areas/universal/main-adv-double.php",
	"AREA_FILE_SHOW" => "file",
	"EDIT_TEMPLATE" => "",
),
	false
);?>
