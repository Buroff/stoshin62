$(function ($) {
	var $catalog_replyForm = $('#catalog_replyForm');
	var updateReplyForm = function ($form, data) {
		if (typeof(data) == 'undefined') {
			data = [];
		}
		data.push({name: "arParams", value: $form.data('arparams')});
		data.push({name: "template", value: $form.data('template').toString()});
		$catalog_replyForm.setAjaxLoading();
		$.ajax({
			type: "POST",
			url: rz.AJAX_DIR + "catalog_replyForm.php",
			data: data,
			success: function (msg) {
				$catalog_replyForm.html(msg);
                $catalog_replyForm.refreshForm();
				$catalog_replyForm.stopAjaxLoading();
			}
		})
	};
	$catalog_replyForm.on('submit', 'form', function (e) {
		e.preventDefault();
		var $this = $(this),
			data = $this.serializeArray();
		updateReplyForm($this, data);
	});
	if ('refreshComments' in rz && rz.refreshComments) {
		updateReplyForm($catalog_replyForm.find('form'));
	}
});