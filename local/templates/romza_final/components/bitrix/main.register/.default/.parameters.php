<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$arTemplateParameters = array(
	"USER_PROPERTY_NAME"=>array(
		"NAME" => GetMessage("USER_PROPERTY_NAME"),
		"TYPE" => "STRING",
		"DEFAULT" => "",
	),
	"RULEZ_URL" => array(
		"NAME" => GetMessage("RZ_RULEZ_URL"),
		"TYPE" => "STRING",
		"DEFAULT" => "/about/rules.php"
	)
);