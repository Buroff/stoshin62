<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use \Yenisite\Core\Tools;
if (!isset($arParams['RETURN_NULL']) || (isset($arParams['RETURN_NULL']) && !$arParams['RETURN_NULL'])) {
	Tools::addComponentDeferredJS($templateFile);
}