<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2014 Bitrix
 */
use \Yenisite\Core\Tools;
/**
 * Bitrix vars
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @param array $arParams
 * @param array $arResult
 * @param CBitrixComponentTemplate $this
 */
\Bitrix\Main\Localization\Loc::loadMessages($_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH . '/lang/'.LANGUAGE_ID.'/header.php');
$pathToRules = COption::GetOptionString(CRZShinmarketSettings::getModuleId(),'path_tu_rules_privacy',SITE_DIR.'personal/rules/personal_data.php');
$rand = $this->randString();

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
if (!isset($arParams['RETURN_NULL']) || (isset($arParams['RETURN_NULL']) && !$arParams['RETURN_NULL'])) {
	include_once $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/tools/include_module.php';
	$prefix = "popup";
	if(!isset($_REQUEST['pass_gen'])) {
		$_REQUEST['pass_gen'] = 1;
	};
	$isAjax = Tools::isAjax();
	?>
	<?if (!$isAjax && method_exists($this, 'setFrameMode')) $this->setFrameMode(true); ?>
	<?if($isAjax && $USER->IsAuthorized()):?>
		<div class="message message-success">
			<b><?=GetMessage("MAIN_REGISTER_AUTH")?></b>
		</div>
		<script type="text/javascript">
			window.location.reload();
		</script>
		<?die();?>
	<?endif;?>

	<?if(!$isAjax):?>
	<a href="#" class="btn btn-register" data-toggle="modal" data-target="#modal-register">
		<span class="flaticon-list41"></span>
		<span class="btn-inner"><?=GetMessage('AUTH_REGISTER')?></span>
	</a>
	<?endif;?>
	<?if(!$isAjax):?>
		<?$this->SetViewTarget('modal_register')?>
	<div class="modal fade" id="modal-register" tabindex="-1" role="dialog" aria-labelledby="register_modalLabel" aria-hidden="true">
		<div class="popup modal-dialog popup_register" id="register_modalForm" data-arparams='<?=Tools::GetEncodedArParams($arParams)?>' data-template='<?=$templateName?>' >
	<?endif?>
			<span class="close flaticon-delete30" data-dismiss="modal"></span>

			<h2 id="register_modalLabel">
				<span class="brand-primary-color flaticon-list41"></span>
				<?=GetMessage('AUTH_REGISTER')?>:
			</h2>

			<?	if (count($arResult["ERRORS"]) > 0) :?>
			<div class="message message-error">
				<?foreach ($arResult["ERRORS"] as $key => $error)
					if (intval($key) == 0 && $key !== 0)
						$arResult["ERRORS"][$key] = str_replace("#FIELD_NAME#", "&quot;" . GetMessage("REGISTER_FIELD_" . $key) . "&quot;", $error);
					echo implode("<br />", $arResult["ERRORS"]);?>
			</div>
			<? endif ?>
			<form class="form-horizontal form-register modal-form" role="form" name="regform" enctype="multipart/form-data">
                <input type="hidden" name="privacy_policy" value="N"/>
				<? if ($arResult["BACKURL"] <> ''): ?>
					<input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>"/>
				<? endif; ?>
				<div class="form-group form-group-radio generate-pass">
					<div class="forms-wrapper radio_wrapper">
						<input type="radio" name="pass_gen" id="popup_register_generate_pass_<?= $prefix ?>" value="1"<?if($_REQUEST['pass_gen'] > 0):?> checked<?endif?> />
						<label for="popup_register_generate_pass_<?= $prefix ?>"><?=GetMessage('RZ_GEN_LOGIN_PASSWORD')?></label>
					</div>
				</div>
				<div class="form-group form-group-radio generate-pass">
					<div class="forms-wrapper radio_wrapper">
						<input type="radio" name="pass_gen" id="popup_register_set_pass_<?= $prefix ?>" value="0" <?if($_REQUEST['pass_gen'] == 0):?> checked<?endif?> />
						<label for="popup_register_set_pass_<?= $prefix ?>"><?=GetMessage('RZ_TYPE_LOGIN_PASSWORD')?></label>
					</div>
				</div>
				<? foreach ($arResult["SHOW_FIELDS"] as $FIELD): ?>
					<div class="form-group<? if ($arResult["REQUIRED_FIELDS_FLAGS"][$FIELD] == "Y"): ?> required<? endif ?>">
							<label for="popup_register_<?=$FIELD,"_",$prefix?>" class="control-label"><?= GetMessage("REGISTER_FIELD_" . $FIELD) ?>:</label>
							<div class="control-wrapper">
								<?switch ($FIELD) {
									case "PASSWORD":?>
										<input size="30" type="password" name="REGISTER[<?= $FIELD ?>]" id="popup_register_<?=$FIELD,"_",$prefix?>"
												 value="<?= $arResult["VALUES"][$FIELD] ?>" autocomplete="off" class="form-control password-field" />
										<?break;

									case "CONFIRM_PASSWORD":?>
										<input size="30" type="password" name="REGISTER[<?= $FIELD ?>]" id="popup_register_<?=$FIELD,"_",$prefix?>"
												 value="<?= $arResult["VALUES"][$FIELD] ?>" autocomplete="off" class="form-control password-field" /><?
										break;

									case "PERSONAL_GENDER":?>
										<select name="REGISTER[<?= $FIELD ?>]" id="popup_register_<?=$FIELD,"_",$prefix?>" class="form-control" >
											<option value=""><?= GetMessage("USER_DONT_KNOW") ?></option>
											<option value="M"<?= $arResult["VALUES"][$FIELD] == "M" ? " selected=\"selected\"" : "" ?>><?= GetMessage("USER_MALE") ?></option>
											<option value="F"<?= $arResult["VALUES"][$FIELD] == "F" ? " selected=\"selected\"" : "" ?>><?= GetMessage("USER_FEMALE") ?></option>
										</select>
										<?break;

									case "PERSONAL_COUNTRY":
									case "WORK_COUNTRY":?>
										<select name="REGISTER[<?= $FIELD ?>]" id="popup_register_<?=$FIELD,"_",$prefix?>" class="form-control">
										<?foreach ($arResult["COUNTRIES"]["reference_id"] as $key => $value) {?>
											<option value="<?= $value ?>"<? if ($value == $arResult["VALUES"][$FIELD]): ?> selected="selected"<? endif ?>>
												<?= $arResult["COUNTRIES"]["reference"][$key] ?>
											</option>
										<?}?>
										</select>
										<?break;

									case "PERSONAL_PHOTO":
									case "WORK_LOGO":?>
										<input size="30" type="file" name="REGISTER_FILES_<?= $FIELD ?>" id="popup_register_<?=$FIELD,"_",$prefix?>" class="form-control"/>
										<?break;

									case "PERSONAL_NOTES":
									case "WORK_NOTES":?>
										<textarea cols="30" rows="5" id="popup_register_<?=$FIELD,"_",$prefix?>" class="form-control" name="REGISTER[<?= $FIELD ?>]">
											<?= $arResult["VALUES"][$FIELD] ?>
										</textarea>
										<?break;

									default:
										if ($FIELD == "PERSONAL_BIRTHDAY"):?>
											<small><?= $arResult["DATE_FORMAT"] ?></small><br/>
										<?endif;?>
											<input size="30" type="text" name="REGISTER[<?= $FIELD ?>]" id="popup_register_<?=$FIELD,"_",$prefix?>" class="form-control"
												 value="<?= $arResult["VALUES"][$FIELD] ?>" />
										<?if ($FIELD == "PERSONAL_BIRTHDAY"):?>
											<?$APPLICATION->IncludeComponent(
												'bitrix:main.calendar',
												'',
												array(
													'SHOW_INPUT' => 'N',
													'FORM_NAME' => 'regform',
													'INPUT_NAME' => 'REGISTER[PERSONAL_BIRTHDAY]',
													'SHOW_TIME' => 'N'
												),
												null,
												array("HIDE_ICONS" => "Y")
											);?>
										<?endif;?>
										<?
								}?>
							</div>
					</div>
				<? endforeach; ?>
				<? // ********************* User properties ***************************************************?>
				<? if ($arResult["USER_PROPERTIES"]["SHOW"] == "Y"): ?>
					<div class="form-group">
						<?= strlen(trim($arParams["USER_PROPERTY_NAME"])) > 0 ? $arParams["USER_PROPERTY_NAME"] : GetMessage("USER_TYPE_EDIT_TAB") ?>
					</div>
					<? foreach ($arResult["USER_PROPERTIES"]["DATA"] as $FIELD_NAME => $arUserField): ?>
						<div class="form-group<? if ($arUserField["MANDATORY"] == "Y"): ?> required<? endif ?>">
							<label for="popup_register_<?=$FIELD,"_",$prefix?>">
								<?= $arUserField["EDIT_FORM_LABEL"] ?>:
							</label>
							<div class="control-wrapper">
								<?$APPLICATION->IncludeComponent(
									"bitrix:system.field.edit",
									$arUserField["USER_TYPE"]["USER_TYPE_ID"],
									array("bVarsFromForm" => $arResult["bVarsFromForm"], "arUserField" => $arUserField, "form_name" => "regform"), null, array("HIDE_ICONS" => "Y"));?>
							</div>
						</div>
					<? endforeach; ?>
				<? endif; ?>
				<? // ******************** /User properties ***************************************************?>
				<?if ($arResult["USE_CAPTCHA"] == "Y") :?>
					<? if (!$isAjax && method_exists($this, 'createFrame')) $frame = $this->createFrame()->begin(''); ?>
					<div class="form-group form-group-captcha">
						<label for="popup_register_captcha_<?= $prefix ?>" class="control-label required"><?= GetMessage("REGISTER_CAPTCHA_PROMT") ?>:</label>
						<div class="captcha_wrapper">
							<input type="hidden" name="captcha_sid" value="<?= $arResult["CAPTCHA_CODE"] ?>"/>
							<img src="<?= SITE_TEMPLATE_PATH ?>/tools/captcha.php?captcha_sid=<?= $arResult["CAPTCHA_CODE"] ?>"  alt="CAPTCHA"/>
						</div>
						<div class="control-wrapper">
							<input required id="popup_register_captcha_<?= $prefix ?>" class="form-control " type="text" name="captcha_word" value=""/>
						</div>
					</div>
					<? if (!$isAjax && method_exists($this, 'createFrame')) $frame->end(); ?>
				<? endif /* !CAPTCHA */?>
				<div class="form-group required_note"><span class="star">*</span><?= GetMessage("AUTH_REQ") ?></div>
				<? if (isset($arParams["RULEZ_URL"]) && strlen($arParams["RULEZ_URL"]) > 0): ?>
					<div class="form-group required">
						<div class="forms-wrapper">
							<input required type="checkbox" name="accept_rulez"
								   id="popup_register_accept_<?= $prefix ?>"<? if (isset($_REQUEST['accept_rulez']) && $_REQUEST['accept_rulez']): ?> checked<? endif ?> >
							<label for="popup_register_accept_<?= $prefix ?>">
								<?= GetMessage('RZ_ACCEPT_RULEZ') ?>
							</label>&nbsp;<?=GetMessage('RZ_ACCEPT_RULEZ_URL',array("#URL#" => Tools::GetConstantUrl($arParams["RULEZ_URL"])))?>
						</div>
					</div>
				<? endif ?>
                <div class="form-group agreement-policy">
                    <div class="forms-wrapper">
                        <input id="privacy_policy_<?=$rand?>" required type="checkbox" name="privacy_policy" value="Y" <?=$_REQUEST['privacy_policy'] == 'Y' ? ' checked' : ''?>>
                        <label for="privacy_policy_<?=$rand?>">
                            <?= GetMessage('RZ_ACCEPT') ?> <?=GetMessage('RZ_CONCLUSION',array("#URL#" => Tools::GetConstantUrl($pathToRules)))?>
                        </label>
                    </div>
                </div>
				<div class="form-group submit">
					<button type="submit" class="btn btn-primary btn-register-submit"><?=GetMessage('AUTH_REGISTER')?></button>
				</div>
			</form>
	<?if(!$isAjax):?>
		</div>
	</div>
		<?$this->EndViewTarget()?>
	<?endif;?>
<?}?>