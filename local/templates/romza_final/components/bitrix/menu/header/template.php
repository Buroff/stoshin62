<? use Yenisite\Core\Tools;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
include_once $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/tools/include_module.php';
if (method_exists($this, 'setFrameMode')) $this->setFrameMode(true);
if (!empty($arResult)): ?>
	<ul class="nav navbar-nav">
		<? foreach ($arResult as $arItem):
			if ($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) continue; ?>
			<? if ($arItem["SELECTED"]): ?>
				<li><a href="<?= Tools::GetConstantUrl($arItem["LINK"]) ?>" class="selected"><span class="inner"><?= $arItem["TEXT"] ?></span></a></li>
			<? else: ?>
				<li><a href="<?= Tools::GetConstantUrl($arItem["LINK"]) ?>"><span class="inner"><?= $arItem["TEXT"] ?></span></a></li>
			<?endif ?>
		<? endforeach ?>
	</ul>
<? endif ?>