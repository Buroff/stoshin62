<div class="sub-menu-gallery sly-frame" data-sly-from="0">
	<div class="slidee">
		<?foreach($hits as $hit):?>
				<div class="preview-item">
			<div class="preview-item__image-wrap">
				<a href="<?= $hit["DETAIL_PAGE_URL"] ?>" class="preview-item__image-link">
					<img src='<?= $hit["PHOTO"]; ?>' alt='<?= $hit["NAME"] ?>' class="preview-item__image" />
				</a>
			</div><!-- preview-item__image-wrap -->
				<a class="item-name" href="<?= $hit["DETAIL_PAGE_URL"] ?>"><span class="classic-link"><?= $hit["NAME"] ?></span></a>
			<div class="preview-item__caption">
				<div class="caption__item-info">
				<?$APPLICATION->IncludeComponent(
					"bitrix:iblock.vote",
					"stars",
					array(
						"IBLOCK_TYPE" => $hit['IBLOCK_TYPE'],
						"IBLOCK_ID" => $hit['IBLOCK_ID'],
						"ELEMENT_ID" => $hit['ID'],
						"ELEMENT_CODE" => "",
						"MAX_VOTE" => "5",
						"VOTE_NAMES" => array("1", "2", "3", "4", "5"),
						"SET_STATUS_404" => "N",
						"DISPLAY_AS_RATING" => $arParams['VOTE_DISPLAY_AS_RATING'],
						"CACHE_TYPE" => $arParams['CACHE_TYPE'],
						"CACHE_TIME" => $arParams['CACHE_TIME']
					),
					$component,
					array("HIDE_ICONS" => "Y")
				);?>
					<?
					$hit['PRINT_DISCOUNT_VALUE'] = $hit['PRICE'][$arParams['PRICE_CODE']]['PRINT_DISCOUNT_VALUE'];
					$hit['PRINT_VALUE'] = $hit['PRICE'][$arParams['PRICE_CODE']]['PRINT_VALUE'];
					?>
					<?if ($hit['PRINT_DISCOUNT_VALUE'] != $hit['PRINT_VALUE']):?>
					<div class="old-price"><del><?= $hit['PRINT_VALUE']; ?></del></div>
					<?endif?>
					<div class="price">
						<?if(isset($hit['PRICE']['HAS_OFFER']) && $hit['PRICE']['HAS_OFFER'] == "Y"):?><?=GetMessage('RZ_OFFERS_SUF')?>&nbsp;<?endif;?>
						<?= $hit["PRINT_DISCOUNT_VALUE"] ?>
					</div>
				</div>
			</div>
		</div>
	<? endforeach; ?>
	</div>
</div>