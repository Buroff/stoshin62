<? use Yenisite\Core\Resize;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
include_once $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/tools/include_module.php';

if (method_exists($this, 'setFrameMode')) $this->setFrameMode(true); ?>
<div class="js-slider_goods_list slider_goods_list goods_list_block brands">
	<div class="goods_list__head">
		<h2><?=GetMessage("RZ_BRENDI")?></h2>
		<div class="btn-group btn-group-arrows">
			<span class="btn btn-default btn-arrow flaticon-arrow133"></span>
			<span class="btn btn-default btn-arrow flaticon-right20"></span>
		</div>
	</div>
	<div class="main_content__brands_list">
		<?foreach($arResult['ITEMS'] as $arItem):?>
		<div class="main_content__brands_item">
			<a class="main_content__brands_item__inner" href="<?=$arItem['URL']?>">
				<img src="<?= Resize::GetResizedImg($arItem,
					array('WIDTH' => 150, 'HEIGHT' => 150 , 'SET_ID' => intval($arParams['RESIZER_PRODUCT'])))?>"
					 alt="<?=$arItem['NAME']?>" title="<?=$arItem['NAME']?>">
			</a>
		</div>
		<?endforeach?>
	</div>
</div>