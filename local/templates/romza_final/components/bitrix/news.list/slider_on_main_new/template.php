<? use Yenisite\Core\Tools;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?if (method_exists($this, 'setFrameMode')) $this->setFrameMode(true); ?>
<? if (count($arResult['ITEMS']) > 0): ?>

	<div class="banners_rotator_block">
		<div class="js-banners-rotator slick-slider banners_rotator">
			<? foreach ($arResult["ITEMS"] as $arItem): ?>

				<?
				$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
				$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
				?>
				<?
				$imgAlt = (isset($arItem['ITEMS']['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_ALT'])) ? $arItem['ITEMS']['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_ALT'] : $arItem['NAME'];
				$imgTitle = (isset($arItem['ITEMS']['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE'])) ? $arItem['ITEMS']['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE'] : $arItem['NAME'];
				?>
				<div class="item" id="<?= $this->GetEditAreaId($arItem['ID']); ?>" data-name="<?=$arItem['NAME']?>" data-text="<?=htmlspecialcharsEx($arItem['PREVIEW_TEXT'])?>">
					<img src="<?= $arItem['PREVIEW_PICTURE']['SRC'] ?>" alt="<?= $imgAlt ?>">
				</div>
			<? endforeach; ?>
			<div class="container">
				<div class="banners_rotator__inner">
					<div class="banners_rotator__inner__head">
					
					<a href="<?=$arResult["ITEMS"][0]["PROPERTIES"]["HREF"]["VALUE"]?>">
						<span class="big"><?=$arResult["ITEMS"][0]['NAME']?></span>
						<span class="small"><?=$arResult["ITEMS"][0]['PREVIEW_TEXT']?></span>
						<!-- <span class="shop_name"><?= SITE_NAME ?></span> -->
					</a>
						
					</div>
					<div class="banners_rotator__navigate">
						<span class="item btn btn-default btn-arrow flaticon-arrow133 btn-prev"></span><span
							class="item btn btn-default btn-arrow flaticon-right20 btn-next"></span>
					</div>
				</div>
			</div>
		</div>
		<div class="container__abs">
			<div class="container">
				<div class="page_vertical__nav">
					<?if($arParams['SHOW_MENU'] == 'vertical' && !defined('IS_CATALOG')):?>
						<? Tools::IncludeArea('header', 'menu_horizon', array(), true) ?>
					<?endif?>
				</div>
			</div>
		</div>
	</div>
<? endif; ?>