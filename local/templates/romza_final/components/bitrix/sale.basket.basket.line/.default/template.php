<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use \Yenisite\Core\Tools;
if (!$isAjax && method_exists($this, 'setFrameMode')) $this->setFrameMode(true);
include_once $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/tools/include_module.php';
$isAjax = Tools::isAjax();
?>
<?if(!$isAjax):?>
<span class="js-modal-basket btn btn-primary btn-cart" id="cartLine_Refresh" data-toggle="modal" data-target="#modal-basket"
	data-arparams='<?=Tools::GetEncodedArParams($arParams)?>' data-template='<?=$templateName?>'>
<?endif;?>
	<span class="flaticon-shopping16">
		<span class="badge">
			<? if (!$isAjax && method_exists($this, 'createFrame')) $frame = $this->createFrame()->begin(''); ?>
				<? if ($arParams['SHOW_PRODUCTS'] == "Y" || $arParams['SHOW_PRODUCTS'] == true) {
					$q = 0;
					if(!empty($arResult['CATEGORIES']['READY'])) {
						foreach ($arResult['CATEGORIES']['READY'] as &$arProduct) {
							$q += $arProduct['QUANTITY'];
						} unset($arProduct);
					}
					echo $q;
				} else {
					echo $arResult['NUM_PRODUCTS'];
				}
				?>
			<? if (!$isAjax && method_exists($this, 'createFrame')) $frame->end(); ?>
		</span>
	</span>
	<span class="basket-title"><?=GetMessage('RZ_CART')?></span>
<?if(!$isAjax):?>
</span>
<?endif;?>