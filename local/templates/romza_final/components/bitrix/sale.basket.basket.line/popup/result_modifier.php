<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$arResult['allSum'] = 0;
$arResult['CURRENCY'] = 'RUB';
foreach ($arResult['CATEGORIES']['READY'] as &$arItem) {
		$arResult['ITEMS']['AnDelCanBuy'][] = $arItem;
		$arResult['allSum'] += $arItem['PRICE'] * $arItem['QUANTITY'];
		$arResult['CURRENCY'] = $arItem['CURRENCY'];
}
$arResult['allSum_FORMATED'] = FormatCurrency($arResult['allSum'],$arResult['CURRENCY']);
unset($arItem);