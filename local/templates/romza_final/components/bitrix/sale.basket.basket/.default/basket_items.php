<? use Yenisite\Core\Resize;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
echo ShowError($arResult["ERROR_MESSAGE"]);

$bDelayColumn  = false;
$bDeleteColumn = false;
$bWeightColumn = false;
$bPropsColumn  = false;
$bPriceType    = false;

if ($normalCount > 0):?>
<div id="basket_items_list">
	<table id="basket_items">
		<thead class="basket_order__head">
			<tr>
				<?
				foreach ($arResult["GRID"]["HEADERS"] as $id => $arHeader):

					$arHeaders[] = $arHeader["id"];

					// remember which values should be shown not in the separate columns, but inside other columns
					if (in_array($arHeader["id"], array("TYPE")))
					{
						$bPriceType = true;
						continue;
					}
					elseif ($arHeader["id"] == "PROPS")
					{
						$bPropsColumn = true;
						continue;
					}
					elseif ($arHeader["id"] == "DELAY")
					{
						$bDelayColumn = true;
						continue;
					}
					elseif ($arHeader["id"] == "DELETE")
					{
						$bDeleteColumn = true;
						continue;
					}
					elseif ($arHeader["id"] == "WEIGHT")
					{
						$bWeightColumn = true;
					}

					if ($arHeader["id"] == "NAME"): ?>
						<th class="title" colspan="2" id="col_<?=getColumnId($arHeader)?>">
					<? elseif ($arHeader["id"] == "DISCOUNT"): ?>
						<th class="discount" id="col_<?=getColumnId($arHeader)?>">
					<? elseif ($arHeader["id"] == "QUANTITY"): ?>
						<th class="amount" id="col_<?=getColumnId($arHeader)?>">
					<? elseif ($arHeader["id"] == "PRICE"): ?>
						<th class="price" id="col_<?=getColumnId($arHeader)?>">
					<? else: ?>
						<th class="custom" id="col_<?=getColumnId($arHeader)?>">
					<? endif; ?>
						<?=getColumnName($arHeader)?>
						</th>
				<?
				endforeach;

				if ($bDeleteColumn || $bDelayColumn):
				?>
					<th class="custom"></th>
				<?
				endif;
				?>
					<th class="margin"></th>
			</tr>
		</thead>
		<tbody class="basket_order__main">
			<?
			foreach ($arResult["GRID"]["ROWS"] as $k => $arItem):

				if ($arItem["DELAY"] == "N" && $arItem["CAN_BUY"] == "Y"):
			?>
				<tr id="<?=$arItem["ID"]?>">
					<?
					foreach ($arResult["GRID"]["HEADERS"] as $id => $arHeader):

						if (in_array($arHeader["id"], array("PROPS", "DELAY", "DELETE", "TYPE"))) // some values are not shown in the columns in this template
							continue;

						if ($arHeader["id"] == "NAME"):
						?>
							<td class="img">
									<img src="<?= Resize::GetResizedImg($arItem,array('WIDTH' => 168, 'HEIGHT' => 170, 'SET_ID' => intval($arParams['RESIZER_PRODUCT'])))?>" alt=""/>
							</td>
							<td class="title">
								<?if (strlen($arItem["DETAIL_PAGE_URL"]) > 0):?><a href="<?=$arItem["DETAIL_PAGE_URL"] ?>"><?endif;?>
									<?=$arItem["NAME"]?>
								<?if (strlen($arItem["DETAIL_PAGE_URL"]) > 0):?></a><?endif;?>
								<? if (count($arItem['PROPS']) > 0): ?>
									<div class="help-block">
										<? foreach($arItem['PROPS'] as $arProp):?>
											<?= $arProp['NAME'] , ' : ' , $arProp['VALUE'] ?><br/>
										<? endforeach ?>
									</div>
								<? endif ?>
							</td>
						<?
						elseif ($arHeader["id"] == "QUANTITY"):
						?>
							<td class="amount">
								<?
								$max = isset($arItem["AVAILABLE_QUANTITY"]) ? "max=\"".$arItem["AVAILABLE_QUANTITY"]."\"" : "";
								?>
								<input class="js-touchspin" type="text" value="<?= $arItem['QUANTITY'] ?>"
									   name="QUANTITY_<?=$arItem['ID']?>"  title="">
							</td>
						<?
						elseif ($arHeader["id"] == "PRICE"):
						?>
							<td class="price">
								<?=$arItem["PRICE_FORMATED"]?>
							</td>
						<?
						elseif ($arHeader["id"] == "DISCOUNT"):
						?>
							<td class="discount">
								<?=$arItem["DISCOUNT_PRICE_PERCENT_FORMATED"]?>
							</td>
						<?
						elseif ($arHeader["id"] == "WEIGHT"):
						?>
							<td class="custom">
								<?=$arItem["WEIGHT_FORMATED"]?>
							</td>
						<?
						else:
						?>
							<td class="custom">
								<?= $arItem[$arHeader["id"]];?>
							</td>
						<?
						endif;
					endforeach;?>

					<?if($bDelayColumn):?>
						<td class="put_off">
							<a href="<?=str_replace("#ID#", $arItem["ID"], $arUrls["delay"])?>" class="put_off_link">
								<span class="icon icon_arrow652"></span>
								<?=GetMessage("SALE_DELAY")?>
							</a>
						</td>
					<?endif;?>
					<?if($bDeleteColumn):?>
						<td class="delete">
							<a href="<?=str_replace("#ID#", $arItem["ID"], $arUrls["delete"])?>" class="delete_link">
								<span class="icon icon_recycling10"></span>
								<?=GetMessage("SALE_DELETE")?>
							</a>
						</td>
					<? endif;?>
				</tr>
				<?
				endif;
			endforeach;
			?>
		</tbody>
	</table>
	<p></p>
	<div class="basket_order__foot">
		<? if ($arParams["HIDE_COUPON"] != "Y"):
			$couponClass = "";
			if (array_key_exists('VALID_COUPON', $arParams)) {
				$couponClass = ($arParams["VALID_COUPON"] == "Y") ? "has-success" : "has-error";
			}elseif (array_key_exists('COUPON', $arResult) && strlen($arResult["COUPON"]) > 0) {
				$couponClass = "has-success";
			}
			?>
			<div class="basket_order__submit column coupon_wrapper <?=$couponClass?>">
				<?=GetMessage("STB_COUPON_PROMT")?>
				<input type="text" id="coupon" name="COUPON" value="<?if($couponClass != 'has-error') { echo $arResult["COUPON"]; } else { echo htmlspecialcharsbx($_REQUEST["COUPON"]); }?>"
					    size="21" class="form-control <?=$couponClass?>">
				&nbsp;<a class="btn btn-default normal apply_coupon" href="javascript:;" style="margin-top: -1px;"><?=GetMessage("RZ_PRIMENIT_")?></a>
			</div>
		<?else:?>
			&nbsp;
		<?endif;?>
		<div class="basket_order__total column">
			<?if ($arParams["PRICE_VAT_SHOW_VALUE"] == "Y"):?>
				<div>
					<?= GetMessage('SALE_VAT_EXCLUDED')?> <b><?=$arResult["allSum_wVAT_FORMATED"]?></b>
				</div>
				<div>
					<?= GetMessage('SALE_VAT_INCLUDED')?> <b><?=$arResult["allVATSum_FORMATED"]?></b>
				</div>
			<?endif;?>
			<div>
				<?=GetMessage("SALE_TOTAL")?> <b><?=$arResult["allSum_FORMATED"]?></b>
			</div>
			<div>
				<?if (floatval($arResult["DISCOUNT_PRICE_ALL"]) > 0):?>
					<?=GetMessage("RZ_SUMMA_BEZ_SKIDKI")?> <b style="text-decoration:line-through; color:#828282;"><?=$arResult["PRICE_WITHOUT_DISCOUNT"]?></b>
				<?endif;?>
			</div>
			<?if ($bWeightColumn):?>
				<div>
					<?=GetMessage("SALE_TOTAL_WEIGHT")?> <b><?=$arResult["allWeight_FORMATED"]?></b>
				</div>
			<?endif;?>
			<div class="basket_order_buttons">
				<? if (CModule::IncludeModule('sale') && CModule::IncludeModule('yenisite.oneclick')): ?>
					<?$APPLICATION->IncludeComponent(
						"yenisite:oneclick.buy",
						"ajax",
						array(
							"PERSON_TYPE_ID" => $arParams["ONECLICK_PERSON_TYPE_ID"],
							"SHOW_FIELDS" => $arParams["ONECLICK_SHOW_FIELDS"],
							"REQ_FIELDS" => $arParams["ONECLICK_REQ_FIELDS"],
							"ALLOW_AUTO_REGISTER" => $arParams["ONECLICK_ALLOW_AUTO_REGISTER"],
							"USE_CAPTCHA" => $arParams["ONECLICK_USE_CAPTCHA"],
							"MESSAGE_OK" => $arParams["~ONECLICK_MESSAGE_OK"],
							"PAY_SYSTEM_ID" => $arParams["ONECLICK_PAY_SYSTEM_ID"],
							"DELIVERY_ID" => $arParams["ONECLICK_DELIVERY_ID"],
							"AS_EMAIL" => $arParams["ONECLICK_AS_EMAIL"],
							"AS_NAME" => $arParams["ONECLICK_AS_NAME"],
							"SEND_REGISTER_EMAIL" => $arParams["ONECLICK_SEND_REGISTER_EMAIL"],
							"FIELD_CLASS" => "form-control",
							"FIELD_PLACEHOLDER" => $arParams["ONECLICK_FIELD_PLACEHOLDER"],
							'OFFER_PROPS' => $arParams['OFFER_TREE_PROPS'],
							'BASKET_ONE_CLICK' => true
						),
						$component
					);?>
				<? endif ?>
				<div class="submit_button_wrapper">
					<input type="submit" class="btn btn-primary" value="<?=GetMessage("SALE_ORDER")?>">
					<?if ($arParams["USE_PREPAYMENT"] == "Y" && strlen($arResult["PREPAY_BUTTON"]) > 0):?>
						<?=$arResult["PREPAY_BUTTON"]?>
						<span><?=GetMessage("SALE_OR")?></span>
					<?endif;?>
				</div>
			</div>
		</div>
	</div>
</div>
<? else: ?>
<div id="basket_items_list">
	<table>
		<tbody>
			<tr>
				<td colspan="<?=$numCells?>" style="text-align:center">
					<div class=""><?=GetMessage("SALE_NO_ITEMS");?></div>
				</td>
			</tr>
		</tbody>
	</table>
</div>
<? endif; ?>