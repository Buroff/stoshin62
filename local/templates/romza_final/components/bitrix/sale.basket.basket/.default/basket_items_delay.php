<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<table class="sale_basket_basket data-table">
	<thead class="basket_order__head">
		<tr>
			<?if (in_array("NAME", $arParams["COLUMNS_LIST"])):?>
				<th class="title"><?= GetMessage("SALE_NAME")?></th>
			<?endif;?>
			<?if (in_array("PRICE", $arParams["COLUMNS_LIST"])):?>
				<th class="price"><?= GetMessage("SALE_PRICE")?></th>
			<?endif;?>
			<?if (in_array("TYPE", $arParams["COLUMNS_LIST"])):?>
				<th><?= GetMessage("SALE_PRICE_TYPE")?></th>
			<?endif;?>
			<?if (in_array("QUANTITY", $arParams["COLUMNS_LIST"])):?>
				<th class="amount"><?= GetMessage("SALE_QUANTITY")?></th>
			<?endif;?>
			<?if (in_array("DELETE", $arParams["COLUMNS_LIST"])):?>
				<th><?= GetMessage("SALE_DELETE")?></th>
			<?endif;?>
			<?if (in_array("DELAY", $arParams["COLUMNS_LIST"])):?>
				<th><?= GetMessage("SALE_OTLOG")?></th>
			<?endif;?>
			<?if (in_array("WEIGHT", $arParams["COLUMNS_LIST"])):?>
				<th><?= GetMessage("SALE_WEIGHT")?></th>
			<?endif;?>
		</tr>
	</thead>
	<tbody class="basket_order__main">
		<? foreach($arResult["ITEMS"]["DelDelCanBuy"] as $arBasketItems) { ?>
			<tr>
				<?if (in_array("NAME", $arParams["COLUMNS_LIST"])):?>
					<td class="title">
						<? if (strlen($arBasketItems["DETAIL_PAGE_URL"])>0): ?>
						<a href="<?echo $arBasketItems["DETAIL_PAGE_URL"] ?>">
						<? endif;?>
							<?=$arBasketItems["NAME"]?>
						<? if (strlen($arBasketItems["DETAIL_PAGE_URL"])>0):?>
						</a>
						<? endif; ?>
						<? if (count($arItem['PROPS']) > 0): ?>
							<div class="help-block">
								<? foreach($arItem['PROPS'] as $arProp):?>
									<?= $arProp['NAME'] , ' : ' , $arProp['VALUE'] ?><br/>
								<? endforeach ?>
							</div>
						<? endif ?>
					</td>
				<?endif;?>
				<?if (in_array("PRICE", $arParams["COLUMNS_LIST"])):?>
					<td class="price"><?= $arBasketItems["PRICE_FORMATED"] ?></td>
				<?endif;?>
				<?if (in_array("TYPE", $arParams["COLUMNS_LIST"])):?>
					<td><?= $arBasketItems["NOTES"]?></td>
				<?endif;?>
				<?if (in_array("QUANTITY", $arParams["COLUMNS_LIST"])):?>
					<td class="amount"><?= $arBasketItems["QUANTITY"]?></td>
				<?endif;?>
				<?if (in_array("DELETE", $arParams["COLUMNS_LIST"])):?>
					<td align="center"><input type="checkbox" name="DELETE_<?echo $arBasketItems["ID"] ?>" value="Y"></td>
				<?endif;?>
				<?if (in_array("DELAY", $arParams["COLUMNS_LIST"])):?>
					<td align="center"><input type="checkbox" name="DELAY_<?echo $arBasketItems["ID"] ?>" value="Y" checked></td>
				<?endif;?>
				<?if (in_array("WEIGHT", $arParams["COLUMNS_LIST"])):?>
					<td align="right"><?echo $arBasketItems["WEIGHT_FORMATED"] ?></td>
				<?endif;?>
			</tr>
		<? } ?>
	</tbody>
</table>
<br />
<div width="30%">
	<input type="submit" value="<?= GetMessage("SALE_REFRESH")?>" name="BasketRefresh"><br />
	<small><?= GetMessage("SALE_REFRESH_DESCR")?></small><br />
</div>
<br />
<?