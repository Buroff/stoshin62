<? use Yenisite\Core\Resize;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$bDelayColumn  = false;
$bDeleteColumn = false;
$bWeightColumn = false;
$bPropsColumn  = false;
?>
<div id="basket_items_delayed" class="sale_basket_basket data-table" style="display:none">
	<table id="delayed_items">
		<thead class="basket_order__head">
			<tr>
				<?
				foreach ($arResult["GRID"]["HEADERS"] as $id => $arHeader):

					if (in_array($arHeader["id"], array("TYPE","SUM", "DISCOUNT"))) // some header columns are shown differently
					{
						continue;
					}
					elseif ($arHeader["id"] == "PROPS")
					{
						$bPropsColumn = true;
						continue;
					}
					elseif ($arHeader["id"] == "DELAY")
					{
						continue;
					}
					elseif ($arHeader["id"] == "DELETE")
					{
						$bDeleteColumn = true;
						continue;
					}
					elseif ($arHeader["id"] == "WEIGHT")
					{
						$bWeightColumn = true;
					}

					if ($arHeader["id"] == "NAME"): ?>
						<th class="title" colspan="2" id="col_<?=getColumnId($arHeader)?>">
					<? elseif ($arHeader["id"] == "DISCOUNT"): ?>
						<th class="discount" id="col_<?=getColumnId($arHeader)?>">
					<? elseif ($arHeader["id"] == "QUANTITY"): ?>
						<th class="amount" id="col_<?=getColumnId($arHeader)?>">
					<? elseif ($arHeader["id"] == "PRICE"): ?>
						<th class="price" id="col_<?=getColumnId($arHeader)?>">
					<? else: ?>
						<th class="custom" id="col_<?=getColumnId($arHeader)?>">
					<? endif; ?>
						<?=getColumnName($arHeader)?>
						</th>
				<?
				endforeach;

				if ($bDeleteColumn || $bDelayColumn):
				?>
					<th class="custom"></th>
				<?
				endif;
				?>
					<th class="margin"></th>
			</tr>
		</thead>
		<tbody class="basket_order__main">
			<?
			foreach ($arResult["GRID"]["ROWS"] as $k => $arItem):

				if ($arItem["DELAY"] == "Y" && $arItem["CAN_BUY"] == "Y"):
			?>
				<tr id="<?=$arItem["ID"]?>">
					<?
					foreach ($arResult["GRID"]["HEADERS"] as $id => $arHeader):

						if (in_array($arHeader["id"], array("PROPS", "DELAY", "DELETE", "TYPE","SUM", "DISCOUNT"))) // some values are not shown in the columns in this template
							continue;

						if ($arHeader["id"] == "NAME"):
						?>
							<td class="img">
								<img src="<?= Resize::GetResizedImg($arItem,array('WIDTH' => 168, 'HEIGHT' => 170, 'SET_ID' => intval($arParams['RESIZER_PRODUCT'])))?>" alt=""/>
							</td>
							<td class="title">
								<?if (strlen($arItem["DETAIL_PAGE_URL"]) > 0):?><a href="<?=$arItem["DETAIL_PAGE_URL"] ?>"><?endif;?>
									<?=$arItem["NAME"]?>
								<?if (strlen($arItem["DETAIL_PAGE_URL"]) > 0):?></a><?endif;?>
								<input type="hidden" name="DELAY_<?=$arItem["ID"]?>" value="Y"/>
								<? if (count($arItem['PROPS']) > 0): ?>
									<div class="help-block">
										<? foreach($arItem['PROPS'] as $arProp):?>
											<?= $arProp['NAME'] , ' : ' , $arProp['VALUE'] ?><br/>
										<? endforeach ?>
									</div>
								<? endif ?>
							</td>
						<?
						elseif ($arHeader["id"] == "QUANTITY"):
						?>
							<td class="amount">
								<?
								$max = isset($arItem["AVAILABLE_QUANTITY"]) ? "max=\"".$arItem["AVAILABLE_QUANTITY"]."\"" : "";
								?>
								<input type="text" class="js-touchspin" disabled
									id="QUANTITY_<?=$arItem["ID"]?>"
									name="QUANTITY_<?=$arItem["ID"]?>"
									value="<?=$arItem["QUANTITY"]?>" >
							</td>
						<?
						elseif ($arHeader["id"] == "PRICE"):
						?>
							<td class="price">
								<?=$arItem["PRICE_FORMATED"]?>
							</td>
						<?
						elseif ($arHeader["id"] == "DISCOUNT"):
						?>
							<td class="discount">
								<?=$arItem["DISCOUNT_PRICE_PERCENT_FORMATED"]?>
							</td>
						<?
						elseif ($arHeader["id"] == "WEIGHT"):
						?>
							<td class="custom">
								<?=$arItem["WEIGHT_FORMATED"]?>
							</td>
						<?
						else:
						?>
							<td class="custom">
								<?= $arItem[$arHeader["id"]];?>
							</td>
						<?
						endif;
					endforeach;?>

					<td class="put_off">
						<a href="<?=str_replace("#ID#", $arItem["ID"], $arUrls["add"])?>" class="put_off_link">
							<span class="icon icon_arrow652"></span>
							<?=GetMessage("SALE_ADD_TO_BASKET")?>
						</a>
					</td>
					<?if($bDeleteColumn):?>
						<td class="delete">
							<a href="<?=str_replace("#ID#", $arItem["ID"], $arUrls["delete"])?>" class="delete_link">
								<span class="icon icon_recycling10"></span>
								<?=GetMessage("SALE_DELETE")?>
							</a>
						</td>
					<? endif;?>

				</tr>
				<?
				endif;
			endforeach;
			?>
		</tbody>
	</table>
</div>
<?