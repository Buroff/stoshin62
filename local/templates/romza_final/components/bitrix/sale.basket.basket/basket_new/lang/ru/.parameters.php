<?
$MESS["SALE_PROPERTIES_RECALCULATE_BASKET"] = "Свойства, влияющие на пересчет корзины";

$MESS['RESIZER_SETS'] = 'Ресайзер: выбор набора(ов)';
$MESS['RESIZER_PRODUCT'] = 'Для фото товара в таблице';

$MESS["RZ_ONECLICK_FIELD_PLACEHOLDER"] = "Использовать атрибут placeholder у полей?";
$MESS["RZ_ONECLICK_FIELD_QUANTITY"] = "Использовать указание количества товара";
$MESS["RZ_ONECLICK_GROUP"] = "Заказ в 1 клик";
$MESS["RZ_ONECLICK_SHOW_FIELDS"] = "Выводимые поля";
$MESS["RZ_ONECLICK_SHOW_FIELDS_EMPTY"] = "Выберите тип покупателя";
$MESS["RZ_ONECLICK_PERSON_TYPE_ID"] = "Тип покупателя";
$MESS["RZ_ONECLICK_REQ_FIELDS"] = "Поля необходимые для заполнения";
$MESS["RZ_ONECLICK_REQ_FIELDS_EMPTY"] = "Сначала выберите поля для заполнения";
$MESS["RZ_ONECLICK_AS_EMAIL"] = "Поле E-mail";
$MESS["RZ_ONECLICK_AS_EMAIL_EMPTY"] = "Сначала выберите поля для заполнения";
$MESS["RZ_ONECLICK_AS_EMAIL_NOT_USE"] = "Не использовать";
$MESS["RZ_ONECLICK_AS_NAME"] = "Поле Имя";
$MESS["RZ_ONECLICK_AS_NAME_EMPTY"] = "Сначала выберите поля для заполнения";
$MESS["RZ_ONECLICK_AS_NAME_NOT_USE"] = "Не использовать";
$MESS["RZ_ONECLICK_USER_REGISTER_EVENT_NAME"] = "Имя почтового шаблона для регистрации пользователя";
$MESS["ONECLICK_SHOW_FIELDS_TIP"] = "символом * отмечены поля которым выставлен признак обязательности";
$MESS["RZ_ONECLICK_MESSAGE_OK"] = "Сообщение об успешном оформлении";
$MESS["ONECLICK_MESSAGE_OK_TIP"] = "можно использовать плейсхолдер #ID# - для замены на номер созданного заказа";
$MESS["RZ_ONECLICK_MESSAGE_OK_DEFAULT"] = "Ваш заказ принят, его номер - <b>#ID#</b>. Менеджер свяжется с вами в ближайшее время.<br> Спасибо что выбрали нас!";
$MESS["RZ_ONECLICK_PAY_SYSTEM_ID"] = "Служба оплаты";
$MESS["RZ_ONECLICK_PAY_SYSTEM_ID_NOT_SET"] = "Не устанавливать";
$MESS["RZ_ONECLICK_PAY_SYSTEM_ID_EMPTY"] = "Сначала выберите тип плательщика";
$MESS["ONECLICK_PAY_SYSTEM_ID_TIP"] = "По-умолчанию будет установленна эта служба";
$MESS["RZ_ONECLICK_DELIVERY_ID"] = "Служба доставки";
$MESS["RZ_ONECLICK_DELIVERY_ID_NOT_SET"] = "Не устанавливать";
$MESS["ONECLICK_DELIVERY_ID_TIP"] = "По-умолчанию будет установленна эта служба";
$MESS['RZ_ONECLICK_SEND_USER_REGISTER_EMAIL'] = 'Отправлять письмо пользователю об успешной регистрации';
$MESS["RZ_ONECLICK_ALLOW_AUTO_REGISTER"] = "Автоматически регистрировать и авторизовавать пользователя";
$MESS["RZ_ONECLICK_USE_CAPTCHA"] = "Использовать CAPTCHA";
$MESS["ONECLICK_USE_CAPTCHA_TIP"] = "Включает CAPTCHA при оформлении заказа если пользователь не авторизован";
?>