<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<div class="ordering_block step_4">
	<?
	if (!empty($arResult["ORDER"])) {
		?>
		<p class="order_ready__head"><?= GetMessage("SOA_TEMPL_ORDER_COMPLETE") ?></p>
		<p class="order_ready__info">
			<?= GetMessage("SOA_TEMPL_ORDER_SUC", Array("#ORDER_DATE#" => $arResult["ORDER"]["DATE_INSERT"], "#ORDER_ID#" => $arResult["ORDER"]["ID"],'#SITE_DIR#' => SITE_DIR)) ?>
		</p>
		<div class="order_ready__note">
			<p><?= GetMessage("SOA_TEMPL_ORDER_SUC1", Array("#LINK#" => $arParams["PATH_TO_PERSONAL"])) ?></p>
		</div>
		<? if (!empty($arResult["PAY_SYSTEM"])) { ?>
			<div class="order_ready__note">
				<p class="pay_name"><?= GetMessage("SOA_TEMPL_PAY") ?></p>
				<?= CFile::ShowImage($arResult["PAY_SYSTEM"]["LOGOTIP"], 100, 100, "border=0", "", false); ?>
				<p class="paysystem_name"><?= $arResult["PAY_SYSTEM"]["NAME"] ?></p>
				<?
				if (strlen($arResult["PAY_SYSTEM"]["ACTION_FILE"]) > 0) {
					?>
					<div>
						<?
						if ($arResult["PAY_SYSTEM"]["NEW_WINDOW"] == "Y") {
							?>
							<script language="JavaScript">
								window.open('<?=$arParams["PATH_TO_PAYMENT"]?>?ORDER_ID=<?=urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"]))?>');
							</script>
							<?= GetMessage("SOA_TEMPL_PAY_LINK", Array("#LINK#" => $arParams["PATH_TO_PAYMENT"] . "?ORDER_ID=" . urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"])))) ?>
							<?
							if (CSalePdf::isPdfAvailable() && CSalePaySystemsHelper::isPSActionAffordPdf($arResult['PAY_SYSTEM']['ACTION_FILE'])) {
								?><br/>
								<?= GetMessage("SOA_TEMPL_PAY_PDF", Array("#LINK#" => $arParams["PATH_TO_PAYMENT"] . "?ORDER_ID=" . urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"])) . "&pdf=1&DOWNLOAD=Y")) ?>
							<?
							}
						} else {
							if (strlen($arResult["PAY_SYSTEM"]["PATH_TO_ACTION"]) > 0) {
								include($arResult["PAY_SYSTEM"]["PATH_TO_ACTION"]);
							}
						}
						?>
					</div>
				<?
				}
				?>
			</div>
		<?
		}
	} else {
		?>
		<p class="order_ready__head"><?= GetMessage("SOA_TEMPL_ERROR_ORDER") ?></p>
		<p class="order_ready__info"><?= GetMessage("SOA_TEMPL_ERROR_ORDER_LOST", Array("#ORDER_ID#" => $arResult["ACCOUNT_NUMBER"])) ?></p>
		<div class="order_ready__note">
			<?= GetMessage("SOA_TEMPL_ERROR_ORDER_LOST1") ?>
		</div>
	<?
	}
	?>
</div>
