<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if(count($arResult["PERSON_TYPE"]) > 1)
{
	?>
	<div class="ordering_block__inner">
		<div class="ordering_block__inner_head"><?=GetMessage("SOA_TEMPL_PERSON_TYPE")?></div>
		<div class="user_type">
			<?foreach($arResult["PERSON_TYPE"] as $v):?>
				<label class="forms-wrapper" for="PERSON_TYPE_<?=$v["ID"]?>">
					<input type="radio" name="PERSON_TYPE" id="PERSON_TYPE_<?=$v["ID"]?>"
						   value="<?=$v["ID"]?>" <?if ($v["CHECKED"]=="Y") echo " checked=\"checked\"";?> onclick="submitForm()">
					<span class="form-label"><?=$v["NAME"]?></span>
				</label>
			<?endforeach;?>
		</div>
		<input type="hidden" name="PERSON_TYPE_OLD" value="<?=$arResult["USER_VALS"]["PERSON_TYPE_ID"]?>" />
	</div>
	<?
}
else
{
	if(IntVal($arResult["USER_VALS"]["PERSON_TYPE_ID"]) > 0)
	{
		//for IE 8, problems with input hidden after ajax
		?>
		<span style="display:none;">
		<input type="text" name="PERSON_TYPE" value="<?=IntVal($arResult["USER_VALS"]["PERSON_TYPE_ID"])?>" />
		<input type="text" name="PERSON_TYPE_OLD" value="<?=IntVal($arResult["USER_VALS"]["PERSON_TYPE_ID"])?>" />
		</span>
		<?
	}
	else
	{
		foreach($arResult["PERSON_TYPE"] as $v)
		{
			?>
			<input type="hidden" id="PERSON_TYPE" name="PERSON_TYPE" value="<?=$v["ID"]?>" />
			<input type="hidden" name="PERSON_TYPE_OLD" value="<?=$v["ID"]?>" />
			<?
		}
	}
}
?>