(typeof(jQuery) != 'undefined')
&& (function ($) {
	if(BX.Sale.OrderAjaxComponent !== 'undefined')
	{
		$("input[name='PERSON_TYPE']").closest('label').on('change', function(){BX.Sale.OrderAjaxComponent.sendRequest()});
		$("select[name='PROFILE_ID']").on('change', function(){BX.Sale.OrderAjaxComponent.sendRequest()});
	}
})(jQuery);