<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
$arAuthServices = $arPost = array();
if (is_array($arResult['SERVICES'])) {
	$arAuthServices = $arResult['SERVICES'];
}
if (is_array($arParams["~POST"])) {
	$arPost = $arParams["~POST"];
}
?>
<?
//todo: missing icons
$arIcons = array(
	'yandex',
	'fb',
	'ok',
	'vk',
	'twitter'
)
?>
<?if (method_exists($this, 'setFrameMode')) $this->setFrameMode(true); ?>
<div class="form-group socials">
	<div class="socials_head"><?= GetMessage("RZ_SOCSERV_AUTH") ?>:</div>
	<div class="socials_list">
		<form method="post" name="bx_auth_services<?= $arParams["SUFFIX"] ?>" target="_top" action="<?= $arParams["AUTH_URL"] ?>">
			<? foreach ($arAuthServices as $service): ?>
				<? if (!in_array($service["ICON"], $arIcons)):?>
					<a href="javascript:void(0)" onclick="BxShowAuthService('<?= $service["ID"] ?>', '<?= $arParams["SUFFIX"] ?>')"
					   class="social_icon"
					   id="bx_auth_href_<?= $arParams["SUFFIX"] ?><?= $service["ID"] ?>" title="<?=htmlspecialcharsbx($service["NAME"])?>">
						<i class="bx-ss-icon <?=htmlspecialcharsbx($service["ICON"])?>"></i>
					</a>
				<? else:?>
					<a href="javascript:void(0)" onclick="BxShowAuthService('<?= $service["ID"] ?>', '<?= $arParams["SUFFIX"] ?>')"
						id="bx_auth_href_<?= $arParams["SUFFIX"] ?><?= $service["ID"] ?>"
						title="<?=htmlspecialcharsbx($service["NAME"])?>"
						class="social_icon sprite sprite-<?= htmlspecialcharsbx($service["ICON"]) ?>"></a>
				<? endif ?>
			<? endforeach ?>
			<div class="bx-auth-service-form" id="bx_auth_serv<?= $arParams["SUFFIX"] ?>" style="display:none">
				<? foreach ($arAuthServices as $service): ?>
					<? if (($arParams["~FOR_SPLIT"] != 'Y') || (!is_array($service["FORM_HTML"]))): ?>
						<div id="bx_auth_serv_<?= $arParams["SUFFIX"] ?><?= $service["ID"] ?>"
							 style="display:none"><?= $service["FORM_HTML"] ?></div>
					<? endif; ?>
				<? endforeach ?>
			</div>
			<? foreach ($arPost as $key => $value): ?>
				<? if (!preg_match("|OPENID_IDENTITY|", $key)): ?>
					<input type="hidden" name="<?= $key ?>" value="<?= $value ?>"/>
				<? endif; ?>
			<? endforeach ?>
			<input type="hidden" name="auth_service_id" value=""/>
		</form>
	</div>
</div>