<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
use \Yenisite\Core\Tools;
//***********************************
//setting section
//***********************************
\Bitrix\Main\Localization\Loc::loadMessages($_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH . '/lang/'.LANGUAGE_ID.'/header.php');
$pathToRules = COption::GetOptionString(CRZShinmarketSettings::getModuleId(),'path_tu_rules_privacy',SITE_DIR.'personal/rules/personal_data.php');
$rand = $this->randString();

?>
<div class="col-md-6">
	<form class="modal-form" action="<?= $arResult["FORM_ACTION"] ?>" method="post">
        <input type="hidden" name="privacy_policy" value="N"/>
		<?= bitrix_sessid_post(); ?>
		<div class="form-group">
			<h2><?= GetMessage("subscr_title_settings") ?></h2>
		</div>
		<div class="form-group required">
			<label for=""><?= GetMessage("subscr_email") ?></label>
			<input type="text" name="EMAIL" class="form-control"
				   value="<?= ($arResult["SUBSCRIPTION"]["EMAIL"] != "") ?
					   $arResult["SUBSCRIPTION"]["EMAIL"] : $arResult["REQUEST"]["EMAIL"]; ?>"/>
		</div>
		<div class="form-group required">
			<label for=""><?= GetMessage("subscr_rub") ?></label>
		</div>
		<? foreach ($arResult["RUBRICS"] as $itemID => $itemValue): ?>
			<div class="form-group">
				<label>
					<input type="checkbox" name="RUB_ID[]"
						   value="<?= $itemValue["ID"] ?>"<? if ($itemValue["CHECKED"]) echo " checked" ?> /><?= $itemValue["NAME"] ?>
				</label>
			</div>
		<? endforeach; ?>
		<div class="form-group">
			<label for=""><?= GetMessage("subscr_fmt") ?></label>
			<label>
				<input type="radio" name="FORMAT"
					   value="text"<? if ($arResult["SUBSCRIPTION"]["FORMAT"] == "text") echo " checked" ?> /><?= GetMessage("subscr_text") ?>
			</label>&nbsp;/&nbsp;<label>
				<input type="radio" name="FORMAT"
					   value="html"<? if ($arResult["SUBSCRIPTION"]["FORMAT"] == "html") echo " checked" ?> />HTML</label>
		</div>
		<div class="form-group">
			<p class="help-block"><?= GetMessage("subscr_settings_note1") ?></p>

			<p class="help-block"><?= GetMessage("subscr_settings_note2") ?></p>
		</div>
        <div class="form-group agreement-policy">
            <div class="forms-wrapper">
                <input id="privacy_policy_<?=$rand?>" required type="checkbox" name="privacy_policy" value="Y" <?=$_REQUEST['privacy_policy'] == 'Y' ? ' checked' : ''?>>
                <label for="privacy_policy_<?=$rand?>">
                    <?= GetMessage('RZ_ACCEPT') ?> <?=GetMessage('RZ_CONCLUSION',array("#URL#" => Tools::GetConstantUrl($pathToRules)))?>
                </label>
            </div>
        </div>
		<div class="form-group">
			<input type="submit" name="Save" class="btn btn-primary"
				   value="<?= ($arResult["ID"] > 0 ? GetMessage("subscr_upd") : GetMessage("subscr_add")) ?>"/>
			<input type="reset" value="<?= GetMessage("subscr_reset") ?>" name="reset" class="btn btn-link"/>
		</div>
		<input type="hidden" name="PostAction" value="<?= ($arResult["ID"] > 0 ? "Update" : "Add") ?>"/>
		<input type="hidden" name="ID" value="<?= $arResult["SUBSCRIPTION"]["ID"]; ?>"/>
		<? if ($_REQUEST["register"] == "YES"): ?>
			<input type="hidden" name="register" value="YES"/>
		<? endif; ?>
		<? if ($_REQUEST["authorize"] == "YES"): ?>
			<input type="hidden" name="authorize" value="YES"/>
		<? endif; ?>
	</form>
</div>
