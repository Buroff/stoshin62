<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (!$arResult["NavShowAlways"]) {
	if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
		return;
}

$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"] . "&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?" . $arResult["NavQueryString"] : "");
?>
<div class="pagination_wrapper">
	<ul class="pagination">
		<? if ($arResult["bDescPageNumbering"] === true): ?>
			<? if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]): ?>
				<? if ($arResult["bSavePage"]): ?>
					<li><a class="first"
						   href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["NavPageCount"] ?>"><?= GetMessage("nav_begin") ?></a>
					</li>
					<li><a class="prev icon icon_arrow133"
						   href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] + 1) ?>"></a>
					</li>
				<? else: ?>
					<li><a class="first" href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>"><?= GetMessage("nav_begin") ?></a>
					</li>
					<? if ($arResult["NavPageCount"] == ($arResult["NavPageNomer"] + 1)): ?>
						<li><a class="prev icon icon_arrow133" href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>"></a></li>
					<? else: ?>
						<li><a class="prev icon icon_arrow133"
							   href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] + 1) ?>"></a>
						</li>
					<?endif ?>
				<?endif ?>
			<? else: ?>
				<li><a class="first disabled"><?= GetMessage("nav_begin") ?></a></li>
				<li><a class="prev icon icon_arrow133 disabled"></a></li>
			<?endif ?>

			<? while ($arResult["nStartPage"] >= $arResult["nEndPage"]): ?>
				<? $NavRecordGroupPrint = $arResult["NavPageCount"] - $arResult["nStartPage"] + 1; ?>
				<? if ($arResult["nStartPage"] == $arResult["NavPageNomer"]): ?>
					<li><a class="current"><?= $NavRecordGroupPrint ?></a></li>
				<? elseif ($arResult["nStartPage"] == $arResult["NavPageCount"] && $arResult["bSavePage"] == false): ?>
					<li><a href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>"><?= $NavRecordGroupPrint ?></a></li>
				<?
				else: ?>
					<li>
						<a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["nStartPage"] ?>"><?= $NavRecordGroupPrint ?></a>
					</li>
				<?endif ?>
				<? $arResult["nStartPage"]-- ?>
			<? endwhile ?>

			<? if ($arResult["NavPageNomer"] > 1): ?>
				<li>
					<a class="next icon icon_right20"
					   href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] - 1) ?>"></a>
				</li>
				<li>
					<a class="last" href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=1">
						<?= GetMessage("nav_end") ?>
					</a>
				</li>
			<? else: ?>
				<li><a class="next icon icon_right20 disabled"></a></li>
				<li><a class="last disabled"><?= GetMessage("nav_end") ?></a></li>
			<?endif ?>
		<? else: ?>
			<? if ($arResult["NavPageNomer"] > 1): ?>
				<? if ($arResult["bSavePage"]): ?>
					<li>
						<a class="first" href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=1">
							<?= GetMessage("nav_begin") ?>
						</a>
					</li>
					<li>
						<a class="next icon icon_right20"
						   href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] - 1) ?>"></a>
					</li>
				<? else: ?>
					<li>
						<a class="first" href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>">
							<?= GetMessage("nav_begin") ?>
						</a>
					</li>
					<? if ($arResult["NavPageNomer"] > 2): ?>
						<li>
							<a class="prev icon icon_arrow133"
							   href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] - 1) ?>"></a>
						</li>
					<? else: ?>
						<li>
							<a class="prev icon icon_arrow133" href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>"></a>
						</li>
					<?endif ?>
				<?endif ?>
			<? else: ?>
				<li><a class="first disabled"><?= GetMessage("nav_begin") ?></a></li>
				<li><a class="prev icon icon_arrow133 disabled"></a></li>
			<?endif ?>
			<? while ($arResult["nStartPage"] <= $arResult["nEndPage"]): ?>
				<? if ($arResult["nStartPage"] == $arResult["NavPageNomer"]): ?>
					<li><a class="current"><?= $arResult["nStartPage"] ?></a></li>
				<? elseif ($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false): ?>
					<li>
						<a href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>"><?= $arResult["nStartPage"] ?></a>
					</li>
				<?
				else: ?>
					<li>
						<a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["nStartPage"] ?>">
							<?= $arResult["nStartPage"] ?>
						</a>
					</li>
				<?endif ?>
				<? $arResult["nStartPage"]++ ?>
			<? endwhile ?>

			<? if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]): ?>
				<li>
					<a class="next icon icon_right20"
					   href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] + 1) ?>"></a>
				</li>
				<li>
					<a class="last"
					   href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["NavPageCount"] ?>">
						<?= GetMessage("nav_end") ?>
					</a>
				</li>
			<? else: ?>
				<li><a class="next icon icon_right20 disabled"></a></li>
				<li><a class="last disabled"><?= GetMessage("nav_end") ?></a></li>
			<?endif ?>
		<?endif ?>

	</ul>
</div>