<?
$MESS["CPT_BCT_TPL_VIEW_MODE_BANNER"] = "баннер";
$MESS["CPT_BCT_TPL_VIEW_MODE_SLIDER"] = "слайдер";
$MESS["CPT_BCT_TPL_VIEW_MODE_SECTION"] = "список";
$MESS["CP_BCT_TPL_THEME_SITE"] = "Брать тему из настроек сайта (для решения bitrix.eshop)";
$MESS["CP_BCT_TPL_THEME_BLUE"] = "синяя (тема по умолчанию)";
$MESS["CP_BCT_TPL_THEME_GREEN"] = "зеленая";
$MESS["CP_BCT_TPL_THEME_RED"] = "красная";
$MESS["CP_BCT_TPL_THEME_WOOD"] = "дерево";
$MESS["CP_BCT_TPL_THEME_YELLOW"] = "желтая";
$MESS["CP_BCT_TPL_THEME_BLACK"] = "темная";
$MESS["CP_BCT_TPL_DML_SIMPLE"] = "простой режим";
$MESS["CP_BCT_TPL_DML_EXT"] = "расширенный";
$MESS["CP_BCT_TPL_PROP_EMPTY"] = "не выбрано";
$MESS["CPT_BCT_TPL_VIEW_MODE"] = "Показ элементов";
$MESS["CP_BCT_TPL_TEMPLATE_THEME"] = "Цветовая тема";
$MESS["CP_BCT_TPL_PRODUCT_DISPLAY_MODE"] = "Схема отображения";
$MESS["CP_BCT_TPL_ADD_PICT_PROP"] = "Дополнительная картинка основного товара";
$MESS["CP_BCT_TPL_LABEL_PROP"] = "Свойство меток товара";
$MESS["CP_BCT_TPL_OFFER_ADD_PICT_PROP"] = "Дополнительные картинки предложения";
$MESS["CP_BCT_TPL_OFFER_TREE_PROPS"] = "Свойства для отбора предложений";
$MESS["CP_BCT_TPL_SHOW_DISCOUNT_PERCENT"] = "Показывать процент скидки";
$MESS["CP_BCT_TPL_SHOW_OLD_PRICE"] = "Показывать старую цену";
$MESS["CP_BCT_TPL_ROTATE_TIMER"] = "Время показа одного слайда, сек (0 - выключить автоматическую смену слайдов)";
$MESS["CP_BCT_TPL_SHOW_PAGINATION"] = "Показывать навигацию по слайдам";
$MESS["CP_BCT_TPL_MESS_BTN_BUY"] = "Текст кнопки \"Купить\"";
$MESS["CP_BCT_TPL_MESS_BTN_ADD_TO_BASKET"] = "Текст кнопки \"Добавить в корзину\"";
$MESS["CP_BCT_TPL_MESS_BTN_DETAIL"] = "Текст кнопки \"Подробнее\"";
$MESS["CP_BCT_TPL_MESS_NOT_AVAILABLE"] = "Сообщение об отсутствии товара";
$MESS["CP_BCT_TPL_MESS_BTN_BUY_DEFAULT"] = "Купить";
$MESS["CP_BCT_TPL_MESS_BTN_ADD_TO_BASKET_DEFAULT"] = "В корзину";
$MESS["CP_BCT_TPL_MESS_BTN_DETAIL_DEFAULT"] = "Подробнее";
$MESS["CP_BCT_TPL_MESS_NOT_AVAILABLE_DEFAULT"] = "Отсутствует";
$MESS["VIEW_MODE_TIP"] = "Настройка определяет, как будет выглядеть перечень элементов на странице";
$MESS["TEMPLATE_THEME_TIP"] = "Цветовая тема для отображения. По умолчанию берется синяя тема.";
$MESS["PRODUCT_DISPLAY_MODE_TIP"] = "Схема отображения товаров (с SKU или без и т.д.)";
$MESS["ADD_PICT_PROP_TIP"] = "Свойство дополнительных картинок товара";
$MESS["LABEL_PROP_TIP"] = "Свойство меток товара";
$MESS["OFFER_ADD_PICT_PROP_TIP"] = "Свойство дополнительных картинок предложений (если есть)";
$MESS["OFFER_TREE_PROPS_TIP"] = "Свойства, по значениям которых будут группироваться торговые предложения";
$MESS["SHOW_DISCOUNT_PERCENT_TIP"] = "Вывод процентного значения скидки, если скидка есть";
$MESS["SHOW_OLD_PRICE_TIP"] = "Показывать старую цену, если есть скидка";
$MESS["ROTATE_TIMER_TIP"] = "Время показа одного слайда. Используется для автоматической прокрутки слайдов.";
$MESS["SHOW_PAGINATION_TIP"] = "Показывать навигационную строку слайдов";
$MESS["MESS_BTN_BUY_TIP"] = "Какой текст выводить на кнопке";
$MESS["MESS_BTN_ADD_TO_BASKET_TIP"] = "Какой текст выводить на кнопке";
$MESS["MESS_BTN_DETAIL_TIP"] = "Какой текст выводить на кнопке";
$MESS["MESS_NOT_AVAILABLE_TIP"] = "Какой текст выводить на кнопке";

$MESS['RESIZER_SETS'] = 'Ресайзер: выбор набора(ов)';
$MESS['RESIZER_PRODUCT'] = 'Для фото товара в списке';

$MESS["RZ_ONECLICK_FIELD_PLACEHOLDER"] = "Использовать атрибут placeholder у полей?";
$MESS["RZ_ONECLICK_FIELD_QUANTITY"] = "Использовать указание количества товара";
$MESS["RZ_ONECLICK_GROUP"] = "Заказ в 1 клик";
$MESS["RZ_ONECLICK_SHOW_FIELDS"] = "Выводимые поля";
$MESS["RZ_ONECLICK_SHOW_FIELDS_EMPTY"] = "Выберите тип покупателя";
$MESS["RZ_ONECLICK_PERSON_TYPE_ID"] = "Тип покупателя";
$MESS["RZ_ONECLICK_REQ_FIELDS"] = "Поля необходимые для заполнения";
$MESS["RZ_ONECLICK_REQ_FIELDS_EMPTY"] = "Сначала выберите поля для заполнения";
$MESS["RZ_ONECLICK_AS_EMAIL"] = "Поле E-mail";
$MESS["RZ_ONECLICK_AS_EMAIL_EMPTY"] = "Сначала выберите поля для заполнения";
$MESS["RZ_ONECLICK_AS_EMAIL_NOT_USE"] = "Не использовать";
$MESS["RZ_ONECLICK_AS_NAME"] = "Поле Имя";
$MESS["RZ_ONECLICK_AS_NAME_EMPTY"] = "Сначала выберите поля для заполнения";
$MESS["RZ_ONECLICK_AS_NAME_NOT_USE"] = "Не использовать";
$MESS["RZ_ONECLICK_USER_REGISTER_EVENT_NAME"] = "Имя почтового шаблона для регистрации пользователя";
$MESS["ONECLICK_SHOW_FIELDS_TIP"] = "символом * отмечены поля которым выставлен признак обязательности";
$MESS["RZ_ONECLICK_MESSAGE_OK"] = "Сообщение об успешном оформлении";
$MESS["ONECLICK_MESSAGE_OK_TIP"] = "можно использовать плейсхолдер #ID# - для замены на номер созданного заказа";
$MESS["RZ_ONECLICK_MESSAGE_OK_DEFAULT"] = "Ваш заказ принят, его номер - <b>#ID#</b>. Менеджер свяжется с вами в ближайшее время.<br> Спасибо что выбрали нас!";
$MESS["RZ_ONECLICK_PAY_SYSTEM_ID"] = "Служба оплаты";
$MESS["RZ_ONECLICK_PAY_SYSTEM_ID_NOT_SET"] = "Не устанавливать";
$MESS["RZ_ONECLICK_PAY_SYSTEM_ID_EMPTY"] = "Сначала выберите тип плательщика";
$MESS["ONECLICK_PAY_SYSTEM_ID_TIP"] = "По-умолчанию будет установленна эта служба";
$MESS["RZ_ONECLICK_DELIVERY_ID"] = "Служба доставки";
$MESS["RZ_ONECLICK_DELIVERY_ID_NOT_SET"] = "Не устанавливать";
$MESS["ONECLICK_DELIVERY_ID_TIP"] = "По-умолчанию будет установленна эта служба";
$MESS['RZ_ONECLICK_SEND_USER_REGISTER_EMAIL'] = 'Отправлять письмо пользователю об успешной регистрации';
$MESS["RZ_ONECLICK_ALLOW_AUTO_REGISTER"] = "Автоматически регистрировать и авторизовавать пользователя";
$MESS["RZ_ONECLICK_USE_CAPTCHA"] = "Использовать CAPTCHA";
$MESS["ONECLICK_USE_CAPTCHA_TIP"] = "Включает CAPTCHA при оформлении заказа если пользователь не авторизован";
?>