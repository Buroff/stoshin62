<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @var string $strElementEdit */
/** @var string $strElementDelete */
/** @var array $arElementDeleteParams */
/** @var array $arSkuTemplate */
if (method_exists($this, 'setFrameMode')) $this->setFrameMode(true); ?>
<div class="modal fade" id="modal-basket" tabindex="-1" role="dialog" <?/*aria-labelledby="basket_modalLabel"*/?> aria-hidden="true">
	<div class="container">
		<div class="popup modal-dialog popup_basket">
		</div>
	</div>
</div>