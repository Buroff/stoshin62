<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
<ul class="breadcrumb">
	<li><a class="flaticon-home39" href="/"></a></li>
	<li class="active"><?= GetMessage("RZ_OSHIBKA") ?> 404</li>
</ul>
<h2><?= GetMessage("RZ_OSHIBKA") ?> 404</h2>

<div class="error_404">
	<?= GetMessage("RZ_OJ__KAZHETSYA_CHTO_TO_POSHLO_NE_TAK_") ?><br>
	<?= GetMessage("RZ_STRANITCA__KOTORUYU_VI_ISHETE__UDALENA_ILI_PROSTO_NE_SUSHESTVUET_") ?><br>
	<?= GetMessage("RZ_NO_MI_UVERENI__CHTO_VI_SMOZHETE_NAJTI_TO__CHTO_VAM_NUZHNO_") ?><br>
	<?= GetMessage("RZ_PEREJTI_NA") ?> <a href="sitemap.php"><?= GetMessage("RZ_KARTU_SAJTA") ?></a>
</div>
