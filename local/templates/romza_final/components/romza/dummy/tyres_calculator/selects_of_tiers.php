<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$bHasTiers = !empty($arResult['OTHERS_PROPS']['MODELS'][0]['TIERS_SIZES']);
if ($bHasTiers){
    $arTiers = $arResult['OTHERS_PROPS']['MODELS'][0]['TIERS_SIZES'];
    $arJsH = $arResult['PROPS']['H'];
    $arJsW = $arResult['PROPS']['W'];
    $arJsR = $arResult['PROPS']['R'];
    $arResult['PROPS']['R'] = array_flip($arTiers['FRONTS_D']);
    $arResult['PROPS']['H'] = array_flip($arTiers['FRONTS_P']);
    $arResult['PROPS']['W'] = array_flip($arTiers['FRONTS_W']);
}
?>
<div class="descr-wrap">
    <div class="descr-sel">
        <?=GetMessage('RZ_SHIRINA')?>
        <span class="text"><?=GetMessage('RZ_MILIMETRI')?></span>
    </div>
    <div class="descr-sel">
        <?=GetMessage('RZ_PROFIL')?>
        <span class="text"><?=GetMessage('RZ_MILIMETRI')?></span>
    </div>
    <div class="descr-sel">
        <?=GetMessage('RZ_RADIUS')?>
        <span class="text"><?=GetMessage('RZ_DUIMS')?></span>
    </div>
</div>
<div class="selects-wrap">
    <div class="sel-wrap">
        <select <?=$bOldNames ? 'name="old_W" id="old_W"' : 'name="new_W" id="new_W"'?>>
            <?foreach ($arResult['PROPS']['W'] as $idRadius => $value):?>
                <option value="<?=$idRadius?>"><?=$idRadius?></option>
            <?endforeach;?>
        </select>
    </div>
    <div class="sel-wrap">
        <select <?=$bOldNames ? 'name="old_H" id="old_H"' : 'name="new_H" id="new_H"'?>>
            <?foreach ($arResult['PROPS']['H'] as $idRadius => $value):?>
                <option value="<?=$idRadius?>"><?=$idRadius?></option>
            <?endforeach;?>
        </select>
    </div>
    <div class="sel-wrap">
        <select <?=$bOldNames ? 'name="old_D" id="old_R"' : 'name="new_D" id="new_R"'?>>
            <?foreach ($arResult['PROPS']['R'] as $idRadius => $value):?>
                <option value="<?=$idRadius?>"><?=$idRadius?></option>
            <?endforeach;?>
        </select>
    </div>
</div>
<?if ($bHasTiers){
    $arResult['PROPS']['R'] = $arJsR;
    $arResult['PROPS']['H'] = $arJsH;
    $arResult['PROPS']['W'] = $arJsW;
    unset($arJsH,$arJsR,$arJsW);
}?>