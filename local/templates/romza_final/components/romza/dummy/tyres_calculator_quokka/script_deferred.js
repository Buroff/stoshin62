(typeof(jQuery) != 'undefined')
&& (function ($) {

	var $calc = $('#search_formCalculator');
	var $jslider = $('.js-speed-slider');
	_$ = {
		'old_W': '', 'old_H': '', 'old_R': '',
		'new_W': '', 'new_H': '', 'new_R': '',
		'old_W_mm': '', 'old_H_mm': '', 'old_R_mm': '', 'old_D_mm': '',
		'new_W_mm': '', 'new_H_mm': '', 'new_R_mm': '', 'new_D_mm': '',
		'res_W_mm': '', 'res_H_mm': '', 'res_R_mm': '', 'res_D_mm': '', 'res_C_mm': '',
		'speed': '', 'res_V': '', 'diff_V': ''
	};
	if (typeof(calcObj) != 'object') {
		calcObj = {
			W: '',
			H: '',
			R: ''
		}
	}
	$.each(_$, function (key, val) {
		_$[key] = $('#' + key);
	});

	var defSpeed = (_$.speed.val() | 0 == 0) ? 90 : _$.speed.val();

	$calc.on('change', 'input', function (e) {
		do_calc();
	});
	$calc.on('click', '.form_reset_button', function (e) {
		e.preventDefault();
		$calc[0].reset();
		$jslider.val(defSpeed);
		do_calc();
	});
	// osobennost rabotyi s float
	// http://habrahabr.ru/post/112953/
	var calc_fixed_res = function ($a, $b) {
		return ((
		(+$a * 100)
		- (+$b * 100)
		) / 100).toFixed(2)
	};
	// osobennost rabotyi s float
	// http://habrahabr.ru/post/112953/
	var calc_fixed_sum = function ($a, $b) {
		return ((
		(+$a * 100)
		+ (+$b * 100)
		) / 100).toFixed(2)
	};
	var do_calc = function () {
		_$.old_W_mm.text(_$.old_W.val());
		_$.new_W_mm.text(_$.new_W.val());
		_$.res_W_mm.text(_$.new_W.val() - _$.old_W.val());
		_$.old_H_mm.text((_$.old_W.val() * _$.old_H.val() / 100).toFixed(2));
		_$.new_H_mm.text((_$.new_W.val() * _$.new_H.val() / 100).toFixed(2));
		_$.res_H_mm.text(calc_fixed_res(_$.new_H_mm.text(), _$.old_H_mm.text()));
		_$.old_R_mm.text((25.4 * _$.old_R.val()).toFixed(2));
		_$.new_R_mm.text((25.4 * _$.new_R.val()).toFixed(2));
		_$.res_R_mm.text(calc_fixed_res(_$.new_R_mm.text(), _$.old_R_mm.text()));
		_$.old_D_mm.text(calc_fixed_sum(_$.old_R_mm.text(), 2 * _$.old_H_mm.text()));
		_$.new_D_mm.text(calc_fixed_sum(_$.new_R_mm.text(), 2 * _$.new_H_mm.text()));
		_$.res_D_mm.text(calc_fixed_res(_$.new_D_mm.text(), _$.old_D_mm.text()));
		_$.res_C_mm.text((_$.res_D_mm.text() / 2).toFixed(2));
		if ((_$.speed.val() | 0) == 0) {
			_$.speed.val(defSpeed);
		}
		_$.res_V.text((_$.new_D_mm.text() / _$.old_D_mm.text() * _$.speed.val()).toFixed(2));
		_$.diff_V.text((_$.res_V.text() - _$.speed.val()).toFixed(2));


	};

	var $sectionTyres = $('#sectionTyres');
	var processFilter = function () {
		var arFilter = [];
		if (calcObj.W.hasOwnProperty(_$.new_W.val())) {
			arFilter.push(calcObj.W[_$.new_W.val()]);
		}
		if (calcObj.H.hasOwnProperty(_$.new_H.val())) {
			arFilter.push(calcObj.H[_$.new_H.val()]);
		}
		if (calcObj.R.hasOwnProperty(_$.new_R.val())) {
			arFilter.push(calcObj.R[_$.new_R.val()]);
		}
		var filter = "?" + arFilter.join('&') + '&set_filter=Y';
		$sectionTyres.attr('href', $sectionTyres.data('href') + filter);
	};

	_$.new_W.on('change', function () {
		processFilter();
	});
	_$.new_H.on('change', function () {
		processFilter();
	});
	_$.new_R.on('change', function () {
		processFilter();
	});

	processFilter();
	do_calc();

// кнопка отправить
$('#sectionTyres').on('click', function (event) {
	event.preventDefault();
	skipSelectedParams();

	console.log('--- click ---');

	var new_W = $('[name="new_W"]').val();
	var new_H = $('[name="new_H"]').val();
	var new_D = $('[name="new_D"]').val();

	var type_W = 'SHIRINA_PROFILA', 
		type_H ='VISOTA_PROFILA',
		type_D = 'DIAMETR';

	var elems = $('#tires_by_param input');
	setTyreParam(new_W,type_W,elems);
	setTyreParam(new_H,type_H,elems);
	setTyreParam(new_D,type_D,elems);


	$('#set_filter').trigger("click");
});

var setTyreParam = function(tyre_param, type, elems){
	console.log('--- setTyreParam ---');
	if(type == 'DIAMETR'){
				tyre_param = 'R'+tyre_param;
	}

	$.each(elems, function(index, value){
		let element = $('#'+value.id + '-styler').siblings('.text');

		if(element.data('property') == type){

			if(type == 'SHIRINA_PROFILA'){
			console.log(element.text());
			console.log(tyre_param);
			    console.log(value.id);
			    console.log(element.data('property'));
			}

		    if(tyre_param == element.text()){
		    	//if($('#'+value.id).prop('checked')==false){
		    		$('#'+value.id).prop('checked', true);
         	 		$('#'+value.id+'-styler').addClass('checked');

		    	//}



		    }
		}


	});
} 

var skipSelectedParams = function(){
 	console.log('--- skipSelected ---');
 	var elems = $('#tires_by_param input');
  	$.each(elems, function(index, value){
			// console.log(value.id);
			$('#'+value.id).prop('checked', false);
			$('#'+value.id+'-styler').removeClass('checked');
 	});
}
 
})(jQuery);