(typeof(jQuery) != 'undefined')
&& (function ($) {

	var updateBasket = function (addData) {
		$basketContainer.setAjaxLoading();
		if (typeof(addData) == 'undefined') {
			addData = [];
		}
		var data = [
			{name: "arParams", value: $basketContainer.data('arparams')},
			{name: "template", value: $basketContainer.data('template').toString()},
			{name: "URL", value: $basketContainer.data('url')}
		];
		$.merge(data, addData);
		return $.ajax({
			type: "POST",
			url: rz.AJAX_DIR + 'cart.php',
			data: data,
			success: function (html) {
				$basketContainer.html(html);
				$basketContainer.reStyler();
                $basketContainer.refreshForm();
				$basketContainer.reSpin();
				$basketContainer.stopAjaxLoading();
				updateBasketAjax();
			}
		});
	};
	var changeTimer = null;
	var $basketContainer = $('#order_block_step_1');
	$basketContainer.on('click', '#basket_items .delete_link', function (e) {
		var $this = $(this);
		var data = [
			{name: 'BasketRefresh', value: 'Y'},
			{name: 'action', value: 'delete'},
			{name: 'id', value: $this.data('id')}
		];
		updateBasket(data);
		$(document).trigger('cartLine_Refresh');
	});
	$basketContainer.on('change', '#basket_items .js-touchspin', function (e) {
		e.preventDefault();
		if (changeTimer != null) {
			clearTimeout(changeTimer);
		}
		changeTimer = setTimeout(function () {
			var data = $basketContainer.find('.basket_order_form').serializeArray();
			data.push({name: 'BasketRefresh', value: 'Y'});
			updateBasket(data);
			$(document).trigger('cartLine_Refresh');
		}, 700);

	});
	
	updateBasketAjax();
	function updateBasketAjax()
	{
		var initialCost = $('tr.total-of-total .price').text().replace(/\D+/, '');
		$('.delivery_select input').on('change', function () {
			setDelivery(initialCost);
		});
		setDelivery(initialCost);
		var errors = $('div.errortext');
		var text = '';
		for (var i = 0; i < errors.length; i++) {
			text += errors.eq(i).text() + "<br><br>";
		}
	}	
	
	function setDelivery(initialCost) {
		var deliveryCost = $('.delivery_select').find('input:checked').attr('placeholder');
		if (deliveryCost > 0) {
			var $tr = $('tr.ys-delivery');
			$tr.css('display', 'table-row');
			var $deliveryTag = $tr.find("span.price");
			if (/\d+/.test($deliveryTag.text())) {
				$deliveryTag.html(deliveryCost);
				$('tr.total-of-total .price').html(parseFloat(initialCost, 10) + parseFloat(deliveryCost, 10));
			}
		} else {
			$('tr.ys-delivery').hide();

			//var newCost = $('.ys-sum').find("strong").html().replace(/\d+/, initialCost);
			//$('.ys-sum').find("strong").replaceWith('<strong>'+newCost+'</strong>');
			$('tr.total-of-total .price').html(initialCost);
		}
	}

})(jQuery);