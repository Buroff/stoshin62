<?
$MESS["RZ_VASHA_KORZINA_PUSTA"] = "Ваша корзина пуста";
$MESS["RZ_TCENA_ZA_ED_"] = "Цена за ед.";
$MESS["RZ_KOLICHESTVO"] = "Количество";
$MESS["RZ_TOVAR"] = "Товар";
$MESS["RZ_TOVAR"] = "товар";
$MESS["SALE_NO_ITEMS"] = "В вашей корзине ещё нет товаров.";
$MESS["RZ_COUNT_ITEMS_TITLE"] = "Вы выбрали <span class=\"brand-primary-color\">#NUM#</span> на сумму ";
$MESS["RZ_SUMM_TITLE"] = "Итого: ";
$MESS["RZ_GET_ORDER"] = "Оформить заказ";
$MESS["RZ_SET_ASIDE"] = "отложить";
$MESS["RZ_REMOVE"] = "удалить";
