<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
use Yenisite\Core\Ajax;
use Yenisite\Core\Tools;
use \Yenisite\Core\Catalog;

include $_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH.'/include/debug_info.php';
include_once $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/tools/include_module.php';
global $rz_options;

$arSort = Catalog::getSort($arParams,NULL);
$count = Catalog::getCount();
$id = 'wish-list';

$arParams['PERSONAL_FAVORITE_CONT'] = $id;
$arCatalogParams = Ajax::getParams('bitrix:catalog', 'main_catalog', '', SITE_ID);
global ${$arParams['FILTER_NAME']};

if (empty(${$arParams['FILTER_NAME']})) {
    echo '<h2>',GetMessage("RZ_CT_BCSE_NOT_FOUND"),'</h2>';
    return;
}
$isAjax = Tools::isAjax();
if (!$isAjax) {
Ajax::saveParams($this, $arParams, 'personal_favorite', SITE_ID);
?>
<div <?Ajax::printAjaxDataAttr($this, 'personal_favorite') ?> id="<?=$id?>">
    <?}?>
        <?$APPLICATION->IncludeComponent(
            "bitrix:catalog.section",
            $arParams['CATALOG_TEMPLATE'],
            array(
                "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                "ELEMENT_SORT_FIELD" => $arSort['BY'],
                "ELEMENT_SORT_ORDER" => $arSort['ORDER'],
                "ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
                "ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
                "PAGE_ELEMENT_COUNT" => '99999999',
                "LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
                "PROPERTY_CODE" => $arParams["PROPERTY_CODE"],

                "OFFERS_CART_PROPERTIES" => $arResult['CATALOG_PARAMS']["OFFERS_CART_PROPERTIES"],
                "OFFERS_FIELD_CODE" => $arResult['CATALOG_PARAMS']["LIST_OFFERS_FIELD_CODE"],
                "OFFERS_PROPERTY_CODE" => $arResult['CATALOG_PARAMS']["LIST_OFFERS_PROPERTY_CODE"],
                "OFFERS_SORT_FIELD" => $arResult['CATALOG_PARAMS']["OFFERS_SORT_FIELD"],
                "OFFERS_SORT_ORDER" => $arResult['CATALOG_PARAMS']["OFFERS_SORT_ORDER"],
                "OFFERS_SORT_FIELD2" => $arResult['CATALOG_PARAMS']["OFFERS_SORT_FIELD2"],
                "OFFERS_SORT_ORDER2" => $arResult['CATALOG_PARAMS']["OFFERS_SORT_ORDER2"],
                "OFFERS_LIMIT" => $arResult['CATALOG_PARAMS']["LIST_OFFERS_LIMIT"],
                'OFFER_ADD_PICT_PROP' => $arResult['CATALOG_PARAMS']['OFFER_ADD_PICT_PROP'],
                'OFFER_TREE_PROPS' => $arResult['CATALOG_PARAMS']['OFFER_TREE_PROPS'],
                "OFFERS_PROPERTY_CODE" => array_merge($arResult['CATALOG_PARAMS']["LIST_OFFERS_PROPERTY_CODE"],$arResult['CATALOG_PARAMS']['OFFER_TREE_PROPS']),

                "SECTION_URL" => $arParams["SECTION_URL"],
                "DETAIL_URL" => $arParams["DETAIL_URL"],
                "BASKET_URL" => $arParams["BASKET_URL"],
                "ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
                "PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
                "PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
                "PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
                "SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
                "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                "CACHE_TIME" => $arParams["CACHE_TIME"],
                "DISPLAY_COMPARE" => $arParams["DISPLAY_COMPARE"],
                "PRICE_CODE" => $arParams["PRICE_CODE"],
                "USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
                "SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
                "PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
                "PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],
                "USE_PRODUCT_QUANTITY" => $arParams["USE_PRODUCT_QUANTITY"],
                "CONVERT_CURRENCY" => $arParams["CONVERT_CURRENCY"],
                "CURRENCY_ID" => $arParams["CURRENCY_ID"],
                "HIDE_NOT_AVAILABLE" => $arParams["HIDE_NOT_AVAILABLE"],
                "DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
                "DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
                "PAGER_TITLE" => $arParams["PAGER_TITLE"],
                "PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
                "PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
                "PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
                "PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
                "PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
                "FILTER_NAME" => $arParams['FILTER_NAME'],
                "SECTION_ID" => '',
                "SECTION_CODE" => "",
                "SECTION_USER_FIELDS" => array(),
                "INCLUDE_SUBSECTIONS" => "Y",
                "SHOW_ALL_WO_SECTION" => "Y",
                "META_KEYWORDS" => "",
                "META_DESCRIPTION" => "",
                "BROWSER_TITLE" => "",
                "ADD_SECTIONS_CHAIN" => "N",
                "SET_TITLE" => "N",
                "SET_STATUS_404" => "N",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "N",
                'VIEW_MODE' => Catalog::getViewMode(),
                'BY_LINK' => 'Y',
                'RESIZER_PRODUCT' => $arParams['RESIZER_PRODUCT'],
                'CLASS_ADD' => 'in_fav',
                'FAVORITE' => true,
                'PERSONAL_PAGE' => true,
                'SHOW_CATCH_BUY' => $rz_options['show_catchbuy_in_section'] != 'N',
                'USE_STORE' => $rz_options['show_stores_in_section'],
                'USE_ONE_CLICK' => $rz_options['show_one_click_in_section'],
                'ONECLICK_PERSON_TYPE_ID' => $arCatalogParams["ONECLICK_PERSON_TYPE_ID"],
                'ONECLICK_SHOW_FIELDS' => $arCatalogParams["ONECLICK_SHOW_FIELDS"],
                'ONECLICK_REQ_FIELDS' => $arCatalogParams["ONECLICK_REQ_FIELDS"],
                'ONECLICK_ALLOW_AUTO_REGISTER' => $arCatalogParams["ONECLICK_ALLOW_AUTO_REGISTER"],
                'ONECLICK_USE_CAPTCHA' => $arCatalogParams["ONECLICK_USE_CAPTCHA"],
                'ONECLICK_MESSAGE_OK' => $arCatalogParams["ONECLICK_MESSAGE_OK"],
                'ONECLICK_PAY_SYSTEM_ID' => $arCatalogParams["ONECLICK_PAY_SYSTEM_ID"],
                'ONECLICK_DELIVERY_ID' => $arCatalogParams["ONECLICK_DELIVERY_ID"],
                'ONECLICK_AS_EMAIL' => $arCatalogParams["ONECLICK_AS_EMAIL"],
                'ONECLICK_AS_NAME' => $arCatalogParams["ONECLICK_AS_NAME"],
                'ONECLICK_SEND_REGISTER_EMAIL' => $arCatalogParams["ONECLICK_SEND_REGISTER_EMAIL"],
                'ONECLICK_FIELD_PLACEHOLDER' => $arCatalogParams["ONECLICK_FIELD_PLACEHOLDER"],
                'ONECLICK_FIELD_QUANTITY' => $arCatalogParams["ONECLICK_FIELD_QUANTITY"],
            ),
            $arResult["THEME_COMPONENT"]
        );
        ?>
            <?if (!$isAjax):?>
        </div>
<?endif?>
