<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use \Yenisite\Core\Tools;
include_once $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/tools/include_module.php';
$isAjax = Tools::isAjax();
\Bitrix\Main\Localization\Loc::loadMessages($_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH . '/lang/'.LANGUAGE_ID.'/header.php');
$pathToRules = COption::GetOptionString(CRZShinmarketSettings::getModuleId(),'path_tu_rules_privacy',SITE_DIR.'personal/rules/personal_data.php');
$rand = $this->randString();
?>
<?if (!$isAjax && method_exists($this, 'setFrameMode')) $this->setFrameMode(true); ?>
<?if (!$isAjax):?>
<div class="modal fade" id="modal-complain" tabindex="-1" role="dialog" aria-labelledby="complain_modalLabel" aria-hidden="true">
	<div class="popup modal-dialog popup_complain" id="price_abuse_modalForm" data-arparams='<?=Tools::GetEncodedArParams($arParams)?>' data-template='<?=$templateName?>'>
<?endif;?>
		<span class="close flaticon-delete30" data-dismiss="modal"></span>
		<? if ($arResult['SUCCESS'] === TRUE): ?>
			<? if (!empty($arResult['SUCCESS_TEXT'])): ?>
				<div class="form-group">
					<div class="message message_success">
						<?= $arResult['SUCCESS_TEXT']; ?>
					</div>
				</div>
			<? endif; ?>
		<? else: ?>
			<h2 id="complain_modalLabel"><?=$arParams['TITLE']?></h2>
			<? if (!empty($arResult['ERROR'])): ?>
				<div class="form-group">
					<div class="message message_error">
						<?= $arResult['ERROR']?>
					</div>
				</div>
			<? endif; ?>
			<form class="form-horizontal form-complain modal-form" role="form" enctype="multipart/form-data">
                <input type="hidden" name="privacy_policy" value="N"/>
                <? foreach ($arResult['FIELDS'] as $arItem): ?>
					<? if ($arItem['PROPERTY_TYPE'] == 'E'): ?>
						<?= $arItem['HTML']; ?>
					<? endif; ?>
				<? endforeach; ?>
				<? foreach ($arResult['FIELDS'] as $arItem): ?>
					<? if(strtoupper($arItem['CODE']) == 'EMAIL'):?>
						<div class="form-group email_wrapper required">
							<label for="price_abuse_<?=$arItem['CODE']?>" class="control-label"><?= $arItem['NAME']; ?></label>
							<div class="control-wrapper">
								<?=$arItem['HTML']?>
							</div>
						</div>
					<? elseif(strtoupper($arItem['CODE']) == 'NEED_PRICE'):?>
						<div class="form-group want_price required">
							<span>
								<span class="control-label-span"><?=GetMessage("RZ_YA_HOCHU_TCENU")?></span>
								<input id="price" class="form-control form-control-small" type="text" title=""
									   name="<?=$arResult['CODE'] . "[" . $arItem['CODE'] . "]"?>">
								<span class="b-rub"><?=GetMessage('SHIN_RUB_SYMBOL')?></span>
							</span>
							<div class="slider_wrapper">
								<span class="price_slader__point from">0</span>
								<div class="js-price-slider price_slider range_slider single" data-max="<?=$arParams['MIN_PRICE']?>"></div>
								<span class="price_slader__point to"><?=$arParams['MIN_PRICE']?> <span class="b-rub"><?=GetMessage('SHIN_RUB_SYMBOL')?></span></span>
							</div>
						</div>
					<? elseif(strtoupper($arItem['CODE']) == 'HREF'): ?>
						<div class="form-group cheaper_link required">
							<label for="price_abuse_<?=$arItem['CODE']?>" class="control-label full-width"><?=GetMessage("RZ_VVEDITE_SSILKU_NA_TOVAR_V_MAGAZINE")?>:</label>
							<div class="control-wrapper">
								<?= $arItem['HTML']; ?>
							</div>
						</div>
					<? else:?>
						<? if (!empty($arItem['HTML']) && $arItem['PROPERTY_TYPE'] != 'E'): ?>
							<div class='form-group<?= ($arItem['IS_REQUIRED'] == 'Y') ? ' required' : ''; ?>'>
								<label for="price_abuse_<?= $arItem['CODE'] ?>" class="control-label"><?= $arItem['NAME']; ?>:</label>

								<div class="control-wrapper">
									<?= $arItem['HTML']; ?>
								</div>
							</div>
						<? endif; ?>
					<? endif?>
				<? endforeach; ?>
				<? if ($arParams['TEXT_SHOW'] == 'Y'): ?>
					<div class='form-group<?= ($arParams['TEXT_REQUIRED'] == 'Y') ? ' required' : '' ?>'>
						<label for="popup_call_text" class="control-label"><?= GetMessage("MESSAGE"); ?>:</label>
						<textarea id="popup_call_text" class="form-control"
								  name='<?= $arResult['CODE']; ?>[text]'><?= $arResult['DATA']['text']; ?></textarea>
					</div>
				<? endif; ?>
				<? if (!empty($arResult["CAPTCHA_CODE"])): ?>
					<div class="form-group">
						<img alt="<?= GetMessage("CAPTCHA_ALT") ?>"
							 src="/bitrix/tools/captcha.php?captcha_code=<?= $arResult["CAPTCHA_CODE"] ?>"/>
						<input type="hidden" name="captcha_code" value="<?= $arResult["CAPTCHA_CODE"] ?>"/>
					</div>
					<div class="form-group required">
						<label for="popup_call_captcha" class="control-label"><?= GetMessage("CAPTCHA_TITLE") ?>:</label>
						<input id='popup_call_captcha' class="form-control" type="text" name="captcha_word"/>
					</div>
				<? endif; ?>
                <div class="form-group agreement-policy">
                    <div class="forms-wrapper">
                        <input id="privacy_policy_<?=$rand?>" required type="checkbox" name="privacy_policy" value="Y" <?=$_REQUEST['privacy_policy'] == 'Y' ? ' checked' : ''?>>
                        <label for="privacy_policy_<?=$rand?>">
                            <?= GetMessage('RZ_ACCEPT') ?> <?=GetMessage('RZ_CONCLUSION',array("#URL#" => Tools::GetConstantUrl($pathToRules)))?>
                        </label>
                    </div>
                </div>
				<div class="form-group required_note"><span class="star">*</span><?= GetMessage("REQUIRED"); ?></div>
				<div class="form-group submit">
					<button type="submit" class="btn btn-primary btn-submit-follow_price"><?= GetMessage("RZ_SEND"); ?></button>
				</div>
			</form>
		<?endif; ?>
<?if (!$isAjax):?>
	</div>
</div>
<?endif;?>