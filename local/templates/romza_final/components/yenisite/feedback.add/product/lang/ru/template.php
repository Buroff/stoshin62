<?
$MESS["RZ_NAPISAT__OTZIV"] = "Написать отзыв";
$MESS["RZ_FAJL_NE_VIBRAN"] = "Файл не выбран";
$MESS["RZ_VIBERITE_FAJL"] = "Выберите файл";
$MESS["CAPTCHA_TITLE"] = "Введите код";
$MESS["SECTION_SELECT"] = "Выберите раздел";
$MESS["MESSAGE"] = "Сообщение";
$MESS["SEND"] = "Отправить";
$MESS["ERROR"] = "Ошибка!";
$MESS["REQUIRED"] = " - поля, обязательные для заполнения";
