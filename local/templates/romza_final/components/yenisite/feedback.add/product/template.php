<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Yenisite\Core\Tools;
\Bitrix\Main\Localization\Loc::loadMessages($_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH . '/lang/'.LANGUAGE_ID.'/header.php');
$pathToRules = COption::GetOptionString(CRZShinmarketSettings::getModuleId(),'path_tu_rules_privacy',SITE_DIR.'personal/rules/personal_data.php');
$rand = $this->randString();

include_once $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/tools/include_module.php';
?>
<div class="row">
	<div class="col-md-6 feedbacks_block">
		<h2><?=GetMessage("RZ_NAPISAT__OTZIV")?></h2>
		<form class="modal-form" method="POST" id="guestbook_addForm" name="guestbook" action="<?= $APPLICATION->GetCurPageParam() ?>"
			  data-arparams="<?=Tools::GetEncodedArParams($arParams)?>" data-template="<?=$templateName?>"
			  enctype="multipart/form-data">
			<input type="hidden" name="add" value="ok"/>
            <input type="hidden" name="privacy_policy" value="N"/>
			<?if(!empty($arResult['ERROR'])):?>
				<div class="form-group">
					<div class="message error-message">
						<?=$arResult['ERROR']?>
					</div>
				</div>
			<?endif;?>
<? if ($arResult['SUCCESS'] === TRUE): ?>
	<? if (!empty($arResult['SUCCESS_TEXT'])): ?>
		<?= $arResult['SUCCESS_TEXT']; ?>
		<br/><br/>
	<? endif; ?>
<? else: ?>
			<div class="form-group">
				<? foreach ($arResult['FIELDS'] as $arItem): ?>
					<? if ($arItem['PROPERTY_TYPE'] == 'E'): ?>
						<?= $arItem['HTML']; ?>
					<? endif; ?>
				<? endforeach; ?>
			</div>
			<? if (!empty($arResult['SECTIONS_SELECT'])): ?>
				<?= GetMessage('SECTION_SELECT') . ': ' . $arResult['SECTIONS_SELECT']; ?>
			<? endif; ?>
			<? foreach ($arResult['FIELDS'] as $arItem): ?>
				<? if (!empty($arItem['HTML']) && $arItem['PROPERTY_TYPE'] != 'E' && $arItem['PROPERTY_TYPE'] != 'F'): ?>
					<div class='form-group'>
						<label>
							<?= $arItem['NAME']; ?><?= ($arItem['IS_REQUIRED'] == 'Y') ? '<span style="color: red">*</span>' : ''; ?>
							:
						</label>
						<?= $arItem['HTML']; ?>
					</div>
				<? endif; ?>
				<? if ($arItem['PROPERTY_TYPE'] == 'F'): ?>
					<div class="form-group">
						<?
						$uid = abs(crc32($arResult['CODE']));
						?>
						<label for="fileInput<?= $uid ?>"><?= $arItem['NAME'] ?></label>

						<div>
								<span class="custom_input_file_wrapper">
									<span class="custom_input_file btn btn-primary"><?=GetMessage("RZ_VIBERITE_FAJL")?></span>
									<input type="file" class="custom_input_file_field" data-target="#fileValue<?= $uid ?>"
										   name="<?= $arResult['CODE'] . '[' . $arItem['CODE'] . ']' ?>"
										   id="fileInput<?= $uid ?>">
								</span>
						</div>
						<div id="fileValue<?= $uid ?>"><?=GetMessage("RZ_FAJL_NE_VIBRAN")?></div>
					</div>
				<? endif ?>
			<? endforeach; ?>

			<? if ($arParams['TEXT_SHOW'] == 'Y'): ?>
				<div class='form-group'>
					<label>
						<?= GetMessage("MESSAGE"); ?><?= ($arParams['TEXT_REQUIRED'] == 'Y') ? '<span style="color: red">*</span>' : '' ?>
						:</label>
					<textarea required class="form-control" name='<?= $arResult['CODE']; ?>[text]'><?= $arResult['DATA']['text']; ?></textarea>
				</div>
			<? endif; ?>
			<? if (!empty($arResult["CAPTCHA_CODE"])): ?>
				<div class="form-group">
					<label><?= GetMessage("CAPTCHA_TITLE") ?>:</label>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-sm-3">
							<img alt="<?= GetMessage("CAPTCHA_ALT") ?>" class="img"
								 src="<?= SITE_TEMPLATE_PATH ?>/tools/captcha.php?captcha_sid=<?= $arResult["CAPTCHA_CODE"] ?>"/>
						</div>
						<div class="col-sm-9">
							<input class="form-control col-sm-10" type="text" name="captcha_word"/>
						</div>
					</div>
					<input type="hidden" name="captcha_code" value="<?= $arResult["CAPTCHA_CODE"] ?>"/>
				</div>
			<? endif; ?>
            <div class="form-group agreement-policy">
                <div class="forms-wrapper">
                    <input id="privacy_policy_<?=$rand?>" required type="checkbox" name="privacy_policy" value="Y" <?=$_REQUEST['privacy_policy'] == 'Y' ? ' checked' : ''?>>
                    <label for="privacy_policy_<?=$rand?>">
                        <?= GetMessage('RZ_ACCEPT') ?> <?=GetMessage('RZ_CONCLUSION',array("#URL#" => Tools::GetConstantUrl($pathToRules)))?>
                    </label>
                </div>
            </div>
			<div class="form-group req_block">
				<span style="color: red">*</span>
				<?= GetMessage("REQUIRED"); ?>
			</div>
			<div class="form-group">
				<button class="btn btn-primary" type="submit"><?= GetMessage("SEND") ?></button>
			</div>
<?endif?>
		</form>
	</div>
</div>
