<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

foreach ($arResult['ITEMS'] as &$arItem) {
	$arItem['PREVIEW_TEXT'] = str_replace('<noindex>', '<!--noindex-->', $arItem['PREVIEW_TEXT']);
	$arItem['PREVIEW_TEXT'] = str_replace('</noindex>', '<!--/noindex-->', $arItem['PREVIEW_TEXT']);
}
