<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use \Yenisite\Core\Tools;
include_once $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/tools/include_module.php';
$isAjax = Tools::isAjax();
if (!$isAjax && method_exists($this, 'createFrame')) $frame = $this->createFrame()->begin('');
?>
<div class="feedbacks__list">
	<? if (!empty($arResult['ITEMS'])): ?>
		<? foreach ($arResult['ITEMS'] as $arItem): ?>
			<div class="feedbacks__item">
				<div class="feedbacks__item__username"><?= $arItem['PROPERTY_NAME_VALUE'] ?></div>
				<div class="feedbacks__item__date"><?= ConvertDateTime($arItem["DATE_CREATE"], "DD.MM.YYYY") ?></div>
				<div class="feedbacks__item__text"><?= $arItem['PREVIEW_TEXT'] ?></div>
			</div>
		<? endforeach ?>
	<? else: ?>
		<? if (empty($arResult['SECTIONS'])): ?>
			<?= GetMessage("MESSAGES_EMPTY"); ?>
		<? elseif (!empty($arResult['CUR_SECTION_CODE']) && !empty($arResult['SECTIONS'])): ?>
			<?= GetMessage("MESSAGES_EMPTY"); ?>
		<? endif; ?>
	<? endif; ?>
</div>
<div class='pager-block'>
	<?= $arResult['NAV']; ?>
</div>
<? if (!$isAjax && method_exists($this, 'createFrame')) $frame->end(); ?>