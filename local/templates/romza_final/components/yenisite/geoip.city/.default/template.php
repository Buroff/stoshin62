<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<?if(function_exists('yenisite_GetCompositeLoader')){global $MESS;$MESS ['COMPOSITE_LOADING'] = yenisite_GetCompositeLoader();}?>

<?if(method_exists($this, 'setFrameMode')) $this->setFrameMode(true);?>
<?if(CModule::IncludeModule('statistic')):?>
	<?if(method_exists($this, 'createFrame')) $frame = $this->createFrame()->begin('');?>
	<?$bDeliverTo = false;?>
	<?if (CModule::IncludeModule('yenisite.geoipstore') && $arParams['UNITE_WITH_GEOIPSTORE'] != 'Y'):?>
		<?$bDeliverTo = true;?>
		<span class="sym" style="color: white;"><?=GetMessage("LOC_TO")?></span>
	<?endif?>
	<div class="city_selection_wrapper" <?if($bDeliverTo):?>style="display: inline-block; margin-top: 5px"<?endif?>>
		<input type="hidden" name="city_inline" id="city_inline" value="<?=$arResult['CITY_INLINE'];?>"/>
		<span class="js-city-selection city-selection ys-loc-city" data-toggle="modal" data-target="#ys-locator">
			<span class="flaticon-location4"></span>
			<span class="ys-city-name">
				<?
				if (!empty($arResult['CITY_INLINE'])) {
					echo $arResult['CITY_INLINE'];
				} else {
					echo GetMessage('CHOOSE_CITY');
				}
				?>
			</span>
		</span>
	</div>
	<?$this->SetViewTarget('modal_locator');?>
<div class="modal fade ys-popup" id="ys-locator" tabindex="-1" role="dialog" aria-labelledby="city_modalLabel" aria-hidden="true">
	<div class="popup modal-dialog popup_city_selection">
		<span class="close flaticon-delete30" data-dismiss="modal"></span>
		<h2 id="city_modalLabel">
			<?if(!empty($arResult['CITY_IP'])):?>
				<?=GetMessage('YOUR_CITY').' <span class="ys-city-header">'.$arResult['CITY_IP'].'</span>?';?>
			<?else:?>
				<?=GetMessage('YOUR_CITY').'?';?>
			<?endif;?>
		</h2>
		<form role="form" method="post">
			<div class="form-group location_input_wrapper">
				<span><?=GetMessage('ENTER_YOUR_CITY');?></span>
				<input class="txt ys-city-query form-control" id="location" type="text" placeholder="<?=GetMessage('ENTER_YOUR_CITY');?>">
				<div class="ys-loc-autocomplete"></div>
			</div>
			<div class="popup_city_selection__list_head"><?=GetMessage("RZ_ILI_VIBERITE_IZ_SPISKA")?>:</div>
			<?if(!empty($arResult['CITY_IP']) || !empty($arResult['CITY_INLINE'])):?>
				<?$selected_city =!empty($arResult['CITY_INLINE']) ? $arResult['CITY_INLINE'] : $arResult['CITY_IP'];?>
			<?endif;?>
			<ul class="popup_city_selection__list ys-loc-cities">
				<?if(!empty($selected_city)):?>
					<li class="ys-your-city">
						<a href="#"><span><?=$selected_city;?></span></a>
					</li>
				<?else:?>
					<li class="ys-your-city">
						<a href="#"><span><?=$arResult['CITY'][0];?></span></a>
					</li>
				<?endif;?>
				<li><a href="#"><span><?=$arResult['CITY'][1];?></span></a></li>
				<li><a href="#"><span><?=$arResult['CITY'][2];?></span></a></li>
				<li><a href="#"><span><?=$arResult['CITY'][3];?></span></a></li>
				<li><a href="#"><span><?=$arResult['CITY'][4];?></span></a></li>
				<li><a href="#"><span><?=$arResult['CITY'][5];?></span></a></li>
				<li><a href="#"><span><?=$arResult['CITY'][6];?></span></a></li>
				<li><a href="#"><span><?=$arResult['CITY'][7];?></span></a></li>
				<li><a href="#"><span><?=$arResult['CITY'][8];?></span></a></li>
			</ul>

			<div class="form-group submit ys-my-city" <?if(empty($arResult['CITY_IP'])):?>style="display: none;"<?endif?>>
				<input class='btn btn-primary btn-city-submit button' type="submit" name="submit" value="<?=GetMessage("THIS_IS_MY_CITY")?>">
			</div>
			<input type="hidden" id="ys-COMPONENT_DIRECTORY" value="<?=$componentPath.'/'; //$arResult["COMPONENT_DIRECTORY"]?>" />
			<input type="hidden" id="ys-SITE_ID" value="<?=SITE_ID?>" />
		</form>
	</div>
</div>
	<?$this->EndViewTarget();?>
<?if(method_exists($this, 'createFrame')) $frame->end();?>
<?endif;?>