<?php
/*************************************
 ** @product shinmarket.loc        **
 ** @authors                        **
 **         Morozov P. Artem        **
 ** @license MIT                    **
 ** @mailto tashiro@ya.ru           **
 *************************************/
foreach ($arResult['DISPLAY_PROPERTIES'] as &$arProp) {
	if ($arProp['PROPERTY_TYPE'] == "E" && $arProp['CODE'] = 'SEASON' && !empty($arProp['VALUE'])) {
		/** @noinspection PhpDynamicAsStaticMethodCallInspection */
		$dbElem = CIBlockElement::GetList(array(),array("ID" => $arProp['VALUE']),false,false,array('ID','PROPERTY_ICON_CLASS'));
		$arElem = $dbElem->GetNext();
		$arProp['ICON_CLASS'] = $arElem['PROPERTY_ICON_CLASS_VALUE'];
	}
}
unset($arProp);
