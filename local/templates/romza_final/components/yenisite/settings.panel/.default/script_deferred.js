$(document).ready(function(){
	$('div.block').click(function(){
		$(this).siblings('input[type=hidden]').val($(this).attr('data-value'));
		$(this).addClass('active').siblings().removeClass('active');
	});

	var $body = $(document.body);

	$.fn.serializeAssoc = function () {
		var obj = {
			result: {},
			add: function (name, value) {
				var tmp = name.match(/^(.*)\[([^\]]*)\]$/);
				if (tmp) {
					var v = {};
					if (tmp[2])
						v[tmp[2]] = value;
					else
						v[$.count(v)] = value;
					this.add(tmp[1], v);
				} else if (typeof value == 'object') {
					if (typeof this.result[name] != 'object') {
						this.result[name] = {};
					}
					this.result[name] = $.extend(this.result[name], value);
				} else {
					this.result[name] = value;
				}
			}
		};
		var ar = this.serializeArray();
		for (var i = 0; i < ar.length; i++) {
			obj.add(ar[i].name, ar[i].value);
		}
		return obj.result;
	};
	$body.on('click', '#modal-settings :input[name=settings_apply_button]', function (e) {
		var $setPanel = $('#modal-settings form');
		e.preventDefault();
		var arSettings = $setPanel.serializeAssoc();
		arSettings['settings_apply'] = 'Y';
		$.ajax({
			type: 'POST',
			url: rz.AJAX_DIR + 'composite.php',
			data: arSettings,
			dataType: 'json',
			success: function (obj) {
				$setPanel.submit();
			}
		});
	});
});