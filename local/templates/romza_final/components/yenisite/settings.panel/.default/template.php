<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?// if show settings panel - off composite
$frame = $this->createFrame()->begin('');

if (count($arParams["EDIT_SETTINGS"]) == 0) {
	$frame->end();
	return;
}
include "functions.php";
?>
<div class="theme-switch-wrapper">
	<span class="settings_button sets_icon" data-toggle="modal" data-target="#modal-settings"></span>
</div>
	<div class="modal fade" id="modal-settings" tabindex="-1" role="dialog" aria-labelledby="settings_modalLabel" aria-hidden="true">
		<div class="container">
			<div class="popup modal-dialog popup_settings">
				<span class="close flaticon-delete30" data-dismiss="modal"></span>
				<h2 id="settings_modalLabel"><?= GetMessage("SETTINGS") ?>:</h2>

				<form id='settings-form' method="post" action="?settings">
					<?foreach($arResult['GROUPS'] as $groupID => $arItem):?>
						<?
						foreach ($arItem['SETTINGS'] as $key => $val) {
							if (!in_array($val, $arParams["EDIT_SETTINGS"])) {
								unset($arItem['SETTINGS'][$key]);
							};
						}?>
						<?foreach ($arItem['SETTINGS'] as $key => $val):?>
							<?showSettingsItem($arResult['SETTINGS'][$val], $arResult['CURRENT_SETTINGS'][$val],$templateFolder); ?>
						<?endforeach?>
					<?endforeach?>
						<? if ($USER->IsAdmin()): ?>
							<input type="checkbox" name='SETTINGS[SET_DEFAULT]' id="SETTINGS[SET_DEFAULT]" value='Y' class="checkbox-styled">
							<label for="SETTINGS[SET_DEFAULT]"><?= GetMessage("SET_DEFAULT") ?></label>
						<? endif ?>
						<input type="hidden" name="settings_apply" value="Y">
						<button class="sets_icon submit_settings" name="settings_apply_button" type="submit" value='Y'></button>
				</form>
			</div>
		</div>
	</div>