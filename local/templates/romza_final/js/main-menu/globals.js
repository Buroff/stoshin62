var $body, $doc = $(document), $win = $(window),
	isWindowLoaded = false, isDocumentReady = false,
	winScrollTop = $win.scrollTop(), $btnUp,
	resizeHandlers = [], resizeTimeout, s = {};
$(function($){
if (typeof rmz == 'undefined') rmz = {}; // it can be already defined in inline theme switch in head

// process all "on resize" functions in one place with delay to ensure
// that resizing is ended.
// How to use: every time you need to do something on resize,
// make this "something" a named function, and then add it like this:
// resizeHandlers.push(something);
// And that's it. You're done.

function resizeDelay(){
	if (resizeHandlers.length === 0) return;
	clearTimeout(resizeTimeout);
	resizeTimeout = setTimeout(function(){
		for (var i = 0; i < resizeHandlers.length; i++){
			if (typeof resizeHandlers[i] === 'function'){
				resizeHandlers[i].call();
			}
		}
		$('.page_aside__filter .jq-selectbox-multy__dropdown').each(function() {
			$(this).css('display', '').jScrollPane();
		});
	}, 300);
}
	$win.on('resize', resizeDelay)
	.on('load', function(){
		$('.page_aside__filter .jq-selectbox-multy__dropdown').jScrollPane();
		isWindowLoaded = true;
	});


	$(document).ready(function(){
		$body = $('body');
		$btnUp = $('.btn-up');
		isDocumentReady = true;

		$('[data-toggle="tooltip"], [data-tooltip]').tooltip();

		if ( $('.catalog-description').length ) {
			var _categ_desc = $('.catalog-description');

			if(!_categ_desc.hasClass('not-full')) {
				_categ_desc.heightControl().removeClass('minified').css('max-height', _categ_desc.get(0).scrollHeight + 100);
				// _categ_desc.find('.height-toggle span').html(BX.message('COLLAPSED'));
			} else{
				_categ_desc.heightControl().css('max-height', _categ_desc.get(0).scrollHeight);
			}
		}

		$body.on('click', '.addable', function() {
			$(this).toggleClass("active");
		});
	});
})