var defaultSly = {
	horizontal: true,

	// Item based navigation
    itemNav:        'basic',  // Item navigation type. Can be: 'basic', 'centered', 'forceCentered'.
    smart:          false, // Repositions the activated item to help with further navigation.
    activateOn:     'click',  // Activate an item on this event. Can be: 'click', 'mouseenter', ...
    activateMiddle: false, // Always activate the item in the middle of the FRAME. forceCentered only.
    mouseDragging:  true,

    touchDragging: true, // Enable navigation by dragging the SLIDEE with touch events.
    swingSpeed:    0.1,
	elasticBounds: true,

    // Pagesbar
    pagesBar:       null, // Selector or DOM element for pages bar container.
    activatePageOn: 'click', // Event used to activate page. Can be: click, mouseenter, ...
    pageBuilder:          // Page item generator.
        function (index) {
            return '<li>';
        },

    // Mixed options
    speed:         400,       // Animations speed in milliseconds. 0 to disable animations.
    easing:        'swing', // Easing for duration based (tweening) animations.
    startAt:       null,    // Starting offset in pixels or items.
    keyboardNavBy: 'pages'    // Enable keyboard navigation by 'items' or 'pages'.
};

function initHSly(frame) {
    // console.log('init Sly on', frame);
    var $frame = $(frame),
        $pagesWrap = $frame.siblings('.pages-wrap');

    if (!$pagesWrap.length) {
        $pagesWrap = $frame.find('.pages-wrap');
    }

    var $numCurrent = $pagesWrap.find('.num-current'),
        $numTotal = $pagesWrap.find('.num-total');

    function updateNums(slyInstance) {
        if (!$numCurrent.length) return;

        var firstItem = slyInstance.rel.firstItem + 1,
            lastItem = slyInstance.rel.lastItem + 1;
        if (lastItem - firstItem) {
            $numCurrent.html(firstItem + '-' + lastItem);
        } else {
            $numCurrent.html(firstItem);
        }
    }

    if (!$pagesWrap.length) $pagesWrap = $frame.find('.pages-wrap');
    var $prev = $pagesWrap.find('.prevPage'),
        $next = $pagesWrap.find('.nextPage'),
        $pagi = $pagesWrap.find('.pages'),
        slyChanges = {
            itemNav: $frame.hasClass('no-item-frame') ? null : 'basic',
            prevPage: $prev,
            nextPage: $next,
            pagesBar: $pagi
        },
        settings = $.extend({}, defaultSly, slyChanges),
        actions = {
            activePage: function (e, i) {
                updateNums(this);
            },
            load: function () {
                // console.log('loaded h sly');
                $numTotal.length && $numTotal.html(this.items.length);
                if (this.pos.start === this.pos.end) $frame.addClass('no-scroll');
                else $frame.removeClass('no-scroll');

                updateNums(this);
                $frame.data('sly', this);
            }
        };

    $frame.sly(settings, actions);
    function update() {
        $frame.sly('reload');
    }

    resizeHandlers.push(update);
}

function initVSly(frame) {
    var $frame = $(frame),
        $prev = $frame.siblings('.up'),
        $next = $frame.siblings('.down'),
        bp = $frame.data('breakpoint') || 0;
    horizontal = Modernizr.mq('(max-width: ' + bp + 'px)');

    function init() {
        $frame.sly({
            horizontal: horizontal,
            // Item based navigation
            itemNav: 'basic',  // Item navigation type. Can be: 'basic', 'centered', 'forceCentered'.
            smart: true, // Repositions the activated item to help with further navigation.
            activateOn: 'click',  // Activate an item on this event. Can be: 'click', 'mouseenter', ...

            mouseDragging: true,
            touchDragging: true,
            elasticBounds: true,
            swingSpeed: 0.1,
            releaseSwing: true,

            // Navigation buttons
            prevPage: $prev, // Selector or DOM element for "previous page" button.
            nextPage: $next, // Selector or DOM element for "next page" button.

            // Mixed options
            speed: 400,       // Animations speed in milliseconds. 0 to disable animations.
            keyboardNavBy: 'items',    // Enable keyboard navigation by 'items' or 'pages'.
        }, {
            load: function () {
                // console.log('loaded v sly');
            },
        });
    }

    function update() {
        horizontal = Modernizr.mq('(max-width: ' + bp + 'px)');
        var sly = $frame.data('sly');

        if (sly && sly.initialized) {
            if (sly.options.horizontal !== horizontal) {
                sly.destroy();
                init();
            } else sly.reload();
        } else {
            init();
        }
    }

    update();

    // if ($frame.parent().is('.modal-thumbs')){
    //     $('#modal-gallery').on('shown.bs.modal', function(){
    //         update();
    //     });
    // }

    // var resizeTimeout;
    // $(window).on('resize', function(){
    //     clearTimeout(resizeTimeout);
    //     resizeTimeout = setTimeout(update, 300);
    // });
}

function initReviewsCarousel(frame) {
    $(frame).on('click', '.action-link', function (e) {
        $(this).closest('.review-text__wrap').toggleClass('opened');
        return false;
    });
    function updateReviewsCarousel() {
        var $frame = $(frame),
            sly = $frame.data('sly');

        if (Modernizr.mq('(max-width: 767px)')) {
            if (!sly) $frame.sly(defaultSly);
            else $frame.sly('reload');
        } else {
            sly && sly.initialized && $frame.sly('destroy');
        }

        $frame.find('.review-text__wrap').each(function () {
            var $wrap = $(this).removeClass('long-text opened'),
                $text = $wrap.find('.review-text');
            if ($text.height() > 160) $wrap.addClass('long-text');
        })
    }

    updateReviewsCarousel();
    resizeHandlers.push(updateReviewsCarousel);
}
    function initSly(frame) {
        var $frame = $(frame);
        $frame.each(function () {
            var $t = $(this),
                $slidee = $t.find('.slidee'),
                from = parseInt($t.data('sly-from')) || 0,
                to = parseInt($t.data('sly-to')) || null,
                $prev = $t.find('.prevPage'),
                $next = $t.find('.nextPage'),
                $dots = $t.find('.pages');

            function updateSly(sly) {
                if (!sly) {
                    var slyChanges = {
                            prevPage: $prev,
                            nextPage: $next,
                            pagesBar: $dots
                        },
                        settings = $.extend({}, defaultSly, slyChanges);
                    sly = new Sly($t.get(0), settings).init();
                    $t.data('sly', sly);
                } else $t.sly('reload');
            }

            function update() {
                var sly = $t.data('sly');
                if (to !== null) {
                    if (Modernizr.mq('(min-width:' + from + 'px) and \
                        (max-width: ' + to + 'px)')) {
                        updateSly(sly);
                    } else {
                        sly && sly.initialized && $t.sly('destroy');
                    }
                } else if (Modernizr.mq('(min-width:' + from + 'px')) {
                    updateSly(sly);
                } else {
                    sly && sly.initialized && $t.sly('destroy');
                }
            }

            $slidee.find('img').each(function () {
                $(this).one('load', update);
            })

            update();
            resizeHandlers.push(update);
        });
    }
