// for switching color themes
$(document).ready(function(){
	var themeInputs = $('[name="colorScheme"]');
	themeInputs.prop('checked', false);
	themeInputs.filter('[value='+rmz.theme+']').prop('checked', true);
	$('body').find('[data-theme]').attr('data-theme', rmz.theme);
	$('#current-colot-theme').find('img').attr('src', 'images/i/' + rmz.theme + '.jpg');

	// color-theme switch
	themeInputs.on('change', function(){
		console.log( 'sdf' );
		var newTheme = $(this).val();
		if (rmz.theme !== newTheme){
			rmz.theme = newTheme;

			rmz.themeLink.setAttribute('href', 'css/themes/' + rmz.theme + '.min.css');
			rmz.themeLink.setAttribute('data-theme', rmz.theme);

			$('body').find('[data-theme]').attr('data-theme', rmz.theme);
			$('#current-colot-theme').find('img').attr('src', 'images/i/' + rmz.theme + '.jpg');
			localStorage.setItem('rmz_theme', rmz.theme);
		} else return;
	});
})