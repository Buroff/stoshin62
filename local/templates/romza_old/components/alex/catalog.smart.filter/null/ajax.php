<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use \Yenisite\Core\Tools;
use \Yenisite\Shinmarket\ManagerCalc;

unset($arResult["COMBO"]);
if (CRZShinmarketSettings::isTiersSolution() && ManagerCalc::tablesExist()){
    $arResult['VENDORS'] = ManagerCalc::processOnlyVendors();
    $arOptionsBDIds = CRZShinmarket::getPropsIDOfTiersWheels();
    $carId = htmlspecialcharsbx($_REQUEST['car_id']);
    if (!empty($carId)){
        $arResult['OTHERS_PROPS']['TIERS_SIZES'] = ManagerCalc::getTiersByIDModel($carId);
        $arResult['OTHERS_PROPS']['WHEELS_SIZES'] = ManagerCalc::getWheelsByIDModel($carId);
    } else{
        $arResult['OTHERS_PROPS'] = ManagerCalc::processItemsByVendor($arResult['VENDORS'][0]);
    }
    $arTXProps = $arResult['OTHERS_PROPS']['MODELS'][0] ? $arResult['OTHERS_PROPS']['MODELS'][0] : $arResult['OTHERS_PROPS'];
    CRZShinmarket::setValuesForTXProps($arTXProps,$arOptionsBDIds);
    CRZShinmarket::filterValuesOfFilterByTXProps($arResult,$arOptionsBDIds);
}

$curEnc = Tools::getLogicalEncoding();
if( $curEnc != 'utf-8') {

	$arResult = Tools::getEncodeArrayWithKeys($arResult,$curEnc, 'utf-8');
}
global $APPLICATION;
$APPLICATION->RestartBuffer();
// $result = implode(",", $arResult);
// echo($result);
echo json_encode($arResult);
die();