(typeof(jQuery) != 'undefined')
&& (function ($) {
	var $diskRangeSlider = $(".js-price-range-slider");
	var elementNums = false;

	$diskRangeSlider.each(function () {
		var $this = $(this),
			$parent = $this.closest('.seach_form__inner_price'),
			$priceTo = $parent.find('.price_to:eq(0)'),
			$priceFrom = $parent.find('.price_from:eq(0)'),
			priceMinVal = Math.floor(parseFloat($priceFrom.data('val'))),
			priceMaxVal = Math.ceil(parseFloat($priceTo.data('val'))),
			priceToVal = $priceTo.val(),
			priceFromVal = $priceFrom.val();
		if (priceToVal.length == 0) {
			priceToVal = priceMaxVal;
		}
		if (priceFromVal.length == 0) {
			priceFromVal = priceMinVal;
		} 
		
		$this.noUiSlider({
			start: [priceFromVal, priceToVal],
			connect: true,
			step: 1,
			range: {
				'min': priceMinVal,
				'max': priceMaxVal
			},
			format: wNumb({
				decimals: 0
			})
		});
		$this.on({
			set: function (e, vals) {
				$priceFrom.val('');
				$priceTo.val('');
			},
			change: function (e, vals) {
				if ($priceFrom.val().length == 0 || $priceTo.val().length == 0) {
					$priceFrom.val(vals[0]);
					$priceTo.val(vals[1]);
					$priceTo.trigger('change');
				}
			}
		});

		var triggerSliderInput = function (val) {
			var $this = $(this);
			if ($this.val() != val && $this.val().length > 0) {
			$this.val(val);
				if (sliderTrigger != null) {
					clearTimeout(sliderTrigger);
				}
				sliderTrigger = setTimeout(function () {
					$this.trigger('change');
				}, 700);
			}
		};
		$this.Link('lower').to($priceFrom, function (val) {
			triggerSliderInput.bind(this, val);
		});
		$this.Link('upper').to($priceTo, function (val) {

			triggerSliderInput.bind(this, val);
		});

		$this.Link('lower').to('-inline-<div class="tooltip"></div>', function (value) {
			$(this).html('<span class="tooltip_inner">' + value + '</span>');
		});

		$this.Link('upper').to('-inline-<div class="tooltip"></div>', function (value) {
			$(this).html('<span class="tooltip_inner">' + value + '</span>');
		});
	});

	var $diskSectionFilters = $('.filterHorizontal');


	$diskSectionFilters.on('change', 'input', function (e) {
		var $diskSectionFilter = $(this).closest('form.filterHorizontal');
		var rnd = $diskSectionFilter.data('rnd');
		var data = $diskSectionFilter.serializeArray();
		data.push({name: 'arParams', value: $diskSectionFilter.data('arparams')});
		$diskSectionFilter.setAjaxLoading();
		$.ajax({
			url: rz.AJAX_DIR + "catalog_Filter.php",
			data: data,
			dataType: "json",
			success: function (json) {
				rz.FILTER_AJAX = [];
				$.each(json.ITEMS, function (key, val) {
					$.each(val.VALUES, function (valKey, valVal) {
						diskAjaxValuePrepare(valVal, rnd);
					});
				});
				$diskSectionFilter.stopAjaxLoading();
				$(document).trigger('catalog_applyFilter');
			}
		})
	});

	$diskSectionFilters.on('click', '.form_reset_button', function (e) {
		e.preventDefault();
		var $diskSectionFilter = $(this).closest('form.filterHorizontal');
		$diskSectionFilter[0].reset();
		var rnd = $diskSectionFilter.data('rnd');
		var $jsSlider = $diskSectionFilter.find('.js-price-range-slider');
		var priceToVal = $diskSectionFilter.find('.price_to:eq(0)').val(),
			priceFromVal = $diskSectionFilter.find('.price_from:eq(0)').val();
		$jsSlider.val([priceFromVal,priceToVal]);
		$diskSectionFilter.find('input:visible:eq(0)').trigger('change');

	});

	var diskAjaxValuePrepare = function (val, rnd) {
		var $curItem = $('#' + rnd + val.CONTROL_ID);
		var $itemStyler = $('#' + rnd + val.CONTROL_ID + "-styler");
		var lastsymb = val.CONTROL_NAME.substr(-3);
		var isNum = false;
		if (lastsymb == 'MAX' || lastsymb == "MIN") {
			isNum = true;
		}
		if ('DISABLED' in val && val.DISABLED == 1) {
			if ($itemStyler.length > 0) {
				$itemStyler.addClass('disabled');
			}
			$curItem.attr('disabled', 'disabled');
		} else {
			if ($itemStyler.length > 0) {
				$itemStyler.removeClass('disabled');
			}
			$curItem.removeAttr('disabled');
		}
	};


	// --- событие при изменение данных в списке выбора автомобиля
	var disks_selects = {
		'mark': '[name="disks_makr_filter"]', 
		'model': '[name="disks_model_filter"]',
		'year':'[name="disks_year_filter"]',
		'modif':'[name="disks_modif_filter"]'
	};
	
			
	// var $diskdiskSectionFilters = $('.filterHorizontal');
    $.each(disks_selects, function (key, val) {
        disks_selects[key] = $diskSectionFilters.find(val);
    });
    
	disks_selects['mark'].on('change', function () {
        var vendor = $(this).find('option:selected').text();
        // console.log('--- change ---');
        // console.log(vendor);

        diskAjax('by_vendor',$(this),vendor);
    });
    disks_selects['model'].on('change', function () {
        var $this = $(this),
            vendor = $this.getSelOptFromForm(disks_selects['mark']).text(),
            model = $this.find('option:selected').text();
        diskAjax('by_vendor_model',$(this),vendor,model);
    });
    disks_selects['year'].on('change', function () {
        var $this = $(this),
            vendor = $this.getSelOptFromForm(disks_selects['mark']).text(),
            model = $this.getSelOptFromForm(disks_selects['model']).text(),
            year = $this.find('option:selected').text();
        diskAjax('by_vendor_model_year',$(this),vendor,model,year);
    });
    disks_selects['modif'].on('change', function () {
        var $this = $(this),
            vendor = $this.getSelOptFromForm(disks_selects['mark']).text(),
            model = $this.getSelOptFromForm(disks_selects['model']).text(),
            year = $this.getSelOptFromForm(disks_selects['year']).text(),
            modif = $this.find('option:selected').text();
        diskAjax('by_vendor_model_year_modif',$(this),vendor,model,year,modif);
    });


    // --- устаналиваем значения в HTML
	var _setHtmlOptions = function(elements,apendTo){
		console.log('--- _setHtmlOptions ---');
		// console.log(elements);
		// console.log(apendTo);

	    var options = '';
	    if (typeof elements != 'undefined'){
	        $.each(elements,function(index,value){
	        	// console.log(value);
	            if (value) {
	                options += '<option value="' + value + '">' + value + '</option>'
	            }
	        });
	        apendTo.children().remove();
	        apendTo.append($(options));
	    }
	};

	var _getModels = function(elements){

		// console.log(elements);

	    var models = [];
	    $.each(elements,function(){
	        if (this.NAME){
	            models.push(this.NAME);
	        }
	    });
	    return models;
	};

 // ищем элементы по параметрам из БД автомобилей
    var findDiskElements = function(model, type){
    	console.log('--- findDiskElements ---');
    	// console.log(model);
    	// console.log(type);

    	if(type == 'disky'){
    		var elems = $('#disks_by_param input');
    	}
    	// console.log(elems);
    	console.log(elems.length);
    	var i = 0;

		$.each(elems, function(index, value){
				// console.log("INDEX: " + index + "\n");
				// console.log(value);

				let element = $('#'+value.id + '-styler').siblings('.text');
				let typeCheckbox = false;
				switch (element.data('property')){
					case 'VYLET_ET':
						 setDiskCheckbox(model.WHEELS_FACTORY.VYLET_ET, element.data('trimparamvalue'), value.id, element.data('property'));
					break;

					case 'DIAMETR_OBODA':
						 setDiskCheckbox(model.WHEELS_FACTORY.DIAMETR_OBODA, element.data('trimparamvalue'), value.id, element.data('property'));
					break;

					case 'SHIRINA_DISKA': // Ширина (обода)
						 setDiskCheckbox(model.WHEELS_FACTORY.SHIRINA_DISKA, element.data('trimparamvalue'), value.id, element.data('property'));
					break;
				}
			
			i++;

			if(i == elems.length){
				elementNums = true;
			}

			
		});
    }

	// устанавливаем чекбоксы
	var setDiskCheckbox = function(data, trimparamvalue, id, typeCheckbox){
		console.log('--- setCheckbox ---');

		// console.log(data);

		// console.log(trimparamvalue);
		// console.log(id);

		// console.log(typeCheckbox);
		// console.log('--- / setCheckbox ---');

		data.forEach(function (value, i) {

			// if(typeCheckbox == 'SHIRINA_DISKA'){
			// 	value = value + ' "';

			// 	console.log('--- SHIRINA_DISKA ---');
			// 		console.log('WHEELS_FACTORY');
			// 		console.log(value);
			// 		console.log('DOM');
			// 		trimparamvalue = String(trimparamvalue);
			// 		trimparamvalue = trimparamvalue + ' "';
			// 		console.log(trimparamvalue);
			// 	console.log('--- /');

			// }


			if(typeCheckbox == 'SHIRINA_DISKA'){
					value = value + ' "';
					trimparamvalue = String(trimparamvalue);
					trimparamvalue = trimparamvalue + ' "';
			}

				
         	if(value == trimparamvalue){

         		if(typeCheckbox == 'SHIRINA_DISKA'){
	         		console.log('--- value == text ---');
	         		console.log(value);
	         		console.log(id);

	         	}

         	 	$('#'+id).prop('checked', true);
         	 	$('#'+id+'-styler').addClass('checked');
         	}

        });
	}

    // --- Ajax на PHP скрипт
    var diskAjax = function(action,$this,strVendor,strModel,strYear,strModif){

    	console.log('diskAjax');

	   	// сбрасываем фильтр по параметрам
        skipSelectedAllDisk();
        elementNums = false;

        // тип шины или диски
        var type = $('#type_disks').data('type');

        var data = {},
          $container = $this.closest('.filterHorizontal'),
            bHasInput = $container.find('[name="car-id"]').length ? true : false,
            $inputCar = bHasInput ? $container.find('[name="car-id"]') : $('<input type="hidden" name="car-id"/>');

	       if (typeof strVendor != 'undefined'){
	            data['vendor'] = strVendor;
	        }
	        if (typeof strModel != 'undefined'){
	            data['model'] = strModel;
	        }
	        if (typeof strYear != 'undefined'){
	            data['year'] = strYear;
	        }
	        if (typeof strModif != 'undefined'){
	            data['modif'] = strModif;
	        }

        data['action_get_info'] = action;
        data['type'] = type;
        
        $container.setAjaxLoading();

        $( "#set_disks_filter" ).prop( "disabled", true);
        // $this.prop( "disabled", true);
 
        
        // получаем данные по шинам (ширина, высота и диаметр) по выбранному автомобилю 
        $.ajax({
        	url: rz.AJAX_DIR + '/car_filter/car_ajax.php',
        	data:data,
            dataType:'json',
            method: 'POST',
            success: function(result){

            	// var modif,years,carID,models,objData;
            	var car, models, years, modif;
	            	models = result.data_base.MODELS;
	            	modif = result.data_base.MODIFICATIONS;
	            	years = result.data_base.YEARS;

	            console.log('--- Disk Ajax ---');
            	console.log(result.data_base.MODEL);
           	
                // выставляем новые значения
                findDiskElements(result.data_base.MODEL, type); // ищем в фильрах совпадения по параметрам

                // выводим данные в HTML
		        _setHtmlOptions(models,disks_selects['model']);
		        _setHtmlOptions(modif,disks_selects['modif']);
		        _setHtmlOptions(years,disks_selects['year']);

                if(elementNums){
	                $container.find('select').trigger('refresh');
	                $container.stopAjaxLoading();
	                $container.find('input').eq(0).trigger('change');
	                
	                // снимаем неактивность с кнопки Подобрать
	                setTimeout(function(){
						  $( "#set_disks_filter" ).prop( "disabled", false);
					}, 3000);
                }
               
            }

        });
    };



// кнопка убрать фильтр (чтобы надпись менялась)
$('.collapse_btn_wrapper').on('click', function(event){
	$('.seach_form__submit').find(".btn-inner").text(function(i, text){
	          return text === "ПОКАЗАТЬ ФИЛЬТР" ? "СКРЫТЬ ФИЛЬТР" : "ПОКАЗАТЬ ФИЛЬТР";
	 });
});



// --- TEST 

var $skipParam = $('#skip_param').on('click', function(event){
	console.log('--- skip_param ---');
	// skipSelected();
	skipSelectedAllDisk();
});

var skipSelectedAllDisk = function(){
 	console.log('--- skipSelectedAllDisk ---');
 	var elems = $('#disks_by_param input');
  	$.each(elems, function(index, value){
			// console.log(value.id);
			$('#'+value.id).prop('checked', false);
			$('#'+value.id+'-styler').removeClass('checked');
 	});
}


// сбрасываем значение при переключении на вкладку Подбор по автомобилю
$("a[href='#disks_by_car']").on('click', function () {

	$('#disks [name="disks_makr_filter"]').trigger("change");
});

})(jQuery);