<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use \Yenisite\Core\Tools;
include_once $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/tools/include_module.php';
Tools::addComponentDeferredJS($templateFile);
?>

<script type="text/javascript">
    if(typeof rz == 'undefined'){
        rz = {}
    }
    rz.COMPARE_LIST = new Array();

    <?foreach($templateData['ITEMS'] as $arItem):?>
        if ('COMPARE_LIST' in rz) {
            rz.COMPARE_LIST.push(<?=$arItem['ID']?>);
        } else {
            rz.COMPARE_LIST = [<?=$arItem['ID']?>];
        }
    <?endforeach?>

</script>
