<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
if (!CModule::IncludeModule('iblock')) die();
if (count($arResult) > 0) {
	/** @noinspection PhpDynamicAsStaticMethodCallInspection */
	$rsElements = CIBlockElement::GetList(array(),
		array(
			'ID' => array_keys($arResult)
		),
		false, false,
		array(
			'ID', 'PREVIEW_PICTURE', 'DETAIL_PICTURE'
		)
	);
	while ($arItem = $rsElements->GetNext()) {
		$arResult[$arItem['ID']]['PREVIEW_PICTURE'] = $arItem['PREVIEW_PICTURE'];
		$arResult[$arItem['ID']]['DETAIL_PICTURE'] = $arItem['DETAIL_PICTURE'];
	}
}