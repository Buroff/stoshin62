(typeof(jQuery) != 'undefined')
&& (function ($) {
	var $compareBtn = $('#compare_button'),
		data = [
			{name: "arParams", value: $compareBtn.data('arparams')},
			{name: "template", value: $compareBtn.data('template').toString()},
			{name: "is_refresh", value: 'Y'}
		];
	var $compareBtnAmount = $compareBtn.find('.amount');
	$(document).on('compare_Refresh', function () {
		$.ajax({
			type: "POST",
			dataType: 'json',
			url: rz.AJAX_DIR + "compare_Modal.php",
			data: data,
			success: function (count) {
				$compareBtnAmount.text(count);
				if (count > 0) {
					$compareBtn.removeClass('hidden');
				} else {
					$compareBtn.addClass('hidden');
				}
			}
		})
	});

	var $modalCompare = $('#modal-compare');
	$modalCompare.on('click', '.flush_compare', function (e) {
		e.preventDefault();
		var $modalCompareContent = $modalCompare.find('.popup_compare');
		var data = [
			{name: 'arParams', value: $modalCompare.data('arparams')},
			{name: 'template', value: $modalCompare.data('template').toString()},
			{name: 'action', value: 'FLUSH_ALL'}
		];
		$modalCompareContent.setAjaxLoading();
		return $.ajax({
			type: "POST",
			url: rz.AJAX_DIR + 'compare_Modal.php',
			data: data,
			success: function (html) {
				$modalCompareContent.html(html);
				$modalCompareContent.reStyler();
				$modalCompareContent.reSpin();
				$modalCompareContent.stopAjaxLoading();
				$(document).trigger('compare_Refresh');
			}
		});
	});
})(jQuery);