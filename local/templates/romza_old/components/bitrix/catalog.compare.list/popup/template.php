<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Yenisite\Core\Resize;
use \Yenisite\Core\Tools;
include_once $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/tools/include_module.php';

$isRefresh = (isset($_POST['is_refresh']) && $_POST['is_refresh'] == 'Y');
$isAjax = Tools::isAjax();
if (!$isAjax && method_exists($this, 'setFrameMode')) $this->setFrameMode(true);
$cnt = count($arResult);?>
<? if (!$isAjax && !$isRefresh): ?>
	<a class="btn top-compare-wrap<? if ($cnt < 1): ?> hidden<? endif ?>" role="button" data-toggle="modal"
		data-arparams="<?=Tools::GetEncodedArParams($arParams)?>" data-template="<?=$templateName?>"
		  id="compare_button" data-target="#modal-compare">
		<span class="icon icon_measuring7"></span>
		<span class="count amount">
			<? if (method_exists($this, 'createFrame')) $frame = $this->createFrame()->begin(''); ?>
				<?= $cnt ?>
			<? if (method_exists($this, 'createFrame')) $frame->end(); ?>
		</span>
	</a>
	<?
	global $arModals;
	if(!is_array($arModals)) {
		$arModals = array();
	}
	ob_start();
	?>
	<div class="modal fade" id="modal-compare" tabindex="-1" role="dialog" aria-labelledby="compare_modalLabel"
		 data-arparams="<?=Tools::GetEncodedArParams($arParams)?>" data-template="<?=$templateName?>"
		 aria-hidden="true">
		<div class="popup modal-dialog popup_compare">
			<h2 id="compare_modalLabel"><?=GetMessage("RZ_V_SPISKE_SRAVNENIYA")?></h2>
		</div>
	</div>
	<?$arModals[] = ob_get_clean();?>
<? elseif($isAjax && !$isRefresh): ?>
	<span class="close flaticon-delete30" data-dismiss="modal"></span>
	<h2 id="compare_modalLabel"><?=GetMessage("RZ_V_SPISKE_SRAVNENIYA")?> <?=$cnt,' ',Tools::rusQuantity($cnt,GetMessage("RZ_TOVAR"))?>:</h2>
	<form class="compare_form" role="form" action="<?= $arParams["COMPARE_URL"] ?>">
		<div class="compare__main_wrapper">
			<div class="compare__main_inner">
				<table class="compare__main">
					<tbody>
					<? foreach ($arResult as $arElement): ?>
						<tr>
							<td class="img">
								<img src="<?= Resize::GetResizedImg($arElement,
									array('WIDTH'=> 52, 'HEIGHT' => 80, 'SET_ID' => intval($arParams['RESIZER_PRODUCT']))
								)
								?>" alt=""/>
							</td>
							<td class="title">
								<input type="hidden" name="ID[]" value="<?= $arElement["ID"] ?>"/>
								<a href="<?= $arElement["DETAIL_PAGE_URL"] ?>"><?= $arElement["NAME"] ?></a>
							</td>
							<td class="delete">
								<div class="basket_order_form">
									<div class="basket_order__main ">
										<a class="delete_link" href="javascript:;" rel="nofollow" data-id="<?= $arElement['ID'] ?>">
											<span class="icon icon_recycling10"></span>
											<?= GetMessage("CATALOG_DELETE") ?>
										</a>
									</div>
								</div>
							</td>
						</tr>
					<? endforeach ?>
					</tbody>
				</table>
			</div>
		</div>
		<? if ($cnt>=2): ?>
			<div class="compare__submit">
				<button class="btn btn-primary btn-compare-popup" type="submit">
					<?= GetMessage("CATALOG_COMPARE") ?>
				</button>
				<a href="javascript:;" class="btn btn-default pull-right flush_compare"><?=GetMessage('SHIN_CLEAR')?></a>
				<input type="hidden" name="action" value="COMPARE"/>
				<input type="hidden" name="IBLOCK_ID" value="<?= $arParams["IBLOCK_ID"] ?>"/>
			</div>
		<? endif; ?>
	</form>
<?elseif($isAjax && $isRefresh):?>
	<?$APPLICATION->RestartBuffer();?>
	<?=json_encode($cnt); die();?>
<? endif; ?>
<?$templateData['ITEMS'] = $arResult;?>
