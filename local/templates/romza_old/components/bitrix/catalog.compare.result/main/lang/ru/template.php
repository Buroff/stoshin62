<?
$MESS["RZ_MODEL_"] = "Модель";
$MESS["RZ_KUPIT_"] = "Купить";
$MESS["RZ_UDALIT_"] = "Удалить";
$MESS["RZ_IZ_SPISKA_SRAVNENIYA"] = "из списка сравнения";
$MESS["RZ_TCENA"] = "Цена";
$MESS["CATALOG_COMPARE_BUY"] = "Купить";
$MESS["CATALOG_NOT_AVAILABLE"] = "(нет на складе)";
$MESS["CATALOG_ONLY_DIFFERENT"] = "Только различающиеся";
$MESS["CATALOG_ALL_CHARACTERISTICS"] = "Все характеристики";
$MESS["CATALOG_REMOVE_PRODUCTS"] = "Удалить товары";
$MESS["CATALOG_REMOVE_FEATURES"] = "Удалить свойства";
$MESS["CATALOG_REMOVED_FEATURES"] = "Удаленные свойства";
$MESS["CATALOG_ADD_TO_COMPARE_LIST"] = "Добавить в список сравнения";
