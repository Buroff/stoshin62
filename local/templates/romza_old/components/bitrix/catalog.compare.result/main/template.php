<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Yenisite\Core\Resize;
use \Yenisite\Core\Tools;
include_once $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/tools/include_module.php';?>
<?$isAjax = Tools::isAjax();?>
<?if(!$isAjax):?>
<div class="compare_block" id="compare_Block"
	 data-arparams="<?=Tools::GetEncodedArParams($arParams)?>"
	 data-template="<?=$templateName?>"
	>
<?endif;?>
	<div class="compare_main_table_wrapper">
		<div class="compare_main_table_inner">
			<table class="compare_main_table">
				<tbody>
				<tr>
					<th></th>
					<td class="compare_type" colspan="<?=count($arResult["ITEMS"])?>">
						<?if($arResult["DIFFERENT"]):?>
							<a class="compare_type__item"
								href="<?=htmlspecialcharsbx($APPLICATION->GetCurPageParam("DIFFERENT=N",array("DIFFERENT")))?>" rel="nofollow">
								<?=GetMessage("CATALOG_ALL_CHARACTERISTICS")?>
							</a>
						<? else:?>
							<a class="compare_type__item current">
								<?=GetMessage("CATALOG_ALL_CHARACTERISTICS")?>
							</a>
						<? endif ?>
						<? if(!$arResult["DIFFERENT"]): ?>
							<a class="compare_type__item"
								href="<?=htmlspecialcharsbx($APPLICATION->GetCurPageParam("DIFFERENT=Y",array("DIFFERENT")))?>" rel="nofollow">
								<?=GetMessage("CATALOG_ONLY_DIFFERENT")?>
							</a>
						<? else: ?>
							<a class="compare_type__item current">
								<?=GetMessage("CATALOG_ONLY_DIFFERENT")?>
							</a>
						<? endif ?>
					</td>
				</tr>
				<tr>
					<th><div><?=GetMessage("RZ_MODEL_")?></div></th>
					<?foreach($arResult["ITEMS"] as $arElement):?>
					<td class="item_td">
						<div class="goods_item_wrapper">
							<div class="goods_item">
								<div class="goods_item__inner">
									<a href="<?=$arElement['DETAIL_PAGE_URL']?>" class="goods_item__img_wrapper">
										<img
											src="<?= Resize::GetResizedImg($arElement, array('WIDTH' => 168, 'HEIGHT' => 170, 'SET_ID' => intval($arParams['RESIZER_PRODUCT'])))?>"
											alt="" class="goods_item__img">
									</a>
									<div class="goods_item__info">
										<div class="goods_item__title">
											<a href="<?=$arElement['DETAIL_PAGE_URL']?>"><?=$arElement['NAME']?></a>
										</div>
										<div class="goods_item__rating">
											<?$APPLICATION->IncludeComponent(
												"bitrix:iblock.vote",
												"stars",
												array(
													"IBLOCK_TYPE" => $arParams['IBLOCK_TYPE'],
													"IBLOCK_ID" => $arParams['IBLOCK_ID'],
													"ELEMENT_ID" => $arElement['ID'],
													"ELEMENT_CODE" => "",
													"MAX_VOTE" => "5",
													"VOTE_NAMES" => array("1", "2", "3", "4", "5"),
													"SET_STATUS_404" => "N",
													"DISPLAY_AS_RATING" => $arParams['VOTE_DISPLAY_AS_RATING'],
													"CACHE_TYPE" => $arParams['CACHE_TYPE'],
													"CACHE_TIME" => $arParams['CACHE_TIME']
												),
												$component,
												array("HIDE_ICONS" => "Y")
											);?>
										</div>
										<?/*?>
										<div class="goods_item__price">
											<?if($arElement['CUR_PRICE']):?>
												<?=$arElement['CUR_PRICE']?>
											<?endif;?>
										</div>
										<div class="goods_item__buy_button">
											<span class="btn btn-primary btn-buy"><?=GetMessage("RZ_KUPIT_")?></span>
										</div>
										<?*/?>
									</div>
								</div>
							</div>
							<div class="goods_item__delete_wrapper">
								<form action="<?=$APPLICATION->GetCurPage()?>" method="get">
									<input type="hidden" name="action" value="DELETE_FROM_COMPARE_RESULT" />
									<input type="hidden" name="URL" value="<?=$APPLICATION->GetCurPage(false)?>"/>
									<input type="hidden" name="ID" value="<?=$arElement['ID']?>" />
									<input type="hidden" name="IBLOCK_ID" value="<?=$arParams["IBLOCK_ID"]?>" />
									<div class="text-center">
										<a class="goods_item_delete">
											<span class="icon icon_delete30"></span>
											<?=GetMessage("RZ_UDALIT_")?>
										</a>
									</div>
								</form>
							</div>
						</div>

					</td>
					<?endforeach?>
				</tr>
				<tr>
					<th><div><?=GetMessage("RZ_TCENA")?></div></th>
					<?foreach($arResult["ITEMS"] as $arElement):?>
						<td class="price_td">
							<div class="goods_item__price">
								<?if($arElement['CUR_PRICE']):?>
									<?=$arElement['CUR_PRICE']?>
								<?endif;?>
							</div>
						</td>
					<?endforeach?>
				</tr>
				</tbody>
			</table>
		</div>
	</div>
	<table class="compare_sub_table">
		<tbody>
		<?foreach($arResult["SHOW_PROPERTIES"] as $code=>$arProperty):
			$arCompare = Array();
			foreach($arResult["ITEMS"] as $arElement)
			{
				$arPropertyValue = $arElement["DISPLAY_PROPERTIES"][$code]["VALUE"];
				if(is_array($arPropertyValue))
				{
					sort($arPropertyValue);
					$arPropertyValue = implode(" / ", $arPropertyValue);
				}
				$arCompare[] = $arPropertyValue;
			}
			$diff = (count(array_unique($arCompare)) > 1 ? true : false);
			if($diff || !$arResult["DIFFERENT"]):?>
				<tr>
					<th><div><?=$arProperty["NAME"]?></div></th>
					<?foreach($arResult["ITEMS"] as $arElement):?>
						<?if($code == 'SEASON'):?>
							<td class="season">
								<div>
									<?	$val = $arElement["PROPERTIES"]['SEASON']['VALUE'];
										if (!empty($val)):?>
											<?$class = Yenisite\Core\Catalog::getIconClassFromPropId(
													$arElement["PROPERTIES"]['SEASON']['ID'],
													$arElement["PROPERTIES"]['SEASON']['VALUE'])?>
											<span class="icon <?=$class?>"></span>
										<?else:?>
											--
										<?endif;?>
								</div>
							</td>
						<?else:?>
							<td>
								<div>
									<?	$val = $arElement["DISPLAY_PROPERTIES"][$code]['DISPLAY_VALUE'];?>
									<?if (!empty($val)):?>
									<?=(is_array($arElement["DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"])
										? implode("/ ", $arElement["DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"])
										: $arElement["DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"])?>
									<?else:?>
										--
									<?endif;?>
								</div>
							</td>
						<?endif?>
					<?endforeach?>
				</tr>
			<?endif?>
		<?endforeach;?>
		</tbody>
	</table>
<?if(!$isAjax):?>
</div>
<?endif;?>
