<table class="item_modifications">
	<caption><?=GetMessage("RZ_TIPORAZMERI_I_VARIANTI")?>:</caption>
	<thead>
	<tr>
		<?foreach ($arResult['OFFERS_PROP'] as $skuKey => $isSet):?>
			<?if($isSet):?>
				<th><?=$arResult['SKU_PROPS'][$skuKey]['NAME']?></th>
			<?endif?>
		<?endforeach?>
		<?$firstOffer = reset($arResult['OFFERS']);?>
		<?foreach ($firstOffer['DISPLAY_PROPERTIES'] as $arOfferProps):?>
			<th><?=$arOfferProps['NAME']?></th>
		<?endforeach?>
		<th><?=GetMessage("RZ_NALICHIE")?></th>
		<th><?=GetMessage("RZ_TCENA")?></th>
		<?if($arResult['DEFAULT_QUANT'] > 1):?>
			<th><?=GetMessage("RZ_TCENA_ZA")?> <?=$arResult['DEFAULT_QUANT']?> <?=GetMessage("RZ_SHT")?></th>
		<?endif?>
		<th><?=GetMessage("RZ_KUPIT_")?></th>
	</tr>
	</thead>
	<tbody>
	<?foreach($arResult['OFFERS'] as $arOffer):?>
		<?
		$canBuy = ($arOffer['CAN_BUY'] == true || $arOffer['CAN_BUY'] == 'Y');
		?>
	<tr>
		<?foreach ($arResult['OFFERS_PROP'] as $skuKey => $isSet):?>
			<?if($isSet):?>
				<?
				$skuPropValId = $arOffer['TREE']['PROP_'.$arResult['SKU_PROPS'][$skuKey]['ID']];
				$arSkuPropVal = $arResult['SKU_PROPS'][$skuKey]['VALUES'][$skuPropValId];
				?>
				<td>
					<?if(!empty($arSkuPropVal['PICT'])):?>
						<img class="color" src="<?=$arSkuPropVal['PICT']['SRC']?>" alt=""/>
					<?else:?>
						<?if(!empty($arSkuPropVal['ICON_CLASS'])):?>
							<span class="item_specification__list">
								<span class="icon <?=$arSkuPropVal['ICON_CLASS']?>" title="<?=$arSkuPropVal['NAME']?>"></span>
							</span>
						<?else:?>
							<?=$arSkuPropVal['NAME']?>
						<?endif?>
					<?endif?>
				</td>
			<?endif?>
		<?endforeach?>
		<?foreach ($arOffer['DISPLAY_PROPERTIES'] as $arOfferProp):?>
			<td><?=$arOfferProp['DISPLAY_VALUE']?></td>
		<?endforeach?>
		<td>
			<? if($arOffer['CAN_BUY']): ?>
				<span class="avalible"><?=GetMessage("RZ_EST_")?></span>
			<? else: ?>
				<span class="unavailable"><?=GetMessage("RZ_NET")?></span>
			<? endif ?>
		</td>
		<td><span class="goods_item__price"><?=$arOffer['MIN_PRICE']['PRINT_DISCOUNT_VALUE']?></span></td>
		<?if($arResult['DEFAULT_QUANT'] > 1):?>
			<td>
				<span class="goods_item__price for4">
					<?=CurrencyFormat($arOffer['MIN_PRICE']['DISCOUNT_VALUE'] * $arResult['DEFAULT_QUANT'],$arOffer['MIN_PRICE']['CURRENCY'])?>
				</span>
			</td>
		<?endif?>
		<td>
			<form class="item_manage__inner item_manage__buying add2basket_form with_modal" method="get"
				  action="<?=$APPLICATION->GetCurPage()?>"
				  data-target="#modal-buy"
				  data-varQ="<?= $arParams["PRODUCT_QUANTITY_VARIABLE"]; ?>"
				  data-varId="<?= $arParams['PRODUCT_ID_VARIABLE'] ?>"
				  data-varAct="<?=$arParams['ACTION_VARIABLE']?>">
				<input type="hidden" name="action" value="ADD2BASKET"/>
				<input type="hidden" name="product_name" value="<?=$arResult['NAME']?>"/>
				<input type="hidden" name="product_img" value="<?=$arResult['PICTURE']?>"/>
					<div class="item_manage__amount">
						<input class="js-touchspin<?= !$canBuy ? ' disabled' : '' ?>" type="text" value="<?=$arResult['DEFAULT_QUANT']?>" name="<?= $arParams["PRODUCT_QUANTITY_VARIABLE"]; ?>" >
					</div>
				<div class="item_manage__submit">
					<input type="hidden" name="<?=$arParams['PRODUCT_ID_VARIABLE']?>" value="<?=$arOffer['ID']?>"/>
					<button class="btn btn-primary btn-buy<?= !$canBuy ? ' disabled' : '' ?>"
							type="submit"<?= !$canBuy ? ' disabled' : '' ?>><?= GetMessage("RZ_KUPIT_") ?></button>
				</div>
			</form>
		</td>
	</tr>
	<?endforeach?>
	</tbody>
</table>