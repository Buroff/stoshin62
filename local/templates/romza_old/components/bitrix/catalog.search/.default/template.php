<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?global $rz_options;
use Yenisite\Core\Ajax;
use \Yenisite\Core\Catalog;
?>
<?if (method_exists($this, 'setFrameMode')) $this->setFrameMode(true);?>
<?include_once $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/tools/include_module.php';?>
	<h1><?$APPLICATION->ShowTitle()?></h1>
<div id="catalog_searchResult">
<?
$arCatalogParams = Ajax::getParams('bitrix:catalog', 'main_catalog', '', SITE_ID);
$arElements = $APPLICATION->IncludeComponent(
	"bitrix:search.page",
	"null",
	Array(
		"RESTART" => $arParams["RESTART"],
		"NO_WORD_LOGIC" => $arParams["NO_WORD_LOGIC"],
		"USE_LANGUAGE_GUESS" => $arParams["USE_LANGUAGE_GUESS"],
		"CHECK_DATES" => $arParams["CHECK_DATES"],
		"arrFILTER" => array("iblock_" . $arParams["IBLOCK_TYPE"]),
		"arrFILTER_iblock_" . $arParams["IBLOCK_TYPE"] => array($arParams["IBLOCK_ID"]),
		"USE_TITLE_RANK" => "N",
		"DEFAULT_SORT" => "rank",
		"FILTER_NAME" => "",
		"SHOW_WHERE" => "N",
		"arrWHERE" => array(),
		"SHOW_WHEN" => "N",
		"PAGE_RESULT_COUNT" => 50,
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "N",
	),
	$component
);
if (!empty($arElements) && is_array($arElements)) {
	global $rz_options;
	if(!empty($rz_options['GEOIP']['PRICES'])) {
		$arParams["PRICE_CODE"] = $rz_options['GEOIP']['PRICES'];
	}
	$arParams['STORES'] = NULL;
	if(!empty($rz_options['GEOIP']['STORES'])) {
		$arParams['STORES'] = $rz_options['GEOIP']['STORES'];
	}
	$arSort = Catalog::getSort($arParams,NULL);
	$count = Catalog::getCount();
	$sectionId = intval($_GET['SECTION_ID']);
	if($sectionId > 0) {
		$byLink = "";
	} else {
		$byLink = "Y";
	}
	global $searchFilter;
	$arElements = array_values($arElements);
	$searchFilter = array(
		"=ID" => $arElements,
	);
	$APPLICATION->IncludeComponent(
		"bitrix:catalog.section",
		"main-list-new",
		array(
			"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"ELEMENT_SORT_FIELD" => $arSort['BY'],
			"ELEMENT_SORT_ORDER" => $arSort['ORDER'],
			"ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
			"ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
			"PAGE_ELEMENT_COUNT" => $count,
			"LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
			"PROPERTY_CODE" => $arParams["PROPERTY_CODE"],
			"OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
			"OFFERS_FIELD_CODE" => $arParams["OFFERS_FIELD_CODE"],
			"OFFERS_PROPERTY_CODE" => $arParams["OFFERS_PROPERTY_CODE"],
			"OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
			"OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
			"OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
			"OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
			"OFFERS_LIMIT" => $arParams["OFFERS_LIMIT"],
			"SECTION_URL" => $arParams["SECTION_URL"],
			"DETAIL_URL" => $arParams["DETAIL_URL"],
			"BASKET_URL" => $arParams["BASKET_URL"],
			"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
			"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
			"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
			"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
			"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
			"CACHE_TYPE" => $arParams["CACHE_TYPE"],
			"CACHE_TIME" => $arParams["CACHE_TIME"],
			"DISPLAY_COMPARE" => $arParams["DISPLAY_COMPARE"],
			"PRICE_CODE" => $arParams["PRICE_CODE"],
			"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
			"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
			"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
			"PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],
			"USE_PRODUCT_QUANTITY" => $arParams["USE_PRODUCT_QUANTITY"],
			"CONVERT_CURRENCY" => $arParams["CONVERT_CURRENCY"],
			"CURRENCY_ID" => $arParams["CURRENCY_ID"],
			"HIDE_NOT_AVAILABLE" => $arParams["HIDE_NOT_AVAILABLE"],
			"DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
			"DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
			"PAGER_TITLE" => $arParams["PAGER_TITLE"],
			"PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
			"PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
			"PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
			"PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
			"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
			"FILTER_NAME" => "searchFilter",
			"SECTION_ID" => $sectionId,
			"SECTION_CODE" => "",
			"SECTION_USER_FIELDS" => array(),
			"INCLUDE_SUBSECTIONS" => "Y",
			"SHOW_ALL_WO_SECTION" => "Y",
			"META_KEYWORDS" => "",
			"META_DESCRIPTION" => "",
			"SET_BROWSER_TITLE" => "N",
			"ADD_SECTIONS_CHAIN" => "N",
			"SET_TITLE" => "N",
			"SET_STATUS_404" => "Y",
			"CACHE_FILTER" => "N",
			"CACHE_GROUPS" => "N",
			'VIEW_MODE' => Catalog::getViewMode(),
			'BY_LINK' => $byLink,
			'RESIZER_PRODUCT' => $arParams['RESIZER_PRODUCT'],
			'ONECLICK_PERSON_TYPE_ID' => $arCatalogParams["ONECLICK_PERSON_TYPE_ID"],
			'ONECLICK_SHOW_FIELDS' => $arCatalogParams["ONECLICK_SHOW_FIELDS"],
			'ONECLICK_REQ_FIELDS' => $arCatalogParams["ONECLICK_REQ_FIELDS"],
			'ONECLICK_ALLOW_AUTO_REGISTER' => $arCatalogParams["ONECLICK_ALLOW_AUTO_REGISTER"],
			'ONECLICK_USE_CAPTCHA' => $arCatalogParams["ONECLICK_USE_CAPTCHA"],
			'ONECLICK_MESSAGE_OK' => $arCatalogParams["ONECLICK_MESSAGE_OK"],
			'ONECLICK_PAY_SYSTEM_ID' => $arCatalogParams["ONECLICK_PAY_SYSTEM_ID"],
			'ONECLICK_DELIVERY_ID' => $arCatalogParams["ONECLICK_DELIVERY_ID"],
			'ONECLICK_AS_EMAIL' => $arCatalogParams["ONECLICK_AS_EMAIL"],
			'ONECLICK_AS_NAME' => $arCatalogParams["ONECLICK_AS_NAME"],
			'ONECLICK_SEND_REGISTER_EMAIL' => $arCatalogParams["ONECLICK_SEND_REGISTER_EMAIL"],
			'ONECLICK_FIELD_PLACEHOLDER' => $arCatalogParams["ONECLICK_FIELD_PLACEHOLDER"],
			'ONECLICK_FIELD_QUANTITY' => $arCatalogParams["ONECLICK_FIELD_QUANTITY"],
			'USE_STORE_PHONE' => $arParams['USE_STORE_PHONE'],
			'USE_STORE_SCHEDULE' => $arParams['USE_STORE_SCHEDULE'],
			'USE_STORE' => $arParams['USE_STORE'],
			'SHOW_CATCH_BUY' => $rz_options['show_catchbuy_in_section'] != 'N',
			'USE_ONE_CLICK' => $rz_options['show_one_click_in_section'],
		),
		$arResult["THEME_COMPONENT"]
	);
	?>
	<script type="text/javascript">
		rz.FILTER_AJAX = [
			{
				'name': 'CUSTOM_FILTER[=ID]',
				'value' : <?=CUtil::PhpToJSObject($searchFilter['=ID'])?>
			}
		]
	</script>
	<?
} else {
	echo GetMessage("CT_BCSE_NOT_FOUND");
}
?>
</div>