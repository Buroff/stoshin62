<?php
$arTemplateParameters = array(
	'SEARCH_PAGE_URL' => array(
		'NAME' => GetMessage('RZ_SEARCH_PAGE_URL'),
		'TYPE' => 'STRING',
		'MULTIPLE' => 'N',
		'ADDITIONAL_VALUES' => 'N',
		'REFRESH' => 'N',
		'DEFAULT' => '#SITE_DIR#search/',
	),
);