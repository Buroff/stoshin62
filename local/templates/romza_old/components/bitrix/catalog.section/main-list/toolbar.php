<div class="goods_list__toolbar">
	<div class="items_on_page_wrapper">
		<?=GetMessage('RZ_SHOW_COUNT_TITLE')?>
		<select class="items_on_page_select" name="select_items_on_page"<?if(!empty($arParams['PAGEN_URL'])):?> data-href="<?=$arParams['PAGEN_URL']?>"<?endif?>>
			<option value="16"<?if($arParams['PAGE_ELEMENT_COUNT'] == '16'):?> selected <?endif?>>16</option>
			<option value="32"<?if($arParams['PAGE_ELEMENT_COUNT'] == '32'):?> selected <?endif?>>32</option>
			<option value="48"<?if($arParams['PAGE_ELEMENT_COUNT'] == '48'):?> selected <?endif?>>48</option>
			<option value="ALL"<?if($arParams['PAGE_ELEMENT_COUNT'] == '10000'):?> selected <?endif?>><?=GetMessage('RZ_SHOW_COUNT_ALL')?></option>
		</select>
	</div>

	<div class="change_view_wrapper">
		<?=GetMessage('RZ_VIEW_MODE_TITLE')?>
		<div class="js-change-view btn-group btn-group-change-view">
			<button type="button" class="btn<?if($viewMode == 'grid_view'):?> active<?endif?>" data-class="goods_list grid_view">
				<span class="icon icon_2x2_2"></span>
			</button>
			<button type="button" class="btn<?if($viewMode == 'list_view'):?> active<?endif?>" data-class="goods_list list_view">
				<span class="icon icon_list"></span>
			</button>
			<button type="button" class="btn<?if($viewMode == 'table_view'):?> active<?endif?>" data-class="goods_list table_view">
				<span class="icon icon_lines"></span>
			</button>
		</div>
	</div>

	<?=$arResult['NAV_STRING']?>
</div>