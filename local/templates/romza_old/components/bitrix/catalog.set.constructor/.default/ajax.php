<?
define("NOT_CHECK_PERMISSIONS", true);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if(!CModule::IncludeModule("catalog"))
{
	return;
}

if ($_SERVER["REQUEST_METHOD"]=="POST" && strlen($_POST["action"])>0 && check_bitrix_sessid())
{
	$APPLICATION->RestartBuffer();

	switch ($_POST["action"])
	{
		case "catalogSetAdd2Basket":
			if (is_array($_POST["set_ids"]))
			{
				foreach($_POST["set_ids"] as $itemID)
				{
					$product_properties = true;
					if (!empty($_POST["setOffersCartProps"]))
					{
						$product_properties = CIBlockPriceTools::GetOfferProperties(
							$itemID,
							$_POST["iblockId"],
							$_POST["setOffersCartProps"]
						);
					}
					$ratio = 1;
					if ($_POST["itemsRatio"][$itemID])
						$ratio = $_POST["itemsRatio"][$itemID];

					if (intval($itemID))
						Add2BasketByProductID(intval($itemID), $ratio, array("LID" => $_POST["lid"]), $product_properties);
				}
			}
			break;
		case "ajax_recount_prices":
			include_once $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/tools/include_module.php';
			CRZEventHandlers::SetSiteID(SITE_ID);
			if (strlen($_POST["currency"])>0)
			{
				$arPrices = array("formatSum" => "", "formatOldSum" => "", "formatDiscDiffSum" => "",
								"NotformatSum" => "", "NotformatOldSum" => "", "NotformatDiscDiffSum" => "");
				if ($_POST["sumPrice"])
				{
					$arPrices["NotformatSum"] = CCurrencyLang::CurrencyFormat($_POST["sumPrice"], $_POST["currency"], false);
					$arPrices["formatSum"] = FormatCurrency($_POST["sumPrice"], $_POST["currency"]);
				}
				if ($_POST["sumOldPrice"] && $_POST["sumOldPrice"] != $_POST["sumPrice"])
				{
					$arPrices["NotformatOldSum"] = CCurrencyLang::CurrencyFormat($_POST["sumOldPrice"], $_POST["currency"], false);
					$arPrices["formatOldSum"] = FormatCurrency($_POST["sumOldPrice"], $_POST["currency"]);
				}
				if ($_POST["sumDiffDiscountPrice"])
				{
					$arPrices["NotformatDiscDiffSum"] = CCurrencyLang::CurrencyFormat($_POST["sumDiffDiscountPrice"], $_POST["currency"], false);
					$arPrices["formatDiscDiffSum"] = FormatCurrency($_POST["sumDiffDiscountPrice"], $_POST["currency"]);
				}
				if (SITE_CHARSET != "utf-8")
					$arPrices = $APPLICATION->ConvertCharsetArray($arPrices, SITE_CHARSET, "utf-8");
				echo json_encode($arPrices);
			}
			break;
	}

	die();
}
?>