(typeof(jQuery) != 'undefined')
&& (function ($) {
	var $rangeSlider = $(".js-price-range-slider");


	$rangeSlider.each(function () {
		var $this = $(this),
			$parent = $this.closest('.seach_form__inner_price'),
			$priceTo = $parent.find('.price_to:eq(0)'),
			$priceFrom = $parent.find('.price_from:eq(0)'),
			priceMinVal = Math.floor(parseFloat($priceFrom.data('val'))),
			priceMaxVal = Math.ceil(parseFloat($priceTo.data('val'))),
			priceToVal = $priceTo.val(),
			priceFromVal = $priceFrom.val();
		if (priceToVal.length == 0) {
			priceToVal = priceMaxVal;
		}
		if (priceFromVal.length == 0) {
			priceFromVal = priceMinVal;
		}
		$this.noUiSlider({
			start: [priceFromVal, priceToVal],
			connect: true,
			step: 1,
			range: {
				'min': priceMinVal,
				'max': priceMaxVal
			},
			format: wNumb({
				decimals: 0
			})
		});
		$this.on({
			set: function (e, vals) {
				$priceFrom.val('');
				$priceTo.val('');
			},
			change: function (e, vals) {
				if ($priceFrom.val().length == 0 || $priceTo.val().length == 0) {
					$priceFrom.val(vals[0]);
					$priceTo.val(vals[1]);
					$priceTo.trigger('change');
				}
			}
		});

		var triggerSliderInput = function (val) {
			var $this = $(this);
			if ($this.val() != val && $this.val().length > 0) {
			$this.val(val);
				if (sliderTrigger != null) {
					clearTimeout(sliderTrigger);
				}
				sliderTrigger = setTimeout(function () {
					$this.trigger('change');
				}, 700);
			}
		};
		$this.Link('lower').to($priceFrom, function (val) {
			triggerSliderInput.bind(this, val);
		});
		$this.Link('upper').to($priceTo, function (val) {

			triggerSliderInput.bind(this, val);
		});

		$this.Link('lower').to('-inline-<div class="tooltip"></div>', function (value) {
			$(this).html('<span class="tooltip_inner">' + value + '</span>');
		});

		$this.Link('upper').to('-inline-<div class="tooltip"></div>', function (value) {
			$(this).html('<span class="tooltip_inner">' + value + '</span>');
		});
	});

	var $sectionFilters = $('.filterHorizontal');
	$sectionFilters.on('change', 'input', function (e) {
		var $sectionFilter = $(this).closest('form.filterHorizontal');
		var rnd = $sectionFilter.data('rnd');
		var data = $sectionFilter.serializeArray();
		data.push({name: 'arParams', value: $sectionFilter.data('arparams')});
		$sectionFilter.setAjaxLoading();
		$.ajax({
			url: rz.AJAX_DIR + "catalog_Filter.php",
			data: data,
			dataType: "json",
			success: function (json) {
				rz.FILTER_AJAX = [];
				$.each(json.ITEMS, function (key, val) {
					$.each(val.VALUES, function (valKey, valVal) {
						ajaxValuePrepare(valVal, rnd);
					});
				});
				$sectionFilter.stopAjaxLoading();
				$(document).trigger('catalog_applyFilter');
			}
		})
	});

	$sectionFilters.on('click', '.form_reset_button', function (e) {
		e.preventDefault();
		var $sectionFilter = $(this).closest('form.filterHorizontal');
		$sectionFilter[0].reset();
		var rnd = $sectionFilter.data('rnd');
		var $jsSlider = $sectionFilter.find('.js-price-range-slider');
		var priceToVal = $sectionFilter.find('.price_to:eq(0)').val(),
			priceFromVal = $sectionFilter.find('.price_from:eq(0)').val();
		$jsSlider.val([priceFromVal,priceToVal]);
		$sectionFilter.find('input:visible:eq(0)').trigger('change');

	});

	var ajaxValuePrepare = function (val, rnd) {
		var $curItem = $('#' + rnd + val.CONTROL_ID);
		var $itemStyler = $('#' + rnd + val.CONTROL_ID + "-styler");
		var lastsymb = val.CONTROL_NAME.substr(-3);
		var isNum = false;
		if (lastsymb == 'MAX' || lastsymb == "MIN") {
			isNum = true;
		}
		if ('DISABLED' in val && val.DISABLED == 1) {
			if ($itemStyler.length > 0) {
				$itemStyler.addClass('disabled');
			}
			$curItem.attr('disabled', 'disabled');
		} else {
			if ($itemStyler.length > 0) {
				$itemStyler.removeClass('disabled');
			}
			$curItem.removeAttr('disabled');
		}
	};


	// --- устаналиваем значения в HTML
	var _setHtmlOptions = function(elements,apendTo){
		// console.log('--- _setHtmlOptions ---');
		// console.log(elements);
		// console.log(apendTo);

	    var options = '';
	    if (typeof elements != 'undefined'){
	        $.each(elements,function(index,value){
	        	// console.log(value);
	            if (value) {
	                options += '<option value="' + value + '">' + value + '</option>'
	            }
	        });
	        apendTo.children().remove();
	        apendTo.append($(options));
	    }
	};

	var _getModels = function(elements){
	    var models = [];
	    $.each(elements,function(){
	        if (this.NAME){
	            models.push(this.NAME);
	        }
	    });
	    return models;
	};

	// --- событие при изменение данных в списке выбора автомобиля
	var cars_selects = {'mark': '[name="tiers_makr_filter"]', 'model': '[name="tiers_model_filter"]','year':'[name="tiers_year_filter"]','modif':'[name="tiers_modif_filter"]'};
	
	var $sectionFilters = $('.filterHorizontal');

    $.each(cars_selects, function (key, val) {
        cars_selects[key] = $sectionFilters.find(val);
    });

    
	/*cars_selects['mark'].on('change', function () {
        var vendor = $(this).find('option:selected').text();
        doAjaxGetValsOfCars('by_vendor',$(this),vendor);
    });
    cars_selects['model'].on('change', function () {
        var $this = $(this),
            vendor = $this.getSelOptFromForm(cars_selects['mark']).text(),
            model = $this.find('option:selected').text();
        doAjaxGetValsOfCars('by_vendor_model',$(this),vendor,model);
    });
    cars_selects['year'].on('change', function () {
        var $this = $(this),
            vendor = $this.getSelOptFromForm(cars_selects['mark']).text(),
            model = $this.getSelOptFromForm(cars_selects['model']).text(),
            year = $this.find('option:selected').text();
        doAjaxGetValsOfCars('by_vendor_model_year',$(this),vendor,model,year);
    });
    cars_selects['modif'].on('change', function () {
        var $this = $(this),
            vendor = $this.getSelOptFromForm(cars_selects['mark']).text(),
            model = $this.getSelOptFromForm(cars_selects['model']).text(),
            year = $this.getSelOptFromForm(cars_selects['year']).text(),
            modif = $this.find('option:selected').text();
        doAjaxGetValsOfCars('by_vendor_model_year_modif',$(this),vendor,model,year,modif);
    });*/

    // --- 

    // Ajax на PHP скрипт
    /*var doAjaxGetValsOfCars = function(action,$this,strVendor,strModel,strYear,strModif){

    	// console.log('--- doAjaxGetValsOfCars ---');
    	// console.log(action);
    	// console.log(strVendor);
    	// console.log(strYear);
    	// console.log(strModif);

        var data = {},
          $container = $this.closest('.filterHorizontal'),
            bHasInput = $container.find('[name="car-id"]').length ? true : false,
            $inputCar = bHasInput ? $container.find('[name="car-id"]') : $('<input type="hidden" name="car-id"/>');

	       if (typeof strVendor != 'undefined'){
	            data['vendor'] = strVendor;
	        }
	        if (typeof strModel != 'undefined'){
	            data['model'] = strModel;
	        }
	        if (typeof strYear != 'undefined'){
	            data['year'] = strYear;
	        }
	        if (typeof strModif != 'undefined'){
	            data['modif'] = strModif;
	        }

        data['action_get_info'] = action;

        
        // получаем данные по шинам (ширина, высота и диаметр) по выбранному автомобилю 
        $.ajax({
        	url: rz.AJAX_DIR + '/car_filter/car_ajax.php',
        	data:data,
            dataType:'json',
            method: 'POST',
            success: function(result){

            	// var modif,years,carID,models,objData;
            	var car, models, years, modif;
	            	models = result.data_base.MODELS;
	            	modif = result.data_base.MODIFICATIONS;
	            	years = result.data_base.YEARS;

	            // console.log(cars_selects['year']);
            	// console.log(years);
            	// console.log(result);
            	// console.log(models);
            	// console.log(result.data_base.MODE);

            	//
            	// console.log('--- cars_selects ---');
            	// console.log(cars_selects['model']);
            	// console.log(cars_selects['modif']);
            	// console.log(cars_selects['year']);
            	// console.log(models);
            	// console.log(modif);
            	// console.log(years);



            	// выводим данные в HTML
            	_setHtmlOptions(models,cars_selects['model']);
            	_setHtmlOptions(modif,cars_selects['modif']);
                _setHtmlOptions(years,cars_selects['year']);
                // _setHtmlOptions(models,cars_selects['model']);
                
                $container.find('select').trigger('refresh');
                $container.stopAjaxLoading();
                $container.find('input').eq(0).trigger('change');


                // сбрасываем фильтр по параметрам
            	skipSelectedAll();

                let type = $('#type').data('type');
                // выставляем новые значения
                findElements(result.data_base.MODEL, type); // ищем в фильрах совпадения по параметрам

            }


        });
    };*/




    // ищем элементы по параметрам из БД автомобилей
    var findElements = function(model, type){
    	console.log('--- findElements ---');
    	console.log(model);
    	console.log(type);

    	if(type == 'shiny'){
    		var elems = $('#tires_by_param input');
    	}
    	// console.log(elems);

		$.each(elems, function(index, value){
				// console.log("INDEX: " + index + "\n");
				// console.log(value);

				let element = $('#'+value.id + '-styler').siblings('.text');
				switch (element.data('property')){
					case 'DIAMETR':
				    	setCheckbox(model.TYRES_FACTORY.DIAMETR, element.text(), value.id);
				    break;

				    case 'SHIRINA_PROFILA':
				    	setCheckbox(model.TYRES_FACTORY.SHIRINA_PROFILA, element.text(), value.id);
				    break;    

				    case 'VISOTA_PROFILA':
				    	setCheckbox(model.TYRES_FACTORY.VISOTA_PROFILA, element.text(), value.id);
				    break;
				}
		});
    }

	// устанавливаем чекбоксы
	var setCheckbox = function(data, text, id){
		console.log('--- setCheckbox ---');
		data.forEach(function (value, i) {
         	if(value == text){
         	 	$('#'+id).prop('checked', true);
         	 	$('#'+id+'-styler').addClass('checked');
         	}
        });
	}


// кнопка убрать фильтр (чтобы надпись менялась)
$('.collapse_btn_wrapper').on('click', function(event){
	$('.seach_form__submit').find(".btn-inner").text(function(i, text){
	          return text === "ПОКАЗАТЬ ФИЛЬТР" ? "СКРЫТЬ ФИЛЬТР" : "ПОКАЗАТЬ ФИЛЬТР";
	 });
});




// --- TEST --- _doAjaxGetValsOfCars

var $skipParam = $('#skip_param').on('click', function(event){
	console.log('--- skip_param ---');
	// skipSelected();
	skipSelectedAll();
});


var $skipParam = $('#skip_param').on('click', function(event){
	console.log('--- skip_param ---');
	// skipSelected();
	skipSelectedAll();
});



var skipSelectedAll = function(){
 	console.log('--- skipSelected ---');
 	var elems = $('#tires_by_param input');
  	$.each(elems, function(index, value){
			// console.log(value.id);
			$('#'+value.id).prop('checked', false);
			$('#'+value.id+'-styler').removeClass('checked');
 	});
}

var skipSelected = function(){
 	console.log('--- skipSelected ---');
 	$("#KZyYNzarrFilter_194_1772422577").attr('checked', false);
 	$("#KZyYNzarrFilter_194_1772422577-styler").removeClass('checked');
}

var $checkParam = $('#check_param').on('click', function(){
	    console.log('--- check_param ---');
	    checkSelected();
});

var checkSelected = function(){
	console.log('--- checkSelected ---');
	$("#KZyYNzarrFilter_194_1772422577").attr('checked', true);
 	$("#KZyYNzarrFilter_194_1772422577-styler").addClass('checked');
}



// $('#tires_by_param').on('click', function(event){
// $('#tires_by_param').on('change', function(event){
// 	console.log('--- tires_by_param ---');
// 	var elem = event.target;
// 	console.log(elem);
// });

// $('.nav-tabs').on('click', function(event){
// 	console.log('--- click nav-tabs ---');
// 	var elem = event.target;
// 	skipSelectedAll();
// });

// tiers_model_filter
// $( "#tires_by_car").on('click', function(event){
// 	console.log('--- click tires_by_car ---');
// 	// var elem = event.target;
// 	// console.log(elem);
// 	skipSelected();
// });




	var testModel = {
		'NAME':"Camaro",
		'SELECTED_CAR_ID':"4784",
		'TYRES_FACTORY':{
			DIAMETR:["16"],
			SHIRINA_PROFILA:["245"],
			VISOTA_PROFILA:["50"]
		}
	}


    cars_selects['modif'].on('change', function () {
    	console.log('trigger');

        var $this = $(this),
            _vendor = 'Chevrolet',
            _model = 'Camaro',
            _year = '2000',
            _modif = '5.7';
        _doAjaxGetValsOfCars('by_vendor_model_year_modif',$(this),_vendor,_model,_year,_modif);
    });


	var $setParam = $('#set_param').on('click', function(event){
		console.log('--- set_param ---');
		$('#tires [name="tiers_modif_filter"]').trigger("change");

		// console.log(testModel);
		// var _models = ["Camaro", "Astro", "Avalanche", "Aveo", "Aveo II", "Blazer", "Camaro V", "Camaro VI", "Captiva", "Cavalier", "Cobalt", "Cobalt II", "Colorado", "Colorado II", "Corvette", "Corvette VII (C7)", "Cruze", "Cruze II", "Epica", "Equinox", "Equinox II", "Express", "HHR", "Impala", "Impala X", "Lacetti", "Lanos", "Malibu", "Malibu VII", "Malibu VIII", "Malibu IX", "Monte Carlo", "Niva", "Orlando", "Rezzo", "Silverado", "Spark", "Spark III (M300)", "Spark IV (M400)", "SSR", "Suburban", "Tahoe", "Tahoe III", "Tahoe IV", "TrailBlazer", "TrailBlazer II", "Traverse", "Trax", "Uplander", "Venture"];
		// var _modif = ["5.7", "3.8"];
		// var _years = ["2000", "1998", "1999", "2001", "2002"];
		// let type = 'shiny';
		// findElements(testModel, type); // ищем в фильрах совпадения по параметрам

	});


 	// Ajax на PHP скрипт
    var _doAjaxGetValsOfCars = function(action,$this,strVendor,strModel,strYear,strModif){

    	console.log('--- _ doAjaxGetValsOfCars ---');
    	// console.log(action);
    	// console.log(strVendor);
    	// console.log(strYear);
    	// console.log(strModif);

        var data = {},
          $container = $this.closest('.filterHorizontal'),
            bHasInput = $container.find('[name="car-id"]').length ? true : false,
            $inputCar = bHasInput ? $container.find('[name="car-id"]') : $('<input type="hidden" name="car-id"/>');

	       if (typeof strVendor != 'undefined'){
	            data['vendor'] = strVendor;
	        }
	        if (typeof strModel != 'undefined'){
	            data['model'] = strModel;
	        }
	        if (typeof strYear != 'undefined'){
	            data['year'] = strYear;
	        }
	        if (typeof strModif != 'undefined'){
	            data['modif'] = strModif;
	        }

        data['action_get_info'] = action;

        console.log(data);

        
        // получаем данные по шинам (ширина, высота и диаметр) по выбранному автомобилю 
        $.ajax({
        	url: rz.AJAX_DIR + '/car_filter/car_ajax.php',
        	data:data,
            dataType:'json',
            method: 'POST',
            success: function(result){

            	// var modif,years,carID,models,objData;
            	var car, models, years, modif;
	            	models = result.data_base.MODELS;
	            	modif = result.data_base.MODIFICATIONS;
	            	years = result.data_base.YEARS;

            	console.log(result.data_base.MODEL);

            	// выводим данные в HTML
            	// _test_setHtmlOptions(models,cars_selects['model']);
            	// _test_setHtmlOptions(modif,cars_selects['modif']);
                // 	_test_setHtmlOptions(years,cars_selects['year']);

	            _setHtmlOptions(models,cars_selects['model']);
	            _setHtmlOptions(modif,cars_selects['modif']);
	            _setHtmlOptions(years,cars_selects['year']);

                 
	            $container.find('select').trigger('refresh');
	            $container.stopAjaxLoading();
	            $container.find('input').eq(0).trigger('change');

                // сбрасываем фильтр по параметрам
            	skipSelectedAll();

                //let type = $('#type').data('type');
                // выставляем новые значения
                _findElements(result.data_base.MODEL); // ищем в фильрах совпадения по параметрам

            }


        });



    };


    // ищем элементы по параметрам из БД автомобилей
    var _findElements = function(model, type = 'shiny'){
    	console.log('--- findElements ---');
    	// console.log(model);
    	// console.log(type);

    	if(type == 'shiny'){
    		var elems = $('#tires_by_param input');
    	}
    	// console.log(elems);

		$.each(elems, function(index, value){
				// console.log("INDEX: " + index + "\n");
				// console.log(value);

				let element = $('#'+value.id + '-styler').siblings('.text');
				switch (element.data('property')){
					case 'DIAMETR':
				    	_setCheckbox(model.TYRES_FACTORY.DIAMETR, element.text(), value.id);
				    break;

				    case 'SHIRINA_PROFILA':
				    	_setCheckbox(model.TYRES_FACTORY.SHIRINA_PROFILA, element.text(), value.id);
				    break;    

				    case 'VISOTA_PROFILA':
				    	_setCheckbox(model.TYRES_FACTORY.VISOTA_PROFILA, element.text(), value.id);
				    break;
				}
		});
    }

	// устанавливаем чекбоксы
	var _setCheckbox = function(data, text, id){
		console.log('--- setCheckbox ---');
		data.forEach(function (value, i) {
         	if(value == text){
         	 	$('#'+id).prop('checked', true);
         	 	$('#'+id+'-styler').addClass('checked');
         	}
        });
	}

	// --- устаналиваем значения в HTML
	var _test_setHtmlOptions = function(elements,apendTo){
		// console.log('--- _setHtmlOptions ---');
		// console.log(elements);
		// console.log(apendTo);

	    var options = '';
	    if (typeof elements != 'undefined'){
	        $.each(elements,function(index,value){
	        	// console.log(value);
	            if (value) {
	                options += '<option value="' + value + '">' + value + '</option>'
	            }
	        });
	        apendTo.children().remove();
	        apendTo.append($(options));
	    }
	};

	var _test_getModels = function(elements){
	    var models = [];
	    $.each(elements,function(){
	        if (this.NAME){
	            models.push(this.NAME);
	        }
	    });
	    return models;
	};


})(jQuery);