<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
if (isset($arParams['SECTION_ID']) && intval($arParams['SECTION_ID']) > 0) {
	$dbSection = CIBlockSection::GetByID($arParams['SECTION_ID']);
	$dbSection->SetUrlTemplates();
	$arSection = $dbSection->GetNext();
	$arResult['FORM_ACTION'] = $arSection['SECTION_PAGE_URL'];
}
$arPropsID = array();
foreach ($arResult['ITEMS'] as $key => &$arItem) {
	$arPropsID[$arItem['ID']] = $key;
	if ($arItem['PROPERTY_TYPE'] == "E") {
		foreach ($arItem['VALUES'] as $id => &$arValue) {
			$arValue['ICON_CLASS'] = \Yenisite\Core\Catalog::getIconClassFromPropId($arItem['ID'], $id);
		}
		unset($arValue);
	}
// var_dump($arPropsID);
	
}
unset($arItem);

$dbProp = CIBlockProperty::GetList(array(), array(
	'ACTIVE' => 'Y',
	'IBLOCK_ID' => $arParams['IBLOCK_ID']
));

while ($arProp = $dbProp->Fetch()) {
	if (!isset($arPropsID[$arProp['ID']])) continue;
	if (isset($arProp['HINT']) && strlen($arProp['HINT']) > 0) {
		$decoded = json_decode($arProp['HINT'], true);
		if (isset($decoded)) {
			$arResult['ITEMS'][$arPropsID[$arProp['ID']]]['HINT'] = $decoded;
		}
	}

	// var_dump($arProp);
	// var_dump($arProp["NAME"]);
}


$arrFilter = $GLOBALS[$arParams['FILTER_NAME']];
if (is_array($arrFilter) && count($arrFilter) > 0) {
	foreach ($arrFilter as $key => $val) {
		if (strpos($key,'=PROPERTY_') !== false) {
			$propId = str_replace('=PROPERTY_','',$key);
			foreach ($val as $valueId) {
				$arResult['CURRENT_VALUES'][] = array(
					'CONTROL_NAME' => $arResult['ITEMS'][$propId]['VALUES'][$valueId]['CONTROL_NAME'],
					'VALUE' => $arResult['ITEMS'][$propId]['VALUES'][$valueId]['VALUE'],
					'HTML_VALUE' => $arResult['ITEMS'][$propId]['VALUES'][$valueId]['HTML_VALUE'],
				);
			};
		}
	}
}

// var_dump($arResult['FILTER_URL']);
// var_dump($arResult['SECTION']['ID']);
// $SECTION_CODE = 
// $rsParentSection = CIBlockSection::GetByID($arResult['SECTION']['ID']);
// if($arParentSection = $rsParentSection->GetNext()){ 
// 	if($arParentSection["CODE"]=='gruzovye'){

// 	}
// }

// --- удаляем лишние парамтры для умного фильра
// раздел диски
if($arResult['SECTION']['ID'] == STALNYE){
	foreach($arResult["ITEMS"] as $key=>$arItem){
		if($arItem["CODE"] == 'CAR_MARK' || $arItem["CODE"] == 'CAR_MODEL' || $arItem["CODE"] == 'YEAR' || $arItem["CODE"] == 'CAR_MODIFICATION'){
			unset($arResult["ITEMS"][$key]);
		}
	}
}

//if($arResult['SECTION']['ID'] == DISKI){
	// foreach($arResult["ITEMS"] as $key=>$arItem){
	// 	if($arItem["CODE"] == 'MANUFACTURER'){
	// 		unset($arResult["ITEMS"][$key]);
	// 	}
	// }
//}


// раздел шины
// if($arResult['SECTION']['ID'] == GRUZOVYE){
// 	foreach($arResult["ITEMS"] as $key=>$arItem){
// 		if($arItem["CODE"] !== 'AXLE'){
// 			unset($arResult["ITEMS"][$key]);
// 		}
// 	}
// }

if($arResult['SECTION']['ID'] == SHINY || $arResult['SECTION']['ID'] == LEGKOVYE || $arResult['SECTION']['ID'] == LEGKOGRUZOVYE || $arResult['SECTION']['ID'] == FOUR_FOUR){
	foreach($arResult["ITEMS"] as $key=>$arItem){

		if($arItem["CODE"] !== 'MANUFACTURER' && $arItem["CODE"] !== 'SEASON' && $arItem["CODE"] !== 'SHIRINA_PROFILA' && $arItem["CODE"] !== 'VISOTA_PROFILA' && $arItem["CODE"] !== 'DIAMETR'){
			unset($arResult["ITEMS"][$key]);
		}

	}
}

 


