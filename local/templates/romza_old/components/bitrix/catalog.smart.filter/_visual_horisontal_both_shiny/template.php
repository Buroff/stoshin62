<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$rnd = $this->randString();
use Yenisite\Core\Tools;
use \Yenisite\Shinmarket\ManagerCalc;

$bHasTablTX = CRZShinmarketSettings::isTiersSolution() && ManagerCalc::tablesExist();

// echo $arParams['TAB_TITLE'];
// echo $arParams['SECTION_ID'];
?>


<!--suppress Annotator --> 
<div style="display: none;" id="type" data-type="<?=$arParams['TYPE']?>"></div>
 <form name="<?= $arResult["FILTER_NAME"]."_form"?>" action="<?= $arResult["FORM_ACTION"]?>" method="get"
                                class="seach_form tyres filterHorizontal" data-rnd="<?=$rnd?>" data-arparams="<?=Tools::GetEncodedArParams($arParams)?>" id="search_tires">

					 <ul class="-seach_form__tabs_list nav nav-tabs custom-nav" role="tablist">
					                       <li class="-seach_form__tabs_item -tyres active">
					                           <a href="#tires_by_param" role="tab" data-toggle="tab" aria-expanded="true">
					                               <span class="icon icon_tyrebold"></span>
					                               <span class="seach_form__tabs_item__inner"> Подбор по параметрам</span>
					                           </a>
					                       </li>
					                       <li class="-seach_form__tabs_item">
					                           <a href="#tires_by_car" role="tab" data-toggle="tab" aria-expanded="false">
					                               <span class="icon icon_disk2"></span>
					                               <span class="seach_form__tabs_item__inner">Подбор по автомобилю</span>
					                           </a>
					                       </li>
					                   </ul>
                   
                   
                   <div class="custom-seach-form-tabs-content -seach_form__tabs_content tab-content">
                       

                       <div class="tab-pane active" id="tires_by_param">

                           
                            <div class="collapsed_area">

                                <div class="seach_form__inner seach_form__inner_type">
                                        <?if ($bHasTablTX){
                                           //  include 'tx_selects_fo_base.php';
                                        }?>

                                  <?foreach($arResult["HIDDEN"] as $arItem):?>
                                    <input
                                      type="hidden"
                                      name="<?= $arItem["CONTROL_NAME"]?>"
                                      id="<?= $rnd.$arItem["CONTROL_ID"]?>"
                                      value="<?= $arItem["HTML_VALUE"]?>"
                                      />

                                  <?endforeach;?>
                                  <?foreach($arResult["ITEMS"] as $arItem):?>
                                    <?if($arItem['CODE'] != "SEASON" || empty($arItem["VALUES"])) continue;?>
                                    <div class="tires-types-selector">
                                      <?foreach($arItem["VALUES"] as $val => $ar):?>
                                        <input <?= $ar["DISABLED"] ? 'disabled': ''?>
                                          class="tires-types-selector__input" type="checkbox"
                                          value="<?= $ar["HTML_VALUE"]?>"
                                          name="<?= $ar["CONTROL_NAME"]?>"
                                          id="<?= $rnd.$ar["CONTROL_ID"]?>"
                                          <?= $ar["CHECKED"]? 'checked="checked"': ''?>>
                                        <?
                                        switch ($ar['ICON_CLASS']) {
                                          case "icon_clear3":
                                            $seasonName = "summer";
                                            break;
                                          case "icon_snowflake":
                                            $seasonName = "winter";
                                            break;
                                          case "icon_screwsnow":
                                            $seasonName = "winter-pinned";
                                            break;
                                          case "icon_cloudy1":
                                            $seasonName = "all-seasonal";
                                            break;
                                          default:
                                            $seasonName = "";
                                        }
                                        ?>
                                        <label class="tires-types-selector__item <?=$seasonName?>" for="<?= $rnd.$ar["CONTROL_ID"]?>">
                                          <span class="item_icon icon <?=$ar['ICON_CLASS']?>"></span>
                                          <span class="item_title"><?= $ar["VALUE"];?></span>
                                        </label>
                                        <input
                                                        type="radio"
                                                        name="<?= $ar["CONTROL_NAME"]?>"
                                                        id="<?= $ar["CONTROL_ID"]?>"
                                                        value="<?= $ar["HTML_VALUE"]?>"
                                                        />
                                      <?endforeach?>
                                    </div>
                                  <?endforeach;?>
                                </div>
                                <?
                                $counter = 0;
                                $delim = 4;
                                ?>
                                <?foreach($arResult["ITEMS"] as $key=>$arItem):?>
                                      <?
                                      // var_dump($arItem);
                                     // if($arItem['CODE'] == 'MANUFACTURER'){
                                         // var_dump($arItem['CODE']);
                                                                            
                                      //}

                                      ?>
                                <?if($arItem["PROPERTY_TYPE"] == "N" ):?>
                                  <?
                                  if (!$arItem["VALUES"]["MIN"]["VALUE"] || !$arItem["VALUES"]["MAX"]["VALUE"] || $arItem["VALUES"]["MIN"]["VALUE"] == $arItem["VALUES"]["MAX"]["VALUE"])
                                    continue;
                                  $minVal = intval($arItem["VALUES"]["MIN"]["VALUE"]);
                                  if (isset($arItem["VALUES"]["MIN"]["HTML_VALUE"])) {
                                    $minVal = intval($arItem["VALUES"]["MIN"]["HTML_VALUE"]);
                                  }
                                  $maxVal = intval($arItem["VALUES"]["MAX"]["VALUE"]);
                                  if (isset($arItem["VALUES"]["MAX"]["HTML_VALUE"])) {
                                    $maxVal = intval($arItem["VALUES"]["MAX"]["HTML_VALUE"]);
                                  }



//                                   if($arItem['CODE'] == 'MANUFACTURER'){
// var_dump($arItem['CODE']);
//                                   }
                                  ?>
                                  <div class="clearfix"></div>
                                  <div class="seach_form__inner seach_form__inner_price">
                                    <div class="seach_form__inner_price__head"></div>
                                    <div class="range_prices_wrapper">
                                      <?= $arItem["NAME"] , "&nbsp;" , GetMessage("RZ_OT") ?>
                                      <input name="<?= $arItem["VALUES"]["MIN"]["CONTROL_NAME"] ?>"
                                           data-val="<?= $arItem["VALUES"]["MIN"]["VALUE"] ?>"
                                           value="<?= $arItem["VALUES"]["MIN"]["HTML_VALUE"] ?>"
                                           type="text" class="form-control form-control-small price_from">
                                      <?= GetMessage("RZ_DO") ?>
                                      <input name="<?= $arItem["VALUES"]["MAX"]["CONTROL_NAME"] ?>"
                                           data-val="<?= $arItem["VALUES"]["MAX"]["VALUE"] ?>"
                                           value="<?= $arItem["VALUES"]["MAX"]["HTML_VALUE"] ?>"
                                           type="text" class="form-control form-control-small price_to">
                                    </div>
                                    <div class="range_slider_wrapper">
                                      <div class="js-price-range-slider range_slider"></div>
                                    </div>
                                  </div>
                                <?elseif(!empty($arItem["VALUES"]) && !isset($arItem["PRICE"]) && $arItem['CODE'] != "SEASON"):?>
                                <?if ($counter == 0 || $counter % $delim == 0):?>
                                <?if(false):?><div><?endif;?>
                                  <?if($counter != 0 && $counter % $delim == 0):?></div><!--.sidebar_filter__inner_group--><?endif;?>
                                <div class="seach_form__inner seach_form__inner_params">
                                  <?endif;?>
                                  <div class="column">
                                    <div class="field_head">
                                      <div class="field_head__main">
                                        <div class="field_head__main__inner">
                                          <?=$arItem["NAME"]?>
                                          <?if(isset($arItem['HINT']['TEXT'])):?>
                                            <button class="brand-primary-color show_tip" type="button" data-toggle="popover"
                                                data-tip="#filter_pop_<?=$rnd.$arItem['ID']?>">?</button>
                                            <div id="filter_pop_<?=$rnd.$arItem['ID']?>" style="display: none" >
                                              <div class="popover_inner <?=$arItem['HINT']['CLASS']?>">
                                                <div class="popover_tip"><?=$arItem['HINT']['TEXT']?></div>
                                                <?if(isset($arItem['HINT']['SUBTEXT'])):?>
                                                  <div class="popover_tip__note">
                                                    <?=$arItem['HINT']['SUBTEXT']?>
                                                  </div>
                                                <?endif;?>
                                              </div>
                                            </div>
                                          <?endif;?>
                                        </div>
                                      </div>
                                      <div class="field_head__sub"></div>
                                    </div>
                                    <div>
                                      <div class="jq-selectbox-multy jqselect">
                                        <div class="jq-selectbox-multy__select" style="position: relative">
                                          <?$arFirst = array_shift($arItem["VALUES"]);?>
                                          <div style="width: 152px;" class="jq-selectbox-multy__select-text">
                                           <?if($arItem['CODE']=='DIAMETR'):?>
                                                      <?$arFirst["VALUE"] = 'R'.$arFirst["VALUE"];?>
                                            <?endif;?>
                                            <label for="<?= $rnd.$arFirst["CONTROL_ID"]?>">
                                            
                                              <input type="checkbox"
                                                   value="<?= $arFirst["HTML_VALUE"]?>"
                                                   name="<?= $arFirst["CONTROL_NAME"]?>"
                                                   id="<?= $rnd.$arFirst["CONTROL_ID"]?>"
                                                <?= $arFirst["CHECKED"]? 'checked="checked"': ''?>
                                                   <?if ($arFirst["DISABLED"]):?>disabled<?endif?>
                                                />
                                              <span class="text"><?= $arFirst["VALUE"];?></span>
                                            </label>
                                          </div>
                                          <div class="jq-selectbox-multy__trigger">
                                            <div class="jq-selectbox-multy__trigger-arrow"></div>
                                          </div>
                                        </div>
                                        <div class="jq-selectbox-multy__dropdown">
                                          <ul>
                                            <?foreach($arItem["VALUES"] as $val => $ar):?>
                                             <?if($arItem['CODE']=='DIAMETR'):?>
                                                      <?$ar["VALUE"] = 'R'.$ar["VALUE"];?>
                                                    <?endif;?>
                                              <li>
                                                <label for="<?= $rnd.$ar["CONTROL_ID"]?>">
                                                  <input type="checkbox"
                                                       value="<?= $ar["HTML_VALUE"]?>"
                                                       name="<?= $ar["CONTROL_NAME"]?>"
                                                       id="<?= $rnd.$ar["CONTROL_ID"]?>"
                                                    <?= $ar["CHECKED"]? 'checked="checked"': ''?>
                                                       <?if ($ar["DISABLED"]):?>disabled<?endif?>
                                                    />

                                      <!-- !!!  -->
                                      <?
                                        switch ($arItem['CODE']) {
                                            
                                            case "DIAMETR":
                                                  $paramType = "DIAMETR";
                                              break;

                                              case "SHIRINA_PROFILA":
                                                 $paramType ="SHIRINA_PROFILA";
                                              break;

                                              case "VISOTA_PROFILA":
                                                  $paramType = "VISOTA_PROFILA";
                                              break;

                                              default:
                                                $paramType = '';

                                        }
                                      ?>
                                                  <span class="text" data-property="<?= $paramType;?>"><?= $ar["VALUE"];?></span>
                                                </label>
                                              </li>
                                            <?endforeach;?>
                                          </ul>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <?++$counter;?>
                                  <?endif;?>
                                  <?endforeach;?>
                                  <?if(false):?><div><?endif;?>
                                    <?if($counter > 0):?></div><?endif?><!--.sidebar_filter__inner_group-->

                                  <?foreach($arResult["ITEMS"] as $key=>$arItem):
                                    $key = md5($key);
                                    ?>
                                    <?if(isset($arItem["PRICE"])):?>
                                    <?
                                    if (!$arItem["VALUES"]["MIN"]["VALUE"] || !$arItem["VALUES"]["MAX"]["VALUE"] || $arItem["VALUES"]["MIN"]["VALUE"] == $arItem["VALUES"]["MAX"]["VALUE"])
                                      continue;
                                    ?>
                                    <div class="seach_form__inner seach_form__inner_price">
                                      <div class="range_prices_wrapper">
                                        <?=GetMessage("RZ_TCENOVOJ_DIAPAZON_OT")?>
                                        <input name="<?= $arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>"
                                             id="<?= $rnd.$arItem["VALUES"]["MIN"]["CONTROL_ID"]?>"
                                             data-val="<?= $arItem["VALUES"]["MIN"]["VALUE"] ?>"
                                             value="<?= intval($arItem["VALUES"]["MIN"]["VALUE"])?>"
                                             type="text" class="form-control form-control-small price_from">
                                        <?=GetMessage("RZ_DO")?>
                                        <input name="<?= $arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>"
                                             id="<?= $rnd.$arItem["VALUES"]["MAX"]["CONTROL_ID"]?>"
                                             data-val="<?= $arItem["VALUES"]["MAX"]["VALUE"] ?>"
                                             value="<?= intval($arItem["VALUES"]["MAX"]["VALUE"])?>"
                                             type="text" class="form-control form-control-small price_to">
                                        <span class="b-rub"><?=GetMessage('SHIN_RUB_SYMBOL')?></span>
                                      </div>
                                      <div class="range_slider_wrapper">
                                        <div class="js-price-range-slider range_slider"></div>
                                      </div>
                                    </div>
                                  <?endif?>
                                  <?endforeach?>
                                </div>
                                



                                


                       </div>
                   
                        <div class="tab-pane" id="tires_by_car">
                              <div class="collapsed_area">
                        
                                      <?if ($bHasTablTX){
                                          // include 'tx_selects_fo_base.php';
                                          include 'vehicle.php';
                                      }?>

                             </div>

					                   
					             </div> 

                       <div class="seach_form__submit">
                                  
<!-- 
                                      <div class="collapse_btn_wrapper">
                                          <span class="btn btn-default btn-collapse">
                                            <span class="hide-filter">
                                              <span class="btn-inner"><?=GetMessage("RZ_SKRIT__FIL_TR")?></span>
                                              <span class="flaticon-arrow215"></span>
                                            </span>
                                            <span class="show-filter">
                                              <span class="btn-inner"><?=GetMessage("RZ_POKAZAT__FIL_TR")?></span>
                                              <span class="flaticon-arrow222"></span>
                                            </span>
                                          </span>
                                      </div>


 -->
                                    <div class="submit">

                                      <div class="submit_inner">
                                         <!-- <?if(!empty($arParams['ADDITIONAL_URL']) && !empty($arParams['ADDITIONAL_URL_TEXT'])):?>
                                           <div><a href="<?=$arParams['ADDITIONAL_URL']?>"><?=$arParams['ADDITIONAL_URL_TEXT']?></a></div>
                                         <?endif?> -->
                                         <div><button class="form_reset_button" type="reset"><?=GetMessage("RZ_SBROSIT__VSE_PARAMETRI_POISKA")?></button></div>
                                       </div> 


                                        <div class="submit_inner">
                                          <button id="set_filter" class="btn btn-default btn-find" type="submit" name="set_filter" value="Y"><?=GetMessage("RZ_PODOBRAT_")?></button>
                                        </div>
                                        
                                        <!--       
                                        <a href="#" id="check_param">check_param</a>
                                        <a href="#" id="skip_param">skip_param</a> 
                                        -->
                                        <!-- <a href="#" id="set_param">set param</a> -->
                                        <!-- <a href="#" id="skip_param">skip_param</a>  -->

                                    </div>
                                    <p class="backend-cnt-products found-product hidden"><?=GetMessage('RZ_FOUNDED')?>: <span class="text"></span></p>
                                
                      </div>
                            
                   
                   </div>
</form>