(typeof(jQuery) != 'undefined')
&& (function ($) {
	var $rangeSlider = $(".js-price-range-slider");
	var elementNums = false;

	$rangeSlider.each(function () {
		var $this = $(this),
			$parent = $this.closest('.seach_form__inner_price'),
			$priceTo = $parent.find('.price_to:eq(0)'),
			$priceFrom = $parent.find('.price_from:eq(0)'),
			priceMinVal = Math.floor(parseFloat($priceFrom.data('val'))),
			priceMaxVal = Math.ceil(parseFloat($priceTo.data('val'))),
			priceToVal = $priceTo.val(),
			priceFromVal = $priceFrom.val();
		if (priceToVal.length == 0) {
			priceToVal = priceMaxVal;
		}
		if (priceFromVal.length == 0) {
			priceFromVal = priceMinVal;
		} 
		
		$this.noUiSlider({
			start: [priceFromVal, priceToVal],
			connect: true,
			step: 1,
			range: {
				'min': priceMinVal,
				'max': priceMaxVal
			},
			format: wNumb({
				decimals: 0
			})
		});
		$this.on({
			set: function (e, vals) {
				$priceFrom.val('');
				$priceTo.val('');
			},
			change: function (e, vals) {
				if ($priceFrom.val().length == 0 || $priceTo.val().length == 0) {
					$priceFrom.val(vals[0]);
					$priceTo.val(vals[1]);
					$priceTo.trigger('change');
				}
			}
		});

		var triggerSliderInput = function (val) {
			var $this = $(this);
			if ($this.val() != val && $this.val().length > 0) {
			$this.val(val);
				if (sliderTrigger != null) {
					clearTimeout(sliderTrigger);
				}
				sliderTrigger = setTimeout(function () {
					$this.trigger('change');
				}, 700);
			}
		};
		$this.Link('lower').to($priceFrom, function (val) {
			triggerSliderInput.bind(this, val);
		});
		$this.Link('upper').to($priceTo, function (val) {

			triggerSliderInput.bind(this, val);
		});

		$this.Link('lower').to('-inline-<div class="tooltip"></div>', function (value) {
			$(this).html('<span class="tooltip_inner">' + value + '</span>');
		});

		$this.Link('upper').to('-inline-<div class="tooltip"></div>', function (value) {
			$(this).html('<span class="tooltip_inner">' + value + '</span>');
		});
	});

	var $sectionFilters = $('.filterHorizontal');


	$sectionFilters.on('change', 'input', function (e) {
		var $sectionFilter = $(this).closest('form.filterHorizontal');
		var rnd = $sectionFilter.data('rnd');
		var data = $sectionFilter.serializeArray();
		data.push({name: 'arParams', value: $sectionFilter.data('arparams')});
		$sectionFilter.setAjaxLoading();
		$.ajax({
			url: rz.AJAX_DIR + "catalog_Filter.php",
			data: data,
			dataType: "json",
			success: function (json) {
				rz.FILTER_AJAX = [];
				$.each(json.ITEMS, function (key, val) {
					$.each(val.VALUES, function (valKey, valVal) {
						ajaxValuePrepare(valVal, rnd);
					});
				});
				$sectionFilter.stopAjaxLoading();
				$(document).trigger('catalog_applyFilter');
			}
		})
	});

	$sectionFilters.on('click', '.form_reset_button', function (e) {
		e.preventDefault();
		var $sectionFilter = $(this).closest('form.filterHorizontal');
		$sectionFilter[0].reset();
		var rnd = $sectionFilter.data('rnd');
		var $jsSlider = $sectionFilter.find('.js-price-range-slider');
		var priceToVal = $sectionFilter.find('.price_to:eq(0)').val(),
			priceFromVal = $sectionFilter.find('.price_from:eq(0)').val();
		$jsSlider.val([priceFromVal,priceToVal]);
		$sectionFilter.find('input:visible:eq(0)').trigger('change');

	});

	var ajaxValuePrepare = function (val, rnd) {
		var $curItem = $('#' + rnd + val.CONTROL_ID);
		var $itemStyler = $('#' + rnd + val.CONTROL_ID + "-styler");
		var lastsymb = val.CONTROL_NAME.substr(-3);
		var isNum = false;
		if (lastsymb == 'MAX' || lastsymb == "MIN") {
			isNum = true;
		}
		if ('DISABLED' in val && val.DISABLED == 1) {
			if ($itemStyler.length > 0) {
				$itemStyler.addClass('disabled');
			}
			$curItem.attr('disabled', 'disabled');
		} else {
			if ($itemStyler.length > 0) {
				$itemStyler.removeClass('disabled');
			}
			$curItem.removeAttr('disabled');
		}
	};


	// --- событие при изменение данных в списке выбора автомобиля
	var cars_selects = {'mark': '[name="tiers_makr_filter"]', 'model': '[name="tiers_model_filter"]','year':'[name="tiers_year_filter"]','modif':'[name="tiers_modif_filter"]'};
	// var $sectionFilters = $('.filterHorizontal');
    $.each(cars_selects, function (key, val) {
        cars_selects[key] = $sectionFilters.find(val);
    });
    
	cars_selects['mark'].on('change', function () {
        var vendor = $(this).find('option:selected').text();
        doAjaxGetValsOfCars('by_vendor',$(this),vendor);
    });
    cars_selects['model'].on('change', function () {
        var $this = $(this),
            vendor = $this.getSelOptFromForm(cars_selects['mark']).text(),
            model = $this.find('option:selected').text();
        doAjaxGetValsOfCars('by_vendor_model',$(this),vendor,model);
    });
    cars_selects['year'].on('change', function () {
        var $this = $(this),
            vendor = $this.getSelOptFromForm(cars_selects['mark']).text(),
            model = $this.getSelOptFromForm(cars_selects['model']).text(),
            year = $this.find('option:selected').text();
        doAjaxGetValsOfCars('by_vendor_model_year',$(this),vendor,model,year);
    });
    cars_selects['modif'].on('change', function () {
        var $this = $(this),
            vendor = $this.getSelOptFromForm(cars_selects['mark']).text(),
            model = $this.getSelOptFromForm(cars_selects['model']).text(),
            year = $this.getSelOptFromForm(cars_selects['year']).text(),
            modif = $this.find('option:selected').text();
        doAjaxGetValsOfCars('by_vendor_model_year_modif',$(this),vendor,model,year,modif);
    });


    // --- устаналиваем значения в HTML
	var _setHtmlOptions = function(elements,apendTo){
		console.log('--- _setHtmlOptions ---');
		console.log(elements);
		console.log(apendTo);

	    var options = '';
	    if (typeof elements != 'undefined'){
	        $.each(elements,function(index,value){
	        	// console.log(value);
	            if (value) {
	                options += '<option value="' + value + '">' + value + '</option>'
	            }
	        });
	        apendTo.children().remove();
	        apendTo.append($(options));
	    }
	};

	var _getModels = function(elements){

		// console.log(elements);

	    var models = [];
	    $.each(elements,function(){
	        if (this.NAME){
	            models.push(this.NAME);
	        }
	    });
	    return models;
	};

 // ищем элементы по параметрам из БД автомобилей
    var findElements = function(model, type){
    	console.log('--- findElements ---');
    	// console.log(model);
    	// console.log(type);

    	if(type == 'shiny'){
    		var elems = $('#tires_by_param input');
    	}
    	// console.log(elems);
    	console.log(elems.length);
    	var i = 0;
		$.each(elems, function(index, value){
				// console.log("INDEX: " + index + "\n");
				// console.log(value);

				let element = $('#'+value.id + '-styler').siblings('.text');
				let typeCheckbox = false;
				switch (element.data('property')){
					case 'DIAMETR':
						setCheckbox(model.TYRES_FACTORY.DIAMETR, element.text(), value.id, element.data('property'));
				    break;

				    case 'SHIRINA_PROFILA':
				    	setCheckbox(model.TYRES_FACTORY.SHIRINA_PROFILA, element.text(), value.id, element.data('property'));
				    break;    

				    case 'VISOTA_PROFILA':
				    	setCheckbox(model.TYRES_FACTORY.VISOTA_PROFILA, element.text(), value.id, element.data('property'));
				    break;
				}

			
			i++;

			if(i == elems.length){
				elementNums = true;
			}

			
		});
    }

	// устанавливаем чекбоксы
	var setCheckbox = function(data, text, id, typeCheckbox){
		console.log('--- setCheckbox ---');

		data.forEach(function (value, i) {
			if(typeCheckbox == 'DIAMETR'){
				value = 'R' + value;
			}
         	if(value == text){
         	 	$('#'+id).prop('checked', true);
         	 	$('#'+id+'-styler').addClass('checked');
         	}
        });
	}

    // --- Ajax на PHP скрипт
    var doAjaxGetValsOfCars = function(action,$this,strVendor,strModel,strYear,strModif){

	   	// сбрасываем фильтр по параметрам
        skipSelectedAll();
        elementNums = false;

        // тип шины или диски
        var type = $('#type').data('type');

        var data = {},
          $container = $this.closest('.filterHorizontal'),
            bHasInput = $container.find('[name="car-id"]').length ? true : false,
            $inputCar = bHasInput ? $container.find('[name="car-id"]') : $('<input type="hidden" name="car-id"/>');

	       if (typeof strVendor != 'undefined'){
	            data['vendor'] = strVendor;
	        }
	        if (typeof strModel != 'undefined'){
	            data['model'] = strModel;
	        }
	        if (typeof strYear != 'undefined'){
	            data['year'] = strYear;
	        }
	        if (typeof strModif != 'undefined'){
	            data['modif'] = strModif;
	        }

        data['action_get_info'] = action;
        data['type'] = type;
        $container.setAjaxLoading();

        $( "#set_filter" ).prop( "disabled", true);
        // $this.prop( "disabled", true);
 
        
        // получаем данные по шинам (ширина, высота и диаметр) по выбранному автомобилю 
        $.ajax({
        	url: rz.AJAX_DIR + '/car_filter/car_ajax.php',
        	data:data,
            dataType:'json',
            method: 'POST',
            success: function(result){

            	// var modif,years,carID,models,objData;
            	var car, models, years, modif;
	            	models = result.data_base.MODELS;
	            	modif = result.data_base.MODIFICATIONS;
	            	years = result.data_base.YEARS;

	            console.log('--- Ajax ---');
            	console.log(result.data_base.MODEL);
           	
                // выставляем новые значения
                findElements(result.data_base.MODEL, type); // ищем в фильрах совпадения по параметрам

                // выводим данные в HTML
		        _setHtmlOptions(models,cars_selects['model']);
		        _setHtmlOptions(modif,cars_selects['modif']);
		        _setHtmlOptions(years,cars_selects['year']);

                if(elementNums){
	                $container.find('select').trigger('refresh');
	                $container.stopAjaxLoading();
	                $container.find('input').eq(0).trigger('change');
	                
	                // снимаем неактивность с кнопки Подобрать
	                setTimeout(function(){
						  $( "#set_filter" ).prop( "disabled", false);
					}, 3000);
                }
               
            }

        });
    };



// кнопка убрать фильтр (чтобы надпись менялась)
$('.collapse_btn_wrapper').on('click', function(event){
	$('.seach_form__submit').find(".btn-inner").text(function(i, text){
	          return text === "ПОКАЗАТЬ ФИЛЬТР" ? "СКРЫТЬ ФИЛЬТР" : "ПОКАЗАТЬ ФИЛЬТР";
	 });
});



// --- TEST 

var $skipParam = $('#skip_param').on('click', function(event){
	console.log('--- skip_param ---');
	// skipSelected();
	skipSelectedAll();
});

var skipSelectedAll = function(){
 	console.log('--- skipSelected ---');
 	var elems = $('#tires_by_param input');
  	$.each(elems, function(index, value){
			// console.log(value.id);
			$('#'+value.id).prop('checked', false);
			$('#'+value.id+'-styler').removeClass('checked');
 	});
}

var skipSelected = function(){
 	console.log('--- skipSelected ---');
 	$("#KZyYNzarrFilter_194_1772422577").attr('checked', false);
 	$("#KZyYNzarrFilter_194_1772422577-styler").removeClass('checked');
}

var $checkParam = $('#check_param').on('click', function(){
	    console.log('--- check_param ---');
	    checkSelected();
});

var checkSelected = function(){
	console.log('--- checkSelected ---');
	$("#KZyYNzarrFilter_194_1772422577").attr('checked', true);
 	$("#KZyYNzarrFilter_194_1772422577-styler").addClass('checked');
}


// сбрасываем значение при переключении на вкладку Подбор по автомобилю
$("a[href='#tires_by_car']").on('click', function () {
	$('#tires [name="tiers_makr_filter"]').trigger("change");
});

})(jQuery);