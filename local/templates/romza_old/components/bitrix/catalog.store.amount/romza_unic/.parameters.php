<?php
global $arComponentParameters, $arValues;
$arComponentParameters['GROUPS']['RZ_VISUAL'] = array('NAME' => GetMessage('RZ_GROUP_VISUAL'), 'SORT' => 100);
$arComponentParameters['GROUPS']['RZ_COLORS'] = array('NAME' => GetMessage('RZ_GROUP_COLOR'), 'SORT' => 200);
// disable notneed params
$arTemplateParameters['USE_MIN_AMOUNT']['HIDDEN'] = 'Y';
$arTemplateParameters['MIN_AMOUNT']['HIDDEN'] = 'Y';
$arTemplateParameters['SHOW_GENERAL_STORE_INFORMATION']['HIDDEN'] = 'Y';
unset($arComponentParameters['PARAMETERS']['USE_MIN_AMOUNT']);
unset($arComponentParameters['PARAMETERS']['MIN_AMOUNT']);
unset($arComponentParameters['PARAMETERS']['SHOW_GENERAL_STORE_INFORMATION']);
//set need params for old visual editor
$arValues['USE_MIN_AMOUNT'] = 'N';

$arTemplateParameters = array(
	'WIDTH' => array(
		'PARENT' => 'RZ_VISUAL',
		'NAME' => GetMessage("RZ_WIDTH"),
		'TYPE' => 'STRING',
		'DEFAULT' => 'auto',
	),
	'VIEW_MODE' => array(
		'PARENT' => 'RZ_VISUAL',
		'NAME' => GetMessage('VIEW_MODE'),
		'DEFAULT' => 'COLS',
		'TYPE' => 'LIST',
		'VALUES' => array('COLS' => GetMessage('VIEW_MODE_COLS') ,'TEXT' => GetMessage('VIEW_MODE_TEXT'), 'NUMB' => GetMessage('VIEW_MODE_NUMBS'))
	),
	'POPUP_TRIGGER' => array(
		'PARENT' => 'RZ_VISUAL',
		'NAME' => GetMessage('POPUP_TRIGGER'),
		'DEFAULT' => 'COLS',
		'TYPE' => 'LIST',
		'VALUES' => array('CLICK' => GetMessage('POPUP_TRIGGER_CLICK') ,'HOVER' => GetMessage('POPUP_TRIGGER_HOVER'))
	),
	'MANY_VAL' => array(
		'PARENT' => 'RZ_VISUAL',
		'NAME' => GetMessage('MANY_VAL'),
		'DEFAULT' => "10",
		'TYPE' => 'STRING',
	),
	'AVERAGE_VAL' => array(
		'PARENT' => 'RZ_VISUAL',
		'NAME' => GetMessage('AVERAGE_VAL'),
		'DEFAULT' => "5",
		'TYPE' => 'STRING',
	),
	'MANY_NAME' => array(
		'PARENT' => 'RZ_VISUAL',
		'NAME' => GetMessage('MANY_NAME'),
		'DEFAULT' => GetMessage('MANY_NAME_DEFAULT'),
		'TYPE' => 'STRING',
	),
	'AVERAGE_NAME' => array(
		'PARENT' => 'RZ_VISUAL',
		'NAME' => GetMessage('AVERAGE_NAME'),
		'DEFAULT' => GetMessage('AVERAGE_NAME_DEFAULT'),
		'TYPE' => 'STRING',
	),
	'FEW_NAME' => array(
		'PARENT' => 'RZ_VISUAL',
		'NAME' => GetMessage('FEW_NAME'),
		'DEFAULT' => GetMessage('FEW_NAME_DEFAULT'),
		'TYPE' => 'STRING',
	),
	'NONE_NAME' => array(
		'PARENT' => 'RZ_VISUAL',
		'NAME' => GetMessage('NONE_NAME'),
		'DEFAULT' => GetMessage('NONE_NAME_DEFAULT'),
		'TYPE' => 'STRING',
	),
	'ONE_POPUP' => array(
		'PARENT' => 'RZ_VISUAL',
		'NAME' => GetMessage('ONE_POPUP'),
		'DEFAULT' => 'N',
		'TYPE' => 'CHECKBOX',
		'REFRESH' => 'Y',
	),
	'COLOR_NONE_BG' => array(
		'PARENT' => 'RZ_COLORS',
		'NAME' => GetMessage('RZ_COLOR_NONE_BG'),
		'TYPE' => 'COLORPICKER',
		'DEFAULT' => '#C0C0C0',
	),
	'COLOR_NONE_TEXT' => array(
		'PARENT' => 'RZ_COLORS',
		'NAME' => GetMessage('RZ_COLOR_NONE_TEXT'),
		'TYPE' => 'COLORPICKER',
		'DEFAULT' => '#000000',
	),
	'COLOR_MANY_BG' => array(
		'PARENT' => 'RZ_COLORS',
		'NAME' => GetMessage('RZ_COLOR_MANY_BG'),
		'TYPE' => 'COLORPICKER',
		'DEFAULT' => '#328D00',
	),
	'COLOR_MANY_TEXT' => array(
		'PARENT' => 'RZ_COLORS',
		'NAME' => GetMessage('RZ_COLOR_MANY_TEXT'),
		'TYPE' => 'COLORPICKER',
		'DEFAULT' => '#FFFFFF',
	),
	'COLOR_AVERAGE_BG' => array(
		'PARENT' => 'RZ_COLORS',
		'NAME' => GetMessage('RZ_COLOR_AVERAGE_BG'),
		'TYPE' => 'COLORPICKER',
		'DEFAULT' => '#FFA500',
	),
	'COLOR_AVERAGE_TEXT' => array(
		'PARENT' => 'RZ_COLORS',
		'NAME' => GetMessage('RZ_COLOR_AVERAGE_TEXT'),
		'TYPE' => 'COLORPICKER',
		'DEFAULT' => '#FFFFFF',
	),
	'COLOR_FEW_BG' => array(
		'PARENT' => 'RZ_COLORS',
		'NAME' => GetMessage('RZ_COLOR_FEW_BG'),
		'TYPE' => 'COLORPICKER',
		'DEFAULT' => '#FF0000',
	),
	'COLOR_FEW_TEXT' => array(
		'PARENT' => 'RZ_COLORS',
		'NAME' => GetMessage('RZ_COLOR_FEW_TEXT'),
		'TYPE' => 'COLORPICKER',
		'DEFAULT' => '#FFFFFF',
	),
);
if($arCurrentValues['ONE_POPUP'] == 'Y') {
	$arTemplateParameters['ONLY_NAME_IN_LIST'] = array(
		'PARENT' => 'RZ_VISUAL',
		'NAME' => GetMessage('ONLY_NAME_IN_LIST'),
		'DEFAULT' => 'Y',
		'TYPE' => 'CHECKBOX',
	);
	$arTemplateParameters['HAS_NAME'] = array(
		'PARENT' => 'RZ_VISUAL',
		'NAME' => GetMessage('HAS_NAME'),
		'DEFAULT' => GetMessage('HAS_NAME_DEFAULT'),
		'TYPE' => 'STRING',
	);
}
if (\Bitrix\Main\Loader::IncludeModule('yenisite.resizer2')) {
	$arSetList = array();
	$arSets = CResizer2Set::GetList();
	while ($ar = $arSets->Fetch()) {
		$arSetList[$ar['id']] = '[' . $ar['id'] . '] ' . $ar['NAME'];
	}
	$arTemplateParameters['RESIZER_SET'] = array(
		'PARENT' => 'ADDITIONAL_SETTINGS',
		'NAME' => GetMessage('RZ_RESIZER_SET'),
		'TYPE' => 'LIST',
		'VALUES' => $arSetList,
	);
} else {
	$arTemplateParameters['IMG_WIDTH'] = array(
		'PARENT' => 'ADDITIONAL_SETTINGS',
		'NAME' => GetMessage('RZ_IMG_WIDTH'),
		'TYPE' => 'STRING',
		'VALUES' => '400',
	);
	$arTemplateParameters['IMG_HEIGHT'] = array(
		'PARENT' => 'ADDITIONAL_SETTINGS',
		'NAME' => GetMessage('RZ_IMG_HEIGHT'),
		'TYPE' => 'STRING',
		'VALUES' => '200',
	);
}
