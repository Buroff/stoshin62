<div class="RzStore__item inline">
	<?
	list($class, $title, $style) = rzSwitchAmount($arItems['TOTAL_AMOUNT'],$arParams);
	$arStore['AMOUNT'] = $arItems['TOTAL_AMOUNT'];
	?>
	<div class="RzStore__popupUrl" data-title="<?= GetMessage('RZ_STORE_QUANTITY') ?>">
		<?
		if($arParams['ONLY_NAME_IN_LIST'] == 'Y') {
			if($arItems['TOTAL_AMOUNT'] > 0) {
				echo '<div class="RzStore__indicator text many"><span class="many">', $arParams['HAS_NAME'], '</span></div>';
			} else {
				echo '<div class="RzStore__indicator text none"><span class="none">', $arParams['NONE_NAME'], '</span></div>';
			}
		} else {
			include(__DIR__ . '/view_mode.php');
		}
		?>
		<div class="RzStore__popupBody">
			<? $bNoStore = true; foreach ($arItems["STORES"] as $arStore): ?>
				<? if ($arParams['SHOW_EMPTY_STORE'] == 'N' && intval($arStore['AMOUNT']) == 0) continue; ?>
				<? list($class, $title, $style) = rzSwitchAmount($arStore['AMOUNT'],$arParams); $bNoStore = false;?>
				<div class="RzStore__popupInline">
					<b><?= $arStore['TITLE'] ?>: </b>
					<? include(__DIR__ . '/view_mode.php') ?>
					<br/>
				</div>
			<? endforeach ?>
			<?if($bNoStore) echo GetMessage("RZ_NET_INFORMATCII_PO_OSTATKAM") ?>
		</div>
	</div>
</div>