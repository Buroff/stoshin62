<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
if (!\Bitrix\Main\Loader::includeModule('iblock'))
	return;
$boolCatalog = \Bitrix\Main\Loader::includeModule('catalog');

$arSKU = false;
$boolSKU = false;
if ($boolCatalog && (isset($arCurrentValues['IBLOCK_ID']) && 0 < intval($arCurrentValues['IBLOCK_ID'])))
{
	$arSKU = CCatalogSKU::GetInfoByProductIBlock($arCurrentValues['IBLOCK_ID']);
	$boolSKU = !empty($arSKU) && is_array($arSKU);
}

$arThemes = array();
if (\Bitrix\Main\ModuleManager::isModuleInstalled('bitrix.eshop'))
{
	$arThemes['site'] = GetMessage('CPT_BC_TPL_THEME_SITE');
}

$arThemes['blue'] = GetMessage('CPT_BC_TPL_THEME_BLUE');
$arThemes['green'] = GetMessage('CPT_BC_TPL_THEME_GREEN');
$arThemes['red'] = GetMessage('CPT_BC_TPL_THEME_RED');
$arThemes['wood'] = GetMessage('CPT_BC_TPL_THEME_WOOD');
$arThemes['yellow'] = GetMessage('CPT_BC_TPL_THEME_YELLOW');
$arThemes['black'] = GetMessage('CP_BC_TPL_THEME_BLACK');

$arViewMods = array('TEXT' => GetMessage('VIEW_MODE_TEXT'), 'BOTH' => GetMessage('VIEW_MODE_BOTH'),'PICTURE' => GetMessage('VIEW_MODE_PICTURE'));

/*$arFilterViewModeList = array(
	"VERTICAL" => GetMessage("CPT_BC_FILTER_VIEW_MODE_VERTICAL"),
	"HORIZONTAL" => GetMessage("CPT_BC_FILTER_VIEW_MODE_HORIZONTAL")
);*/

/*$arTemplateParameters = array(
	"SECTIONS_VIEW_MODE" => array(
		"PARENT" => "SECTIONS_SETTINGS",
		"NAME" => GetMessage('CPT_BC_SECTIONS_VIEW_MODE'),
		"TYPE" => "STRING",
		"VALUES" => $rz_options['switch_subsection_view'],
	),
	"SECTIONS_SHOW_PARENT_NAME" => array(
		"PARENT" => "SECTIONS_SETTINGS",
		"NAME" => GetMessage('CPT_BC_SECTIONS_SHOW_PARENT_NAME'),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "Y"
	)
);*/
$arTemplateParameters['FULL_DESCTRIPTION'] = array(
	'PARENT' => 'SECTIONS_SETTINGS',
	"NAME" => GetMessage("FULL_DESCTRIPTION"),
	"TYPE" => "CHECKBOX",
	"DEFAULT" => "N",
);
$arTemplateParameters['SECTION_COUNT_ELEMENTS'] = array(
	'PARENT' => 'SECTIONS_SETTINGS',
	"NAME" => GetMessage("SECTION_COUNT_ELEMENTS"),
	"TYPE" => "CHECKBOX",
	"DEFAULT" => "Y",
);
$arPosition = array('TOP' => GetMessage('SUBSECTION_POSITION_TOP'), 'BOTTOM' => GetMessage('SUBSECTION_POSITION_BOTTOM'),'ALL' => GetMessage('SUBSECTION_POSITION_ALL'), 'NONE' => GetMessage('SUBSECTION_POSITION_NONE'));
$arTemplateParameters['SUBSECTION_POSITION'] = array(
	'PARENT' => 'SECTIONS_SETTINGS',
	"NAME" => GetMessage("SUBSECTION_POSITION"),
	"TYPE" => "LIST",
	"DEFAULT" => "TOP",
	"VALUES" => $arPosition,
	"MULTIPLE" => "N",
);


if (isset($arCurrentValues['SECTIONS_VIEW_MODE']) && 'TILE' == $arCurrentValues['SECTIONS_VIEW_MODE'])
{
	$arTemplateParameters['SECTIONS_HIDE_SECTION_NAME'] = array(
		'PARENT' => 'SECTIONS_SETTINGS',
		'NAME' => GetMessage('CPT_BC_SECTIONS_HIDE_SECTION_NAME'),
		'TYPE' => 'CHECKBOX',
		'DEFAULT' => 'N'
	);
}

/*$arTemplateParameters["FILTER_VIEW_MODE"] = array(
	"PARENT" => "FILTER_SETTINGS",
	"NAME" => GetMessage('CPT_BC_FILTER_VIEW_MODE'),
	"TYPE" => "LIST",
	"VALUES" => $arFilterViewModeList,
	"DEFAULT" => "VERTICAL",
	"HIDDEN" => (!isset($arCurrentValues['USE_FILTER']) || 'N' == $arCurrentValues['USE_FILTER'])
);*/

$arTemplateParameters['TEMPLATE_THEME'] = array(
	'PARENT' => 'VISUAL',
	'NAME' => GetMessage("CP_BC_TPL_TEMPLATE_THEME"),
	'TYPE' => 'LIST',
	'VALUES' => $arThemes,
	'DEFAULT' => 'blue',
	'ADDITIONAL_VALUES' => 'Y'
);

if (isset($arCurrentValues['IBLOCK_ID']) && 0 < intval($arCurrentValues['IBLOCK_ID']))
{
	$arAllPropList = array();
	$arFilePropList = array(
		'-' => GetMessage('CP_BC_TPL_PROP_EMPTY')
	);
	$arListPropList = array(
		'-' => GetMessage('CP_BC_TPL_PROP_EMPTY')
	);
	$arHighloadPropList = array(
		'-' => GetMessage('CP_BC_TPL_PROP_EMPTY')
	);
	$rsProps = CIBlockProperty::GetList(
		array('SORT' => 'ASC', 'ID' => 'ASC'),
		array('IBLOCK_ID' => $arCurrentValues['IBLOCK_ID'], 'ACTIVE' => 'Y')
	);
	while ($arProp = $rsProps->Fetch())
	{
		$strPropName = '['.$arProp['ID'].']'.('' != $arProp['CODE'] ? '['.$arProp['CODE'].']' : '').' '.$arProp['NAME'];
		if ('' == $arProp['CODE'])
			$arProp['CODE'] = $arProp['ID'];
		$arAllPropList[$arProp['CODE']] = $strPropName;
		if ('F' == $arProp['PROPERTY_TYPE'])
			$arFilePropList[$arProp['CODE']] = $strPropName;
		if ('L' == $arProp['PROPERTY_TYPE'])
			$arListPropList[$arProp['CODE']] = $strPropName;
		if ('S' == $arProp['PROPERTY_TYPE'] && 'directory' == $arProp['USER_TYPE'] && CIBlockPriceTools::checkPropDirectory($arProp))
			$arHighloadPropList[$arProp['CODE']] = $strPropName;
	}

	$arTemplateParameters['ADD_PICT_PROP'] = array(
		'PARENT' => 'VISUAL',
		'NAME' => GetMessage('CP_BC_TPL_ADD_PICT_PROP'),
		'TYPE' => 'LIST',
		'MULTIPLE' => 'N',
		'ADDITIONAL_VALUES' => 'N',
		'REFRESH' => 'N',
		'DEFAULT' => '-',
		'VALUES' => $arFilePropList
	);
	$arTemplateParameters['LABEL_PROP'] = array(
		'PARENT' => 'VISUAL',
		'NAME' => GetMessage('CP_BC_TPL_LABEL_PROP'),
		'TYPE' => 'LIST',
		'MULTIPLE' => 'N',
		'ADDITIONAL_VALUES' => 'N',
		'REFRESH' => 'N',
		'DEFAULT' => '-',
		'VALUES' => $arListPropList
	);

	if ($boolSKU)
	{
		$arDisplayModeList = array(
			'N' => GetMessage('CP_BC_TPL_DML_SIMPLE'),
			'Y' => GetMessage('CP_BC_TPL_DML_EXT')
		);
		$arTemplateParameters['PRODUCT_DISPLAY_MODE'] = array(
			'PARENT' => 'VISUAL',
			'NAME' => GetMessage('CP_BC_TPL_PRODUCT_DISPLAY_MODE'),
			'TYPE' => 'LIST',
			'MULTIPLE' => 'N',
			'ADDITIONAL_VALUES' => 'N',
			'REFRESH' => 'Y',
			'DEFAULT' => 'N',
			'VALUES' => $arDisplayModeList
		);
		$arAllOfferPropList = array();
		$arFileOfferPropList = array(
			'-' => GetMessage('CP_BC_TPL_PROP_EMPTY')
		);
		$arTreeOfferPropList = array(
			'-' => GetMessage('CP_BC_TPL_PROP_EMPTY')
		);
		$rsProps = CIBlockProperty::GetList(
			array('SORT' => 'ASC', 'ID' => 'ASC'),
			array('IBLOCK_ID' => $arSKU['IBLOCK_ID'], 'ACTIVE' => 'Y')
		);
		while ($arProp = $rsProps->Fetch())
		{
			if ($arProp['ID'] == $arSKU['SKU_PROPERTY_ID'])
				continue;
			$arProp['USER_TYPE'] = (string)$arProp['USER_TYPE'];
			$strPropName = '['.$arProp['ID'].']'.('' != $arProp['CODE'] ? '['.$arProp['CODE'].']' : '').' '.$arProp['NAME'];
			if ('' == $arProp['CODE'])
				$arProp['CODE'] = $arProp['ID'];
			$arAllOfferPropList[$arProp['CODE']] = $strPropName;
			if ('F' == $arProp['PROPERTY_TYPE'])
				$arFileOfferPropList[$arProp['CODE']] = $strPropName;
			if ('N' != $arProp['MULTIPLE'])
				continue;
			if (
				'L' == $arProp['PROPERTY_TYPE']
				|| 'E' == $arProp['PROPERTY_TYPE']
				|| ('S' == $arProp['PROPERTY_TYPE'] && 'directory' == $arProp['USER_TYPE'] && CIBlockPriceTools::checkPropDirectory($arProp))
			)
				$arTreeOfferPropList[$arProp['CODE']] = $strPropName;
		}
		$arTemplateParameters['OFFER_ADD_PICT_PROP'] = array(
			'PARENT' => 'VISUAL',
			'NAME' => GetMessage('CP_BC_TPL_OFFER_ADD_PICT_PROP'),
			'TYPE' => 'LIST',
			'MULTIPLE' => 'N',
			'ADDITIONAL_VALUES' => 'N',
			'REFRESH' => 'N',
			'DEFAULT' => '-',
			'VALUES' => $arFileOfferPropList
		);
		$arTemplateParameters['OFFER_TREE_PROPS'] = array(
			'PARENT' => 'VISUAL',
			'NAME' => GetMessage('CP_BC_TPL_OFFER_TREE_PROPS'),
			'TYPE' => 'LIST',
			'MULTIPLE' => 'Y',
			'ADDITIONAL_VALUES' => 'N',
			'REFRESH' => 'N',
			'DEFAULT' => '-',
			'VALUES' => $arTreeOfferPropList
		);
	}
}

$arTemplateParameters['DETAIL_DISPLAY_NAME'] = array(
	'PARENT' => 'DETAIL_SETTINGS',
	'NAME' => GetMessage('CP_BC_TPL_DETAIL_DISPLAY_NAME'),
	'TYPE' => 'CHECKBOX',
	'DEFAULT' => 'Y'
);

$detailPictMode = array(
	'IMG' => GetMessage('DETAIL_DETAIL_PICTURE_MODE_IMG'),
	'POPUP' => GetMessage('DETAIL_DETAIL_PICTURE_MODE_POPUP'),
	/*	'MAGNIFIER' => GetMessage('DETAIL_DETAIL_PICTURE_MODE_MAGNIFIER'),
		'GALLERY' => GetMessage('DETAIL_DETAIL_PICTURE_MODE_GALLERY') */
);

$arTemplateParameters['DETAIL_DETAIL_PICTURE_MODE'] = array(
	'PARENT' => 'DETAIL_SETTINGS',
	'NAME' => GetMessage('CP_BC_TPL_DETAIL_DETAIL_PICTURE_MODE'),
	'TYPE' => 'LIST',
	'DEFAULT' => 'IMG',
	'VALUES' => $detailPictMode
);

$arTemplateParameters['DETAIL_ADD_DETAIL_TO_SLIDER'] = array(
	'PARENT' => 'DETAIL_SETTINGS',
	'NAME' => GetMessage('CP_BC_TPL_DETAIL_ADD_DETAIL_TO_SLIDER'),
	'TYPE' => 'CHECKBOX',
	'DEFAULT' => 'N'
);

$displayPreviewTextMode = array(
	'H' => GetMessage('CP_BC_TPL_DETAIL_DISPLAY_PREVIEW_TEXT_MODE_HIDE'),
	'E' => GetMessage('CP_BC_TPL_DETAIL_DISPLAY_PREVIEW_TEXT_MODE_EMPTY_DETAIL'),
	'S' => GetMessage('CP_BC_TPL_DETAIL_DISPLAY_PREVIEW_TEXT_MODE_SHOW')
);

$arTemplateParameters['DETAIL_DISPLAY_PREVIEW_TEXT_MODE'] = array(
	'PARENT' => 'DETAIL_SETTINGS',
	'NAME' => GetMessage('CP_BC_TPL_DETAIL_DISPLAY_PREVIEW_TEXT_MODE'),
	'TYPE' => 'LIST',
	'VALUES' => $displayPreviewTextMode,
	'DEFAULT' => 'E'
);

if ($boolCatalog)
{
/*	$arTemplateParameters['PRODUCT_SUBSCRIPTION'] = array(
		'PARENT' => 'VISUAL',
		'NAME' => GetMessage('CP_BC_TPL_PRODUCT_SUBSCRIPTION'),
		'TYPE' => 'CHECKBOX',
		'DEFAULT' => 'N',
	); */
	$arTemplateParameters['SHOW_DISCOUNT_PERCENT'] = array(
		'PARENT' => 'VISUAL',
		'NAME' => GetMessage('CP_BC_TPL_SHOW_DISCOUNT_PERCENT'),
		'TYPE' => 'CHECKBOX',
		'DEFAULT' => 'N',
		'REFRESH' => 'Y',
	);
	$arTemplateParameters['SHOW_OLD_PRICE'] = array(
		'PARENT' => 'VISUAL',
		'NAME' => GetMessage('CP_BC_TPL_SHOW_OLD_PRICE'),
		'TYPE' => 'CHECKBOX',
		'DEFAULT' => 'N',
	);
	$arTemplateParameters['DETAIL_SHOW_MAX_QUANTITY'] = array(
		'PARENT' => 'VISUAL',
		'NAME' => GetMessage('CP_BC_TPL_DETAIL_SHOW_MAX_QUANTITY'),
		'TYPE' => 'CHECKBOX',
		'DEFAULT' => 'N',
	);
}

$arTemplateParameters['MESS_BTN_BUY'] = array(
	'PARENT' => 'VISUAL',
	'NAME' => GetMessage('CP_BC_TPL_MESS_BTN_BUY'),
	'TYPE' => 'STRING',
	'DEFAULT' => GetMessage('CP_BC_TPL_MESS_BTN_BUY_DEFAULT')
);
$arTemplateParameters['MESS_BTN_ADD_TO_BASKET'] = array(
	'PARENT' => 'VISUAL',
	'NAME' => GetMessage('CP_BC_TPL_MESS_BTN_ADD_TO_BASKET'),
	'TYPE' => 'STRING',
	'DEFAULT' => GetMessage('CP_BC_TPL_MESS_BTN_ADD_TO_BASKET_DEFAULT')
);
$arTemplateParameters['MESS_BTN_COMPARE'] = array(
	'PARENT' => 'VISUAL',
	'NAME' => GetMessage('CP_BC_TPL_MESS_BTN_COMPARE'),
	'TYPE' => 'STRING',
	'DEFAULT' => GetMessage('CP_BC_TPL_MESS_BTN_COMPARE_DEFAULT')
);
$arTemplateParameters['MESS_BTN_DETAIL'] = array(
	'PARENT' => 'VISUAL',
	'NAME' => GetMessage('CP_BC_TPL_MESS_BTN_DETAIL'),
	'TYPE' => 'STRING',
	'DEFAULT' => GetMessage('CP_BC_TPL_MESS_BTN_DETAIL_DEFAULT')
);
$arTemplateParameters['MESS_NOT_AVAILABLE'] = array(
	'PARENT' => 'VISUAL',
	'NAME' => GetMessage('CP_BC_TPL_MESS_NOT_AVAILABLE'),
	'TYPE' => 'STRING',
	'DEFAULT' => GetMessage('CP_BC_TPL_MESS_NOT_AVAILABLE_DEFAULT')
);
$arTemplateParameters['DETAIL_USE_VOTE_RATING'] = array(
	'PARENT' => 'VISUAL',
	'NAME' => GetMessage('CP_BC_TPL_DETAIL_USE_VOTE_RATING'),
	'TYPE' => 'CHECKBOX',
	'DEFAULT' => 'N',
	'REFRESH' => 'Y'
);
if (isset($arCurrentValues['DETAIL_USE_VOTE_RATING']) && 'Y' == $arCurrentValues['DETAIL_USE_VOTE_RATING'])
{
	$arTemplateParameters['DETAIL_VOTE_DISPLAY_AS_RATING'] = array(
		'PARENT' => 'VISUAL',
		'NAME' => GetMessage('CP_BC_TPL_DETAIL_VOTE_DISPLAY_AS_RATING'),
		'TYPE' => 'LIST',
		'VALUES' => array(
			'rating' => GetMessage('CP_BC_TPL_DVDAR_RATING'),
			'vote_avg' => GetMessage('CP_BC_TPL_DVDAR_AVERAGE'),
		),
		'DEFAULT' => 'rating'
	);
}

if (IsModuleInstalled("blog"))
{
	$arTemplateParameters['DETAIL_USE_COMMENTS'] = array(
		'PARENT' => 'VISUAL',
		'NAME' => GetMessage('CP_BC_TPL_DETAIL_USE_COMMENTS'),
		'TYPE' => 'CHECKBOX',
		'DEFAULT' => 'N',
		'REFRESH' => 'Y'
	);
	if (isset($arCurrentValues['DETAIL_USE_COMMENTS']) && 'Y' == $arCurrentValues['DETAIL_USE_COMMENTS'])
	{
		$boolRus = false;
		$langBy = "id";
		$langOrder = "asc";
		$rsLangs = CLanguage::GetList($langBy, $langOrder, array('ID' => 'ru',"ACTIVE" => "Y"));
		if ($arLang = $rsLangs->Fetch())
		{
			$boolRus = true;
		}

		$arTemplateParameters['DETAIL_BLOG_USE'] = array(
			'PARENT' => 'VISUAL',
			'NAME' => GetMessage('CP_BC_TPL_DETAIL_BLOG_USE'),
			'TYPE' => 'CHECKBOX',
			'DEFAULT' => 'N',
			'REFRESH' => 'Y'
		);
		if ($boolRus)
		{
			$arTemplateParameters['DETAIL_VK_USE'] = array(
				'PARENT' => 'VISUAL',
				'NAME' => GetMessage('CP_BC_TPL_DETAIL_VK_USE'),
				'TYPE' => 'CHECKBOX',
				'DEFAULT' => 'N',
				'REFRESH' => 'Y'
			);

			if (isset($arCurrentValues['DETAIL_VK_USE']) && 'Y' == $arCurrentValues['DETAIL_VK_USE'])
			{
				$arTemplateParameters['DETAIL_VK_API_ID'] = array(
					'PARENT' => 'VISUAL',
					'NAME' => GetMessage('CP_BC_TPL_DETAIL_VK_API_ID'),
					'TYPE' => 'STRING',
					'DEFAULT' => 'API_ID'
				);
			}
		}
		$arTemplateParameters['DETAIL_FB_USE'] = array(
			'PARENT' => 'VISUAL',
			'NAME' => GetMessage('CP_BC_TPL_DETAIL_FB_USE'),
			'TYPE' => 'CHECKBOX',
			'DEFAULT' => 'N',
			'REFRESH' => 'Y'
		);

		if (isset($arCurrentValues['DETAIL_FB_USE']) && 'Y' == $arCurrentValues['DETAIL_FB_USE'])
		{
			$arTemplateParameters['DETAIL_FB_APP_ID'] = array(
				'PARENT' => 'VISUAL',
				'NAME' => GetMessage('CP_BC_TPL_DETAIL_FB_APP_ID'),
				'TYPE' => 'STRING',
				'DEFAULT' => ''
			);
		}
	}
}

if (IsModuleInstalled("highloadblock"))
{
	$arTemplateParameters['DETAIL_BRAND_USE'] = array(
		'PARENT' => 'VISUAL',
		'NAME' => GetMessage('CP_BC_TPL_DETAIL_BRAND_USE'),
		'TYPE' => 'CHECKBOX',
		'DEFAULT' => 'N',
		'REFRESH' => 'Y'
	);

	if (isset($arCurrentValues['DETAIL_BRAND_USE']) && 'Y' == $arCurrentValues['DETAIL_BRAND_USE'])
	{
		$arTemplateParameters['DETAIL_BRAND_PROP_CODE'] = array(
			'PARENT' => 'VISUAL',
			"NAME" => GetMessage("CP_BC_TPL_DETAIL_PROP_CODE"),
			"TYPE" => "LIST",
			'PARENT' => 'VISUAL',
			"VALUES" => $arHighloadPropList
		);
	}
}

if (isset($arCurrentValues['SHOW_TOP_ELEMENTS']) && 'Y' == $arCurrentValues['SHOW_TOP_ELEMENTS'])
{
	$arTopViewModeList = array(
		'BANNER' => GetMessage('CPT_BC_TPL_VIEW_MODE_BANNER'),
		'SLIDER' => GetMessage('CPT_BC_TPL_VIEW_MODE_SLIDER'),
		'SECTION' => GetMessage('CPT_BC_TPL_VIEW_MODE_SECTION')
	);
	$arTemplateParameters['TOP_VIEW_MODE'] = array(
		'PARENT' => 'TOP_SETTINGS',
		'NAME' => GetMessage('CPT_BC_TPL_TOP_VIEW_MODE'),
		'TYPE' => 'LIST',
		'VALUES' => $arTopViewModeList,
		'MULTIPLE' => 'N',
		'DEFAULT' => 'SECTION',
		'REFRESH' => 'Y'
	);
	if (isset($arCurrentValues['TOP_VIEW_MODE']) && ('SLIDER' == $arCurrentValues['TOP_VIEW_MODE'] || 'BANNER' == $arCurrentValues['TOP_VIEW_MODE']))
	{
		$arTemplateParameters['TOP_ROTATE_TIMER'] = array(
			'PARENT' => 'TOP_SETTINGS',
			'NAME' => GetMessage('CPT_BC_TPL_TOP_ROTATE_TIMER'),
			'TYPE' => 'STRING',
			'DEFAULT' => '30'
		);
	}
}

if (\Bitrix\Main\Loader::IncludeModule("yenisite.resizer2")) {
	$resizer_sets_list = array();
	$arSets = CResizer2Set::GetList();
	while ($arr = $arSets->Fetch()) {
		$resizer_sets_list[$arr["id"]] = "[" . $arr["id"] . "] " . $arr["NAME"];
	}

	global $arComponentParameters;

	$arComponentParameters["GROUPS"]["RESIZER_SETS"] = array(
		"NAME" => GetMessage("RESIZER_SETS"),
		"SORT" => 1
	);

	$arTemplateParameters["RESIZER_PRODUCT"] = array(
			"PARENT" => "RESIZER_SETS",
			"NAME" => GetMessage("RESIZER_PRODUCT"),
			"TYPE" => "LIST",
			"VALUES" => $resizer_sets_list,
			"DEFAULT" => "2",
		);
	$arTemplateParameters["RESIZER_PRODUCT_BIG"] = array(
			"PARENT" => "RESIZER_SETS",
			"NAME" => GetMessage("RESIZER_PRODUCT_BIG"),
			"TYPE" => "LIST",
			"VALUES" => $resizer_sets_list,
			"DEFAULT" => "1",
		);
	$arTemplateParameters["RESIZER_PRODUCT_GALLERY"] = array(
			"PARENT" => "RESIZER_SETS",
			"NAME" => GetMessage("RESIZER_PRODUCT_GALLERY"),
			"TYPE" => "LIST",
			"VALUES" => $resizer_sets_list,
			"DEFAULT" => "1",
		);
	$arTemplateParameters["RESIZER_PRODUCT_THUMB"] = array(
			"PARENT" => "RESIZER_SETS",
			"NAME" => GetMessage("RESIZER_PRODUCT_THUMB"),
			"TYPE" => "LIST",
			"VALUES" => $resizer_sets_list,
			"DEFAULT" => "4",
		);
}
if(\Bitrix\Main\Loader::includeModule('forum')) {
	\Bitrix\Main\Loader::includeModule('iblock');
	$arIblockType = array();
	$rsType = CIBlockType::GetList();
	while ($arType = $rsType->Fetch()) {
		if ($arIBType = CIBlockType::GetByIDLang($arType["ID"], LANG)) {
			$arIblockType[$arType['ID']] = '[' . $arType['ID'] . '] ' . $arIBType["NAME"];
		}
	}
	$arComponentParameters['GROUPS']['COMMENTS_IBLOCK'] = array(
		'NAME' => GetMessage('COMMENTS_IBLOCK'),
		'SORT' => 2
	);
	$arTemplateParameters['RZ_COMMENTS_IBLOCK_TYPE'] = array(
		'PARENT' => 'COMMENTS_IBLOCK',
		'NAME' => GetMessage('RZ_COMMENTS_IBLOCK_TYPE'),
		'TYPE' => 'LIST',
		'VALUES' => $arIblockType,
		'REFRESH' => 'Y',
	);
	if(strlen($arCurrentValues['RZ_COMMENTS_IBLOCK_TYPE']) > 0 ) {
		$arIblockId = array();
		/** @noinspection PhpDynamicAsStaticMethodCallInspection */
		$rsIbId = CIBlock::GetList(array(), array('TYPE' => $arCurrentValues['RZ_COMMENTS_IBLOCK_TYPE']));
		while ($arIbId = $rsIbId->Fetch()) {
			$arIblockId[$arIbId['ID']] = '[' . $arIbId['ID'] . '] ' . $arIbId["NAME"];
		}

		$arTemplateParameters['RZ_COMMENTS_IBLOCK_ID'] = array(
			'PARENT' => 'COMMENTS_IBLOCK',
			'NAME' => GetMessage('RZ_COMMENTS_IBLOCK_ID'),
			'TYPE' => 'LIST',
			'VALUES' => $arIblockId,
			'REFRESH' => 'Y',
		);
	}
	if(intval($arCurrentValues['RZ_COMMENTS_IBLOCK_ID']) > 0 ) {
		$arProps = array();
		$rsProps = CIBlockProperty::GetList(array(),
			array(
				'ACTIVE' => 'Y',
				'IBLOCK_ID' => $arCurrentValues['RZ_COMMENTS_IBLOCK_ID']
			));
		while ($arTmpProp = $rsProps->Fetch()) {
			$arProps[$arTmpProp['CODE']] = '[' . $arTmpProp['CODE'] . '] ' . $arTmpProp['NAME'];
		}
		$arTemplateParameters['RZ_COMMENTS_NAME_FIELD'] = array(
			'PARENT' => 'COMMENTS_IBLOCK',
			'NAME' => GetMessage('RZ_COMMENTS_NAME_FIELD'),
			'TYPE' => 'LIST',
			'VALUES' => $arProps,
		);
		$arTemplateParameters['RZ_COMMENTS_SUCCESS_TEXT'] = array(
			'PARENT' => 'COMMENTS_IBLOCK',
			'NAME' => GetMessage('RZ_COMMENTS_SUCCESS_TEXT'),
			'TYPE' => 'STRING',
			'DEFAULT' => GetMessage('RZ_COMMENTS_SUCCESS_DEFAULT')
		);
		$arTemplateParameters['RZ_COMMENTS_PRINT_FIELDS'] = array(
			'PARENT' => 'COMMENTS_IBLOCK',
			'NAME' => GetMessage('RZ_COMMENTS_PRINT_FIELDS'),
			'TYPE' => 'LIST',
			'VALUES' => $arProps,
			'MULTIPLE' => 'Y',
		);
		$arTemplateParameters['RZ_COMMENTS_NAME'] = array(
			'PARENT' => 'COMMENTS_IBLOCK',
			'NAME' => GetMessage('RZ_COMMENTS_NAME'),
			'TYPE' => 'LIST',
			'VALUES' => $arProps,

		);
		$arTemplateParameters['RZ_COMMENTS_EMAIL'] = array(
			'PARENT' => 'COMMENTS_IBLOCK',
			'NAME' => GetMessage('RZ_COMMENTS_EMAIL'),
			'TYPE' => 'LIST',
			'VALUES' => $arProps,

		);
		$arTemplateParameters['RZ_COMMENTS_PHONE'] = array(
			'PARENT' => 'COMMENTS_IBLOCK',
			'NAME' => GetMessage('RZ_COMMENTS_PHONE'),
			'TYPE' => 'LIST',
			'VALUES' => $arProps,
		);
	}
}
if (\Bitrix\Main\Loader::includeModule('sale') && \Bitrix\Main\Loader::includeModule('yenisite.oneclick')) {
	global $arComponentParameters;

	$arComponentParameters['GROUPS']['ONECLICK'] = array('NAME' => GetMessage('RZ_ONECLICK_GROUP'), 'SORT' => 100);

	define('MAX_LINES_IN_LIST', 6);
	$arPersonTypes = array();
	$rsPersonTypes = CSalePersonType::GetList(array(), array('ACTIVE' => 'Y'));
	while ($ar = $rsPersonTypes->Fetch()) {
		$arPersonTypes[$ar['ID']] = '[' . $ar['LID'] . '] ' . $ar['NAME'];
	}

	$arDelivery = array(0 => GetMessage('RZ_ONECLICK_DELIVERY_ID_NOT_SET'));
	$rsDelivery = CSaleDelivery::GetList(array(), array('ACTIVE' => 'Y'));
	while ($ar = $rsDelivery->Fetch()) {
		$arDelivery[$ar['ID']] = '[' . $ar['LID'] . '] ' . $ar['NAME'];
	}
	$rsDelivery = CSaleDeliveryHandler::GetList(array(), array('ACTIVE' => 'Y'));
	while ($ar = $rsDelivery->Fetch()) {
		foreach ($ar['PROFILES'] as $profileSID => $arProfile) {
			$arDelivery[$ar['SID'] . ':' . $profileSID] = '[' . $ar['SID'] . ':' . $profileSID . '] ' . $ar['NAME'] . ' : ' . $arProfile['TITLE'];
		}
	}

	$arTemplateParameters['ONECLICK_PERSON_TYPE_ID'] = array(
		'NAME' => GetMessage('RZ_ONECLICK_PERSON_TYPE_ID'),
		'PARENT' => 'ONECLICK',
		'TYPE' => 'LIST',
		'VALUES' => $arPersonTypes,
		'REFRESH' => 'Y',
		'DEFAULT' => '1',
	);

	$arTemplateParameters['ONECLICK_SHOW_FIELDS'] = array(
		'NAME' => GetMessage('RZ_ONECLICK_SHOW_FIELDS'),
		'PARENT' => 'ONECLICK',
		'TYPE' => 'LIST',
		'MULTIPLE' => 'Y',
		'REFRESH' => 'Y',
		'VALUES' => array('NULL' => GetMessage('RZ_ONECLICK_SHOW_FIELDS_EMPTY')),
		'SIZE' => 2,
	);

	$arTemplateParameters['ONECLICK_REQ_FIELDS'] = array(
		'NAME' => GetMessage('RZ_ONECLICK_REQ_FIELDS'),
		'PARENT' => 'ONECLICK',
		'TYPE' => 'LIST',
		'MULTIPLE' => 'Y',
		'VALUES' => array('NULL' => GetMessage('RZ_ONECLICK_REQ_FIELDS_EMPTY')),
		'SIZE' => 2,
	);

	$arTemplateParameters['ONECLICK_ALLOW_AUTO_REGISTER'] = array(
		'NAME' => GetMessage('RZ_ONECLICK_ALLOW_AUTO_REGISTER'),
		'PARENT' => 'ONECLICK',
		'TYPE' => 'CHECKBOX',
		'DEFAULT' => 'Y',
	);

	$arTemplateParameters['ONECLICK_MESSAGE_OK'] = array(
		'NAME' => GetMessage('RZ_ONECLICK_MESSAGE_OK'),
		'PARENT' => 'ONECLICK',
		'TYPE' => 'STRING',
		'DEFAULT' => GetMessage('RZ_MESSAGE_OK_DEFAULT'),
	);

	$arTemplateParameters['ONECLICK_DELIVERY_ID'] = array(
		'NAME' => GetMessage('RZ_ONECLICK_DELIVERY_ID'),
		'PARENT' => 'ONECLICK',
		'TYPE' => 'LIST',
		'VALUES' => $arDelivery,
		'DEFAULT' => 0,
	);

	$arTemplateParameters['ONECLICK_PAY_SYSTEM_ID'] = array(
		'NAME' => GetMessage('RZ_ONECLICK_PAY_SYSTEM_ID'),
		'PARENT' => 'ONECLICK',
		'TYPE' => 'LIST',
		'VALUES' => array('NULL' => GetMessage('RZ_ONECLICK_PAY_SYSTEM_ID_EMPTY')),
		'DEFAULT' => 0,
	);

	$arTemplateParameters['ONECLICK_AS_EMAIL'] = array(
		'NAME' => GetMessage('RZ_ONECLICK_AS_EMAIL'),
		'PARENT' => 'ONECLICK',
		'TYPE' => 'LIST',
		'VALUES' => array('NULL' => GetMessage('RZ_ONECLICK_AS_EMAIL_EMPTY')),
		'REFRESH' => 'Y',
	);

	$arTemplateParameters['ONECLICK_AS_NAME'] = array(
		'NAME' => GetMessage('RZ_ONECLICK_AS_NAME'),
		'PARENT' => 'ONECLICK',
		'TYPE' => 'LIST',
		'VALUES' => array('NULL' => GetMessage('RZ_ONECLICK_AS_NAME_EMPTY')),
	);

	$arTemplateParameters['ONECLICK_FIELD_PLACEHOLDER'] = array(
		'NAME' => GetMessage('RZ_ONECLICK_FIELD_PLACEHOLDER'),
		'PARENT' => 'ONECLICK',
		'TYPE' => 'CHECKBOX',
		'DEFAULT' => 'Y'
	);

	$arTemplateParameters['ONECLICK_FIELD_QUANTITY'] = array(
		'NAME' => GetMessage('RZ_ONECLICK_FIELD_QUANTITY'),
		'PARENT' => 'ONECLICK',
		'TYPE' => 'CHECKBOX',
		'DEFAULT' => 'Y'
	);

	$arTemplateParameters['ONECLICK_USE_CAPTCHA'] = array(
		'NAME' => GetMessage('RZ_ONECLICK_USE_CAPTCHA'),
		'PARENT' => 'ONECLICK',
		'TYPE' => 'CHECKBOX',
		'DEFAULT' => 'Y'
	);

	$arSaleParams = array();
	if (intval($arCurrentValues['ONECLICK_PERSON_TYPE_ID']) > 0) {
		/** @noinspection PhpDynamicAsStaticMethodCallInspection */
		$rsSaleParams = CSaleOrderProps::GetList(array(), array('PERSON_TYPE_ID' => $arCurrentValues['ONECLICK_PERSON_TYPE_ID']));
		while ($ar = $rsSaleParams->Fetch()) {
			if ('Y' === $ar['REQUIED']) {
				$ar['NAME'] .= ' *';
			}
			$arSaleParams[$ar['CODE']] = $ar['NAME'];
		}
		$arTemplateParameters['ONECLICK_SHOW_FIELDS']['VALUES'] = $arSaleParams;
		$arTemplateParameters['ONECLICK_SHOW_FIELDS']['SIZE'] = (count($arSaleParams) > MAX_LINES_IN_LIST) ? MAX_LINES_IN_LIST : count($arSaleParams);

		$arPaySystem = array(0 => GetMessage('RZ_ONECLICK_PAY_SYSTEM_ID_NOT_SET'));
		$rsPaySystem = CSalePaySystem::GetList(array(), array('PERSON_TYPE_ID' => $arCurrentValues['ONECLICK_PERSON_TYPE_ID'], 'ACTIVE' => 'Y'));
		while ($ar = $rsPaySystem->Fetch()) {
			$arPaySystem[$ar['ID']] = '[' . $ar['LID'] . '] ' . $ar['NAME'];
		}
		$arTemplateParameters['ONECLICK_PAY_SYSTEM_ID']['VALUES'] = $arPaySystem;
	}
	if (is_array($arCurrentValues['ONECLICK_SHOW_FIELDS'])) {
		foreach ($arCurrentValues['ONECLICK_SHOW_FIELDS'] as $key => $val) {
			if (strlen($val) == 0) {
				unset($arCurrentValues['ONECLICK_SHOW_FIELDS'][$key]);
			}
		}
	}
	if (count($arCurrentValues['ONECLICK_SHOW_FIELDS']) > 0) {
		$hasVals = true;
		if (strpos($arCurrentValues['ONECLICK_SHOW_FIELDS'][0], 'NULL') === 0) {
			$hasVals = false;
		}
		if ($hasVals) {
			global $arValues;
			if (!is_array($arValues['ONECLICK_REQ_FIELDS'])) $arValues['ONECLICK_REQ_FIELDS'] = array();
			$arSaleParamsREQ = array();

			foreach ($arCurrentValues['ONECLICK_SHOW_FIELDS'] as $code) {
				if (substr($arSaleParams[$code], -1, 1) == "*") {
					if (array_search($code, $arValues['ONECLICK_REQ_FIELDS']) === false) {
						$arValues['ONECLICK_REQ_FIELDS'][] = $code;
					}
				}
				$arSaleParamsREQ[$code] = $arSaleParams[$code];
			}
			$arTemplateParameters['ONECLICK_REQ_FIELDS']['VALUES'] = $arSaleParamsREQ;
			$arTemplateParameters['ONECLICK_REQ_FIELDS']['SIZE'] = (count($arSaleParamsREQ) > MAX_LINES_IN_LIST) ? MAX_LINES_IN_LIST : count($arSaleParamsREQ);
			$arTemplateParameters['ONECLICK_AS_EMAIL']['VALUES'] = array_merge(array( 0 => GetMessage('RZ_ONECLICK_AS_EMAIL_NOT_USE')), $arSaleParamsREQ);
			$arTemplateParameters['ONECLICK_AS_NAME']['VALUES'] = array_merge(array( 0 => GetMessage('RZ_ONECLICK_AS_NAME_NOT_USE')), $arSaleParamsREQ);
		}
	}

	if (!empty($arCurrentValues['ONECLICK_AS_EMAIL']) && $arCurrentValues['ONECLICK_AS_EMAIL'] !== 0) {
		$arTemplateParameters['ONECLICK_SEND_REGISTER_EMAIL'] = array(
			'PARENT' => 'ONECLICK',
			'NAME' => GetMessage('RZ_ONECLICK_SEND_USER_REGISTER_EMAIL'),
			'TYPE' => 'CHECKBOX',
			'DEFAULT' => 'Y',
		);
	}
	if (!empty($arCurrentValues['ONECLICK_SEND_REGISTER_EMAIL']) && $arCurrentValues['ONECLICK_SEND_REGISTER_EMAIL'] == 'Y') {
		$arTemplateParameters['ONECLICK_USER_REGISTER_EVENT_NAME'] = array(
			'PARENT' => 'ONECLICK',
			'NAME' => GetMessage('RZ_ONECLICK_USER_REGISTER_EVENT_NAME'),
			'TYPE' => 'STRING',
			'DEFAULT' => 'USER_INFO',
		);
	}
}
//STORE_SETTINGS
if (CModule::IncludeModule('yenisite.storeamount')) {
	$arTemplateParameters['USE_MIN_AMOUNT']['HIDDEN'] = 'Y';
	$arTemplateParameters['MIN_AMOUNT']['HIDDEN'] = 'Y';
	$arTemplateParameters['SHOW_GENERAL_STORE_INFORMATION']['HIDDEN'] = 'Y';
	$arTemplateParameters['USER_FIELDS']['HIDDEN'] = 'Y';
	$arTemplateParameters['STORE_PATH']['HIDDEN'] = 'Y';
	$arTemplateParameters['FIELDS']['HIDDEN'] = 'Y';

	if ($arCurrentValues['USE_STORE'] == 'Y') {
		$arTemplateParameters['STORE_MANY_VAL'] = array(
			'PARENT' => 'STORE_SETTINGS',
			'NAME' => GetMessage('STORE_MANY_VAL'),
			'DEFAULT' => "10",
			'TYPE' => 'STRING',
		);
		$arTemplateParameters['STORE_AVERAGE_VAL'] = array(
			'PARENT' => 'STORE_SETTINGS',
			'NAME' => GetMessage('STORE_AVERAGE_VAL'),
			'DEFAULT' => "5",
			'TYPE' => 'STRING',
		);
		/*
		$arTemplateParameters['USE_STORE_PHONE'] = array(
			'PARENT' => 'STORE_SETTINGS',
			'NAME' => GetMessage('USE_STORE_PHONE'),
			'DEFAULT' => "Y",
			'TYPE' => 'CHECKBOX',
		);*/
	}
}

if (\Bitrix\Main\Loader::includeModule('yenisite.core')) {
	\Yenisite\Core\Resize::AddResizerParams(array('SECTION_IMG','SECTIONS_IMG','LINE_SECTIONS_IMG'), $arTemplateParameters);
}
?>