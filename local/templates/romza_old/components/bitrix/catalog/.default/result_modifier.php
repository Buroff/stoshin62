<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
use Yenisite\Core\Ajax;
$arParams['RESIZER_SECTION_IMG'] = $arParams['RESIZER_SECTION_IMG'] ? : 4;
$arParams['RESIZER_SECTIONS_IMG'] = $arParams['RESIZER_SECTIONS_IMG'] ? : 5;
$arParams['RESIZER_LINE_SECTIONS_IMG'] = $arParams['RESIZER_LINE_SECTIONS_IMG'] ? : 6;
$arParams['VIEW_MODE'] = $arParams['VIEW_MODE'] ? : 'BOTH';
$arParams['SUBSECTION_POSITION'] = $arParams['SUBSECTION_POSITION'] ? : 'TOP';
$arParams['FULL_DESCTRIPTION'] = $arParams['FULL_DESCTRIPTION'] ? : 'N';
$arParams['SECTION_COUNT_ELEMENTS'] = $arParams['SECTION_COUNT_ELEMENTS'] ? : 'Y';
$arParams['USE_STORE_PHONE'] = $arParams['USE_STORE_PHONE'] ? : 'Y';

Ajax::saveParams($this, $arParams, 'main_catalog', SITE_ID, false);
