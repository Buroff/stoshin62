<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?include 'get_cur_section.php';
global $rz_options?>
<?
$APPLICATION->IncludeComponent("bitrix:catalog.section.list", "catalog", array(
	"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
	"IBLOCK_ID" => $arParams["IBLOCK_ID"],
	"SECTION_ID" => intval($arCurSection["ID"]),
	"SECTION_CODE" => $arCurSection["CODE"],
	"COUNT_ELEMENTS" => $arParams["SECTION_COUNT_ELEMENTS"],
	"TOP_DEPTH" => $arParams['SECTION_TOP_DEPTH'],
	"SECTION_URL" => "",
	"CACHE_TYPE" => $arParams["CACHE_TYPE"],
	"CACHE_TIME" => $arParams["CACHE_TIME"],
	"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
	"ADD_SECTIONS_CHAIN" => "N",
	"SHOW_PARENT_NAME" => "N",
	"RESIZER_SET" => $arParams['RESIZER_SUBSECTION'],
	"RESIZER_SECTION_IMG" => $arParams['RESIZER_SECTION_IMG'],
	"RESIZER_SECTIONS_IMG" => $arParams['RESIZER_SECTIONS_IMG'],
	"RESIZER_LINE_SECTIONS_IMG" => $arParams['RESIZER_LINE_SECTIONS_IMG'],
	"SHOW_SUBSECTIONS" => $arParams['SHOW_SUBSECTIONS'],
	"SHOW_DESCRIPTION" => $arParams['SHOW_DESCRIPTION'],
	"FULL_DESCTRIPTION" => $arParams['FULL_DESCTRIPTION'] != 'N',
	"VIEW_MODE" => $rz_options['switch_subsection_view'],
	),
	$component
);
?>