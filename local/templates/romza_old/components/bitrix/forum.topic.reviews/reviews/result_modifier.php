<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

foreach ($arResult['MESSAGES'] as &$arItem) {
    $arItem['POST_MESSAGE_TEXT'] = str_replace('<noindex>', '<!--noindex-->', $arItem['POST_MESSAGE_TEXT']);
    $arItem['POST_MESSAGE_TEXT'] = str_replace('</noindex>', '<!--/noindex-->', $arItem['POST_MESSAGE_TEXT']);
}