<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use \Yenisite\Core\Tools;
include_once $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/tools/include_module.php';
$isAjax = Tools::isAjax();
\Bitrix\Main\Localization\Loc::loadMessages($_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH . '/lang/'.LANGUAGE_ID.'/header.php');
$pathToRules = COption::GetOptionString(CRZShinmarketSettings::getModuleId(),'path_tu_rules_privacy',SITE_DIR.'personal/rules/personal_data.php');
$rand = $this->randString();

if (!$isAjax && method_exists($this, 'createFrame')) $frame = $this->createFrame()->begin('');
// ************************* Input params***************************************************************
$arParams["SHOW_LINK_TO_FORUM"] = ($arParams["SHOW_LINK_TO_FORUM"] == "Y" ? "Y" : "N");
$arParams["FILES_COUNT"] = intVal(intVal($arParams["FILES_COUNT"]) > 0 ? $arParams["FILES_COUNT"] : 1);
$arParams["IMAGE_SIZE"] = (intVal($arParams["IMAGE_SIZE"]) > 0 ? $arParams["IMAGE_SIZE"] : 100);
// *************************/Input params***************************************************************
?>
<?if(!$isAjax):?>
	<div id="catalog_replyForm">
<?endif;?>
		<? foreach ($arResult["MESSAGES"] as $res): ?>
			<div class="feedbacks__item">
				<div class="feedbacks__item__username"><?= $res["AUTHOR_NAME"] ?></div>
				<div class="feedbacks__item__date"><?= $res["POST_DATE"] ?></div>
				<div class="feedbacks__item__text"><?= $res["POST_MESSAGE_TEXT"] ?></div>
			</div><!--.reply-item-->
		<? endforeach; ?>
		<? if (strlen($arResult["NAV_STRING"]) > 0 && $arResult["NAV_RESULT"]->NavPageCount > 1): ?>
			<?= $arResult["NAV_STRING"] ?>
		<? endif; ?>
	<div class="reply-form">
		<?if(strlen($arResult["ERROR_MESSAGE"]) > 0):?>
			<div class="message message-error">
				<?= $arResult["ERROR_MESSAGE"] ?>
			</div>
		<?endif;?>
		<?if(strlen($arResult["OK_MESSAGE"]) > 0):?>
			<div class="message message-success">
				<?= $arResult["OK_MESSAGE"] ?>
			</div>
		<?endif;?>
		<form method="POST" enctype="multipart/form-data" class="form_review modal-form"
			  data-arparams="<?=Tools::GetEncodedArParams($arParams)?>" data-template="<?=$templateName?>">
            <input type="hidden" name="privacy_policy" value="N"/>
			<input type="hidden" name="back_page" value="<?= $arResult["CURRENT_PAGE"] ?>"/>
			<input type="hidden" name="ELEMENT_ID" value="<?= $arParams["ELEMENT_ID"] ?>"/>
			<input type="hidden" name="SECTION_ID" value="<?= $arResult["ELEMENT_REAL"]["IBLOCK_SECTION_ID"] ?>"/>
			<input type="hidden" name="save_product_review" value="Y"/>
			<input type="hidden" name="preview_comment" value="N"/>
			<?= bitrix_sessid_post() ?>
			<h2><?= GetMessage('ADD_REVIEW'); ?></h2>
			<? /* GUEST PANEL */
			if (!$arResult["IS_AUTHORIZED"]): ?>
				<div class="form-group">
					<label><?= GetMessage("OPINIONS_NAME") ?>:<span class="req">*</span></label>
					<input required class="form-control" name="REVIEW_AUTHOR" size="30" type="text" title=""
						   value="<?= $arResult["REVIEW_AUTHOR"] ?>" <? if (!empty($tabIndex)): ?> tabindex="<?= $tabIndex++; ?>" <? endif; ?> />
				</div><!--.form-group-->
				<? if ($arResult["FORUM"]["ASK_GUEST_EMAIL"] == "Y"): ?>
					<div class="form-group">
						<label><?= GetMessage("OPINIONS_EMAIL") ?> <span class="req">*</span>:</label>
						<input required class="form-control" type="text" name="REVIEW_EMAIL" size="30" title=""
							   value="<?= $arResult["REVIEW_EMAIL"] ?>" <? if (!empty($tabIndex)): ?> tabindex="<?= $tabIndex++; ?>" <? endif; ?> />
					</div><!--.form-group-->
				<? endif; ?>
			<? endif; ?>

			<div class="form-group">
				<label><?= GetMessage('REVIEW_TEXT'); ?>:</label>
				<textarea name="REVIEW_TEXT" class="form-control" title=""></textarea>
			</div>
			<!--.form-group-->
			<? if (strlen($arResult["CAPTCHA_CODE"]) > 0): ?>
				<div class="form-group">
					<input required type="hidden" name="captcha_code" value="<?= $arResult["CAPTCHA_CODE"] ?>"/>
					<label for="captcha_word"><?= GetMessage("F_CAPTCHA_PROMT") ?><span class="reviews-required-field">*</span></label>
					<input id="captcha_word" class="txt w100" type="text" size="30"
						   name="captcha_word" <? if (!empty($tabIndex)): ?> tabindex="<?= $tabIndex++; ?>" <? endif; ?>
						   autocomplete="off"/>
					<span class="captcha_img">
						<img src="/bitrix/tools/captcha.php?captcha_code=<?= $arResult["CAPTCHA_CODE"] ?>"
							 alt="<?= GetMessage("F_CAPTCHA_TITLE") ?>"/>
					</span>
				</div><!--.form-group-->
			<? endif; ?>
            <div class="form-group agreement-policy">
                <div class="forms-wrapper">
                    <input id="privacy_policy_<?=$rand?>" required type="checkbox" name="privacy_policy" value="Y" <?=$_REQUEST['privacy_policy'] == 'Y' ? ' checked' : ''?>>
                    <label for="privacy_policy_<?=$rand?>">
                        <?= GetMessage('RZ_ACCEPT') ?> <?=GetMessage('RZ_CONCLUSION',array("#URL#" => Tools::GetConstantUrl($pathToRules)))?>
                    </label>
                </div>
            </div>
			<div class="form-group">
				<input name="send_button" type="submit" class="btn btn-primary"
					   value="<?= GetMessage("OPINIONS_SEND") ?>"
					<? if (!empty($tabIndex)): ?> tabindex="<?= $tabIndex++; ?>" <? endif; ?> />
			</div>
			<!--.form-group-->
		</form>
	</div>
	<!--.reply-form-->
<?if(!$isAjax):?>
		<script type="text/javascript">
			if(typeof(rz) == 'undefined') {
				rz = {};
			}
			rz.refreshComments = 1;
		</script>
	</div>
<?endif;?>
<? if (!$isAjax && method_exists($this, 'createFrame')) $frame->end(); ?>