<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
if (method_exists($this, 'setFrameMode')) $this->setFrameMode(true);
$isJson = (isset($_REQUEST['json']) && $_REQUEST['json'] == 'Y');
//if ($arParams["DISPLAY_AS_RATING"] == "vote_avg") {
	if ($arResult["PROPERTIES"]["vote_count"]["VALUE"])
		$votesValue = round($arResult["PROPERTIES"]["vote_sum"]["VALUE"] / $arResult["PROPERTIES"]["vote_count"]["VALUE"], 2);
	else
		$votesValue = 0;
//} else
//	$votesValue = floatval($arResult["PROPERTIES"]["rating"]["VALUE"]);

$votesCount = intval($arResult["PROPERTIES"]["vote_count"]["VALUE"]);
?>
<?if($isJson):
	echo json_decode($votesValue);
	?>
<?else:?>
<span class="rateit<? if ($arResult['VOTED']): ?> disabled<?endif?>" data-id="<?= $arResult['ID'] ?>"
	  data-rateit-value="<?= $votesValue ?>" data-rateit-step="1" data-rateit-resetable="false" data-rateit-starwidth="19"
	  data-rateit-starheight="18" <? if ($arResult['VOTED']): ?>data-rateit-readonly="true"<? endif ?>
	  data-arparams="<?= $arResult['AJAX_PARAMS'] ?>" data-template="<?= $templateName ?>"
	></span>
<?endif?>