(typeof(jQuery) != 'undefined')
&& (function ($) {
	var $form = $('#profile_settings_form');
	var $btns = $('.form-group.form-submit input');
	$btns.on('click', function (e) {
		e.preventDefault();
		var $this = $(this);
		if ($this.attr('type') == 'submit') {
			$form.append($('<input type="hidden" name="' + $this.attr('name') + '" value="' + $this.val() + '"/>'));
			$form.submit();
		} else if ($this.attr('type') == 'reset') {
			$form[0].reset();
		}
	});
})(jQuery);