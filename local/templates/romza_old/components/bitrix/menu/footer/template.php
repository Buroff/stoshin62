<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?if (method_exists($this, 'setFrameMode')) $this->setFrameMode(true); ?>
<? if (!empty($arResult)): ?>
	<h2><?=GetMessage('RZ_MENU_TITLE')?></h2>
	<ul class="page_footer__nav">
		<? foreach ($arResult as $arItem):
			if ($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) continue; ?>
			<? if ($arItem["SELECTED"]): ?>
				<li><a href="<?= $arItem["LINK"] ?>" class="selected"><?= $arItem["TEXT"] ?></a></li>
			<? else: ?>
				<li><a href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a></li>
			<?endif ?>
		<? endforeach ?>
	</ul>
<? endif ?>