<?
use Yenisite\Core\Resize;
use Yenisite\Core\Tools;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if ($arParams["RUB_SIGN"] != "N") $arParams["RUB_SIGN"] = "Y";
if ($arParams["VIEW_HIT"] == "Y") {
	$obCache = new CPHPCache;
	$life_time = $arParams['MENU_CACHE_TIME'] ? IntVal($arParams['MENU_CACHE_TIME']) : 604800;
	$cache_id = "menu_hits_horizontal";

	if ($obCache->InitCache($life_time, $cache_id, $cache_id )) {
		$vars = $obCache->GetVars();
		if (is_array($vars['MENU_HITS']) && count($vars['MENU_HITS']) > 0)
			$arResult['HITS'] = $vars['MENU_HITS'];
	} elseif ($obCache->StartDataCache()) {
		if ($arResult[0]['PARAMS']['FROM_IBLOCK'] == 1) {
			$fromIb = true;
		}

		$arHits = array();

		if (CModule::IncludeModule('catalog')) {
			$arPrice = CCatalogGroup::GetList(array(), array("NAME" => $arParams["PRICE_CODE"]),
				false, false, array("ID"))->Fetch();
		}

		foreach ($arResult as $index => &$arItem) {
			if ($arItem['DEPTH_LEVEL'] == 1 && $index !== 'HITS') {
				$arSelect = array(
					"ID",
					"NAME",
					"CODE",
					"IBLOCK_ID",
					"IBLOCK_TYPE",
					"IBLOCK_SECTION_ID",
					"DETAIL_PAGE_URL",
					"DETAIL_PICTURE",
					"PREVIEW_PICTURE",
					"CATALOG_GROUP_" . $arPrice["ID"]
				);

				if (CModule::IncludeModule('yenisite.bitronic')
					|| CModule::IncludeModule('yenisite.bitroniclite')
					|| CModule::IncludeModule('yenisite.bitronicpro')
				) {
					$arSelect += array(
						"PROPERTY_NEW",
						"PROPERTY_HIT",
						"PROPERTY_SALE",
						"PROPERTY_BESTSELLER"
					);
				}

				$arFilter = $arItem['PARAMS']['FILTER'];

				if (!is_array($arFilter) || empty($arFilter)) {
					$arFilter = array();

					$arIbType = CIBlockType::GetList(array(), array("NAME" => $arItem["TEXT"]))->Fetch();
					if (!empty($arIbType)) {
						$arFilter = array("IBLOCK_TYPE" => $arIbType["ID"]);
					} else {
						$arIb = CIBlock::GetList(array(), array("NAME" => $arItem["TEXT"], 'SITE_ID' => SITE_ID, 'ACTIVE' => 'Y'))->Fetch();

						if (!empty($arIb)) {
							$arFilter = array("IBLOCK_ID" => $arIb["ID"]);
						} else {
							//$from = 'section';

							$arSect = CIBlockSection::GetList(array(), array("NAME" => $arItem["TEXT"], "DEPTH_LEVEL" => "1", 'ACTIVE' => 'Y'))->Fetch();

							$arFilter = array("SECTION_ID" => $arSect["ID"], "INCLUDE_SUBSECTIONS" => "Y");
						}
					}
				}

				$dbRes = CIBlockElement::GetList(array("PROPERTY_WEEK_COUNTER" => "desc"),
					$arFilter, false, array("nTopCount" => 3), $arSelect);

				while ($obEl = $dbRes->GetNextElement()) {
					$arFields = $obEl->GetFields();

					if (!empty($arFields["IBLOCK_SECTION_ID"])) {
						$arSec = $arSect;
					}

					$path = Resize::GetResizedImg($arFields,array('WIDTH' => 150, 'HEIGHT' => 150, 'SET_ID' => intval($arParams['RESIZER_PRODUCT'])));

					if (CModule::IncludeModule('catalog')) {
						$arResultPrices = CIBlockPriceTools::GetCatalogPrices($arFields["IBLOCK_ID"], array($arParams["PRICE_CODE"]));

						$arProduct = CCatalogProduct::GetByID($arFields["ID"]);
						$arPrices = array();
						if ($arProduct != false ) {
							$arProduct['VAT_INCLUDED'] = ($arProduct['VAT_INCLUDED'] == 'Y') ? true : false;
							if(CCatalogSKU::IsExistOffers($arProduct["ID"],$arFields['IBLOCK_ID'])) {
								$arOffers = CIBlockPriceTools::GetOffersArray($arFields["IBLOCK_ID"],$arFields["ID"],
									array("CATALOG_PRICE_".$arPrice['ID'] => "ASC"),
									array(),
									array(),
									1,
									$arResultPrices,
									$arProduct['VAT_INCLUDED'],
									array("CURRENCY_ID" => $arParams["CURRENCY"])
								);
								$arPrices = $arOffers[0]['PRICES'];
								$arPrices["HAS_OFFER"] = "Y";
							} else {
								$arPrices = CIBlockPriceTools::GetItemPrices($arFields["IBLOCK_ID"], $arResultPrices, $arFields, $arProduct['VAT_INCLUDED'], array("CURRENCY_ID" => $arParams["CURRENCY"]));
							}
						}
					} else if (CModule::IncludeModule('yenisite.market')) {
						include_once $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/tools/include_module.php';
						$arPrices = CMarketPrice::GetItemPriceValues($arFields['ID'], $arFields['PRICES']);
						if (count($arPrices) > 0) {
							unset($arFields['PRICES']);
					}
						$minPrice = false;
						foreach ($arPrices as $k => $pr) {
							$pr = floatval($pr);
							$arFields['PRICES'][$k]['VALUE'] = $pr;
							$arFields['PRICES'][$k]['PRINT_VALUE'] = $pr;
							if ((empty($minPrice) || $minPrice > $pr) && $pr > 0) {
								$minPrice = $pr;
							}
						}
						if ($minPrice !== false) {
							$arFields['MIN_PRICE']['VALUE'] = $minPrice;
							$arFields['MIN_PRICE']['PRINT_VALUE'] = Tools::FormatPrice($minPrice);
							$arFields['MIN_PRICE']['DISCOUNT_VALUE'] = $minPrice;
							$arFields['MIN_PRICE']['PRINT_DISCOUNT_VALUE'] = Tools::FormatPrice($minPrice);
							$arFields['CATALOG_MEASURE_RATIO'] = 1;
							$arFields['CAN_BUY'] = true;
						}
						$arFields['CHECK_QUANTITY'] = $arResult['CHECK_QUANTITY'];
						$arFields['CATALOG_QUANTITY'] = CMarketCatalogProduct::GetQuantity($arFields['ID'], $arFields['IBLOCK_ID']);

						if ($arFields['CHECK_QUANTITY'] && $arFields['CATALOG_QUANTITY'] <= 0) {
							$arFields['CAN_BUY'] = false;
						}
						$arFields['CATALOG_TYPE'] = 1; //simple product
						$arPrices = array($arParams['PRICE_CODE'] => $arFields['MIN_PRICE']);
					}
					//end Prices for MARKET

					$arHits[$index][] = array(
						'ID' => $arFields["ID"],
						'IBLOCK_ID' => $arFields["IBLOCK_ID"],
						"IBLOCK_TYPE" => $arFields["IBLOCK_TYPE"],
						"NAME" => $arFields["NAME"],
						"SECTION" => $arSec["NAME"],
						"SECTION_PAGE_URL" => $arSec["SECTION_PAGE_URL"],
						"DETAIL_PAGE_URL" => $arFields["DETAIL_PAGE_URL"],
						"PHOTO" => $path,
						"PRICE" => $arPrices,
						"HIT" => $arFields["PROPERTY_HIT_VALUE"],
						"NEW" => $arFields["PROPERTY_NEW_VALUE"],
						"SALE" => $arFields["PROPERTY_SALE_VALUE"],
						"BESTSELLER" => $arFields["PROPERTY_BESTSELLER_VALUE"],
					);
				} // while( $obEl = $dbRes->GetNextElement() )

				$arResult['HITS'][$index] = $arHits[$index];

			} // if($arParams["VIEW_HIT"] == "Y" && $arItem['DEPTH_LEVEL'] == 1)

		} // foreach($arResult as &$arItem => $index)

			$obCache->EndDataCache(array("MENU_HITS" => $arHits));
	} // if(!is_array($arResult['HITS']))

	unset($obCache);
}
foreach ($arResult as &$val) {
	if(isset($val['PARAMS']['UF_FIELDS']['UF_SECTION_ICON'])
		&& ($valId = intval($val['PARAMS']['UF_FIELDS']['UF_SECTION_ICON'])) > 0 ) {
		$dbField = CUserFieldEnum::GetList(array(),
			array(
				'ID' => $valId,
				)
			);
		$arField = $dbField->GetNext();
		$val['ICON_CODE'] = $arField['XML_ID'];
	}
}unset($val);
?>