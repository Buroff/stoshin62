<div class="goods_list">
	<? foreach ($hits as $hit): ?>
		<div class='goods_item'>
			<div class="goods_item__inner">
				<div class="goods_item__img">
					<a href="<?= $hit["DETAIL_PAGE_URL"] ?>">
						<img src='<?= $hit["PHOTO"]; ?>' alt='<?= $hit["NAME"] ?>' class="goods_item__img" />
					</a>
				</div>
				<div class="goods_item__info">
					<div class="goods_item__title">
						<a href="<?= $hit["DETAIL_PAGE_URL"] ?>"><?= $hit["NAME"] ?></a>
					</div>
					<div class="goods_item__rating">
						<?$APPLICATION->IncludeComponent(
							"bitrix:iblock.vote",
							"stars",
							array(
								"IBLOCK_TYPE" => $hit['IBLOCK_TYPE'],
								"IBLOCK_ID" => $hit['IBLOCK_ID'],
								"ELEMENT_ID" => $hit['ID'],
								"ELEMENT_CODE" => "",
								"MAX_VOTE" => "5",
								"VOTE_NAMES" => array("1", "2", "3", "4", "5"),
								"SET_STATUS_404" => "N",
								"DISPLAY_AS_RATING" => $arParams['VOTE_DISPLAY_AS_RATING'],
								"CACHE_TYPE" => $arParams['CACHE_TYPE'],
								"CACHE_TIME" => $arParams['CACHE_TIME']
							),
							$component,
							array("HIDE_ICONS" => "Y")
						);?>
					</div>
					<?
					$hit['PRINT_DISCOUNT_VALUE'] = $hit['PRICE'][$arParams['PRICE_CODE']]['PRINT_DISCOUNT_VALUE'];
					$hit['PRINT_VALUE'] = $hit['PRICE'][$arParams['PRICE_CODE']]['PRINT_VALUE'];
					?>
					<div class="goods_item__price">
						<?if ($hit['PRINT_DISCOUNT_VALUE'] != $hit['PRINT_VALUE']):?>
							<del class="old_price"><?= $hit['PRINT_VALUE']; ?></del>
						<?endif?>
						<ins class="new_price">
							<?if(isset($hit['PRICE']['HAS_OFFER']) && $hit['PRICE']['HAS_OFFER'] == "Y"):?><?=GetMessage('RZ_OFFERS_SUF')?>&nbsp;<?endif;?>
							<?= $hit["PRINT_DISCOUNT_VALUE"] ?>
						</ins>
					</div>
				</div>
			</div>
		</div>
	<? endforeach; ?>
</div>