<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

global $USER;

if ($templateData['CREATED_BY'] != $USER->GetID()
||  $templateData['SITE_ID'] != SITE_ID) {

	LocalRedirect($arResult['LIST_PAGE_URL']);
}