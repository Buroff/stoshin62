<?php
$arTemplateParameters = array(
	'CATALOG_FILTER_NAME' => array(
		'PARENT' => 'VISUAL',
		'NAME' => GetMessage('RZ_CATALOG_FILTER_NAME'),
		'TYPE' => 'STRING',
		'MULTIPLE' => 'N',
		'ADDITIONAL_VALUES' => 'N',
		'REFRESH' => 'N',
		'DEFAULT' => 'arrFilter',
	),
	'CATALOG_URL' => array(
		'PARENT' => 'VISUAL',
		'NAME' => GetMessage('RZ_CATALOG_URL'),
		'TYPE' => 'STRING',
		'MULTIPLE' => 'N',
		'ADDITIONAL_VALUES' => 'N',
		'REFRESH' => 'N',
		'DEFAULT' => '#SITE_DIR#/catalog/',
	),
	'CATALOG_PROP_ID' => array(
		'PARENT' => 'VISUAL',
		'NAME' => GetMessage('RZ_CATALOG_PROP_ID'),
		'TYPE' => 'STRING',
		'MULTIPLE' => 'N',
		'ADDITIONAL_VALUES' => 'N',
		'REFRESH' => 'N',
		'DEFAULT' => '0',
	),
);
if (\Bitrix\Main\Loader::IncludeModule("yenisite.resizer2")) {
	$resizer_sets_list = array();
	$arSets = CResizer2Set::GetList();
	while ($arr = $arSets->Fetch()) {
		$resizer_sets_list[$arr["id"]] = "[" . $arr["id"] . "] " . $arr["NAME"];
	}

	global $arComponentParameters;

	$arComponentParameters["GROUPS"]["RESIZER_SETS"] = array(
		"NAME" => GetMessage("RESIZER_SETS"),
		"SORT" => 1
	);

	$arTemplateParameters["RESIZER_PRODUCT"] = array(
		"PARENT" => "RESIZER_SETS",
		"NAME" => GetMessage("RESIZER_PRODUCT"),
		"TYPE" => "LIST",
		"VALUES" => $resizer_sets_list,
		"DEFAULT" => "2",
	);
}