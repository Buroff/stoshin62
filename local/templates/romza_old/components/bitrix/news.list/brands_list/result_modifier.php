<? use Yenisite\Core\Tools;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
include_once $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/tools/include_module.php';

$arParams['CATALOG_PROP_ID'] = intval($arParams['CATALOG_PROP_ID']);
if ($arParams['CATALOG_PROP_ID'] > 0) {
	$arParams['CATALOG_FILTER_NAME'] = trim($arParams['CATALOG_FILTER_NAME']);
	if (empty($arParams['CATALOG_FILTER_NAME'])) {
		$arParams['CATALOG_FILTER_NAME'] = 'arrFilter';
	}
	$arParams['CATALOG_URL'] = trim($arParams['CATALOG_URL']);
	if (empty($arParams['CATALOG_URL'])) {
		$arParams['CATALOG_URL'] = '#SITE_DIR#/catalog/';
	}
	$arParams['CATALOG_URL'] = Tools::GetConstantUrl($arParams['CATALOG_URL']);
	foreach ($arResult['ITEMS'] as &$arItem) {
		$arItem['URL'] = htmlspecialcharsbx($arParams['CATALOG_URL'] .
			'?' . $arParams['CATALOG_FILTER_NAME']
			. '_' . $arParams['CATALOG_PROP_ID']
			. '_' . abs(crc32($arItem['ID']))
			. '=Y&set_filter=Y');
	}
}
