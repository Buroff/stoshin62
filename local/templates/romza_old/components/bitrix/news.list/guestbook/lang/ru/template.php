<?
$MESS["RZ_POCHITAT__VSE_OTZIVI"] = "Почитать все отзывы";
$MESS["RZ_OTZIVI_POKUPATELEJ"] = "Отзывы покупателей";
$MESS["RZ_OTZIVOV_POKA_NET"] = "Отзывов пока нет";
$MESS["REFRESH"] = "Обновить";
$MESS["RESPONSE"] = "Ответить";
$MESS["CANCEL"] = "Отмена";
$MESS["ANSWER"] = "Ответ";
$MESS["DOWNLOAD"] = "Скачать файл";
$MESS["IP"] = "IP-адрес:";
$MESS["MESSAGES_EMPTY"] = "Сообщений нет";
$MESS["CHOOSE_SECTION"] = "Выберите раздел";
