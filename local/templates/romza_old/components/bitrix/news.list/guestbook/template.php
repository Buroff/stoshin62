<? use Yenisite\Core\Resize;
use Yenisite\Core\Tools;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
include_once $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/tools/include_module.php';
if (method_exists($this, 'setFrameMode')) $this->setFrameMode(true);
?>
<div class="feedbacks_block aside_block">
	<h2><?=GetMessage("RZ_OTZIVI_POKUPATELEJ")?></h2>

	<div class="feedbacks__list">
		<? if (!empty($arResult['ITEMS'])): ?>
			<? foreach ($arResult['ITEMS'] as $arItem): ?>
				<?
				$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
				$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
				?>
				<div class="feedbacks__item" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
					<div class="feedbacks__item__inner">
						<? if (!empty($arItem['PROPERTIES']['file']['VALUE'])) : ?>
							<img class="feedbacks__item__userpic img-circle"
								 src="<?= Resize::GetResizedImg($arItem['PROPERTIES']['file']['VALUE'],array('WIDTH' => 80, 'HEIGHT' => 80)) ?>"
								 alt="<?= $arItem['NAME'] ?>">
						<? endif ?>
						<div class="feedbacks__item__info">
							<div class="feedbacks__item__username"><?= $arItem['NAME'] ?></div>
							<div class="feedbacks__item__date"><?= ConvertDateTime($arItem['TIMESTAMP_X'], "DD.MM.YYYY"); ?></div>
							<?if(!empty($arItem['PROPERTIES']['raiting']['VALUE'])):?>
								<div class="feedbacks__item__rating_wrapper">
									<span class="stars stars_<?=$arItem['PROPERTIES']['raiting']['VALUE']?>"></span>
								</div>
							<?endif?>
						</div>
					</div>
					<div class="feedbacks__item__text"><?= $arItem['PREVIEW_TEXT'] ?></div>
				</div>
			<? endforeach ?>
		<? else: ?>
			<h2><?=GetMessage("RZ_OTZIVOV_POKA_NET")?></h2>
		<? endif; ?>
		<div class="read_more"><a href="<?= Tools::GetConstantUrl($arParams['DETAIL_URL'])?>"><?=GetMessage("RZ_POCHITAT__VSE_OTZIVI")?></a></div>
	</div>
</div>