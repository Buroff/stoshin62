<? use Yenisite\Core\Tools;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
include_once $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/tools/include_module.php';
if (method_exists($this, 'setFrameMode')) $this->setFrameMode(true);
?>
<div class="news_block aside_block">
	<h2><?=GetMessage('RZ_NEWS_TITLE')?></h2>
	<div class="news__list">
		<? foreach ($arResult["ITEMS"] as $arItem): ?>
			<?
			$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
			?>
			<div class="news__item" id="<?= $this->GetEditAreaId($arItem['ID']); ?>" >
				<h3 class="news__item_title">
					<? if (!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])): ?>
						<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><?= $arItem["NAME"]; ?></a>
					<?else:?>
						<b><?= $arItem["NAME"]; ?></b>
					<?endif;?>
				</h3>
				<div class="news__item_date">
					<? if ($arParams["DISPLAY_DATE"] != "N" && $arItem["DISPLAY_ACTIVE_FROM"]): ?>
						<?= $arItem["DISPLAY_ACTIVE_FROM"] ?>
					<? endif ?>
				</div>
				<div class="news__item_text">
					<? if ($arParams["DISPLAY_PREVIEW_TEXT"] != "N" && $arItem["PREVIEW_TEXT"]): ?>
						<?= $arItem["PREVIEW_TEXT"]; ?>
					<? endif; ?>
				</div>
			</div>
		<? endforeach ?>
	</div>
	<div class="read_more">
		<a href="<?= Tools::GetConstantUrl($arResult['LIST_PAGE_URL'])?>">
			<?=GetMessage('RZ_NEWS_GET_ALL')?>
		</a>
	</div>
</div>
