<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use \Yenisite\Core\Tools;
if (method_exists($this, 'setFrameMode')) $this->setFrameMode(true);

$arUrls = Array(
	"delete" => $APPLICATION->GetCurPage() . "?" . $arParams["ACTION_VARIABLE"] . "=delete&id=#ID#",
	"delay" => $APPLICATION->GetCurPage() . "?" . $arParams["ACTION_VARIABLE"] . "=delay&id=#ID#",
	"add" => $APPLICATION->GetCurPage() . "?" . $arParams["ACTION_VARIABLE"] . "=add&id=#ID#",
);

$arBasketJSParams = array(
	'SALE_DELETE' => GetMessage("SALE_DELETE"),
	'SALE_DELAY' => GetMessage("SALE_DELAY"),
	'SALE_TYPE' => GetMessage("SALE_TYPE"),
	'TEMPLATE_FOLDER' => $templateFolder,
	'DELETE_URL' => $arUrls["delete"],
	'DELAY_URL' => $arUrls["delay"],
	'ADD_URL' => $arUrls["add"]
);
include_once $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/tools/include_module.php';
$isAjax = Tools::isAjax();
?>
<?if(!$isAjax):?>
<div class="ordering_block step_1" id="order_block_step_1" data-template="<?=$templateName?>"
	data-arparams="<?=Tools::GetEncodedArParams($arParams)?>" data-url="<?=$APPLICATION->GetCurPage()?>">
<?endif?>
	<?
	include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/functions.php");

	if (strlen($arResult["ERROR_MESSAGE"]) <= 0) {
		?>
			<?
			if (is_array($arResult["WARNING_MESSAGE"]) && !empty($arResult["WARNING_MESSAGE"])):?>
				<div class="message message-warning">
				<?= implode('<br/>',$arResult["WARNING_MESSAGE"]);?>
				</div>
			<?endif;?>
		<?

		$normalCount = count($arResult["ITEMS"]["AnDelCanBuy"]);
		$normalHidden = ($normalCount == 0) ? "style=\"display:none\"" : "";

		$delayCount = count($arResult["ITEMS"]["DelDelCanBuy"]);
		$delayHidden = ($delayCount == 0) ? "style=\"display:none\"" : "";

		$subscribeCount = count($arResult["ITEMS"]["ProdSubscribe"]);
		$subscribeHidden = ($subscribeCount == 0) ? "style=\"display:none\"" : "";

		$naCount = count($arResult["ITEMS"]["nAnCanBuy"]);
		$naHidden = ($naCount == 0) ? "style=\"display:none\"" : "";
		?>
		<div class="ordering_block__in_basket_wrapper">
			<span class="ordering_block__in_basket_head"><?= GetMessage("SALE_ITEMS") ?></span>
			<span data-target="#basket_items_list"
			   class="ordering_block__out_basket ordering_block__active">
				<?= GetMessage("SALE_BASKET_ITEMS") ?>(<?= $normalCount ?>)
			</span>
			<span data-target="#basket_items_delayed"
			   class="ordering_block__out_basket ordering_block__inactive" <?= $delayHidden ?>>
				<?= GetMessage("SALE_BASKET_ITEMS_DELAYED") ?>(<?= $delayCount ?>)
			</span>
			<span data-target="#basket_items_subscribed"
			   class="ordering_block__out_basket ordering_block__inactive"<?= $subscribeHidden ?>>
				<?= GetMessage("SALE_BASKET_ITEMS_SUBSCRIBED") ?>(<?= $subscribeCount ?>)
			</span>
			<span data-target="#basket_items_not_available"
			   class="ordering_block__out_basket ordering_block__inactive"<?= $naHidden ?>>
				<?= GetMessage("SALE_BASKET_ITEMS_NOT_AVAILABLE") ?>(<?= $naCount ?>)
			</span>
		</div>

		<form method="post" action="" name="basket_form" id="basket_form" class="basket_order_form">
			<?
			include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/basket_items.php");
			include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/basket_items_delayed.php");
			include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/basket_items_subscribed.php");
			include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/basket_items_not_available.php");
			?>
			<input type="hidden" name="BasketOrder" value="BasketOrder"/>
			<!-- <input type="hidden" name="ajax_post" id="ajax_post" value="Y"> -->
		</form>
	<?
	} else {
		ShowError($arResult["ERROR_MESSAGE"]);
	}
	?>
<?if(!$isAjax):?>
</div>
<?endif?>