(typeof(jQuery) != 'undefined')
&& (function ($) {

	$('body').on('click', '.ordering_block__in_basket_wrapper .ordering_block__out_basket', function (e) {
		e.preventDefault();
		var $this = $(this),
			$target = $($this.data('target')),
			$orderingBlock = $this.closest('.ordering_block__in_basket_wrapper');
		var $tables = $('.basket_order_form > div');
		$orderingBlock.find('.ordering_block__active')
			.removeClass('ordering_block__active')
			.addClass('ordering_block__inactive');
		$this.removeClass('ordering_block__inactive').addClass('ordering_block__active');
		$tables.hide();
		$target.show();
	});

	var updateBasket = function (addData) {
		$basketContainer.setAjaxLoading();
		if (typeof(addData) == 'undefined') {
			addData = [];
		}
		var data = [
			{name: "arParams", value: $basketContainer.data('arparams')},
			{name: "template", value: $basketContainer.data('template').toString()},
			{name: "URL", value: $basketContainer.data('url')}
		];
		$.merge(data, addData);
		return $.ajax({
			type: "POST",
			url: rz.AJAX_DIR + 'cart.php',
			data: data,
			success: function (html) {
				$basketContainer.html(html);
				$basketContainer.reStyler();
				$basketContainer.reSpin();
				$basketContainer.stopAjaxLoading();
			}
		});
	};

	var changeTimer = null;
	var $basketContainer = $('#order_block_step_1');
	$basketContainer.on('change', '#basket_items .js-touchspin', function (e) {
		e.preventDefault();
		if (changeTimer != null) {
			clearTimeout(changeTimer);
		}
		changeTimer = setTimeout(function () {
			var data = $basketContainer.find('.basket_order_form').serializeArray();
			data.push({name: 'BasketRefresh', value: 'Y'});

			updateBasket(data);
			$(document).trigger('cartLine_Refresh');
		}, 700);

	});
	$basketContainer.on('click', '.apply_coupon', function () {
		var data = $basketContainer.find('.basket_order_form').serializeArray();
		data.push({name: 'BasketRefresh', value: 'Y'});

		updateBasket(data);
		$(document).trigger('cartLine_Refresh');
	});

	$(document).on('BigBasket_Refresh', function () {
		var data = new Array();
		data.push({name: 'BasketRefresh', value: 'Y'});
		updateBasket(data)
	});
})(jQuery);