<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

if (CModule::IncludeModule("catalog"))
{
	$arSKU = false;
	$boolSKU = false;
	$arOfferProps = array();

	// get iblock props from all catalog iblocks including sku iblocks
	$arSkuIblockIDs = array();
	$dbCatalog = CCatalog::GetList(array(), array());
	while ($arCatalog = $dbCatalog->GetNext())
	{
		$arSKU = CCatalogSKU::GetInfoByProductIBlock($arCatalog['IBLOCK_ID']);

		if (!empty($arSKU) && is_array($arSKU))
		{
			$arSkuIblockIDs[] = $arSKU["IBLOCK_ID"];
			$arSkuData[$arSKU["IBLOCK_ID"]] = $arSKU;

			$boolSKU = true;
		}
	}

	// iblock props
	$arProps = array();
	foreach ($arSkuIblockIDs as $iblockID)
	{
		$dbProps = CIBlockProperty::GetList(
			array(
				"SORT"=>"ASC",
				"NAME"=>"ASC"
			),
			array(
				"IBLOCK_ID" => $iblockID,
				"ACTIVE" => "Y",
				"CHECK_PERMISSIONS" => "N",
			)
		);

		while ($arProp = $dbProps->GetNext())
		{
			if ($arProp['ID'] == $arSkuData[$arSKU["IBLOCK_ID"]]["SKU_PROPERTY_ID"])
				continue;

			if ($arProp['XML_ID'] == 'CML2_LINK')
				continue;

			$strPropName = '['.$arProp['ID'].'] '.('' != $arProp['CODE'] ? '['.$arProp['CODE'].']' : '').' '.$arProp['NAME'];


			if (($arProp['PROPERTY_TYPE'] == 'L' || $arProp['PROPERTY_TYPE'] == 'E' || ($arProp['PROPERTY_TYPE'] == 'S' && $arProp['USER_TYPE'] == 'directory')) && 'N' == $arProp['MULTIPLE'])
			{
				$arOfferProps[$arProp['CODE']] = $strPropName;
			}
		}

		if (!empty($arOfferProps) && is_array($arOfferProps))
		{
			$arTemplateParameters['OFFERS_PROPS'] = array(
				'PARENT' => 'OFFERS_PROPS',
				'NAME' => GetMessage('SALE_PROPERTIES_RECALCULATE_BASKET'),
				'TYPE' => 'LIST',
				'MULTIPLE' => 'Y',
				'SIZE' => '7',
				'ADDITIONAL_VALUES' => 'N',
				'REFRESH' => 'N',
				'DEFAULT' => '-',
				'VALUES' => $arOfferProps
			);
		}
	}
}
if (\Bitrix\Main\Loader::IncludeModule("yenisite.resizer2")) {
	$resizer_sets_list = array();
	$arSets = CResizer2Set::GetList();
	while ($arr = $arSets->Fetch()) {
		$resizer_sets_list[$arr["id"]] = "[" . $arr["id"] . "] " . $arr["NAME"];
	}

	global $arComponentParameters;

	$arComponentParameters["GROUPS"]["RESIZER_SETS"] = array(
		"NAME" => GetMessage("RESIZER_SETS"),
		"SORT" => 1
	);

	$arTemplateParameters["RESIZER_PRODUCT"] = array(
		"PARENT" => "RESIZER_SETS",
		"NAME" => GetMessage("RESIZER_PRODUCT"),
		"TYPE" => "LIST",
		"VALUES" => $resizer_sets_list,
		"DEFAULT" => "4",
	);
}
if (\Bitrix\Main\Loader::includeModule('sale') && \Bitrix\Main\Loader::includeModule('yenisite.oneclick')) {
	global $arComponentParameters;

	$arComponentParameters['GROUPS']['ONECLICK'] = array('NAME' => GetMessage('RZ_ONECLICK_GROUP'), 'SORT' => 100);

	define('MAX_LINES_IN_LIST', 6);
	$arPersonTypes = array();
	$rsPersonTypes = CSalePersonType::GetList(array(), array('ACTIVE' => 'Y'));
	while ($ar = $rsPersonTypes->Fetch()) {
		$arPersonTypes[$ar['ID']] = '[' . $ar['LID'] . '] ' . $ar['NAME'];
	}

	$arDelivery = array(0 => GetMessage('RZ_ONECLICK_DELIVERY_ID_NOT_SET'));
	$rsDelivery = CSaleDelivery::GetList(array(), array('ACTIVE' => 'Y'));
	while ($ar = $rsDelivery->Fetch()) {
		$arDelivery[$ar['ID']] = '[' . $ar['LID'] . '] ' . $ar['NAME'];
	}
	$rsDelivery = CSaleDeliveryHandler::GetList(array(), array('ACTIVE' => 'Y'));
	while ($ar = $rsDelivery->Fetch()) {
		foreach ($ar['PROFILES'] as $profileSID => $arProfile) {
			$arDelivery[$ar['SID'] . ':' . $profileSID] = '[' . $ar['SID'] . ':' . $profileSID . '] ' . $ar['NAME'] . ' : ' . $arProfile['TITLE'];
		}
	}

	$arTemplateParameters['ONECLICK_PERSON_TYPE_ID'] = array(
		'NAME' => GetMessage('RZ_ONECLICK_PERSON_TYPE_ID'),
		'PARENT' => 'ONECLICK',
		'TYPE' => 'LIST',
		'VALUES' => $arPersonTypes,
		'REFRESH' => 'Y',
		'DEFAULT' => '1',
	);

	$arTemplateParameters['ONECLICK_SHOW_FIELDS'] = array(
		'NAME' => GetMessage('RZ_ONECLICK_SHOW_FIELDS'),
		'PARENT' => 'ONECLICK',
		'TYPE' => 'LIST',
		'MULTIPLE' => 'Y',
		'REFRESH' => 'Y',
		'VALUES' => array('NULL' => GetMessage('RZ_ONECLICK_SHOW_FIELDS_EMPTY')),
		'SIZE' => 2,
	);

	$arTemplateParameters['ONECLICK_REQ_FIELDS'] = array(
		'NAME' => GetMessage('RZ_ONECLICK_REQ_FIELDS'),
		'PARENT' => 'ONECLICK',
		'TYPE' => 'LIST',
		'MULTIPLE' => 'Y',
		'VALUES' => array('NULL' => GetMessage('RZ_ONECLICK_REQ_FIELDS_EMPTY')),
		'SIZE' => 2,
	);

	$arTemplateParameters['ONECLICK_ALLOW_AUTO_REGISTER'] = array(
		'NAME' => GetMessage('RZ_ONECLICK_ALLOW_AUTO_REGISTER'),
		'PARENT' => 'ONECLICK',
		'TYPE' => 'CHECKBOX',
		'DEFAULT' => 'Y',
	);

	$arTemplateParameters['ONECLICK_MESSAGE_OK'] = array(
		'NAME' => GetMessage('RZ_ONECLICK_MESSAGE_OK'),
		'PARENT' => 'ONECLICK',
		'TYPE' => 'STRING',
		'DEFAULT' => GetMessage('RZ_MESSAGE_OK_DEFAULT'),
	);

	$arTemplateParameters['ONECLICK_DELIVERY_ID'] = array(
		'NAME' => GetMessage('RZ_ONECLICK_DELIVERY_ID'),
		'PARENT' => 'ONECLICK',
		'TYPE' => 'LIST',
		'VALUES' => $arDelivery,
		'DEFAULT' => 0,
	);

	$arTemplateParameters['ONECLICK_PAY_SYSTEM_ID'] = array(
		'NAME' => GetMessage('RZ_ONECLICK_PAY_SYSTEM_ID'),
		'PARENT' => 'ONECLICK',
		'TYPE' => 'LIST',
		'VALUES' => array('NULL' => GetMessage('RZ_ONECLICK_PAY_SYSTEM_ID_EMPTY')),
		'DEFAULT' => 0,
	);

	$arTemplateParameters['ONECLICK_AS_EMAIL'] = array(
		'NAME' => GetMessage('RZ_ONECLICK_AS_EMAIL'),
		'PARENT' => 'ONECLICK',
		'TYPE' => 'LIST',
		'VALUES' => array('NULL' => GetMessage('RZ_ONECLICK_AS_EMAIL_EMPTY')),
		'REFRESH' => 'Y',
	);

	$arTemplateParameters['ONECLICK_AS_NAME'] = array(
		'NAME' => GetMessage('RZ_ONECLICK_AS_NAME'),
		'PARENT' => 'ONECLICK',
		'TYPE' => 'LIST',
		'VALUES' => array('NULL' => GetMessage('RZ_ONECLICK_AS_NAME_EMPTY')),
	);

	$arTemplateParameters['ONECLICK_FIELD_PLACEHOLDER'] = array(
		'NAME' => GetMessage('RZ_ONECLICK_FIELD_PLACEHOLDER'),
		'PARENT' => 'ONECLICK',
		'TYPE' => 'CHECKBOX',
		'DEFAULT' => 'Y'
	);

	$arTemplateParameters['ONECLICK_FIELD_QUANTITY'] = array(
		'NAME' => GetMessage('RZ_ONECLICK_FIELD_QUANTITY'),
		'PARENT' => 'ONECLICK',
		'TYPE' => 'CHECKBOX',
		'DEFAULT' => 'Y'
	);

	$arTemplateParameters['ONECLICK_USE_CAPTCHA'] = array(
		'NAME' => GetMessage('RZ_ONECLICK_USE_CAPTCHA'),
		'PARENT' => 'ONECLICK',
		'TYPE' => 'CHECKBOX',
		'DEFAULT' => 'Y'
	);

	$arSaleParams = array();
	if (intval($arCurrentValues['ONECLICK_PERSON_TYPE_ID']) > 0) {
		/** @noinspection PhpDynamicAsStaticMethodCallInspection */
		$rsSaleParams = CSaleOrderProps::GetList(array(), array('PERSON_TYPE_ID' => $arCurrentValues['ONECLICK_PERSON_TYPE_ID']));
		while ($ar = $rsSaleParams->Fetch()) {
			if ('Y' === $ar['REQUIED']) {
				$ar['NAME'] .= ' *';
			}
			$arSaleParams[$ar['CODE']] = $ar['NAME'];
		}
		$arTemplateParameters['ONECLICK_SHOW_FIELDS']['VALUES'] = $arSaleParams;
		$arTemplateParameters['ONECLICK_SHOW_FIELDS']['SIZE'] = (count($arSaleParams) > MAX_LINES_IN_LIST) ? MAX_LINES_IN_LIST : count($arSaleParams);

		$arPaySystem = array(0 => GetMessage('RZ_ONECLICK_PAY_SYSTEM_ID_NOT_SET'));
		$rsPaySystem = CSalePaySystem::GetList(array(), array('PERSON_TYPE_ID' => $arCurrentValues['ONECLICK_PERSON_TYPE_ID'], 'ACTIVE' => 'Y'));
		while ($ar = $rsPaySystem->Fetch()) {
			$arPaySystem[$ar['ID']] = '[' . $ar['LID'] . '] ' . $ar['NAME'];
		}
		$arTemplateParameters['ONECLICK_PAY_SYSTEM_ID']['VALUES'] = $arPaySystem;
	}
	if (is_array($arCurrentValues['ONECLICK_SHOW_FIELDS'])) {
		foreach ($arCurrentValues['ONECLICK_SHOW_FIELDS'] as $key => $val) {
			if (strlen($val) == 0) {
				unset($arCurrentValues['ONECLICK_SHOW_FIELDS'][$key]);
			}
		}
	}
	if (count($arCurrentValues['ONECLICK_SHOW_FIELDS']) > 0) {
		$hasVals = true;
		if (strpos($arCurrentValues['ONECLICK_SHOW_FIELDS'][0], 'NULL') === 0) {
			$hasVals = false;
		}
		if ($hasVals) {
			global $arValues;
			if (!is_array($arValues['ONECLICK_REQ_FIELDS'])) $arValues['ONECLICK_REQ_FIELDS'] = array();
			$arSaleParamsREQ = array();

			foreach ($arCurrentValues['ONECLICK_SHOW_FIELDS'] as $code) {
				if (substr($arSaleParams[$code], -1, 1) == "*") {
					if (array_search($code, $arValues['ONECLICK_REQ_FIELDS']) === false) {
						$arValues['ONECLICK_REQ_FIELDS'][] = $code;
					}
				}
				$arSaleParamsREQ[$code] = $arSaleParams[$code];
			}
			$arTemplateParameters['ONECLICK_REQ_FIELDS']['VALUES'] = $arSaleParamsREQ;
			$arTemplateParameters['ONECLICK_REQ_FIELDS']['SIZE'] = (count($arSaleParamsREQ) > MAX_LINES_IN_LIST) ? MAX_LINES_IN_LIST : count($arSaleParamsREQ);
			$arTemplateParameters['ONECLICK_AS_EMAIL']['VALUES'] = array_merge(array( 0 => GetMessage('RZ_ONECLICK_AS_EMAIL_NOT_USE')), $arSaleParamsREQ);
			$arTemplateParameters['ONECLICK_AS_NAME']['VALUES'] = array_merge(array( 0 => GetMessage('RZ_ONECLICK_AS_NAME_NOT_USE')), $arSaleParamsREQ);
		}
	}

	if (!empty($arCurrentValues['ONECLICK_AS_EMAIL']) && $arCurrentValues['ONECLICK_AS_EMAIL'] !== 0) {
		$arTemplateParameters['ONECLICK_SEND_REGISTER_EMAIL'] = array(
			'PARENT' => 'ONECLICK',
			'NAME' => GetMessage('RZ_ONECLICK_SEND_USER_REGISTER_EMAIL'),
			'TYPE' => 'CHECKBOX',
			'DEFAULT' => 'Y',
		);
	}
	if (!empty($arCurrentValues['ONECLICK_SEND_REGISTER_EMAIL']) && $arCurrentValues['ONECLICK_SEND_REGISTER_EMAIL'] == 'Y') {
		$arTemplateParameters['ONECLICK_USER_REGISTER_EVENT_NAME'] = array(
			'PARENT' => 'ONECLICK',
			'NAME' => GetMessage('RZ_ONECLICK_USER_REGISTER_EVENT_NAME'),
			'TYPE' => 'STRING',
			'DEFAULT' => 'USER_INFO',
		);
	}
}
?>