(typeof(jQuery) != 'undefined')
&& (function ($) {
	submitForm = function (val) {
		if (val != 'Y')
			BX('confirmorder').value = 'N';

		var orderForm = BX('ORDER_FORM'),
			$orderForm = $("#ORDER_FORM");
		$orderForm.setAjaxLoading();
		BX.ajax.submitComponentForm(orderForm, 'order_form_content', false);
		BX.submit(orderForm);
		BX.addCustomEvent('onAjaxSuccess', function () {
			$orderForm.reStyler();
			$orderForm.reSpin();
            $orderForm.refreshForm();
			$orderForm.stopAjaxLoading();
		});

		return true;
	};
	$('.ordering_block.step_4').find('input[type=submit]:not(.btn)').addClass('btn btn-primary');
})(jQuery);