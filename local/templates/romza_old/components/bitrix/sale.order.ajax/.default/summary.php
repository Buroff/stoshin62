<? use Yenisite\Core\Resize;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$bDefaultColumns = $arResult["GRID"]["DEFAULT_COLUMNS"];
$colspan = ($bDefaultColumns) ? count($arResult["GRID"]["HEADERS"]) : count($arResult["GRID"]["HEADERS"]) - 1;
$bPropsColumn = false;
$bUseDiscount = false;
$bPriceType = false;
$bShowNameWithPicture = ($bDefaultColumns) ? true : false; // flat to show name and picture column in one column
?>
<h4><?=GetMessage("SALE_PRODUCTS_SUMMARY");?></h4>
<div class="ordering_block step_1">
	<div class="basket_order_form">
		<table id="basket_items">
	<thead class="basket_order__head">
			<tr>
				<?
				$bPreviewPicture = false;
				$bDetailPicture = false;
				$imgCount = 0;

				// prelimenary column handling
				foreach ($arResult["GRID"]["HEADERS"] as $id => $arColumn)
				{
					if ($arColumn["id"] == "PROPS")
						$bPropsColumn = true;

					if ($arColumn["id"] == "NOTES")
						$bPriceType = true;

					if ($arColumn["id"] == "PREVIEW_PICTURE")
						$bPreviewPicture = true;

					if ($arColumn["id"] == "DETAIL_PICTURE")
						$bDetailPicture = true;
				}

				if ($bPreviewPicture || $bDetailPicture)
					$bShowNameWithPicture = true;


				foreach ($arResult["GRID"]["HEADERS"] as $id => $arColumn):

					if (in_array($arColumn["id"], array("PROPS", "TYPE", "NOTES"))) // some values are not shown in columns in this template
						continue;

					if ($arColumn["id"] == "PREVIEW_PICTURE" && $bShowNameWithPicture)
						continue;

					if ($arColumn["id"] == "NAME" && $bShowNameWithPicture):
					?>
						<th class="title" colspan="2">
					<?
						echo GetMessage("SALE_PRODUCTS");
					elseif ($arColumn["id"] == "NAME" && !$bShowNameWithPicture):
					?>
						<th class="title">
					<?
						echo $arColumn["name"];
					elseif ($arColumn["id"] == "PRICE"):
					?>
						<th class="price">
					<?
						echo $arColumn["name"];
					else:
					?>
						<th class="custom">
					<?
						echo $arColumn["name"];
					endif;
					?>
						</th>
				<?endforeach;?>
			</tr>
		</thead>

		<tbody class="basket_order__main">
			<?foreach ($arResult["GRID"]["ROWS"] as $k => $arData):?>
			<tr>
				<?
				if ($bShowNameWithPicture):
				?>
					<td class="img">
							<?
							if (strlen($arData["data"]["DETAIL_PAGE_URL"]) > 0):?><a href="<?=$arData["data"]["DETAIL_PAGE_URL"] ?>"><?endif;?>
								<img src="<?= Resize::GetResizedImg($arData["data"], array('WIDTH' => 168, 'HEIGHT' => 170, 'SET_ID' => intval($arParams['RESIZER_PRODUCT'])))?>" alt=""/>
							<?if (strlen($arData["data"]["DETAIL_PAGE_URL"]) > 0):?></a><?endif;?>
					</td>
				<?
				endif;
				// prelimenary check for images to count column width
				foreach ($arResult["GRID"]["HEADERS"] as $id => $arColumn)
				{
					$arItem = (isset($arData["columns"][$arColumn["id"]])) ? $arData["columns"] : $arData["data"];
					if (is_array($arItem[$arColumn["id"]]))
					{
						foreach ($arItem[$arColumn["id"]] as $arValues)
						{
							if ($arValues["type"] == "image")
								$imgCount++;
						}
					}
				}

				foreach ($arResult["GRID"]["HEADERS"] as $id => $arColumn):

					$class = ($arColumn["id"] == "PRICE_FORMATED") ? "price" : "";

					if (in_array($arColumn["id"], array("PROPS", "TYPE", "NOTES"))) // some values are not shown in columns in this template
						continue;

					if ($arColumn["id"] == "PREVIEW_PICTURE" && $bShowNameWithPicture)
						continue;

					$arItem = (isset($arData["columns"][$arColumn["id"]])) ? $arData["columns"] : $arData["data"];

					if ($arColumn["id"] == "NAME"):
						$width = 70 - ($imgCount * 20);
					?>
						<td class="title">
								<?if (strlen($arItem["DETAIL_PAGE_URL"]) > 0):?><a href="<?=$arItem["DETAIL_PAGE_URL"] ?>"><?endif;?>
									<?=$arItem["NAME"]?>
								<?if (strlen($arItem["DETAIL_PAGE_URL"]) > 0):?></a><?endif;?>
							<? if (count($arItem['PROPS']) > 0): ?>
								<div class="help-block">
									<? foreach($arItem['PROPS'] as $arProp):?>
										<?= $arProp['NAME'] , ' : ' , $arProp['VALUE'] ?><br/>
									<? endforeach ?>
								</div>
							<? endif ?>
						</td>
					<?
					elseif ($arColumn["id"] == "PRICE_FORMATED"):
					?>
						<td class="price">
							<div class="current_price"><?=$arItem["~PRICE_FORMATED"]?></div>
							<div class="old_price" style="text-decoration:line-through; color:#828282;">
								<?
								if (doubleval($arItem["DISCOUNT_PRICE"]) > 0):
									echo SaleFormatCurrency($arItem["PRICE"] + $arItem["DISCOUNT_PRICE"], $arItem["CURRENCY"]);
									$bUseDiscount = true;
								endif;
								?>
							</div>

							<?if ($bPriceType && strlen($arItem["NOTES"]) > 0):?>
								<div style="text-align: left">
									<div class="type_price"><?=GetMessage("SALE_TYPE")?></div>
									<div class="type_price_value"><?=$arItem["NOTES"]?></div>
								</div>
							<?endif;?>
						</td>
					<?
					elseif ($arColumn["id"] == "DISCOUNT"):
					?>
						<td class="discount">
							<?=$arItem["DISCOUNT_PRICE_PERCENT_FORMATED"]?>
						</td>
					<?
					elseif ($arColumn["id"] == "DETAIL_PICTURE" && $bPreviewPicture):
					?>
						<td class="img">
								<?
								$url = "";
								if ($arColumn["id"] == "DETAIL_PICTURE" && strlen($arData["data"]["DETAIL_PICTURE_SRC"]) > 0)
									$url = $arData["data"]["DETAIL_PICTURE_SRC"];

								if ($url == "")
									$url = $templateFolder."/images/no_photo.png";

								if (strlen($arData["data"]["DETAIL_PAGE_URL"]) > 0):?><a href="<?=$arData["data"]["DETAIL_PAGE_URL"] ?>"><?endif;?>
									<img src="<?=$url?>" alt=""/>
								<?if (strlen($arData["data"]["DETAIL_PAGE_URL"]) > 0):?></a><?endif;?>
						</td>
					<?
					elseif (in_array($arColumn["id"], array("QUANTITY", "WEIGHT_FORMATED", "DISCOUNT_PRICE_PERCENT_FORMATED", "SUM"))):
					?>
						<td class="amount">
							<?if(!empty($arItem['~'.$arColumn["id"]])):?><?=$arItem['~'.$arColumn["id"]]?><?else:?><?=$arItem[$arColumn["id"]]?><?endif;?>
						</td>
					<?
					else: // some property value

						if (is_array($arItem[$arColumn["id"]])):

							foreach ($arItem[$arColumn["id"]] as $arValues)
								if ($arValues["type"] == "image")
									$columnStyle = "width:20%";
						?>
						<td class="custom" style="<?=$columnStyle?>">
							<?
							foreach ($arItem[$arColumn["id"]] as $arValues):
								if ($arValues["type"] == "image"):
								?>
									<div class="bx_ordercart_photo_container">
										<div class="bx_ordercart_photo" style="background-image:url('<?=$arValues["value"]?>')"></div>
									</div>
								<?
								else: // not image
									echo $arValues["value"]."<br/>";
								endif;
							endforeach;
							?>
						</td>
						<?
						else: // not array, but simple value
						?>
						<td class="custom" style="<?=$columnStyle?>">
							<span><?=getColumnName($arColumn)?>:</span>
							<?
								echo $arItem[$arColumn["id"]];
							?>
						</td>
						<?
						endif;
					endif;

				endforeach;
				?>
			</tr>
			<?endforeach;?>
		</tbody>
	</table>
		<div class="basket_order__foot">
			<div class="basket_order__submit column"></div>
			<div class="basket_order__total column">
				<div>
					<?=GetMessage("SOA_TEMPL_SUM_WEIGHT_SUM")?>
					<b><?=$arResult["ORDER_WEIGHT_FORMATED"]?></b>
				</div>
				<div>
					<?=GetMessage("SOA_TEMPL_SUM_SUMMARY")?>
					<b><?=$arResult["ORDER_PRICE_FORMATED"]?></b>
				</div>
				<?
				if (doubleval($arResult["DISCOUNT_PRICE"]) > 0)
				{
					?>
					<div>
						<?=GetMessage("SOA_TEMPL_SUM_DISCOUNT")?><?if (strLen($arResult["DISCOUNT_PERCENT_FORMATED"])>0):?> (<?echo $arResult["DISCOUNT_PERCENT_FORMATED"];?>)<?endif;?>:
						<b><?= $arResult["DISCOUNT_PRICE_FORMATED"]?></b>
					</div>
				<?
				}
				if(!empty($arResult["TAX_LIST"]))
				{
					foreach($arResult["TAX_LIST"] as $val)
					{
						?>
						<div>
							<?=$val["VALUE_FORMATED"]?>:
							<b><?=$val["VALUE_MONEY_FORMATED"]?></b>
						</div>
					<?
					}
				}
				if (doubleval($arResult["DELIVERY_PRICE"]) > 0)
				{
					?>
					<div>
						<?=GetMessage("SOA_TEMPL_SUM_DELIVERY")?>
						<b><?=$arResult["DELIVERY_PRICE_FORMATED"]?></b>
					</div>
				<?
				}
				if (strlen($arResult["PAYED_FROM_ACCOUNT_FORMATED"]) > 0)
				{
					?>
					<div>
						<?=GetMessage("SOA_TEMPL_SUM_PAYED")?>
						<b><?=$arResult["PAYED_FROM_ACCOUNT_FORMATED"]?></b>
					</div>
				<?
				}

				if ($bUseDiscount):
					?>
					<div>
						<?=GetMessage("SOA_TEMPL_SUM_IT")?>
						<b><?=$arResult["ORDER_TOTAL_PRICE_FORMATED"]?></b>
					</div>
					<div>
						<b style="text-decoration:line-through; color:#828282;"><?=$arResult["PRICE_WITHOUT_DISCOUNT"]?></b>
					</div>
				<?
				else:
					?>
					<div>
						<?=GetMessage("SOA_TEMPL_SUM_IT")?>
						<b><?=$arResult["ORDER_TOTAL_PRICE_FORMATED"]?></b>
					</div>
				<?
				endif;
				?>
			</div>
		</div>
		<div class="ordering_block__inner">
			<div class="ordering_block__inner_head"><?=GetMessage("SOA_TEMPL_SUM_COMMENTS")?></div>
			<div class="comment_block">
				<div class="comment_head"><??></div>
				<textarea name="ORDER_DESCRIPTION" id="ORDER_DESCRIPTION" rows="10" class="form-control"><?=$arResult["USER_VALS"]["ORDER_DESCRIPTION"]?></textarea>
			</div>
			<input type="hidden" name="" value="">
		</div>
	</div>
</div>
