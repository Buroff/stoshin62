<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/**
 * @var array $arParams
 * @var array $arResult
 * @var SaleOrderAjax $component
 */

$component = $this->__component;
$component::scaleImages($arResult['JS_DATA'], $arParams['SERVICES_IMAGES_SCALING']);

// $obProps = Bitrix\Sale\Internals\OrderPropsValueTable::getList();
// while($prop = $obProps->Fetch()){
//    echo $prop['NAME'].': '.$prop['VALUE'];
// }