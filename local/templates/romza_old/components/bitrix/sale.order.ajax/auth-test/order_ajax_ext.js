(function () {
    'use strict'; 
 

 var initParent = BX.Sale.OrderAjaxComponent.init,
        getBlockFooterParent = BX.Sale.OrderAjaxComponent.getBlockFooter,
        editOrderParent = BX.Sale.OrderAjaxComponent.editOrder,
        editDeliveryInfoParent = BX.Sale.OrderAjaxComponent.editDeliveryInfo,
        initOptionsParent = BX.Sale.OrderAjaxComponent.initOptions,
        editFadeDeliveryContentParent = BX.Sale.OrderAjaxComponent.editFadeDeliveryContent,
        editPaySystemInfoParent = BX.Sale.OrderAjaxComponent.editPaySystemInfo
    ;

    BX.namespace('BX.Sale.OrderAjaxComponentExt');
    BX.Sale.OrderAjaxComponentExt = BX.Sale.OrderAjaxComponent;

    BX.Sale.OrderAjaxComponentExt.getBlockFooter = function (node) {
        var parentNodeSection = BX.findParent(node, {className: 'bx-soa-section'});

        getBlockFooterParent.apply(this, arguments);

        // if (/bx-soa-properties/.test(parentNodeSection.id)) {
        //     BX.remove(parentNodeSection.querySelector('.pull-left'));
        //     /*BX.remove(parentNodeSection.querySelector('.pull-right'));*/
        // }

         if (/bx-soa-delivery/.test(parentNodeSection.id)) {
            // BX.remove(parentNodeSection.querySelector('.pull-left'));
            BX.remove(parentNodeSection.querySelector('.pull-right'));
        }

    };


    BX.Sale.OrderAjaxComponentExt.initOptions = function() {
        initOptionsParent.apply(this, arguments);
        this.propertyDeliveryCollection = new BX.Sale.PropertyCollection(BX.merge({publicMode: true}, this.result.DELIVERY_PROPS));
       this.propertyPaysystemCollection = new BX.Sale.PropertyCollection(BX.merge({publicMode: true}, this.result.PAY_PROPS));
    };


     BX.Sale.OrderAjaxComponentExt.editDeliveryInfo = function(deliveryNode) {
        editDeliveryInfoParent.apply(this, arguments); //вызываем родителя
        // var deliveryInfoContainer = deliveryNode.querySelector('.bx-soa-pp-company-desc'); //находим блок с описанием службы доставки
        var deliveryInfoContainer = deliveryNode.querySelector('.bx-soa-pp-item-container'); 
        console.log('---- deliveryNode ----');
        console.log(deliveryNode);
        // console.log(deliveryInfoContainer);

        var group, property, groupIterator = this.propertyDeliveryCollection.getGroupIterator(), propsIterator, htmlAddress;
//используем коллекцию, инициализированную в предыдущем методе
        var deliveryItemsContainer = BX.create('DIV', {props: {className: 'col-sm-12 bx-soa-delivery'}}); //создаем контейнер для будущего поля
        while (group = groupIterator())
        {
            propsIterator =  group.getIterator();
            while (property = propsIterator())
            {
                console.log(property);
                console.log(property.getGroupId());
                if (property.getGroupId()==5) { //если это свойство является параметром доставки
                    this.getPropertyRowNode(property, deliveryItemsContainer, false); //вставляем свойство в подготовленный контейнер
                    deliveryInfoContainer.appendChild(deliveryItemsContainer); //контейнер вместе со свойством в нём добавляем в конце блока с описанием (deliveryInfoContainer)

                }
            }
        }
    };

    
BX.Sale.OrderAjaxComponentExt.initValidation = function() {
        if (!this.result.ORDER_PROP || !this.result.ORDER_PROP.properties)
            return;

        var properties = this.result.ORDER_PROP.properties, 

            deliveryProps = this.result.DELIVERY_PROPS.properties,
            obj = {}, deliveryObj = {}, i;


        for (i in properties)
        {
            if (properties.hasOwnProperty(i))
                obj[properties[i].ID] = properties[i];
        }
        for (i in deliveryProps)
        {
            if (deliveryProps.hasOwnProperty(i))
                deliveryObj[deliveryProps[i].ID] = deliveryProps[i];
        }

        this.validation.properties = obj;
        this.validation.deliveryProperties = deliveryObj;
    };

BX.Sale.OrderAjaxComponentExt.isValidDeliveryBlock = function(excludeLocation) {
        if (!this.options.propertyValidation)
            return [];

        var props = this.orderBlockNode.querySelectorAll('.bx-soa-customer-field[data-property-id-row]'),
            propsErrors = [],
            id, propContainer, arProperty, data, i;
        for (i = 0; i < props.length; i++)
        {
            id = props[i].getAttribute('data-property-id-row');

            if (!!excludeLocation && this.locations[id])
                continue;

            propContainer = props[i].querySelector('.soa-property-container');
            if (propContainer)
            {
                arProperty = this.validation.deliveryProperties[id];
                data = this.getValidationData(arProperty, propContainer);
                propsErrors = propsErrors.concat(this.isValidProperty(data, true));
            }
        }
        return propsErrors;
    };

    BX.Sale.OrderAjaxComponentExt.editFadeDeliveryContent = function(node) {
        editFadeDeliveryContentParent.apply(this, arguments);
        if (this.initialized.delivery) { //проверяем, была ли инициализирована доставка
            var validDeliveryErrors = this.isValidDeliveryBlock(); //вызываем наш метод
            if (validDeliveryErrors.length && BX.hasClass(BX.findParent(node),'bx-selected') == true) {
                this.showError(this.deliveryBlockNode, validDeliveryErrors);
            } else { //если ошибок нет и всё в порядке
                node.querySelector('.alert.alert-danger').style.display = 'none';

                var section = BX.findParent(node.querySelector('.alert.alert-danger'), {className: 'bx-soa-section'});

                node.setAttribute('data-visited', 'true');
                BX.removeClass(section, 'bx-step-error'); //убираем иконку, что есть ошибка в этом шаге
                BX.addClass(section, 'bx-step-completed'); //выставляем, что блок валиден и готов
            }
        }
    };


    BX.Sale.OrderAjaxComponentExt.saveOrder = function(result) {
        var res = BX.parseJSON(result), redirected = false;
        if (res && res.order)
        {
            result = res.order;
            this.result.SHOW_AUTH = result.SHOW_AUTH;
            this.result.AUTH = result.AUTH;

            if (this.result.SHOW_AUTH)
            {
                this.editAuthBlock();
                this.showAuthBlock();
                this.animateScrollTo(this.authBlockNode);
            }
            else
            {
                if (result.REDIRECT_URL && result.REDIRECT_URL.length)
                {
                    if (this.params.USE_ENHANCED_ECOMMERCE === 'Y')
                    {
                        this.setAnalyticsDataLayer('purchase', result.ID);
                    }

                    redirected = true;
                    document.location.href = result.REDIRECT_URL;
                }
                if (result.ERROR.hasOwnProperty('PROPERTY')) {
                    result.ERROR['DELIVERY'] = result.ERROR.PROPERTY;
                    delete result.ERROR.PROPERTY;
                }
                this.showErrors(result.ERROR, true, true);
            }
        }

        if (!redirected)
        {
            this.endLoader();
            this.disallowOrderSave();
        }
    };
 
})();