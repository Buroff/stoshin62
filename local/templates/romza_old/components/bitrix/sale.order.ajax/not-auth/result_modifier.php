<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/**
 * @var array $arParams
 * @var array $arResult
 * @var SaleOrderAjax $component
 */

$component = $this->__component;
$component::scaleImages($arResult['JS_DATA'], $arParams['SERVICES_IMAGES_SCALING']);


// foreach ($arParams as $key => $value) {
// 	echo '<pre>';
// 	echo $key;
// 	var_dump($value);
// 	echo '</pre>';

// }

// unset($arResult["STORE_LIST"][1]["ADDRESS"]);
// unset($arResult["STORE_LIST"][1]["TITLE"]);
// unset($arResult["STORE_LIST"][1]["DESCRIPTION"]);
// unset($arResult["STORE_LIST"][1]);



// foreach ($arResult as $key => $item) {
// 		echo '<pre>';
// 		echo $key;
// 		echo '<br/>';
// 		var_dump($item);
// 		echo '</pre>';

// }