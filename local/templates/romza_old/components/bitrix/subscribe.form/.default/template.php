<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
?>
<? if (method_exists($this, 'setFrameMode')) $this->setFrameMode(true); ?>
<div class="subscribe_block aside_block">
	<h2><?= GetMessage('RZ_SUBSCRIBE_TITLE') ?></h2>

	<form class="subscribe_form" action="<?= $arResult["FORM_ACTION"] ?>">
		<? foreach ($arResult["RUBRICS"] as $itemID => $itemValue): ?>
			<input type="hidden" name="sf_RUB_ID[]" id="sf_RUB_ID_<?= $itemValue["ID"] ?>" value="<?= $itemValue["ID"] ?>"/>
		<? endforeach; ?>
		<div class="intro"><?= GetMessage('RZ_SUBSCRIBE_INTRO') ?></div>
		<div class="fantasy-form-control-wrapper">
			<input class="form-control" type="email" name="sf_EMAIL" value="<?= $arResult["EMAIL"] ?>"
				   title="<?= GetMessage("subscr_form_email_title") ?>">
		</div>
		<div>
			<button class="btn btn-primary" type="submit" name="OK"><?= GetMessage("subscr_form_button") ?></button>
		</div>
	</form>
</div>
