<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>
<?
use \Yenisite\Core\Tools;
\Bitrix\Main\Localization\Loc::loadMessages($_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH . '/lang/'.LANGUAGE_ID.'/header.php');
$pathToRules = COption::GetOptionString(CRZShinmarketSettings::getModuleId(),'path_tu_rules_privacy',SITE_DIR.'personal/rules/personal_data.php');
$rand = $this->randString();

ShowMessage($arParams["~AUTH_RESULT"]);
ShowMessage($arResult['ERROR_MESSAGE']);
?>
	<h2>
		<span class="brand-primary-color flaticon-incoming4"></span>
		<?= GetMessage('AUTH_PLEASE_AUTH') ?>
	</h2>
	<div><?= GetMessage('RZ_OR') ?> <a href="#" data-dismiss="modal" data-toggle="modal"
									   data-target="#modal-register"><?= GetMessage('RZ_REG_TITLE') ?></a></div>
<?
if ($arResult['SHOW_ERRORS'] == 'Y' && $arResult['ERROR']):?>
	<div class="message message-error">
		<? ShowMessage($arResult['ERROR_MESSAGE']); ?>
	</div>
<? endif; ?>
	<form class="form-horizontal form-login modal-form" role="form">
        <input type="hidden" name="privacy_policy" value="N"/>
        <? if ($arResult["BACKURL"] <> ''): ?>
			<input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>"/>
		<? endif ?>
		<? foreach ($arResult["POST"] as $key => $value): ?>
			<input type="hidden" name="<?= $key ?>" value="<?= $value ?>"/>
		<? endforeach ?>
		<input type="hidden" name="AUTH_FORM" value="Y"/>
		<input type="hidden" name="TYPE" value="AUTH"/>

		<div class="form-group">
			<label for="popup_auth_login_<?= $arResult["RND"]; ?>" class="control-label"><?= GetMessage("RZ_FORM_LOGIN") ?></label>

			<div class="control-wrapper">
				<input id="popup_auth_login_<?= $arResult["RND"]; ?>" type="text" class="form-control" name="USER_LOGIN">
			</div>
		</div>
		<div class="form-group">
			<label for="popup_auth_password_<?= $arResult["RND"]; ?>" class="control-label"><?= GetMessage("RZ_FORM_PASS") ?></label>

			<div class="control-wrapper">
				<input id="popup_auth_password_<?= $arResult["RND"]; ?>" class="form-control password-field" type="password"
					   name="USER_PASSWORD">
			</div>
		</div>
		<div class="form-group manage">
			<? if ($arResult["STORE_PASSWORD"] == "Y"): ?>
				<span class="forms-wrapper remember">
						<input type="checkbox" id="popup_auth_remember_<?= $arResult["RND"]; ?>" name="USER_REMEMBER">
						<label for="popup_auth_remember_<?= $arResult["RND"]; ?>"><?= GetMessage("RZ_FORM_REMEMBER_ME") ?></label>
					</span>
			<? endif; ?>
			<a class="recovery" href="<?= $arResult["AUTH_FORGOT_PASSWORD_URL"] ?>"
			   rel="nofollow"><?= GetMessage("RZ_FORM_FORGOT_PASS") ?></a>
		</div>
        <? if (!empty($arResult["CAPTCHA_CODE"])): ?>
            <div class="form-group">
                <img alt="<?= GetMessage("CAPTCHA_ALT") ?>"
                     src="/bitrix/tools/captcha.php?captcha_code=<?= $arResult["CAPTCHA_CODE"] ?>"/>
                <input type="hidden" name="captcha_code" value="<?= $arResult["CAPTCHA_CODE"] ?>"/>
            </div>
            <div class="form-group required">
                <label for="popup_call_captcha" class="control-label"><?= GetMessage("CAPTCHA_TITLE") ?>:</label>
                <input id='popup_call_captcha' class="form-control" type="text" name="captcha_word"/>
            </div>
        <? endif; ?>
        <div class="form-group agreement-policy">
            <div class="forms-wrapper">
                <input id="privacy_policy_<?=$rand?>" required type="checkbox" name="privacy_policy" value="Y" <?=$_REQUEST['privacy_policy'] == 'Y' ? ' checked' : ''?>>
                <label for="privacy_policy_<?=$rand?>">
                    <?= GetMessage('RZ_ACCEPT') ?> <?=GetMessage('RZ_CONCLUSION',array("#URL#" => Tools::GetConstantUrl($pathToRules)))?>
                </label>
            </div>
        </div>
		<div class="form-group submit">
			<button type="submit" class="btn btn-primary btn-submit-login"><?= GetMessage("RZ_FORM_ENTER") ?></button>
		</div>

	</form>
<? if ($arResult["AUTH_SERVICES"]): ?>
	<div class="form-login">
		<? $APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "",
			array(
				"AUTH_SERVICES" => $arResult["AUTH_SERVICES"],
				"AUTH_URL" => $arResult["AUTH_URL"],
				"POST" => $arResult["POST"],
				"POPUP" => "N",
				"SUFFIX" => "form_" . $arResult["RND"],
			),
			$component,
			array("HIDE_ICONS" => "Y")
		); ?>
	</div>
<? endif ?>