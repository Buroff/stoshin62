<?
$MESS["RZ_PEREJTI_V_IZBRANNOE"] = "Перейти в избранное";
$MESS["RZ_VIJTI"] = "выйти";
$MESS["RZ_LOGIN_BUTTON"] = "Войти";
$MESS["RZ_LOGIN_TITLE"] = "Вход в личный кабинет";
$MESS["RZ_OR"] = "или";
$MESS["RZ_REG_TITLE"] = "регистрация";
$MESS["RZ_FORM_LOGIN"] = "Логин";
$MESS["RZ_FORM_PASS"] = "Пароль";
$MESS["RZ_FORM_REMEMBER_ME"] = "Запомнить меня";
$MESS["RZ_FORM_FORGOT_PASS"] = "Забыли пароль?";
$MESS["RZ_FORM_ENTER"] = "Войти";
$MESS["RZ_SOCSERV_AUTH"] = "Войти через социальные сети";
$MESS["RZ_PERSONAL_CABINET"] = "Личный кабинет";
$MESS["RZ_AUTH_SUCCESS"] = "Вы успешно авторизовались";
