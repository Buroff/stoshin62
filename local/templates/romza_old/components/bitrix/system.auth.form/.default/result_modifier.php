<? use Yenisite\Core\Tools;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
include_once $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/tools/include_module.php';

$arResult["PROFILE_URL"] = Tools::GetConstantUrl($arResult["PROFILE_URL"]);
$arResult["AUTH_FORGOT_PASSWORD_URL"] = Tools::GetConstantUrl($arResult["AUTH_FORGOT_PASSWORD_URL"]);