<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
use \Yenisite\Core\Catalog;
use Yenisite\Core\Resize;
use Yenisite\Core\Tools;

/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
$arDefaultParams = array(
	'ADD_PICT_PROP' => '-',
	'OFFER_ADD_PICT_PROP' => '-',
	'SHOW_DISCOUNT_PERCENT' => 'N',
	'SHOW_OLD_PRICE' => 'N',
	'SHOW_PAGINATION' => 'Y',
	'MESS_BTN_BUY' => '',
	'MESS_BTN_ADD_TO_BASKET' => '',
	'MESS_BTN_DETAIL' => '',
	'MESS_NOT_AVAILABLE' => ''
);
if (!CModule::IncludeModule('catalog') && CModule::IncludeModule('yenisite.market')) {
	$arResult['CHECK_QUANTITY'] = (CMarketCatalog::UsesQuantity($arParams['IBLOCK_ID']) == 1);
}
$resizeParams = array('WIDTH' => 168, 'HEIGHT' => 170, 'SET_ID' => intval($arParams['RESIZER_PRODUCT']));
$arParams = array_merge($arDefaultParams, $arParams);

$arParams['ADD_PICT_PROP'] = trim($arParams['ADD_PICT_PROP']);
if ('-' == $arParams['ADD_PICT_PROP'])
	$arParams['ADD_PICT_PROP'] = '';
$arParams['OFFER_ADD_PICT_PROP'] = trim($arParams['OFFER_ADD_PICT_PROP']);
if ('-' == $arParams['OFFER_ADD_PICT_PROP'])
	$arParams['OFFER_ADD_PICT_PROP'] = '';
if ('Y' != $arParams['SHOW_DISCOUNT_PERCENT'])
	$arParams['SHOW_DISCOUNT_PERCENT'] = 'N';
if ('Y' != $arParams['SHOW_OLD_PRICE'])
	$arParams['SHOW_OLD_PRICE'] = 'N';

$arParams['MESS_BTN_BUY'] = trim($arParams['MESS_BTN_BUY']);
$arParams['MESS_BTN_ADD_TO_BASKET'] = trim($arParams['MESS_BTN_ADD_TO_BASKET']);
$arParams['MESS_BTN_DETAIL'] = trim($arParams['MESS_BTN_DETAIL']);
$arParams['MESS_NOT_AVAILABLE'] = trim($arParams['MESS_NOT_AVAILABLE']);


if (!empty($arResult['ITEMS'])) {
	$boolSKU = false;
	$strBaseCurrency = '';
	$boolConvert = isset($arResult['CONVERT_CURRENCY']['CURRENCY_ID']);

	if ($arResult['MODULES']['catalog']) {
		if (!$boolConvert)
			$strBaseCurrency = CCurrency::GetBaseCurrency();

		$arSKU = CCatalogSKU::GetInfoByProductIBlock($arParams['IBLOCK_ID']);
		$boolSKU = !empty($arSKU) && is_array($arSKU);
	}

	$arNewItemsList = array();
	foreach ($arResult['ITEMS'] as $key => $arItem) {
		$arItem['CHECK_QUANTITY'] = false;
		if (!isset($arItem['CATALOG_MEASURE_RATIO']))
			$arItem['CATALOG_MEASURE_RATIO'] = 1;
		if (!isset($arItem['CATALOG_QUANTITY']))
			$arItem['CATALOG_QUANTITY'] = 0;
		$arItem['CATALOG_QUANTITY'] = (
		0 < $arItem['CATALOG_QUANTITY'] && is_float($arItem['CATALOG_MEASURE_RATIO'])
			? floatval($arItem['CATALOG_QUANTITY'])
			: intval($arItem['CATALOG_QUANTITY'])
		);
		$arItem['CATALOG'] = false;
		if (!isset($arItem['CATALOG_SUBSCRIPTION']) || 'Y' != $arItem['CATALOG_SUBSCRIPTION'])
			$arItem['CATALOG_SUBSCRIPTION'] = 'N';

		$productPictures = CIBlockPriceTools::getDoublePicturesForItem($arItem, $arParams['ADD_PICT_PROP']);

		if (empty($productPictures['SECOND_PICT']))
			$productPictures['SECOND_PICT'] = $productPictures['PICT'];

		$arItem['SECOND_PICT'] = true;

		if ($arResult['MODULES']['catalog']) {
			$arItem['CATALOG'] = true;
			if (!isset($arItem['CATALOG_TYPE']))
				$arItem['CATALOG_TYPE'] = CCatalogProduct::TYPE_PRODUCT;
			if (
				(CCatalogProduct::TYPE_PRODUCT == $arItem['CATALOG_TYPE'] || CCatalogProduct::TYPE_SKU == $arItem['CATALOG_TYPE'])
				&& !empty($arItem['OFFERS'])
			) {
				$arItem['CATALOG_TYPE'] = CCatalogProduct::TYPE_SKU;
			}
			switch ($arItem['CATALOG_TYPE']) {
				case CCatalogProduct::TYPE_SET:
					$arItem['OFFERS'] = array();
					$arItem['CATALOG_MEASURE_RATIO'] = 1;
					$arItem['CATALOG_QUANTITY'] = 0;
					$arItem['CHECK_QUANTITY'] = false;
					break;
				case CCatalogProduct::TYPE_SKU:
					break;
				case CCatalogProduct::TYPE_PRODUCT:
				default:
					$arItem['CHECK_QUANTITY'] = ('Y' == $arItem['CATALOG_QUANTITY_TRACE'] && 'N' == $arItem['CATALOG_CAN_BUY_ZERO']);
					break;
			}
		} else {
			$arItem['CATALOG_TYPE'] = 0;
			$arItem['OFFERS'] = array();
		}

		if ($arItem['CATALOG'] && isset($arItem['OFFERS']) && !empty($arItem['OFFERS'])) {
			$arItem['MIN_PRICE'] = CIBlockPriceTools::getMinPriceFromOffers(
				$arItem['OFFERS'],
				$boolConvert ? $arResult['CONVERT_CURRENCY']['CURRENCY_ID'] : $strBaseCurrency
			);
		}

		if ($arResult['MODULES']['catalog'] && $arItem['CATALOG'] && CCatalogProduct::TYPE_PRODUCT == $arItem['CATALOG_TYPE']) {
			CIBlockPriceTools::setRatioMinPrice($arItem, true);
		}

		if (!empty($arItem['DISPLAY_PROPERTIES'])) {
			foreach ($arItem['DISPLAY_PROPERTIES'] as $propKey => $arDispProp) {
				if ('F' == $arDispProp['PROPERTY_TYPE'])
					unset($arItem['DISPLAY_PROPERTIES'][$propKey]);
			}
		}
		$arItem['LAST_ELEMENT'] = 'N';
		$arItem['PICTURE'] = Resize::GetResizedImg($arItem, $resizeParams);
		$arItem['PICTURE_SECOND'] = Resize::GetResizedImg($productPictures['SECOND_PICT']['ID'], $resizeParams);
		if (count($arItem['OFFERS']) > 0) {
			/** @noinspection PhpVoidFunctionResultUsedInspection */
			foreach ($arItem['OFFERS'] as &$arOffer){
				$arOffer = CRZShinmarket::getDisplayValueForProps($arOffer);
			}
			unset($arOffer);
			$arItem['FLAT_OFFERS'] = Catalog::getFlatOffersList($arItem, $arParams['OFFERS_PROPERTY_CODE']);
		}
		if (isset($arItem['FLAT_OFFERS']) && !empty($arItem['FLAT_OFFERS'])) {
			foreach($arItem['FLAT_OFFERS'] as &$arFlat) {
				if($arFlat['CAN_BUY'] == true) {
					$arItem['MIN_PRICE']['PRINT_VALUE'] = htmlspecialcharsback($arFlat['PRICE_OLD']);
					$arItem['MIN_PRICE']['PRINT_DISCOUNT_VALUE'] = htmlspecialcharsback($arFlat['PRICE']);
					$arItem['CUR_OFFER'] = $arFlat['ID'];
					$arItem['CAN_BUY'] = $arFlat['CAN_BUY'];
					break;
				}
			} unset($arFlat);
		}
		//Prices for MARKET
		if (!CModule::IncludeModule('catalog') && CModule::IncludeModule('yenisite.market')) {
			include_once $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/tools/include_module.php';

			$prices = CMarketPrice::GetItemPriceValues($arItem['ID'], $arItem['PRICES']);
			if (count($prices) > 0) {
				unset($arItem['PRICES']);
			}
			$minPrice = false;
			foreach ($prices as $k => $pr) {
				$pr = floatval($pr);
				$arItem['PRICES'][$k]['VALUE'] = $pr;
				$arItem['PRICES'][$k]['PRINT_VALUE'] = $pr;
				if ((empty($minPrice) || $minPrice > $pr) && $pr > 0) {
					$minPrice = $pr;
				}
			}
			if ($minPrice !== false) {
				$arItem['MIN_PRICE']['VALUE'] = $minPrice;
				$arItem['MIN_PRICE']['PRINT_VALUE'] = Tools::FormatPrice($minPrice);
				$arItem['MIN_PRICE']['DISCOUNT_VALUE'] = $minPrice;
				$arItem['MIN_PRICE']['PRINT_DISCOUNT_VALUE'] = Tools::FormatPrice($minPrice);
				$arItem['CATALOG_MEASURE_RATIO'] = 1;
				$arItem['CAN_BUY'] = true;
			}
			$arItem['CHECK_QUANTITY'] = $arResult['CHECK_QUANTITY'];
			$arItem['CATALOG_QUANTITY'] = CMarketCatalogProduct::GetQuantity($arItem['ID'], $arItem['IBLOCK_ID']);

			if ($arItem['CHECK_QUANTITY'] && $arItem['CATALOG_QUANTITY'] <= 0) {
				$arItem['CAN_BUY'] = false;
			}
			$arItem['CATALOG_TYPE'] = 1; //simple product
		}
		//end Prices for MARKET
		$arNewItemsList[$key] = $arItem;
	}
	$arNewItemsList[$key]['LAST_ELEMENT'] = 'Y';
	$arResult['ITEMS'] = $arNewItemsList;
}

$bFilterByProp = (!empty($arParams['PROPERTY_FILTER_CODE']));
if ($bFilterByProp) {
	$arrFilter = array('IBLOCK_ID' => $arParams['IBLOCK_ID']);
	/** @noinspection PhpDynamicAsStaticMethodCallInspection */
	if(!empty($arParams['PROPERTY_FILTER_VALUE']))
		$arrFilter['PROPERTY_' . $arParams['PROPERTY_FILTER_CODE']] = $arParams['PROPERTY_FILTER_VALUE'];
	else
		$arrFilter['!PROPERTY_' . $arParams['PROPERTY_FILTER_CODE']] = false;
	
	$cnt = CIBlockElement::GetList(array(),
		$arrFilter,
		array()
	);
	$arResult['CNT'] = $cnt;
}
if (isset($arResult['SECTIONS']) && count($arResult['SECTIONS']) > 0) {
	/** @noinspection PhpDynamicAsStaticMethodCallInspection */
	$dbSections = CIBlockSection::GetList(array(),
		array(
			'IBLOCK_ID' => $arParams['IBLOCK_ID'],
			'ID' => array_keys($arResult['SECTIONS']),
		),
		false,
		array('NAME', 'ID', 'UF_SECTION_ICON')
	);
	while ($arSection = $dbSections->GetNext()) {
		/** @noinspection PhpDynamicAsStaticMethodCallInspection */
		$dbIcon = CUserFieldEnum::GetList(array(),
			array(
				'ID' => $arSection['UF_SECTION_ICON'],
			)
		);
		$arIcon = $dbIcon->GetNext();
		$arResult["ITEMS_BY_SECTION"][$arSection['ID']]['NAME'] = $arSection['NAME'];
		$arResult["ITEMS_BY_SECTION"][$arSection['ID']]['ICON'] = $arIcon['XML_ID'];
		foreach ($arResult['SECTIONS'][$arSection['ID']] as $itemListId => $itemId) {
			$arResult["ITEMS_BY_SECTION"][$arSection['ID']]['ITEMS'][] = $arResult['ITEMS'][$itemListId];
		}
	}
}
if(CRZShinmarket::isCatchBuy() && $arParams['CATCH_BUY_BLOCK']){
	$arParams['CATCHBUY'] = CRZShinmarket::getCatchBuyList();
}

// $arResult['DEFAULT_QUANT'] = 1; // !!!
// foreach ($arParams as $key => $value) {
// 	echo "<pre>";
// 	echo $key;
// 	var_dump($value);
// 	echo "</pre>";
// }


?>