<?
$MESS["RZ_OSHIBKA"] = "Ошибка";
$MESS["RZ_OJ__KAZHETSYA_CHTO_TO_POSHLO_NE_TAK_"] = "Ой, кажется что-то пошло не так.";
$MESS["RZ_STRANITCA__KOTORUYU_VI_ISHETE__UDALENA_ILI_PROSTO_NE_SUSHESTVUET_"] = "Страница, которую вы ищете, удалена или просто не существует.";
$MESS["RZ_NO_MI_UVERENI__CHTO_VI_SMOZHETE_NAJTI_TO__CHTO_VAM_NUZHNO_"] = "Но мы уверены, что вы сможете найти то, что вам нужно.";
$MESS["RZ_PEREJTI_NA"] = "Перейти на";
$MESS["RZ_KARTU_SAJTA"] = "карту сайта";
