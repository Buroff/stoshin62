<?
if (!CModule::IncludeModule('iblock')) die();
$arIBlockTypes = CIBlockParameters::GetIBlockTypes();
$arIBlock1 = array();
$arIBlock2 = array();
$arIBlockCalc = array();
$arSections1 = array();
$arSections2 = array();
$arSectionsCalc = array();
$arPrice = array();
$arPropsId = array();
if(CModule::IncludeModule('catalog')) {
/** @noinspection PhpDynamicAsStaticMethodCallInspection */
$rsPrice = CCatalogGroup::GetList($v1 = 'sort', $v2 = 'asc');
while ($arr = $rsPrice->Fetch()) $arPrice[$arr['NAME']] = '[' . $arr['NAME'] . '] ' . $arr['NAME_LANG'];
} elseif
(CModule::IncludeModule('yenisite.market')) {
	/** @noinspection PhpDynamicAsStaticMethodCallInspection */
	$rsPrice = CMarketPrice::GetList($v1 = 'id', $v2 = 'asc');
	while ($arr = $rsPrice->Fetch()) {
		$arPrice[$arr['code']] = '[' . $arr['code'] . '] ' . $arr['name'];
	}
}
global $arComponentParameters;
$arComponentParameters['GROUPS']['F1'] = array('NAME' => GetMessage('RZ_TAB_NAME', array('#ID#' => '1')));
$arComponentParameters['GROUPS']['F2'] = array('NAME' => GetMessage('RZ_TAB_NAME', array('#ID#' => '2')));
$arComponentParameters['GROUPS']['F3'] = array('NAME' => GetMessage('RZ_TAB_NAME', array('#ID#' => '3')));

$arIcons = array(
	'icon_tyrebold' => GetMessage('RZ_ICON_TYREBOLD'),
	'icon_disk3' => GetMessage('RZ_ICON_DISK3'),
	'icon_car48' => GetMessage('RZ_ICON_CAR48'),
	'icon_oil2' => GetMessage('RZ_ICON_OIL2'),
	'icon_wheel4' => GetMessage('RZ_ICON_WHEEL4'),
	'icon_jeep' => GetMessage('RZ_ICON_JEEP'),
	'icon_car' => GetMessage('RZ_ICON_CAR'),
);
$arTemplateParameters = array(
	'F1_IBLOCK_TYPE' => array(
		'PARENT' => 'BASE',
		'NAME' => GetMessage('RZ_F1_IBLOCK_TYPE'),
		'TYPE' => 'LIST',
		'MULTIPLE' => 'N',
		'ADDITIONAL_VALUES' => 'N',
		'REFRESH' => 'Y',
		'DEFAULT' => '-',
		'VALUES' => $arIBlockTypes
	),
	'F1_IBLOCK_ID' => array(
		'PARENT' => 'BASE',
		'NAME' => GetMessage('RZ_F1_IBLOCK_ID'),
		'TYPE' => 'LIST',
		'MULTIPLE' => 'N',
		'ADDITIONAL_VALUES' => 'N',
		'REFRESH' => 'Y',
		'DEFAULT' => '-',
		'VALUES' => $arIBlock1
	),
	'F1_IBLOCK_SECTION_ID' => array(
		'PARENT' => 'BASE',
		'NAME' => GetMessage('RZ_F1_IBLOCK_SECTION_ID'),
		'TYPE' => 'LIST',
		'MULTIPLE' => 'N',
		'ADDITIONAL_VALUES' => 'Y',
		'DEFAULT' => '-',
		'VALUES' => $arSections1
	),
	'F1_FILTER_NAME' => array(
		'PARENT' => 'BASE',
		'NAME' => GetMessage('RZ_F1_FILTER_NAME'),
		'TYPE' => 'STRING',
		'DEFAULT' => 'arrFilter',
	),
	'F1_PRICE_CODE' => array(
		'PARENT' => 'BASE',
		'NAME' => GetMessage('RZ_F1_PRICE_CODE'),
		'TYPE' => 'LIST',
		'MULTIPLE' => 'Y',
		'VALUES' => $arPrice,
	),
	'F1_TITLE' => array(
		'PARENT' => 'BASE',
		'NAME' => GetMessage('RZ_F1_TITLE'),
		'TYPE' => 'STRING',
		'DEFAULT' => 'title',
	),
	'F1_ICON_CLASS' => array(
		'PARENT' => 'BASE',
		'NAME' => GetMessage('RZ_F1_ICON_CLASS'),
		'TYPE' => 'LIST',
		'ADDITIONAL_VALUES' => 'Y',
		'DEFAULT' => key($arIcons),
		'VALUES' => $arIcons
	),
	'F1_ADDITIONAL_URL' => array(
		'PARENT' => 'BASE',
		'NAME' => GetMessage('RZ_ADDITIONAL_URL'),
		'TYPE' => 'STRING',
		'DEFAULT' => '',
	),
	'F1_ADDITIONAL_URL_TEXT' => array(
		'PARENT' => 'BASE',
		'NAME' => GetMessage('RZ_ADDITIONAL_URL_TEXT'),
		'TYPE' => 'STRING',
		'DEFAULT' => '',
	),

	'F2_IBLOCK_TYPE' => array(
		'PARENT' => 'BASE',
		'NAME' => GetMessage('RZ_F2_IBLOCK_TYPE'),
		'TYPE' => 'LIST',
		'MULTIPLE' => 'N',
		'ADDITIONAL_VALUES' => 'N',
		'REFRESH' => 'Y',
		'DEFAULT' => '-',
		'VALUES' => $arIBlockTypes
	),
	'F2_IBLOCK_ID' => array(
		'PARENT' => 'BASE',
		'NAME' => GetMessage('RZ_F2_IBLOCK_ID'),
		'TYPE' => 'LIST',
		'MULTIPLE' => 'N',
		'ADDITIONAL_VALUES' => 'N',
		'REFRESH' => 'Y',
		'DEFAULT' => '-',
		'VALUES' => $arIBlock2
	),
	'F2_IBLOCK_SECTION_ID' => array(
		'PARENT' => 'BASE',
		'NAME' => GetMessage('RZ_F2_IBLOCK_SECTION_ID'),
		'TYPE' => 'LIST',
		'MULTIPLE' => 'N',
		'ADDITIONAL_VALUES' => 'Y',
		'DEFAULT' => '-',
		'VALUES' => $arSections2
	),
	'F2_FILTER_NAME' => array(
		'PARENT' => 'BASE',
		'NAME' => GetMessage('RZ_F2_FILTER_NAME'),
		'TYPE' => 'STRING',
		'DEFAULT' => 'arrFilter',
	),
	'F2_PRICE_CODE' => array(
		'PARENT' => 'BASE',
		'NAME' => GetMessage('RZ_F2_PRICE_CODE'),
		'TYPE' => 'LIST',
		'MULTIPLE' => 'Y',
		'VALUES' => $arPrice,
	),
	'F2_TITLE' => array(
		'PARENT' => 'BASE',
		'NAME' => GetMessage('RZ_F2_TITLE'),
		'TYPE' => 'STRING',
		'DEFAULT' => 'title',
	),
	'F2_ICON_CLASS' => array(
		'PARENT' => 'BASE',
		'NAME' => GetMessage('RZ_F2_ICON_CLASS'),
		'TYPE' => 'LIST',
		'ADDITIONAL_VALUES' => 'Y',
		'DEFAULT' => key($arIcons),
		'VALUES' => $arIcons
	),
	'F2_ADDITIONAL_URL' => array(
		'PARENT' => 'BASE',
		'NAME' => GetMessage('RZ_ADDITIONAL_URL'),
		'TYPE' => 'STRING',
		'DEFAULT' => '',
	),
	'F2_ADDITIONAL_URL_TEXT' => array(
		'PARENT' => 'BASE',
		'NAME' => GetMessage('RZ_ADDITIONAL_URL_TEXT'),
		'TYPE' => 'STRING',
		'DEFAULT' => '',
	),
	'CALC_IBLOCK_TYPE' => array(
		'PARENT' => 'BASE',
		'NAME' => GetMessage('RZ_CALC_IBLOCK_TYPE'),
		'TYPE' => 'LIST',
		'REFRESH' => 'Y',
		'VALUES' => $arIBlockTypes
	),
	'CALC_IBLOCK_ID' => array(
		'PARENT' => 'BASE',
		'NAME' => GetMessage('RZ_CALC_IBLOCK_ID'),
		'TYPE' => 'LIST',
		'REFRESH' => 'Y',
		'VALUES' => $arIBlockCalc
	),
	'CALC_SECTION_TYRES_ID' => array(
		'PARENT' => 'BASE',
		'NAME' => GetMessage('RZ_CALC_SECTION_TYRES_ID'),
		'TYPE' => 'LIST',
		'MULTIPLE' => 'N',
		'DEFAULT' => '-',
		'VALUES' => $arSectionsCalc
	),
	'CALC_SECTION_DISKS_ID' => array(
		'PARENT' => 'BASE',
		'NAME' => GetMessage('RZ_CALC_SECTION_DISKS_ID'),
		'TYPE' => 'LIST',
		'MULTIPLE' => 'N',
		'DEFAULT' => '-',
		'VALUES' => $arSectionsCalc
	),
	'CALC_PROP_WIDTH' => array(
		'PARENT' => 'BASE',
		'NAME' => GetMessage('RZ_CALC_PROP_WIDTH'),
		'TYPE' => 'LIST',
		'MULTIPLE' => 'N',
		'DEFAULT' => '-',
		'VALUES' => $arPropsId
	),
	'CALC_PROP_HEIGHT' => array(
		'PARENT' => 'BASE',
		'NAME' => GetMessage('RZ_CALC_PROP_HEIGHT'),
		'TYPE' => 'LIST',
		'MULTIPLE' => 'N',
		'DEFAULT' => '-',
		'VALUES' => $arPropsId
	),
	'CALC_PROP_DIAMTER' => array(
		'PARENT' => 'BASE',
		'NAME' => GetMessage('RZ_CALC_PROP_DIAMTER'),
		'TYPE' => 'LIST',
		'MULTIPLE' => 'N',
		'DEFAULT' => '-',
		'VALUES' => $arPropsId
	),
	'CALC_FILTER_NAME' => array(
		'PARENT' => 'BASE',
		'NAME' => GetMessage('RZ_CALC_FILTER_NAME'),
		'TYPE' => 'STRING',
		'DEFAULT' => 'arrFilter',
	)
);

if (strlen($arCurrentValues['F1_IBLOCK_TYPE']) > 0) {
	/** @noinspection PhpDynamicAsStaticMethodCallInspection */
	$rsIBlock = CIBlock::GetList(array('sort' => 'asc'), array('TYPE' => $arCurrentValues['F1_IBLOCK_TYPE'], 'ACTIVE' => 'Y'));
	while ($arr = $rsIBlock->Fetch()) {
		$arIBlock1[$arr['ID']] = '[' . $arr['ID'] . '] ' . $arr['NAME'];
	}
	$arTemplateParameters['F1_IBLOCK_ID']['VALUES'] = $arIBlock1;
}
if (strlen($arCurrentValues['F2_IBLOCK_TYPE']) > 0) {
	/** @noinspection PhpDynamicAsStaticMethodCallInspection */
	$rsIBlock = CIBlock::GetList(array('sort' => 'asc'), array('TYPE' => $arCurrentValues['F2_IBLOCK_TYPE'], 'ACTIVE' => 'Y'));
	while ($arr = $rsIBlock->Fetch()) {
		$arIBlock2[$arr['ID']] = '[' . $arr['ID'] . '] ' . $arr['NAME'];
	}
	$arTemplateParameters['F2_IBLOCK_ID']['VALUES'] = $arIBlock2;
}
if (strlen($arCurrentValues['CALC_IBLOCK_TYPE']) > 0) {
	/** @noinspection PhpDynamicAsStaticMethodCallInspection */
	$rsIBlock = CIBlock::GetList(array('sort' => 'asc'), array('TYPE' => $arCurrentValues['CALC_IBLOCK_TYPE'], 'ACTIVE' => 'Y'));
	while ($arr = $rsIBlock->Fetch()) {
		$arIBlockCalc[$arr['ID']] = '[' . $arr['ID'] . '] ' . $arr['NAME'];
	}
	$arTemplateParameters['CALC_IBLOCK_ID']['VALUES'] = $arIBlockCalc;
}

if (intval($arCurrentValues['F1_IBLOCK_ID']) > 0) {
	$dbSections = CIBlockSection::GetTreeList(array('IBLOCK_ID' => $arCurrentValues['F1_IBLOCK_ID'], 'ACTIVE' => 'Y', 'GLOBAL_ACTIVE' => 'Y'));
	while ($arSection = $dbSections->GetNext()) {
		if ($arSection['DEPTH_LEVEL'] > 1) {
			$arSections1[$arSection['ID']] = str_repeat('.', $arSection['DEPTH_LEVEL']) . $arSection['NAME'];
		} else {
			$arSections1[$arSection['ID']] = $arSection['NAME'];
		}
	}
	$arTemplateParameters['F1_IBLOCK_SECTION_ID']['VALUES'] = $arSections1;
}
if (intval($arCurrentValues['F2_IBLOCK_ID']) > 0) {
	$dbSections = CIBlockSection::GetTreeList(array('IBLOCK_ID' => $arCurrentValues['F2_IBLOCK_ID'], 'ACTIVE' => 'Y', 'GLOBAL_ACTIVE' => 'Y'));
	while ($arSection = $dbSections->GetNext()) {
		if ($arSection['DEPTH_LEVEL'] > 1) {
			$arSections2[$arSection['ID']] = str_repeat('.', $arSection['DEPTH_LEVEL']) . $arSection['NAME'];
		} else {
			$arSections2[$arSection['ID']] = $arSection['NAME'];
		}
	}
	$arTemplateParameters['F2_IBLOCK_SECTION_ID']['VALUES'] = $arSections2;
}
if (intval($arCurrentValues['CALC_IBLOCK_ID']) > 0) {
	$dbSections = CIBlockSection::GetTreeList(array('IBLOCK_ID' => $arCurrentValues['CALC_IBLOCK_ID'], 'ACTIVE' => 'Y', 'GLOBAL_ACTIVE' => 'Y'));
	while ($arSection = $dbSections->GetNext()) {
		if ($arSection['DEPTH_LEVEL'] > 1) {
			$arSectionsCalc[$arSection['ID']] = str_repeat('.', $arSection['DEPTH_LEVEL']) . $arSection['NAME'];
		} else {
			$arSectionsCalc[$arSection['ID']] = $arSection['NAME'];
		}
	}
	$arTemplateParameters['CALC_SECTION_TYRES_ID']['VALUES'] = $arSectionsCalc;
	$arTemplateParameters['CALC_SECTION_DISKS_ID']['VALUES'] = $arSectionsCalc;

	$dbProps = CIBlockProperty::GetList(array('SORT', 'ASC'), array(
		'ACTIVE' => 'Y',
		'IBLOCK_ID' => $arCurrentValues['CALC_IBLOCK_ID'],
	));
	$propTypes = array('L');
	while ($arProp = $dbProps->GetNext()) {
		if (in_array($arProp['PROPERTY_TYPE'], $propTypes)) {
			$arPropsId[$arProp['ID']] = '[' . $arProp['ID'] . '] ' . $arProp['NAME'];
		}
	}
	$arTemplateParameters['CALC_PROP_WIDTH']['VALUES'] = $arPropsId;
	$arTemplateParameters['CALC_PROP_HEIGHT']['VALUES'] = $arPropsId;
	$arTemplateParameters['CALC_PROP_DIAMTER']['VALUES'] = $arPropsId;
}