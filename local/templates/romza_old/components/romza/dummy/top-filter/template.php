<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
if (method_exists($this, 'setFrameMode')) $this->setFrameMode(true);
$bShowCalculator = CRZShinmarketSettings::getEdition() == 'PRO' || CRZShinmarketSettings::getEdition() == 'LITE' || CRZShinmarketSettings::getModuleId() == 'yenisite.shinmarket'?>
<div class="seach_form_block">
	<div class="container">
		<ul class="seach_form__tabs_list nav nav-tabs" role="tablist">
			<li class="seach_form__tabs_item tyres active">
				<a href="#tires" role="tab" data-toggle="tab">
					<span class="icon <?= $arParams['F1_ICON_CLASS'] ?>"></span>
					<span class="seach_form__tabs_item__inner"><?= $arParams['F1_TITLE'] ?></span>
				</a>
			</li>
            <?if (!empty($arParams['F2_TITLE'])):?>
                <li class="seach_form__tabs_item disks">
                    <a href="#disks" role="tab" data-toggle="tab">
                        <span class="icon <?= $arParams['F2_ICON_CLASS'] ?>"></span>
                        <span class="seach_form__tabs_item__inner"><?= $arParams['F2_TITLE'] ?></span>
                    </a>
                </li>
            <?endif?>
            <?if ($bShowCalculator):?>
                <li class="seach_form__tabs_item calc">
                    <a href="#calculator" role="tab" data-toggle="tab">
                        <span class="icon icon_disk3"></span>
                        <span class="seach_form__tabs_item__inner"><?=GetMessage("RZ_SHINNIJ_KAL_KULYATOR")?></span>
                    </a>
                </li>
            <?endif?>
		</ul>

		<div class="seach_form__tabs_content tab-content">
			<div class="tab-pane active" id="tires">
				<?$APPLICATION->IncludeComponent(
					"bitrix:catalog.smart.filter",
					"visual_horizontal",
					Array(
						"IBLOCK_TYPE" => $arParams['F1_IBLOCK_TYPE'], //
						"IBLOCK_ID" => $arParams['F1_IBLOCK_ID'], //
						"SECTION_ID" => $arParams['F1_IBLOCK_SECTION_ID'], //
						"FILTER_NAME" => $arParams['F1_FILTER_NAME'], //
						"PRICE_CODE" => $arParams['F1_PRICE_CODE'], //
						"TAB_TITLE" => $arParams['F1_TITLE'],
						"ICON_CLASS" => $arParams['F1_ICON_CLASS'],
						"CACHE_TYPE" => "A", //
						"CACHE_TIME" => "36000", //
						"CACHE_GROUPS" => "Y", //
						"SAVE_IN_SESSION" => "N",
						"XML_EXPORT" => "N",
						"SECTION_TITLE" => "NAME",
						"SECTION_DESCRIPTION" => "DESCRIPTION",
						'HIDE_NOT_AVAILABLE' => "N", //
						'ADDITIONAL_URL' => $arParams['F1_ADDITIONAL_URL'],
						'ADDITIONAL_URL_TEXT' => $arParams['F1_ADDITIONAL_URL_TEXT'],
					),
					false,
					array('HIDE_ICONS' => 'N')
				);?>
			</div>
            <?if (!empty($arParams['F2_TITLE'])):?>
                <div class="tab-pane" id="disks">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:catalog.smart.filter",
                        "visual_horizontal",
                        Array(
                            "IBLOCK_TYPE" => $arParams['F2_IBLOCK_TYPE'], //
                            "IBLOCK_ID" => $arParams['F2_IBLOCK_ID'], //
                            "SECTION_ID" => $arParams['F2_IBLOCK_SECTION_ID'], //
                            "FILTER_NAME" => $arParams['F2_FILTER_NAME'], //
                            "PRICE_CODE" => $arParams['F2_PRICE_CODE'], //
                            "TAB_TITLE" => $arParams['F2_TITLE'],
                            "ICON_CLASS" => $arParams['F2_ICON_CLASS'],
                            "CACHE_TYPE" => "A", //
                            "CACHE_TIME" => "36000", //
                            "CACHE_GROUPS" => "Y", //
                            "SAVE_IN_SESSION" => "N",
                            "XML_EXPORT" => "N",
                            "SECTION_TITLE" => "NAME",
                            "SECTION_DESCRIPTION" => "DESCRIPTION",
                            'HIDE_NOT_AVAILABLE' => "N", //
                            'ADDITIONAL_URL' => $arParams['F2_ADDITIONAL_URL'],
                            'ADDITIONAL_URL_TEXT' => $arParams['F2_ADDITIONAL_URL_TEXT'],
                        ),
                        false,
                        array('HIDE_ICONS' => 'N')
                    );?>
                </div>
            <?endif?>
            <?if ($bShowCalculator):?>
                <div class="tab-pane" id="calculator">
                    <?$APPLICATION->IncludeComponent('romza:dummy', 'tyres_calculator', array(
                            'IBLOCK_TYPE' => $arParams['CALC_IBLOCK_TYPE'],
                            'IBLOCK_ID' => $arParams['CALC_IBLOCK_ID'],
                            'SECTION_TYRES_ID' => $arParams['CALC_SECTION_TYRES_ID'],
                            'SECTION_DISKS_ID' => $arParams['CALC_SECTION_DISKS_ID'],
                            'PROP_WIDTH' => $arParams['CALC_PROP_WIDTH'],
                            'PROP_HEIGHT' => $arParams['CALC_PROP_HEIGHT'],
                            'PROP_DIAMTER' => $arParams['CALC_PROP_DIAMTER'],
                            'FILTER_NAME' => $arParams['CALC_FILTER_NAME']
                        ),
                        $component,
                        array('HIDE_ICONS' => 'Y')
                    )?>
                </div>
            <?endif?>
		</div>
	</div>
</div>