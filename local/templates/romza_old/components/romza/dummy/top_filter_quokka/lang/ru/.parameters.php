<?
$MESS['RZ_TAB_NAME'] = 'Закладка #ID#';

$MESS['RZ_ICON_TYREBOLD'] = 'Шина';
$MESS['RZ_ICON_DISK3'] = 'Диск';
$MESS['RZ_ICON_CAR48'] = 'Аккумулятор';
$MESS['RZ_ICON_OIL2'] = 'Масло';
$MESS['RZ_ICON_WHEEL4'] = 'Шестерня';
$MESS['RZ_ICON_JEEP'] = 'Джип вид сбоку';
$MESS['RZ_ICON_CAR'] = 'Машина вид спереди';

$MESS['RZ_F1_IBLOCK_TYPE'] = 'Тип Инфоблока';
$MESS['RZ_F1_IBLOCK_ID'] = 'ID Инфоблока';
$MESS['RZ_F1_IBLOCK_SECTION_ID'] = 'ID Секции';
$MESS['RZ_F1_FILTER_NAME'] = 'Имя переменной фильтра';
$MESS['RZ_F1_PRICE_CODE'] = 'Цены';
$MESS['RZ_F1_TITLE'] = 'Заголовок таба';
$MESS['RZ_F1_ICON_CLASS'] = 'Класс икноки таба';

$MESS['RZ_F2_IBLOCK_TYPE'] = 'Тип Инфоблока';
$MESS['RZ_F2_IBLOCK_ID'] = 'ID Инфоблока';
$MESS['RZ_F2_IBLOCK_SECTION_ID'] = 'ID Секции';
$MESS['RZ_F2_FILTER_NAME'] = 'Имя переменной фильтра';
$MESS['RZ_F2_PRICE_CODE'] = 'Цены';
$MESS['RZ_F2_TITLE'] = 'Заголовок таба';
$MESS['RZ_F2_ICON_CLASS'] = 'Класс икноки таба';

$MESS['RZ_CALC_IBLOCK_TYPE'] = 'Тип Инфоблока';
$MESS['RZ_CALC_IBLOCK_ID'] = 'ID инфоблока';
$MESS['RZ_CALC_SECTION_TYRES_ID'] = 'ID секции для шин';
$MESS['RZ_CALC_SECTION_DISKS_ID'] = 'ID секции для дисков';
$MESS['RZ_CALC_PROP_WIDTH'] = 'Свойство ширины профиля';
$MESS['RZ_CALC_PROP_HEIGHT'] = 'Свойство высоты профиля';
$MESS['RZ_CALC_PROP_DIAMTER'] = 'Свойство диаметра';
$MESS['RZ_CALC_FILTER_NAME'] = 'Имя переменной фильтра';


$MESS['RZ_ADDITIONAL_URL'] = 'Доп. ссылка внизу фильтра';
$MESS['RZ_ADDITIONAL_URL_TEXT'] = 'Доп. ссылка - текст';

