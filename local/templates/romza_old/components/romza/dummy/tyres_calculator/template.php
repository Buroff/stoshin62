<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use \Yenisite\Shinmarket\ManagerCalc;
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
if (method_exists($this, 'setFrameMode')) $this->setFrameMode(true);
$bHasSpecTables = CRZShinmarketSettings::isTiersSolution() && ManagerCalc::tablesExist()?>
<form class="seach_form calculator" id="search_formCalculator" action="/">
	<div class="collapsed_area">
        <?
        // if ($bHasSpecTables){
        //     include "selects_from_db_tx.php";
        // }
        ?>
		<div class="seach_form__inner seach_form__inner_values">
			<div class="column">
				<div class="field_head">
					<div class="field_head__main"><?=GetMessage("RZ_ISHODNIJ_TIPORAZMER")?></div>
				</div>
				<div>
                    <? $bOldNames = true;
                    include "selects_of_tiers.php"?>
				</div>

			</div>
			<div class="column">
				<div class="field_head">
					<div class="field_head__main"><?=GetMessage("RZ_NOVIJ_TIPORAZMER")?></div>
				</div>
				<div>
                    <?$bOldNames = false;
                    include "selects_of_tiers.php"?>
				</div>
			</div>
			<div class="column submit_button_wrapper">
				<a class="btn btn-default submit_button" id="sectionTyres" href="<?=$arResult['SECTION_TYRES_URL']?>" data-href="<?=$arResult['SECTION_TYRES_URL']?>"><?=GetMessage("RZ_PODOBRAT__SHINI")?></a>
				<button class="form_reset_button" type="reset"><?=GetMessage("RZ_SBROSIT__VSE_ZNACHENIYA")?></button>
                <p class="hidden backend-cnt-items-calc found-product"><?=GetMessage('RZ_FINDED')?>: <span class="text"></span></p>
			</div>
		</div>
		<div class="seach_form__inner seach_form__inner_seach_form__inner_summary">
			<div class="summary_table_wrapper">
				<table class="table summary_table">
					<thead>
					<tr>
						<th>&nbsp;</th>
						<th class="width"><?=GetMessage("RZ_SHIRINA_SHINI__MM")?></th>
						<th class="height"><?=GetMessage("RZ_VISOTA_PROFILYA__MM")?></th>
						<th class="diameter"><?=GetMessage("RZ_VNUTRENNIJ_DIAMETR__MM")?></th>
						<th class="diametr_out"><?=GetMessage("RZ_VNESHNIJ_DIAMETR__MM")?></th>
					</tr>
					</thead>
					<tbody>
					<tr>
						<th><?=GetMessage("RZ_ISHODNIJ")?></th>
						<td id="old_W_mm">175</td>
						<td id="old_H_mm">123</td>
						<td id="old_R_mm">330</td>
						<td id="old_D_mm">575</td>
					</tr>
					<tr>
						<th><?=GetMessage("RZ_NOVIJ")?></th>
						<td id="new_W_mm">185</td>
						<td id="new_H_mm">139</td>
						<td id="new_R_mm">356</td>
						<td id="new_D_mm">633</td>
					</tr>
					<tr>
						<th><?=GetMessage("RZ_RAZNITCA")?></th>
						<td><b id="res_W_mm">10</b></td>
						<td><b id="res_H_mm">16</b></td>
						<td><b id="res_R_mm">26</b></td>
						<td><b id="res_D_mm">58</b></td>
					</tr>
					</tbody>
				</table>
			</div>
			<div class="summary_changes_wrapper">
				<div class="summary_changes">
					<span><?=GetMessage("RZ_DOROZHNIJ_PROSVET_IZMENITSYA_NA")?></span>
					<span class="summary_changes__value" id="res_C_mm">29</span>
					<span><?=GetMessage("RZ_MILIMETROV")?></span>
				</div>
			</div>
		</div>
		<div class="seach_form__inner seach_form__inner_speed">
			<div class="speeds_wrapper">
				<?=GetMessage("RZ_POKAZANIYA_SPIDOMETRA")?>:
				<input id="speed" type="number" class="form-control form-control-small" title="">
				<?=GetMessage("RZ_KM")?>/<?=GetMessage("RZ_CH")?>
			</div>
			<div class="seach_form__inner_speed__total">
				<div><?=GetMessage("RZ_REAL_NAYA_SKOROST_")?>: <span class="extra" id="res_V">159</span> <?=GetMessage("RZ_KM")?>/<?=GetMessage("RZ_CH")?></div>
				<div><?=GetMessage("RZ_POGRESHNOST_")?>: <span class="big" id="diff_V">9</span> <?=GetMessage("RZ_KM")?>/<?=GetMessage("RZ_CH")?></div>
			</div>
			<div class="range_slider_wrapper">
				<span class="slider__range_point from">0</span>

				<div class="js-speed-slider range_slider single"></div>
				<span class="slider__range_point to">300 <?=GetMessage("RZ_KM")?>/<?=GetMessage("RZ_CH")?></span>
			</div>
		</div>
	</div>
	<div class="seach_form__submit">
		<div class="collapse_btn_wrapper">
							<span class="btn btn-default btn-collapse">
								<span class="hide-filter">
									<span class="btn-inner"><?=GetMessage("RZ_SKRIT__FIL_TR")?></span>
									<span class="flaticon-arrow215"></span>
								</span>
								<span class="show-filter">
									<span class="btn-inner"><?=GetMessage("RZ_POKAZAT__FIL_TR")?></span>
									<span class="flaticon-arrow222"></span>
								</span>
							</span>
		</div>
		<div class="seach_form__recomendation_note"><?=GetMessage("RZ_SHIN_NOTE")?></div>
	</div>
</form>

<script type="text/javascript">
	calcObj = <?=CUtil::PhpToJSObject($arResult['PROPS'])?>;
</script>