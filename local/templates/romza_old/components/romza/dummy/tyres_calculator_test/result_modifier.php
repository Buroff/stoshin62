<?
$arParams['FILTER_NAME'] = trim($arParams['FILTER_NAME']);
$arParams['PROP_WIDTH'] = intval($arParams['PROP_WIDTH']);
$arParams['PROP_HEIGHT'] = intval($arParams['PROP_HEIGHT']);
$arParams['PROP_DIAMTER'] = intval($arParams['PROP_DIAMTER']);
$arParams['SECTION_DISKS_ID'] = intval($arParams['SECTION_DISKS_ID']);
$arParams['SECTION_TYRES_ID'] = intval($arParams['SECTION_TYRES_ID']);

$obCache = new CPHPCache();
if ($obCache->InitCache(36000, serialize($arParams), "romza/TyresCalc/" . $this->__name)) {
	$arResult = $obCache->GetVars();
} elseif ($obCache->StartDataCache()) {
	$obCache->CleanDir("romza/TyresCalc/" . $this->__name);
	$arResult = array();
	if ($arParams['SECTION_DISKS_ID'] > 0) {
		$dbSection = CIBlockSection::GetByID($arParams['SECTION_DISKS_ID']);
		$dbSection->SetUrlTemplates();
		$arSection = $dbSection->GetNext();

		$arResult['SECTION_DISKS_URL'] = $arSection['SECTION_PAGE_URL'];
	}
	if ($arParams['SECTION_TYRES_ID'] > 0) {
		$dbSection = CIBlockSection::GetByID($arParams['SECTION_TYRES_ID']);
		$dbSection->SetUrlTemplates();
		$arSection = $dbSection->GetNext();

		$arResult['SECTION_TYRES_URL'] = $arSection['SECTION_PAGE_URL'];
	}
	$dbProp = CIBlockProperty::GetPropertyEnum($arParams['PROP_WIDTH'], array('VALUE' => 'ASC'));
	while ($arProp = $dbProp->GetNext()) {
		$arResult['PROPS']['W'][$arProp['VALUE']] = htmlspecialcharsbx($arParams['FILTER_NAME'] . '_' . $arProp['PROPERTY_ID'] . '_' . abs(crc32($arProp['ID'])) . '=Y');
	}
	$dbProp = CIBlockProperty::GetPropertyEnum($arParams['PROP_HEIGHT'], array('VALUE' => 'ASC'));
	while ($arProp = $dbProp->GetNext()) {
		$arResult['PROPS']['H'][$arProp['VALUE']] = htmlspecialcharsbx($arParams['FILTER_NAME'] . '_' . $arProp['PROPERTY_ID'] . '_' . abs(crc32($arProp['ID'])) . '=Y');
	}
	$dbProp = CIBlockProperty::GetPropertyEnum($arParams['PROP_DIAMTER'], array('VALUE' => 'ASC'));
	while ($arProp = $dbProp->GetNext()) {
		$arResult['PROPS']['R'][$arProp['VALUE']] = htmlspecialcharsbx($arParams['FILTER_NAME'] . '_' . $arProp['PROPERTY_ID'] . '_' . abs(crc32($arProp['ID'])) . '=Y');
	}
	$obCache->EndDataCache($arResult);
}