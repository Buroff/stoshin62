<? use Yenisite\Core\Resize;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
include $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/tools/include_module.php';
if (!empty($arResult['ITEMS'])) {
	foreach ($arResult["ITEMS"] as $k => &$arItem) {
		$arItem["CAN_BUY"] = "Y";
		$arItem['PRODUCT_ID'] = $arItem['ID'];
		$arItem['PRODUCT_PICTURE_SRC'] = Resize::GetResizedImg($arItem['FIELDS'], array('WIDTH' => 168, 'HEIGHT' => 170, 'SET_ID' => intval($arParams['RESIZER_PRODUCT'])));
		$arItem['SUM_NOT_FORMATED'] = $arItem['COUNT'] * ((intval($arItem['DISCOUNT_PRICE']) > 0) ? $arItem['DISCOUNT_PRICE'] : $arItem['MIN_PRICE']);
	}
	unset($arItem);
}