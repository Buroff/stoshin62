<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use \Yenisite\Core\Tools;
include_once $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/tools/include_module.php';
$isAjax = Tools::isAjax();
\Bitrix\Main\Localization\Loc::loadMessages($_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH . '/lang/'.LANGUAGE_ID.'/header.php');
$pathToRules = COption::GetOptionString(CRZShinmarketSettings::getModuleId(),'path_tu_rules_privacy',SITE_DIR.'personal/rules/personal_data.php');
$rand = $this->randString();
?>
<?if(!$isAjax):?>
<div class="ordering_block step_1" id="order_block_step_1" data-template="<?=$templateName?>"
	 data-arparams="<?=Tools::GetEncodedArParams($arParams)?>" data-url="<?=$APPLICATION->GetCurPage()?>">
<?endif?>
	<? if (is_array($arResult['ERROR'])):
		foreach ($arResult['ERROR'] as $key => $err):?>
			<? if($key !== 'BASKET') continue; ?>
			<div class="message message-error">
				<?= GetMessage('ERROR'), $err ?>
			</div>
		<? endforeach ?>
	<? endif ?>
	<? if (isset($arResult["ITEMS"])): ?>
		<form method="POST" id="basket_form" action="" class="basket_order_form modal-form">
			<div class="make_order" id="basket_form_container">
                <input type="hidden" name="privacy_policy" value="N"/>
				<input type="hidden" name="no_calculate" id="calculate" value="Y"/>
				<input type="hidden" name="order" id="order" value="<?= trim(GetMessage("ORDER")) ?>"/>
				<div id="basket_items_list">
					<table id="basket_items">
						<thead class="basket_order__head">
						<tr>
							<th class="title" colspan="2"><?= GetMessage('SALE_NAME') ?></th>
							<th>&nbsp;</th>
							<th class="price"><?= GetMessage('SALE_PRICE') ?></th>
							<th class="amount"><?= GetMessage('SALE_QUANTITY') ?></th>
							<th class="margin"></th>
						</tr>
						</thead>
						<tbody class="basket_order__main">
						<?
						foreach ($arResult["ITEMS"] as $k => $arItem):

							if ($arItem["CAN_BUY"] == "Y"):
								?>
								<tr>
									<td class="img">
										<?if (strlen($arItem['FIELDS']["DETAIL_PAGE_URL"]) > 0):?>
											<a href="<?= $arItem['FIELDS']["DETAIL_PAGE_URL"] ?>">
										<?endif?>
											<img src="<?= $arItem['PRODUCT_PICTURE_SRC'] ?>" alt="<?= $arItem['FIELDS']["NAME"] ?>">
										<?if (strlen($arItem['FIELDS']["DETAIL_PAGE_URL"]) > 0):?>
											</a>
										<?endif?>
									</td>
									<td class="title">
										<?if (strlen($arItem['FIELDS']["DETAIL_PAGE_URL"]) > 0):?>
											<a href="<?= $arItem['FIELDS']["DETAIL_PAGE_URL"] ?>">
										<?endif?>
											<?= $arItem['FIELDS']["NAME"] ?>
										<?if (strlen($arItem['FIELDS']["DETAIL_PAGE_URL"]) > 0):?>
											</a>
										<?endif?>
										<? if (count($arItem['PROPERTIES']) > 0): ?>
											<div class="help-block">
												<? foreach($arItem['PROPERTIES'] as $arProp):?>
													<?= $arProp['NAME'] , ' : ' , $arProp['VALUE'] ?><br/>
												<? endforeach ?>
											</div>
										<? endif ?>
										<input type="hidden" name="DELAY_<?= $arItem["ID"] ?>" id="DELAY_<?= $arItem["ID"] ?>" value="N"/>
										<input type="hidden" name="DELETE_<?= $arItem["ID"] ?>" id="DELETE_<?= $arItem["ID"] ?>" value="N"/>
									</td>
									<td>&nbsp;</td>
									<td class="price">
										<?= Tools::FormatPrice($arItem["MIN_PRICE"]) ?>
									</td>
									<td class="amount">
										<?
										$max = isset($arItem["AVAILABLE_QUANTITY"]) ? "max=\"".$arItem["AVAILABLE_QUANTITY"]."\"" : "";
										?>
										<input class="js-touchspin" type="text" value="<?= $arItem['COUNT'] ?>"
											   name="count[<?=$arItem["KEY"]?>]" title="">
									</td>
									<td class="delete">
										<a href="javascript:;" class="delete_link" data-id="<?=$arItem['KEY']?>">
											<span class="icon icon_recycling10"></span>
											<?=GetMessage("SALE_DELETE")?>
										</a>
									</td>
								</tr>
							<?endif?>
						<?endforeach?>
						</tbody>
					</table>
				</div>
				<div class="ordering_block step_3">
					<div class="ordering_block__inner">
						<div class="ordering_block__inner_head"><?= GetMessage('SOA_TEMPL_BUYER_INFO') ?></div>
						<div class="buyer_info" id="buyer_props">
							<div class="row">
								<?
								$bHasPropErr = false;
								foreach ($arResult['ERROR'] as $key => $err):?>
									<? if($key === 'BASKET') continue; ?>
									<div class="message message-error">
										<?= GetMessage('ERROR'), $err ; $bHasPropErr = true;?>
									</div>
								<? endforeach ?>
								<div class="col-sm-4">
									<?
									$reqFlag = false;
									foreach ($arResult["DISPLAY_PROPERTIES"] as $code => $arProp):?>
										<? if ($code == 'PAYMENT_E' || $code == 'DELIVERY_E') continue;?>
										<?
										$errClass = '';
										if(array_search($arProp['NAME'], $arResult['ERROR']) !== false) {
											$errClass = ' has-error';
										}
										?>
										<div class="form-group<?=$errClass?>">
											<label>
												<?= $arProp["NAME"] ?>
												<? if ($arProp['IS_REQUIRED'] == "Y"):$reqFlag = true; ?>
													<span class="bx_sof_req">*</span>
												<? endif ?>:
											</label>
											<?= str_replace(
												array(' />','></textarea>'),
												array(' class="form-control" /> ', ' class="form-control"></textarea>' ),
												$arProp["INPUT"]) ?>
										</div>
									<? endforeach ?>
									<? if ($reqFlag): ?>
										<span class="req_message"><span class="required">*</span> <?= GetMessage('YS_BASKET_REQUIRED'); ?></span>
									<? endif ?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<? if (array_key_exists('PAYMENT_E', $arResult['DISPLAY_PROPERTIES'])): ?>
				<div class="ordering_block step_3">
					<div class="ordering_block__inner">
						<div class="ordering_block__inner_head"><?= $arResult['DISPLAY_PROPERTIES']['PAYMENT_E']['NAME'] ?></div>
						<div class="buyer_info"><?
							$arr = explode("<br/>", $arResult['DISPLAY_PROPERTIES']['PAYMENT_E']['INPUT']);
							foreach ($arr as $k => &$ar):?><?
								if (empty($ar)) continue;?>
								<?= str_replace(
									array('<input', '/>'),
									array('<input id="payment' . $k . '"', '/><label for="payment' . $k . '">'),
									$ar) ?></label><?
							endforeach ?>
						</div>
					</div>
				</div>
				<? endif ?>
				<? if (array_key_exists('DELIVERY_E', $arResult['DISPLAY_PROPERTIES'])): ?>
					<div class="ordering_block step_3">
						<div class="ordering_block__inner">
							<div class="ordering_block__inner_head"><?= $arResult['DISPLAY_PROPERTIES']['DELIVERY_E']['NAME'] ?></div>
							<div class="buyer_info delivery_select"><?
								$arr = explode("<br/>", $arResult['DISPLAY_PROPERTIES']['DELIVERY_E']['INPUT']);
								foreach ($arr as $k => &$ar):?><?
									if (empty($ar)) continue;?>
									<?= str_replace(
										array('<input', '/>', '"rubl"'),
										array('<input class="radio-styled" id="delivery' . $k . '"', '/><label for="delivery' . $k . '">', '"b-rub"'),
										$ar) ?></label><?
								endforeach ?>
							</div>
						</div>
					</div>
				<? endif ?>
			</div><!--.make_order-->
			<div class="ordering_block step_3">
				<div class="ordering_block__inner">
                    <div class="checkbox col-md-4 aggremment">
                        <div class="form-group agreement-policy">
                            <input id="privacy_policy_<?=$rand?>" required type="checkbox" name="privacy_policy" value="Y" <?=$_REQUEST['privacy_policy'] == 'Y' ? ' checked' : ''?>>
                            <label for="privacy_policy_<?=$rand?>">
                                <?= GetMessage('RZ_ACCEPT') ?> <?=GetMessage('RZ_CONCLUSION',array("#URL#" => Tools::GetConstantUrl($pathToRules)))?>
                            </label>
                        </div>
                    </div>
					<div class="checkbox col-md-6">
						<input type="checkbox" id="agreement" data-title="<?= GetMessage('AGREEMENT3') ?>" class="checkbox-styled">
						<label title="<?= GetMessage('AGREEMENT3') ?>" for="agreement"><?= GetMessage('AGREEMENT1') ?></label> <a
							title="<?= GetMessage('AGREEMENT2') ?>" href="<?= SITE_DIR ?>about/delivery/"><?= GetMessage('AGREEMENT2') ?></a>
					</div>
					<div class="col-md-3 pull-right">
						<table class="total-info">
							<tr class="ys-delivery">
								<td><?= GetMessage("DELIVERY"); ?>:</td>
								<td><span class="price">0</span> <span class="b-rub"><?= GetMessage('RUB'); ?></span></td>
							</tr>
							<tr class="total-of-total">
								<td><?= GetMessage("ITOG"); ?>:</td>
								<td><span class="price"><?=$arResult["COMMON_PRICE"]?></span> <span class="b-rub"><?= GetMessage('RUB'); ?></span></td>
							</tr>
						</table>
						<br/><br/>
						<button type="submit" class="btn btn-primary"><?= GetMessage("ORDER"); ?></button>
					</div>
				</div>
			</div>
		</form>
	<? else: ?>
		<font class="errortext"><?= GetMessage("YENISITE_BASKET_EMPTY") ?></font>
	<? endif; ?>
	<? if ($bHasPropErr): ?>
	<script type="text/javascript">
		$(function ($) {
			$('html, body').scrollTop($('#buyer_props').offset().top);
		});
	</script>
	<? endif ?>
<?if(!$isAjax):?>
</div>
<?endif?>