<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
use \Yenisite\Core\Ajax;
use Bitrix\Main\Loader;

if (Loader::IncludeModule('yenisite.core')) {
    $arResult['CATALOG_PARAMS'] = Ajax::getParams('bitrix:catalog', 'main_catalog', '', SITE_ID);

    if (empty($arResult['CATALOG_PARAMS']))
        $arResult['CATALOG_PARAMS'] = Ajax::getParams('', '', SITE_DIR.'catalog/',SITE_ID);
}
