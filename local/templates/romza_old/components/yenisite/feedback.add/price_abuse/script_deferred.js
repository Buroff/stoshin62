(typeof(jQuery) != 'undefined')
&& (function ($) {
	$('body').on('submit', '#price_abuse_modalForm', function (e) {
		e.preventDefault();
		var $this = $(this),
			data = $this.find('form').serializeArray();
		data.push({name: "arParams", value: $this.data('arparams')});
		data.push({name: "template", value: $this.data('template').toString()});
		data.push({name: "add", value: 'ok'});
		$this.setAjaxLoading();
		$.ajax({
			type: "POST",
			url: rz.AJAX_DIR + "call_me_modalForm.php",
			data: data,
			success: function(msg) {
				$this.html(msg);
                $this.refreshForm();
				// SITE_TEMPLATE_PATH/js/script_after.js
				$this.reStyler();
				$this.stopAjaxLoading();
			}
		})
	});
})(jQuery);