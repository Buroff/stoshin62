<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<h2><?=GetMessage("RZ_OTZIVI_POKUPATELEJ")?></h2>
<br/><br/>
<div class="feedbacks__list">
	<? if (!empty($arResult['ITEMS'])): ?>
		<? foreach ($arResult['ITEMS'] as $arItem): ?>
			<div class="feedbacks__item">
				<div class="feedbacks__item__inner">
					<? if (!empty($arItem['FILE'])) : ?>
						<img class="feedbacks__item__userpic img-circle" src="<?=$arItem['FILE']['SRC']; ?>" alt="<?= $arItem['PROPERTY_NAME_VALUE'] ?>">
					<?endif?>
					<div class="feedbacks__item__info">
						<div class="feedbacks__item__username"><?= $arItem['PROPERTY_NAME_VALUE'] ?></div>
						<div class="feedbacks__item__date"><?= ConvertDateTime($arItem['DATE_CREATE'], "DD.MM.YYYY"); ?></div>
						<?if(!empty($arItem['PROPERTY_RAITING_VALUE'])):?>
							<div class="feedbacks__item__rating_wrapper">
								<span class="stars stars_<?=$arItem['PROPERTY_RAITING_VALUE']?>"></span>
							</div>
						<?endif?>
					</div>
				</div>
				<div class="feedbacks__item__text"><?= $arItem['PREVIEW_TEXT'] ?></div>
			</div>
		<? endforeach ?>
	<? else: ?>
		<? if (empty($arResult['SECTIONS'])): ?>
			<?= GetMessage("MESSAGES_EMPTY"); ?>
		<? elseif (!empty($arResult['CUR_SECTION_CODE']) && !empty($arResult['SECTIONS'])): ?>
			<?= GetMessage("MESSAGES_EMPTY"); ?>
		<? endif; ?>
	<? endif; ?>
</div>
<div class='pager-block'>
	<?= $arResult['NAV']; ?>
</div>