<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use \Yenisite\Core\Tools;
include_once $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/tools/include_module.php';
if (CModule::IncludeModule("statistic")) {

	$APPLICATION->SetAdditionalCSS("{$templateFolder}/css/jquery.jgrowl.css");
	Tools::addDeferredJS($templateFolder . "/js/jquery.textchange.min.js");
	Tools::addDeferredJS($templateFolder . "/js/jquery.jgrowl_minimized.js");
	Tools::addDeferredJS($templateFolder . "/js/eshop.js");
	//Tools::addDeferredJS($templateFolder . "/js/bitronic.js");

	?>
	<script>
		YS.GeoIP.OrderProps = {
			'PERSON_TYPE_1': {
				'locationID': <?=(!empty($arParams['P1_LOCATION_ID'])) ? $arParams['P1_LOCATION_ID'] : 5 ?>,
				'cityID': <?=(!empty($arParams['P1_CITY_ID'])) ? $arParams['P1_CITY_ID'] : 6 ?>
			},
			'PERSON_TYPE_2': {
				'locationID': <?=(!empty($arParams['P2_LOCATION_ID'])) ? $arParams['P2_LOCATION_ID'] : 18 ?>,
				'cityID': <?=(!empty($arParams['P2_CITY_ID'])) ? $arParams['P2_CITY_ID'] : 17 ?>
			}
		};
		YS.GeoIP.AutoConfirm = <?if($arParams['AUTOCONFIRM'] == "Y"):?>true<?else:?>false<?endif?>;
	</script><?

}
?>