<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?if(method_exists($this, 'setFrameMode')) $this->setFrameMode(true);?>
<?if(CModule::IncludeModule('statistic')):?>

<?if(function_exists('yenisite_GetCompositeLoader')){global $MESS;$MESS ['COMPOSITE_LOADING'] = yenisite_GetCompositeLoader();}?>
<?if(method_exists($this, 'createFrame')) $frame = $this->createFrame()->begin(GetMessage('COMPOSITE_LOADING')); ?>

<?if($arParams['ONLY_GEOIP'] != 'Y'):?>
	<div class="city_selection_wrapper">
		<span class="js-city-selection city-selection ys-loc-store" data-toggle="modal" data-target="#ys-geoipstore">
			<span><?=GetMessage("LOC_FROM")?></span>
			<span class="ys-city-name ys-geoip-store-city"><?=$arResult['CITY']?></span>
		</span>
	</div>
	<?$this->SetViewTarget('modal_locator');?>
	<div class="modal fade ys-popup" id="ys-geoipstore" tabindex="-1" role="dialog" aria-labelledby="city_modalLabel" aria-hidden="true">
		<div class="popup modal-dialog popup_city_selection">
			<span class="close flaticon-delete30" data-dismiss="modal"></span>
			<h2><?=GetMessage("RZ_VIBERITE_GOROD")?></h2>
			<?
			$arItemsRow = array_chunk($arResult['ITEMS'], 2);
			?>
			<? foreach ($arItemsRow as $arRow): ?>
				<div class="row">
					<? foreach ($arRow as $item): ?>
						<div class="col-sm-6 ys-geoipstore-container <? if ($arResult['ACTIVE_ITEM_ID'] == $item['ID']): ?><?= "ys-geoipstore-cont-active" ?><? endif ?>">
							<div class="ys-geoipstore-item">
								<a href="#" class="ys-geoipstore-itemlink" data-ys-item-id="<?= $item['ID'] ?>"
								   data-dismiss="modal"><?= $item['CITY_DEL_NAME'] ?></a>
								<? if ($arResult['ACTIVE_ITEM_ID'] == $item['ID']): ?><span class="sym" style="color: red;">&#245;</span><? endif ?>
								<? if (!empty($item['STORES'])): ?>
									<ul class="ys-geoipstore-stores">
										<? foreach ($item['STORES'] as $store): ?>
											<li><span><?= $store['TITLE'] ?></span></li>
										<? endforeach ?>
									</ul>
								<? endif ?>
							</div>
						</div>
					<? endforeach ?>
				</div>
			<? endforeach ?>
		</div>
	</div>
	<?$this->EndViewTarget();?>
<?else: // object name : id , for geoip?>
	<script>
		var ysGeoStoreActiveId = <?=$arResult['ACTIVE_ITEM_ID']?>;
		var ysGeoStoreList = {
		<?foreach($arResult['ITEMS'] as $item){
			echo "'{$item['CITY_DEL_NAME']}':{$item['ID']},";
			if($item['DEFAULT'] == 'Y'){
				$default_store_id = $item['ID'] ;
			}
		}?>
		};
		var ysGeoStoreDefault = <?=$default_store_id?>;
	</script>
<?endif;?>
<?if(method_exists($this, 'createFrame')) $frame->end();?>
<?endif;?>