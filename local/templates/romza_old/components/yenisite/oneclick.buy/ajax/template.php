<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
if (!CModule::IncludeModule('yenisite.oneclick')) {
	die(GetMessage('RZ_ERR_NO_YENISITE_ONECLICK_MODULE_INSTALLED'));
}
use CRZ\OneClick\Tools;
include_once $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/tools/include_module.php';
\Bitrix\Main\Localization\Loc::loadMessages($_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH . '/lang/'.LANGUAGE_ID.'/header.php');
$pathToRules = COption::GetOptionString(CRZShinmarketSettings::getModuleId(),'path_tu_rules_privacy',SITE_DIR.'personal/rules/personal_data.php');
$rand = $this->randString();
?>
<?
$isAjax = (Tools::isAjax() && $_REQUEST['IS_AJAX'] == 'Y');
?>
<? if (!$isAjax): ?>
	<?if (method_exists($this, 'setFrameMode')) $this->setFrameMode(true); ?>
	<?if(empty($arParams['BASKET_ONE_CLICK']) && !$arParams['BASKET_ONE_CLICK']):?>
		<!-- <div class="rz_oneclick-buy">
			<a href="javascript:;" class="do-order<?if($arParams['BUTTON_TYPE'] == 'BUTTON'):?> btn btn-primary<?endif?>" data-arparams="<?= Tools::GetEncodedArParams($arParams) ?>" data-name="<?= $arParams['NAME']?>"
			   data-template="<?=$templateName?>"><?=GetMessage("RZ_KUPIT_V_1_KLIK")?></a>
		</div> -->
	<?else:?>
		<!-- <div class="buy-one-click">
			<a data-basket="Y" data-template="<?=$templateName?>" data-arparams="<?= Tools::GetEncodedArParams($arParams) ?>" href="javascript:;" class="do-order"><?=GetMessage("RZ_KUPIT_V_1_KLIK")?></a>
		</div> -->
	<?endif?>
	<?global $bRzSingleOne;
	if (!is_bool($bRzSingleOne)) {
		$bRzSingleOne = false;
	}?>
	<? if (!$bRzSingleOne): ?>
		<script type="text/javascript">
			if(typeof rzSingleOne == 'undefined') {
				rzSingleOne = {
					'AJAX_URL': "<?= $this->__component->__path , '/component.php'?>",
					'URL' : "<?=$APPLICATION->GetCurPage(true)?>"
				};
			}
		</script>
		<? $bRzSingleOne = true; ?>
	<? endif ?>
<? endif ?>
<? if ($isAjax): ?>
	<? if (!empty($arResult['ERROR'])): ?>
		<br/>
		<div class="message message-error">
			<?
			foreach($arResult['ERROR'] as $err){
				if(is_array($err)) {
					echo $err['TEXT'], '<br>';
				} else {
					$err = trim(strip_tags($err));
					if($err{0} == ':') {
						$err{0} = '';
						$err = trim($err);
				}
					echo str_replace(':','',$err) ,'<br>';
				}
			}?>
		</div>
	<? endif; ?>
	<? if (isset($arResult['SUCCESS'])): ?>
		<br/>
		<div class="message message-success">
			<?= htmlspecialcharsback(htmlspecialcharsback(htmlspecialcharsback($arResult['SUCCESS'])))//double json decoding ?>
		</div>
	<? else: ?>
		<form class="modal-form">
            <input type="hidden" name="privacy_policy" value="N"/>
			<input type="hidden" name="MESSAGE_OK" value="<?= htmlspecialcharsbx($arParams['MESSAGE_OK']) ?>"/>
			<input type="hidden" name="RZ_BASKET" value="<?=htmlspecialcharsbx($_REQUEST['RZ_BASKET'])?>" />
			<input type="hidden" name="IS_AJAX" value="Y"/>
			<?= bitrix_sessid_post() ?>
			<? foreach ($arResult['HIDDEN_FIELDS'] as $arField) {
				echo $arField['HTML'], "\n";
			} ?>
			<input type="hidden" name="SITE_ID" value="<?= SITE_ID ?>"/>
			<input type="hidden" name="ELEMENT_ID" value="<?= $arParams['ELEMENT_ID'] ?>" title=""/>
			<?if (!empty($_REQUEST['NAME'])):?>
				<div class="form-group">
				<label><?=GetMessage('RZ_TOVAR')?>:</label> <label><?=htmlspecialcharsbx($_REQUEST['NAME'])?></label>
				</div>
			<?endif?>
			<? if ($arParams['FIELD_QUANTITY'] == 'Y'): ?>
				<div class="form-group">
					<label><?=GetMessage("RZ_KOLICHESTVO")?></label>:<input type="number" class="form-control" name="QUANTITY"
						   value="<?= $arResult['QUANTITY'] ?>" title=""/>
				</div>
			<? endif ?>
			<? foreach ($arResult['FIELDS'] as $arItem): ?>
				<div class="form-group<?if(isset($arResult['ERROR'][$arItem['CODE']])):?> has-error<?endif?>">
					<label<? if ($arItem['REQ']): ?> class="req" <? endif ?>><?= $arItem['NAME'] ?></label>:<?= $arItem['HTML'] ?>
				</div>
			<? endforeach ?>
			<? if ('Y' == $arResult['USE_CAPTCHA']): ?>
				<div class="form-group">
					<input  required type="hidden" name="captcha_sid" value="<?= $arResult["CAPTCHA_CODE"] ?>">
					<br/>
					<label><?= GetMessage("CAPTCHA_REGF_TITLE") ?></label>
				</div>
				<div class="form-group">
					<img src="/bitrix/tools/captcha.php?captcha_sid=<?= $arResult["CAPTCHA_CODE"] ?>" width="180" height="40" alt="CAPTCHA">
					<br/>
					<label class="req"><?= GetMessage("CAPTCHA_REGF_PROMT") ?></label>:<input type="text" name="captcha_word"  class="form-control" value="" title="">
				</div>
			<? endif ?>
            <div class="form-group agreement-policy">
                <div class="forms-wrapper">
                    <input id="privacy_policy_<?=$rand?>" required type="checkbox" name="privacy_policy" value="Y" <?=$_REQUEST['privacy_policy'] == 'Y' ? ' checked' : ''?>>
                    <label for="privacy_policy_<?=$rand?>">
                        <?= GetMessage('RZ_ACCEPT') ?> <?=GetMessage('RZ_CONCLUSION',array("#URL#" => Tools::GetConstantUrl($pathToRules)))?>
                    </label>
                </div>
            </div>
			<p class="help-block"><b class="req"></b> &mdash; <?=GetMessage("RZ_POLYA_OBYAZATELNIE_DLYA_ZAPOLNENIYA")?></p>
			<br/>
			<button type="submit" name="BUY_SUBMIT" class="btn btn-primary" value="Y"><?=GetMessage("RZ_ZAKAZAT")?></button>
		</form>
	<? endif ?>
<? endif ?>