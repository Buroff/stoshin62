<?
if (!function_exists('showSettingsItem')) {
    function showSettingsItem($arItem, $curValue, $templateFolder)
    {
        if ($arItem["CODE"] == 'COLOR_SCHEME') {
            $type = "COLOR_THEME";
        } elseif (isset($arItem["type"])) {
            $type = $arItem["type"];
        }
        // echo "<pre style='text-align:left;'>";print_r($curValue);echo "</pre>";
        // echo "<pre style='text-align:left;'>";print_r($arItem);echo "</pre>";
        switch ($type) {
            case 'HIDDEN':
                ?>
                <input type="hidden" name="SETTINGS[<?= $arItem["CODE"] ?>]" value="<?= $curValue ?>"
                       data-name="<?= $arItem["CODE"] ?>" data-default="<?= $arItem['default'] ?>">
                <? break;

            case "COLOR_THEME":
                ?>
                <? $i = 0; ?>
                <div class="form-group">
                    <? if (!empty($arItem['title'])):?>
                        <div class="h4"><?= $arItem['title'] ?>:
                            <? if (isset($arItem['has_review']) && $arItem['has_review'] == 'Y'):?>
                                <span class="icon icon_eye63" data-tooltip="" data-trigger="hover" data-placement="top"
                                      title="<?= GetMessage('HAS_REVIEW') ?>"></span>
                            <?endif ?>
                        </div>
                    <?endif ?>

                    <? foreach ($arItem['values'] as $theme): ?>
                        <? if ($i == 0): ?>
                            <div id="current-colot-theme">
										<span>
											<img alt="<?= $theme ?>"
                                                 src="<?= $templateFolder . '/img/' . $curValue . '.jpg' ?>">
										</span>
                            </div>
                        <? endif ?>
                        <? ++$i ?>
                        <label class="radio" title="<?= $arItem['names'][$theme] ?>">
                            <input data-name-func="<?= $arItem['function_call'] ?>" type="radio"
                                   name="SETTINGS[COLOR_SCHEME]" class="switcher_radio"
                                   value="<?= $theme ?>" <? if ($theme == $curValue): ?> checked<? endif ?>>
                            <span class="content"><?= $arItem['names'][$theme] ?></span>
                        </label>
                    <? endforeach ?>
                </div>
                <? unset($arItem) ?>
                <? break;

            case 'CHECKBOX':
                ?>
                <div class="form-group">
                    <div class="h4">
                        <? if (!empty($arItem['title'])):?>
                            <?= $arItem['title'] ?>:
                        <?
                        elseif (is_null($arItem['title'])):?>
                            <?= $arItem['name'] ?>:
                        <?endif ?>
                        <? if (isset($arItem['has_review']) && $arItem['has_review'] == 'Y'):?>
                            <span class="icon icon_eye63" data-tooltip="" data-trigger="hover" data-placement="top"
                                  title="<?= GetMessage('HAS_REVIEW') ?>"></span>
                        <?endif ?>
                    </div>
                    <label for="SETTINGS[<?= $arItem["CODE"] ?>]" title="<?= $arItem['name'] ?: $arItem['title'] ?>">
                        <input type="hidden" name="SETTINGS[<?= $arItem["CODE"] ?>]"
                               value="<?= $arItem['values'][1] ?>">
                        <input id="SETTINGS[<?= $arItem["CODE"] ?>]" type="checkbox"
                               name="SETTINGS[<?= $arItem["CODE"] ?>]"
                               value="<?= $arItem['values'][0] ?>" <? if ($arItem['values'][0] == $curValue): ?> checked="checked"<? endif ?>>
                        <span class="text"><?= $arItem['name'] ?></span>
                    </label>
                </div>
                <? break;

            case 'RADIO':
                ?>
                <div class="form-group">
                    <? if (!empty($arItem['title'])):?>
                        <div class="h4"><?= $arItem['title'] ?>:
                            <? if (isset($arItem['has_review']) && $arItem['has_review'] == 'Y'):?>
                                <span class="icon icon_eye63" data-tooltip="" data-trigger="hover" data-placement="top"
                                      title="<?= GetMessage('HAS_REVIEW') ?>"></span>
                            <?endif ?>
                        </div>
                    <?endif ?>
                    <? foreach ($arItem['values'] as $value): ?>
                        <label class="radio" title="<?= $arItem['names'][$value] ?>">
                            <input data-name-func="<?= $arItem['function_call'] ?>" type="radio"
                                   name="SETTINGS[<?= $arItem["CODE"] ?>]" class="switcher_radio"
                                   value="<?= $value ?>" <? if ($value == $curValue): ?> checked<? endif ?>>
                            <span class="content"><?= $arItem['names'][$value] ?></span>
                        </label>
                    <? endforeach ?>
                </div>

                <? break;

            /*case 'SELECT': ?>
                <div class="setting-desc">
                    <?= $arItem['name'] ?>:
                    <? if ($arItem['preview']):?>
                        <i class="has-preview flaticon-43123" title="<?= GetMessage('BITRONIC2_SETTING_HAS_PREVIEW') ?>" data-tooltip></i>
                    <? endif ?>
                </div><!-- .setting-desc -->
                <div class="setting-content">
                    <select name="SETTINGS[<?= $arItem["CODE"] ?>]" id="settings_<?= $arItem["CODE"] ?>" title="<?= $arItem['name'] ?>" data-name="<?= $arItem["CODE"] ?>"
                            data-autowidth="true">
                        <?foreach ($arItem["values"] as $value):?>
                            <option value="<?= $value ?>"<?= ($curValue == $value) ? ' selected' : '' ?>
                                <?if($arItem['default'] == $value):?> data-default<?endif?>
                                >
                                <?if(!empty($arItem['names'][$value])):?>
                                    <?= $arItem['names'][$value] ?>
                                <?else:?>
                                    <?= GetMessage('BITRONIC2_' . strtoupper($arItem["CODE"]) . '_' . strtoupper($value)) ?>
                                <?endif?>
                            </option>
                        <?endforeach?>
                    </select>
                </div><!-- .setting-content -->
            <? break;*/

            /*case 'CHECKBOX_MOBILE': ?>
            <div class="setting-desc">
                <? if ($arItem['preview']):?>
                    <i class="has-preview flaticon-43123" title="<?= GetMessage('BITRONIC2_SETTING_HAS_PREVIEW') ?>" data-tooltip></i>
                <? endif ?>
            </div><!-- .setting-desc -->
            <div class="setting-content">
                <div class="pull-left">
                    <?
                    $isDisabled = $arItem['states']['mobile']['state'] == 'disabled';

                    if($isDisabled) {
                        $checked = true;
                        if($arItem['states']['mobile']['status'] == 'unchecked') {
                            $checked = false;
                        }
                    }
                    ?>
                    <input type="hidden" name="SETTINGS[<?= $arItem["CODE"] ?>_MOBILE]" id="settings_<?= $arItem["CODE"] ?>_MOBILE_hidden" value="<?= $arItem['values'][1]?>">
                    <label class="checkbox-styled" for="settings_<?= $arItem["CODE"] ?>_MOBILE">
                        <input name="SETTINGS[<?= $arItem["CODE"] ?>_MOBILE]" type="checkbox" id="settings_<?= $arItem["CODE"] ?>_MOBILE" value="<?= $arItem['values'][0]?>"
                               data-name="<?= $arItem["CODE"] ?>_mobile" <?if($isDisabled):?> disabled<?endif?>
                            <?if(isset($checked)):?>
                                <?= ($checked) ? ' checked ' : '' ?>
                            <? else :?>
                                <?= ($curValue['mobile'] == $arItem['values'][0]) ? ' checked ' : '' ?>
                            <? endif ?>
                            <?if($arItem['default_MOBILE'] == $arItem['values'][0]):?> data-default="1"<?else:?> data-default="0"<?endif?>
                            />
                        <span class="checkbox-content"><i class="flaticon-check14"></i>&nbsp;<span class="icon flaticon-phone12" title="<?=GetMessage('BITRONIC2_TITLE_FOR_MOBILE')?>" data-tooltip></span></span></label>
                    <br>
                    <input type="hidden" name="SETTINGS[<?= $arItem["CODE"] ?>]" id="settings_<?= $arItem["CODE"] ?>_hidden" value="<?= $arItem['values'][1]?>">
                    <label class="checkbox-styled" for="settings_<?= $arItem["CODE"] ?>">
                        <input name="SETTINGS[<?= $arItem["CODE"] ?>]" type="checkbox" id="settings_<?= $arItem["CODE"] ?>" value="<?= $arItem['values'][0]?>"
                               data-name="<?= $arItem["CODE"] ?>"
                            <?= ($curValue['orig'] == $arItem['values'][0]) ? ' checked ' : '' ?>
                            <?if($arItem['default'] == $arItem['values'][0]):?> data-default="1"<?else:?> data-default="0"<?endif?>
                            />
                        <span class="checkbox-content"><i class="flaticon-check14"></i>&nbsp;<span class="icon flaticon-widescreen" title="<?=GetMessage('BITRONIC2_TITLE_FOR_PC')?>" data-tooltip></span></span></label>
                </div>
                <div class="pull-left combined_name">
                    <div class="wrapper">
                        <span><?= $arItem['name'] ?></span>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div><!-- .setting-content -->
            <? break;

            case 'SLIDER': ?>
                <div class="setting-desc">
                    <?= $arItem['name'] ?>:
                    <? if ($arItem['preview']):?>
                        <i class="has-preview flaticon-43123" title="<?= GetMessage('BITRONIC2_SETTING_HAS_PREVIEW') ?>" data-tooltip></i>
                    <? endif ?>
                </div><!-- .setting-desc -->
                <div class="setting-content">
                    <input type="text" name="SETTINGS[<?= $arItem["CODE"] ?>]" id="settings_<?= $arItem["CODE"] ?>"
                           class="textinput <?= $arItem["CODE"] ?>-input" value="<?= (!empty($curValue)) ? $curValue : $arItem['default'] ?>"
                           data-name="<?= $arItem["CODE"] ?>" data-default="<?= $arItem['default'] ?>">
                    <div class="simple-slider <?= $arItem["CODE"] ?>-slider" id="<?= $arItem["CODE"] ?>-slider"
                         data-name="<?= $arItem["CODE"] ?>" data-start="<?= (!empty($curValue)) ? $curValue : $arItem['default'] ?>"
                         data-min="<?= $arItem["min"] ?>" data-max="<?= $arItem["max"] ?>" data-step="<?= $arItem["step"] ?>"
                         <?if ($arItem["preview"]): ?>data-set<? endif ?>
                         <?if(!empty($arItem['postfix'])):?> data-postfix="<?=$arItem['postfix']?>"<? endif ?>
                        ></div>
                </div><!-- .setting-content -->
                <? break;*/

            case 'COLOR': ?>
                <div class="settings-option">
                    <? if (!empty($arItem['title'])): ?>
                        <div class="settings-title"><?= $arItem['title'] ?>:</div>
                    <?
                    elseif (is_null($arItem['title'])): ?>
                        <div class="settings-title"><?= $arItem['name'] ?>:</div>
                    <?endif ?>
                    <?
                    $curValue = (!empty($curValue)) ? $curValue : $arItem['default'];
                    //$dataType = ($curValue{0} == '#') ? 'color' : 'pattern';
                    ?>
                    <?/*if (isset($arItem['choose'])): ?>
						<select class="type-choose" name="<?= $arItem["CODE"] ?>_type" id="<?= $arItem["CODE"] ?>_type" title="">
							<? foreach ($arItem['choose'] as $val => $name): ?>
								<option value="<?= $val ?>"<?= ($dataType == $val) ? ' selected' : '' ?>><?= $name ?></option>
							<? endforeach ?>
						</select>
						<? if (isset($arItem['choose']['pattern'])): */ ?>
                    <div class="settings-bg-wrap active" <?// ($dataType != 'pattern') ? 'style="display:none;"' : '' ?>>
                        <?
                        $arFiles = glob($_SERVER['DOCUMENT_ROOT'] . BX_ROOT . '/images/' . \Yenisite\Furniture\Settings::getModuleId() . '/patterns/*.{jpg,png,gif,jpeg}', GLOB_BRACE);
                        ?>
                        <div class="patterns">
                            <?
                            natsort($arFiles);
                            $filterType = ($arItem['filter']{0} == '!') ? 'not' : '';
                            $filterVal = ($filterType == 'not') ? substr($arItem['filter'], 1) : $arItem['filter'];
                            foreach ($arFiles as $file): ?>
                                <label class="pattern-label" data-name="SETTINGS[<?= $arItem["CODE"] ?>]">
                                    <?
                                    $baseName = basename($file);
                                    $curValue = str_replace(')', '', $curValue);
                                    $bgValue = 'url(' . str_replace($_SERVER['DOCUMENT_ROOT'], '', $file) . ')';
                                    $bgSrc = str_replace($_SERVER['DOCUMENT_ROOT'], '', $file);
                                    ?>
                                    <input type="radio" class="pattern-radio" name="SETTINGS[<?= $arItem["CODE"] ?>]"
                                           value="<?= $bgValue ?>" <?= (basename($curValue) == $baseName) ? ' checked' : '' ?>>
                                    <span class="pattern-image-wrap"><img src="<?= $bgSrc ?>" alt=""
                                                                          class="pattern-image"></span>
                                </label>
                            <? endforeach ?>
                        </div>
                    </div>
                    <?/* endif ?>
					<? endif */ ?>
                </div><!-- .setting-otion -->
                <? break;
        }
    }
}

