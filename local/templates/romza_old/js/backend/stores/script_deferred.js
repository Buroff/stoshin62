$(function ($) {
	$.fn.initStorePopover = function() {
		var width = 450,trigger = {'CLICK': 0, 'HOVER': 0};
		if (typeof rzStoreOne != 'undefined'){
            width = rzStoreOne.WIDTH;
            trigger = rzStoreOne.TRIGGER;
		}
		this.find('.RzStore__popupUrl').webuiPopover({
			placement: 'auto',//values: auto,top,right,bottom,left,top-right,top-left,bottom-right,bottom-left
			width: width,//can be set with number
			height: 'auto',//can be set with number
			trigger: trigger,//values:click,hover
			style: '',//values:'',inverse
			constrains: '', // constrains the direction when placement is  auto,  values: horizontal,vertical
			animation: 'pop', //pop with animation,values: pop,fade (only take effect in the browser which support css3 transition)
			delay: {//show and hide delay time of the popover, works only when trigger is 'hover',the value can be number or object
				show: null,
				hide: 300
			},
			//async: {
			//	before: function (that, xhr) {
			//	},//executed before ajax request
			//	success: function (that, data) {
			//	}//executed after successful ajax request
			//},
			cache: true,//if cache is set to false,popover will destroy and recreate
			multi: false,//allow other popovers in page at same time
			arrow: true,//show arrow or not
			title: function () {
				return $(this).data('title');
			},//the popover title ,if title is set to empty string,title bar will auto hide
			content: function () {
				return $(this).closest('.RzStore__item').find('.RzStore__popupBody').html();
			},//content of the popover,content can be function
			closeable: true,//display close button or not
			padding: true,//content padding
			type: 'html',//content type, values:'html','iframe','async'
			url: ''//if not empty ,plugin will load content by url
		});
	};
	var $body = $('body');
	$body.initStorePopover();
	$body.on('shown.webui.popover', '.RzStore__popupUrl', function(e) {
		var $this = $(this),
			$target = $('#' + $this.data('target')),
			$map = $target.find('.store_map');
		if($this.data('init')) {
			return true;
		}
		if ($map.length > 0 && $map.data('gpsn') > 0 && $map.data('gpss') > 0) {
			console.log('start render map');
			var gps_n = $map.data('gpsn'),
				gps_s = $map.data('gpss');
			ymaps.ready(function () {
				var myMap = new ymaps.Map($map[0], {
					center: [gps_s, gps_n],
					zoom: 15
				});
				myMap.geoObjects.add(new ymaps.Placemark([gps_s, gps_n]));
			});
		}
		$this.data('init', true);
	});
});