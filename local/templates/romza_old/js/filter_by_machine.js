var _setHtmlOptions = function(elements,apendTo){
	// console.log('--- _setHtmlOptions ---');
	// console.log(elements);
	// console.log(apendTo);

    var options = '';
    if (typeof elements != 'undefined'){
        $.each(elements,function(index,value){
        	console.log(value);
            if (value) {
                options += '<option value="' + value + '">' + value + '</option>'
            }
        });
        apendTo.children().remove();
        apendTo.append($(options));
    }
};

var _getModels = function(elements){
    var models = [];
    $.each(elements,function(){
        if (this.NAME){
            models.push(this.NAME);
        }
    });
    return models;
};