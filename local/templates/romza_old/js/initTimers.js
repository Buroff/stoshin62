$(function($) {
    function initTimers() {
        $(document).find('.timer').each(function () {
            var $t = $(this);
            var liftoff = new Date($t.data('until'));
            $t.countdown({until: liftoff});
        })
    }

    $(document).ready(function () {
        initTimers();
    });
});