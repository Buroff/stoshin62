// MAIN MENU PROCESSING
$(function($){
"use strict";

function closeMenuItem ($item) {
$item.children('.mm-submenu').stop(true, true).slideUp('fast');
    $item.data('opened', false).removeClass('opened');
}
function openMenuItem ($item, $submenu) {
    var $others = $item.siblings('.opened');
    $item.children('.mm-submenu').stop(true, true).slideDown('fast');
    $item.data('opened', true).addClass('opened');

    if ($others.length) $others.each(function () {
        closeMenuItem($(this));
    });
}
function menuInLine ($wrap, $hidden, $items, $mainList) {
    for (var i = $items.length - 1; i > 0; i--){
        if ($items.eq(i).position().left < $mainList.width()){
            if ( $items.eq(i).position().left + $items.eq(i).width() > $mainList.width() ) {
                // found last visible item, move others to hidden
                $items.slice(i).appendTo($hidden);
                $wrap.addClass('w-hidden-items ready');

                return;
            }
        }
    }
    $wrap.addClass('all-inline');
}
function menuInColumn ($wrap, $hidden, $items, $mainList, $search, $toggle, $_mm_addit) {
    if ( $wrap.parents('.big-slider').length )
        $search.removeClass('search-panel-full');

    if ( $mainList.outerHeight() > $wrap.height() ) {
        for (var i = $items.length - 1; i > 0; i--) {
            if ( ( $items.eq(i).position().top + $items.eq(i).height() ) <= ( $wrap.height() - $toggle.height() ) ) {
                // found last visible item, move others to hidden
                $_mm_addit = true;
                $items.slice(i + 1).appendTo($hidden);
                $wrap.addClass('w-hidden-items');

                return;
            }
        }
    } else if ( $_mm_addit === false ) {
        if ($items.length > 10) {
            $items.slice(10).appendTo($hidden);
            $wrap.addClass('w-hidden-items');

            return;
        }
    }
    $wrap.addClass('all-incolumn');
}

$('#mm0-additional').on('show.bs.collapse', function () {
    $('#mainmenu-wrap').addClass('shown-additional');
});

$('#mm0-additional').on('hide.bs.collapse', function () {
    $('#mainmenu-wrap').removeClass('shown-additional');
});

$('#mainmenu-wrap').on('click', '.mm0-link, .mm1-link, .mm2-link', function (e) {
    if (!Modernizr.mq('(max-width: 767px)') && !Modernizr.mq('(max-width: 1199px)')) return true;

    var $item = $(this).closest('li'),
        hasSub = $item.hasClass('has-submenu');

    if (!hasSub || $(e.target).is('.badge')){
        return true;
    }

    if ($item.data('opened')){
        closeMenuItem($item);
        if(Modernizr.mq('(max-width: 1199px)')){
            return true;
        }
    }
    else { openMenuItem($item);}
        
    return false;
});

rmz.updateMenu = function(){
    var $wrap = $('#mainmenu-wrap'),
        $main = $('#mainmenu-lvl0'),
        $mainList = $('#mainmenu-lvl0-main'),
        $hidden = $('#mm0-additional'),
        $hidItems = $hidden.find('.mm0-item'),
        $toggle = $wrap.find('#menuheight-toggle'),
        $search = $('.page_header__main'),
        $slider = $('.banners_rotator_block'),
        $items;

    // reset everything
    if ( $wrap.hasClass('w-horizontal') )
        $wrap.removeClass('w-horizontal');
    if ( $wrap.hasClass('all-inline') )
        $wrap.removeClass('all-inline');
    if ( $wrap.hasClass('all-incolumn') )
        $wrap.removeClass('all-incolumn');
    if ($hidItems)
        $hidItems.appendTo($mainList);
    if ( $search.hasClass('search_panel__full') )
        $search.removeClass('search_panel__full');
    $wrap.removeClass('w-hidden-items ready');
    $main.css({
        'min-height': '',
    });
    // end of reset

    var $_menuWrap = '.main-menu-outer-wrap',
        $_menuHeader = '.page_header__nav .container',
        $_menuVertic = '.page_vertical__nav',
        $_mm_addit = false;

    // menu height is greater than wrap's. Moving extra items to hidden.
    // but first, refresh items array after reset
    $items = $main.find('.mm0-item');

    if ( !$wrap.parents($_menuHeader).length ) {
        // change mainmenu position
        $wrap.appendTo($_menuHeader);
    }

    if (!Modernizr.mq('(max-width: 767px)')) {
        if ($('[data-menu-type]').attr('data-menu-type') === 'horizontal') {
            $wrap.addClass('w-horizontal');

            menuInLine($wrap, $hidden, $items, $mainList);
        } else if ($('[data-menu-type]').attr('data-menu-type') === 'vertical') {
            if ( Modernizr.mq('(min-width: 768px)') || $slider.length ) {
                if ( ($($_menuVertic).length && $($_menuVertic).css('display') !== 'none') && ($wrap.parents($_menuHeader).length || $wrap.parents($_menuVertic).length) ) {
                    if ( $wrap.parents($_menuHeader).length ) {
                        // menu is at the top position - change mainmenu position
                        $wrap.appendTo($_menuVertic);
                        if ( !$slider.length )
                            $search.addClass('search_panel__full');
                    }

                    menuInColumn ($wrap, $hidden, $items, $mainList, $search, $toggle, $_mm_addit);
                }
            }
        }
    }
    initSly('[data-sly-from]');
}

$('#mainmenu-wrap').off('mouseenter mouseleave').on('mouseenter', '.mm0-item', function(e){
    if (Modernizr.mq('(max-width: 767px)')) return;
    var $t = $(this),
        $submenu = $t.children('.mm-submenu');

    $submenu.show().outerHeight();
    $submenu.find('[data-sly-from]').sly('reload');
}).on('mouseleave', '.mm0-item', function(){
    if (Modernizr.mq('(max-width: 767px)')) return;
    var $t = $(this),
        $submenu = $t.children('.mm-submenu');

    $submenu.one('bsTransitionEnd', function(){
        $submenu.hide();
    });
});

resizeHandlers.push(rmz.updateMenu);
$(window).load(rmz.updateMenu);

});
// END OF MAIN MENU PROCESSING
