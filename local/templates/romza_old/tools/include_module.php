<?
use Bitrix\Main\Loader;

$moduleId = include 'module_code.php';
$return = function ($msg, $isAjax) {
	if ($isAjax) {
		echo json_encode(array('success' => 0, 'msg' => $msg));
		die();
	}else{
		die($msg);
	}
};
$isAjax = Bitrix\Main\Application::getInstance()->getContext()->getRequest()->isAjaxRequest();
if (!Loader::IncludeModule($moduleId)) $return('Module ' . $moduleId . ' not installed!', $isAjax);
if (!Loader::IncludeModule('yenisite.core')) $return(GetMessage('NOT_INSTALL_CORE', array('#CORE_HREF#' => '/bitrix/admin/update_system_partner.php?lang='.LANGUAGE_ID)), $isAjax);
if (!Loader::IncludeModule('yenisite.resizer2')) $return(GetMessage('NOT_INSTALL_RESIZER', array('#CORE_HREF#' => '/bitrix/admin/update_system_partner.php?lang='.LANGUAGE_ID)), $isAjax);