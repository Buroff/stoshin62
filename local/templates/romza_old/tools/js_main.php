<?// MAIN_MENU?>
<? use Bitrix\Main\Page\Asset;
global $rz_options;

$Asset = Asset::getInstance();
$Asset->addJs(SITE_TEMPLATE_PATH . '/js/main-menu/sly.min.js'); ?>
<script src="<?= SITE_TEMPLATE_PATH ?>/js/main-menu/modernizr-custom.js"></script>
<script src="<?= SITE_TEMPLATE_PATH ?>/js/jScrollPane/script/jquery.jscrollpane.min.js"></script>
<script src="<?= SITE_TEMPLATE_PATH ?>/js/bootstrap-touchspin/src/jquery.bootstrap-touchspin.js"></script>
<script src="<?= SITE_TEMPLATE_PATH ?>/js/um-height-control.js"></script>
<script src="<?= SITE_TEMPLATE_PATH ?>/js/settings-helpers.js"></script>
<script src="<?= SITE_TEMPLATE_PATH ?>/js/main-menu/initSly.js"></script>
<script src="<?= SITE_TEMPLATE_PATH ?>/js/main-menu/globals.js"></script>
<script src="<?= SITE_TEMPLATE_PATH ?>/js/main-menu/mainmenu.js"></script>

<?//TIMER CATCHBUY?>
<script src="<?= SITE_TEMPLATE_PATH ?>/js/jquery.maskedinput.min.js"></script>
<script src="<?= SITE_TEMPLATE_PATH ?>/js/jquery.countdown.2.0.2/jquery.plugin.js"></script>
<script src="<?= SITE_TEMPLATE_PATH ?>/js/jquery.countdown.2.0.2/jquery.countdown.min.js"></script>
<script src="<?= SITE_TEMPLATE_PATH ?>/js/jquery.countdown.2.0.2/jquery.countdown-ru.js"></script>
<script src="<?= SITE_TEMPLATE_PATH ?>/js/initTimers.js"></script>

<?//VALIDATOR?>
<script src="<?= SITE_TEMPLATE_PATH ?>/js/lib/validator_custom.js"></script>
<script src="<?= SITE_TEMPLATE_PATH ?>/js/inits/init-valid.js"></script>

<?//POPOVER?>
<?if ($rz_options['show_stores_in_section'] == 'Y' || $rz_options['show_stores_in_element'] == 'Y'):?>
    <script src="<?= SITE_TEMPLATE_PATH ?>/js/popover/popover.js"></script>
    <script src="<?= SITE_TEMPLATE_PATH ?>/js/backend/stores/script_deferred.js"></script>
<?endif?>

