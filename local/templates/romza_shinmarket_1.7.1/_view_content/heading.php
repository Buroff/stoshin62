<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
$showBreadCrumb = $APPLICATION->GetProperty('FORCE_SHOW_BREADCRUMBS', 'N');
if ($showBreadCrumb != 'N' && CHTTP::GetLastStatus() != '404 Not Found') :?>
	<?$APPLICATION->IncludeComponent('romza:breadcrumb.get', '', array())?>
<?endif;
$showHeading = $APPLICATION->GetProperty('FORCE_SHOW_HEADING', 'N');
if ($showHeading != 'N') :?>
	<h1><b><?= $APPLICATION->GetTitle() ?></b></h1>
<?endif;


