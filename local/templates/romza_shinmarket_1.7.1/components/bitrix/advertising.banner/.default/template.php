<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
<?if (method_exists($this, 'setFrameMode')) $this->setFrameMode(true); ?>
<div class="main_content__banners_item">
	<? if (method_exists($this, 'createFrame')) $frame = $this->createFrame()->begin(''); ?>
	<?= $arResult["BANNER"]; ?>
	<? if (method_exists($this, 'createFrame')) $frame->end(); ?>
</div>
