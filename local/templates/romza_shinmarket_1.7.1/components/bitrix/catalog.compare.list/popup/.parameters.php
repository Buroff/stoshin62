<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;

if (Loader::IncludeModule("yenisite.resizer2")) {
	$resizer_sets_list = array();
	$arSets = CResizer2Set::GetList();
	while ($arr = $arSets->Fetch()) {
		$resizer_sets_list[$arr["id"]] = "[" . $arr["id"] . "] " . $arr["NAME"];
	}

	global $arComponentParameters;

	$arComponentParameters["GROUPS"]["RESIZER_SETS"] = array(
		"NAME" => GetMessage("RESIZER_SETS"),
		"SORT" => 1
	);

	$arTemplateParameters = array(
		"RESIZER_PRODUCT" => array(
			"PARENT" => "RESIZER_SETS",
			"NAME" => GetMessage("RESIZER_PRODUCT"),
			"TYPE" => "LIST",
			"VALUES" => $resizer_sets_list,
			"DEFAULT" => "6",
		),
	);
}