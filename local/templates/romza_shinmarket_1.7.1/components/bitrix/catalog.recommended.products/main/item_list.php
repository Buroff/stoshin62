<?
	$boolFirst = true;
	$arRowIDs = array();
	foreach ($arItems as $keyItem => $arItem) {
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
		$strMainID = $this->GetEditAreaId($arItem['ID']);

		$strTitle = (
		isset($arItem["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_TITLE"]) && '' != isset($arItem["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_TITLE"])
			? $arItem["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_TITLE"]
			: $arItem['NAME']
		);?>
		<?
		$hasOfferSelect = false;
		if(isset($arItem['FLAT_OFFERS']) && count($arItem['FLAT_OFFERS']) > 0 ) $hasOfferSelect = true;
		?>
		<div class="goods_item <?if($hasOfferSelect):?> has_offer_select<?endif?>">
			<div class="goods_item__inner">
				<a href="<?=$arItem['DETAIL_PAGE_URL']; ?>"
				   class="goods_item__img_wrapper" >
					<img src="<?=$arItem['PICTURE'] ?>"
						 alt="<?= $strTitle; ?>" title="<?= $strTitle; ?>"
						 class="goods_item__img" >
				</a>
				<div class="goods_item__info">
					<div class="goods_item__description_wrapper">
						<div class="goods_item__title">
							<div class="goods_item__title">
								<a href="<?= $arItem['DETAIL_PAGE_URL']; ?>" title="<?= $arItem['NAME']; ?>">
									<?= $arItem['NAME']; ?>
								</a>
							</div>
						</div>
					</div>
					<div class="goods_item__buying">
						<div class="goods_item__rating">
							<?$APPLICATION->IncludeComponent(
								"bitrix:iblock.vote",
								"stars",
								array(
									"IBLOCK_TYPE" => $arParams['IBLOCK_TYPE'],
									"IBLOCK_ID" => $arParams['IBLOCK_ID'],
									"ELEMENT_ID" => $arItem['ID'],
									"ELEMENT_CODE" => "",
									"MAX_VOTE" => "5",
									"VOTE_NAMES" => array("1", "2", "3", "4", "5"),
									"SET_STATUS_404" => "N",
									"DISPLAY_AS_RATING" => $arParams['VOTE_DISPLAY_AS_RATING'],
									"CACHE_TYPE" => $arParams['CACHE_TYPE'],
									"CACHE_TIME" => $arParams['CACHE_TIME']
								),
								$component,
								array("HIDE_ICONS" => "Y")
							);?>
						</div>
						<div class="goods_item__price">
							<?if(method_exists($this, 'createFrame')) $frame = $this->createFrame()->begin();?>
								<?if ($arItem['MIN_PRICE']['PRINT_DISCOUNT_VALUE'] != $arItem['MIN_PRICE']['PRINT_VALUE']):?>
									<del class="old_price"><?= $arItem['MIN_PRICE']['PRINT_VALUE']; ?></del>
								<?endif?>
								<div class="current_price">
									<? if (!empty($arItem['MIN_PRICE'])) {
										if ('N' == $arParams['PRODUCT_DISPLAY_MODE'] && isset($arItem['OFFERS']) && !empty($arItem['OFFERS'])) {
											echo GetMessage( 'CT_BCT_TPL_MESS_PRICE_SIMPLE_MODE',
												array (
													'#PRICE#' => $arItem['MIN_PRICE']['PRINT_DISCOUNT_VALUE'],
												)
											);
										} else {
											echo $arItem['MIN_PRICE']['PRINT_DISCOUNT_VALUE'];
										}
									}?>
								</div>
							<?if(method_exists($this, 'createFrame')) $frame->end();?>
						</div>
						<div class="goods_item__manage_wrapper">
							<div class="goods_item__amount">
								<form class="add2basket_form" method="get" action="<?=$APPLICATION->GetCurPage()?>"
								  data-varQ="<?= $arParams["PRODUCT_QUANTITY_VARIABLE"]; ?>"
								  data-varId="<?= $arParams['PRODUCT_ID_VARIABLE'] ?>"
								  data-varAct="<?=$arParams['ACTION_VARIABLE']?>"
								>
									<?if(method_exists($this, 'createFrame')) $frame = $this->createFrame()->begin();?>
										<input type="hidden" name="<?=$arParams['ACTION_VARIABLE']?>" value="ADD2BASKET"/>
										<? if($hasOfferSelect):?>
											<select class="normal offer_select" name="<?= $arParams['PRODUCT_ID_VARIABLE'] ?>" title="">
												<?$f = true; foreach($arItem['FLAT_OFFERS'] as $id => $arOffer):?>
													<option
														<?if(isset($arOffer['PRICE'])):?>
															data-price="<?=$arOffer['PRICE']?>"
														<?endif?>
														<?if(isset($arOffer['NAME'])):?>
															data-name="<?=$arOffer['NAME']?>"
														<?endif?>
														<?if(isset($arOffer['CATCH_BUY']['TIMER'])):?>
															data-timer="<?=$arOffer['CATCH_BUY']['TIMER']?>"
														<?endif?>
														<?if(isset($arOffer['CATCH_BUY']['PROGRESS'])):?>
															data-progres="<?=$arOffer['CATCH_BUY']['PROGRESS']?>"
														<?endif?>
														<?if(isset($arOffer['PRICE_OLD'])):?>
															data-priceold="<?=$arOffer['PRICE_OLD']?>"
														<?endif?>
														value="<?= $id ?>"<?if($f):?> selected<?$f = false; endif;?> ><?=trim($arOffer['NAME'])?></option>
												<?endforeach?>
											</select>
										<?else:?>
											<input type="hidden" name="<?=$arParams['PRODUCT_ID_VARIABLE']?>" value="<?=$arItem['ID']?>"/>
										<?endif;?>
										<input type="submit" class="btn btn-primary btn-buy" value="<?=$arParams['MESS_BTN_BUY']?>" title="<?=$arParams['MESS_BTN_BUY']?>">
									<? if ($arItem['CAN_BUY'] == 'Y' && $arParams['USE_ONE_CLICK'] == 'Y' && CModule::IncludeModule('sale') && CModule::IncludeModule('yenisite.oneclick')): ?>
										<?$APPLICATION->IncludeComponent(
											"yenisite:oneclick.buy",
											"ajax",
											array(
												"PERSON_TYPE_ID" => $arParams["ONECLICK_PERSON_TYPE_ID"],
												"SHOW_FIELDS" => $arParams["ONECLICK_SHOW_FIELDS"],
												"REQ_FIELDS" => $arParams["ONECLICK_REQ_FIELDS"],
												"ALLOW_AUTO_REGISTER" => $arParams["ONECLICK_ALLOW_AUTO_REGISTER"],
												"IBLOCK_TYPE" => $arParams['IBLOCK_TYPE'],
												"QUANTITY" => $arItem['DEFAULT_QUANT'],
												"IBLOCK_ID" => $arParams['IBLOCK_ID'],
												"IBLOCK_ELEMENT_ID" =>$arItem['ID'],
												"USE_CAPTCHA" => $arParams["ONECLICK_USE_CAPTCHA"],
												"MESSAGE_OK" => $arParams["~ONECLICK_MESSAGE_OK"],
												"PAY_SYSTEM_ID" => $arParams["ONECLICK_PAY_SYSTEM_ID"],
												"DELIVERY_ID" => $arParams["ONECLICK_DELIVERY_ID"],
												"AS_EMAIL" => $arParams["ONECLICK_AS_EMAIL"],
												"AS_NAME" => $arParams["ONECLICK_AS_NAME"],
												"BUTTON_TYPE" => "HREF",
												"SEND_REGISTER_EMAIL" => $arParams["ONECLICK_SEND_REGISTER_EMAIL"],
												"FIELD_CLASS" => "form-control",
												"FIELD_PLACEHOLDER" => $arParams["ONECLICK_FIELD_PLACEHOLDER"],
												"FIELD_QUANTITY" => $arParams["ONECLICK_FIELD_QUANTITY"],
												'OFFER_PROPS' => $arParams['OFFER_TREE_PROPS'],
												'NAME' => $arItem['NAME']
											),
											$component
										);?>
									<? endif ?>
									<?if(method_exists($this, 'createFrame')) $frame->end();?>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	<? } // endforeach ?>