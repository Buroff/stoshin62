<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
/** @global CDatabase $DB */

$this->setFrameMode(true);
if (isset($arResult['ITEMS']) && !empty($arResult['ITEMS'])): ?>
	<div class="popup_buy__content">
		<div class="popup_buy__content_head"><?=GetMessage("RZ_S_ETIM_TOVAROM_TAKZHE_POKUPAYUT")?>:</div>
		<div class="goods_list grid_view">
			<? $arItems = &$arResult['ITEMS'];?>
			<?include(__DIR__.'/item_list.php')?>
		</div>
	</div>
<? endif ?>