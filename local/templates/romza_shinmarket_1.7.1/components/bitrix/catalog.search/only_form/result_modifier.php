<?
use Bitrix\Main\Loader;
use Yenisite\Core\Tools;

include_once $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/tools/include_module.php';
$arParams['SEARCH_PAGE_URL'] = trim($arParams['SEARCH_PAGE_URL']);
if (strlen($arParams['SEARCH_PAGE_URL']) == 0) {
	$arParams['SEARCH_PAGE_URL'] = '#SITE_DIR#search/';
}
$arParams['SEARCH_PAGE_URL'] = Tools::GetConstantUrl($arParams['SEARCH_PAGE_URL']);
if (!Loader::IncludeModule('iblock')) die();
$IBLOCK_ID = intval($arParams['IBLOCK_ID']);
if ($IBLOCK_ID > 0) {
	/** @noinspection PhpDynamicAsStaticMethodCallInspection */
	$dbSections = CIBlockSection::GetList(array("SORT" => "ASC"),
		array(
			'IBLOCK_ID' => $IBLOCK_ID,
			'DEPTH_LEVEL' => 1,
            'ACTIVE' => 'Y',
		),
		false,
		array('ID', 'NAME')
	);
	$arSections = array();
	while ($arSection = $dbSections->GetNext()) {
		$arSections[$arSection['ID']] = $arSection['NAME'];
	}
	$arResult['SECTIONS'] = $arSections;
}