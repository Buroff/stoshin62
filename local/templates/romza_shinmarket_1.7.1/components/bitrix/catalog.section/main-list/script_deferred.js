$(function ($) {
	var $catalog_sectionList = $('#catalog_sectionList');
	var $body = $("html, body");

	$catalog_sectionList.on('click', '.pagination a:not(.disabled,.current)', function (e) {
		updateCatalog($(this), e);
		$body.animate({scrollTop: $catalog_sectionList.offset().top}, 400, 'linear');
	});
	$catalog_sectionList.on('change', 'select.sorting_type_select', function (e) {
		var $this = $(this);
		setCookie('CATALOG_SORT', $this.val(), {path: '/'});
		updateCatalog($this, e);
	});

	$catalog_sectionList.on('click', '#btn_tableBuy', function (e) {
		var $this = $(this);
		e.preventDefault();
		$this.setAjaxLoading();

		var requests = [];
		$catalog_sectionList.find('.add2basket_form').each(function (i, el) {
			var $el = $(el),
				dataQ = $el.attr('data-varQ'),
				$q = $el.find('input[name=' + dataQ + ']'),
				q = $q.val() | 0;
			if (q > 0) {
				requests.push($el.doAjaxAddToCart());
			}
		});

		$.when.apply($, requests).done(function () {
			$this.stopAjaxLoading();
			//$this.text(BX.message('RZ_CATALOG_LIST_IN_CART'));
		});
	});

	$catalog_sectionList.on('change', 'input.js-touchspin', function (e) {
		$catalog_sectionList.updateTableBuyButton(e);
	});
	$catalog_sectionList.on('change', 'select.items_on_page_select', function (e) {
		var $this = $(this);
		setCookie('LIST_COUNT', $this.val(), {path: '/'});
		updateCatalog($this, e);
	});

	$(document).on('catalog_applyFilter', function (e) {
		updateCatalog(null, e);
	});
	var refreshParams = function (arParams) {
		for (var i = 0; i < arParams.length; i++) {
			if (arParams[i].name == "list_count"
				|| arParams[i].name == "catalog_sort"
				|| arParams[i].name == "view_mode") {

				arParams[i].value = getCookie(arParams[i].name.toUpperCase());
			}
		}
		return arParams;
	};
	var updateCatalog = function ($this, e) {
		if (typeof(e) == "object") {
			e.preventDefault();
		}
		var isFilterAjax = false,
			isCustomFilter = false;
		if ("FILTER_AJAX" in rz && rz.FILTER_AJAX.length > 0) {
			$.each(rz.FILTER_AJAX, function (i, el) {
				if (el.name.search('CUSTOM_FILTER') != -1) {
					isCustomFilter = true;
				}
			});
			isFilterAjax = true;
		}
		var hrefParams = [],
			getParams = [];
		if ($this) {
			if ($this.data('href')) {
				hrefParams = getQueryVariable(false, $this.data('href'));
			} else {
				hrefParams = getQueryVariable(false, $this.attr('href'));
			}
		}
		if (!isFilterAjax) {
			getParams = getQueryVariable(false, false, {'PAGEN_1': 1});
		} else {
			getParams = rz.FILTER_AJAX;
		}
		var filterParams = $('#sectionFilter').data('arparams');
		var data = [
			{name: 'arParams', value: $catalog_sectionList.data('arparams')},
			{name: 'template', value: $catalog_sectionList.data('template').toString()},
			{name: 'URL', value: $catalog_sectionList.data('url')},
			{name: 'IS_SKU', value: $catalog_sectionList.data('issku')}
		];
		hrefParams = refreshParams(hrefParams);
		var pagen = {};
		for (var i = 0; i < hrefParams.length; i++) {
			if (hrefParams[i].name.substr(0, 5) == 'PAGEN') {
				pagen = {name: hrefParams[i].name, value: hrefParams[i].value};
			}
			data.push({name: "_" + hrefParams[i].name, value: hrefParams[i].value});
		}
		var filter_title = '';
		getParams = refreshParams(getParams);
		for (var i = 0; i < getParams.length; i++) {
			data.push({name: "_" + getParams[i].name, value: getParams[i].value});
			if (getParams[i].name == 'filter_title') {
				filter_title = getParams[i].value;
				getParams.splice(i, 1);
			}
		}
		if (typeof(filterParams) != 'undefined' && filterParams.length > 0) {
			data.push({name: "arFilterParams", value: filterParams});
		}
		if (!isCustomFilter) {
			var filterUrl = '?' + $.param(getParams);
			if (filterUrl != '?') {
				setLocation(filterUrl);
			}
			if (filter_title.length > 0) {
				addParameter('filter_title', filter_title);
			}
			addParameter('catalog_sort', getCookie('CATALOG_SORT', 'NAME'));
			addParameter('list_count', getCookie('LIST_COUNT', 16));
			addParameter('view_mode', getCookie('VIEW_MODE', 'grid_view'));
			if (typeof(pagen.name) != 'undefined' && typeof(pagen.value) != 'undefined') {
				addParameter(pagen.name, pagen.value);
			} else {
				removeParameter('PAGEN_1');
			}
			if (isFilterAjax) {
				addParameter('set_filter', 'Y');
			}
		}
		// SITE_TEMPLATE_PATH/js/script_after.js
		$catalog_sectionList.setAjaxLoading();
		var $wishListPersonal = $('#wish-list');
		if ($wishListPersonal.length > 0) {
			var ajaxFile = 'wishList.php';
			data.push({name:'rz_ajax', value:'y'});
			data.push({name:'ajax', value:$wishListPersonal.data('ajax')});
			data.push({name: 'WISH_LIST', value: rz.WISH_LIST.length});
		}
		else
			var ajaxFile = 'catalog_sectionList.php';

		$(document).find('.timer').countdown('destroy');

		$.ajax({
			type: "POST",
			url: rz.AJAX_DIR + ajaxFile,
			data: data,
			success: function (msg) {
				$catalog_sectionList.html(msg);
				$catalog_sectionList.reSpin();
				$catalog_sectionList.reStyler();
				$catalog_sectionList.reRate();
				if(typeof $.fn.initStorePopover == 'function') {
					$catalog_sectionList.initStorePopover();
				}
				// SITE_TEMPLATE_PATH/js/script_after.js
				$('.catalog-description').heightControl();
				$catalog_sectionList.reTimers();
				$('select.offer_select').trigger('change');
				$catalog_sectionList.stopAjaxLoading();
			}
		})
	}
});