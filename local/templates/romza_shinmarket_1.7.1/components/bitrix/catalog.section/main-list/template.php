<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @var string $strElementEdit */
/** @var string $strElementDelete */
/** @var array $arElementDeleteParams */
/** @var array $arSkuTemplate */
use \Yenisite\Core\Tools;
include_once $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/tools/include_module.php';
$isAjax = Tools::isAjax();
$arParams['MESS_BTN_BUY'] = trim($arParams['MESS_BTN_BUY']);
if (strlen($arParams['MESS_BTN_BUY']) == 0) {
	$arParams['MESS_BTN_BUY'] = GetMessage('CT_BCS_TPL_MESS_BTN_BUY');
}
if (method_exists($this, 'setFrameMode')) $this->setFrameMode(true); ?>
<?
global $APPLICATION;
$title = $arResult['NAME'];
if(isset($arResult['IPROPERTY_VALUES']['SECTION_META_TITLE'])) {
	$title = $arResult['IPROPERTY_VALUES']['SECTION_META_TITLE'];
}
if(isset($arResult['IPROPERTY_VALUES']['SECTION_PAGE_TITLE'])) {
	$title = $arResult['IPROPERTY_VALUES']['SECTION_PAGE_TITLE'];
}
if (isset($arParams['CUSTOM_TITLE']) && strlen($arParams['CUSTOM_TITLE']) > 0) {
	$title = $arParams['CUSTOM_TITLE'];
}
?>
<?if(!$isAjax):?>
	<div class="<?if(!$arParams['PERSONAL_PAGE']):?>goods_list_block<?else:?>goods_list <?= $arParams['VIEW_MODE'] ?><?endif?><?if(isset($arParams['CLASS_ADD'])):?> <?=$arParams['CLASS_ADD']?><?endif?>" id="catalog_sectionList" data-arparams='<?=Tools::GetEncodedArParams($arParams)?>' data-template='<?=$templateName?>'
		data-url="<?=$arResult['SECTION_PAGE_URL']?>" data-issku="<?=$arResult["IS_SKU"]?>">
<?endif?>
<?if (!empty($arResult['ITEMS'])) {
	$strElementEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT");
	$strElementDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE");
	$arElementDeleteParams = array("CONFIRM" => GetMessage('CT_BCT_ELEMENT_DELETE_CONFIRM'));
	$viewMode = $arParams['VIEW_MODE'];
	?>
	<?if(!$arParams['PERSONAL_PAGE']):?>
		<div class="goods_list__head catalogue_head">
			<h2><?=$title?></h2>

			<div class="sorting_type_wrapper">
				<?=GetMessage('RZ_SORT_TITLE')?>
				<select class="sorting_type_select" name="sort_section_select" title=""<?if(!empty($arParams['PAGEN_URL'])):?> data-href="<?=$arParams['PAGEN_URL']?>"<?endif?>>
					<option value="NAME"
							<?if($arParams['ELEMENT_SORT_FIELD'] == "NAME"):?>selected<?endif;?>
						><?=GetMessage('RZ_SORT_BY_NAME')?></option>
					<option value="PRICE_ASC"
							<?if(
							(strpos($arParams['ELEMENT_SORT_FIELD'],"CATALOG_PRICE") !== false
								|| $arParams['ELEMENT_SORT_FIELD'] == 'PROPERTY_MINIMUM_PRICE'
								|| $arParams['ELEMENT_SORT_FIELD'] == 'PROPERTY_PRICE_BASE'
							)
							&& $arParams['ELEMENT_SORT_ORDER'] == 'ASC'):?>selected<?endif;?>
						><?=GetMessage('RZ_SORT_BY_PRICE_ASC')?></option>
					<option value="PRICE_DESC"
							<?if(
							(strpos($arParams['ELEMENT_SORT_FIELD'],"CATALOG_PRICE") !== false
								|| $arParams['ELEMENT_SORT_FIELD'] == 'PROPERTY_MINIMUM_PRICE'
								|| $arParams['ELEMENT_SORT_FIELD'] == 'PROPERTY_PRICE_BASE'
							)
							&& $arParams['ELEMENT_SORT_ORDER'] == 'DESC'):?>selected<?endif;?>
						><?=GetMessage('RZ_SORT_BY_PRICE_DESC')?></option>
					<option value="SHOWS"
							<?if($arParams['ELEMENT_SORT_FIELD'] == "SHOWS"):?>selected<?endif;?>
						><?=GetMessage('RZ_SORT_BY_SHOWS')?></option>
				</select>
				<button class="btn btn-primary btn-buy" type="button" id="btn_tableBuy"
					<?if($viewMode == 'table_view'):?>style="display: inline-block"<?endif;?>
					disabled
					><?=GetMessage('CT_BCS_TPL_MESS_BTN_BUY')?> (<span>0</span>)</button>
			</div>
		</div>
		<?include(__DIR__.'/toolbar.php')?>
	<?endif?>
		<?
		$arItems = $arResult['ITEMS'];
		include(__DIR__ . '/item_list.php');
		?>

	<?if(!$arParams['PERSONAL_PAGE']):?>
		<?include(__DIR__.'/toolbar.php')?>

		<div class="main_content__banners">
			<?/*
			<a href="#" class="main_content__banners_item">
				<img src="images/i/banners/banner_5.jpg" alt="">
			</a>
			<a href="#" class="main_content__banners_item">
				<img src="images/i/banners/banner_6.jpg" alt="">
			</a>
			*/?>
		</div>
	<?endif?>
<?} else { //!empty($arResult['ITEMS']?>
		<div class="goods_list__head catalogue_head">
			<h2><?=$title?></h2>

			<h3><?=GetMessage('RZ_NO_ITEM_FILTER')?></h3>
		</div>
<?}?>
<?if(!$isAjax):?>
	</div>
<?endif?>
