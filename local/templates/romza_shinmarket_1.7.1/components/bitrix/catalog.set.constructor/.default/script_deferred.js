$(function ($) {
	$('body').on('click', '.benefits_goods__discount .buy_button_wrapper a', function (e) {
		var $this = $(this);
		e.preventDefault();
		$this.setAjaxLoading();

		var requests = [];
		var arId = $this.data('id').split(',');
		$.each(arId, function (i, el) {
			requests.push(doAjaxAddToCartSimple(el));
		});
		var $imgs = $this.closest('.benefits_goods_block').find('.goods_item img'),
			$miniCart = $('#cartLine_Refresh');

		$.when.apply($, requests).done(function () {
			if ($imgs.length > 0) {
				$imgs.each(function () {
					var $img = $(this),
						$clone = $img.clone()
							.offset({
								top: $img.offset().top,
								left: $img.offset().left
							})
							.css({
								'opacity': '0.5',
								'position': 'absolute',
								'height': $img.height() + 'px',
								'width': $img.width() + 'px',
								'z-index': '99999'
							})
							.appendTo($('body'))
							.animate({
								'top': $miniCart.offset().top + 10,
								'left': $miniCart.offset().left + 10,
								'width': 75,
								'height': 75
							}, 1000);
					$clone.animate({
						'width': 0,
						'height': 0
					}, function () {
						$(this).detach()
					});
				});
			}
			$(document).trigger('cartLine_Refresh');
			$this.stopAjaxLoading();
			//$this.text(BX.message('RZ_CATALOG_LIST_IN_CART'));
		});
	});
});