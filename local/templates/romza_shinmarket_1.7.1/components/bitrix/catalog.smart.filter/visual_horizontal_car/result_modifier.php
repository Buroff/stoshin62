<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use \Yenisite\Shinmarket\ManagerCalc;
use \Yenisite\Core\Ajax;

$bAjax = Ajax::isAjax();

if (isset($arParams['SECTION_ID']) && intval($arParams['SECTION_ID']) > 0) {
	$dbSection = CIBlockSection::GetByID($arParams['SECTION_ID']);
	$dbSection->SetUrlTemplates();
	$arSection = $dbSection->GetNext();
	$arResult['FORM_ACTION'] = $arSection['SECTION_PAGE_URL'];
}
$arPropsID = array();
foreach ($arResult['ITEMS'] as $key => &$arItem) {
	$arPropsID[$arItem['ID']] = $key;
	if ($arItem['PROPERTY_TYPE'] == "E") {
		foreach ($arItem['VALUES'] as $id => &$arValue) {
			$arValue['ICON_CLASS'] = \Yenisite\Core\Catalog::getIconClassFromPropId($arItem['ID'], $id);
		}
		unset($arValue);
	}
}
unset($arItem);

$dbProp = CIBlockProperty::GetList(array(), array(
	'ACTIVE' => 'Y',
	'IBLOCK_ID' => $arParams['IBLOCK_ID']
));

while ($arProp = $dbProp->Fetch()) {
	if (!isset($arPropsID[$arProp['ID']])) continue;
	if (isset($arProp['HINT']) && strlen($arProp['HINT']) > 0) {
		$decoded = json_decode($arProp['HINT'], true);
		if (isset($decoded)) {
			$arResult['ITEMS'][$arPropsID[$arProp['ID']]]['HINT'] = $decoded;
		}
	}
}

if (CRZShinmarketSettings::isTiersSolution() && ManagerCalc::tablesExist() && !$bAjax){
    $arResult['VENDORS'] = ManagerCalc::processOnlyVendors();
    $arResult['OTHERS_PROPS'] = ManagerCalc::processItemsByVendor($arResult['VENDORS'][0]);
    $arOptionsBDIds = CRZShinmarket::getPropsIDOfTiersWheels();
    CRZShinmarket::setValuesForTXProps($arResult['OTHERS_PROPS']['MODELS'][0],$arOptionsBDIds);
    CRZShinmarket::filterValuesOfFilterByTXProps($arResult,$arOptionsBDIds);
}



