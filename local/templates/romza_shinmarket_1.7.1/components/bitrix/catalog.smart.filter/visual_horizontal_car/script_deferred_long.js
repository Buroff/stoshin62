(typeof(jQuery) != 'undefined')
&& (function ($) {
	var $rangeSlider = $(".js-price-range-slider"),
		cars_selects = {'mark': '[name="tiers_makr_filter"]', 'model': '[name="tiers_model_filter"]','year':'[name="tiers_year_filter"]','modif':'[name="tiers_modif_filter"]'};
	$rangeSlider.each(function () {
		var $this = $(this),
			$parent = $this.closest('.seach_form__inner_price'),
			$priceTo = $parent.find('.price_to:eq(0)'),
			$priceFrom = $parent.find('.price_from:eq(0)'),
			priceMinVal = Math.floor(parseFloat($priceFrom.data('val'))),
			priceMaxVal = Math.ceil(parseFloat($priceTo.data('val'))),
			priceToVal = $priceTo.val(),
			priceFromVal = $priceFrom.val();
		if (priceToVal.length == 0) {
			priceToVal = priceMaxVal; 
		}
		if (priceFromVal.length == 0) {
			priceFromVal = priceMinVal;
		}
		$this.noUiSlider({
			start: [priceFromVal, priceToVal],
			connect: true,
			step: 1,
			range: {
				'min': priceMinVal,
				'max': priceMaxVal
			},
			format: wNumb({
				decimals: 0
			})
		});
		$this.on({
			set: function (e, vals) {
				$priceFrom.val('');
				$priceTo.val('');
			},
			change: function (e, vals) {
				if ($priceFrom.val().length == 0 || $priceTo.val().length == 0) {
					$priceFrom.val(vals[0]);
					$priceTo.val(vals[1]);
					$priceTo.trigger('change');
				}
			}
		});

		var triggerSliderInput = function (val) {
			var $this = $(this);
			if ($this.val() != val && $this.val().length > 0) {
			$this.val(val);
				if (sliderTrigger != null) {
					clearTimeout(sliderTrigger);
				}
				sliderTrigger = setTimeout(function () {
					$this.trigger('change');
				}, 700);
			}
		};
		$this.Link('lower').to($priceFrom, function (val) {
			triggerSliderInput.bind(this, val);
		});
		$this.Link('upper').to($priceTo, function (val) {
			triggerSliderInput.bind(this, val);
		});

		$this.Link('lower').to('-inline-<div class="tooltip"></div>', function (value) {
			$(this).html('<span class="tooltip_inner">' + value + '</span>');
		});

		$this.Link('upper').to('-inline-<div class="tooltip"></div>', function (value) {
			$(this).html('<span class="tooltip_inner">' + value + '</span>');
		});
	});



	var $sectionFilters = $('.filterHorizontal');

    $.each(cars_selects, function (key, val) {
        cars_selects[key] = $sectionFilters.find(val);
    });

// --- $sectionFilters     

	$sectionFilters.on('change', 'input', function (e) {
		var $sectionFilter = $(this).closest('form.filterHorizontal');
		var rnd = $sectionFilter.data('rnd');
		var data = $sectionFilter.serializeArray(),
            carId = $sectionFilter.find('[name="car-id"]').val();
		data.push({name: 'arParams', value: $sectionFilter.data('arparams')});
		data.push({name: 'car_id', value: carId});
		$sectionFilter.setAjaxLoading();

		// ---
		console.log('--- sectionFilters  ---');
		console.log(rz.AJAX_DIR + "catalog_Filter.php");
		console.log(data);

		$.ajax({
			url: rz.AJAX_DIR + "catalog_Filter.php",
			data: data,
			dataType: "json",
			success: function (json) {
				rz.FILTER_AJAX = [];
				$.each(json.ITEMS, function (key, val) {
					$.each(val.VALUES, function (valKey, valVal) {
						ajaxValuePrepare(valVal, rnd);
					});
				});
				if (json.ELEMENT_COUNT) {
                    setHtmlForFilterCnt($sectionFilter,json.ELEMENT_COUNT);
                }
				$sectionFilter.stopAjaxLoading();
				$(document).trigger('catalog_applyFilter');
			}
		})
	});


	$sectionFilters.on('click', '.form_reset_button', function (e) {
		e.preventDefault();
		var $sectionFilter = $(this).closest('form.filterHorizontal');
		$sectionFilter[0].reset();
		var rnd = $sectionFilter.data('rnd');
		var $jsSlider = $sectionFilter.find('.js-price-range-slider');
		var priceToVal = $sectionFilter.find('.price_to:eq(0)').val(),
			priceFromVal = $sectionFilter.find('.price_from:eq(0)').val();
		$jsSlider.val([priceFromVal,priceToVal]);
		$sectionFilter.find('input:visible:eq(0)').trigger('change');
	});



// --- ajaxValuePrepare
	var ajaxValuePrepare = function (val, rnd) {
		var $curItem = $('#' + rnd + val.CONTROL_ID);
		var $itemStyler = $('#' + rnd + val.CONTROL_ID + "-styler");
		var lastsymb = val.CONTROL_NAME.substr(-3);
		var isNum = false;
		if (lastsymb == 'MAX' || lastsymb == "MIN") {
			isNum = true;
		}
		if ('DISABLED' in val && val.DISABLED == 1) {
			if ($itemStyler.length > 0) {
				$itemStyler.addClass('disabled');
			}
			$curItem.attr('disabled', 'disabled');
		} else {
			if ($itemStyler.length > 0) {
				$itemStyler.removeClass('disabled');
			}
			$curItem.removeAttr('disabled');
		}
	};



    cars_selects['mark'].on('change', function () {
        var vendor = $(this).find('option:selected').text();
        doAjaxGetValsOfCars('by_vendor',$(this),vendor);
    });
    cars_selects['model'].on('change', function () {
        var $this = $(this),
            vendor = $this.getSelOptFromForm(cars_selects['mark']).text(),
            model = $this.find('option:selected').text();
        doAjaxGetValsOfCars('by_vendor_model',$(this),vendor,model);
    });
    cars_selects['year'].on('change', function () {
        var $this = $(this),
            vendor = $this.getSelOptFromForm(cars_selects['mark']).text(),
            model = $this.getSelOptFromForm(cars_selects['model']).text(),
            year = $this.find('option:selected').text();
        doAjaxGetValsOfCars('by_vendor_model_year',$(this),vendor,model,year);
    });
    cars_selects['modif'].on('change', function () {
        var $this = $(this),
            vendor = $this.getSelOptFromForm(cars_selects['mark']).text(),
            model = $this.getSelOptFromForm(cars_selects['model']).text(),
            year = $this.getSelOptFromForm(cars_selects['year']).text(),
            modif = $this.find('option:selected').text();
        doAjaxGetValsOfCars('by_vendor_model_year_modif',$(this),vendor,model,year,modif);
    });



// --- doAjaxGetValsOfCars
    var doAjaxGetValsOfCars = function(action,$this,strVendor,strModel,strYear,strModif){
        var data = {},
            $container = $this.closest('.filterHorizontal'),
            bHasInput = $container.find('[name="car-id"]').length ? true : false,
            $inputCar = bHasInput ? $container.find('[name="car-id"]') : $('<input type="hidden" name="car-id"/>');

        if (typeof strVendor != 'undefined'){
            data['vendor'] = strVendor;
        }
        if (typeof strModel != 'undefined'){
            data['model'] = strModel;
        }
        if (typeof strYear != 'undefined'){
            data['year'] = strYear;
        }
        if (typeof strModif != 'undefined'){
            data['modif'] = strModif;
        }

        data['action_get_info'] = action;
        $container.setAjaxLoading();

        

        console.log('--- doAjaxGetValsOfCars ---');
        console.log(rz.AJAX_DIR + 'tiers_calc.php');
        console.log(data);

        $.ajax({
            url: rz.AJAX_DIR + 'tiers_calc.php',
            data:data,
            dataType:'json',
            method: 'POST',
            success: function(result){
                var modif,years,carID,models,objData;
                if (!result.data_base.MODELS){
                    objData = result.data_base;
                }else{
                    models = getModels(result.data_base.MODELS)
                    objData = result.data_base.MODELS[0];
                }
                modif = objData.MODIFICATIONS;
                years = objData.YEARS;
                carID = objData.SELECTED_CAR_ID;
                $inputCar.val(carID);
                if (!bHasInput){
                    $container.append($inputCar);
                }
                setHtmlOptions(modif,cars_selects['modif']);
                setHtmlOptions(years,cars_selects['year']);
                setHtmlOptions(models,cars_selects['model']);
                $container.find('select').trigger('refresh');
                $container.stopAjaxLoading();
                $container.find('input').eq(0).trigger('change');
            }
        });
    };

})(jQuery);