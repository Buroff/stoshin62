<? foreach ($arItems as $arStore): ?>
	<? if ($arParams['SHOW_EMPTY_STORE'] == 'N' && intval($arStore['AMOUNT']) == 0) continue; ?>
	<div class="RzStore__item">
		<? if (!empty($arStore['TITLE'])): ?>
			<span class="RzStore__popupUrl" data-title="<?= $arStore['TITLE'] ?>"><?= trim($arStore['TITLE']) ?></span>&nbsp;
		<? endif ?>
		<?
		list($class, $title,$style) = rzSwitchAmount($arStore['AMOUNT'],$arParams);
		?>
		<? // painted columns are set by class on parent: many / average / few ?>
		<div class="RzStore__popupBody">
			<div class="Store_AVG">
				<? include 'view_mode.php' ?>
			</div>
			<? if (!empty($arStore['PHONE'])): ?>
				<p><b><?=GetMessage("RZ_TELEFON")?>:</b>&nbsp;<?= $arStore['PHONE'] ?></p>
			<? endif ?>
			<? if (!empty($arStore['SCHEDULE'])): ?>
				<p><b><?=GetMessage("RZ_REZHIM_RABOTI")?>:</b>&nbsp;<?= $arStore['SCHEDULE'] ?></p>
			<? endif ?>
			<? if (!empty($arStore['EMAIL'])): ?>
				<p><b>Email:</b>&nbsp;<a href="mailto:<?= $arStore['EMAIL'] ?>"><?= $arStore['EMAIL'] ?></a></p>
			<? endif ?>
			<?
			if (!empty($arStore['COORDINATES']['GPS_N']) && !empty($arStore['COORDINATES']['GPS_S'])): ?>
				<?
				$mapHeight = '';
				if(intval($arParams['WIDTH']) > 0) {
					$mapHeight = 'height: ' . ceil($arParams['WIDTH'] / 2) . 'px;';
				} else {
					$mapHeight = 'min-height : 100px';
				}
				?>
				<div class="store_map" data-gpsn="<?= $arStore['COORDINATES']['GPS_N']?>" data-gpss="<?= $arStore['COORDINATES']['GPS_S']?>"
					 style="width: 100%; <?=$mapHeight?>">
				</div>
			<? endif ?>
			<? if (intval($arStore['IMAGE_ID']) > 0): ?>
				<div class="RzStore__imgWrapper">
					<img class="img img-responsive" src="<?= CRZ\StoreAmount\Resize::GetResizedImg($arStore['IMAGE_ID'], array('WIDTH' => $arParams['IMG_WIDTH'], 'HEIGHT' => $arParams['IMG_HEIGHT'], 'SET_ID' => intval($arParams['RESIZER_SET']))) ?>"/>
				</div>
			<? endif ?>
			<? if (!empty($arStore['DESCRIPTION'])): ?>
				<p class="help-block"><?= $arStore['DESCRIPTION'] ?></p>
			<? endif ?>
		</div>
	</div>
<? endforeach ?>