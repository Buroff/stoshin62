<?php
$MESS['RZ_GROUP_VISUAL'] = 'Отображение';
$MESS['RZ_WIDTH'] = 'Ширина окна';
$MESS['RZ_WIDTH_TIP'] = 'Может быть использованно значение auto';
$MESS['MANY_VAL'] = 'Количество больше которого много';
$MESS['AVERAGE_VAL'] = 'Количество больше которого досаточно';
$MESS['MANY_NAME'] = 'Текст при много';
$MESS['AVERAGE_NAME'] = 'Текст при достаточно';
$MESS['FEW_NAME'] = 'Текст при мало';
$MESS['NONE_NAME'] = 'Текст при отсутствии';
$MESS['MANY_NAME_DEFAULT'] = 'много';
$MESS['AVERAGE_NAME_DEFAULT'] = 'достаточно';
$MESS['FEW_NAME_DEFAULT'] = 'мало';
$MESS['NONE_NAME_DEFAULT'] = 'Нет в наличии';
$MESS['ONE_POPUP'] = 'Один попап на все склады';
$MESS['VIEW_MODE'] = 'Режим отображения';
$MESS['VIEW_MODE_COLS'] = 'Цветные столбы';
$MESS['VIEW_MODE_TEXT'] = 'Текст';
$MESS['VIEW_MODE_NUMBS'] = 'Число';
$MESS['RZ_RESIZER_SET'] = 'Набор ресайзера для изображений';
$MESS['RZ_IMG_WIDTH'] = 'Ширина картинки';
$MESS['RZ_IMG_HEIGHT'] = 'Высота картинки';

$MESS['RZ_GROUP_COLOR'] = 'Цвета';
$MESS['RZ_COLOR_NONE_BG'] = 'Фон при отсутствии';
$MESS['RZ_COLOR_NONE_TEXT'] = 'Цвет шрифта отсутствии';
$MESS['RZ_COLOR_MANY_BG'] = 'Фон при много';
$MESS['RZ_COLOR_MANY_TEXT'] = 'Цвет текста при много';
$MESS['RZ_COLOR_AVERAGE_BG'] = 'Фон при достаточно';
$MESS['RZ_COLOR_AVERAGE_TEXT'] = 'Цвет текста при достаточно';
$MESS['RZ_COLOR_FEW_BG'] = 'Фон при мало';
$MESS['RZ_COLOR_FEW_TEXT'] = 'Цвет текста при мало';

$MESS['ONLY_NAME_IN_LIST'] = 'показывать только наличие \ нет';
$MESS['HAS_NAME'] = 'Текст при в наличии';
$MESS['HAS_NAME_DEFAULT'] = 'В наличии';

$MESS['POPUP_TRIGGER'] = 'Выводить попап';
$MESS['POPUP_TRIGGER_CLICK'] = 'По клику';
$MESS['POPUP_TRIGGER_HOVER'] = 'При наведении';


