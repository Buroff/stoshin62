<?
$MESS["RZ_NET_INFORMATCII_PO_OSTATKAM"] = "Нет информации по остаткам";
$MESS["RZ_REZHIM_RABOTI"] = "Режим работы";
$MESS["RZ_TELEFON"] = "Телефон(ы)";
$MESS["MANY_NAME_DEFAULT"] = "много";
$MESS["AVERAGE_NAME_DEFAULT"] = "достаточно";
$MESS["FEW_NAME_DEFAULT"] = "мало";
$MESS["NONE_NAME_DEFAULT"] = "Нет в наличии";
$MESS["HAS_NAME_DEFAULT"] = "В наличии";
$MESS["RZ_STORE_QUANTITY"] = "Количество на сладах:";
$MESS["RZ_MODULE_NOT_INSTALLED"] = "Модуль \"Остатки на складах\" (yenisite.storeamount) не установлен";
