<? if ($arParams['VIEW_MODE'] == 'COLS'): ?>
	<div class="RzStore__indicator cols <?= $class ?>" title="<?= $title ?>" style="<?=$style?>">
		<div class="none many average few"></div>
		<div class="none many average"></div>
		<div class="none many"></div>
	</div>
<? elseif ($arParams['VIEW_MODE'] == 'TEXT'): ?>
	<div class="RzStore__indicator text <?= $class ?>"><span class="<?= $class ?>"><?= $title ?></span></div>
<? elseif ($arParams['VIEW_MODE'] == 'NUMB'): ?>
	<div class="RzStore__indicator text <?= $class ?>"><span class="<?= $class ?>"><?= $arStore['AMOUNT'] ?></span></div>
<? endif ?>