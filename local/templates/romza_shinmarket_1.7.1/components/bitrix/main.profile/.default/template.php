<?
/**
 * @global CMain $APPLICATION
 * @param array $arParams
 * @param array $arResult
 */
use Yenisite\Core\Resize;
use \Yenisite\Core\Tools;

\Bitrix\Main\Localization\Loc::loadMessages($_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH . '/lang/'.LANGUAGE_ID.'/header.php');
$pathToRules = COption::GetOptionString(CRZShinmarketSettings::getModuleId(),'path_tu_rules_privacy',SITE_DIR.'personal/rules/personal_data.php');
$rand = $this->randString();
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
?>
<form class="form-horizontal form-settings profile_settings modal-form"
		  enctype="multipart/form-data"
		  action="<?=$arResult["FORM_TARGET"]?>" role="form" method="post" id="profile_settings_form">
        <input type="hidden" name="privacy_policy" value="N"/>
		<?=$arResult["BX_SESSION_CHECK"]?>
		<input type="hidden" name="lang" value="<?=LANG?>" />
		<input type="hidden" name="ID" value=<?=$arResult["ID"]?> />
		<div class="form-group">
			<? ShowError($arResult["strProfileError"]);?>
			<?
			if ($arResult['DATA_SAVED'] == 'Y')
				ShowNote(GetMessage('PROFILE_DATA_SAVED'));
			?>
		</div>
		<div class="form-group">
			<label for="settings_profile_name" class="control-label"><?=GetMessage('NAME')?></label>
			<div class="control-wrapper">
				<input id="settings_profile_name" type="text" name="NAME" value="<?=$arResult["arUser"]["NAME"]?>" class="form-control">
			</div>
		</div>
		<div class="form-group">
			<label for="settings_profile_lastname" class="control-label"><?=GetMessage('LAST_NAME')?></label>
			<div class="control-wrapper">
				<input id="settings_profile_lastname" type="text" name="LAST_NAME" value="<?=$arResult["arUser"]["LAST_NAME"]?>" class="form-control">
			</div>
		</div>
		<div class="form-group">
			<label for="settings_profile_secondname" class="control-label"><?=GetMessage('SECOND_NAME')?></label>
			<div class="control-wrapper">
				<input id="settings_profile_secondname" type="text" name="SECOND_NAME" value="<?=$arResult["arUser"]["SECOND_NAME"]?>" class="form-control">
			</div>
		</div>
		<?if($arResult["arUser"]["EXTERNAL_AUTH_ID"] == ''):?>
			<div class="form-group">
				<label for="settings_profile_password" class="control-label"><?=GetMessage('NEW_PASSWORD_REQ')?></label>
				<div class="control-wrapper">
					<input id="settings_profile_password" type="password" name="NEW_PASSWORD" value="" class="form-control password-field" autocomplete="off">
				</div>
			</div>
			<div class="form-group">
				<label for="settings_profile_password" class="control-label"><?=GetMessage('NEW_PASSWORD_CONFIRM')?></label>
				<div class="control-wrapper">
					<input id="settings_profile_password" type="password" name="NEW_PASSWORD_CONFIRM" value="" class="form-control password-field" autocomplete="off">
				</div>
			</div>
		<?endif?>
		<div class="form-group">
			<label for="settings_profile_email" class="control-label"><?=GetMessage('EMAIL')?></label>
			<div class="control-wrapper">
				<input id="settings_profile_email" type="email" name="EMAIL" value="<?=$arResult["arUser"]["EMAIL"]?>" class="form-control">
			</div>
		</div>
		<div class="form-group">
			<label for="settings_profile_email" class="control-label"><?=GetMessage('LOGIN')?></label>
			<div class="control-wrapper">
				<input id="settings_profile_email" type="text" name="LOGIN" value="<?=$arResult["arUser"]["LOGIN"]?>" class="form-control">
			</div>
		</div>
		<div class="form-group">

			<label for="PERSONAL_BIRTHDAY" class="control-label"><?=GetMessage("USER_BIRTHDAY_DT")?> (<?=$arResult["DATE_FORMAT"]?>):</label>
			<div class="control-wrapper calendar-wrapper">
				<?
				$APPLICATION->IncludeComponent(
					'bitrix:main.calendar',
					'',
					array(
						'SHOW_INPUT' => 'Y',
						'FORM_NAME' => 'form1',
						'INPUT_NAME' => 'PERSONAL_BIRTHDAY',
						'INPUT_VALUE' => $arResult["arUser"]["PERSONAL_BIRTHDAY"],
						'SHOW_TIME' => 'N',
						'INPUT_ID' => 'settings_profile_birth'
					),
					null,
					array('HIDE_ICONS' => 'Y')
				);
				?>
			</div>
		</div>
		<div class="form-group">
			<label for="settings_profile_phone" class="control-label"><?=GetMessage('USER_PHONE')?></label>
			<div class="control-wrapper">
				<input id="settings_profile_phone" type="tel" name="PERSONAL_PHONE" class="form-control"
					   value="<?=$arResult["arUser"]["PERSONAL_PHONE"]?>">
			</div>
		</div>
		<div class="form-group">
			<label for="settings_profile_address" class="control-label"><?=GetMessage("RZ_ADRES_DOSTAVKI")?>:</label>
			<div class="control-wrapper">
				<input id="settings_profile_address" type="text" class="form-control">
			</div>
		</div>

		<div class="form-group">
			<label for="settings_profile_photo" class="control-label"><?=GetMessage("USER_PHOTO")?></label>
			<div class="control-wrapper">
				<div class="profile_user__img_wrapper">
					<?if(intval($arResult["arUser"]['PERSONAL_PHOTO']) == 0):?>
						<img class="profile_user__img" src="<?= SITE_TEMPLATE_PATH ?>/images/i/userpic_big.png" alt="">
					<?else:?>
						<img class="profile_user__img"
							 src="<?= Resize::GetResizedImg($arResult["arUser"]['PERSONAL_PHOTO'],
								 array('WIDTH'=> 90, 'HEIGHT' => 90,'SET_ID' => intval($arParams['RESIZER_AVATAR'])))?>"
						>
					<?endif?>
				</div>
				<div class="profile_user__img_settings">
					<div>
						<span class="custom_input_file_wrapper">
							<span class="custom_input_file btn btn-primary"><?=GetMessage("RZ_VIBERITE_FAJL")?></span>
							<input id="settings_profile_photo" data-target="#fileValue"
								   name="PERSONAL_PHOTO" class="custom_input_file_field" type="file">
						</span>
					</div>
					<div id="fileValue"><?=GetMessage("RZ_FAJL_NE_VIBRAN")?></div>
					<label>
						<input type="checkbox" name="PERSONAL_PHOTO_del" title=""/>
						<?=GetMessage("RZ_UDALIT__AVATARKU")?>
					</label>
				</div>
			</div>
        </div>
    <div class="form-group agreement-policy">
        <div class="forms-wrapper">
            <input id="privacy_policy_<?=$rand?>" required type="checkbox" name="privacy_policy" value="Y" <?=$_REQUEST['privacy_policy'] == 'Y' ? ' checked' : ''?>>
            <label for="privacy_policy_<?=$rand?>">
                <?= GetMessage('RZ_ACCEPT') ?> <?=GetMessage('RZ_CONCLUSION',array("#URL#" => Tools::GetConstantUrl($pathToRules)))?>
            </label>
        </div>
    </div>
        <div class="form-group form-submit">
            <input class="btn btn-primary" type="submit" name="save" value="<?=(($arResult["ID"]>0) ? GetMessage("MAIN_SAVE") : GetMessage("MAIN_ADD"))?>">
            <input class="btn btn-link" type="reset" value="<?=GetMessage('MAIN_RESET');?>">
        </div>
	</form>
	<?/*?>
	<div class="form-group">
		<span class="control-label"><?=GetMessage("RZ_SOTCIAL_NIE_AKKAUNTI")?>:</span>
		<div class="control-wrapper">
			<ul class="share_attached_list">
				<li class="share_attached__item">
					<a class="social_icon sprite sprite-vk" href="#"></a>
											<span class="delete_item">
												<span class="icon icon_delete"></span>
												<span class="inner"><?=GetMessage("RZ_OTVYAZAT_")?></span>
											</span>
				</li>
				<li class="share_attached__item">
					<a class="social_icon sprite sprite-ok" href="#"></a>
											<span class="delete_item">
												<span class="icon icon_delete"></span>
												<span class="inner"><?=GetMessage("RZ_OTVYAZAT_")?></span>
											</span>
				</li>
			</ul>
		</div>
	</div>
	<div class="form-group">
		<span class="control-label"><?=GetMessage("RZ_PRIVYAZAT__AKKAUNT")?>:</span>
		<div class="control-wrapper">
			<div class="socials_list">
				<a class="social_icon sprite sprite-twitter" href="#"></a>
				<a class="social_icon sprite sprite-vk" href="#"></a>
				<a class="social_icon sprite sprite-ok" href="#"></a>
				<a class="social_icon sprite sprite-fb" href="#"></a>
				<a class="social_icon sprite sprite-yandex" href="#"></a>
			</div>
		</div>
	</div>
	<?*/?>
	<? if($arResult["SOCSERV_ENABLED"]): ?>
		<div class="form-horizontal form-settings profile_settings">
		<?$APPLICATION->IncludeComponent("bitrix:socserv.auth.split", ".default", array(
			"SHOW_PROFILES" => "Y",
			"ALLOW_DELETE" => "Y"
		),
			false
		);
		?>
		</div>
	<? endif ?>

<form method="post" name="form1" action="<?=$arResult["FORM_TARGET"]?>" enctype="multipart/form-data">
	<?// ********************* User properties ***************************************************?>
	<?if($arResult["USER_PROPERTIES"]["SHOW"] == "Y"):?>
	<div class="profile-link profile-user-div-link"><a title="<?=GetMessage("USER_SHOW_HIDE")?>" href="javascript:void(0)" onclick="SectionClick('user_properties')"><?=strlen(trim($arParams["USER_PROPERTY_NAME"])) > 0 ? $arParams["USER_PROPERTY_NAME"] : GetMessage("USER_TYPE_EDIT_TAB")?></a></div>
	<div id="user_div_user_properties" class="profile-block-<?=strpos($arResult["opened"], "user_properties") === false ? "hidden" : "shown"?>">
	<table class="data-table profile-table">
		<thead>
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
		</thead>
		<tbody>
		<?$first = true;?>
		<?foreach ($arResult["USER_PROPERTIES"]["DATA"] as $FIELD_NAME => $arUserField):?>
		<tr><td class="field-name">
			<?if ($arUserField["MANDATORY"]=="Y"):?>
				<span class="starrequired">*</span>
			<?endif;?>
			<?=$arUserField["EDIT_FORM_LABEL"]?>:</td><td class="field-value">
				<?$APPLICATION->IncludeComponent(
					"bitrix:system.field.edit",
					$arUserField["USER_TYPE"]["USER_TYPE_ID"],
					array("bVarsFromForm" => $arResult["bVarsFromForm"], "arUserField" => $arUserField), null, array("HIDE_ICONS"=>"Y"));?></td></tr>
		<?endforeach;?>
		</tbody>
	</table>
	</div>
	<?endif;?>
	<?// ******************** /User properties ***************************************************?>
</form>

</div>