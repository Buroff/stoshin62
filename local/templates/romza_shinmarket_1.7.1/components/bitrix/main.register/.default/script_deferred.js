$(function ($) {
	var $registerModal = $('#register_modalForm'),
		loginStr = 'input[name="REGISTER[LOGIN]"]',
		passStr = 'input[name="REGISTER[PASSWORD]"]',
		passChqStr = 'input[name="REGISTER[CONFIRM_PASSWORD]"]',
		$body = $('body');

	$body.on('change', '#register_modalForm .generate-pass input', function (e) {
		var $this = $(this);
		if ($this.is(':checked')) {
			refreshForm();
		}
	});
	$body.on('submit', '#register_modalForm', function (e) {
		var $this = $(this),
			data = $this.find('form').serializeArray();
		if (!$this.checkFields()) {
			return false;
		}
		e.preventDefault();
		data.push({name: "arParams", value: $this.data('arparams')});
		data.push({name: "template", value: $this.data('template').toString()});
		data.push({name: "register_submit_button", value: 'Y'});
		$this.setAjaxLoading();
		$.ajax({
			type: "POST",
			url: rz.AJAX_DIR + "register_modalForm.php",
			data: data,
			success: function (msg) {
				$this.html(msg);
				// SITE_TEMPLATE_PATH/js/script_after.js
				$this.reStyler();
				refreshForm();
                $this.refreshForm();
				$this.stopAjaxLoading();
			}
		})
	});
	$body.on('change', '.form-group-agreement input[type="checkbox"]', function () {
		var $this = $(this),
			$parent = $this.closest('.form-group-agreement');
		if ($this.is(':checked')) {
			$parent.removeClass('has-error');
		} else {
			$parent.addClass('has-error');
		}

	});

	var refreshForm = function () {
		var $registerForm = $registerModal.find('form');
		var data = $registerForm.serializeArray(),
			pass_gen = false;
		$.each(data, function (i, el) {
			if (el.name == 'pass_gen') {
				el.value = el.value | 0;
				if (el.value > 0) {
					pass_gen = true;
				}
				return false;
			}
		});
		var $fields = $registerForm.find(loginStr + ', ' + passStr + ', ' + passChqStr);
		if (pass_gen) {
			$fields.closest('.form-group').hide().removeClass('required');
		} else {
			$fields.closest('.form-group').show().addClass('required');
		}
	};
	refreshForm();
});