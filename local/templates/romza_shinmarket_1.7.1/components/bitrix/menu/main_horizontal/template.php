<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?if (method_exists($this, 'setFrameMode')) $this->setFrameMode(true); ?>
		<nav class="mainmenu-wrap dropdown <?if($arParams['VIEW_MODE'] == 'horizontal'):?>w-horizontal<?endif?>" id="mainmenu-wrap">
			<button type="button" class="btn btn-mainmenu-toggle btn-primary" data-toggle="dropdown">
				<?=GetMessage('RZ_CATALOG')?> <span class="hidden-sm"><?=GetMessage('RZ_OF_GOODS')?></span>
				<span class="caret"></span>
			</button>
			<div class="mainmenu-lvl0 dropdown-menu" id="mainmenu-lvl0">
				<ul class="mainmenu-lvl0-list" id="mainmenu-lvl0-main">
				<?$hits = false;?>
				<? foreach ($arResult['TREE'] as $index => &$itemLvl0): ?>
					<? if ($index === 'HITS') continue ?>
						<?$text = $itemLvl0['TEXT']?>
						<li class="mm0-item<?=(empty($itemLvl0['CHILDREN'])) ? '' : ' has-submenu'?>
								<?=($itemLvl0['SELECTED']) ? ' active' : ''?>">
							<?/* if ($itemLvl0['SELECTED']):?>
								<span class="mm0-link"><?=$text?></span>
							<? else: ?>
								<a href="<?=$itemLvl0['LINK']?>" class="mm0-link"><?=$text?><span class="badge"></span></a>
							<? endif */?>

								<a href="<?=$itemLvl0['LINK']?>" class="mm0-link"><?=$text?><span class="badge"></span></a>

							<? if (!empty($itemLvl0['CHILDREN'])):?>
								<div class="mm-submenu mainmenu-lvl1">
									<div class="m1-header"><?=$itemLvl0['TEXT']?></div>
									<ul class="mainmenu-lvl1-list">
										<? foreach($itemLvl0['CHILDREN'] as $itemLvl1):
											$quan = empty($itemLvl1['PARAMS']['ELEMENT_CNT']) ? '' : '<sup>' . $itemLvl1['PARAMS']['ELEMENT_CNT'] . '</sup>';
											$text = $itemLvl1['TEXT'] . $quan;?>
												<li class="mm1-item<?=(empty($itemLvl1['CHILDREN'])) ? '' : ' has-submenu'?>
														<?=($itemLvl1['SELECTED']) ? ' active' : ''?>">
													<?/* if ($itemLvl1['SELECTED']):?>
														<span class="mm1-link"><?=$text?></span>
													<? else: ?>
														<a href="<?=$itemLvl1['LINK']?>" class="mm1-link link link-black"><?=$text?><span class="badge"></span></a>
													<? endif */?>

													<a href="<?=$itemLvl1['LINK']?>" class="mm1-link link link-black"><?=$text?><span class="badge"></span></a>

													<? if (!empty($itemLvl1['CHILDREN'])):?>
														<ul class="mm-submenu mainmenu-lvl2-list">
															<? foreach($itemLvl1['CHILDREN'] as $itemLvl2):
																$quan = empty($itemLvl2['PARAMS']['ELEMENT_CNT']) ? '' : '<sup>' . $itemLvl2['PARAMS']['ELEMENT_CNT'] . '</sup>';
																$text = $itemLvl2['TEXT'] . $quan;?>
																<?/* if ($itemLvl2['SELECTED']):?>
																<li class="mm2-item active">
																	<span class="mm2-link"><?=$text?></span>
																</li>
															<? else: ?>
																<li class="mm2-item">
																	<a href="<?=$itemLvl2['LINK']?>" class="link link-std mm2-link"><?=$text?></a>
																</li>
															<? endif */?>

																<li class="mm2-item <? if ($itemLvl2['SELECTED']):?>active<?endif?>">
																	<a href="<?=$itemLvl2['LINK']?>" class="link link-std mm2-link"><?=$text?></a>
																</li>
															<? endforeach ?>
														</ul><!-- .mainmenu-lvl2-list -->
													<? endif ?>
												</li>
											<?endforeach?>
										</ul>
									<?$hits = $arResult['HITS'][$index]?>
									<? if (is_array($hits) && !empty($hits)) include 'item_template.php' ?>
									</div>
							<?endif?>
						</li>
				<?endforeach?>
					</ul><!-- .mainmenu-lvl0-list -->
					<a href="#mm0-additional" data-toggle="collapse" class="mainmenu-height-toggle link-black collapsed" id="menuheight-toggle">
						<span class="text when-collapsed">&bullet; &bullet; &bullet;&nbsp;&nbsp;<?=GetMessage('RZ_MORE')?><span class="hidden-sm"> <?=GetMessage('RZ_GOODS_TITLE_SMALL')?></span></span>
						<span class="text when-expanded"><span class="hidden-sm">&bullet; &bullet; &bullet;&nbsp;&nbsp;</span><?=GetMessage('RZ_GOODS_HIDE')?></span>
					</a>
					<ul class="collapse" id="mm0-additional"></ul>
			</div>
		</nav>