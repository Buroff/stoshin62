<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?if (method_exists($this, 'setFrameMode')) $this->setFrameMode(true); ?>
<div class="page_aside__nav__wrapper">
	<nav class="page_aside__nav aside_block">
		<h2><?=GetMessage("RZ_CATALOG_TITLE")?></h2>
<?if (!empty($arResult)):
	$count = 0;
	foreach ($arResult as $arItem) {
		if ($arItem["DEPTH_LEVEL"] == 1)
		++$count;
	}
	$previousLevel = 0;
	$i = 0;
	$level1Counter = 0;
	$level1Middle = floor($count /2 );
	$hits = false;
?>
	<ul class="page_aside__nav_list">
		<? foreach ($arResult as $index => &$arItem): ?>
			<? if ($index === 'HITS') continue ?>
			<? if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel): ?>
				<? if ($arItem["DEPTH_LEVEL"] == 1): ?>
					<?= str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"] - 1)); ?></ul>
					<? if (is_array($hits)) include 'item_template.php' ?>
						</div>
					</li>
					<? else: ?>
						<?= str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"])); ?>
					<? endif ?>
				<? endif ?>
				<?/*if ($arItem['DEPTH_LEVEL'] == 1 && (strpos($arItem["LINK"], 'catalog_') !== false ||
							strpos($arItem["LINK"], SITE_ID . '_') !== false)
					) {
						$word = explode('_', $arItem["LINK"]);
						$word = $word[0];
						$word = explode('/', $word);
						$arItem["LINK"] = str_replace($word[1] . '_', "", $arItem["LINK"]);
						$arItem["LINK"] = str_replace("_", "-", $arItem["LINK"]);
					}*/
						$liClass = ($count < 5) ? 'littlemenucount ' : '';
					?>
			<? if ($arItem["IS_PARENT"]): ?>
				<? if ($arItem["DEPTH_LEVEL"] == 1): ?>
					<?
					++$level1Counter;
					$hits = $arResult['HITS'][$index];
					if($level1Counter > $level1Middle) {
						$liClass.=" right_handed";
					}
					?>
					<li<? if ($liClass): ?> class="<?= $liClass ?>" <? endif; ?>>
						<a href="<?= $arItem["LINK"] ?>">
							<?if(isset($arItem['ICON_CODE']) && strlen($arItem['ICON_CODE']) > 0):?>
								<span class="icon <?=$arItem['ICON_CODE']?>"></span>
							<?endif;?>
							<?= $arItem["TEXT"] ?><?/*
							*/?><? if ($arItem["PARAMS"]["ELEMENT_CNT"] > 0): ?><?/*
								*/?>&nbsp;<span>(<?= $arItem["PARAMS"]["ELEMENT_CNT"] ?>)</span><?/*
							*/?><? endif ?>
						</a>
							<div class="subnav">
								<ul class="subnav_list">
						<? $i = 0; ?>
				<? else: ?>
						<li class="<? if ($arItem["SELECTED"]): ?>item-selected<? endif; ?>">
							<a href="<?= $arItem["LINK"] ?>">
								<?= $arItem["TEXT"] ?><?/*
								*/?><? if ($arItem["PARAMS"]["ELEMENT_CNT"] > 0): ?><?/*
									*/?>&nbsp;(<?= $arItem["PARAMS"]["ELEMENT_CNT"] ?>)
								<? endif ?>
							</a>
							<ul>
				<? endif ?>
			<? else:
				$liClass .= 'no-subnav';
				?>
				<? if ($arItem["PERMISSION"] > "D"): ?>
					<? if ($arItem["DEPTH_LEVEL"] == 1): ?>
						<?
						++$level1Counter;
						$hits = $arResult['HITS'][$index];
						if($level1Counter > $level1Middle) {
							$liClass.=" right_handed";
						}
						?>
						<li class="<?= $liClass ?>">
							<a href="<?= $arItem["LINK"]; ?>">
								<?if(isset($arItem['ICON_CODE']) && strlen($arItem['ICON_CODE']) > 0):?>
									<span class="icon <?=$arItem['ICON_CODE']?>"></span>
								<?endif;?>
								<?= $arItem["TEXT"] ?><?/*
								*/?><? if ($arItem["PARAMS"]["ELEMENT_CNT"] > 0): ?><?/*
									*/?><span>(<?= $arItem["PARAMS"]["ELEMENT_CNT"] ?>)</span><?/*
								*/?><? endif ?>
							</a>
							<? if (is_array($hits)):?>
								<div class="subnav">
									<? include 'item_template.php' ?>
								</div>
							<?endif;?>
						</li>
					<? else: $i++; ?>
						<? if ($arItem["SELECTED"]): ?>
							<li class="<? if ($arItem["SELECTED"]): ?>item-selected<? endif; ?>">
								<?= $arItem["TEXT"] ?><?/*
								*/?><? if ($arItem["PARAMS"]["ELEMENT_CNT"] > 0): ?><?/*
									*/?>&nbsp;(<?= $arItem["PARAMS"]["ELEMENT_CNT"] ?>)<?/*
								*/?><? endif ?>
							</li>
						<? else: ?>
							<li>
								<a href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?><?/*
								*/?><? if ($arItem["PARAMS"]["ELEMENT_CNT"] > 0): ?><?/*
									*/?>&nbsp;(<?= $arItem["PARAMS"]["ELEMENT_CNT"] ?>)<?/*
								*/?><? endif ?>
								</a>
							</li>
						<?endif ?>
					<?endif ?>
				<? else: ?>
					<? if ($arItem["DEPTH_LEVEL"] == 1): ?>
						<? if ($arItem["SELECTED"]): ?>
							<li class="<?= $liClass ?>"><?= $arItem["TEXT"] ?></li>
						<? else: ?>
							<li class="<?= $liClass ?>">
								<a href="" title="<?= GetMessage("MENU_ITEM_ACCESS_DENIED") ?>"><?/*
									*/?><? if ($arItem["PARAMS"]["ELEMENT_CNT"] > 0): ?><?/*
										*/?>&nbsp;(<?= $arItem["PARAMS"]["ELEMENT_CNT"] ?>)<?/*
									*/?><? endif ?>
								</a>

							</li>
						<?endif ?>
					<? else: ?>
						<li>
							<a href="" class="denied" title="<?= GetMessage("MENU_ITEM_ACCESS_DENIED") ?>">
								<?= $arItem["TEXT"] ?><?/*
								*/?><? if ($arItem["PARAMS"]["ELEMENT_CNT"] > 0): ?><?/*
									*/?>&nbsp;(<?= $arItem["PARAMS"]["ELEMENT_CNT"] ?>)<?/*
								*/?><? endif ?>
							</a>
						</li>
					<?endif ?>
				<?endif ?>
			<?endif ?>
				<? $previousLevel = $arItem["DEPTH_LEVEL"]; ?>
		<? endforeach; unset($arItem); ?>
		<? if ($previousLevel > 1): //close last item tags?>
			<?= str_repeat("</ul></li>", ($previousLevel - 2)); ?> </ul>
			<? if (is_array($hits)) include 'item_template.php' ?>
				</div>
					</li>
		<? endif; ?>
				</ul>
<? endif; ?>
	</nav>
</div>

