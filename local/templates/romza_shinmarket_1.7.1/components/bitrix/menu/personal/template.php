<? use Yenisite\Core\Tools;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
include_once $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/tools/include_module.php';
?>
<aside class="profile_aside">
	<?if($USER->IsAuthorized()):?>
		<div class="profile_user_block">
			<div class="profile_user__img_wrapper">
				<img class="profile_user__img" src="<?=CRZShinmarket::getUserAvatar(array('WIDTH' => 66, 'HEIGHT' => 66, 'SET_ID' => intval($arParams['RESIZER_PRODUCT'])))?>" alt="">
			</div>
			<div class="profile_user__info">
				<div class="profile_user__name"><?= $USER->GetFullName() ?></div>
				<div class="profile_user__email">[<?= $USER->GetLogin() ?>]</div>
				<div class="profile_user__logout">(<a href="?logout=yes"><?=GetMessage("RZ_VIJTI")?></a>)</div>
			</div>
		</div>
	<?endif?>
	<? if (!empty($arResult)): ?>
		<?
		$cur_page = $APPLICATION->GetCurPage(true);
		$cur_page_no_index = $APPLICATION->GetCurPage(false);
		?>
		<ul class="profile_nav">
			<?
			foreach ($arResult as $arItem):
				if ($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1)
					continue;?>
				<?
				$link = Tools::GetConstantUrl($arItem["LINK"]);
				if (!$hasSubscribe && strpos($link, 'subscribe/') !== false) {
					continue;
				}
				?>
				<? if ($link == $cur_page || $link == $cur_page_no_index ): ?>
					<li><?= $arItem["TEXT"] ?></li>
				<? else: ?>
					<li>
						<a href="<?= $link?>">
							<?= $arItem["TEXT"] ?>
						</a>
					</li>
				<?endif ?>
			<? endforeach ?>
		</ul>
	<? endif ?>
</aside>
