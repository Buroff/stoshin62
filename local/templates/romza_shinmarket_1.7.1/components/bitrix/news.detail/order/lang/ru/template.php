<?
$MESS['ORDER_TITLE'] = 'Заказ №';
$MESS['ORDER_FROM'] = 'от';
$MESS['ORDER_LIST_LINK'] = 'В список заказов';
$MESS['SPOD_ORDER_PROPERTIES'] = 'Параметры заказа';
$MESS['SPOD_ORDER_PAYMENT'] = 'Параметры доставки и оплаты';
$MESS['SPOD_ORDER_BASKET'] = 'Содержимое заказа';
$MESS['SPOD_NAME'] = 'Наименование';
$MESS['SPOD_PRICE'] = 'Цена';
$MESS['SPOD_QUANTITY'] = 'Количество';
$MESS['SPOD_DEFAULT_MEASURE'] = 'шт';
$MESS['SPOD_GO_BACK'] = 'Вернуться назад';
$MESS['ADD_ORDER'] = 'добавлен';
$MESS['PAY_ORDER'] = 'оплатить';
?>