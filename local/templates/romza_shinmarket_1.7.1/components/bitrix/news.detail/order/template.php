<? use Yenisite\Core\Tools;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$templateData['SITE_ID'] = $arResult['PROPERTIES']['SITE_ID']['VALUE']?:SITE_ID;
$templateData['CREATED_BY'] = $arResult['CREATED_BY'];

?>
<div class="bx_my_order_switch">
	<a class="bx_mo_link" href="<?= Tools::GetConstantUrl($arResult['LIST_PAGE_URL']) ?>"><?= GetMessage('ORDER_LIST_LINK') ?></a>
</div>

<div class="bx_order_list">

	<table class="bx_order_list_table table">
		<thead>
			<tr>
				<td colspan="2">
					<?= GetMessage('ORDER_TITLE') ?> <?= $arResult['ID'] ?>
					<? if (strlen($arResult['DATE_CREATE_FORMATTED'])): ?>
						<?= GetMessage('ORDER_FROM') ?> <?= $arResult['DATE_CREATE_FORMATTED'] ?>
					<? endif ?>
				</td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>
					<?= $arResult['PROPERTIES']['STATUS']['NAME'] ?>:
				</td>
				<td>
					<?= $arResult['PROPERTIES']['STATUS']['VALUE'] ?>
				</td>
			</tr>
		<tr class="last">
			<td>
				<?= $arResult['PROPERTIES']['AMOUNT']['NAME'] ?>:
			</td>
			<td>
				<?= Tools::FormatPrice($arResult['PROPERTIES']['AMOUNT']['VALUE']) ?>
			</td>
		</tr>

		<tr>
			<td colspan="2"><?= GetMessage('SPOD_ORDER_PROPERTIES') ?></td>
		</tr><?

		foreach ($arResult["DISPLAY_PROPERTIES"] as $pid => $arProperty):?>

		<tr<?if ($arProperty['LAST']) echo ' class="last"'?>>
			<td>
				<?= $arProperty["NAME"] ?>:
			</td>
			<td>
				<?if (is_array($arProperty["DISPLAY_VALUE"])):?>
					<?= implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]); ?>
				<?
				elseif (is_array($arProperty['LINK_ELEMENT_VALUE'])):?>
					<?foreach ($arProperty['LINK_ELEMENT_VALUE'] as $arLink):?>
						<?= $arLink['NAME'] ?>
						<?if($arLink['CODE'] == 'robokassa' && ($arResult['PROPERTIES']['STATUS']['VALUE_XML_ID'] == "added" || $arResult['PROPERTIES']['STATUS']['VALUE'] == GetMessage('ADD_ORDER'))):?>
							<div class="UI-element">
								<a href="/personal/order/?payment=Y&id=<?=$arResult['ID']?>" class="btn-submit"><span class="btn-text"><?=GetMessage('PAY_ORDER')?></span></a>
							</div>
						<?endif?>
					<?endforeach?>
				<?
				else:?>
					<?= $arProperty["DISPLAY_VALUE"]; ?>
				<?endif?>
			</td>
			</tr><?
		endforeach; ?>

		<tr>
			<td colspan="2"><?= GetMessage("SPOD_ORDER_PAYMENT") ?></td>
		</tr><?
		foreach ($arResult['PAYMENT_PROPERTIES'] as $arProperty):?>

		<tr>
			<td>
				<?= $arProperty["NAME"] ?>:
			</td>
			<td>
				<?if (is_array($arProperty["DISPLAY_VALUE"])):?>
					<?= implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]); ?>
				<?
				elseif (is_array($arProperty['LINK_ELEMENT_VALUE'])):?>
					<?foreach ($arProperty['LINK_ELEMENT_VALUE'] as $arLink):?>
						<?= $arLink['NAME'] ?>
					<?endforeach?>
				<?
				else:?>
					<?= $arProperty["DISPLAY_VALUE"]; ?>
				<?endif?>
			</td><?

			endforeach ?>

		</tbody>
	</table>

	<h3><?= GetMessage('SPOD_ORDER_BASKET') ?></h3>
	<table class="bx_order_list_table_order table">
		<thead>
		<tr>
			<td colspan="2"><?= GetMessage('SPOD_NAME') ?></td>
			<td class="custom price"><?= GetMessage('SPOD_PRICE') ?></td>
			<!--<td class="custom amount"><?= GetMessage('SPOD_PRICETYPE') ?></td>-->
			<td class="custom price"><?= GetMessage('SPOD_QUANTITY') ?></td>
		</tr>
		</thead>
		<tbody>
		<? foreach ($arResult["BASKET_ITEMS"] as $prod): ?>
			<tr>
				<? $hasLink = !empty($prod["DETAIL_PAGE_URL"]); ?>
				<td class="custom img">
					<? if ($hasLink): ?>
					<a href="<?= $prod["DETAIL_PAGE_URL"] ?>" target="_blank">
						<? endif ?>
						<img src="<?= $prod['PICTURE'] ?>" alt="<?= $prod['NAME'] ?>"/>
						<? if ($hasLink): ?>
					</a>
				<? endif ?>
				</td>
				<td class="custom name">
					<? if ($hasLink): ?>
					<a href="<?= $prod["DETAIL_PAGE_URL"] ?>" target="_blank">
						<? endif ?>
						<?= $prod["NAME_FULL"] ?>
						<? if ($hasLink): ?>
					</a>
				<? endif ?>
				</td>
				<td class="custom price">
					<?= Tools::FormatPrice($prod["PRICE"]) ?>
				</td>
				<td class="custom">
					<span class="fm"><?= GetMessage('SPOD_QUANTITY') ?>:</span> <?= $prod["COUNT"] ?>
					<? if (strlen($prod['MEASURE_TEXT'])): ?>
						<?= $prod['MEASURE_TEXT'] ?>
					<? else: ?>
						<?= GetMessage('SPOD_DEFAULT_MEASURE') ?>
					<? endif ?>
				</td>
			</tr>
		<? endforeach ?>

		</tbody>
	</table>
	<br>
	<table class="bx_control_table" style="width: 100%;">
		<tr>
			<td>
				<a href="<?= Tools::GetConstantUrl($arResult['LIST_PAGE_URL']) ?>" class="btn btn-default">
					<?= GetMessage('SPOD_GO_BACK') ?>
				</a>
			</td>
		</tr>
	</table>
</div>
