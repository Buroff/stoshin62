<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
// we dont trust input params, so validation is required
$legalColors = array(
	'green' => true,
	'yellow' => true,
	'red' => true,
	'gray' => true
);
// default colors in case parameters unset
$defaultColors = array(
	'N' => 'green',
	'P' => 'yellow',
	'F' => 'gray',
	'PSEUDO_CANCELLED' => 'red'
);

foreach ($arParams as $key => $val)
	if (strpos($key, "STATUS_COLOR_") !== false && !$legalColors[$val])
		unset($arParams[$key]);

// to make orders follow in right status order

if (is_array($arResult["ORDERS"]) && !empty($arResult["ORDERS"]))
	foreach ($arResult["ORDERS"] as $order) {
		$order['HAS_DELIVERY'] = intval($order["ORDER"]["DELIVERY_ID"]) || strpos($order["ORDER"]["DELIVERY_ID"], ":") !== false;

		$stat = $order['ORDER']['CANCELED'] == 'Y' ? 'PSEUDO_CANCELLED' : $order["ORDER"]["STATUS_ID"];
		$color = $arParams['STATUS_COLOR_' . $stat];
		$order['STATUS_COLOR_CLASS'] = empty($color) ? 'gray' : $color;

		$arResult["ORDER_BY_STATUS"][$stat][] = $order;
	}
?>