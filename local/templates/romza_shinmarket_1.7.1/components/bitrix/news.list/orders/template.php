<? use Yenisite\Core\Tools;

if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();?>

<?if(!empty($arResult['ERRORS']['FATAL'])):?>

	<?foreach($arResult['ERRORS']['FATAL'] as $error):?>
		<?=ShowError($error)?>
	<?endforeach?>

<?else:?>

	<?if(!empty($arResult['ERRORS']['NONFATAL'])):?>

		<?foreach($arResult['ERRORS']['NONFATAL'] as $error):?>
			<?=ShowError($error)?>
		<?endforeach?>

	<?endif?>

	<?if(!empty($arResult['ITEMS'])):?>

		<?foreach($arResult['ITEMS'] as $key => $order):?>

				<div class='bx_my_order'>
					<table class='bx_my_order_table table'>
						<thead>
							<tr>
								<td><?=GetMessage('SPOL_ORDER')?> <?=GetMessage('SPOL_NUM_SIGN')?><?=$order['ID']?> <?=GetMessage('SPOL_FROM')?> <?=$order['DATE_CREATE'];?></td>
								<td style='text-align: right;'>
									<a href='<?=$order['DETAIL_PAGE_URL']?>'><?=GetMessage('SPOL_ORDER_DETAIL')?></a>
								</td>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									<strong><?=GetMessage('SPOL_PAY_SUM')?>:</strong> <?= Tools::FormatPrice($order['PROPERTIES']['AMOUNT']['VALUE'])?> <br />


									<? // PAY SYSTEM ?>
									<?if(isset($order['DISPLAY_PROPERTIES']['PAYMENT_E'])):?>
										<?$arPayment = reset($order['DISPLAY_PROPERTIES']['PAYMENT_E']['LINK_ELEMENT_VALUE']);?>
										<strong><?=GetMessage('SPOL_PAYSYSTEM')?>:</strong> <?=$arPayment['NAME']?> <br />
									<?endif?>

									<? // DELIVERY SYSTEM ?>
									<?if(isset($order['DISPLAY_PROPERTIES']['DELIVERY_E'])):?>
										<?$arDeliv = reset($order['DISPLAY_PROPERTIES']['DELIVERY_E']['LINK_ELEMENT_VALUE']);?>
										<strong><?=GetMessage('SPOL_DELIVERY')?>:</strong> <?=$arDeliv['NAME']?> <br />
									<?endif?>
									<?if(isset($order['DISPLAY_PROPERTIES']['ITEMS'])):?>
										<strong><?=GetMessage('SPOL_BASKET')?>:</strong>
										<ul class='bx_item_list'>
										<?foreach ($order['DISPLAY_PROPERTIES']['ITEMS']['LINK_ELEMENT_VALUE'] as $item):?>
											<li>
												<?if(strlen($item['DETAIL_PAGE_URL'])):?>
													<a href='<?=$item['DETAIL_PAGE_URL']?>' target='_blank'>
												<?endif?>
													<?=$item['NAME']?>
												<?if(strlen($item['DETAIL_PAGE_URL'])):?>
													</a> 
												<?endif?>
											</li>
										<?endforeach?>

									</ul>
									<?endif?>
								</td>
								<td>
									<?=$order['ORDER']['DATE_STATUS_FORMATED'];?>
									<div class='bx_my_order_status <?=$arResult['INFO']['STATUS'][$key]['COLOR']?><?/*yellow*/ /*red*/ /*green*/ /*gray*/?>'><?=$arResult['INFO']['STATUS'][$key]['NAME']?></div>
									<?if(false && $order['ORDER']['CANCELED'] != 'Y'):?>
										<div class='form-group'>
											<a href='<?=$order['ORDER']['URL_TO_CANCEL']?>' class='btn btn-default'><?=GetMessage('SPOL_CANCEL_ORDER')?></a>
										</div>
									<?endif?>
									<?/*?>
									<div class='form-group'>
										<a href='<?=$order['ORDER']['URL_TO_COPY']?>' class='btn btn-primary'><?=GetMessage('SPOL_REPEAT_ORDER')?></a>
									</div>
									<?*/?>
								</td>
							</tr>
						</tbody>
					</table>

				</div>

		<?endforeach?>

		<?if(strlen($arResult['NAV_STRING'])):?>
			<?=$arResult['NAV_STRING']?>
		<?endif?>

	<?else:?>
		<?=GetMessage('SPOL_NO_ORDERS')?>
	<?endif?>

<?endif?>