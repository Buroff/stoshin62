<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
if (defined("SITE_NAME") && strlen(SITE_NAME) > 0) {
	$arResult["SITE_NAME"] = SITE_NAME;
} else {
	$dbSite = CSite::GetByID(SITE_ID);
	$arSite = $dbSite->GetNext();
	$arResult["SITE_NAME"] = $arSite["NAME"];
}