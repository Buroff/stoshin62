<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Yenisite\Core\Resize;
use \Yenisite\Core\Tools;
include_once $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/tools/include_module.php';
$isAjax = Tools::isAjax();
?>
<?if (!$isAjax && method_exists($this, 'setFrameMode')) $this->setFrameMode(true); ?>
<? if ($isAjax): ?>
	<? if (strlen($arResult["ERROR_MESSAGE"]) <= 0): ?>
		<?
		$numProd = count($arResult['ITEMS']['AnDelCanBuy']);
		?>
		<?if (is_array($arResult["WARNING_MESSAGE"]) && !empty($arResult["WARNING_MESSAGE"])):?>
			<div class="message message-warning">
				<?= implode('<br />', $arResult["WARNING_MESSAGE"])?>
			</div>
		<?endif?>
		<span class="close flaticon-delete30" data-dismiss="modal"></span>
		<? if (count($arResult['ITEMS']['AnDelCanBuy']) > 0): ?>
			<h2 id="basket_modalLabel">
				<?= GetMessage("RZ_COUNT_ITEMS_TITLE", array("#NUM#" => $numProd . ' ' . Tools::rusQuantity($numProd, GetMessage("RZ_TOVAR")))) ?>
				<span class="brand-primary-color"><?= $arResult['allSum_FORMATED'] ?></span>
			</h2>
			<form class="basket_order_form" role="form">
				<table class="basket_order__head">
					<thead>
					<tr>
						<th class="title"><?=GetMessage("RZ_TOVAR")?></th>
						<th class="amount"><?=GetMessage("RZ_KOLICHESTVO")?></th>
						<th class="price"><?=GetMessage("RZ_TCENA_ZA_ED_")?></th>
					</tr>
					</thead>
				</table>

				<div class="basket_order__main_wrapper">
					<div class="basket_order__main_inner">
						<div class="basket_order__main_inner_2">
							<table class="basket_order__main">
								<tbody>
								<? foreach ($arResult['ITEMS']['AnDelCanBuy'] as $arItem): ?>
									<tr>
										<td class="img">
											<img src="<?= Resize::GetResizedImg($arItem,
												array('WIDTH' => 80, 'HEIGHT' => 80, 'SET_ID' => intval($arParams['RESIZER_PRODUCT']))) ?>"
												 alt="<?= $arItem['NAME'] ?>" title="<?= $arItem['NAME'] ?>">
										</td>
										<td class="title">
											<a href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><?= $arItem['NAME'] ?></a>
											<? if (count($arItem['PROPS']) > 0): ?>
												<div class="help-block">
													<? foreach($arItem['PROPS'] as $arProp):?>
														<?= $arProp['NAME'] , ' : ' , $arProp['VALUE'] ?><br/>
													<? endforeach ?>
												</div>
											<? endif ?>
										</td>
										<td class="amount">
											<input class="js-touchspin" type="text" value="<?= $arItem['QUANTITY'] ?>"
												name="QUANTITY_<?=$arItem['ID']?>"  title="">
										</td>
										<td class="price"><?= $arItem['PRICE_FORMATED'] ?></td>
										<td class="put_off">
											<a class="put_off_link" href="#" data-id="<?= $arItem['ID'] ?>">
												<span class="icon icon_arrow652"></span>
												<?= GetMessage('RZ_SET_ASIDE') ?>
											</a>
										</td>
										<td class="delete">
											<a class="delete_link" href="#" data-id="<?= $arItem['ID'] ?>">
												<span class="icon icon_recycling10"></span>
												<?= GetMessage('RZ_REMOVE') ?>
											</a>
										</td>
									</tr>
								<? endforeach ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="basket_order__foot">
					<div class="basket_order__submit column">
						<?
						$orderURL = Tools::GetConstantUrl($arParams['PATH_TO_BASKET']);
						?>
						<a class="btn btn-primary btn-order" href="<?= $orderURL ?>">
							<?= GetMessage('RZ_GET_ORDER') ?>
						</a>
					</div>
					<div class="basket_order__total column">
						<?= GetMessage('RZ_SUMM_TITLE') ?><span
							class="total_price brand-primary-color"><?= $arResult['allSum_FORMATED'] ?></span>
					</div>
					<? if (CModule::IncludeModule('sale') && CModule::IncludeModule('yenisite.oneclick') && $arParams['USE_ONE_CLICK'] == 'Y'): ?>
						<?$APPLICATION->IncludeComponent(
							"yenisite:oneclick.buy",
							"ajax",
							array(
								"PERSON_TYPE_ID" => $arParams["ONECLICK_PERSON_TYPE_ID"],
								"SHOW_FIELDS" => $arParams["ONECLICK_SHOW_FIELDS"],
								"REQ_FIELDS" => $arParams["ONECLICK_REQ_FIELDS"],
								"ALLOW_AUTO_REGISTER" => $arParams["ONECLICK_ALLOW_AUTO_REGISTER"],
								"USE_CAPTCHA" => $arParams["ONECLICK_USE_CAPTCHA"],
								"MESSAGE_OK" => $arParams["~ONECLICK_MESSAGE_OK"],
								"PAY_SYSTEM_ID" => $arParams["ONECLICK_PAY_SYSTEM_ID"],
								"DELIVERY_ID" => $arParams["ONECLICK_DELIVERY_ID"],
								"AS_EMAIL" => $arParams["ONECLICK_AS_EMAIL"],
								"AS_NAME" => $arParams["ONECLICK_AS_NAME"],
								"SEND_REGISTER_EMAIL" => $arParams["ONECLICK_SEND_REGISTER_EMAIL"],
								"FIELD_CLASS" => "form-control",
								"FIELD_PLACEHOLDER" => $arParams["ONECLICK_FIELD_PLACEHOLDER"],
								'OFFER_PROPS' => $arParams['OFFER_TREE_PROPS'],
								'BASKET_ONE_CLICK' => true
							),
							$component
						);?>
					<? endif ?>
				</div>
			</form>
		<? else: ?>
			<h2><?=GetMessage("RZ_VASHA_KORZINA_PUSTA")?></h2>
		<? endif; ?>
	<? else: ?>
		<div class="message message-error">
			<? ShowError($arResult["ERROR_MESSAGE"]); ?>
		</div>
	<?endif ?>
<? else: ?>
	<? if (method_exists($this, 'setFrameMode')) $this->setFrameMode(true); ?>
	<div class="modal fade" id="modal-basket" tabindex="-1" role="dialog" <?/*aria-labelledby="basket_modalLabel"*/?> aria-hidden="true"
		 data-arparams="<?= Tools::GetEncodedArParams($arParams) ?>" data-template="<?= $templateName ?>"
		 data-actionvar="<?= $arParams['ACTION_VARIABLE'] ?>"
		>
		<div class="container">
			<div class="popup modal-dialog popup_basket">
			</div>
		</div>
	</div>
<?endif ?>