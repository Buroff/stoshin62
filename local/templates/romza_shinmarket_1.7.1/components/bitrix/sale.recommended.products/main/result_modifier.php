<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
use \Yenisite\Core\Tools;
use \Yenisite\Core\Catalog;
use \Yenisite\Core\Resize;

$resizeParams = array('WIDTH' => 168, 'HEIGHT' => 170, 'SET_ID' => intval($arParams['RESIZER_PRODUCT']));
if(CRZShinmarket::isCatchBuy()){
	$arParams['CATCHBUY'] = CRZShinmarket::getCatchBuyList();
}

if (!empty($arResult['ITEMS'])) {
	$arNewItemsList = array();
	foreach ($arResult['ITEMS'] as $key => $arItem) {
		$arItem['CHECK_QUANTITY'] = false;
		if (!isset($arItem['CATALOG_MEASURE_RATIO']))
			$arItem['CATALOG_MEASURE_RATIO'] = 1;
		if (!isset($arItem['CATALOG_QUANTITY']))
			$arItem['CATALOG_QUANTITY'] = 0;
		$arItem['CATALOG_QUANTITY'] = (
		0 < $arItem['CATALOG_QUANTITY'] && is_float($arItem['CATALOG_MEASURE_RATIO'])
			? floatval($arItem['CATALOG_QUANTITY'])
			: intval($arItem['CATALOG_QUANTITY'])
		);
		$arItem['CATALOG'] = false;
		if (!isset($arItem['CATALOG_SUBSCRIPTION']) || 'Y' != $arItem['CATALOG_SUBSCRIPTION'])
			$arItem['CATALOG_SUBSCRIPTION'] = 'N';

		$productPictures = CIBlockPriceTools::getDoublePicturesForItem($arItem, $arParams['ADD_PICT_PROP']);

		if (empty($productPictures['SECOND_PICT']))
			$productPictures['SECOND_PICT'] = $productPictures['PICT'];

		$arItem['SECOND_PICT'] = true;

		if ($arResult['MODULES']['catalog']) {
			$arItem['CATALOG'] = true;
			if (!isset($arItem['CATALOG_TYPE']))
				$arItem['CATALOG_TYPE'] = CCatalogProduct::TYPE_PRODUCT;
			if (
				(CCatalogProduct::TYPE_PRODUCT == $arItem['CATALOG_TYPE'] || CCatalogProduct::TYPE_SKU == $arItem['CATALOG_TYPE'])
				&& !empty($arItem['OFFERS'])
			) {
				$arItem['CATALOG_TYPE'] = CCatalogProduct::TYPE_SKU;
			}
			switch ($arItem['CATALOG_TYPE']) {
				case CCatalogProduct::TYPE_SET:
					$arItem['OFFERS'] = array();
					$arItem['CATALOG_MEASURE_RATIO'] = 1;
					$arItem['CATALOG_QUANTITY'] = 0;
					$arItem['CHECK_QUANTITY'] = false;
					break;
				case CCatalogProduct::TYPE_SKU:
					break;
				case CCatalogProduct::TYPE_PRODUCT:
				default:
					$arItem['CHECK_QUANTITY'] = ('Y' == $arItem['CATALOG_QUANTITY_TRACE'] && 'N' == $arItem['CATALOG_CAN_BUY_ZERO']);
					break;
			}
		} else {
			$arItem['CATALOG_TYPE'] = 0;
			$arItem['OFFERS'] = array();
		}

		if ($arItem['CATALOG'] && isset($arItem['OFFERS']) && !empty($arItem['OFFERS'])) {
			$arItem['MIN_PRICE'] = CIBlockPriceTools::getMinPriceFromOffers(
				$arItem['OFFERS'],
				$boolConvert ? $arResult['CONVERT_CURRENCY']['CURRENCY_ID'] : $strBaseCurrency
			);
		}

		if ($arResult['MODULES']['catalog'] && $arItem['CATALOG'] && CCatalogProduct::TYPE_PRODUCT == $arItem['CATALOG_TYPE']) {
			CIBlockPriceTools::setRatioMinPrice($arItem, true);
		}

		if (!empty($arItem['DISPLAY_PROPERTIES'])) {
			foreach ($arItem['DISPLAY_PROPERTIES'] as $propKey => $arDispProp) {
				if ('F' == $arDispProp['PROPERTY_TYPE'])
					unset($arItem['DISPLAY_PROPERTIES'][$propKey]);
			}
		}
		$arItem['LAST_ELEMENT'] = 'N';
		$arItem['PICTURE'] = Resize::GetResizedImg($arItem, $resizeParams);
		$arItem['PICTURE_SECOND'] = Resize::GetResizedImg($productPictures['SECOND_PICT']['ID'], $resizeParams);
		if (count($arItem['OFFERS']) > 0) {
			/** @noinspection PhpVoidFunctionResultUsedInspection */
			foreach ($arItem['OFFERS'] as &$arOffer){
				$arOffer = CRZShinmarket::getDisplayValueForProps($arOffer);
				$arCatchBuy = $arParams['CATCHBUY'][$arOffer['ID']];
				$bTimer = !empty($arCatchBuy['ACTIVE_TO']);
				$bProgressBar = $arCatchBuy['MAX_USES'] > 0;
				$arCatchBuy['PERCENT'] = ($bProgressBar) ? $arCatchBuy['COUNT_USES'] / $arCatchBuy['MAX_USES'] * 100 : 0;
				if(($bTimer || $bProgressBar) && $arOffer['CAN_BUY']){
					$arItem['CATCHBUY']['HAS_OFFERS'] = true;
					if($bTimer){
						$arOffer['CATCH_BUY']['TIMER'] = str_replace('XXX', 'T', ConvertDateTime($arCatchBuy['ACTIVE_TO'], 'YYYY-MM-DDXXXhh:mm:ss'));
					}
					if ($bProgressBar){
						$arOffer['CATCH_BUY']['PROGRESS'] = intVal($arCatchBuy['PERCENT']);
					}
				}
			}
			unset($arOffer);
			$arItem['FLAT_OFFERS'] = Catalog::getFlatOffersList($arItem, $arParams['OFFERS_PROPERTY_CODE']);
		}
		if (isset($arItem['FLAT_OFFERS']) && !empty($arItem['FLAT_OFFERS'])) {
			$arOffer = reset($arItem['FLAT_OFFERS']);
			$arItem['MIN_PRICE']['PRINT_VALUE'] = htmlspecialcharsback($arOffer['PRICE_OLD']);
			$arItem['MIN_PRICE']['PRINT_DISCOUNT_VALUE'] = htmlspecialcharsback($arOffer['PRICE']);
		}
		$arNewItemsList[$key] = $arItem;
	}
	$arNewItemsList[$key]['LAST_ELEMENT'] = 'Y';
	$arResult['ITEMS'] = $arNewItemsList;
}

$bFilterByProp = (!empty($arParams['PROPERTY_FILTER_CODE']) && !empty($arParams['PROPERTY_FILTER_VALUE']));
if ($bFilterByProp) {
	/** @noinspection PhpDynamicAsStaticMethodCallInspection */
	$cnt = CIBlockElement::GetList(array(),
		array(
			'IBLOCK_ID' => $arParams['IBLOCK_ID'],
			'PROPERTY_' . $arParams['PROPERTY_FILTER_CODE'] => $arParams['PROPERTY_FILTER_VALUE']
		),
		array()
	);
	$arResult['CNT'] = $cnt;
}
$arResult["IS_SKU"] = $boolSKU;
?>