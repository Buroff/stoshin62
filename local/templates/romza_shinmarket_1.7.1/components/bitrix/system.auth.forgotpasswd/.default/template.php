<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use \Yenisite\Core\Tools;
\Bitrix\Main\Localization\Loc::loadMessages($_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH . '/lang/'.LANGUAGE_ID.'/header.php');
$pathToRules = COption::GetOptionString(CRZShinmarketSettings::getModuleId(),'path_tu_rules_privacy',SITE_DIR.'personal/rules/personal_data.php');
$rand = $this->randString();?>
<form class="modal-form" name="bform" method="post" target="_top" action="<?= $arResult["AUTH_URL"] ?>">
	<?
	if (strlen($arResult["BACKURL"]) > 0) {
		?>
		<input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>"/>
	<?
	}
	?>
	<input type="hidden" name="AUTH_FORM" value="Y">
	<input type="hidden" name="TYPE" value="SEND_PWD">

	<div class="row">
		<div class="col-md-6 col-xs-10">
			<? if (is_array($arParams["~AUTH_RESULT"]) && isset($arParams["~AUTH_RESULT"]['MESSAGE'])): ?>
				<div class="message <?if($arParams["~AUTH_RESULT"]['TYPE'] == 'OK'):?>message-success<?else:?>message-error<?endif?>">
					<?= $arParams["~AUTH_RESULT"]['MESSAGE'] ?>
				</div>
			<? endif ?>
			<div class="form-group">
				<h2><?= GetMessage("AUTH_GET_CHECK_STRING") ?></h2>

				<p class="help-block"><?= GetMessage("AUTH_FORGOT_PASSWORD_1") ?></p>
			</div>
			<div class="form-group">
				<label><?= GetMessage("AUTH_LOGIN") ?></label>
				<input type="text" name="USER_LOGIN" class="form-control"
					   value="<?= $arResult["LAST_LOGIN"] ?>"/>
			</div>
			<div class="form-group">
				<b>&nbsp;<?= GetMessage("AUTH_OR") ?></b>
			</div>
			<div class="form-group">
				<label><?= GetMessage("AUTH_EMAIL") ?></label>
				<input type="email" name="USER_EMAIL" class="form-control"/>
			</div>
            <? if (!empty($arResult["CAPTCHA_CODE"])): ?>
                <div class="form-group">
                    <img alt="<?= GetMessage("CAPTCHA_ALT") ?>"
                         src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>"/>
                    <input  type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>"/>
                </div>
                <div class="form-group required">
                    <label for="popup_call_captcha" class="control-label"><?= GetMessage("CAPTCHA_TITLE") ?>:</label>
                    <input required id='popup_call_captcha' class="form-control" type="text" name="captcha_word"/>
                </div>
            <? endif; ?>
            <div class="form-group agreement-policy">
                <div class="forms-wrapper">
                    <input id="privacy_policy_<?=$rand?>" required type="checkbox" name="privacy_policy" value="Y" <?=$_REQUEST['privacy_policy'] == 'Y' ? ' checked' : ''?>>
                    <label for="privacy_policy_<?=$rand?>">
                        <?= GetMessage('RZ_ACCEPT') ?> <?=GetMessage('RZ_CONCLUSION',array("#URL#" => Tools::GetConstantUrl($pathToRules)))?>
                    </label>
                </div>
            </div>
			<div class="form-group">
				<input type="submit" name="send_account_info" class="btn btn-primary" value="<?= GetMessage("AUTH_SEND") ?>"/>
			</div>
			<p>
				<a href="<?= $arResult["AUTH_AUTH_URL"] ?>"><b><?= GetMessage("AUTH_AUTH") ?></b></a>
			</p>
		</div>
	</div>
</form>
<script type="text/javascript">
	document.bform.USER_LOGIN.focus();
</script>
