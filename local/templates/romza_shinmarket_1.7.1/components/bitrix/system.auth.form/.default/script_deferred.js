(typeof(jQuery) != 'undefined')
&& (function ($) {
	var $modalForm = $('#auth_modalForm');
	$('body').on('submit', '#auth_modalForm_Form', function (e) {
		var $this = $(this);
		e.preventDefault();
		var data = $this.serializeArray();
		data.push({name: "arParams", value: $modalForm.data('arparams')});
		data.push({name: "template", value: $modalForm.data('template').toString()});
		data.push({name: "Login", value: 'Y'});
		$modalForm.setAjaxLoading();
		$.ajax({
			type: "POST",
			url: rz.AJAX_DIR + "auth_modalForm.php",
			data: data,
			success: function (msg) {
				$modalForm.html(msg);
				// SITE_TEMPLATE_PATH/js/script_after.js
				$modalForm.reStyler();
                $modalForm.refreshForm();
				$modalForm.stopAjaxLoading();
			}
		})
	});
	var $wishListData = $('#wishListData');
	$(document).on('wishListData_update', function () {
		var data = [
			{name: 'rz_ajax', value: 'y'},
			{name: 'ajax', value: $wishListData.data('ajax')},
		];
		return $.ajax({
			type: "POST",
			data:data,
			dataType: 'html',
			url: rz.AJAX_DIR + "wishList.php",
			success: function (msg) {
				if(parseInt(msg) > 0) {
					$wishListData.html(msg).removeAttr('disabled');
				} else {
					$wishListData.html(msg).attr('disabled');
				}
			}
		});
	});
	/* AJAX FOR FAVORITE LIST, MABY USE WHEN FIX TRABLE WITH JQUERY VERSIONS 1.8.3 or 1.11.2

	var $wishListPersonal = $('#wish-list');
	$(document).on('wishListPersonal_update', function () {
		$wishListPersonal.setAjaxLoading();
		data = [
			{name: 'rz_ajax', value: 'y'},
			{name: 'ajax', value: $wishListPersonal.data('ajax')},
			{name: 'WISH_LIST', value: rz.WISH_LIST.length},
		];
		return $.ajax({
			type: "POST",
			data:data,
			dataType: 'html',
			url: rz.AJAX_DIR + "wishList.php",
			success: function (msg) {
				$wishListPersonal.html(msg);
				$wishListPersonal.stopAjaxLoading();
			}
		});
	});

	/*var $wishList = $('#wishListData');
	 if('WISH_LIST' in rz && rz.WISH_LIST.length > 0) {
	 $wishList.text(rz.WISH_LIST.length);
	 } else {
	 $wishList.text(0).attr('disabled',true);
	 }*/
})(jQuery);