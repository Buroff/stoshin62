<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use \Yenisite\Core\Tools;
include_once $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/tools/include_module.php';
$isAjax = Tools::isAjax();
\Bitrix\Main\Localization\Loc::loadMessages($_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH . '/lang/'.LANGUAGE_ID.'/header.php');
$pathToRules = COption::GetOptionString(CRZShinmarketSettings::getModuleId(),'path_tu_rules_privacy',SITE_DIR.'personal/rules/personal_data.php');
$rand = $this->randString();

if (!$isAjax && method_exists($this, 'setFrameMode')) $this->setFrameMode(true); ?>
<? if ($arResult["FORM_TYPE"] == "login"): ?>
	<?if (!$isAjax): ?>
<?if (!$USER->IsAuthorized()):?>
<?//INCLUDE FAVORITE_HEADER?>
<span class="user_authorized">
			<a href="#" class="btn  btn-login" data-toggle="modal" data-target="#modal-login">
				<span class="flaticon-incoming4"></span>
				<span class="btn-inner"><?= GetMessage('RZ_LOGIN_BUTTON') ?></span>
			</a>
			<?$APPLICATION->IncludeComponent("bitrix:main.include", "", Array("AREA_FILE_SHOW" => "file",	"PATH" => SITE_DIR."include_areas/site_template/register.php",	"EDIT_TEMPLATE" => "include_areas_template.php"	), false, array("HIDE_ICONS"=>"Y"));?>
			<? Tools::IncludeArea('site_template', 'favorite', array(), true) ?>
			<?$APPLICATION->IncludeComponent("bitrix:main.include", "", Array("AREA_FILE_SHOW" => "file",	"PATH" => SITE_DIR."include_areas/site_template/compare.php",	"EDIT_TEMPLATE" => "include_areas_template.php"	), false, array("HIDE_ICONS"=>"Y"));?>
</span>
<?endif?>
	<?endif; ?>
	<?if (!$isAjax): ?>
		<?$this->SetViewTarget('modal_auth') ?>
		<div class="modal fade" id="modal-login" tabindex="-1" role="dialog" aria-labelledby="auth_modalLabel" aria-hidden="true">
		<div class="popup modal-dialog popup_login" id="auth_modalForm" data-arparams='<?= Tools::GetEncodedArParams($arParams) ?>' data-template='<?= $templateName ?>'>
	<?endif; ?>
	<span class="close flaticon-delete30" data-dismiss="modal"></span>
	<h2 id="auth_modalLabel">
		<span class="brand-primary-color flaticon-incoming4"></span>
		<?= GetMessage('RZ_LOGIN_TITLE') ?>:
	</h2>
	<div><?= GetMessage('RZ_OR') ?> <a href="#" data-dismiss="modal" data-toggle="modal"
									   data-target="#modal-register"><?= GetMessage('RZ_REG_TITLE') ?></a></div>
	<? if ($arResult['SHOW_ERRORS'] == 'Y' && $arResult['ERROR']): ?>
		<div class="message message-error">
			<?= $arResult['ERROR_MESSAGE']['MESSAGE']; ?>
		</div>
	<? endif ?>
	<form class="form-horizontal form-login modal-form" role="form" id="auth_modalForm_Form">
        <input type="hidden" name="privacy_policy" value="N"/>
		<?if ($arResult["BACKURL"] <> ''): ?>
			<input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>"/>
		<?endif ?>
		<?foreach ($arResult["POST"] as $key => $value): ?>
			<input type="hidden" name="<?= $key ?>" value="<?= $value ?>"/>
		<?endforeach ?>
		<input type="hidden" name="AUTH_FORM" value="Y"/>
		<input type="hidden" name="TYPE" value="AUTH"/>

		<div class="form-group">
			<label for="popup_auth_login_<?= $arResult["RND"]; ?>" class="control-label"><?= GetMessage("RZ_FORM_LOGIN") ?></label>

			<div class="control-wrapper">
				<input id="popup_auth_login_<?= $arResult["RND"]; ?>" type="text" class="form-control" name="USER_LOGIN">
			</div>
		</div>
		<div class="form-group">
			<label for="popup_auth_password_<?= $arResult["RND"]; ?>" class="control-label"><?= GetMessage("RZ_FORM_PASS") ?></label>

			<div class="control-wrapper">
				<input id="popup_auth_password_<?= $arResult["RND"]; ?>" class="form-control password-field" type="password"
					   name="USER_PASSWORD">
			</div>
		</div>
		<div class="form-group manage">
			<?if ($arResult["STORE_PASSWORD"] == "Y"): ?>
				<span class="forms-wrapper remember">
						<input type="checkbox" id="popup_auth_remember_<?= $arResult["RND"]; ?>" name="USER_REMEMBER">
						<label for="popup_auth_remember_<?= $arResult["RND"]; ?>"><?= GetMessage("RZ_FORM_REMEMBER_ME") ?></label>
					</span>
			<?endif; ?>
			<a class="recovery" href="<?= $arResult["AUTH_FORGOT_PASSWORD_URL"] ?>"
			   rel="nofollow"><?= GetMessage("RZ_FORM_FORGOT_PASS") ?></a>
		</div>
        <div class="form-group agreement-policy">
            <div class="forms-wrapper">
                <input id="privacy_policy_<?=$rand?>" required type="checkbox" name="privacy_policy" value="Y" <?=$_REQUEST['privacy_policy'] == 'Y' ? ' checked' : ''?>>
                <label for="privacy_policy_<?=$rand?>">
                    <?= GetMessage('RZ_ACCEPT') ?> <?=GetMessage('RZ_CONCLUSION',array("#URL#" => Tools::GetConstantUrl($pathToRules)))?>
                </label>
            </div>
        </div>
		<div class="form-group submit">
			<button type="submit" class="btn btn-primary btn-submit-login"><?= GetMessage("RZ_FORM_ENTER") ?></button>
		</div>

	</form>
	<?if ($arResult["AUTH_SERVICES"]): ?>
		<div class="form-login">
			<?$APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "",
				array(
					"AUTH_SERVICES" => $arResult["AUTH_SERVICES"],
					"AUTH_URL" => $arResult["AUTH_URL"],
					"POST" => $arResult["POST"],
					"POPUP" => "N",
					"SUFFIX" => "form_" . $arResult["RND"],
				),
				$component,
				array("HIDE_ICONS" => "Y")
			); ?>
		</div>
	<?endif ?>
	<?if (!$isAjax): ?>
		</div>
		</div>
		<?$this->EndViewTarget() ?>
	<?endif; ?>
<?
else:
	?>
	<? if (!$isAjax):?>
	<form action="<?= $arResult["AUTH_URL"] ?>" id="form_authLogout">
		<? foreach ($arResult["GET"] as $key => $value):?>
			<input type="hidden" name="<?= $key ?>" value="<?= $value ?>"/>
		<? endforeach?>
		<div class="user_authorized">
			<span class="user_block">
				<span class="icon icon_user85"></span>
				<span class="hidden-xs hidden-sm"><?=GetMessage('RZ_PERSONAL_CABINET')?></span>
				<a class="name" href="<?= $arResult["PROFILE_URL"] ?>"><?= $arResult["USER_NAME"] ?></a>
				<input type="hidden" name="logout" value="yes"/>
				<input type="hidden" name="logout_butt" value="<?= GetMessage("AUTH_LOGOUT_BUTTON") ?>"/>
				&nbsp;<a class="logout btn" href="javascript:" onclick="document.getElementById('form_authLogout').submit()">
						<i class="icon flaticon-delete30"></i>
						<span class="text"><?= GetMessage("RZ_VIJTI") ?></span>
					</a>
			</span>
			<?//INCLUDE FAVORITE_HEADER?>
			<? Tools::IncludeArea('site_template', 'favorite', array(), true) ?>
			<?$APPLICATION->IncludeComponent("bitrix:main.include", "", Array("AREA_FILE_SHOW" => "file",	"PATH" => SITE_DIR."include_areas/site_template/compare.php",	"EDIT_TEMPLATE" => "include_areas_template.php"	), false, array("HIDE_ICONS"=>"Y"));?>
		</div>
	</form>
<? else:?>
	<div class="message message-success">
		<b><?= GetMessage('RZ_AUTH_SUCCESS') ?></b>
	</div>
	<script type="text/javascript">
		window.location.reload();
	</script>
<? endif;?>
<? endif ?>
