<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

//delayed function must return a string
if (empty($arResult))
	return "";

$strReturn = '<ul class="breadcrumb">';

$num_items = count($arResult);
for ($index = 0, $itemSize = $num_items; $index < $itemSize; $index++) {
	$title = htmlspecialcharsex($arResult[$index]["TITLE"]);

	if ($arResult[$index]["LINK"] <> "" && $index != $itemSize - 1)
		if ($arResult[$index]["LINK"] == SITE_DIR) {
			$strReturn .= '<li><a class="icon icon_home39" href="' . $arResult[$index]["LINK"] . '" title="' . $title . '"></a></li>';
		} else {
			$strReturn .= '<li><a href="' . $arResult[$index]["LINK"] . '" title="' . $title . '">' . $title . '</a></li>';
		}
	else
		if ($arResult[$index]["LINK"] == SITE_DIR) {
			$strReturn .= '<li class="icon icon_home39 active" title="'.$title.'"></li>';
		} else {
			$strReturn .= '<li class="active">' . $title . '</li>';
		}
}
$strReturn .= '</ul>';
return $strReturn;
?>