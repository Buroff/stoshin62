$(function ($) {
	var $sliderList = $('.js-slider_goods_list .goods_list');
	$sliderList.filter(':not(.active)').unslick();
	$('body').on('click', '.goods_list__categories_item__link', function (e) {
		var $this = $(this);
		if ($this.data('slickOpt')) {
			var slickOpt = $this.data('slickOpt');
		} else {
			var slickOpt = $sliderList.getSlick().options;
			$this.data('slickOpt', slickOpt);
		}
		e.preventDefault();
		if ($this.hasClass('active')) {
			return false;
		} else {
			var $parent = $this.closest('.goods_list__categories');
			$parent.find('.goods_list__categories_item__link').removeClass('active');
			var $parentList = $this.closest('.js-slider_goods_list');
			$parentList.find('.goods_list.active').removeClass('active');

			$this.addClass('active');
			$("#" + $this.data('target')).addClass('active');
			var $curList = $parentList.find('.goods_list');
			$curList.filter('.active').slick(slickOpt);
			$curList.filter(':not(.active)').unslick();
		}
	});
});