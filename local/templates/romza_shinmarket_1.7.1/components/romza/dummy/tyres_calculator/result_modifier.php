<?
use \Yenisite\Shinmarket\ManagerCalc;
use \Yenisite\Core\Ajax;

$bAjax = Ajax::isAjax();
$arParams['FILTER_NAME'] = trim($arParams['FILTER_NAME']);
$arParams['PROP_WIDTH'] = intval($arParams['PROP_WIDTH']);
$arParams['PROP_HEIGHT'] = intval($arParams['PROP_HEIGHT']);
$arParams['PROP_DIAMTER'] = intval($arParams['PROP_DIAMTER']);
$arParams['SECTION_DISKS_ID'] = intval($arParams['SECTION_DISKS_ID']);
$arParams['SECTION_TYRES_ID'] = intval($arParams['SECTION_TYRES_ID']);

$obCache = new CPHPCache();
if ($obCache->InitCache(36000, serialize($arParams), "romza/TyresCalc/" . $this->__name)) {
	$arResult = $obCache->GetVars();
} elseif ($obCache->StartDataCache()) {
	$obCache->CleanDir("romza/TyresCalc/" . $this->__name);
	$arResult = array();
	if ($arParams['SECTION_DISKS_ID'] > 0) {
		$dbSection = CIBlockSection::GetByID($arParams['SECTION_DISKS_ID']);
		$dbSection->SetUrlTemplates();
		$arSection = $dbSection->GetNext();

		$arResult['SECTION_DISKS_URL'] = $arSection['SECTION_PAGE_URL'];
	}
	if ($arParams['SECTION_TYRES_ID'] > 0) {
		$dbSection = CIBlockSection::GetByID($arParams['SECTION_TYRES_ID']);
		$dbSection->SetUrlTemplates();
		$arSection = $dbSection->GetNext();

		$arResult['SECTION_TYRES_URL'] = $arSection['SECTION_PAGE_URL'];
	}

    $arValuesOfPropW = array();
    $arValuesOfPropH = array();
    $arValuesOfPropR = array();
	$arFilterElements = array(
        "ACTIVE" => 'Y',
        "INCLUDE_SUBSECTIONS" => "Y",
        "SECTION_ID" => $arParams["SECTION_TYRES_ID"],
        array("LOGIC" => 'OR',
            array("!PROPERTY_".$arParams['PROP_WIDTH'] => false),
            array("!PROPERTY_".$arParams['PROP_HEIGHT'] => false),
            array("!PROPERTY_".$arParams['PROP_DIAMTER'] => false)
        )
    );
    $dbValPropElements = CIBlockElement::GetList(array(),$arFilterElements, array("PROPERTY_".$arParams['PROP_WIDTH'],"PROPERTY_".$arParams['PROP_HEIGHT'],"PROPERTY_".$arParams['PROP_DIAMTER']));
	while ($arPropElement = $dbValPropElements->Fetch()){
	    if (!in_array($arPropElement["PROPERTY_".$arParams['PROP_WIDTH'].'_VALUE'],$arValuesOfPropW) && !empty($arPropElement["PROPERTY_".$arParams['PROP_WIDTH'].'_VALUE'])) {
            $arValuesOfPropW[] = $arPropElement["PROPERTY_" . $arParams['PROP_WIDTH'] . '_VALUE'];
        }
        if (!in_array($arPropElement["PROPERTY_".$arParams['PROP_HEIGHT'].'_VALUE'],$arValuesOfPropH) && !empty($arPropElement["PROPERTY_".$arParams['PROP_HEIGHT'].'_VALUE'])) {
            $arValuesOfPropH[] = $arPropElement["PROPERTY_" . $arParams['PROP_HEIGHT'] . '_VALUE'];
        }
        if (!in_array($arPropElement["PROPERTY_".$arParams['PROP_DIAMTER'].'_VALUE'],$arValuesOfPropR) && !empty($arPropElement["PROPERTY_".$arParams['PROP_DIAMTER'].'_VALUE'])) {
            $arValuesOfPropR[] = $arPropElement["PROPERTY_" . $arParams['PROP_DIAMTER'] . '_VALUE'];
        }
    }

	$dbProp = CIBlockProperty::GetPropertyEnum($arParams['PROP_WIDTH'], array('VALUE' => 'ASC'));
	while ($arProp = $dbProp->GetNext()) {
	    if (!in_array($arProp['VALUE'],$arValuesOfPropW)) continue;
		$arResult['PROPS']['W'][$arProp['VALUE']] = htmlspecialcharsbx($arParams['FILTER_NAME'] . '_' . $arProp['PROPERTY_ID'] . '_' . abs(crc32($arProp['ID'])) . '=Y');
	}
	$dbProp = CIBlockProperty::GetPropertyEnum($arParams['PROP_HEIGHT'], array('VALUE' => 'ASC'));
	while ($arProp = $dbProp->GetNext()) {
        if (!in_array($arProp['VALUE'],$arValuesOfPropH)) continue;
		$arResult['PROPS']['H'][$arProp['VALUE']] = htmlspecialcharsbx($arParams['FILTER_NAME'] . '_' . $arProp['PROPERTY_ID'] . '_' . abs(crc32($arProp['ID'])) . '=Y');
	}
	$dbProp = CIBlockProperty::GetPropertyEnum($arParams['PROP_DIAMTER'], array('VALUE' => 'ASC'));
	while ($arProp = $dbProp->GetNext()) {
        if (!in_array($arProp['VALUE'],$arValuesOfPropR)) continue;
		$arResult['PROPS']['R'][$arProp['VALUE']] = htmlspecialcharsbx($arParams['FILTER_NAME'] . '_' . $arProp['PROPERTY_ID'] . '_' . abs(crc32($arProp['ID'])) . '=Y');
	}
	unset($arValuesOfPropR,$arValuesOfPropH,$arValuesOfPropW);
	$obCache->EndDataCache($arResult);
}
if (CRZShinmarketSettings::isTiersSolution() && ManagerCalc::tablesExist() && !$bAjax){
    $arResult['VENDORS'] = ManagerCalc::processOnlyVendors();
    $arResult['OTHERS_PROPS'] = ManagerCalc::processItemsByVendor($arResult['VENDORS'][0]);
}