(typeof(jQuery) != 'undefined')
&& (function ($) {

	var $calc = $('#search_formCalculator');
	var $jslider = $('.js-speed-slider');
	_$ = {
		'old_W': '', 'old_H': '', 'old_R': '',
		'new_W': '', 'new_H': '', 'new_R': '',
		'old_W_mm': '', 'old_H_mm': '', 'old_R_mm': '', 'old_D_mm': '',
		'new_W_mm': '', 'new_H_mm': '', 'new_R_mm': '', 'new_D_mm': '',
		'res_W_mm': '', 'res_H_mm': '', 'res_R_mm': '', 'res_D_mm': '', 'res_C_mm': '',
		'speed': '', 'res_V': '', 'diff_V': '', 'calc_vendor': '', 'calc_model': '', 'calc_year': '', 'calc_modif': ''
	};
	if (typeof(calcObj) != 'object') {
		calcObj = {
			W: '',
			H: '',
			R: ''
		}
	}
	$.each(_$, function (key, val) {
		_$[key] = $('#' + key);
	});

	var defSpeed = (_$.speed.val() | 0 == 0) ? 90 : _$.speed.val();

	$calc.on('change', 'select, input', function (e) {
		do_calc();
	});
	$calc.on('click', '.form_reset_button', function (e) {
		e.preventDefault();
		$calc[0].reset();
		$jslider.val(defSpeed);
		do_calc();
	});
	// osobennost rabotyi s float
	// http://habrahabr.ru/post/112953/
	var calc_fixed_res = function ($a, $b) {
		return ((
		(+$a * 100)
		- (+$b * 100)
		) / 100).toFixed(2)
	};
	// osobennost rabotyi s float
	// http://habrahabr.ru/post/112953/
	var calc_fixed_sum = function ($a, $b) {
		return ((
		(+$a * 100)
		+ (+$b * 100)
		) / 100).toFixed(2)
	};
	var do_calc = function () {
		_$.old_W_mm.text(_$.old_W.val());
		_$.new_W_mm.text(_$.new_W.val());
		_$.res_W_mm.text(_$.new_W.val() - _$.old_W.val());
		_$.old_H_mm.text((_$.old_W.val() * _$.old_H.val() / 100).toFixed(2));
		_$.new_H_mm.text((_$.new_W.val() * _$.new_H.val() / 100).toFixed(2));
		_$.res_H_mm.text(calc_fixed_res(_$.new_H_mm.text(), _$.old_H_mm.text()));
		_$.old_R_mm.text((25.4 * _$.old_R.val()).toFixed(2));
		_$.new_R_mm.text((25.4 * _$.new_R.val()).toFixed(2));
		_$.res_R_mm.text(calc_fixed_res(_$.new_R_mm.text(), _$.old_R_mm.text()));
		_$.old_D_mm.text(calc_fixed_sum(_$.old_R_mm.text(), 2 * _$.old_H_mm.text()));
		_$.new_D_mm.text(calc_fixed_sum(_$.new_R_mm.text(), 2 * _$.new_H_mm.text()));
		_$.res_D_mm.text(calc_fixed_res(_$.new_D_mm.text(), _$.old_D_mm.text()));
		_$.res_C_mm.text((_$.res_D_mm.text() / 2).toFixed(2));
		if ((_$.speed.val() | 0) == 0) {
			_$.speed.val(defSpeed);
		}
		_$.res_V.text((_$.new_D_mm.text() / _$.old_D_mm.text() * _$.speed.val()).toFixed(2));
		_$.diff_V.text((_$.res_V.text() - _$.speed.val()).toFixed(2));
	};

	var $sectionTyres = $('#sectionTyres');
	var processFilter = function () {
		var arFilter = [];
		if (calcObj.W.hasOwnProperty(_$.new_W.val())) {
			arFilter.push(calcObj.W[_$.new_W.val()]);
		}
		if (calcObj.H.hasOwnProperty(_$.new_H.val())) {
			arFilter.push(calcObj.H[_$.new_H.val()]);
		}
		if (calcObj.R.hasOwnProperty(_$.new_R.val())) {
			arFilter.push(calcObj.R[_$.new_R.val()]);
		}
		var filter = "?" + arFilter.join('&') + '&set_filter=Y';
		$sectionTyres.attr('href', $sectionTyres.data('href') + filter);
	};

	_$.new_W.on('change', function () {
		var $this = $(this);
		processFilter();
        doAjaxGetCountItems($this);
	});
	_$.new_H.on('change', function () {
        var $this = $(this);
		processFilter();
        doAjaxGetCountItems($this);
	});
	_$.new_R.on('change', function () {
        var $this = $(this);
		processFilter();
        doAjaxGetCountItems($this);
	});
	_$.calc_vendor.on('change', function () {
		var vendor = $(this).find('option:selected').text();
        doAjax('by_vendor',vendor);
	});
	_$.calc_model.on('change', function () {
        var vendor = _$.calc_vendor.find('option:selected').text(),
			model = $(this).find('option:selected').text();
        doAjax('by_vendor_model',vendor,model);
	});
	_$.calc_year.on('change', function () {
        var vendor = _$.calc_vendor.find('option:selected').text(),
            model = _$.calc_model.find('option:selected').text(),
            year = $(this).find('option:selected').text();
        doAjax('by_vendor_model_year',vendor,model,year);
	});
	_$.calc_modif.on('change', function () {
        var vendor = _$.calc_vendor.find('option:selected').text(),
            model = _$.calc_model.find('option:selected').text(),
            year = _$.calc_year.find('option:selected').text(),
            modif = $(this).find('option:selected').text();
        doAjax('by_vendor_model_year_modif',vendor,model,year,modif);
	});

	processFilter();
	do_calc();

	var doAjax = function(action,strVendor,strModel,strYear,strModif){
		var data = {},
			$container = _$.calc_model.closest('.tab-pane');

		if (typeof strVendor != 'undefined'){
			data['vendor'] = strVendor;
		}
		if (typeof strModel != 'undefined'){
			data['model'] = strModel;
		}
		if (typeof strYear != 'undefined'){
			data['year'] = strYear;
		}
		if (typeof strModif != 'undefined'){
			data['modif'] = strModif;
		}

		data['action_get_info'] = action;
		$container.setAjaxLoading();

		$.ajax({
            url: rz.AJAX_DIR + 'tiers_calc.php',
			data:data,
			dataType:'json',
			method: 'POST',
            success: function(result){
            	var modif,years,radius,height,width,models,objData,cntItems,messCount = '';
				if (!result.data_base.MODELS){
					objData = result.data_base;
				}else{
					models = getModels(result.data_base.MODELS)
                    objData = result.data_base.MODELS[0];
				}
                modif = objData.MODIFICATIONS;
                years = objData.YEARS;
                radius = objData.FRONTS_D ? objData.FRONTS_D : objData.TIERS_SIZES.FRONTS_D;
                height = objData.FRONT_P ? objData.FRONT_P : objData.TIERS_SIZES.FRONTS_P;
                width = objData.FRONTS_W ? objData.FRONTS_W : objData.TIERS_SIZES.FRONTS_W;
                cntItems = result.count ? result.count : 0;
                setHtmlOptions(modif,_$.calc_modif);
                setHtmlOptions(years,_$.calc_year);
                setHtmlOptions(models,_$.calc_model);
                setHtmlOptions(radius, _$.new_R);
                setHtmlOptions(height,_$.new_H);
                setHtmlOptions(width,_$.new_W);
                setHtmlOptions(radius, _$.old_R);
                setHtmlOptions(height,_$.old_H);
                setHtmlOptions(width,_$.old_W);
                messCount = BX.message('RZ_FINDED_ITEMS').replace('#COUNT#',cntItems);
                $container.find('.backend-cnt-items-calc').removeClass('hidden').find('.text').html(messCount);
				$container.find('select').trigger('refresh');
                $container.stopAjaxLoading();
                processFilter();
			}
		});
	};

	var doAjaxGetCountItems = function ($this) {
        var data = {},
            $container = $this.closest('.tab-pane');

        data['get_count'] = 'Y';
        data['width'] = _$.new_W.val();
        data['height'] = _$.new_H.val();
        data['radius'] = _$.new_R.val();

        $container.setAjaxLoading();

        $.ajax({
            url: rz.AJAX_DIR + 'tiers_calc.php',
            data:data,
            dataType:'json',
            method: 'POST',
            success: function(result){
            	if (result.success) {
                    var cntItems, messCount = '';
                    cntItems = result.count ? result.count : 0;
                    messCount = BX.message('RZ_FINDED_ITEMS').replace('#COUNT#', cntItems);
                    $container.find('.backend-cnt-items-calc').removeClass('hidden').find('.text').html(messCount);
                    $container.stopAjaxLoading();
                }
            }
        });
    };

})(jQuery);