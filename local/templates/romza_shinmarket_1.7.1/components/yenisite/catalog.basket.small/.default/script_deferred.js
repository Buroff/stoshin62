(typeof(jQuery) != 'undefined')
&& (function ($) {
	var $cart = $('#cartLine_Refresh'),
		data = [
			{name: "arParams", value: $cart.data('arparams')},
			{name: "template", value: $cart.data('template').toString()}
		];
	$(document).on('cartLine_Refresh', function () {
		$.ajax({
			type: "POST",
			url: rz.AJAX_DIR + "cartLine_Refresh.php",
			data: data,
			success: function(msg) {
				$cart.html(msg);
			}
		})
	});
})(jQuery);