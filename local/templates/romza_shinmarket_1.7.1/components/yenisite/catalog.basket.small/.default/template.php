<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use \Yenisite\Core\Tools;
$isAjax = Tools::isAjax();
if (!$isAjax && method_exists($this, 'setFrameMode')) $this->setFrameMode(true);

include_once $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/tools/include_module.php';
?>
<?if(!$isAjax): unset($arParams['ACTION_VARIABLE'],$arParams['PRODUCT_ID_VARIABLE'])?>
<div class="js-modal-basket btn btn-primary btn-cart" id="cartLine_Refresh" data-toggle="modal" data-target="#modal-basket"
	data-arparams='<?=Tools::GetEncodedArParams($arParams)?>' data-template='<?=$templateName?>'>
<?endif;?>
	<div class="flaticon-shopping16">
		<div class="badge">
			<? if (!$isAjax && method_exists($this, 'createFrame')) $frame = $this->createFrame()->begin(''); ?>
				<? if ($arParams['SHOW_PRODUCTS'] == "Y" || $arParams['SHOW_PRODUCTS'] == true) {
					$q = 0;
					if(!empty($arResult['ITEMS'])) {
						foreach ($arResult['ITEMS'] as &$arProduct) {
							$q += $arProduct['COUNT'];
						} unset($arProduct);
					}
					echo $q;
				} else {
					echo $arResult['COMMON_COUNT'];
				}
				?>
			<? if (!$isAjax && method_exists($this, 'createFrame')) $frame->end(); ?>
		</div>
	</div>
	<span class="basket-title"><?=GetMessage('RZ_CART')?></span>
<?if(!$isAjax):?>
</div>
<?endif;?>