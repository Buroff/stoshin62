<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Yenisite\Core\Resize;
use \Yenisite\Core\Tools;
include_once $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/tools/include_module.php';
$isAjax = Tools::isAjax();
if (!$isAjax && method_exists($this, 'setFrameMode')) $this->setFrameMode(true); ?>
<? if ($isAjax): ?>
	<? if (empty($arResult["ERROR"])): ?>
		<?
		$numProd = $arResult['COMMON_COUNT'];
		?>
		<span class="close flaticon-delete30" data-dismiss="modal"></span>
		<? if (count($arResult['ITEMS']) > 0): ?>
			<h2 id="basket_modalLabel">
				<?= GetMessage("RZ_COUNT_ITEMS_TITLE", array("#NUM#" => $numProd . ' ' . Tools::rusQuantity($numProd, GetMessage("RZ_TOVAR")))) ?>
				<span class="brand-primary-color"><?= Tools::FormatPrice($arResult['COMMON_PRICE']) ?></span>
			</h2>
			<form class="basket_order_form" role="form">
				<table class="basket_order__head">
					<thead>
					<tr>
						<th class="title"><?=GetMessage("RZ_TOVAR")?></th>
						<th class="amount"><?=GetMessage("RZ_KOLICHESTVO")?></th>
						<th class="price"><?=GetMessage("RZ_TCENA_ZA_ED_")?></th>
					</tr>
					</thead>
				</table>

				<div class="basket_order__main_wrapper">
					<div class="basket_order__main_inner">
						<div class="basket_order__main_inner_2">
							<table class="basket_order__main">
								<tbody>
								<? foreach ($arResult['ITEMS'] as $arItem): ?>
									<tr>
										<td class="img">
											<img src="<?= Resize::GetResizedImg($arItem['FIELDS'],
												array('WIDTH' => 80, 'HEIGHT' => 80, 'SET_ID' => intval($arParams['RESIZER_PRODUCT']))) ?>"
												 alt="<?= $arItem['FIELDS']['NAME'] ?>" title="<?= $arItem['FIELDS']['NAME'] ?>">
										</td>
										<td class="title">
											<a href="<?= $arItem['FIELDS']['DETAIL_PAGE_URL'] ?>"><?= $arItem['FIELDS']['NAME'] ?></a>
										</td>
										<td class="amount">
											<input class="js-touchspin" type="text" value="<?= $arItem['COUNT'] ?>"
												name="QUANTITY_<?=$arItem['KEY']?>"  title="">
										</td>
										<td class="price"><?= Tools::FormatPrice($arItem['MIN_PRICE']) ?></td>
										<?/*?>
										<td class="put_off">
											<a class="put_off_link" href="#" data-id="<?= $arItem['ID'] ?>">
												<span class="icon icon_arrow652"></span>
												<?= GetMessage('RZ_SET_ASIDE') ?>
											</a>
										</td>
										<?*/?>
										<td class="delete">
											<a class="delete_link" href="#" data-id="<?= $arItem['KEY'] ?>">
												<span class="icon icon_recycling10"></span>
												<?= GetMessage('RZ_REMOVE') ?>
											</a>
										</td>
									</tr>
								<? endforeach ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="basket_order__foot">
					<div class="basket_order__submit column">
						<?
						$orderURL = Tools::GetConstantUrl($arParams['PATH_TO_BASKET']);
						?>
						<a class="btn btn-primary btn-order" href="<?= $orderURL ?>">
							<?= GetMessage('RZ_GET_ORDER') ?>
						</a>
					</div>
					<div class="basket_order__total column">
						<?= GetMessage('RZ_SUMM_TITLE') ?><span
							class="total_price brand-primary-color"><?= Tools::FormatPrice($arResult['COMMON_PRICE']) ?></span>
					</div>
				</div>
			</form>
		<? else: ?>
			<h2><?=GetMessage("RZ_VASHA_KORZINA_PUSTA")?></h2>
		<? endif; ?>
	<? else: ?>
		<div class="message message-error">
			<? ShowError($arResult["ERROR"]); ?>
		</div>
	<?endif ?>
<? else: ?>
	<? if (method_exists($this, 'setFrameMode')) $this->setFrameMode(true); ?>
	<div class="modal fade" id="modal-basket" tabindex="-1" role="dialog" <?/*aria-labelledby="basket_modalLabel"*/?> aria-hidden="true"
		 data-arparams="<?= Tools::GetEncodedArParams($arParams) ?>" data-template="<?= $templateName ?>"
		 data-actionvar="action"
		>
		<div class="container">
			<div class="popup modal-dialog popup_basket">
			</div>
		</div>
	</div>
<?endif ?>