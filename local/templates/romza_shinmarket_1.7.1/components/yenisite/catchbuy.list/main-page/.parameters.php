<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;

if (!Loader::includeModule('iblock'))
	return;

$arTemplateParameters["PROPERTY_TITLE"] = array(
	"PARENT" => "VISUAL",
	"NAME" => GetMessage("CATCHBUY_TITLE"),
	"TYPE" => "STRING",
	"DEFAULT" => GetMessage('DEF_CATCHBUY_TITLE'),
);

if(Loader::includeModule('yenisite.oneclick')){
	$arTemplateParameters['USE_ONECLICK'] = array(
		'PARENT' => 'BASE',
		'NAME' => GetMessage('RZ_USE_ONECLICK'),
		'TYPE' => 'CHECKBOX',
		'DEFAULT' => 'Y'
	);
}
$arTemplateParameters['MESS_BTN_BUY'] = array(
	'PARENT' => 'VISUAL',
	'NAME' => GetMessage('CP_BCT_TPL_MESS_BTN_BUY'),
	'TYPE' => 'STRING',
	'DEFAULT' => GetMessage('CP_BCT_TPL_MESS_BTN_BUY_DEFAULT')
);
$arTemplateParameters['MESS_BTN_ADD_TO_BASKET'] = array(
	'PARENT' => 'VISUAL',
	'NAME' => GetMessage('CP_BCT_TPL_MESS_BTN_ADD_TO_BASKET'),
	'TYPE' => 'STRING',
	'DEFAULT' => GetMessage('CP_BCT_TPL_MESS_BTN_ADD_TO_BASKET_DEFAULT')
);
$arTemplateParameters['MESS_BTN_DETAIL'] = array(
	'PARENT' => 'VISUAL',
	'NAME' => GetMessage('CP_BCT_TPL_MESS_BTN_DETAIL'),
	'TYPE' => 'STRING',
	'DEFAULT' => GetMessage('CP_BCT_TPL_MESS_BTN_DETAIL_DEFAULT')
);
$arTemplateParameters['MESS_NOT_AVAILABLE'] = array(
	'PARENT' => 'VISUAL',
	'NAME' => GetMessage('CP_BCT_TPL_MESS_NOT_AVAILABLE'),
	'TYPE' => 'STRING',
	'DEFAULT' => GetMessage('CP_BCT_TPL_MESS_NOT_AVAILABLE_DEFAULT')
);

if (\Bitrix\Main\Loader::includeModule('yenisite.core')) {
	\Yenisite\Core\Resize::AddResizerParams(array('PRODUCT', 'PRODUCT_BIG', 'PRODUCT_THUMB'), $arTemplateParameters);
}

$arTemplateParameters["DISPLAY_TOP_PAGER"]['HIDDEN'] = 'Y';
$arTemplateParameters["DISPLAY_BOTTOM_PAGER"]['HIDDEN'] = 'Y';

