<?
// Resizer
$MESS['RESIZER_PRODUCT'] = 'Для фото товара в списке';
$MESS['RESIZER_PRODUCT_BIG'] = 'Для главного фото товара в карточке';
$MESS['RESIZER_PRODUCT_THUMB'] = 'Для миниатюр в в галерее';
$MESS['RESIZER_PRODUCT_GALLERY'] = 'Для больших фото в галерее';
$MESS["DISPLAY_FAVORITE"] = "Отображать кнопки добавления в избранное";
$MESS["RZ_USE_ONECLICK"] = "Отображать кнопку купить в 1 клик";
$MESS["CATCHBUY_TITLE"] = "Заголовок для блок ауспей купить";
$MESS["DEF_CATCHBUY_TITLE"] = "Успей купить";
$MESS["CP_BCT_TPL_MESS_BTN_BUY"] = "Текст кнопки \"Купить\"";
$MESS["CP_BCT_TPL_MESS_BTN_ADD_TO_BASKET"] = "Текст кнопки \"Добавить в корзину\"";
$MESS["CP_BCT_TPL_MESS_BTN_DETAIL"] = "Текст кнопки \"Подробнее\"";
$MESS["CP_BCT_TPL_MESS_NOT_AVAILABLE"] = "Сообщение об отсутствии товара";
$MESS["CP_BCT_TPL_MESS_BTN_BUY_DEFAULT"] = "Купить";
$MESS["CP_BCT_TPL_MESS_BTN_ADD_TO_BASKET_DEFAULT"] = "В корзину";
$MESS["CP_BCT_TPL_MESS_BTN_DETAIL_DEFAULT"] = "Подробнее";
$MESS["CP_BCT_TPL_MESS_NOT_AVAILABLE_DEFAULT"] = "Нет в наличии";