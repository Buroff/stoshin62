<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
global $rz_options;
$this->setFrameMode(true);

$arParams['SHOW_ONE_CLICK'] = $rz_options['main_one_click'] != 'N';
$arParams['SHOW_CATCH_BUY'] = $rz_options['main_catch_buy_block'] != 'N';
$arParams['CATCH_BUY_BLOCK'] = true;

$APPLICATION->IncludeComponent('romza:catalog.top', $arParams['CATALOG_TEMPLATE'], $arParams, $component);
