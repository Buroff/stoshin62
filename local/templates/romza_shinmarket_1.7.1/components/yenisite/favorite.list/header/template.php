<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

use Yenisite\Core\Ajax;
use Yenisite\Core\Tools;


$arParams['PATH_TO_FAVORITE'] = $arParams['PATH_TO_FAVORITE'] ? : 'personal/wishlist';

include $_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH.'/include/debug_info.php';

$id = 'wishListData';
$arParams['FAVORITE_CONATINER'] = $id;?>

<? $isAjax = Tools::isAjax();
if (!$isAjax)
    Ajax::saveParams($this, $arParams, 'favorite', SITE_ID);

if (!$isAjax):?>
    <a class="btn top-favorite-wrap" <? Ajax::printAjaxDataAttr($this, 'favorite') ?> id="<?=$id?>" href="<?=SITE_DIR.$arParams['PATH_TO_FAVORITE']?>" title="<?= GetMessage("RZ_PEREJTI_V_IZBRANNOE") ?>">
<?endif;?>
<?if ($isAjax){
    $arParams['CACHE_TIME'] = 0;
    $arCatalogParams = Ajax::getParams('bitrix:catalog', 'main_catalog', '', SITE_ID);
    if (!empty($arCatalogParams['IBLOCK_ID'])){
        $arParams['IBLOCK_ID'] = $arCatalogParams['IBLOCK_ID'];
    }
}?>
    <span class="icon icon_heart-empty"></span>
   <? $APPLICATION->IncludeComponent('bitrix:catalog.section', $arParams['CATALOG_TEMPLATE'], $arParams, $component);?>
<?if (!$isAjax):?>
    </a>
<?endif?>



