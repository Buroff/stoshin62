<?
$MESS ['CAPTCHA_TITLE'] = 'Введите код';
$MESS ['SECTION_SELECT'] = 'Выберите раздел';
$MESS ['MESSAGE'] = 'Сообщение';
$MESS ['SEND'] = 'Перезвоните мне';
$MESS ['ERROR'] = 'Ошибка!';
$MESS ['REQUIRED'] = 'Поля, отмеченные звездочкой обязательны для заполнения';