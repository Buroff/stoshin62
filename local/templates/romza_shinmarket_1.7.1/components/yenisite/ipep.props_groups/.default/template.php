<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
} ?>
<? if (method_exists($this, 'setFrameMode')) $this->setFrameMode(true); ?>

<? foreach ($arResult['GROUPS'] as $groupID => $arGroup): ?>
	<div class="techdata-block">
		<? if ($groupID > 0): ?>
			<h3><?= $arGroup['NAME'] ?></h3>
		<? endif ?>
		<ul class="item_specification__list">
			<? foreach ($arGroup['PROPS'] as $pid): ?>
				<li>
					<?
					$arProperty = $arResult['DISPLAY_PROPERTIES'][$pid];
					?>
					<?if(empty($arProperty['HINT'])):?>
						<?= $arProperty["NAME"] ?>
					<?else:?>
						<span <?if(!empty($arProperty["HINT"])):?> class="hint-have"<?endif?> title="<?=$arProperty["HINT"]?>"><?= $arProperty["NAME"] ?></span>
					<?endif?>
					<span class="divider">---------</span>
					<b>
						<?
						if ($arProperty['PROPERTY_TYPE'] == 'L'):
							if (is_array($arProperty['DISPLAY_VALUE'])):
								foreach ($arProperty['DISPLAY_VALUE'] as $n => $value):
									echo $n > 0 ? ', ' : '';
									echo $arProperty['DISPLAY_VALUE'][$n];
								endforeach;
							else:
								echo $arProperty['DISPLAY_VALUE'];
							endif;
						else:
							if (is_array($arProperty["DISPLAY_VALUE"]) && $arProperty['PROPERTY_TYPE'] != 'F'):
								foreach ($arProperty["DISPLAY_VALUE"] as &$p) {
									if (substr_count($p, "a href") > 0) {
										$p = strip_tags($p);
									}
								}
								echo implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]);
							elseif ($pid && $pid == "MANUAL"):
								?>
								<a href="<?= $arProperty["VALUE"] ?>"><?= GetMessage("CATALOG_DOWNLOAD") ?></a><?
							elseif ($arProperty['PROPERTY_TYPE'] == 'F'):
								if ($arProperty['MULTIPLE'] == 'Y'):
									if (is_array($arProperty['DISPLAY_VALUE'])):
										foreach ($arProperty['DISPLAY_VALUE'] as $n => $value):
											echo $n > 0 ? ', ' : '';
											echo str_replace('</a>', ' ' . $arProperty['DESCRIPTION'][$n] . '</a>', $value);
										endforeach;
									else:
										echo str_replace('</a>', ' ' . $arProperty['DESCRIPTION'][0] . '</a>', $arProperty['DISPLAY_VALUE']);
									endif;
								else:
									echo str_replace('</a>', ' ' . $arProperty['DESCRIPTION'] . '</a>', $arProperty['DISPLAY_VALUE']);
								endif;
							else:
								if (substr_count($arProperty["DISPLAY_VALUE"], "a href") > 0) {
									$arProperty["DISPLAY_VALUE"] = strip_tags($arProperty["DISPLAY_VALUE"]);
								}
								if (isset($arProperty['ICON_CLASS']) && strlen($arProperty['ICON_CLASS']) > 0) {
									echo '<span class="icon ',$arProperty['ICON_CLASS'],'" title="',$arProperty["DISPLAY_VALUE"],'"></span>';
								} else {
									echo $arProperty["DISPLAY_VALUE"];
								}
								if (false && $arParams['SHOW_PROPERTY_VALUE_DESCRIPTION'] != 'N') {
									echo ' ', $arProperty['DESCRIPTION'];
								}
							endif;
						endif;
						?>
					</b>
				</li>
			<? endforeach ?>
		</ul>
	</div><!-- .techdata-block -->
<? endforeach ?>
		

