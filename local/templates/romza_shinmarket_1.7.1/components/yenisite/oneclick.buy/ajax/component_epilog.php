<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use \Yenisite\Core\Tools;
include_once $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/tools/include_module.php';
Tools::addComponentDeferredJS($templateFile);
global $bRzHasModal;
if (!is_bool($bRzHasModal)) {
	$bRzHasModal = false;
}
if (!$bRzHasModal) {
	$this->initComponentTemplate();
	$this->__template->SetViewTarget('modal_oneclick');
	?>
		<div class="modal fade" id="rz_modal-oneclick" tabindex="-1" role="dialog" <?//aria-labelledby="oneclick"?> aria-hidden="true">
			<div class="popup modal-dialog">
				<span class="close flaticon-delete30" data-dismiss="modal"></span>
				<h4 class="modal-title" id="oneclick_modalLabel"><?=GetMessage("RZ_BISTRIJ_ZAKAZ")?></h4>
				<div class="modal-body"></div>
			</div>
		</div>
		<?
	$this->__template->EndViewTarget();
	$bRzHasModal = true;
}