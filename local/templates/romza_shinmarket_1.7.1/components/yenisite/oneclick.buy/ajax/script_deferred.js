(typeof(jQuery) != 'undefined')
&& (function ($) {
	if (typeof rzSingleOne == 'undefined') {
		rzSingleOne = {};
	}
	if (!('scriptLoaded' in rzSingleOne)) {
		var $modal = $('#rz_modal-oneclick'),
			$modalContent = $modal.find('.modal-body'),
			$body = $('body');
		$body.on('click', '.rz_oneclick-buy .do-order,  .buy-one-click .do-order', function (e) {
			var $this = $(this);
			if ($this.hasClass('disabled') || $this.is('[disabled]')) {
				return false;
			}
			var data = [
				{name: 'arparams', value: $this.data('arparams')},
				{name: 'template', value: $this.data('template').toString()},
				{name: 'RZ_BASKET', value: $this.attr('data-basket')},
				{name: 'NAME', value: $this.attr('data-name')},
				{name: 'IS_AJAX', value: 'Y'},
				{name: 'URL', value: rzSingleOne.URL}
			];
			return $.ajax({
				type: "POST",
				url: rzSingleOne.AJAX_URL,
				data: data,
				success: function (msg) {
					$modalContent.html(msg);
                    $modalContent.refreshForm();
					$modalContent.data('arparams', $this.data('arparams'));
					$modalContent.data('template', $this.data('template').toString());
					$modal.modal('show');
				}
			});
		});
		$modalContent.on('submit', 'form', function (e) {
			e.preventDefault();
			var data = $(this).serializeArray();
			data.push({name: 'arparams', value: $modalContent.data('arparams')});
			data.push({name: 'template', value: $modalContent.data('template').toString()});
			data.push({name: 'URL', value: rzSingleOne.URL});
			data.push({name: 'BUY_SUBMIT', value: 'Y'});
			return $.ajax({
				type: "POST",
				url: rzSingleOne.AJAX_URL,
				data: data,
				success: function (msg) {
					$modalContent.html(msg);
                    $modalContent.refreshForm();
					$(document).trigger('cartLine_Refresh');
					var $modalBasket = $('#modal-basket'),
						$modalBaskerContent = $modalBasket.find('.popup_basket');
					if($('#order_block_step_1').length) $(document).trigger('BigBasket_Refresh');
					if($modalBaskerContent.length) $(document).trigger('cartPopup_Refresh');
				}
			});
		});
		rzSingleOne.scriptLoaded = true;
	}
})(jQuery);
