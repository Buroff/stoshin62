<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
if (count($arParams['EDIT_SETTINGS']) > 0 && defined('IS_DEMO')) {
	$Asset = \Bitrix\Main\Page\Asset::getInstance();
	$Asset->addJs($templateFolder . '/script_deferred.js');
}