<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if(method_exists($this, 'setFrameMode')) $this->setFrameMode(true);?>

<div class="goods_item__labels">
	<?if($arResult["NEW"]):?>
		<span class="label label-new"><?=GetMessage('STICKER_SECT_NEW')?></span>
	<?endif?>
	<?if($arResult["HIT"]):?>
		<span class="label label-hit"><?=GetMessage('STICKER_SECT_HIT')?></span>
	<?endif?>
	<?if($arResult["SALE"]):?>
		<span class="label label-discount"><?=GetMessage('STICKER_SECT_SALE')?></span>
	<?endif;?>
	<?if($arResult["BESTSELLER"]):?>
		<?/*<div class="sticker sticker-must"><?=GetMessage('STICKER_SECT_BESTSELLER')?></div>*/?>
	<?endif?>
	<?if($arResult["CATCHBUY"] && empty($arParams['NOT_SHOW'])):?>
		<span class="label label-hit"><?=GetMessage('STICKER_SECT_CATCHBUY')?></span>
	<?endif?>
</div>




