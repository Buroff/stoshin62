<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use \Yenisite\Core\Tools;
global $rz_options;
if (!Tools::isEditModeOn()){
    ob_start();
    Tools::IncludeArea('universal','email');
    $mail = ob_get_clean();
}
?>
<? include_once $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/tools/include_module.php'; ?>
		</div><!--.main_content-->
	<? Tools::showViewContent('footer_container') ?>
		</div><!-- .page_content_wrap -->
		<div class="page_footer">
			<div class="container">
				<div class="page_footer__inner">
					<div class="column page_footer__shop_info_wrapper">
						<?$APPLICATION->IncludeComponent("bitrix:main.include", "", Array("PATH" => SITE_DIR."include_areas/footer/about.php", "AREA_FILE_SHOW" => "file", "EDIT_TEMPLATE" => ""), false);?>
						<div class="page_footer__logo_wrapper">
							<a class="page_footer__logo" href="#">
								<?$APPLICATION->IncludeComponent("bitrix:main.include", "",
									Array("PATH" => SITE_DIR."include_areas/universal/main-logo.php", "AREA_FILE_SHOW" => "file", "EDIT_TEMPLATE" => ""), false);?>
								<span class="page_footer__logo_title">
									<?$APPLICATION->IncludeComponent("bitrix:main.include", "",
										Array("PATH" => SITE_DIR."include_areas/universal/main-title.php", "AREA_FILE_SHOW" => "file", "EDIT_TEMPLATE" => ""), false);?>
								</span>
							</a>
						</div>
						<div class="page_footer__shop_info">
							<?$APPLICATION->IncludeComponent("bitrix:main.include", "", Array("PATH" => SITE_DIR."include_areas/footer/shop_info.php", "AREA_FILE_SHOW" => "file", "EDIT_TEMPLATE" => ""), false);?>
						</div>
					</div>
					
					<div class="column page_footer__nav_wrapper">
						<? Tools::IncludeArea('footer', 'menu', array(), true) ?>
					</div>
					
					<div class="column page_footer__contacts">
						<?$APPLICATION->IncludeComponent("bitrix:main.include", "", Array("PATH" => SITE_DIR."include_areas/footer/contact.php", "AREA_FILE_SHOW" => "file", "EDIT_TEMPLATE" => ""), false);?>
						<div class="contacts_block">
							<address>
								<a class="email" href="mailto:<?=$mail?>">
									<span class="contacts__icon icon icon_email20"></span>
									<?$APPLICATION->IncludeComponent("bitrix:main.include", "", Array("PATH" => SITE_DIR."include_areas/universal/email.php", "AREA_FILE_SHOW" => "file", "EDIT_TEMPLATE" => ""), false);?>
								</a>
							</address>
							<address>
								<a class="phone" data-toggle="modal" data-target="#modal-callme">
									<span class="contacts__icon icon icon_tablet5"></span>
									<?$APPLICATION->IncludeComponent("bitrix:main.include", "", Array("PATH" => SITE_DIR."include_areas/universal/phone.php", "AREA_FILE_SHOW" => "file", "EDIT_TEMPLATE" => ""), false);?>
								</a>
							</address>
							<address>
								<div class="address">
									<span class="contacts__icon icon icon_location4"></span>
									<?$APPLICATION->IncludeComponent("bitrix:main.include", "", Array("PATH" => SITE_DIR."include_areas/universal/location.php", "AREA_FILE_SHOW" => "file", "EDIT_TEMPLATE" => ""), false);?>
								</div>
							</address>
						</div>

						<?$APPLICATION->IncludeComponent("bitrix:main.include", "", Array("PATH" => SITE_DIR."include_areas/footer/social.php", "AREA_FILE_SHOW" => "file", "EDIT_TEMPLATE" => ""), false);?>
						<div class="page_footer__share">
							<?$APPLICATION->IncludeComponent("bitrix:main.include", "", Array("PATH" => SITE_DIR."include_areas/footer/social_buttons.php", "AREA_FILE_SHOW" => "file", "EDIT_TEMPLATE" => ""), false);?>
						</div>
					</div>
				</div>
			</div>
			<div class="page_footer__copyrights_wrapper">
				<div class="container">
					<?$APPLICATION->IncludeComponent("bitrix:main.include", "", Array("PATH" => SITE_DIR."include_areas/footer/copyright-1.php", "AREA_FILE_SHOW" => "file", "EDIT_TEMPLATE" => ""), false);?>
					<span class="page_footer__shop_name">
						<?$APPLICATION->IncludeComponent("bitrix:main.include", "", Array("PATH" => SITE_DIR."include_areas/footer/copyright-2.php", "AREA_FILE_SHOW" => "file", "EDIT_TEMPLATE" => ""), false);?>
					</span>&nbsp;
					<?$APPLICATION->IncludeComponent("bitrix:main.include", "", Array("PATH" => SITE_DIR."include_areas/footer/copyright-3.php", "AREA_FILE_SHOW" => "file", "EDIT_TEMPLATE" => ""), false);?>
					<div id="bx-composite-banner"></div>
				</div>
			</div>
		</div>
	</div> <?/* .page_wrapper */?>
	<div class="overlay"></div>

	<?$bVertical = false;?>
	<?if($rz_options['switch_main_menu'] == 'vertical' && !defined('IS_CATALOG') && defined('IS_MAIN_PAGE')){
		$bVertical = true;
	} elseif($rz_options['switch_main_menu'] == 'vertical' && defined('IS_CATALOG') && $rz_options['switch_filter'] != 'top' ){
		$bVertical = true;
	}?>

		<?ob_start()?>
				<? Tools::IncludeArea('header', 'menu_horizon', array(), true) ?>
		<?$content = ob_get_clean()?>
		<?if($bVertical):?>
			<?$tmPContent = $APPLICATION->GetViewContent('vertical_menu');
            if (empty($tmPContent)) {
                $APPLICATION->AddViewContent('vertical_menu', $content);
            }?>
		<?else:?>
			<?$tmPContent = $APPLICATION->GetViewContent('default_menu');
            if (empty($tmPContent)) {
                $APPLICATION->AddViewContent('default_menu', $content);
            }?>
		<?endif?>

	<?$APPLICATION->IncludeComponent("bitrix:main.include", "", Array("AREA_FILE_SHOW" => "file",	"PATH" => SITE_DIR."include_areas/site_template/call_order.php",	"EDIT_TEMPLATE" => "include_areas_template.php"	), false, array("HIDE_ICONS"=>"Y"));?>
	
<?$APPLICATION->IncludeComponent("bitrix:main.include", "", Array("AREA_FILE_SHOW" => "file",	"PATH" => SITE_DIR."include_areas/site_template/basket_popup.php",	"EDIT_TEMPLATE" => "include_areas_template.php"	), false, array("HIDE_ICONS"=>"Y"));?>

	<?
	global $arModals;
	if(is_array($arModals) && count($arModals) > 0) {
		foreach ($arModals as $modalContent) {
			echo $modalContent;
		}
	}
	?>
	<?$APPLICATION->ShowViewContent('modal_lightbox');?>
	<?$APPLICATION->ShowViewContent('modal_locator');?>
	<?$APPLICATION->ShowViewContent('modal_register');?>
	<?$APPLICATION->ShowViewContent('modal_auth');?>
	<?$APPLICATION->ShowViewContent('price_follow_Modal');?>
	<?$APPLICATION->ShowViewContent('modal_constructor');?>
	<?$APPLICATION->ShowViewContent('modal_oneclick');?>
<?$APPLICATION->ShowViewContent('modal_flashmessage');?>
	<script type="text/javascript">
		try {
			hasJqueryLoaded = $.fn.jquery;
		} catch(e) {}
	</script>
	<script src="<?= SITE_TEMPLATE_PATH ?>/js/jquery-1.11.2.min.js"></script>
<?include "tools/js_main.php";?>
	<script src="<?= SITE_TEMPLATE_PATH ?>/js/script_combined.js?v=<?= filemtime($_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/js/script_combined.js'); ?>"></script>
	<script src="<?= SITE_TEMPLATE_PATH ?>/js/script_addon.js?v=<?= filemtime($_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/js/script_addon.js'); ?>"></script>
	<?Tools::showDeferredJS()?>
	<script type="text/javascript">
		if (typeof hasJqueryLoaded != 'undefined') {
			newJq = $.noConflict(true);
		}
	</script>
<?
$sessid = bitrix_sessid();
global $APPLICATION;
$callbaCookie = $APPLICATION->get_cookie($sessid); ?>
<? if ($_GET['callback'] == 'Y' || !empty($callbaCookie)): ?>
    <? $APPLICATION->set_cookie($sessid, 156); ?>
    <? Tools::IncludeArea('footer', 'callbackform'); ?>
<? endif ?>
	</body>
</html>