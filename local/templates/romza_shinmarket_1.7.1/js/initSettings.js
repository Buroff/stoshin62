// these are GLOBALS FOR PURPOSE!
settings = {
	filterPosition: 'left',
	mainmenuType: 'default',
	colorScheme: 'blue',
	isFrontend: true
};

// this function is called on .ready event
function initSettings(){
	$('#modal-settings').on('change', function(e){
		var $t = $(e.target),
			name = $t.data('name') || $t.attr('name'),
			value = $t.val();

		if (value === undefined || name === undefined) return;
		if (value === "false") value = false;
		else if (value === "true") value = true;
		
		if ($t.is('input:checkbox')) value = $t.prop('checked');
		// ^ using input here because selectors :checkbox or :checked are equal to
		// *:checkbox or *:checked, and are SLOW because of universal *. But with
		// input, we are much more specific.

		// if (settings[name] === value) return;
		// ^ moved such checks to set functions

		// console.log('set', name, 'to', value);
		
		if (typeof set[name] === 'function') set[name](value);
		settings.isFrontend && localStorage.setItem(name, value);
		settings[name] = value;
	});
}

if (settings.isFrontend){
	(function(){
		function checkSettings(){
			var changed = {};
			for (var prop in settings){
				var curVal = settings[prop],
					value = localStorage.getItem(prop);

				if (value === 'true') value = true;
				if (value === 'false') value = false;

				if (value && (value !== curVal)){
					// saving changed to, uhm, changed for updating DOM after doc ready
					if (typeof set[prop] === 'function') set[prop](value);
					changed[prop] = value;
					settings[prop] = value;
				}
			}

			// switch DOM state after doc ready
			$(document).one('ready', function(){
				var $elDOM, $input, inputType, value;
				for (var prop in changed){
					value = changed[prop];

					// looking for associated DOM element and updating data-<name>
					$elDOM = $('[data-' + prop + ']');
					if ( $elDOM.length > 0 ) $elDOM.attr('data-' + prop, value);
					
					$input = $('[name=' + prop + '], [data-name=' + prop + ']');
					if (!$input.length) return;
					
					inputType = ( $input.is('select') ) ? 'select' : $input.attr('type');
					switch (inputType){
						case 'select':
							$input.val(value).change(); // .change for styling plugins
							break;
						case 'text':
							$input.val(value);
							break;
						case 'radio':
							$input.filter('[value="' + value + '"]').attr('checked', true);
							break;
						case 'checkbox':
							$input.prop('checked', value);
							break;
						default:
							console.log('unknown input type in settings set: ', inputType);
					}
				}

				$('input[type="radio"].switcher_radio').styler({
					selectSearch: false,
					onSelectOpened: function() {
						$(this).css('z-index', '300');
						$(this).find('.jq-selectbox__dropdown').jScrollPane();
					},
					onSelectClosed: function() {
						$(this).css('z-index', '100');
					},
					onFormStyled: function() {
						$('.jq-checkbox .jq-checkbox__div').addClass('flaticon-check34');
					}
				});
			});
		}

		checkSettings();
	})();
}