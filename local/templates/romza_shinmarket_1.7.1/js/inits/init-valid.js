function initValid (modal,$){
    var $modal = $(modal),
        $form = $modal.is('form') ? $modal : $modal.find('.modal-form');

    // it is needed here, on modal open, to disable button and validator
    // to work properly.
    // See https://github.com/1000hz/bootstrap-validator/issues/336
    $form.validator('destroy').validator({
        'focus': false
    });

    $('.modal').off('shown.bs.modal').on('shown.bs.modal', function(){
        initValid(this,$);
    });
}