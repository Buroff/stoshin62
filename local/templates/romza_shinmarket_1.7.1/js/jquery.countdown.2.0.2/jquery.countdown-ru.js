﻿/* http://keith-wood.name/countdown.html
 * Russian initialisation for the jQuery countdown extension
 * Written by Sergey K. (xslade{at}gmail.com) June 2010. */
(function($) {
	$.countdown.regionalOptions['ru'] = {
		labels: [BX.message('APPR_YEARS'), BX.message('APR_MONTH'), BX.message('APR_WEEK'), BX.message('APR_DAYS'), BX.message('APR_HOURS'),BX.message('APR_MINUTS'), BX.message('APR_SECONDS')],
		labels1: [BX.message('APR_YEAR'), BX.message('APR_MONTH'), BX.message('APR_WEEK'), BX.message('APR_DAY'), BX.message('APR_HOUR'), BX.message('APR_MINUTS'), BX.message('APR_SECONDS')],
		labels2: [BX.message('APR_YEARAS'), BX.message('APR_MONTH'), BX.message('APR_WEEK'), BX.message('APR_DAYN'), BX.message('APR_HOURSES'),BX.message('APR_MINUTS'), BX.message('APR_SECONDS')],
		compactLabels: [BX.message('APR_L'), BX.message('APR_M'), BX.message('APR_N'),BX.message('APR_D')], compactLabels1: [BX.message('APR_G'), BX.message('APR_M'), BX.message('APR_N'),BX.message('APR_D')],
		whichLabels: function(amount) {
			var units = amount % 10;
			var tens = Math.floor((amount % 100) / 10);
			return (amount == 1 ? 1 : (units >= 2 && units <= 4 && tens != 1 ? 2 :
				(units == 1 && tens != 1 ? 1 : 0)));
		},
		digits: ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'],
		timeSeparator: ':', isRTL: false};
	$.countdown.setDefaults($.countdown.regionalOptions['ru']);
})(jQuery);