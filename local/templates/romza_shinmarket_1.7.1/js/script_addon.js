var setLocation = function (curLoc) {
	try {
		history.pushState(null, null, curLoc);
		return;
	} catch (e) {
	}
	location.hash = '#!' + curLoc.substr(1)
};
var addParameter = function (param, value) {
	var url = location.href;
	var val = new RegExp('(\\?|\\&)' + param + '=.*?(?=(&|$))'),
		parts = url.toString().split('#'),
		url = parts[0],
		hash = parts[1],
		qstring = /\?.+$/,
		newURL = url;

	// Check if the parameter exists
	if (val.test(url)) {
		// if it does, replace it, using the captured group
		// to determine & or ? at the beginning
		newURL = url.replace(val, '$1' + param + '=' + value);
	}
	else if (qstring.test(url)) {
		// otherwise, if there is a query string at all
		// add the param to the end of it
		newURL = url + '&' + param + '=' + value;
	}
	else {
		// if there's no query string, add one
		newURL = url + '?' + param + '=' + value;
	}

	if (hash) {
		newURL += '#' + hash;
	}

	setLocation(newURL);
};
var removeParameter = function (param) {
	var url = location.href;
	var val = new RegExp('(\\?|\\&)' + param + '=.*?(?=(&|$))'),
		parts = url.toString().split('#'),
		url = parts[0],
		hash = parts[1],
		newURL = url;

	// Check if the parameter exists
	if (val.test(url)) {
		// if it does, replace it, using the captured group
		// to determine & or ? at the beginning
		newURL = url.replace(val, '');
		if (hash) {
			newURL += '#' + hash;
		}
		setLocation(newURL);
	}
};
var getQueryVariable = function (variable, query, remove) {
	if (!query) {
		query = window.location.search.substring(1);
	} else {
		query = query.split('?')[1];
		if (!query) {
			return [];
		}
	}
	var arResult = [];
	if (query.length > 0) {
		var vars = query.split("&");

		for (var i = 0; i < vars.length; i++) {
			var pair = vars[i].split("=");
			if (variable && pair[0] == variable) {
				return pair[1];
			}
			if (typeof(remove) != 'undefined'
				&& pair[0] in remove) {
				continue;
			}
			arResult.push({name: pair[0], value: pair[1]});
		}
	}
	return (arResult);
};
var setCookie = function (name, value, options) {
	options = options || {};

	var expires = options.expires;

	if (typeof expires == "number" && expires) {
		var d = new Date();
		d.setTime(d.getTime() + expires * 1000);
		expires = options.expires = d;
	}
	if (expires && expires.toUTCString) {
		options.expires = expires.toUTCString();
	}

	value = encodeURIComponent(value);

	var updatedCookie = rz.COOKIE_PREFIX + name + "=" + value;

	for (var propName in options) {
		updatedCookie += "; " + propName;
		var propValue = options[propName];
		if (propValue !== true) {
			updatedCookie += "=" + propValue;
		}
	}

	document.cookie = updatedCookie;
};

var getCookie = function (name, defval) {
	if (typeof(defval) == 'undefined') {
		defval = undefined;
	}
	name = rz.COOKIE_PREFIX + name;
	var matches = document.cookie.match(new RegExp(
		"(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
	));
	return matches ? decodeURIComponent(matches[1]) : defval;
};

var getModels = function(elements){
    var models = [];
    $.each(elements,function(){
        if (this.NAME){
            models.push(this.NAME);
        }
    });
    return models;
};

var setHtmlOptions = function(elements,apendTo){
    var options = '';
    if (typeof elements != 'undefined'){
        $.each(elements,function(index,value){
            if (value) {
                options += '<option value="' + value + '">' + value + '</option>'
            }
        });
        apendTo.children().remove();
        apendTo.append($(options));
    }
};
var setHtmlForFilterCnt = function($form,cnt){
	if (typeof $form == 'undefined') return;
    cnt = cnt ? cnt : 0;
	var $container = $form.find('.backend-cnt-products');
    $container.removeClass('hidden');
    $container.find('.text').html(BX.message('RZ_FINDED_ITEMS').replace('#COUNT#',cnt));
};

$(function ($) {
	var $document = $(document),
		$miniCart = $('#cartLine_Refresh'),
		fontMainSlider = 36,
		heightSlider = 175,
		mobileHeightSlider = 96;

	$('[data-toggle="tooltip"]').tooltip();

	 var stylerOptions = {
		selectSearch: false,
		onSelectOpened: function() {
			$(this).css('z-index', '300');
			$(this).find('.jq-selectbox__dropdown').jScrollPane();
		},
		onSelectClosed: function() {
			$(this).css('z-index', '100');
		},
		onFormStyled: function() {
			$('.jq-checkbox .jq-checkbox__div').addClass('flaticon-check34');
		}
	};

    $.fn.refreshForm = function(){
    	var $this = $(this);
        if (typeof initValid == 'function'){
            initValid($this.parent(),$);
        }
        $this.find('input[type="radio"]:not(.switcher_radio), input[type="checkbox"]:not(.normal), select:not(.normal)').styler(stylerOptions);
	};

	$.fn.clearForm = function () {
		var $allInputs = this.find(':input').not(':button, :submit, :reset, :hidden');
		$allInputs.removeAttr('checked').removeAttr('selected');
		$allInputs.not(':checkbox, :radio, select').val('');
		$allInputs.trigger('refresh');
	};
	$.fn.selectSearch = function () {
		this.find('select.search').styler(stylerOptions);
	};
	$.fn.reMask = function () {
		this.find('input[name*=phone]').mask("+9 (999) 999-9999")
	};
	$.fn.reTimers = function(){
		$(document).find('.timer').each(function () {
			var $t = $(this);
			var liftoff = new Date($t.data('until'));
			$t.countdown({until: liftoff});
		});
	};
	$.fn.getSelOptFromForm = function($el){
		return $(this).closest('form').parent().find($el.selector + ' option:selected');
	};

	$.fn.heightControl = function(action, options){
		return this.each(function(){
			//==VARIABLES
			var that = this, $target = $(this);
			if (!$target.length) return;

			var $link = $target.parent().find('.height-toggle'),
				$content, heightLimit, contentHeight, resizeTimer;
			$content = (options && typeof options.content !== undefined) ? $target.find(options.content) : $target;

			//==METHODS
			this.update = function(){
				if (!$target.length) return;
				$target.css('max-height', '');
				$target.addClass('minified');

				// if (Modernizr.mq('(max-width: 767px)')){
				//  this.destroy();
				//  return;
				// }

				heightLimit = parseInt($target.css('max-height')) || $target.parent().outerHeight();
				contentHeight = $content.get(0).scrollHeight;
				// console.log('scrollHeight', contentHeight, 'heightLimit', heightLimit);

				/*if ( contentHeight - heightLimit > 0 ){
				 $target.addClass('minified expandable').css('max-height', heightLimit);
				 $link.show();
				 } else {
				 $target.removeClass('minified expandable')
				 $link.hide();
				 }*/

				$link.off('click.heightControl').on('click.heightControl', function(e){
					if (!$target.length) return false;
					if ( $target.hasClass('minified') ){
						$target.removeClass('minified').css({
							'max-height': contentHeight + 100
						});
						$link.find('span').html(BX.message('RZ_HIDE'));
					} else {
						$target.addClass('minified').css({
							'max-height': heightLimit
						});
						$link.find('span').html(BX.message('RZ_SHOW_FULL'));
					}
					return false;
				});
			}

			this.destroy = function(){
				$link.off('click.heightControl').hide();
				$target.css('max-height', '').removeClass('minified');
			}

			function resizeHandle(){
				clearTimeout(resizeTimer);

				resizeTimer = setTimeout(function(){
					that.update();
					$target.removeClass('minified').css('max-height',$target.get(0).scrollHeight);
					$link.find('span').html(BX.message('RZ_HIDE'));
				}, 250);
			}
			//==EVENTS

			$(window).on('resize', resizeHandle);
			//==ACTION
			if (!action || action === 'update') this.update();
			if (action === 'destroy') this.destroy();
		})
	}

	$.fn.reStyler = function () {
		this.find('input[type="radio"]:not(.switcher_radio), input[type="checkbox"]:not(.normal), select:not(.normal)').styler(stylerOptions);
		this.reMask();
		this.updateWishCompare();
	};
	var slideFunction = function ($sliders, step) {
		if (typeof(step) == 'undefined') {
			step = 100;
		}
		if ($sliders.length > 0) {
			$sliders.each(function () {
				var $this = $(this),
					$linkTo = $("#price"),
					maxVal = Number($this.data('max'));
				$this.noUiSlider({
					start: [maxVal],
					//connect: true,
					step: step,
					range: {
						'min': [0],
						'max': [maxVal]
					},
					format: wNumb({
						decimals: 0
					})
				});

				$this.Link('lower').to($linkTo);

				$this.Link('lower').to('-inline-<div class="tooltip"></div>', function (value) {
					// The tooltip HTML is 'this', so additional
					// markup can be inserted here.
					$(this).html('<span class="tooltip_inner">' + value + '</span>');
				});

			});
		}
	};
	$.fn.reSlide = function () {
		slideFunction(this.find('.js-price-slider'));
	};
	$.fn.reSpin = function () {
		this.find('.js-touchspin').TouchSpin({
			'buttondown_class': '',
			'buttonup_class': '',
			'buttondown_txt': '<span>-</span>',
			'buttonup_txt': '<span>+</span>'
		});
	};

	$.fn.reRate = function () {
		this.find('.rateit').rateit();
	};
	$.fn.checkFields = function () {
		var errCounter = 0;
		this.find('.required :input:not(:button,:hidden)').each(function () {
			var noErr = true;
			var $this = $(this),
				val = $this.val();
			if ($this.attr('type') == 'checkbox') {
				if (!$this.is(':checked')) {
					noErr = false;
				}
			} else {
				if (!$.trim(val)) {
					noErr = false;
				}
			}
			if (!noErr) {
				$this.closest('.form-group').addClass('has-error');
				++errCounter;
			} else {
				$this.closest('.form-group').removeClass('has-error');
			}
		});
		return (errCounter == 0);
	};
	var $ajaxLoader = $("<div class='ajax_loader brand-primary-color' data-counter='0'></div>"),
		$ajaxShadow = $("<div class='ajax_shadow'>"),
		$body = $('body');
	var AjaxTimer = null;
	$.fn.setAjaxLoading = function () {
		if (AjaxTimer) {
			clearInterval(AjaxTimer);
		}
		var offset = this.offset(),
			width = this.outerWidth(false),
			height = this.outerHeight(false);

		$ajaxLoader.css({
			width: width + 5,
			height: height + 5,
			left: offset.left - 5,
			top: offset.top - 5
		});
		$ajaxShadow.css({
			width: width + 5,
			height: height + 5,
			left: offset.left - 5,
			top: offset.top - 5,
			opacity: 0
		});
		$body.append($ajaxShadow).append($ajaxLoader);
		$ajaxShadow.animate({opacity: 0.9}, 400, 'linear');

		var i = 0;
		AjaxTimer = setInterval(function () {
			i++;
			i = i < 8 ? i : 0;
			$ajaxLoader.attr('data-counter', i);
		}, 100);

	};
	$.fn.stopAjaxLoading = function () {
		$ajaxShadow.stop(true, false);
		$ajaxShadow.animate({opacity: 0}, {
			duration: 400,
			easing: 'linear',
			complete: function () {
				$body.find('.ajax_shadow').remove();
				$body.find('.ajax_loader').remove();
				if (AjaxTimer) {
					clearInterval(AjaxTimer);
				}
			}
		});
	};
	$.fn.doAjaxAddToCart = function () {
		var $this = this;
		var data = $this.serializeArray();
		data.push({name: 'VARQ', value: $this.attr('data-varQ')});
		data.push({name: 'VARID', value: $this.attr('data-varId')});
		data.push({name: 'VARACT', value: $this.attr('data-varAct')});

		return $.ajax({
			dataType: "json",
			url: rz.AJAX_DIR + 'cartAdd.php',
			type: "GET",
			data: data,
			success: function (msg) {
				if (msg.success) {
					var imgtodrag = $this.closest('.goods_item').find("img:visible").eq(0);
					if (imgtodrag.length) {
						var imgclone = imgtodrag.clone()
							.offset({
								top: imgtodrag.offset().top,
								left: imgtodrag.offset().left
							})
							.css({
								'opacity': '0.5',
								'position': 'absolute',
								'height': imgtodrag.height() + 'px',
								'width': imgtodrag.width() + 'px',
								'z-index': '99999'
							})
							.appendTo($('body'))
							.animate({
								'top': $miniCart.offset().top + 10,
								'left': $miniCart.offset().left + 10,
								'width': 75,
								'height': 75
							}, 1000);

						/*setTimeout(function () {
						 cart.effect("shake", {
						 times: 2
						 }, 200);
						 }, 1500);*/

						imgclone.animate({
							'width': 0,
							'height': 0
						}, function () {
							$(this).detach()
						});
					}
					$document.trigger('cartLine_Refresh');
				} else {
					console.log(msg.msg);
				}
			}
		});
	};
	doAjaxAddToCartSimple = function (id) {
		var data = [
			{name: 'ID', value: id},
			{name: 'SIMPLE', value: 'Y'},
			{name: 'Q', value: 1}
		];
		return $.ajax({
			dataType: "json",
			url: rz.AJAX_DIR + 'cartAdd.php',
			type: "GET",
			data: data,
			success: function (msg) {
				if (msg.success) {
					$(document).trigger('cartLine_Refresh');
				} else {
					console.log(msg.msg);
				}
			}
		});
	};
	$.fn.updateWishCompare = function () {
		if (typeof(rz) == 'object' && 'WISH_LIST' in rz) {
			if (rz.WISH_LIST.length > 0) {
				this.find('.fav_id_' + rz.WISH_LIST.join(', .fav_id_')).each(function(){
					var $this = $(this);
					$this.addClass('active brand-primary-color').closest('.favourite_link').addClass('active');
				});
			}
		}
		if (typeof(rz) == 'object' && 'COMPARE_LIST' in rz) {
			if (rz.COMPARE_LIST.length > 0) {
				this.find('.compare_id_' + rz.COMPARE_LIST.join(', .compare_id_')).each(function(){
					var $this = $(this);
					$this.addClass('active brand-primary-color').closest('.compare_link').addClass('active');
				})
			}
		}
	};
	// style elems
	$('input[type="radio"], input[type="checkbox"]:not(.normal), select:not(.normal)').styler(stylerOptions);
	var $main_banner = $('.js-banners-rotator');
	if ($main_banner.length > 0) {
		$main_banner.slickSetOption('onBeforeChange', function (slickSlider, curIndex, targetIndex) {
			var $curSlide = $(slickSlider.$slides[targetIndex]);
			var $slickSlider = $(slickSlider);
			//cached
			var $container;
			if (!$slickSlider.data('current_container')) {
				$container = $curSlide.closest('.banners_rotator_block').find('.banners_rotator__inner__head');
				$slickSlider.data('current_container', $container);
			} else {
				$container = $slickSlider.data('current_container');
			}
			$container.find('.big').text($curSlide.data('name'));
			$container.find('.small').text($curSlide.data('text'));
		}, true);
	}
	var $slickSliderContainers = $('.js-slider_goods_list');
	var $currentSliders = $slickSliderContainers.find('.slick-initialized.slick-slider');
	var toggleSliderArrows = function (slick, maxcount) {
		if (typeof(maxcount) == 'undefined') {
			maxcount = 4;
		}
		var $this = slick.$list,
			$arrows = $this.closest('.js-slider_goods_list').find('.btn-group-arrows');
		if ($arrows.length == 0) {
			$arrows = $this.closest('.js-slider_goods_list2').find('.btn-group-arrows');
		}
		if (slick.slideCount > maxcount) {
			$arrows.show();
		} else {
			$arrows.hide();
		}
	};
	if ($currentSliders.length > 0) {
		$currentSliders.filter(':visible').each(function () {
			toggleSliderArrows($(this).getSlick());
		})
	}
	if ($slickSliderContainers.length > 0) {
		var $slickSliders = $slickSliderContainers.find('.goods_list');
		$slickSliders.slickSetOption('onInit', function () {
			toggleSliderArrows(this)
		});
	}
	var $slidersInDetail = $('.js-slider_goods_list2 .slick-initialized.slick-slider');
	if ($slidersInDetail.length > 0) {
		$slidersInDetail.filter(':visible').each(function () {
			toggleSliderArrows($(this).getSlick(), 6);
		})
	}

	var $priceSlider = $('.js-price-slider');
	slideFunction($priceSlider, 1);

	$document.on('submit', '.add2basket_form', function (e) {
		e.preventDefault();
		var $this = $(this);
		if ($this.hasClass('with_modal')) {
			$this.setAjaxLoading();
			$.when($this.doAjaxAddToCart()).then(function (e) {
				$this.stopAjaxLoading();
				var $modal = $('' + $this.data('target'));
				$modal.find('.product_img').attr('src', $this.find('input[name="product_img"]').val());
				$modal.find('.product_name').text($this.find('input[name="product_name"]').val());
				$modal.find('.product-quant').text($this.find('input[name="' + $this.attr('data-varQ') + '"]').val());
				$modal.modal('show');
			});
		} else {
			$this.doAjaxAddToCart();
		}
	});

	$.fn.updateTableBuyButton = function (e) {
		var $btn_tableBuy = $('#btn_tableBuy'),
			$btn_tableBuySpan = $btn_tableBuy.find('span');
		var $fields = this.find('input.js-touchspin');
		var value = 0;
		$fields.each(function (i, el) {
			if (!el.hasAttribute('disabled')) {
				value += el.value | 0;
			}
		});
		$btn_tableBuySpan.text(value);
		if (value <= 0) {
			$btn_tableBuy.attr('disabled', 'disabled');
		} else {
			$btn_tableBuy.removeAttr('disabled');
		}
		if(e) {
			e.stopPropagation();
		}
	};

	$document.on('click', '.js-change-view button', function () {
		var $this = $(this);
		var $container = $('.js-change-view');
		var listClass = $this.attr('data-class'),
			view_val = listClass.split(' ')[1];
		setCookie('VIEW_MODE', view_val, {path: '/'});
		$container.find('.active').removeClass('active');
		$container.find('[data-class="' + listClass + '"]').addClass('active');
		var $parent = $this.closest('.goods_list_block').find('.goods_list');
		$parent.attr('class', listClass).find('div:not(.bar)').removeAttr('style');

		if (listClass == 'goods_list table_view') {
			$parent.find('input.js-touchspin').val(0);
			$('.sorting_type_wrapper .btn-buy').show();
			$('#catalog_sectionList').updateTableBuyButton();
		} else {
			$parent.find('input.js-touchspin').each(function(){
				var $this = $(this);
				$this.val($this.data('value'));
			});
			$('.sorting_type_wrapper .btn-buy').hide();
		}
		addParameter('view_mode', view_val);
		if (view_val != 'table_view') {
			$('select.offer_select').trigger('change');
		}
	});

	$document.popover({
		selector: '[data-toggle="popover"]',
		container: "body",
		content: function () {
			var $tip = $($(this).attr('data-tip'));
			return $tip.html();
		},
		html: true,
		template: '<div class="popover" role="tooltip"><div class="arrow"></div><div class="popover-content"></div></div>',
		placement: 'bottom',
		trigger: 'focus'
	});

	var $speedSlider = $('.js-speed-slider');
	if ($speedSlider.length) {
		$speedSlider.noUiSlider({
			start: [90],
			//connect: true,
			step: 1,
			range: {
				'min': [0],
				'max': [300]
			},
			format: wNumb({
				decimals: 0
			})
		});
		$speedSlider.Link('lower').to($("#speed"), function (val) {
			var $this = $(this);
			$this.val(val);
			$this.trigger('change');
		});

		$speedSlider.Link('lower').to('-inline-<div class="tooltip"></div>', function (value) {
			// The tooltip HTML is 'this', so additional
			// markup can be inserted here.
			$(this).html('<span class="tooltip_inner">' + value + '</span>');
		});

	}
	var $modalBasket = $('#modal-basket'),
		$modalBaskerContent = $modalBasket.find('.popup_basket');

	var updateModalBasket = function (addData) {
		$modalBaskerContent.setAjaxLoading();
		if (typeof(addData) == 'undefined') {
			addData = [];
		}
		var data = [
			{name: "arParams", value: $modalBasket.data('arparams')},
			{name: "template", value: $modalBasket.data('template').toString()}
		];
		$.merge(data, addData);
		return $.ajax({
			type: "POST",
			url: rz.AJAX_DIR + 'cart.php',
			data: data,
			success: function (html) {
				$modalBaskerContent.html(html);
				$modalBasket.reStyler();
				$modalBasket.reSpin();
				$modalBaskerContent.stopAjaxLoading();
			}
		});
	};

	$(document).on('cartPopup_Refresh', function () {updateModalBasket()});

	$(document).on('click','#cartLine_Refresh', function () {
        $modalBasket.off('shown.bs.modal').on('shown.bs.modal', function () {
            updateModalBasket();
            $miniCart.addClass('active');
        });
	});
	$modalBasket.on('click', '.delete_link', function (e) {
		e.preventDefault();
		var $this = $(this);
		$.when(
			updateModalBasket([
				{name: $modalBasket.data('actionvar'), value: 'delete'},
				{name: 'id', value: $this.data('id')}
			])).done(function () {
				$document.trigger('cartLine_Refresh');
			});
	});
	$modalBasket.on('click', '.put_off_link', function (e) {
		e.preventDefault();
		var $this = $(this);
		$.when(
			updateModalBasket([
				{name: $modalBasket.data('actionvar'), value: 'delay'},
				{name: 'id', value: $this.data('id')}
			])).done(function () {
				$document.trigger('cartLine_Refresh');
			});
	});
	var changeTimer = null;
	$modalBasket.on('change', '.js-touchspin', function (e) {
		e.preventDefault();
		if (changeTimer != null) {
			clearTimeout(changeTimer);
		}
		changeTimer = setTimeout(function () {
			var data = $modalBasket.find('.basket_order_form').serializeArray();
			data.push({name: 'BasketRefresh', value: 'Y'});
			updateModalBasket(data);
			$document.trigger('cartLine_Refresh');
		}, 700);

	});

	$modalBasket.on('hide.bs.modal', function () {
		$miniCart.removeClass('active');
	});

	var $modalCompare = $('#modal-compare');
	if ($modalCompare.length > 0) {
		var $modalCompareContent = $modalCompare.find('.popup_compare'),
			$compareBtn = $('#compare_button');

		var updateModalCompare = function (addData, $addObj) {
			if (typeof(addData) == 'undefined') {
				addData = [];
			}
			var is_add = false,
				no_fly = false;
			if (typeof($addObj) == 'object' && $addObj.length > 0) {
				is_add = true;
				if ($addObj.hasClass('active')) {
					no_fly = true;
				}
			}
			if (!is_add) {
				$modalCompareContent.setAjaxLoading();
			}

			var data = [
				{name: "arParams", value: $modalCompare.data('arparams')},
				{name: "template", value: $modalCompare.data('template').toString()}
			];
			$.merge(data, addData);
			return $.ajax({
				type: "POST",
				url: rz.AJAX_DIR + 'compare_Modal.php',
				data: data,
				success: function (html) {
					if (!is_add) {
						$modalCompareContent.html(html);
						$modalCompareContent.reStyler();
						$modalCompareContent.reSpin();
						$modalCompareContent.stopAjaxLoading();
					} else {
						if (!no_fly) {
							var imgtodrag = $addObj.closest('.goods_item').find("img:visible").eq(0);
							if (imgtodrag.length) {
								var $flyTo = ($compareBtn.is(':visible')) ? $compareBtn : $miniCart;
								var imgclone = imgtodrag.clone()
									.offset({
										top: imgtodrag.offset().top,
										left: imgtodrag.offset().left
									})
									.css({
										'opacity': '0.5',
										'position': 'absolute',
										'height': imgtodrag.height() + 'px',
										'width': imgtodrag.width() + 'px',
										'z-index': '99999'
									})
									.appendTo($('body'))
									.animate({
										'top': $flyTo.offset().top + 10,
										'left': $flyTo.offset().left + 10,
										'width': 75,
										'height': 75
									}, 1000);
								imgclone.animate({
									'width': 0,
									'height': 0
								}, function () {
									$(this).detach()
								});
							}
						}
					}
				}
			});
		};

        $(document).on('click','#compare_button', function () {
            $modalCompare.off('shown.bs.modal').on('shown.bs.modal', function () {
                updateModalCompare();
            });
        });

		$modalCompare.on('click', '.delete_link', function (e) {
			e.preventDefault();
			var $this = $(this);
			var data = [
				{name: 'action', value: 'DELETE_FROM_COMPARE_LIST'},
				{name: 'id', value: $this.data('id')}
			];
			$.when(updateModalCompare(data)).done(function () {
				$document.trigger('compare_Refresh');
			});
		});

		$document.on('click', '.compare_link_item, .compare_link', function (e) {
			e.preventDefault();
			var $this = $(this),
				id = getQueryVariable('id', $this.attr('href'));
			id = id | 0;
			if (id > 0) {
				var data = [
					{name: 'action', value: 'ADD_TO_COMPARE_LIST'},
					{name: 'id', value: id}
				];
				var sId = id.toString();
				if ($this.hasClass('active')) {
					data[0].value = 'DELETE_FROM_COMPARE_LIST';
				}
				$.when(updateModalCompare(data, $this)).done(function () {
					$document.trigger('compare_Refresh');
					if (data[0].value == 'ADD_TO_COMPARE_LIST') {
						$this.addClass('active brand-primary-color');
						if ('COMPARE_LIST' in rz) {
							rz.COMPARE_LIST.push(sId);
						} else {
							rz.COMPARE_LIST = [sId];
						}
					} else {
						$this.removeClass('active brand-primary-color');
						if ('COMPARE_LIST' in rz) {
							rz.COMPARE_LIST.splice(rz.COMPARE_LIST.indexOf(sId), 1);
						}
					}
				});
			}
		});

		var $wishListData = $('#wishListData');
		var $wishListPersonal = $('#wish-list');
		$document.on('click', 'a.favourite_link_item', function (e) {
			e.preventDefault();
			var $this = $(this),
				data = [
					{name: 'ACTION', value: 'ADD'},
					{name: 'ID', value: $this.data('id')},
					{name: 'rz_ajax', value: 'y'},
					{name: 'ajax', value: $wishListData.data('ajax')},
				],
				id = $this.data('id') + '';

			if ($this.hasClass('active') || $this.hasClass('favourite_link_pesonal')) {
				data[0].value = 'DELETE';
			}
			$.ajax({
				type: "POST",
				dataType: 'json',
				url: rz.AJAX_DIR + 'wishList.php',
				data: data,
				success: function (json) {
					if (json.result == 'success') {
						if (!$this.hasClass('active')) {
							$this.addClass('active brand-primary-color');
							if ('WISH_LIST' in rz) {
								rz.WISH_LIST.push(id);
							} else {
								rz.WISH_LIST = [id];
							}
						} else {
							if($wishListPersonal.length == 0) {
								$this.removeClass('active brand-primary-color');
							}
							if ('WISH_LIST' in rz) {
								rz.WISH_LIST.splice(rz.WISH_LIST.indexOf(id), 1);
							}
						}

					} else {
						alert(BX.message(json.msg));
					}
					$document.trigger('wishListData_update');
					if($wishListPersonal.length > 0)
						location.reload();
				}
			})
		});
	}
	$body.updateWishCompare();
	$document.on("change", ".custom_input_file_field", function () {
		var $this = $(this);
		var $target = $($this.data('target'));
		$target.text($this[0].files[0].name);
	});

	$document.on('change', '#modal-settings', function(e){
		var $t = $(e.target),
			name = $t.data('name-func') || $t.attr('name-func'),
			value = $t.val();

		if (value === undefined || name === undefined) return;
		if (value === "false") value = false;
		else if (value === "true") value = true;

		if ($t.is('input:checkbox')) value = $t.prop('checked');
		// ^ using input here because selectors :checkbox or :checked are equal to
		// *:checkbox or *:checked, and are SLOW because of universal *. But with
		// input, we are much more specific.

		// if (settings[name] === value) return;
		// ^ moved such checks to set functions

		// console.log('set', name, 'to', value);

		if (typeof set[name] === 'function') set[name](value);
		//settings.isFrontend && localStorage.setItem(name, value);
		//settings[name] = value;
	});

	$document.on('rated', '.rateit', function () {
		var $this = $(this),
			voteId = $this.data('id') | 0,
			rating = $this.rateit('value') | 0;
		if (voteId > 0 && rating > 0 && !$this.hasClass('disabled')) {
			return $.ajax({
				type: "POST",
				data: [
					{name: 'arParams', value: $this.data('arparams')},
					{name: 'vote_id', value: voteId},
					{name: 'rating', value: rating},
					{name: 'vote', value: 'Y'},
					{name: 'AJAX_CALL', value: 'Y'},
					{name: 'json', value: 'Y'}
				],
				dataType: 'json',
				url: rz.AJAX_DIR + 'rate.php',
				success: function (json) {
					if (!json.success) {
						alert(json.msg);
					} else {
						$this.addClass('disabled')
							.rateit('readonly', true)
							.rateit('value', json.msg);
					}
				}
			});
		}
	});

	$body.on('click', '.mobile-serach-btn', function(){
		if($('.btn-group.btn-group-auth').hasClass('open'))
			if($('form.mobile_search_form input').val())
				$('.mobile_search_form').submit();
	});

	$body.on('change', 'select.offer_select', function () {
		var $this = $(this),
			$priceHtml,
			$priceHtmlOld,
			$available,
			$buyBtn,
            $oneClickBuy,
			$stores;
		if ($this.hasClass('offer_detail_select')) {
			var $parent = $this.closest('.item_manage_block');
			$priceHtml = $parent.find('.item_manage__price');
			$priceHtmlOld = $priceHtml.find('.old_price');
			$priceHtml = $priceHtml.find('.new_price');
			$available = $parent.find('.item_manage__avalible > span');
			$buyBtn = $parent.find('.btn-buy, .do-order.btn');
			$oneClickBuy = $parent.find('.rz_oneclick-buy').find('a');
			$stores = $parent.find('.RzStore__sku')
		} else {
			var $parent = $this.closest('.goods_item__info');
			$priceHtml = $parent.find('.goods_item__price');
			$priceHtmlOld = $priceHtml.find('.old_price');
			$priceHtml = $priceHtml.find('.new_price');
			$buyBtn = $parent.find('.btn-buy, .do-order');
			$oneClickBuy = $parent.find('.rz_oneclick-buy').find('a');
			$stores = $parent.find('.RzStore__sku')
		}
		var curval = $this.val(),
			$option = $this.find('option[value="' + curval + '"]');
		if ($priceHtml.length > 0) {
			if (typeof($option.data('price')) != 'undefined') {
				$priceHtml.html($option.data('price'));
			}
			if (typeof($option.data('priceold')) != 'undefined' && typeof($priceHtmlOld) != 'undefined'
				&& $priceHtmlOld.length > 0
			) {
				$priceHtmlOld.html($option.data('priceold'));
			} else{
				$priceHtmlOld.empty();
			}
		}

		var timer = $option.data('timer'),
		proggres = $option.data('progres'),
			name = $option.attr('data-name');

		if (typeof timer != 'undefined'){
			$parent.find('.countdown').show();
			var $timer = $parent.find('.timer');
			$timer.countdown('destroy');
			var liftoff = new Date(timer);
			$timer.countdown({until: liftoff});
		} else{
			$parent.find('.countdown').hide();
		}

		var $alreadySold = $parent.find('.already-sold__block');
		if(typeof proggres != 'undefined'){
			$alreadySold.show();
			$alreadySold.find('.text').text(proggres+'%');
			$alreadySold.find('.bar').css({'width': proggres+'%'});
		}else{
			$alreadySold.hide();
		}

		var countBlock = $parent.find('.countdown-block');
		if(countBlock.length) {
			if (typeof proggres != 'undefined' || typeof timer != 'undefined') {
				countBlock.show();
			} else {
				countBlock.hide();
			}
		}

		var canBuy = true;
		if (typeof $option.data('canbuy') != 'undefined') {
			canBuy = $option.data('canbuy');
		}
		if ($available && $available.length > 0) {
			if(canBuy == 0) {
				$available.removeClass('avalible').addClass('unavailable');
			} else {
				$available.removeClass('unavailable').addClass('avalible');
			}
		}
		if($buyBtn && $buyBtn.length > 0) {
			if(canBuy == 0) {
				$buyBtn.addClass('disabled').attr('disabled',true);
			} else {
				$buyBtn.removeClass('disabled').removeAttr('disabled');
			}
		}
		if ($stores && $stores.length > 0) {
			$stores.removeClass('active');
			$stores.filter('#RzStore__sku_'+curval).addClass('active');
		}

		if($oneClickBuy && $oneClickBuy.length > 0) {
			if(canBuy == 0) {
				$oneClickBuy.addClass('disabled').attr('disabled',true);
			} else {
				$oneClickBuy.removeClass('disabled').removeAttr('disabled');
			}
			if(typeof  name != 'undefined') {
				$oneClickBuy.attr('data-name', name);
			}
		}

		var params =  $oneClickBuy.attr('data-arparams');
		var id = $this.val();
		var data = [
			{name: 'ID', value: id},
			{name: 'ajax_rz', value: 'Y'},
			{name: 'params', value: params},
		];
		$.ajax({
			type: 'POST',
			dataType: 'json',
			data:data,
			url: rz.AJAX_DIR + 'one_click.php',
			success: function(data){
				if(data.sucess){
					$oneClickBuy.attr('data-arparams',data.params);
				} else {
					console.log(data.msg);
				}
			}
		});



	});
	//+++ experiment
	var $podbor = $('.autoPodborForm');
	if ($podbor.length > 0) {
		var response = null;
		var defaultData = [
			{name: 'callback', value: 'jsonp1417766488792'},
			{name: 'format', value: 'jsonp'},
			{name: 'is_tires_auto', value: '1'}
		];
		var curMark = 0,
			curModel = 0,
			curYear = 0,
			curModif = 0;

		var $autoMark = $podbor.find('.autoMark'),
			$autoModel = $podbor.find('.autoModel'),
			$autoYear = $podbor.find('.autoYear'),
			$autoModif = $podbor.find('.autoModif'),
			$submit = $podbor.find('a.submit');

		var sendRequest = function (data) {
			data = $.merge(data, defaultData);
			return $.ajax({
				crossDomain: true,
				type: 'GET',
				dataType: "jsonp",
				url: 'http://bibinet.ru/service/get_reference/',
				data: data,
				success: function (json) {
					response = json;
				}
			})
		};
		var fillValues = function ($elem, data) {
			$elem.empty();
			$elem.append($('<option/>').text(' -- '));
			$.when(sendRequest(data)).done(function () {
					$.each(response, function (ind, el) {
						$elem.append(
							$('<option></option>').attr('value', el[0]).text(el[1])
						);
					});
				}
			);
		};

		var getMark = function () {
			var data = [
				{name: 'variants', value: 'mark'}
			];
			fillValues($autoMark, data);
		};

		getMark();

		$autoMark.on('change', function () {
			var data = [
				{name: 'variants', value: 'model'},
				{name: 'mark', value: this.value}
			];
			curMark = this.value;
			fillValues($autoModel, data);
		});

		$autoModel.on('change', function () {
			var data = [
				{name: 'variants', value: 'year'},
				{name: 'mark', value: curMark},
				{name: 'model', value: this.value}
			];
			curModel = this.value;
			fillValues($autoYear, data);
		});
		$autoYear.on('change', function () {
			var data = [
				{name: 'variants', value: 'modif'},
				{name: 'mark', value: curMark},
				{name: 'model', value: curModel},
				{name: 'year', value: this.value}
			];
			curYear = this.value;
			fillValues($autoModif, data);
		});
		$autoModif.on('change', function () {
			curModif = this.value;
			$submit.removeAttr('disabled');
		});
		$submit.on('click', function () {
			var data = [
				{name: 'mark', value: curMark},
				{name: 'model', value: curModel},
				{name: 'year', value: curYear},
				{name: 'modif', value: curModif}
			];
			data = $.merge(data, defaultData);
			$.ajax({
				crossDomain: true,
				type: 'GET',
				dataType: "jsonp",
				url: 'http://bibinet.ru/service/search/tires_auto',
				data: data,
				success: function (json) {
					console.log(json);
				}
			})
		});
	}

	var getFontSizeMainSlider = function($container){
		var heightCont = $container.height(), delHeight = 0;
		if (heightCont > heightSlider && !isMobile){
            delHeight = heightSlider / heightCont;
            fontMainSlider = parseInt(fontMainSlider * delHeight);
		}else if (heightCont > mobileHeightSlider && isMobile){
            delHeight = mobileHeightSlider / heightCont;
            fontMainSlider = parseInt(fontMainSlider * delHeight);
		}
	};

	var setFontMainSlider = function($container){
		$container.find('.shop_name').css('font-size',fontMainSlider);
	};

	var processMainSlider = function(){
       var $container = $('.banners_rotator_block .banners_rotator__inner');
        getFontSizeMainSlider($container);
        setFontMainSlider($container);
        if($container.height() > heightSlider && !isMobile){
            processMainSlider();
		} else if($container.height() > mobileHeightSlider && isMobile){
            processMainSlider();
		}
	};

	if (typeof window.frameCacheVars !== "undefined") {
		BX.addCustomEvent("onFrameDataReceived", function (json) {
			$('select.offer_select').trigger('change');
			$('#modal-settings').reStyler();
            initValid(document,$);
            processMainSlider();
		});
	} else {
		jQuery(document).ready(function () {
			$('select.offer_select').trigger('change');
			$('#modal-settings').reStyler();
            initValid(document,$);
            processMainSlider();
		});
	}
});
