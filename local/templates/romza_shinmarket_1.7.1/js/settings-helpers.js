// these are GLOBALS FOR PURPOSE!
// this is setters for all settings where setter is needed.
$(function($){
set = {};

set['colorScheme'] = function(value) {
	// change colorScheme in "js/theme-switch.js"
};
set['filterPosition'] = function(value) {
	var $elem = $('[data-filter]');

	if ( $elem.length > 0 && $elem.find('.catalog_filter').length) {
		if (isDocumentReady) $elem.attr('data-filter', value);
		else $(document).one('ready', function() {
			$elem.attr('data-filter', value);
		});
		$('.page_aside__filter .jq-selectbox-multy__dropdown').jScrollPane();

		rmz.updateMenu();
	}
};
set['mainmenuType'] = function(value) {
	var $elem = $('[data-menu-type]');

	$elem.attr('data-menu-type', value);
	rmz.updateMenu();
};
set['swith-theme'] = function(value) {
	var $themeLink = $('#theme-css-link'),
		 $logo = $('#current-colot-theme img'),
		 $elem = $('[data-menu-type]');


	var newTheme = value;
	var olTheme = $themeLink.attr('data-theme');
	var link = $themeLink.attr('href').replace(olTheme +'.min.css', newTheme + '.min.css');
	if($logo.length > 0){
		var linkForLogo = $logo.attr('src').replace(/(img\/)(.*jpg)/, 'img/' + newTheme + '.jpg');
		$logo.attr('src', linkForLogo);
	}
	$themeLink.attr('href', link);
	$themeLink.attr('data-theme', newTheme);
};
set['subCategories'] = function(value) {
	var $elem = $('[data-sub-view]');
	$elem.attr('data-sub-view', value);
	}
});