<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Корзина");
?><?$APPLICATION->IncludeComponent(
	"bitrix:breadcrumb",
	"default",
Array()
);?>
<h2><?$APPLICATION->ShowTitle()?></h2>
<? if (Cmodule::IncludeModule('sale')): ?>
<?$APPLICATION->IncludeComponent(
	"bitrix:sale.basket.basket", 
	"basket_new", 
	array(
		"ACTION_VARIABLE" => "action",
		"ADDITIONAL_PICT_PROP_13" => "-",
		"ADDITIONAL_PICT_PROP_14" => "-",
		"ADDITIONAL_PICT_PROP_2" => "-",
		"ADDITIONAL_PICT_PROP_23" => "-",
		"ADDITIONAL_PICT_PROP_3" => "-",
		"AUTO_CALCULATION" => "Y",
		"BASKET_IMAGES_SCALING" => "adaptive",
		"COLUMNS_LIST" => array(
			0 => "NAME",
			1 => "DISCOUNT",
			2 => "PROPS",
			3 => "DELETE",
			4 => "DELAY",
			5 => "PRICE",
			6 => "QUANTITY",
			7 => "SUM",
		),
		"COLUMNS_LIST_EXT" => array(
			0 => "PREVIEW_PICTURE",
			1 => "DISCOUNT",
			2 => "DELETE",
			3 => "DELAY",
			4 => "SUM",
			5 => "PROPERTY_CML2_ARTICLE",
		),
		"COMPATIBLE_MODE" => "Y",
		"CORRECT_RATIO" => "Y",
		"COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",
		"GIFTS_BLOCK_TITLE" => "Выберите один из подарков",
		"GIFTS_CONVERT_CURRENCY" => "N",
		"GIFTS_HIDE_BLOCK_TITLE" => "N",
		"GIFTS_HIDE_NOT_AVAILABLE" => "N",
		"GIFTS_MESS_BTN_BUY" => "Выбрать",
		"GIFTS_MESS_BTN_DETAIL" => "Подробнее",
		"GIFTS_PAGE_ELEMENT_COUNT" => "4",
		"GIFTS_PLACE" => "BOTTOM",
		"GIFTS_PRODUCT_PROPS_VARIABLE" => "prop",
		"GIFTS_PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"GIFTS_SHOW_DISCOUNT_PERCENT" => "Y",
		"GIFTS_SHOW_IMAGE" => "Y",
		"GIFTS_SHOW_NAME" => "Y",
		"GIFTS_SHOW_OLD_PRICE" => "N",
		"GIFTS_TEXT_LABEL_GIFT" => "Подарок",
		"HIDE_COUPON" => "Y",
		"OFFERS_PROPS" => array(
			0 => "COLOR_REF",
			1 => "SIZES_SHOES",
			2 => "SIZES_CLOTHES",
		),
		"ONECLICK_ALLOW_AUTO_REGISTER" => "Y",
		"ONECLICK_AS_EMAIL" => "EMAIL",
		"ONECLICK_AS_NAME" => "FIO",
		"ONECLICK_DELIVERY_ID" => "0",
		"ONECLICK_FIELD_PLACEHOLDER" => "Y",
		"ONECLICK_FIELD_QUANTITY" => "Y",
		"ONECLICK_MESSAGE_OK" => "Ваш заказ принят, его номер - <b>#ID#</b>. Менеджер свяжется с вами в ближайшее время.<br> Спасибо что выбрали нас!",
		"ONECLICK_PAY_SYSTEM_ID" => "0",
		"ONECLICK_PERSON_TYPE_ID" => "1",
		"ONECLICK_REQ_FIELDS" => array(
			0 => "FIO",
			1 => "EMAIL",
		),
		"ONECLICK_SEND_REGISTER_EMAIL" => "Y",
		"ONECLICK_SHOW_FIELDS" => array(
			0 => "FIO",
			1 => "EMAIL",
		),
		"ONECLICK_USER_REGISTER_EVENT_NAME" => "USER_INFO",
		"ONECLICK_USE_CAPTCHA" => "N",
		"PATH_TO_ORDER" => "/personal/order/make/",
		"PRICE_VAT_SHOW_VALUE" => "N",
		"QUANTITY_FLOAT" => "N",
		"RESIZER_PRODUCT" => "10",
		"SET_TITLE" => "Y",
		"USE_GIFTS" => "Y",
		"USE_PREPAYMENT" => "N",
		"COMPONENT_TEMPLATE" => "basket_new"
	),
	false
);?>
<? elseif (Cmodule::IncludeModule('yenisite.market')): ?>
	<?$APPLICATION->IncludeComponent(
	"yenisite:catalog.basket",
	".default",
	Array(
		"ADMIN_MAIL" => "admin@email.ru",
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"EVENT" => "SALE_ORDER",
		"EVENT_ADMIN" => "SALE_ORDER_ADMIN",
		"INCLUDE_JQUERY" => "Y",
		"PROPERTY_CODE" => array(0=>"FIO",1=>"EMAIL",2=>"PHONE",3=>"ABOUT",4=>"DELIVERY_E",5=>"PAYMENT_E",),
		"RESIZER_PRODUCT" => "10",
		"THANK_URL" => "//personal/cart/thank_you.php",
		"YENISITE_BS_FLY" => ""
	)
);?>
<? endif ?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>