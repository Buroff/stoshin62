<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Заказы");
?>
<? $APPLICATION->IncludeComponent('romza:dummy','nocomposite')?>
<? $APPLICATION->IncludeComponent(
	"bitrix:breadcrumb",
	"default",
	Array(),
	false
); ?>
	<h2><? $APPLICATION->ShowTitle() ?></h2>
	<div class="profile_wrapper">
		<? $APPLICATION->IncludeComponent("bitrix:menu", "personal", Array(
			"ROOT_MENU_TYPE" => "personal",
			"MAX_LEVEL" => "1",
			"CHILD_MENU_TYPE" => "personal",
			"USE_EXT" => "Y",
			"DELAY" => "N",
			"ALLOW_MULTI_SELECT" => "N",
			"MENU_CACHE_TYPE" => "N",
			"MENU_CACHE_TIME" => "3600",
			"MENU_CACHE_USE_GROUPS" => "Y",
			"MENU_CACHE_GET_VARS" => "",
		),
			false
		); ?>
		<div class="profile_content">
			<? global $USER;
			if ($USER->IsAuthorized()):?>
				<? if (Cmodule::IncludeModule('sale')): ?>
				<? 
				$APPLICATION->IncludeComponent("bitrix:sale.personal.order", "romza_sale_personal_order", Array(
					"SEF_MODE" => "Y",
					"SEF_FOLDER" => "/personal/order/",
					"ORDERS_PER_PAGE" => "10",
					"PATH_TO_PAYMENT" => "/personal/order/payment/",
					"PATH_TO_BASKET" => "/personal/cart/",
					"SET_TITLE" => "Y",
					"SAVE_IN_SESSION" => "N",
					"NAV_TEMPLATE" => "arrows",
					"SEF_URL_TEMPLATES" => array(
						"list" => "index.php",
						"detail" => "detail/#ID#/",
						"cancel" => "cancel/#ID#/",
					),
					"SHOW_ACCOUNT_NUMBER" => "Y"
				),
					false
				); 

				?>
				<? elseif (Cmodule::IncludeModule('yenisite.market')): ?>
					<?
					$_REQUEST['ID'] = intval($_REQUEST['ID']);
					if ($_REQUEST['ID'] > 0):
						?>
						<? 
						$APPLICATION->IncludeComponent(
						"bitrix:news.detail",
						"order",
						array(
							"IBLOCK_TYPE" => "yenisite_market",
							"IBLOCK_ID" => "#MARKET_IBLOCK_ID#",
							"ELEMENT_ID" => $_REQUEST["ID"],
							"ELEMENT_CODE" => "",
							"CHECK_DATES" => "Y",
							"FIELD_CODE" => array(
								0 => "CREATED_BY",
								1 => "DATE_CREATE",
							),
							"PROPERTY_CODE" => array(
								2 => "FIO",
								4 => "PHONE",
								3 => "EMAIL",
								5 => "ABOUT",
								7 => "PAYMENT_E",
								8 => "DELIVERY_E",
							),
							"IBLOCK_URL" => "/personal/order/",
							"AJAX_MODE" => "N",
							"AJAX_OPTION_JUMP" => "N",
							"AJAX_OPTION_STYLE" => "Y",
							"AJAX_OPTION_HISTORY" => "N",
							"CACHE_TYPE" => "N",
							"CACHE_TIME" => "36000000",
							"CACHE_GROUPS" => "Y",
							"SET_TITLE" => "N",
							"SET_BROWSER_TITLE" => "Y",
							"BROWSER_TITLE" => "-",
							"SET_META_KEYWORDS" => "Y",
							"META_KEYWORDS" => "-",
							"SET_META_DESCRIPTION" => "Y",
							"META_DESCRIPTION" => "-",
							"SET_STATUS_404" => "N",
							"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
							"ADD_SECTIONS_CHAIN" => "Y",
							"ADD_ELEMENT_CHAIN" => "N",
							"ACTIVE_DATE_FORMAT" => "d.m.Y",
							"USE_PERMISSIONS" => "N",
							"PAGER_TEMPLATE" => ".default",
							"DISPLAY_TOP_PAGER" => "N",
							"DISPLAY_BOTTOM_PAGER" => "Y",
							"PAGER_TITLE" => "Страница",
							"PAGER_SHOW_ALL" => "Y",
							"DISPLAY_DATE" => "Y",
							"DISPLAY_NAME" => "Y",
							"DISPLAY_PICTURE" => "Y",
							"DISPLAY_PREVIEW_TEXT" => "Y",
							"USE_SHARE" => "N",
							"AJAX_OPTION_ADDITIONAL" => "",
							"RESIZER_BASKET_PHOTO" => "#BIG_BASKET_ICON_RESIZER_SET#"
						),
						false
					);

					?>
					<? else: ?>
						<?
						global $USER;
						if ($USER->GetID()):
							global $arrFilter;
							$arrFilter = array();
							$arrFilter['CREATED_BY'] = $USER->GetID();
							?>
							<? 
							$APPLICATION->IncludeComponent("bitrix:news.list", "orders", Array(
							"IBLOCK_TYPE" => "yenisite_market",    // Тип информационного блока (используется только для проверки)
							"IBLOCK_ID" => "#MARKET_IBLOCK_ID#",    // Код информационного блока
							"NEWS_COUNT" => "100",    // Количество новостей на странице
							"SORT_BY1" => "ACTIVE_FROM",    // Поле для первой сортировки новостей
							"SORT_ORDER1" => "DESC",    // Направление для первой сортировки новостей
							"SORT_BY2" => "SORT",    // Поле для второй сортировки новостей
							"SORT_ORDER2" => "ASC",    // Направление для второй сортировки новостей
							"FILTER_NAME" => "arrFilter",    // Фильтр
							"FIELD_CODE" => array(    // Поля
								0 => "ID",
								1 => "DATE_CREATE",
								2 => "",
							),
							"PROPERTY_CODE" => array(    // Свойства
								0 => "ITEMS",
								1 => "STATUS",
								2 => "AMOUNT",
								3 => "SITE_ID",
								4 => "PAYMENT_E",
								5 => "DELIVERY_E",
							),
							"CHECK_DATES" => "Y",
							"DETAIL_URL" => "?ID=#ID#",
							"AJAX_MODE" => "N",
							"AJAX_OPTION_JUMP" => "N",
							"AJAX_OPTION_STYLE" => "Y",
							"AJAX_OPTION_HISTORY" => "N",
							"CACHE_TYPE" => "A",
							"CACHE_TIME" => "36000000",
							"CACHE_FILTER" => "N",
							"CACHE_GROUPS" => "Y",
							"PREVIEW_TRUNCATE_LEN" => "",
							"ACTIVE_DATE_FORMAT" => "d.m.Y",
							"SET_TITLE" => "N",
							"SET_STATUS_404" => "N",
							"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
							"ADD_SECTIONS_CHAIN" => "Y",
							"HIDE_LINK_WHEN_NO_DETAIL" => "Y",
							"PARENT_SECTION" => "",
							"PARENT_SECTION_CODE" => "",
							"INCLUDE_SUBSECTIONS" => "Y",
							"DISPLAY_TOP_PAGER" => "N",
							"DISPLAY_BOTTOM_PAGER" => "Y",
							"PAGER_TITLE" => "",
							"PAGER_SHOW_ALWAYS" => "N",
							"PAGER_TEMPLATE" => "",
							"PAGER_DESC_NUMBERING" => "N",
							"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
							"PAGER_SHOW_ALL" => "N",
							"DISPLAY_DATE" => "Y",
							"DISPLAY_NAME" => "Y",
							"DISPLAY_PICTURE" => "Y",
							"DISPLAY_PREVIEW_TEXT" => "Y",
							"AJAX_OPTION_ADDITIONAL" => "",
						),
							false,
							array("HIDE_ICONS" => "Y")
						);

						?>
						<? endif ?>
					<? endif ?>
				<? endif ?>
			<? else: ?>
				<? $APPLICATION->AuthForm('', false, false, 'N', false) ?>
			<? endif ?>
		</div>
	</div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>