<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("оформление заказа");
?><?$APPLICATION->IncludeComponent(
	"bitrix:breadcrumb",
	"default",
Array()
);?>
<h2><?$APPLICATION->ShowTitle()?></h2>
<?
/*$APPLICATION->IncludeComponent("bitrix:sale.order.ajax", "titan_new", Array(
	"PAY_FROM_ACCOUNT" => "Y",	// Разрешить оплату с внутреннего счета
		"ONLY_FULL_PAY_FROM_ACCOUNT" => "N",	// Разрешить оплату с внутреннего счета только в полном объеме
		"COUNT_DELIVERY_TAX" => "N",
		"ALLOW_AUTO_REGISTER" => "Y",	// Оформлять заказ с автоматической регистрацией пользователя
		"SEND_NEW_USER_NOTIFY" => "Y",	// Отправлять пользователю письмо, что он зарегистрирован на сайте
		"DELIVERY_NO_AJAX" => "N",	// Когда рассчитывать доставки с внешними системами расчета
		"DELIVERY_NO_SESSION" => "N",	// Проверять сессию при оформлении заказа
		"TEMPLATE_LOCATION" => "popup",	// Визуальный вид контрола выбора местоположений
		"DELIVERY_TO_PAYSYSTEM" => "d2p",	// Последовательность оформления
		"USE_PREPAYMENT" => "N",	// Использовать предавторизацию для оформления заказа (PayPal Express Checkout)
		"PROP_1" => "",
		"PROP_2" => "",
		"ALLOW_NEW_PROFILE" => "Y",	// Разрешить множество профилей покупателей
		"SHOW_PAYMENT_SERVICES_NAMES" => "Y",
		"SHOW_STORES_IMAGES" => "N",	// Показывать изображения складов в окне выбора пункта выдачи
		"PATH_TO_BASKET" => "/personal/cart/",	// Путь к странице корзины
		"PATH_TO_PERSONAL" => "/personal/order/",	// Путь к странице персонального раздела
		"PATH_TO_PAYMENT" => "/personal/order/payment/",	// Страница подключения платежной системы
		"PATH_TO_AUTH" => "/auth/",	// Путь к странице авторизации
		"SET_TITLE" => "Y",	// Устанавливать заголовок страницы
		"PRODUCT_COLUMNS" => "",
		"DISABLE_BASKET_REDIRECT" => "N",	// Оставаться на странице оформления заказа, если список товаров пуст
		"RESIZER_PRODUCT" => "10"
	),
	false
);*/

?>
<?
/*$APPLICATION->IncludeComponent(
	"bitrix:sale.order.ajax", 
	"titan_new_4", 
	array(
		"ACTION_VARIABLE" => "soa-action",
		"ADDITIONAL_PICT_PROP_13" => "-",
		"ADDITIONAL_PICT_PROP_14" => "-",
		"ADDITIONAL_PICT_PROP_2" => "-",
		"ADDITIONAL_PICT_PROP_3" => "-",
		"ALLOW_APPEND_ORDER" => "Y",
		"ALLOW_AUTO_REGISTER" => "Y",
		"ALLOW_NEW_PROFILE" => "N",
		"ALLOW_USER_PROFILES" => "N",
		"BASKET_IMAGES_SCALING" => "adaptive",
		"BASKET_POSITION" => "after",
		"COMPATIBLE_MODE" => "Y",
		"COUNT_DELIVERY_TAX" => "N",
		"DELIVERIES_PER_PAGE" => "9",
		"DELIVERY_FADE_EXTRA_SERVICES" => "N",
		"DELIVERY_NO_AJAX" => "N",
		"DELIVERY_NO_SESSION" => "N",
		"DELIVERY_TO_PAYSYSTEM" => "p2d",
		"DISABLE_BASKET_REDIRECT" => "N",
		"ONLY_FULL_PAY_FROM_ACCOUNT" => "N",
		"PATH_TO_AUTH" => "/auth/",
		"PATH_TO_BASKET" => "/personal/cart/",
		"PATH_TO_PAYMENT" => "/personal/order/payment/",
		"PATH_TO_PERSONAL" => "/personal/order/",
		"PAY_FROM_ACCOUNT" => "Y",
		"PAY_SYSTEMS_PER_PAGE" => "9",
		"PICKUPS_PER_PAGE" => "0",
		"PICKUP_MAP_TYPE" => "yandex",
		"PRODUCT_COLUMNS" => "",
		"PRODUCT_COLUMNS_HIDDEN" => array(
		),
		"PRODUCT_COLUMNS_VISIBLE" => array(
		),
		"PROPS_FADE_LIST_1" => array(
			0 => "20",
		),
		"PROPS_FADE_LIST_2" => array(
		),
		"PROP_1" => "",
		"PROP_2" => "",
		"RESIZER_PRODUCT" => "10",
		"SEND_NEW_USER_NOTIFY" => "Y",
		"SERVICES_IMAGES_SCALING" => "adaptive",
		"SET_TITLE" => "Y",
		"SHOW_BASKET_HEADERS" => "N",
		"SHOW_COUPONS_BASKET" => "N",
		"SHOW_COUPONS_DELIVERY" => "N",
		"SHOW_COUPONS_PAY_SYSTEM" => "N",
		"SHOW_DELIVERY_INFO_NAME" => "Y",
		"SHOW_DELIVERY_LIST_NAMES" => "Y",
		"SHOW_DELIVERY_PARENT_NAMES" => "Y",
		"SHOW_MAP_IN_PROPS" => "N",
		"SHOW_NEAREST_PICKUP" => "N",
		"SHOW_NOT_CALCULATED_DELIVERIES" => "L",
		"SHOW_ORDER_BUTTON" => "final_step",
		"SHOW_PAYMENT_SERVICES_NAMES" => "Y",
		"SHOW_PAY_SYSTEM_INFO_NAME" => "Y",
		"SHOW_PAY_SYSTEM_LIST_NAMES" => "Y",
		"SHOW_PICKUP_MAP" => "N",
		"SHOW_STORES_IMAGES" => "N",
		"SHOW_TOTAL_ORDER_BUTTON" => "N",
		"SHOW_VAT_PRICE" => "Y",
		"SKIP_USELESS_BLOCK" => "Y",
		"SPOT_LOCATION_BY_GEOIP" => "Y",
		"TEMPLATE_LOCATION" => "popup",
		"TEMPLATE_THEME" => "site",
		"USER_CONSENT" => "N",
		"USER_CONSENT_ID" => "0",
		"USER_CONSENT_IS_CHECKED" => "Y",
		"USER_CONSENT_IS_LOADED" => "N",
		"USE_CUSTOM_ADDITIONAL_MESSAGES" => "N",
		"USE_CUSTOM_ERROR_MESSAGES" => "N",
		"USE_CUSTOM_MAIN_MESSAGES" => "N",
		"USE_ENHANCED_ECOMMERCE" => "N",
		"USE_PRELOAD" => "Y",
		"USE_PREPAYMENT" => "N",
		"USE_YM_GOALS" => "N",
		"COMPONENT_TEMPLATE" => "titan_new_4",
		"SHOW_MAP_FOR_DELIVERIES" => array(
			0 => "3",
		)
	),
	false
);*/

?>
<?
/*		$APPLICATION->IncludeComponent(
	"bitrix:sale.order.ajax", 
	"titan_new_3", 
	array(
		"PAY_FROM_ACCOUNT" => "Y",
		"ONLY_FULL_PAY_FROM_ACCOUNT" => "N",
		"COUNT_DELIVERY_TAX" => "N",
		"ALLOW_AUTO_REGISTER" => "Y",
		"SEND_NEW_USER_NOTIFY" => "Y",
		"DELIVERY_NO_AJAX" => "N",
		"DELIVERY_NO_SESSION" => "N",
		"TEMPLATE_LOCATION" => "popup",
		"DELIVERY_TO_PAYSYSTEM" => "d2p",
		"USE_PREPAYMENT" => "N",
		"PROP_1" => "",
		"PROP_2" => "",
		"ALLOW_NEW_PROFILE" => "Y",
		"SHOW_PAYMENT_SERVICES_NAMES" => "Y",
		"SHOW_STORES_IMAGES" => "N",
		"PATH_TO_BASKET" => "/personal/cart/",
		"PATH_TO_PERSONAL" => "/personal/order/",
		"PATH_TO_PAYMENT" => "/personal/order/payment/",
		"PATH_TO_AUTH" => "/auth/",
		"SET_TITLE" => "Y",
		"PRODUCT_COLUMNS" => "",
		"DISABLE_BASKET_REDIRECT" => "N",
		"RESIZER_PRODUCT" => "10",
		"COMPONENT_TEMPLATE" => "titan_new_3",
		"ALLOW_APPEND_ORDER" => "Y",
		"SHOW_NOT_CALCULATED_DELIVERIES" => "L",
		"SPOT_LOCATION_BY_GEOIP" => "Y",
		"SHOW_VAT_PRICE" => "Y",
		"COMPATIBLE_MODE" => "Y",
		"USE_PRELOAD" => "Y",
		"ALLOW_USER_PROFILES" => "N",
		"TEMPLATE_THEME" => "site",
		"SHOW_ORDER_BUTTON" => "final_step",
		"SHOW_TOTAL_ORDER_BUTTON" => "N",
		"SHOW_PAY_SYSTEM_LIST_NAMES" => "Y",
		"SHOW_PAY_SYSTEM_INFO_NAME" => "Y",
		"SHOW_DELIVERY_LIST_NAMES" => "Y",
		"SHOW_DELIVERY_INFO_NAME" => "Y",
		"SHOW_DELIVERY_PARENT_NAMES" => "Y",
		"SKIP_USELESS_BLOCK" => "Y",
		"BASKET_POSITION" => "after",
		"SHOW_BASKET_HEADERS" => "N",
		"DELIVERY_FADE_EXTRA_SERVICES" => "N",
		"SHOW_COUPONS_BASKET" => "N",
		"SHOW_COUPONS_DELIVERY" => "N",
		"SHOW_COUPONS_PAY_SYSTEM" => "N",
		"SHOW_NEAREST_PICKUP" => "N",
		"DELIVERIES_PER_PAGE" => "9",
		"PAY_SYSTEMS_PER_PAGE" => "9",
		"PICKUPS_PER_PAGE" => "5",
		"SHOW_PICKUP_MAP" => "Y",
		"SHOW_MAP_IN_PROPS" => "N",
		"PICKUP_MAP_TYPE" => "yandex",
		"PROPS_FADE_LIST_1" => array(
		),
		"PROPS_FADE_LIST_2" => array(
		),
		"USER_CONSENT" => "N",
		"USER_CONSENT_ID" => "0",
		"USER_CONSENT_IS_CHECKED" => "Y",
		"USER_CONSENT_IS_LOADED" => "N",
		"ACTION_VARIABLE" => "soa-action",
		"PRODUCT_COLUMNS_VISIBLE" => array(
		),
		"ADDITIONAL_PICT_PROP_2" => "-",
		"ADDITIONAL_PICT_PROP_3" => "-",
		"ADDITIONAL_PICT_PROP_13" => "-",
		"ADDITIONAL_PICT_PROP_14" => "-",
		"BASKET_IMAGES_SCALING" => "adaptive",
		"SERVICES_IMAGES_SCALING" => "adaptive",
		"PRODUCT_COLUMNS_HIDDEN" => array(
		),
		"USE_YM_GOALS" => "N",
		"USE_ENHANCED_ECOMMERCE" => "N",
		"USE_CUSTOM_MAIN_MESSAGES" => "N",
		"USE_CUSTOM_ADDITIONAL_MESSAGES" => "N",
		"USE_CUSTOM_ERROR_MESSAGES" => "N"
	),
	false
);*/


		?>

<?
global $USER;
if ($USER->IsAuthorized()){ 
	$template = 'auth';
	if($USER->IsAdmin()){
		// $template = '.default';
		$template = 'auth';
	}
}else{
	$template = 'not-auth';
}

// echo $template;
// echo '<br/>';
// echo $APPLICATION->GetCurDir();

$APPLICATION->IncludeComponent(
			"bitrix:sale.order.ajax", 
		    $template, 
			array(
				"PAY_FROM_ACCOUNT" => "Y",
				"ONLY_FULL_PAY_FROM_ACCOUNT" => "N",
				"COUNT_DELIVERY_TAX" => "N",
				"ALLOW_AUTO_REGISTER" => "Y",
				"SEND_NEW_USER_NOTIFY" => "Y",
				"DELIVERY_NO_AJAX" => "N",
				"DELIVERY_NO_SESSION" => "N",
				"TEMPLATE_LOCATION" => "popup",
				"DELIVERY_TO_PAYSYSTEM" => "d2p",
				"USE_PREPAYMENT" => "N",
				"PROP_1" => "",
				"PROP_2" => "",
				"ALLOW_NEW_PROFILE" => "Y",
				"SHOW_PAYMENT_SERVICES_NAMES" => "Y",
				"SHOW_STORES_IMAGES" => "N",
				"PATH_TO_BASKET" => "/personal/cart/",
				"PATH_TO_PERSONAL" => "/personal/order/",
				"PATH_TO_PAYMENT" => "/personal/order/payment/",
				"PATH_TO_AUTH" => "/auth/",
				"SET_TITLE" => "Y",
				"PRODUCT_COLUMNS" => "",
				"DISABLE_BASKET_REDIRECT" => "N",
				"RESIZER_PRODUCT" => "10",
				"COMPONENT_TEMPLATE" => "titan_new_5",
				"ALLOW_APPEND_ORDER" => "Y",
				"SHOW_NOT_CALCULATED_DELIVERIES" => "L",
				"SPOT_LOCATION_BY_GEOIP" => "Y",
				"SHOW_VAT_PRICE" => "Y",
				"COMPATIBLE_MODE" => "Y",
				"USE_PRELOAD" => "Y",
				"ALLOW_USER_PROFILES" => "N",
				"TEMPLATE_THEME" => "site",
				"SHOW_ORDER_BUTTON" => "always",
				"SHOW_TOTAL_ORDER_BUTTON" => "N",
				"SHOW_PAY_SYSTEM_LIST_NAMES" => "Y",
				"SHOW_PAY_SYSTEM_INFO_NAME" => "Y",
				"SHOW_DELIVERY_LIST_NAMES" => "Y",
				"SHOW_DELIVERY_INFO_NAME" => "Y",
				"SHOW_DELIVERY_PARENT_NAMES" => "Y",
				"SKIP_USELESS_BLOCK" => "Y",
				"BASKET_POSITION" => "after",
				"SHOW_BASKET_HEADERS" => "N",
				"DELIVERY_FADE_EXTRA_SERVICES" => "N",
				"SHOW_COUPONS_BASKET" => "N",
				"SHOW_COUPONS_DELIVERY" => "N",
				"SHOW_COUPONS_PAY_SYSTEM" => "N",
				"SHOW_NEAREST_PICKUP" => "N",
				"DELIVERIES_PER_PAGE" => "9",
				"PAY_SYSTEMS_PER_PAGE" => "9",
				"PICKUPS_PER_PAGE" => "5",
				"SHOW_PICKUP_MAP" => "Y",
				"SHOW_MAP_IN_PROPS" => "N",
				"PICKUP_MAP_TYPE" => "yandex",
				"PROPS_FADE_LIST_1" => array(
					0 => "20",
				),
				"PROPS_FADE_LIST_2" => array(
				),
				"USER_CONSENT" => "N",
				"USER_CONSENT_ID" => "0",
				"USER_CONSENT_IS_CHECKED" => "Y",
				"USER_CONSENT_IS_LOADED" => "N",
				"ACTION_VARIABLE" => "soa-action",
				"PRODUCT_COLUMNS_VISIBLE" => array(
				),
				"ADDITIONAL_PICT_PROP_2" => "-",
				"ADDITIONAL_PICT_PROP_3" => "-",
				"ADDITIONAL_PICT_PROP_13" => "-",
				"ADDITIONAL_PICT_PROP_14" => "-",
				"BASKET_IMAGES_SCALING" => "adaptive",
				"SERVICES_IMAGES_SCALING" => "adaptive",
				"PRODUCT_COLUMNS_HIDDEN" => array(
				),
				"USE_YM_GOALS" => "N",
				"USE_ENHANCED_ECOMMERCE" => "N",
				"USE_CUSTOM_MAIN_MESSAGES" => "N",
				"USE_CUSTOM_ADDITIONAL_MESSAGES" => "N",
				"USE_CUSTOM_ERROR_MESSAGES" => "N"
			),
			false
		);



		?>

		<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>