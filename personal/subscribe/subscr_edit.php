<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("FORCE_SHOW_BREADCRUMBS", "Y");
$APPLICATION->SetPageProperty("FORCE_SHOW_HEADING", "Y");
$APPLICATION->SetTitle("");
?>
<? if (CModule::IncludeModule('subscribe')): ?>
<?$APPLICATION->IncludeComponent(
	"bitrix:subscribe.edit",
	"",
	Array(
		"SHOW_HIDDEN" => "N",
		"ALLOW_ANONYMOUS" => "Y",
		"SHOW_AUTH_LINKS" => "Y",
		"CACHE_TIME" => "36000000",
		"SET_TITLE" => "Y"
	)
);?>
<? endif ?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>