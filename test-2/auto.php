<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("test-2");
?>
<?

$APPLICATION->IncludeComponent(
    "romza:dummy", 
    "visual_horizontal_car", 
    array(
        "F1_IBLOCK_TYPE" => "final",
        "F1_IBLOCK_ID" => "23",
        "F1_IBLOCK_SECTION_ID" => "52",
        "F1_FILTER_NAME" => "arrFilter",
        "F1_PRICE_CODE" => array(
            0 => "BASE",
        ),
        "F1_TITLE" => " Подбор шин",
        "F1_ICON_CLASS" => "icon_tyrebold",
        "F2_IBLOCK_TYPE" => "final",
        "F2_IBLOCK_ID" => "23",
        "F2_IBLOCK_SECTION_ID" => "49",
        "F2_FILTER_NAME" => "arrFilter",
        "F2_PRICE_CODE" => array(
            0 => "BASE",
        ),
        "F2_TITLE" => "Подбор дисков",
        "F2_ICON_CLASS" => "icon_disk2",
        "CALC_IBLOCK_TYPE" => "final",
        "CALC_IBLOCK_ID" => "23",
        "CALC_SECTION_TYRES_ID" => "52",
        "CALC_SECTION_DISKS_ID" => "49",
        "CALC_PROP_WIDTH" => "124",
        "CALC_PROP_HEIGHT" => "126",
        "CALC_PROP_DIAMTER" => "120",
        "CALC_FILTER_NAME" => "arrFilter",
        "F1_ADDITIONAL_URL" => "/stat/kak_pravilno_vibrat_shiny.php",
        "F1_ADDITIONAL_URL_TEXT" => "Советы как правильно выбрать шины",
        "F2_ADDITIONAL_URL" => "",
        "F2_ADDITIONAL_URL_TEXT" => "",
        "COMPONENT_TEMPLATE" => "top-filter"
    ),
    false
);

?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>