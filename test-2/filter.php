<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("test-2");
?>
<?



$APPLICATION->IncludeComponent(
    "bitrix:catalog.smart.filter",
    "visual_horizontal_car",
                    Array(
                        "IBLOCK_TYPE" => 'catalog', //
                        "IBLOCK_ID" => 23, //
                        "SECTION_ID" => 52, //
                        "FILTER_NAME" => 'arrFilter', //
                        "PRICE_CODE" => 'BASE', //
                        "TAB_TITLE" => 'Подбор шин',
                        "ICON_CLASS" => 'icon_tyrebold',
                        "CACHE_TYPE" => "A", //
                        "CACHE_TIME" => "36000", //
                        "CACHE_GROUPS" => "Y", //
                        "SAVE_IN_SESSION" => "N",
                        "XML_EXPORT" => "N",
                        "SECTION_TITLE" => "NAME",
                        "SECTION_DESCRIPTION" => "DESCRIPTION",
                        'HIDE_NOT_AVAILABLE' => "N", //
                        'ADDITIONAL_URL' => '/stat/kak_pravilno_vibrat_shiny.php',
                        'ADDITIONAL_URL_TEXT' => 'Советы как правильно выбрать шины',
                    ),
                    false,
                    array('HIDE_ICONS' => 'N')
);


?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>