<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("test-2");
?>

<?
/*
$APPLICATION->IncludeComponent("bitrix:catalog.search", "only_form", array(
	"IBLOCK_TYPE" => "catalog",
	"IBLOCK_ID" => "13",
	"ELEMENT_SORT_FIELD" => "sort",
	"ELEMENT_SORT_ORDER" => "asc",
	"ELEMENT_SORT_FIELD2" => "id",
	"ELEMENT_SORT_ORDER2" => "desc",
	"HIDE_NOT_AVAILABLE" => "N",
	"PAGE_ELEMENT_COUNT" => "30",
	"LINE_ELEMENT_COUNT" => "3",
	"PROPERTY_CODE" => array(
		0 => "",
		1 => "",
	),
	"OFFERS_FIELD_CODE" => array(
		0 => "",
		1 => "",
	),
	"OFFERS_PROPERTY_CODE" => array(
		0 => "",
		1 => "",
	),
	"OFFERS_SORT_FIELD" => "sort",
	"OFFERS_SORT_ORDER" => "asc",
	"OFFERS_SORT_FIELD2" => "id",
	"OFFERS_SORT_ORDER2" => "desc",
	"OFFERS_LIMIT" => "5",
	"SECTION_URL" => "",
	"DETAIL_URL" => "",
	"BASKET_URL" => "/personal/basket.php",
	"ACTION_VARIABLE" => "action",
	"PRODUCT_ID_VARIABLE" => "id",
	"PRODUCT_QUANTITY_VARIABLE" => "quantity",
	"PRODUCT_PROPS_VARIABLE" => "prop",
	"SECTION_ID_VARIABLE" => "SECTION_ID",
	"AJAX_MODE" => "N",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "N",
	"AJAX_OPTION_HISTORY" => "N",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "36000000",
	"DISPLAY_COMPARE" => "N",
	"PRICE_CODE" => array(
		0 => "BASE",
	),
	"USE_PRICE_COUNT" => "N",
	"SHOW_PRICE_COUNT" => "1",
	"PRICE_VAT_INCLUDE" => "Y",
	"USE_PRODUCT_QUANTITY" => "N",
	"CONVERT_CURRENCY" => "N",
	"OFFERS_CART_PROPERTIES" => array(
	),
	"RESTART" => "N",
	"NO_WORD_LOGIC" => "N",
	"USE_LANGUAGE_GUESS" => "Y",
	"CHECK_DATES" => "N",
	"PAGER_TEMPLATE" => ".default",
	"DISPLAY_TOP_PAGER" => "N",
	"DISPLAY_BOTTOM_PAGER" => "N",
	"PAGER_TITLE" => "Товары",
	"PAGER_SHOW_ALWAYS" => "N",
	"PAGER_DESC_NUMBERING" => "N",
	"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
	"PAGER_SHOW_ALL" => "N",
	"SEARCH_PAGE_URL" => "/search/",
	"AJAX_OPTION_ADDITIONAL" => ""
	),
	false
);*/

/*
$APPLICATION->IncludeComponent("bitrix:catalog.search", 
	"only_form_new", 
	array(
	"IBLOCK_TYPE" => "catalog",
	"IBLOCK_ID" => "13",
	"ELEMENT_SORT_FIELD" => "sort",
	"ELEMENT_SORT_ORDER" => "asc",
	"ELEMENT_SORT_FIELD2" => "id",
	"ELEMENT_SORT_ORDER2" => "desc",
	"HIDE_NOT_AVAILABLE" => "N",
	"PAGE_ELEMENT_COUNT" => "30",
	"LINE_ELEMENT_COUNT" => "3",
	"PROPERTY_CODE" => array(
		0 => "",
		1 => "",
	),
	"OFFERS_FIELD_CODE" => array(
		0 => "",
		1 => "",
	),
	"OFFERS_PROPERTY_CODE" => array(
		0 => "",
		1 => "",
	),
	"OFFERS_SORT_FIELD" => "sort",
	"OFFERS_SORT_ORDER" => "asc",
	"OFFERS_SORT_FIELD2" => "id",
	"OFFERS_SORT_ORDER2" => "desc",
	"OFFERS_LIMIT" => "5",
	"SECTION_URL" => "",
	"DETAIL_URL" => "",
	"BASKET_URL" => "/personal/basket.php",
	"ACTION_VARIABLE" => "action",
	"PRODUCT_ID_VARIABLE" => "id",
	"PRODUCT_QUANTITY_VARIABLE" => "quantity",
	"PRODUCT_PROPS_VARIABLE" => "prop",
	"SECTION_ID_VARIABLE" => "SECTION_ID",
	"AJAX_MODE" => "N",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "N",
	"AJAX_OPTION_HISTORY" => "N",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "36000000",
	"DISPLAY_COMPARE" => "N",
	"PRICE_CODE" => array(
		0 => "BASE",
	),
	"USE_PRICE_COUNT" => "N",
	"SHOW_PRICE_COUNT" => "1",
	"PRICE_VAT_INCLUDE" => "Y",
	"USE_PRODUCT_QUANTITY" => "N",
	"CONVERT_CURRENCY" => "N",
	"OFFERS_CART_PROPERTIES" => array(
	),
	"RESTART" => "N",
	"NO_WORD_LOGIC" => "N",
	"USE_LANGUAGE_GUESS" => "Y",
	"CHECK_DATES" => "N",
	"PAGER_TEMPLATE" => ".default",
	"DISPLAY_TOP_PAGER" => "N",
	"DISPLAY_BOTTOM_PAGER" => "N",
	"PAGER_TITLE" => "Товары",
	"PAGER_SHOW_ALWAYS" => "N",
	"PAGER_DESC_NUMBERING" => "N",
	"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
	"PAGER_SHOW_ALL" => "N",
	"SEARCH_PAGE_URL" => "/search/",
	"AJAX_OPTION_ADDITIONAL" => ""
	),
	false
);*/

use Yenisite\Core\Tools;

include 'include_module.php';
if (!\Bitrix\Main\Loader::IncludeModule('iblock')) die();
echo 'Yenisite\Core\Tools';

$result = Tools::GetDecodedArParams('eyJJQkxPQ0tfVFlQRSI6ImNhdGFsb2ciLCJJQkxPQ0tfSUQiOjEzLCJTRUNUSU9OX0lEIjoxNiwiRklMVEVSX05BTUUiOiJhcnJGaWx0ZXIiLCJQUklDRV9DT0RFIjpbIkJBU0UiXSwiVEFCX1RJVExFIjoiIFx1MDQxZlx1MDQzZVx1MDQzNFx1MDQzMVx1MDQzZVx1MDQ0MCBcdTA0NDhcdTA0MzhcdTA0M2QiLCJJQ09OX0NMQVNTIjoiaWNvbl90eXJlYm9sZCIsIkNBQ0hFX1RZUEUiOiJBIiwiQ0FDSEVfVElNRSI6IjM2MDAwIiwiQ0FDSEVfR1JPVVBTIjp0cnVlLCJTQVZFX0lOX1NFU1NJT04iOmZhbHNlLCJYTUxfRVhQT1JUIjoiTiIsIlNFQ1RJT05fVElUTEUiOiJOQU1FIiwiU0VDVElPTl9ERVNDUklQVElPTiI6IkRFU0NSSVBUSU9OIiwiSElERV9OT1RfQVZBSUxBQkxFIjoiTiIsIkFERElUSU9OQUxfVVJMIjoiXC9zdGF0XC9rYWtfcHJhdmlsbm9fdmlicmF0X3NoaW55LnBocCIsIkFERElUSU9OQUxfVVJMX1RFWFQiOiJcdTA0MjFcdTA0M2VcdTA0MzJcdTA0MzVcdTA0NDJcdTA0NGIgXHUwNDNhXHUwNDMwXHUwNDNhIFx1MDQzZlx1MDQ0MFx1MDQzMFx1MDQzMlx1MDQzOFx1MDQzYlx1MDQ0Y1x1MDQzZFx1MDQzZSBcdTA0MzJcdTA0NGJcdTA0MzFcdTA0NDBcdTA0MzBcdTA0NDJcdTA0NGMgXHUwNDQ4XHUwNDM4XHUwNDNkXHUwNDRiIiwiSU5TVEFOVF9SRUxPQUQiOmZhbHNlLCJDT05WRVJUX0NVUlJFTkNZIjpmYWxzZSwiQ1VSUkVOQ1lfSUQiOiIifQ==');

//var_dump($result);
// foreach ($result as $key => $value) {
// 	echo '<pre>';
// 		echo $key.'<br />';
// 		var_dump($value);
// 	echo '</pre>';
// }

$APPLICATION->IncludeComponent("bitrix:catalog.smart.filter", "null_test", Tools::GetDecodedArParams($result));
?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>